# How to submit tasks to the repository

First of all you have to install:

- [**git**](https://git-scm.com/downloads)
- [**GitHub Desktop**](https://desktop.github.com/) on your machine.

If you encounter some troubles try to [google](https://www.google.ru/) the issue or contact _@tymurlysenkodev_ via telegram.

## 1. Clone the repository **(do this only once!)**

1. Go to the [main page of the repository](https://bitbucket.org/sitiritis/itp-b2018-02/src/master/), find the **"Clone"** button and click it **(1)**. After copy the repository link (only link, without `git clone`) **(2)**

![clone repository](./img/clone_repository.bmp)

2. Open **GitHub Desktop**

    If you open it for the **first time** it will suggest you to log in via github account. You can do that if you wish. After it will ask you for the name and email which will be used for commits. Please, provide real ones

    Then you should see main screen of the program

    1. Click **"Clone a repository"** button
    2. Naviagte to the **"URL"** tab
    3. Paste copied link into as a repository URL
    4. Choose a path to download the repository
    5. Click **"Clone"** button

    ![GitHub Desktop clone repository](./img/github-desktop_clone_repo.bmp)

Congratulations, you've cloned the repository to your machine!

## 2. Sending assignments

### 1. Commit your code into a new branch and push it

1.  Open **GitHub Desktop** and select _ITP-B2018-02_ repository by clicking top left button **"Current repository"**

2. Switch to the **master** branch, by selecting it in **"Current branch"** drop down menu

    ![Switch to master](./img/switch_to_master.bmp)

3. Click the **"Fetch origin"** and **"Pull"** button, **so that you're sure that you work with the latest state of the repository**

    ![Pull changes](./img/pull.bmp)

4. Using file explorer, put your Eiffel project (\*.e and \*.ecf files) to folder **/src/_Firstname\_Lastname_/_Lab#_/_task\_name_/**, where

    - _Firstname\_Lastname_ are yours first and last names (e. g. Tymur Lysenko)
    - _Lab#_ is a number of a lab you want to submit (e. g. Lab 2 (b))
    - _task\_name_ is a name of a task in for a lab work (e. g. Calendar)
  
      For example, my Eiffel project for the calendar from lab #2 will have a path /src/Tymur Lysenko/Lab 2 (b)/Calendar/

5. Create a new branch with name like **_FirstnamLastname-currentdate-lab#_**, by cliking **"Branch"** -> **"New branch..."**. After click **"Create branch"**

6. Make sure that your **"Current branch"** is the branch you've just created

7. Select files that you want to commit by ticking them

8. Write some informative commit message

9. Click **"Commit to _branchname_"** button

    ![Commit changes](./img/commit_changes.bmp)

10. **"Publish branch"**

    ![Publish branch](./img/bublish_branch.bmp)

### 2. Send pull request from the branch

1. Go to the repository and open [**"Pull requests"** tab](https://bitbucket.org/sitiritis/itp-b2018-02/pull-requests/)

2. Click **"Create pull request"**

    ![Create pull request](./img/create_pull_request.bmp)

3. Select your pushed branch on the left as a source of merge

4. Select **master** branch as a target of merge on the right

5. Tick **Close _branchname_ after pull request is merged**

6. Click **"Create pull request"** button

    ![Send pull request](./img/send_pull_request.jpg)
