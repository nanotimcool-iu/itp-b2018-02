note
	description: "bin_tree application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	a, b: AWFUL_BINARY_TREE[INTEGER]

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create a.make (5)
			create b.make (1)
			a.add_left (b)
			create b.make (2)
			a.add_right (b)
			create b.make (3)
			a.left.add_left (b)
			print(a.height.out + "%N")
			a.left.add_left (b)
		end

end
