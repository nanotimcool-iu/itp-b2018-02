note
	description: "Summary description for {AWFUL_BINARY_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	AWFUL_BINARY_TREE[G]

create make

feature
	val: G
	left: detachable AWFUL_BINARY_TREE[G]
	right: detachable AWFUL_BINARY_TREE[G]

feature
	make(v: G)
	do
		val := v
	end

feature
	height: INTEGER
	do
		if left /= Void and right /= Void then
			if left.height > right.height then
				Result := left.height + 1
			else
				Result := right.height + 1
			end
		elseif not (left /= Void) then
			if right /= Void then
				Result := right.height + 1
			else
				Result := 1
			end
		else
			if left /= Void then
				Result := left.height + 1
			else
				Result := 1
			end
		end
	end

	add_left(t: AWFUL_BINARY_TREE[G])
	do
		if left /= Void then
			print("node already has left child")
		else
			left := t
		end
	end

	add_right(t: AWFUL_BINARY_TREE[G])
	do
		if right /= Void then
			print("node already has right child")
		else
			right := t
		end
	end

end
