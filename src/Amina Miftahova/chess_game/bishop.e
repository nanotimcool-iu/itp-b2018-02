note
	description: "Summary description for {BISHOP}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BISHOP

inherit FIGURE

create make

feature
	can_move(n,m: INTEGER; b: CHESS_BOARD): BOOLEAN
	local
		i, s, t: INTEGER
		l : BOOLEAN
	do
		l := true
		if (n - row). abs = (m - col).abs then
			if n > row then
				if m > col then
					from
						i := 1
					until
						i > (n - row).abs - 1
					loop
						if attached b.chess_board[row + i, col + i].figure then
							l := false
						end
						i := i + 1
					end
				else
					from
						i := 1
					until
						i > (n - row).abs - 1
					loop
						if attached b.chess_board[row + i, col - i].figure then
							l := false
						end
						i := i + 1
					end

				end
			else
				if m > col then
					from
						i := 1
					until
						i > (n - row).abs - 1
					loop
						if attached b.chess_board[row - i, col + i].figure then
							l := false
						end
						i := i + 1
					end
				else
					from
						i := 1
					until
						i > (n - row).abs - 1
					loop
						if attached b.chess_board[row - i, col - i].figure then
							l := false
						end
						i := i + 1
					end

				end
			end
		else
			l := false
		end
		Result := l
	end

end
