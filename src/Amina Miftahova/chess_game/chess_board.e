note
	description: "Summary description for {CHESS_BOARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CHESS_BOARD

create make

feature
	chess_board: ARRAY2[CHESS_CELL]

feature
	make
	local
		c: CHESS_CELL
		b: BISHOP
		k: KING
		h: KNIGHT
		pb, pw: PAWN
		q: QUEEN
		r: ROOK
		i: INTEGER
	do
		create c.make
		create chess_board.make_filled (c, 8, 8)
		create r.make("black", 8, 1)
		chess_board[8,1].attach_figure (r)
		create r.make("black", 8, 8)
		chess_board[8,8].attach_figure (r)
		create r.make("white", 1, 1)
		chess_board[1,1].attach_figure (r)
		create r.make("white", 1, 8)
		chess_board[1,8].attach_figure (r)
		create h.make("black", 8, 2)
		chess_board[8,2].attach_figure (h)
		create h.make("black", 8, 7)
		chess_board[8,7].attach_figure (h)
		create h.make("white", 1, 2)
		chess_board[1,2].attach_figure (h)
		create h.make("white", 1, 7)
		chess_board[1,7].attach_figure (h)
		create b.make("black", 8, 3)
		chess_board[8,3].attach_figure (b)
		create b.make("black", 8, 6)
		chess_board[8,6].attach_figure (b)
		create b.make("white", 1, 3)
		chess_board[1,3].attach_figure (b)
		create b.make("white", 1, 6)
		chess_board[1,6].attach_figure (b)
		create q.make("black", 8, 4)
		chess_board[8,4].attach_figure (q)
		create q.make("white", 1, 4)
		chess_board[1,4].attach_figure (q)
		create k.make("black", 8, 5)
		chess_board[8,5].attach_figure (k)
		create k.make("white", 1, 5)
		chess_board[1,5].attach_figure (k)
		from
			i := 1
		until
			i > 8
		loop
			create pb.make("black", 7, i)
			create pw.make("white", 2, i)
			chess_board[7,i].attach_figure (pb)
			chess_board[2,i].attach_figure (pw)
			i := i + 1
		end
	end

	can_move(p1r, p1c, p2r, p2c: INTEGER): BOOLEAN
	require
		valid_cells: (0 < p1r) and (9 > p1r) and (0 < p2r) and (9 > p2r) and (0 < p1c) and (9 > p1c) and (0 < p2c) and (9 > p2c)
		piece_exists: chess_board[p1r, p1c].figure /= Void
	local
		b: BOOLEAN
	do
		if attached chess_board[p1r, p1c].figure as f then
			if attached chess_board[p2r, p2c].figure as g then
				if not (f.colour ~ g.colour) then
					b := true
				else
					b := false
				end
			end
			b := b and f.can_move(p2r, p2c, Current)
		end
		Result := b
	end

	move_piece(p1r, p1c, p2r, p2c: INTEGER)
	require
		valid_cells: (0 < p1r) and (9 > p1r) and (0 < p2r) and (9 > p2r) and (0 < p1c) and (9 > p1c) and (0 < p2c) and (9 > p2c)
		piece_exists: chess_board[p1r, p1c].figure /= Void
	do
		if can_move(p1r, p1c, p2r, p2c) then
			if attached chess_board[p1r, p1c].figure as f then
				f.move(p2r, p2c, Current)
				if attached chess_board[p2r, p2c].figure as g then
					g.out_of_game
				end
				chess_board[p2r, p2c].attach_figure (f)
			end
			chess_board[p2r, p2c].delete_figure
		end
	end

end
