note
	description: "Summary description for {CHESS_CELL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CHESS_CELL

create make

feature
	figure: detachable FIGURE

feature
	make
	do
		print("ok here's your chess cell")
	end

	attach_figure(f: FIGURE)
	do
		figure := f
	end

	delete_figure
	do
		figure := Void
	end

end
