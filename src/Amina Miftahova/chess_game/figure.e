note
	description: "Summary description for {FIGURE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	FIGURE

feature
	row, col: INTEGER
	in_game: BOOLEAN
	colour: STRING

feature
	make (c: STRING; n, m: INTEGER)
	do
		in_game := true
		colour := c
		row := n
		col := m
	end

feature

	out_of_game
	do
		in_game := false
	end

	position: STRING
	do
		Result := "(" + row.out + ", " + col.out + ")"
	end

	can_move(n, m: INTEGER; b: CHESS_BOARD): BOOLEAN
	require
		valid_cell: (n < 9) and (m < 9)
	deferred
	end

	move(n, m: INTEGER; b: CHESS_BOARD)
	do
		if can_move(n, m, b) then
			row := n
			col := m
		end
	end

invariant
	val_row: row < 9
	val_col: col < 9

end
