note
	description: "Summary description for {KNIGHT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	KNIGHT

inherit FIGURE

create make

feature
	can_move(n,m: INTEGER; b: CHESS_BOARD): BOOLEAN
	do
		if ((n - row).abs = 2) and ((m - col.abs) = 1) then
			Result := true
		elseif ((n - row).abs = 2) and ((m - col.abs) = 2) then
			Result := true
		else
			Result := false
		end
	end

end
