note
	description: "Summary description for {PAWN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAWN

inherit FIGURE

create make

feature
	can_move(n, m: INTEGER; b: CHESS_BOARD): BOOLEAN
	do
		if attached b.chess_board[n,m].figure as g then
			if not (g.colour ~ colour) then
				if ((n - row).abs = 1) and ((m - col).abs = 1) then
					Result := true
				else
					Result := false
				end
			else
				Result := false
			end
		else
			if m = col + 1 then
				Result := true
			else
				Result := false
			end
		end
	end

end
