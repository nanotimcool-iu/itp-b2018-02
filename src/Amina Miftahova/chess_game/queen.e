note
	description: "Summary description for {QUEEN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	QUEEN

inherit FIGURE

create make

feature
	can_move(n,m: INTEGER; b: CHESS_BOARD): BOOLEAN
	local
		bish: BISHOP
		rook: ROOK
	do
		create bish.make (colour, row, col)
		create rook.make (colour, row, col)
		Result := bish.can_move (n, m, b) or rook.can_move (n, m, b)
	end

end
