note
	description: "Summary description for {ROOK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ROOK

inherit FIGURE

create make

feature
	can_move(n, m: INTEGER; b: CHESS_BOARD): BOOLEAN
	local
		i, t, s: INTEGER
		f: BOOLEAN
	do
		f := false
		if n = row then
			if m > col then
				s := col
				t := m
			else
				s := m
				t := col
			end
			from
				i := s
			until
				i > t
			loop
				if attached b.chess_board[n, i].figure as k then
					f := true
				end
				i := i + 1
			end
		elseif m = col then
			if n > row then
				s := row
				t := n
			else
				s := n
				t := row
			end
			from
				i := s
			until
				i > t
			loop
				if attached b.chess_board[i, m].figure as k then
					f := true
				end
				i := i + 1
			end
		else
			f := true
		end
		if f then
			Result := false
		else
			Result := true
		end
	end

end
