note
	description: "cipher application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	c: COMBINED_CIPHER
	s: STRING

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			-- key: YQLRFYNTTNSDSYCN
			create c.make
			s := "LAGHYH QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF   FAZ2/TGBZKFIREE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIULZIE KFYGEL//:RLH DXE Z"
			print(s.count.out + "%N")
			s := c.decrypt (s)
			print(s)
		end

end
