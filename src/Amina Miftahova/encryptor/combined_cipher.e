note
	description: "Summary description for {COMBINED_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMBINED_CIPHER

inherit
	CIPHER
	redefine
		encrypt, decrypt
	end

create make

feature
	v: VIGENERE_CIPHER
	sp: SPIRAL_CIPHER

feature

	make
	do
		create v
		create sp
	end
	
	encrypt(s: STRING): STRING
	local
		cip: STRING
	do
		cip := v.encrypt(s)
		cip := sp.encrypt(cip)
		Result := cip
	end

	decrypt(s: STRING): STRING
	local
		mes: STRING
	do
		mes := sp.decrypt(s)
		mes := v.decrypt(mes)
		Result := mes
	end

end
