note
	description: "Summary description for {SPIRAL_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SPIRAL_CIPHER

inherit
	CIPHER
	redefine
		encrypt, decrypt
	end

feature

	square(a: INTEGER): INTEGER
	local
		n, i: INTEGER
	do
		n := 1
		from
			i := n
		until
			i > 1000
		loop
			if (n * n) >= a then
				i := 1002
			else
				n := n + 1
			end
			i := i + 1
		end
		Result := n
	end

	encrypt(s: STRING): STRING
	local
		t: ARRAY2[STRING]
		i, j, m, l, k, left, right, up, down: INTEGER
		mes, cip: STRING
	do
		l := 1
		cip := ""
		mes := s
		print(mes.count.out + "%N")
		m := square(mes.count)
		print(m.out + "%N")
		create t.make_filled ("", m, m)
		from
			i := 1
		until
			i > m
		loop
			from
				j := 1
			until
				j > m
			loop
				if l > mes.count then
					j := m
					i := m
				else
					t[i,j] := mes[l].out
					l := l + 1
				end
				j := j + 1
			end
			i := i + 1
		end
		up := 1
		left := 1
		right := m
		down := m
		from
			i := 1
		until
			i > m * m
		loop
			from
				j := up
			until
				j > down
			loop
				cip := cip + t[j, right]
				j := j + 1
				i := i + 1
			end
			right := right - 1
			from
				j := right
			until
				j < left
			loop
				cip := cip + t[down, j]
				j := j - 1
				i := i + 1
			end
			down := down - 1
			from
				j := down
			until
				j < up
			loop
				cip := cip + t[j, left]
				j := j - 1
				i := i + 1
			end
			left := left + 1
			from
				j := left
			until
				j > right
			loop
				cip := cip + t[up, j]
				j := j + 1
				i := i + 1
			end
			up := up + 1
		end
		Result := cip
	end

	decrypt(s: STRING): STRING
	local
		cip, mes: STRING
		t: ARRAY2[STRING]
		i, j, k, left, right, up, down, m, emp, q: INTEGER
	do
		mes := ""
		cip := s
		m := square(cip.count)
		print(m.out + "%N")
		emp := m * m - cip.count
		create t.make_filled ("", m, m)
		k := 1
		from
			i := 1
		until
			i > (m - 1)
		loop
			t[i, m] := cip[k].out
			k := k + 1
			i := i + 1
		end
		j := m
		from
			i := 1
		until
			i > emp or j < 1
		loop
			j := j - 1
			i := i + 1
		end
		q := m - 1
		if i <= emp then
			from
				i := i
			until
				i > emp
			loop
				q := q - 1
				i := i + 1
			end
			from
				j := q
			until
				j < 1
			loop
				t[j, 1] := cip[k].out
				k := k + 1
				j := j -1
			end
		else
			from
				j := j
			until
				j < 1
			loop
				t[m, j] := cip[k].out
				k := k + 1
				j := j - 1
			end
			from
				j := m -1
			until
				j < 1
			loop
				t[j, 1] := cip[k].out
				k := k + 1
				j := j - 1
			end
		end
		from
			j := 2
		until
			j > (m - 1)
		loop
			t[1, j] := cip[k].out
			k := k + 1
			j := j + 1
		end
		left := 2
		right := m - 1
		up := 2
		down := m - 1
		from
			i := 1
		until
			i > (m -2) * (m - 2)
		loop
			from
				j := up
			until
				j > down
			loop
				if k <= cip.count then
					t[j, right] := cip[k].out
					k := k + 1
				end
				j := j + 1
				i := i + 1
			end
			right := right - 1
			from
				j := right
			until
				j < left
			loop
				if k <= cip.count then
					t[down, j] := cip[k].out
					k := k + 1
				end
				j := j - 1
				i := i + 1
			end
			down := down - 1
			from
				j := down
			until
				j < up
			loop
				if k <= cip.count then
					t[j, left] := cip[k].out
					k := k + 1
				end
				j := j - 1
				i := i + 1
			end
			left := left + 1
			from
				j := left
			until
				j > right
			loop
				if k <= cip.count then
					t[up, j] := cip[k].out
					k := k + 1
				end
				j := j + 1
				i := i + 1
			end
			up := up + 1
		end
		from
			i := 1
		until
			i > m
		loop
			from
				j := 1
			until
				j > m
			loop
				mes := mes + t[i, j]
				j := j + 1
			end
			i := i + 1
		end
		print(t[1,1].out + "%N")
		print(t[1,m].out + "%N")
		print(t[m,m].out + "%N")
		print(t[m,1].out + "%N")
		print(t[6,6].out + "%N")
		Result := mes
	end

end
