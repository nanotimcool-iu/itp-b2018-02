note
	description: "Summary description for {VIGENERE_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHER

inherit
	CIPHER
	redefine
		encrypt, decrypt
	end

feature
	encrypt(s: STRING): STRING
	local
		mes, key, cip, k, m: STRING
		i, a, l: INTEGER
	do
		l := 1
		cip := ""
		mes := s
		io.put_string ("enter the encryption key:%N")
		io.read_line
		key := io.last_string.twin
		from
			i := 1
		until
			i > mes.count
		loop
			if mes[i].code < 65 or mes[i].code >90 then
				cip := cip + mes[i].out
			else
				m := mes[i].out
				if (l \\ key.count) = 0 then
					k := key[key.count].out
				else
					k := key[l \\ key.count].out
				end
				l := l + 1
				a := (m.code (1).as_integer_32 + k.code (1).as_integer_32 - 2 * 65)\\26
				cip.append_code (a.as_natural_32 + 65)
			end
			i := i + 1
		end
		Result := cip
	end

	decrypt(s: STRING): STRING
	local
		cip, key, mes, c, k: STRING
		i, l, a: INTEGER
	do
		l := 1
		mes := ""
		cip := s
		io.put_string ("enter the decryption key:%N")
		io.read_line
		key := io.last_string.twin
		from
			i := 1
		until
			i > cip.count
		loop
			if cip[i].code < 65 or cip[i].code > 90 then
				mes := mes + cip[i].out
			else
				c := cip[i].out
				if (l \\ key.count) = 0 then
					k := key[key.count].out
				else
					k := key[l\\key.count].out
				end
				l := l + 1
				a := (c.code (1).as_integer_32 - k.code (1).as_integer_32)
				if a <0 then
					a := a + 26
				end
				a := a\\26
				mes.append_code (a.as_natural_32 + 65)
			end
			i := i + 1
		end
		Result := mes
	end

end
