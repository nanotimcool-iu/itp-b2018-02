note
	description: "hangman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	game: HANGMAN_GAME

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create game.make
		end

end
