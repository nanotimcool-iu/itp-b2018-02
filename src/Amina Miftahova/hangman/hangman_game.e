note
	description: "Summary description for {HANGMAN_GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HANGMAN_GAME

create
	make

feature
	dict: ARRAYED_LIST[STRING]
	word, palochki: STRING
	num_of_players, num_of_guesses, guesses, cur_player: INTEGER
	names: ARRAYED_LIST[STRING]
	finished: BOOLEAN
	won: ARRAYED_LIST[BOOLEAN]

feature
	make
	do
		print("the game was created!%N")
		start_game
	end

	create_dict
	do
		create dict.make (0)
		dict.extend ("cat")
		dict.extend ("monkey")
		dict.extend ("aardvark")
		dict.extend ("hamster")
		dict.extend ("opossum")
		dict.extend ("quokka")
		dict.extend ("donkey")
		dict.extend ("wombat")
		dict.extend ("armadillo")
		dict.extend ("badger")
		dict.extend ("binturong")
		dict.extend ("whale")
		dict.extend ("walrus")
		dict.extend ("porcupine")
		dict.extend ("pigeon")
		dict.extend ("dolphin")
		dict.extend ("crane")
		dict.extend ("raccoon")
		dict.extend ("tapir")
		dict.extend ("lizard")
		dict.extend ("hedgehog")
		dict.extend ("goose")
		dict.extend ("frog")
		dict.extend ("octopus")
		dict.extend ("beaver")
		dict.extend ("cicada")
		dict.extend ("albatross")
		dict.extend ("eagle")
		dict.extend ("echidna")
		dict.extend ("lion")
		dict.extend ("snake")
		dict.extend ("rat")
		dict.extend ("goat")
		dict.extend ("hummingbird")
		dict.extend ("horse")
		dict.extend ("scunc")
		dict.extend ("deer")
		dict.extend ("bear")
		dict.extend ("pig")
		dict.extend ("bat")
	end

	start_game
	do
		create_dict
		finished := false
		guesses := 0
		palochki := ""
		io.put_string ("enter the number of players: ")
		io.read_integer
		set_players(io.last_integer)
		cur_player := 0
		gameplay
	end

	gameplay
	local
		i, n, j: INTEGER
		a: STRING
		t: BOOLEAN
	do
		n := rand_num \\ dict.count
		if n = 0 then
			n := 1
		end
		word := dict[n]
		if word.count > Current.num_of_players then
			num_of_guesses := word.count
		else
			num_of_guesses := num_of_players
		end
		from
			j := 1
		until
			j > word.count
		loop
			palochki := palochki + "_"
			j := j + 1
		end
		from
			guesses := 1
		until
			guesses > Current.num_of_guesses or Current.finished
		loop
			print("the word is: " + palochki + "%N")
			cur_player := cur_player + 1
			if cur_player = num_of_players + 1 then
				cur_player := 1
			end
			t := false
			io.put_string (names[cur_player] + ", enter the letter: ")
			io.read_line
			from
				i := 1
			until
				i > word.count
			loop
				if word[i].out ~ io.last_string then
					t := true
					a := palochki
					palochki := a.substring (1, i-1) + io.last_string.twin + a.substring ((i + 1), (word.count))
				end
				i := i + 1
			end
			if t then
				print("right!%N")
			else
				print("you are wrong, there is no such letter in the word%N")
			end
			is_finished
			if finished then
				won[cur_player] := true
			end
			guesses := guesses + 1
		end
		finished := true
		end_game
	end

	is_finished
	local
		i: INTEGER
		z: BOOLEAN
	do
		z := true
		from
			i := 1
		until
			i > palochki.count
		loop
			if palochki[i].out ~ "_" then
				z := false
			end
			i := i + 1
		end
		if z then
			finished := true
		end
	end

	set_players(a: INTEGER)
	require
		ok: a > 0
	local
		i: INTEGER
	do
		num_of_players := a
		create names.make (0)
		create won.make(0)
		from
			i := 1
		until
			i > a
		loop
			io.put_string ("enter player name: ")
			io.read_line
			names.extend (io.last_string.twin)
			won.extend(false)
			i := i + 1
		end
	end

	end_game
	require
		game_is_finished: Current.finished = true
	local
		i: INTEGER
		o: BOOLEAN
	do
		o := false
		print("game is finished! the word is: " + word + "%N")
		from
			i := 1
		until
			i > Current.num_of_players
		loop
			if Current.won[i] then
				o := true
				print(Current.names[i] + " is the winner!%N")
				i := Current.num_of_players
			end
			i := i + 1
		end
		if not o then
			print("you lose!")
		end
	end

	rand_num: INTEGER
	local
		r: RANDOM
		i: INTEGER
	do
		create r.make
		from
			i := 1
		until
			i > num_of_players
		loop
			r.forth
			i := i + 1
		end
		Result := r.item
	end

end
