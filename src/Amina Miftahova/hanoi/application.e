note
	description: "hanoi application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			hanoi(4, 'a', 'b', 'c')
		end

feature

	hanoi(n: INTEGER; s, t, o: CHARACTER)
	do
		if n = 1 then
			print("move disk " + n.out + " from " + s.out + " to " + t.out + "%N")
		else
			hanoi(n-1, s, o, t)
			print("move disk " + n.out + " from " + s.out + " to " + t.out + "%N")
			hanoi(n-1, o, t, s)
		end
	end

end
