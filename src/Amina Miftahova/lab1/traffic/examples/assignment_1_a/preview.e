note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		do
			Zurich.add_station ("ugh", -800, -700)
			Zurich.append_station (6, "ugh")
			Zurich_map.update
			Zurich_map.station_view(Zurich.station ("ugh")).highlight
			wait(2)
			Zurich_map.station_view(Zurich.station ("ugh")).unhighlight
			wait(2)
			Zurich_map.station_view(Zurich.station ("ugh")).highlight
			wait(2)
			Zurich_map.station_view(Zurich.station ("ugh")).unhighlight
			wait(2)
			Zurich_map.station_view(Zurich.station ("ugh")).highlight
			Zurich_map.update
			Zurich_map.animate

		end

end
