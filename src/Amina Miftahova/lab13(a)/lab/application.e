note
	description: "lab application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	l: LAB
	b: BIOLOGIST
	b2: BIO_INFORMATIC
	c: COMPUTER_SCIENTIST

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create l.make
			create b.make (1, "Oleg")
			create b2.make (2, "Igor")
			create c.make (3, "Vasya")
			l.add_member (b)
			l.add_member (b2)
			l.add_member (c)
			l.introduce_all
			l.remove_member (b)
			l.introduce_all
		end

end
