note
	description: "Summary description for {BIO_INFORMATIC}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIO_INFORMATIC

inherit
	COMPUTER_SCIENTIST
	undefine
		make
	end
	BIOLOGIST
	redefine
		make
	end

create make

feature
	short_biography: STRING

	make(i: INTEGER; n: STRING)
	local
		p: PET
		pet_name, pet_type: STRING
	do
		io.put_string ("enter the name of a pet: ")
		io.read_line
		pet_name := io.last_string.twin
		io.put_string ("enter the type of a pet: ")
		io.read_line
		pet_type := io.last_string.twin
		create p.make (pet_name, pet_type)
		id := i
		name := n
		discipline := "Bio-informaticians"
		pet := p
		io.put_string ("enter the bio: ")
		io.read_line
		short_biography := io.last_string.twin
	end

end
