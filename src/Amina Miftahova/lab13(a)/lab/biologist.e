note
	description: "Summary description for {BIOLOGIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIOLOGIST

inherit SCIENTIST
	redefine
		make
	end

create make

feature
	pet: PET

feature
	make(i: INTEGER; n: STRING)
	local
		p: PET
		pet_name, pet_type: STRING
	do
		io.put_string ("enter the name of a pet: ")
		io.read_line
		pet_name := io.last_string.twin
		io.put_string ("enter the type of a pet: ")
		io.read_line
		pet_type := io.last_string.twin
		create p.make (pet_name, pet_type)
		id := i
		name := n
		discipline := "Biologist"
		pet := p
	end

end
