note
	description: "Summary description for {COMPUTER_SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMPUTER_SCIENTIST

inherit SCIENTIST
	redefine
		make
	end

create make

feature
	make(i: INTEGER; n: STRING)
	do
		id := i
		name := n
		discipline := "Computer scientist"
	end

end
