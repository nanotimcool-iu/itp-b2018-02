note
	description: "Summary description for {LAB}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LAB

create make

feature
	members: ARRAYED_LIST[SCIENTIST]

feature
	make
	do
		create members.make(0)
	end

	add_member(s: SCIENTIST)
	do
		members.extend(s)
	end

	remove_member(s: SCIENTIST)
	require
		exists: members.has (s)
	local
		t: ARRAYED_LIST[SCIENTIST]
		i: INTEGER
	do
		create t.make (0)
		from
			i := 1
		until
			i > members.count
		loop
			if not (members[i] = s) then
				t.extend (members[i])
			end
			i := i + 1
		end
		members := t
	end

	introduce_all
	local
		i: INTEGER
	do
		from
			i := 1
		until
			i > members.count
		loop
			members[i].introduce
			print("%N")
			i := i + 1
		end
	end

end
