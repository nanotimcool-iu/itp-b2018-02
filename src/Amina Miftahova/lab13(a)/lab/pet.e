note
	description: "Summary description for {PET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PET

create make

feature
	name, type: STRING

feature
	make(n, t: STRING)
	do
		name := n
		type := t
	end

end
