note
	description: "Summary description for {SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SCIENTIST

feature
	id: INTEGER

	name, discipline: STRING

	make(i: INTEGER; n: STRING)
	do
		id := i
		name := n
	end

	introduce
	do
		print("my name is " + name + ", my id is " + id.out + " and i am " + discipline + "%N")
	end

end
