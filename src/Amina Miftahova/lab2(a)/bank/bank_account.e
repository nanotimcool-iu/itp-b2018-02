class
	BANK_ACCOUNT

inherit
	ARGUMENTS

create
	make

feature
	name: STRING
	balance: INTEGER

feature{NONE}
	make
		do
			io.put_string ("your name")
			io.new_line
			io.read_line
			set_name(io.last_string)
			set_balance
			print(name)
			print(balance)
		end

feature
	set_name(s: STRING)
		do
			name := s
		end

feature
	set_balance
		do
			balance := 0
		end

feature
	deposit(s: INTEGER)
		do
			if (balance + s)>1000000 then
				io.put_string ("your balance cannot be more than 1 million")
			else
				balance := balance + s
			end
		end

feature
	withdraw(s:INTEGER)
		do
			balance := balance - s
		end

end
