note
class
	COURSE

inherit
	ARGUMENTS

create
	create_course

feature{NONE}
	create_course
		do
			io.put_string ("course name:")
			io.new_line
			io.read_line
			set_name(io.last_string.twin)
			io.put_string ("course id:")
			io.new_line
			io.read_integer
			set_id(io.last_integer)
			io.put_string ("course schedule:")
			io.new_line
			io.read_line
			set_schedule(io.last_string.twin)
			io.put_string ("maximum student number:")
			io.new_line
			io.read_integer
			if io.last_integer < 3 then
				io.put_string ("number should be more than 2")
				io.new_line
				io.read_integer
			end
			set_max_student(io.last_integer)
			io.put_string (name+"%N")
			io.put_string (schedule+"%N")
			io.put_integer (id)
		end

feature
	name: STRING
	id: INTEGER
	schedule: STRING
	max_student: INTEGER

feature
	set_name(s: STRING)
		do
			name := s
		end

feature
	set_id(s: INTEGER)
		do
			id := s
		end

feature
	set_schedule(s: STRING)
		do
			schedule := s
		end

feature
	set_max_student(s: INTEGER)
		do
			max_student := s
		end


end
