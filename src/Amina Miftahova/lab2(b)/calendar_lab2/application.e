note
	description: "lab2_calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	day1, day2, day3: DATE_TIME
	ktoto, ktoto2: PERSON
	event1, event2, event3: ENTRY
	calend: CALENDAR

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create day1.make (2018, 09, 13, 12, 30, 00)
			create day2.make(2018, 09, 13, 12, 20, 00)
			create day3.make(2018,09,13,9,30,00)
			create ktoto.c_person ("oleg", 12354, "somewhere", "oleg@gmail.com")
			create ktoto2.c_person ("igor", 1634, "firefighter", "igorrr@gmail.com")
			create calend.c_calendar
			event1 := calend.create_entry (day1, ktoto)
			event2 := calend.create_entry (day2, ktoto)
			event3 := calend.create_entry (day2, ktoto2)
			print(calend.in_conflict(event1, event2))
			print("%N")
			print(calend.in_conflict(event2, event3))
			print("%N")
			calend.edit_subject (event1, "playing football")
			calend.edit_date (event2, day3)
			print(calend.in_conflict (event1, event2))
			print("%N")
			print(calend.get_owner_name (event1))
		end

end
