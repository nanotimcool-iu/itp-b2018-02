note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	c_entry

feature
	date: DATE_TIME
	owner: PERSON
	subject: STRING
	place: STRING

feature
	c_entry(d: DATE_TIME; o: PERSON; s: STRING; p: STRING)
	do
		date := d
		owner := o
		subject := s
		place := p
	end

feature
	set_subject(s: STRING)
	do
		subject := s
	end

feature
	set_date(d: DATE_TIME)
	do
		date := d
	end

end
