note
	description: "lab2_cms application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature

	cms1: CMS
	cms2: CMS
	cms3: CMS

	c1: CONTACT
	c2: CONTACT
	c3: CONTACT

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create cms1.smth
			c1 := cms1.create_contact
			create cms2.smth
			c2 := cms2.create_contact
			create cms3.smth
			c3 := cms3.create_contact
			cms1.add_emergency_contact(c1,c2)
			print(c1.call_emergency)
			print("%N")
			cms2.add_emergency_contact(c2,c3)
			print(c2.call_emergency)
			print("%N")
			cms2.edit_contact(c2)
			cms2.remove_emergency_contact(c2)
			if c2.call_emergency /= Void then
				print(c2.call_emergency)
			else
				print("none")
			end
			print("%N")
		end

end
