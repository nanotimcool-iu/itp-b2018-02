note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

create
	smth

feature

	smth
	do
		io.put_string("this is CMS" + "%N")
	end

feature

	create_contact: CONTACT
	local
		n: STRING
		p: INTEGER
		w: STRING
		e: STRING
		d: CONTACT

	do
		io.put_string ("please, enter contact name: ")
		io.read_line
		n := io.last_string.twin
		io.new_line

		io.put_string ("please, enter contact phone number: ")
		io.read_integer
		if io.last_integer > 9999999999 then
			io.new_line
			io.put_string ("please, enter a correct phone number: ")
			io.read_integer
		else
			if io.last_integer < 1000000000 then
				io.new_line
				io.put_string ("please, enter a correct phone number: ")
				io.read_integer
			end
		end
		p := io.last_integer.twin
		io.new_line

		io.put_string ("please, enter contact work place: ")
		io.read_line
		w := io.last_string.twin
		io.new_line

		io.put_string ("please, enter contact email: ")
		io.read_line
		e := io.last_string.twin
		io.new_line

		create d.make(n,p,w,e)
		Result := d
	end

feature

	edit_contact(c: CONTACT)
	do
		io.put_string ("if you want to change name, enter 1" + "%N" + "if you want to change phone number, enter 2" + "%N" +"if you want to change work place, enter 3" + "%N" + "if you want to change email, enter 4" + "%N")
		io.read_integer
		io.new_line
		inspect io.last_integer
		when 1 then
			io.put_string ("enter new name: ")
			io.read_line
			c.set_name (io.last_string)
		when 2 then
			io.put_string ("enter new phone number: ")
			io.read_integer
			c.set_phone_number(io.last_integer)
		when 3 then
			io.put_string ("enter new work place: ")
			io.read_line
			c.set_work_place(io.last_string)
		when 4 then
			io.put_string ("enter new email: ")
			io.read_line
			c.set_email (io.last_string)
		else
			io.put_string ("you entered a wrong number")
		end
		print("%N")

	end

feature

	add_emergency_contact(c: CONTACT; e: CONTACT)
	do
		c.set_call_emergency (e)
	end

feature

	remove_emergency_contact(c: CONTACT)
	local
		g: CONTACT
	do
		c.remove
	end

end
