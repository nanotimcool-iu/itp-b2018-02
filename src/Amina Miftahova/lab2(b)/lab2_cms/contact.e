class
	CONTACT

create
	make

	feature

		name: STRING
		phone_number: INTEGER
		work_place: STRING
		email: STRING
		call_emergency: detachable CONTACT

	feature

		make(n: STRING; p: INTEGER; w: STRING; e: STRING)
		do
			set_name(n)
			set_phone_number(p)
			set_work_place(w)
			set_email(e)
		end

	feature

		set_name(s: STRING)
		do
			name := s
		end

	feature

		set_phone_number(s: INTEGER)
		do
			phone_number := s
		end

	feature

		set_work_place(s: STRING)
		do
			work_place := s
		end

	feature

		set_email(s: STRING)
		do
			email := s
		end

	feature

		set_call_emergency(s: CONTACT)
		do
			call_emergency := s
		end

	feature
		remove
		do
			call_emergency := Void
		end
end
