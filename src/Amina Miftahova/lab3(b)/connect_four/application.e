note
	description: "connect_four application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	game: CONNECT_FOUR
	p1, p2: PLAYER

feature {NONE} -- Initialization

	make
	local
		finish_all: BOOLEAN
		i, row: INTEGER
	do
		finish_all := false
		io.put_string ("enter the name of the 1st player: ")
		io.read_line
		create p1.make(io.last_string.twin)
		io.put_string ("enter the name of the 2nd player: ")
		io.read_line
		create p2.make(io.last_string.twin)
		create game.make(p1, p2)
		from
			i := 1
		until
			i > 42
		loop
			if i \\ 2 = 0 then
				io.put_string (game.player2.name + ", enter the row which you want to put disc in: ")
			else
				io.put_string (game.player1.name + ", enter the row which you want to put disc in: ")
			end
			io.read_integer
			game.put_disc (io.last_integer)
			if game.finished then
				if game.winner = 1 then
					print(" game finished! player " + game.player1.name + " won")
					i := 43
				else
					print(" game finished! player " + game.player2.name + " won")
					i := 43
				end
			end
			i := i + 1
		end
	end

end
