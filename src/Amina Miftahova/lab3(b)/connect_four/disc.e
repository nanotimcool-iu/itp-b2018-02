note
	description: "Summary description for {DISC}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DISC

create
	make

feature
	state: INTEGER

feature
	make(i: INTEGER)
	do
		state := i
	end

	set_state(n: INTEGER)
	do
		state := n
	end

end
