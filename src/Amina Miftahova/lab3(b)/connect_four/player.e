note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create
	make

feature
	name: STRING

feature

	make(n: STRING)
	do
		name := n
	end

end
