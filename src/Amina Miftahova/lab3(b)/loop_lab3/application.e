note
	description: "loop_lab3 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	n, i, j, b, k, c: INTEGER
	s: ARRAYED_LIST[ARRAYED_LIST[STRING]]
	t: ARRAYED_LIST[STRING]

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			b := 0
			k := 1
			c := 1
			create t.make (0)
			io.put_string("enter a positive integer: ")
			io.read_integer
			n := io.last_integer
			create s.make(0)
			from
				i := 1
			until
				i > n
			loop
				if s.count \\ 2 = 0 then
					if s.count > 0 then
						k := k  + 1
					end
				end
				if c \\ 2 = 0 then
					t.extend (" ")
				end
				from
					j := i
				until
					j > (i + k - 1)
				loop
					t.extend (j.out)
					t.extend (" ")
					if j = n then
						j := i + k
					end
					j := j + 1
				end
				if length(t) > b then
					b := length(t)
				end
				s.extend(t)
				i := j
				create t.make (0)
				c := c + 1
			end

			from
				i := 1
			until
				i > s.count
			loop
				t := s[i]
				from
					j := 1
				until
					j > t.count
				loop
					print(t[j])
					j := j + 1
				end
				from
					j := 1
				until
					j > (b * 2 - length(t) * 2 + 1)
				loop
					print(" ")
					j := j + 1
				end
				from
					j := t.count
				until
					j < 1
				loop
					print(t[j])
					j := j - 1
				end
				print("%N")
				i := i + 1
			end
		end

feature
	length( d: ARRAYED_LIST[STRING]): INTEGER
	local
		r: INTEGER
		p: INTEGER
		z: INTEGER
	do
		r := 0
		from
			p := 1
		until
			p > d.count
		loop
			r := r + d[p].count
			p := p + 1
		end
		Result := r
	end

end
