note
	description: "calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	p1, p2, p3: PERSON
	e1, e2, e3, e4, e5: ENTRY
	t1, t2, t3, t4, t5: TIME
	calend: CALENDAR
	i: INTEGER
	s: ARRAYED_LIST[ENTRY]


feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create p1.make ("oleg", 53647, "office", "olegtheamazing@gmail.com")
			create p2.make ("igor", 55347, "bakery", "golubigor@gmail.com")
			create p3.make ("gleb", 93642, "batman_inc", "hlebusheck@gmail.com")

			create t1.make (12, 30, 00)
			create t2.make (12, 50, 00)
			create t3.make (18, 40, 00)
			create t4.make (12, 00, 00)
			create t5.make (21, 30, 00)

			create e1.make (t1, p1, "reading", "library")
			create e2.make (t2, p1, "saving the earth", "london")
			create e3.make (t3, p2, "tea party", "central park")
			create e4.make (t4, p3, "swimming", "swimming pool")
			create e5.make (t5, p3, "feed the parrot", "home")

			create calend.make (28)

			from
				i := 1
			until
				i > 28
			loop
				if i \\ 2 = 0 then
					calend.add_entry (e5, i)
				end
				if i \\ 3 = 0 then
					calend.add_entry (e1, i)
				end
				if i \\ 4 = 0 then
					calend.add_entry (e4, i)
				end
				if i \\ 5 = 0 then
					calend.add_entry (e3, i)
				end
				i := i + 1
			end

			calend.add_entry (e2, 12)

			print(calend.printable_month)
			s := calend.in_conflict (12)
			from
				i := 1
			until
				i > s.count
			loop
				print(s[i].subject + " ")
				i := i + 1
			end
			print("%N")
			print(calend.get_owner_name (e5))
		end

end
