class
	CALENDAR

create
	make

feature
	days: ARRAYED_LIST[DAY]
	i: INTEGER
	j: INTEGER

feature

	make(n: INTEGER)
	local
		d: DAY
	do
		create days.make (0)
		from
			i := 1
		until
			i > n
		loop
			create d.make(i)
			days.extend (d)
			i := i + 1
		end
	end

feature

	add_entry(e: ENTRY; day: INTEGER)
	do
		days[day].add_event(e)
	end


	edit_subject(e: ENTRY; new_subject: STRING)
	do
		e.set_subject(new_subject)
	end


	edit_date(e: ENTRY; new_date: TIME)
	do
		e.set_date(new_date)
	end


	get_owner_name(e: ENTRY): STRING
	do
		Result := e.owner.name
	end


	in_conflict(day: INTEGER): ARRAYED_LIST[ENTRY]
	local
		d: ARRAYED_LIST[ENTRY]
		c: BOOLEAN
	do
		create d.make(0)
		from
			i := 1
		until
			i > (days[day].events.count - 1)
		loop
			c := false
			from
				j := i+1
			until
				j > days[day].events.count
			loop
				if days[day].are_intersecting (days[day].events[i], days[day].events[j]) then
					if not d.has (days[day].events[j]) then
						d.extend (days[day].events[j])
						c := true
					end
				end
				j := j + 1
			end
			if c then
				if not d.has (days[day].events[i]) then
					d.extend (days[day].events[i])
				end
			end
			i := i + 1
		end
		Result := d
	end

	printable_month: STRING
	local
		s: STRING
	do
		s := ""
		from
			i := 1
		until
			i > days.count
		loop
			s := s + "the events in the day " + i.out + " are: "
			from
				j := 1
			until
				j > (days[i].events.count - 1)
			loop
				s := s + days[i].events[j].subject + " in/at/under/upon/above place " + days[i].events[j].place + " for " + days[i].events[j].owner.name + " at " + days[i].events[j].date.out + ", "
				j := j + 1
			end
			j := days[i].events.count
			if days[i].events.count > 0 then
				s := s + days[i].events[j].subject + " in/at/under/upon/above place " + days[i].events[j].place + " for " + days[i].events[j].owner.name + " at " + days[i].events[j].date.out
			end
			s := s + "%N"
			i := i + 1
		end
		Result := s
	end

end
