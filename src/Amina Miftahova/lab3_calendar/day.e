class
    DAY

create
    make

feature
   date: INTEGER
   events: ARRAYED_LIST[ENTRY]

feature

	make(d: INTEGER)
	do
		date := d
		create events.make (0)
	end

feature

    set_date(a_date: INTEGER)
    do
        date := a_date
    end


    set_events(an_events: ARRAYED_LIST[ENTRY])
    do
        events := an_events
    end


    add_event(e: ENTRY)
    require
    	event_unique: not events.has(e)
    do
    	events.extend(e)
    end

    are_intersecting(e1,e2: ENTRY):BOOLEAN
    --we assume that every event duration is 1 hour
	local
		t1: INTEGER
		t2: INTEGER
		temp: INTEGER
	do
		temp := 0
		if e1.owner = e2.owner then
			t1 := e1.date.hour * 3600 + e1.date.minute * 60 + e1.date.second
			t2 := e2.date.hour * 3600 + e2.date.minute * 60 + e2.date.second
			if t1 > t2 then
				temp := t2
				t2 := t1
				t1 := temp
			end
			if (t1 + 3600) > t2 then
				Result := true
				else
				Result := false
			end
		else
			Result := false
		end

	end


end
