class
    ENTRY

create
    make

feature
   date: TIME
   owner: PERSON
   subject: STRING
   place: STRING

feature

	make(a_date: TIME; an_owner: PERSON; a_subject: STRING; a_place: STRING)
    do
	    date := a_date
	    owner := an_owner
	    subject := a_subject
	    place := a_place
	end

feature

    set_date(a_date: TIME)
    do
        date := a_date
    end

    set_owner(an_owner: PERSON)
    do
        owner := an_owner
    end

    set_subject(a_subject: STRING)
    do
        subject := a_subject
    end

    set_place(a_place: STRING)
    do
        place := a_place
    end

end
