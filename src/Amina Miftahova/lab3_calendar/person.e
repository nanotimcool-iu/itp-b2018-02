class
    PERSON

create
    make

feature
   name: STRING
   phone_number: INTEGER
   work_place: STRING
   email: STRING

feature
	make(a_name: STRING; a_phone_number: INTEGER; a_work_place: STRING; an_email: STRING)
    do
	    name := a_name
	    phone_number := a_phone_number
	    work_place := a_work_place
	    email := an_email
	end

feature
    set_name(a_name: STRING)
    do
        name := a_name
    end
    set_phone_number(a_phone_number: INTEGER)
    do
        phone_number := a_phone_number
    end
    set_work_place(a_work_place: STRING)
    do
        work_place := a_work_place
    end
    set_email(an_email: STRING)
    do
        email := an_email
    end

end
