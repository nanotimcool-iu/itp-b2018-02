note
	description: "lab4a application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	a: AWFUL_BAG
	b: AWFUL_BAG
	f: CHARACTER
	n: INTEGER

feature {NONE} -- Initialization

	make
		do
			create a.make
			create b.make
			io.put_string ("enter the value you want to add: ")
			io.read_character
			f := io.last_character
			io.put_string ("enter the number of this value: ")
			io.read_integer
			n := io.last_integer
			a.add (f, n)
			io.put_string ("enter the value you want to add: ")
			io.read_character
			f := io.last_character
			io.put_string ("enter the number of this value: ")
			io.read_integer
			n := io.last_integer
			a.add (f, n)
			io.put_string ("enter the value you want to add: ")
			io.read_character
			f := io.last_character
			io.put_string ("enter the number of this value: ")
			io.read_integer
			n := io.last_integer
			b.add (f, n)
			io.put_string ("enter the value you want to add: ")
			io.read_character
			f := io.last_character
			io.put_string ("enter the number of this value: ")
			io.read_integer
			n := io.last_integer
			b.add (f, n)
			a.print_bag
			b.print_bag
			print(a.is_equal_bag (b))
			print("%N")
			b.remove (f, n)
			b.print_bag
			print(a.is_equal_bag (b))
			print("%N")
			print("max=")
			print(a.max)
			print("%N")
			print("min=")
			print(a.min)
		end

end
