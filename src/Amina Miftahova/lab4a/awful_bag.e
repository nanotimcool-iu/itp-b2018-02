note
	description: "Summary description for {AWFUL_BAG}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	AWFUL_BAG

create
	make

feature
	elements: ARRAYED_LIST[CELL_INFO]

feature
	make
	do
		create elements.make(0)
	end

feature

	print_bag
	local
		i: INTEGER
	do
		from
			i := 1
		until
			i > elements.count
		loop
			io.put_string("(")
			io.put_character (elements[i].value)
			io.put_string (": ")
			io.put_integer (elements[i].number_copies.code)
			io.put_string ("), ")
			i := i + 1
		end
		print("%N")
	end

	ind_element(val: CHARACTER): INTEGER
	local
		i: INTEGER
		g: INTEGER
	do
		g := 0
		from
			i := 1
		until
			i > elements.count
		loop
			if elements[i].value = val then
				g := i
			end
			i := i + 1
		end
		Result := g
	end

	add(val: CHARACTER; n: INTEGER)
	require
		not_zero_elements: n > 0
	local
		t: CELL_INFO
		ind, h: INTEGER
	do
		h := 0
		ind := ind_element(val)
		if ind = 0 then
			create t.make(val,n)
			elements.extend(t)
		else
			h := elements[ind].number_copies.code
			elements[ind].set_number_copies (h + n)
		end
	end


	remove(val: CHARACTER; n: INTEGER)
	local
		ind, t: INTEGER
	do
		ind := ind_element(val)
		if ind > 0 then
			t := elements[ind].number_copies.code
			if t > n then
				elements[ind].set_number_copies(t - n)
			else
				elements[ind].set_number_copies (0)
			end
		end
	end

	min: CHARACTER
	local
		s: CHARACTER
		i: INTEGER
	do
		s := elements[1].value
		from
			i := 2
		until
			i > elements.count
		loop
			if elements[i].value < s then
				s := elements[i].value
			end
			i := i + 1
		end
		Result := s
	end

	max: CHARACTER
	local
		s: CHARACTER
		i: INTEGER
	do
		s := elements[1].value
		from
			i := 2
		until
			i > elements.count
		loop
			if elements[i].value > s then
				s := elements[i].value
			end
			i := i + 1
		end
		Result := s
	end

	is_equal_bag(b: AWFUL_BAG): BOOLEAN
	local
		s: BOOLEAN
		i, ind: INTEGER
	do
		ind := 0
		s := true
		if elements.count = b.elements.count then
			from
				i := 1
			until
				i > elements.count
			loop
				ind := b.ind_element (elements[i].value)
				if ind = 0 then
					s := false
				else
					if b.elements[ind].number_copies /= elements[i].number_copies then
						s := false
					end
				end
				i := i + 1
			end
		else
			s := false
		end
		Result := s
	end

end
