note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	make

feature
	value: CHARACTER
	number_copies: CHARACTER

feature
	make(s: CHARACTER; n: INTEGER)
	do
		set_value(s)
		set_number_copies(n)
	ensure
		value = s
		number_copies.code = n
	end

feature
	set_value(s: CHARACTER)
	do
		value := s
	end


	set_number_copies(s: INTEGER)
	require
		number_is_valid: s > -1
	local
		f: CHARACTER
	do
		f := s.to_character_8
		number_copies := f
	end


end
