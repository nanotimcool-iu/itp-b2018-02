note
	description: "lab_4b application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	m: MATRIX_OPER
	v: VECTOR_OPER
	j, k, p: INTEGER
	a, b, i, c, d, matr: ARRAY2[INTEGER]
	veca, vecb, vecc, vecd, resvec: ARRAYED_LIST[INTEGER]

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create m.make
			create v.make
			create a.make_filled (0, 2, 2)
			create b.make_filled (0, 2, 2)
			create i.make_filled (0, 2, 2)
			create c.make_filled(0, 1, 3)
			create d.make_filled(0, 3, 1)
			c[1,1] := 2
			c[1,2] := -1
			c[1,3] := -1
			d[1,1] := -2
			d[2,1] := -1
			d[3,1] := 3
			a[1,1] := 3
			a[1,2] := 1
			a[2,1] := 5
			a[2,2] := -2
			b[1,1] := -2
			b[1,2] := 1
			b[2,1] := 3
			b[2,2] := 4
			i[1,1] := 1
			i[1,2] := 0
			i[2,1] := 1
			i[2,2] := 0
			matr := m.add (a, b)
			print("the sum is:%N")
			from
				j := 1
			until
				j > matr.height
			loop
				from
					k := 1
				until
					k > matr.width
				loop
					print(matr[j,k])
					print(" ")
					k := k + 1
				end
				print("%N")
				j := j + 1
			end
			print("determinant is:%N")
			p := m.det (a)
			print(p)
			print("%N")
			matr := m.prod (c, d)
			print("the product is:%N")
			from
				j := 1
			until
				j > matr.height
			loop
				from
					k := 1
				until
					k > matr.width
				loop
					print(matr[j,k])
					print(" ")
					k := k + 1
				end
				print("%N")
				j := j + 1
			end

			create veca.make(0)
			create vecb.make(0)
			create vecc.make(0)
			create vecd.make(0)
			veca.extend(3)
			veca.extend (-1)
			veca.extend (1)
			vecb.extend (2)
			vecb.extend (-5)
			vecb.extend (-3)
			vecc.extend (2)
			vecc.extend (4)
			vecc.extend (-1)
			vecd.extend (-2)
			vecd.extend (1)
			vecd.extend (1)
			print("cross product is:%N")
			resvec := v.cross_product (veca, vecb)
			from
				j := 1
			until
				j > resvec.count
			loop
				print(resvec[j])
				print(" ")
				j := j + 1
			end
			print("%N")
			print("dot product is:%N")
			print(v.dot_product (veca, vecb))
			print("%N")
			print("area is:%N")
			print(v.triangle_area (vecc, vecd))
			print("%N")
		end

end
