note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER

create
	make

feature
	make
	do
		print("now you can make magic with matrices")
	end

feature
	add(m1,m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	require
		equal_height: m1.height = m2.height
		equal_width: m1.width = m2.width
	local
		i,j: INTEGER
		m3: ARRAY2[INTEGER]
	do
		create m3.make_filled (0, m1.height, m1.width)
		from
			i := 1
		until
			i > m1.height
		loop
			from
				j := 1
			until
				j > m1.width
			loop
				m3[i,j] := m1[i,j] + m2[i,j]
				j := j + 1
			end
			i := i + 1
		end
		Result := m3
	end

	minus(m1,m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	require
		equal_height: m1.height = m2.height
		equal_width: m1.width = m2.width
	local
		i,j: INTEGER
		m3: ARRAY2[INTEGER]
	do
		create m3.make_filled (0, m1.height, m1.width)
		from
			i := 1
		until
			i > m1.height
		loop
			from
				j := 1
			until
				j > m1.width
			loop
				m3[i,j] := m1[i,j] - m2[i,j]
				j := j + 1
			end
			i := i + 1
		end
		Result := m3
	end

	prod(m1, m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	require
		multipliable: m1.width = m2.height
	local
		i, j, s, k: INTEGER
		m3: ARRAY2[INTEGER]
	do
		create m3.make_filled (0, m1.height, m2.width)
		from
			i := 1
		until
			i > m3.height
		loop
			from
				j := 1
			until
				j > m3.width
			loop
				s := 0
				from
					k := 1
				until
					k > m1.width
				loop
					s := s + m1[i,k] * m2[k,j]
					k := k + 1
				end
				m3[i,j] := s
				j := j + 1
			end
			i := i + 1
		end
		Result := m3
	end

	det(m: ARRAY2[INTEGER]): INTEGER   --only for square matrices with less than 4 rows
	require
		matrix_is_square: m.width = m.height
		i_can_do_it: m.width < 4
	local
		d: INTEGER
	do
		d := 0
		if m.width = 1 then
			d := m[1,1]
		elseif m.width = 2 then
			d := m[1,1] * m[2,2] - m[1,2] * m[2,1]
		else
			d := m[1,1] * (m[2,2] * m[3,3] - m[2,3] * m[3,2]) - m[1,2] * (m[2,1] * m[3,3] - m[3,1] * m[2,3]) + m[1,3] * (m[2,1] * m[3,2] - m[3,1] * m[2,2])
		end
		Result := d
	end
end
