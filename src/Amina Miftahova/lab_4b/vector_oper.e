note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER

inherit
	DOUBLE_MATH

create
	make

feature
	make
	do
		print("now you can do some stuff with vectors!%N")
	end

feature
	cross_product(v1, v2: ARRAYED_LIST[INTEGER]): ARRAYED_LIST[INTEGER] --for 3-dimensional
	require
		arrays_are_suitable: v1.count = 3 and v2.count = 3
	local
		v: ARRAYED_LIST[INTEGER]
	do
		create v.make (0)
		v.extend(v1[2] * v2[3] - v1[3] * v2[2])
		v.extend (v1[1] * v2[3] - v1[3] * v2[1])
		v.extend (v1[1] * v2[2] - v1[2] * v2[1])
		Result := v
	end

	dot_product(v1, v2: ARRAYED_LIST[INTEGER]): INTEGER
	require
		arrays_are_same_dimensional: v1.count = v2.count
	local
		i, l: INTEGER
	do
		l := 0
		from
			i := 1
		until
			i > v1.count
		loop
			l := l + v1[i] * v2[i]
			i := i + 1
		end
		Result := l
	end

	triangle_area(v1, v2: ARRAYED_LIST[INTEGER]): REAL_64
	local
		v: ARRAYED_LIST[INTEGER]
		i: INTEGER
		s: REAL_64
	do
		s := 0
		v := cross_product(v1,v2);
		from
			i := 1
		until
			i > v.count
		loop
			s := s + v[i] * v[i]
			i := i + 1
		end
		Result := sqrt (s)/2
	end
end
