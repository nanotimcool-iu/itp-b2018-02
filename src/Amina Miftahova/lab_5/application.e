note
	description: "lab_5 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	a,b,c,d,e: RANGE

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create a.make (1, 5)
			create c.make(6,3)
			create b.make (5, 8)
			create e.make(4,8)
			print("is range a empty? ")
			print(a.is_empty)
			print("%N")
			print("is range c empty? ")
			print(c.is_empty)
			print("%N")
			print("is c subrange of a? ")
			print(c.is_subrange_of (a))
			print("%N")
			print("is b superrange of a? ")
			print(b.is_superrange_of (a))
			print("%N")
			print("does a overlap b? ")
			print(a.overlaps (b))
			print("%N")
			d := a.add(b)
			print("range a + b is from ")
			print(d.left.out + " to " + d.right.out + "%N")
			d := d.subtract(e)
			print("range d - e is from ")
			print(d.left.out + " to " + d.right.out + "%N")
		end

end
