note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	make

feature
	left, right: INTEGER

feature
	make(l,r: INTEGER)
	do
		left := l
		right := r
	end

feature
	is_equal_range(other: like Current): BOOLEAN
	do
		if(Current.left = other.left) and (Current.right = other.right) then
			Result := true
		else
			Result := false
		end
	end

	is_empty: BOOLEAN
	do
		if Current.left > Current.right then
			Result := true
		else
			Result := false
		end
	end

	is_subrange_of(other: like Current): BOOLEAN
	do
		if Current.is_empty then
			Result := true
		elseif (Current.left >= other.left) and (Current.right <= other.right) then
			Result := true
		else
			Result := false
		end
	end

	is_superrange_of(other: like Current): BOOLEAN
	do
		Result := other.is_subrange_of(Current)
	end

	left_overlaps(other: like Current): BOOLEAN
	require
		not_empty: not Current.is_empty and not other.is_empty
	do
		if Current.left >= other.left then
			Result := true
		else
			Result := false
		end
	end

	right_overlaps(other: like Current): BOOLEAN
	require
		not_empty: not Current.is_empty and not other.is_empty
	do
		if Current.right <= other.right then
			Result := true
		else
			Result := false
		end
	end

	overlaps(other: like Current): BOOLEAN
	do
		if Current.is_empty or other.is_empty then
			Result := false
		elseif Current.left_overlaps(other) or Current.right_overlaps(other) or other.right_overlaps(Current) or other.left_overlaps(Current) then
			Result := true
		else
			Result := false
		end
	end

	add(other: like Current): RANGE
	require
		addable: Current.right >= other.left or Current.left <= other.right
	local
		r: RANGE
		m,n: INTEGER
	do
		if(Current.left < other.left) then
			m := Current.left
		else
			m := other.left
		end
		if(Current.right > other.right) then
			n := Current.right
		else
			n := other.right
		end
		create r.make(m,n)
		Result := r
	end

	can_subtract(other: like Current): BOOLEAN
	do
		if Current.left = other.left then
			if Current.right >= other.right then
				Result := true
			else
				Result := false
			end
		elseif Current.right = other.right then
			if Current.left <= other.left then
				Result := true
			else
				Result := false
			end
		else
			Result := false
		end
	end

	subtract(other: like Current): RANGE
	require
		one: Current.can_subtract(other)
	local
		r: RANGE
		m,n: INTEGER
	do
		if Current.left = other.left then
			m := other.right + 1
			n := Current.right
		else
			m := Current.left
			n := other.left - 1
		end
		create r.make(m,n)
		Result := r
	end

end
