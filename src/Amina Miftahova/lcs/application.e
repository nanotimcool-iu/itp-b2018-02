note
	description: "lcd application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	o,p: STRING

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			o := "abcdef"
			p := ""
			print(lcs(o,p) + "%N")
			p := "abctg"
			print(lcs(o,p) + "%N")
			p := "hrktl"
			print(lcs(o,p) + "%N")
			p := "jtabcdethyf"
			print(lcs(o,p) + "%N")
		end

feature
	lcs(x,y: STRING): STRING
	local
		r, a, b: STRING
	do
		r := ""
		if x.count /= 0 and y.count /= 0 then
			if x.substring (x.count, x.count) ~ y.substring (y.count, y.count) then
				r := lcs(x.substring (1, x.count - 1), y.substring (1, y.count - 1)) + x[x.count].out
			else
				a := lcs(x, y.substring (1, y.count - 1))
				b := lcs(x.substring (1, x.count - 1), y)
				if a.count > b.count then
					r := a
				else
					r := b
				end
			end
		end
		Result := r
	end

end
