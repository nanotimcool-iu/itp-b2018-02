note
	description: "library application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	l: LIBRARIAN

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create l.make
			l.add_book
			l.add_book
			l.add_user
			l.add_user
			l.give_book (l.users[1], l.books[1])
			l.users[1].show_books
			l.give_book (l.users[1], l.books[2])
			l.users[1].show_books
			l.return_book (l.users[1], l.users[1].books[1])
			l.users[1].show_books
		end

end
