note
	description: "Summary description for {BOOK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOOK

create
	make

feature
	title: STRING
	is_available, child: BOOLEAN
	price: INTEGER
	bestseller: BOOLEAN
	due_to: DATE

feature {LIBRARIAN}
	make(t: STRING; p: INTEGER; b, c: BOOLEAN)
	require
		valid_title: not (t ~ "")
		valid_price: p >= 0
	do
		title := t
		is_available := true
		price := p
		bestseller := b
		child := c
		create due_to.make_now
	end

feature {BOOK, LIBRARIAN}

	make_bestseller
	do
		bestseller := true
	end

	not_bestseller
	do
		bestseller := false
	end

	make_unavailable
	do
		is_available := false
	end

	make_available
	do
		is_available := true
	end

	set_date(d: DATE)
	do
		due_to := d
	end

end
