note
	description: "Summary description for {LIBRARIAN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LIBRARIAN

create
	make

feature
	books: ARRAYED_LIST[BOOK]
	users: ARRAYED_LIST[USER_CARD]

feature {APPLICATION, LIBRARIAN}
	make
	do
		create books.make (0)
		create users.make (0)
	end

	add_book
	local
		title: STRING
		cost: INTEGER
		bestseller, for_children: BOOLEAN
		b: BOOK
	do
		io.put_string ("enter the title of a book: ")
		io.read_line
		title := io.last_string.twin
		io.put_string ("enter the price of a book: ")
		io.read_integer
		cost := io.last_integer.twin
		io.put_string ("enter 1 if a book is a best seller, else enter 0: ")
		io.read_integer
		if io.last_integer = 1 then
			bestseller := true
		else
			bestseller := false
		end
		io.put_string ("enter 1 if a book is suitable for children, else enter 0: ")
		io.read_integer
		if io.last_integer = 1 then
			for_children := true
		else
			for_children := false
		end
		create b.make (title, cost, bestseller, for_children)
		books.extend (b)
	end

	add_user
	local
		n, s, ph, ad: STRING
		age, id: INTEGER
		u: USER_CARD
	do
		io.put_string ("enter name: ")
		io.read_line
		n := io.last_string.twin
		io.put_string ("enter surname: ")
		io.read_line
		s := io.last_string.twin
		io.put_string ("enter phone number: ")
		io.read_line
		ph := io.last_string.twin
		io.put_string ("enter address: ")
		io.read_line
		ad := io.last_string.twin
		io.put_string ("enter age: ")
		io.read_integer
		age := io.last_integer.twin
		id := users.count + 1
		create u.make (n, s, ph, ad, age, id)
		users.extend (u)
	end

	give_book(u: USER_CARD; b: BOOK)
	local
		d: DATE
	do
		create d.make_now
		if b.is_available then
			if b.bestseller then
				if d.day + 14 <= d.days_in_i_th_month (d.month, d.year) then
					d.set_day (d.day + 14)
				else
					d.set_day (d.day + 14 - d.days_in_i_th_month (d.month, d.year))
					if d.month = 12 then
						d.set_month (1)
						d.set_year (d.year + 1)
					else
						d.set_month (d.month + 1)
					end
				end
			else
				if d.day + 21 <= d.days_in_i_th_month (d.month, d.year) then
					d.set_day (d.day + 21)
				else
					d.set_day (d.day + 21 - d.days_in_i_th_month (d.month, d.year))
					if d.month = 12 then
						d.set_month (1)
						d.set_year (d.year + 1)
					else
						d.set_month (d.month + 1)
					end
				end
			end
			b.set_date (d)
			u.add_book (b)
			b.make_unavailable
		end
	end

	return_book(u: USER_CARD; b: BOOK)
	local
		d: DATE
	do
		create d.make_now
		u.delete_book (b)
		b.make_available
		if b.due_to.is_less_equal (d) then
			fine(b)
		end
		print("thank you for returning the book!%N")
		b.set_date (d)
	end

	fine(b: BOOK)  -- i assume that every month has 30 days
	local
		d, due: DATE
		k: INTEGER
	do
		create d.make_now
		due := b.due_to
		if due.year = d.year then
			if due.month = d.month then
				k := d.day - due.day
			else
				k := (d.month - due.month - 1) * 30 + d.day + 30 - due.day
			end
		else
			k := b.price
		end
		k := k * 100
		if k > b.price then
			k := b.price
		end
		print("sorry, you should pay " + k.out + " roubles as an overdue fine%N")
	end

end
