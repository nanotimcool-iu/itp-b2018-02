note
	description: "Summary description for {USER_CARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	USER_CARD

create
	make

feature
	name, surname, phone, address: STRING
	age, card_num: INTEGER
	books: ARRAYED_LIST[BOOK]

feature {LIBRARIAN, USER_CARD, APPLICATION}
	show_books
	local
		i: INTEGER
	do
		from
			i := 1
		until
			i > books.count
		loop
			print(i.out + ": " + books[i].title + " ")
			i := i + 1
		end
		print ("%N")
	end

feature {LIBRARIAN}
	make(n, s, p, a: STRING; ag,c: INTEGER)
	require
		valid_name: not (n ~ "")
		valid_surname: not (s ~ "")
		valid_phone_num: not (p ~ "")
		valid_address: not (a ~ "")
		valid_age: ag > 0
	do
		name := n
		surname := s
		phone := p
		address := a
		age := ag
		card_num := c
		create books.make (0)
	end

	add_book(b: BOOK)
	require
		book_exists: b /= Void
		book_is_available: b.is_available
	do
		if age < 13 then
			if books.count = 5 then
				print(name + " " + surname + " already had checked out max number of books, so sorry, but you cannot take one more book%N")
			else
				if b.child then
					books.extend (b)
					print("the book '" + b.title + "' was succesfully checked out! you should return it until " + b.due_to.out + "%N")
				else
					print("the book '" + b.title + "' is not available for children%N")
				end
			end
		else
			books.extend (b)
			print("the book '" + b.title + "' was succesfully checked out! you should return it until " + b.due_to.out + "%N")
		end
	end

	delete_book(b: BOOK)
	require
		book_exists: b /= Void
		book_is_taken_by_him: books.has (b)
	local
		i: INTEGER
		a: ARRAYED_LIST[BOOK]
	do
		create a.make (0)
		from
			i := 1
		until
			i > books.count
		loop
			if books[i] /= b then
				a.extend (b)
			end
			i := i + 1
		end
		books := a
	ensure
		books.count = old books.count - 1
	end

end
