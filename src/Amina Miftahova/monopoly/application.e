note
	description: "monopoly application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	g: GAME

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create g.make
			g.play
		end

end
