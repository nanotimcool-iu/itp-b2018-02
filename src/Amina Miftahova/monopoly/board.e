note
	description: "Summary description for {BOARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOARD

create
	make

feature
	cells: ARRAYED_LIST[BOARD_CELL]

feature
	make
	local
		b: BOARD_CELL
		p: PROPERTY
	do
		create cells.make(0)
		create b.make ("GO", 1)
		cells.extend (b)
		create b.make ("Christ", 2)
		create p.make ("Christ the Redeemer", 60000, 2000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("Luang Pho To", 3)
		create p.make ("Luang Pho To", 60000, 4000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("INCOME TAX", 4)
		cells.extend (b)
		create b.make ("Alyosha", 5)
		create p.make ("Alyosha monument", 80000, 4000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("IN JAIL", 6)
		cells.extend (b)
		create b.make ("Tokyo Wan Kannon", 7)
		create p.make ("Tokyo Wan Kannon", 100000, 6000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("Luangpho Yai", 8)
		create p.make ("Luangpho Yai", 120000, 8000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("CHANCE", 9)
		cells.extend (b)
		create b.make ("The Motherland", 10)
		create p.make ("The Motherland", 160000, 12000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("FREE PARKING", 11)
		cells.extend (b)
		create b.make ("Awaji Kannon", 12)
		create p.make ("Awaji Kannon", 220000, 18000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("CHANCE", 13)
		cells.extend (b)
		create b.make ("Rodina-Mat' Zovyot!", 14)
		create p.make ("Rodina-Mat' Zovyot!", 260000, 22000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("Great Buddha of Thailand", 15)
		create p.make ("Great Buddha of Thailand", 280000, 22000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("GO TO JAIL", 16)
		cells.extend (b)
		create b.make ("Laykyun Setkyar", 17)
		create p.make ("Laykyun Setkyar", 320000, 28000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("Spring Temple Buddha", 18)
		create p.make ("Spring Temple Buddha", 360000, 35000)
		b.set_prop (p)
		cells.extend (b)
		create b.make ("CHANCE", 19)
		cells.extend (b)
		create b.make ("Statue of Unity", 20)
		create p.make ("Statue of Unity", 400000, 50000)
		b.set_prop (p)
		cells.extend (b)
	end

end
