note
	description: "Summary description for {BOARD_CELL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOARD_CELL

create
	make

feature
	num: INTEGER
	name: STRING
	prop: detachable PROPERTY
	owner: detachable PLAYER

feature
	make(n: STRING; m: INTEGER)
	do
		name := n
		num := m
	end

	set_prop(p: PROPERTY)
	require
		valid_property: p /= Void
	do
		prop := p
	end

	set_owner(o: PLAYER)
	require
		valid_owner: o /= Void
	do
		owner := o
	end

end
