note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create make

feature
	board: BOARD
	players: ARRAYED_LIST[PLAYER]
	rounds: INTEGER
	rand: RANDOM

feature
	make
	local
		i, n: INTEGER
		p: PLAYER
	do
		create rand.set_seed(2)
		create board.make
		create players.make (0)
		io.put_string ("enter the number of players: ")
		io.read_integer
		n := io.last_integer
		from
			i := 1
		until
			i > n
		loop
			create p.make(board.cells[1])
			players.extend (p)
			i := i + 1
		end
		rounds := 0
	end

feature

	print_balance(p: PLAYER)
	do
		print("your current balance is " + p.money.out + "%N")
	end

	chance: INTEGER
	local
		n: INTEGER
	do
		rand.forth
		n := rand.item \\ 2
		if n = 1 then
			Result := (rand.item \\ 200000) // 10
		else
			Result := -1 * (rand.item \\ 300000) // 10
		end
	end

	roll_the_bones: INTEGER
	do
		rand.forth
		Result := rand.item \\ 6 + 1
	end

	delete(p: PLAYER)
	require
		exists: players.has (p)
	local
		i: INTEGER
		t: ARRAYED_LIST[PLAYER]
	do
		print(p.name + " is out of the game!%N")
		create t.make (0)
		from
			i := 1
		until
			i > players.count
		loop
			if players[i] /= p then
				t.extend (players[i])
			end
			i := i + 1
		end
		p.away
		players := t
	ensure
		players.count = old players.count - 1
	end

	winner
	local
		i, m: INTEGER
	do
		print("Winners: ")
		m := players[1].money
		from
			i := 2
		until
			i > players.count
		loop
			if players[i].money > m then
				m := players[i].money
			end
			i := i + 1
		end
		from
			i := 1
		until
			i > players.count
		loop
			if players[i].money = m then
				print(players[i].name + " ")
			end
			i := i + 1
		end
		print("%N")
	end

	play
	local
		i, m, n, forw, last: INTEGER
		pl: PLAYER
	do
		from
			rounds := 0
		until
			rounds > 100
		loop
			from
				i := 1
			until
				i > players.count
			loop
				pl := players[i]
				print_balance(players[i])
				print("it's " + players[i].name + "'s turn%N")
				if players[i].in_jail then
					last := players[i].cur_cell.num
					m := roll_the_bones
					n := roll_the_bones
					forw := m + n
					print("you have rolled " + m.out + " and " + n.out + "%N")
					if m = n then
						print("you can go out of jail%N")
						players[i].unjailed
					else
						io.put_string ("enter 1 if you want to pay 50K and get out of jail else enter 0: %N")
						io.read_integer
						if io.last_integer = 1 then
							players[i].set_money (players[i].money - 50000)
							players[i].unjailed
						end
					end
				else
					last := players[i].cur_cell.num
					m := roll_the_bones
					n := roll_the_bones
					print("you have rolled " + m.out + " and " + n.out + "%N")
					forw := m + n
				end
				if players[i].money < 0 then
					delete(players[i])
				end
				if (not players[i].in_jail) and players[i].in_game and players[i] = pl then
					if forw + players[i].cur_cell.num > 20 then
						players[i].set_cell (board.cells[forw + players[i].cur_cell.num - 20])
					else
						players[i].set_cell (board.cells[forw + players[i].cur_cell.num])
					end
					print("you moved to the " + players[i].cur_cell.name + " cell%N")
					if players[i].cur_cell.num < last  then
						print("as you passed the GO square you get 200K bonus!%N")
						players[i].set_money (players[i].money + 200000)
					end
					if attached players[i].cur_cell.prop as p then
						if attached players[i].cur_cell.owner as owner then
							if owner.in_game then
								owner.set_money (owner.money + p.rent)
								players[i].set_money(players[i].money - p.rent)
							else
								io.put_string("if you want to buy " + p.name + " for " + p.price.out + " roubles, enter 1. else enter 0: ")
								io.read_integer
								if io.last_integer.twin = 1 then
									players[i].set_money(players[i].money - p.price)
								end
							end
						else
							io.put_string("if you want to buy " + p.name + " for " + p.price.out + " roubles, enter 1. else enter 0: ")
							io.read_integer
							if io.last_integer.twin = 1 then
								players[i].set_money(players[i].money - p.price)
							end
						end
					end
					if players[i].cur_cell.name ~ "CHANCE" then
						m := chance
						print("as you are on a CHANCE square you get " + m.out + "%N")
						players[i].set_money (players[i].money + m)
					end
					if players[i].cur_cell.num = 4 then
						print("you are on an INCOME TAX square and should pay tax%N")
						players[i].set_money (players[i].money - players[i].money // 10)
					end
					if players[i].cur_cell.num = 16 then
						players[i].set_cell (board.cells[6])
						print("you have gone to jail%N")
						players[i].jailed
					end
					if players[i].money < 0 then
						delete(players[i])
					end
				end
				if players.count = 1 then
					i := players.count + 10
				end
				i := i + 1
			end
			if players.count = 1 then
				rounds := 102
			end
			rounds := rounds + 1
		end
		winner
	end

end
