note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create
	make

feature
	name: STRING
	money: INTEGER
	prop: ARRAYED_LIST[PROPERTY]
	in_jail, in_game: BOOLEAN
	cur_cell: BOARD_CELL

feature
	make(b: BOARD_CELL)
	do
		print("enter the player name: ")
		io.read_line
		name := io.last_string.twin
		money := 150000
		create prop.make(0)
		set_cell(b)
		in_jail := false
		in_game := true
	end

	away
	do
		in_game := false
	end

	jailed
	do
		in_jail := true
	end

	unjailed
	do
		in_jail := false
	end

	set_money(n: INTEGER)
	do
		money := n
	end

	set_cell(c: BOARD_CELL)
	do
		cur_cell := c
	end

end
