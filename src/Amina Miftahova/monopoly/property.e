note
	description: "Summary description for {PROPERTY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROPERTY

create
	make

feature
	name: STRING
	price: INTEGER
	rent: INTEGER

feature
	make(n: STRING; p, r: INTEGER)
	do
		name := n
		price := p
		rent := r
	end

end
