note
	description: "power_set application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	p, h: ARRAYED_LIST[STRING]
	j: INTEGER

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create p.make(0)
			p.extend("2")
			p.extend("3")
			p.extend("1")
			h := power_set(p)
			from
				j := 1
			until
				j > h.count
			loop
				print("{"+h[j] + "} ")
				j := j + 1
			end
		end

feature

	power_set(s: ARRAYED_LIST[STRING]): ARRAYED_LIST[STRING]
	local
		r, k: ARRAYED_LIST[STRING]
		i: INTEGER
	do
		create r.make (0)
		create k.make (0)
		if s.count = 1 then
			r.extend ("")
			r.extend (s[1])
		else
			from
				i := 2
			until
				i > s.count
			loop
				k.extend (s[i])
				i := i + 1
			end
			r := ugh(s[1], power_set(k))
		end
		Result := r
	end

	ugh(a: STRING; b: ARRAYED_LIST[STRING]): ARRAYED_LIST[STRING]
	local
		r: ARRAYED_LIST[STRING]
		i: INTEGER
	do
		create r.make (0)
		from
			i := 1
		until
			i > b.count
		loop
			r.extend (b[i])
			if b[i] ~ "" then
				r.extend (a)
			else
				r.extend (b[i] + ", " + a)
			end
			i := i + 1
		end
		Result := r
	end

end
