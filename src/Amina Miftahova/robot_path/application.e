note
	description: "robot_path application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature

	field: ARRAY2[BOOLEAN]
	i, j: INTEGER
	k: BOOLEAN

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			field := set_field
			from
				i := 1
			until
				i > field.height
			loop
				from
					j := 1
				until
					j > field.width
				loop
					print(field[i,j].out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
			k := find_path(field,1,1)
			if not k then
				print("there is no path!")
			end
		end

feature

	set_field: ARRAY2[BOOLEAN]
	local
		t: ARRAY2[BOOLEAN]
		n, m, v, u: INTEGER
	do
		io.put_string ("enter the number of rows: ")
		io.read_integer
		n := io.last_integer
		io.put_string ("enter the number of columns: ")
		io.read_integer
		m := io.last_integer
		if n < 1 or m < 1 then
			print("invalid parameters, field cannot be created!%N")
			create t.make_filled (true, 1, 1)
		else
			create t.make_filled (true, n, m)
			io.put_string ("enter the number of obstacles you want to put: ")
			io.read_integer
			v := io.last_integer
			from
				u := 1
			until
				u > v
			loop
				io.put_string ("enter the row of an obstacle: ")
				io.read_integer
				n := io.last_integer
				io.put_string ("enter the column of an obstacle: ")
				io.read_integer
				m := io.last_integer
				t := put_obstacle(t, n, m)
				u := u + 1
			end
		end
		Result := t
	end

	put_obstacle(f: ARRAY2[BOOLEAN]; n,m: INTEGER): ARRAY2[BOOLEAN]
	require
		not_first_cell: n /= 1 or m /= 1
		not_last_cell: n /= f.height or m /= f.width
	do
		f[n, m] := false
		Result := f
	end

	find_path(f: ARRAY2[BOOLEAN]; n,m: INTEGER): BOOLEAN
	require
		valid_cell: n <= f.height and m <= f.width and n > 0 and m > 0
		not_obstacle: f[n, m] /= false
	local
		p: BOOLEAN
	do
		p := false
		if n = f.height and m = f.width then
			p := true
		else
			if n < f.height and f[n + 1, m] then
				p := find_path(f, n + 1, m)
				if p then
					print("down ")
				end
			end
			if m < f.width and not p and f[n, m + 1] then
				p := find_path(f, n, m + 1)
				if p then
					print("right ")
				end
			end
		end
		Result := p
	end

end
