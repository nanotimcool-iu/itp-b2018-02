note
	description: "huffman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	h: HUFFMAN

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create h.make ("aardvark_is_sitting_on_a_tree")
		end

end
