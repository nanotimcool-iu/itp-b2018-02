note
	description: "Summary description for {HUFFMAN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN

create make

feature

	word: STRING
	used_characters: ARRAYED_LIST[STRING]
	frequency: ARRAYED_LIST[INTEGER]
	huf_tree: WEIGHT_TREE

feature
	make(s: STRING)
	local
		i: INTEGER
	do
		word := s
		create used_characters.make(0)
		create frequency.make(0)
		fill_frequency
		sort
		create huf_tree.make
		create_tree
		huf_tree.build_tree
		huf_tree.traverse(huf_tree.tree[1], "")
	end

	create_tree
	local
		i: INTEGER
		b: BINARY_TREE[STRING]
	do
		create b.make ("")
		from
			i := 1
		until
			i > frequency.count
		loop
			create b.make (used_characters[i])
			huf_tree.tree.extend (b)
			huf_tree.weight.extend (frequency[i])
			i := i + 1
		end
	end

	sort  --sorts characters by their appearance frequency
	local
		i, j, t: INTEGER
		k: STRING
	do
		from
			i := 1
		until
			i > frequency.count
		loop
			from
				j := frequency.count
			until
				j <= i
			loop
				if frequency[j] < frequency[j - 1] then
					t := frequency[j]
					frequency[j] := frequency[j-1]
					frequency[j-1] := t
					k := used_characters[j]
					used_characters[j] := used_characters[j-1]
					used_characters[j-1] := k
				end
				j := j - 1
			end
			i := i + 1
		end
	end

	print_f   --just a function for printing frequency because i wanted it
	local
		i: INTEGER
	do
		from
			i := 1
		until
			i > frequency.count
		loop
			print(used_characters[i] + ": " + frequency[i].out + "%N")
			i := i + 1
		end
	end

	fill_frequency   --finds frequency of each letter
	local
		i, k: INTEGER
		h: BOOLEAN
	do
		from
			i := 1
		until
			i > word.count
		loop
			h := false
			from
				k := 1
			until
				k > used_characters.count
			loop
				if used_characters[k] ~ word[i].out then
					frequency[k] := frequency[k] + 1
					k := used_characters.count
					h := true
				end
				k := k + 1
			end
			if not h then
				used_characters.extend (word[i].out)
				frequency.extend (1)
			end
			i := i + 1
		end
	end

end
