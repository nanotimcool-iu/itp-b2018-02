note
	description: "Summary description for {WEIGHT_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	WEIGHT_TREE

create make

feature

	weight: ARRAYED_LIST[INTEGER]
	tree: ARRAYED_LIST[BINARY_TREE[STRING]]

feature
	make
	do
		create weight.make(0)
		create tree.make(0)
	end

feature
	build_tree
	local
		b: BINARY_TREE[STRING]
		i, k, j: INTEGER
		w: ARRAYED_LIST[INTEGER]
		t: ARRAYED_LIST[BINARY_TREE[STRING]]
	do
		create b.make ("")
		create w.make (0)
		create t.make (0)
		from
			i := 1
		until
			i < 0
		loop
			if tree.count > 1 then
				k := weight[1] + weight[2]
				create b.make (tree[1].item + tree[2].item)
				b.put_left_child (tree[1])
				b.put_right_child (tree[2])
				w := weight
				t := tree
				create weight.make (0)
				create tree.make (0)
				from
					j := 3
				until
					j > t.count
				loop
					weight.extend (w[j])
					tree.extend (t[j])
					j := j + 1
				end
				insert(b,k)
			else
				i := -2
			end
			i := i + 1
		end
	end

	traverse(b: detachable BINARY_TREE[STRING]; s: STRING)
	local
		d: BINARY_TREE[STRING]
	do
		create d.make ("")
		if b.item.count = 1 then
			print(b.item + ": " + s + "%N")
		else
			if attached b.left_child as l then
				d := l
				traverse(d, (s + "0"))
			end
			if attached b.right_child as r then
				d := r
				traverse(d, (s + "1"))
			end
		end
	end

	insert(b: BINARY_TREE[STRING]; n: INTEGER)  --inserting a node
	local
		i, k, t1, t3: INTEGER
		t2, t4: BINARY_TREE[STRING]
	do
		create t2.make ("")
		create t4.make ("")
		k := weight.count + 1
		from
			i := 1
		until
			i > weight.count
		loop
			if weight[i] > n then
				k := i
				i := weight.count
			end
			i := i + 1
		end
		if k = (weight.count + 1) then
			weight.extend (n)
			tree.extend (b)
		else
			t1 := weight[k]
			t2 := tree[k]
			weight[k] := n
			tree[k] := b
			from
				i := k + 1
			until
				i > weight.count
			loop
				t3 := weight[i]
				t4 := tree[i]
				weight[i] := t1
				tree[i] := t2
				t1 := t3
				t2 := t4
				i := i + 1
			end
			weight.extend (t1)
			tree.extend (t2)
		end
	end

end
