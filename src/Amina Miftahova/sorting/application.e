note
	description: "sorting application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature

	f,res: ARRAYED_LIST[INTEGER]

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create f.make(0)
			f.extend (3)
			f.extend (6)
			f.extend (1)
			f.extend (8)
			f.extend (2)
			f.extend (4)
			f.extend (5)
			res := sort(f)
			mgm(res)
		end

feature

	mgm(a: ARRAYED_LIST[INTEGER])
	local
		i: INTEGER
	do
		from
			i := 1
		until
			i > a.count
		loop
			print(a[i].out + " ")
			i := i + 1
		end
		print("%N")
	end


	sort(a: ARRAYED_LIST[INTEGER]): ARRAYED_LIST[INTEGER]
	local
		c,l,r: ARRAYED_LIST[INTEGER]
		i: INTEGER
	do
		create l.make (0)
		create r.make (0)
		if a.count = 1 then
			c := a
		else
			from
				i := 1
			until
				i > (a.count + 1) // 2
			loop
				l.extend (a[i])
				i := i + 1
			end
			from
				i := i
			until
				i > a.count
			loop
				r.extend (a[i])
				i := i + 1
			end
			c := merge(sort(l),sort(r))
		end
		Result := c
	end

	merge(a,b: ARRAYED_LIST[INTEGER]): ARRAYED_LIST[INTEGER]
	local
		c: ARRAYED_LIST[INTEGER]
		i, j: INTEGER
	do
		create c.make (0)
		from
			i := 1; j := 1
		until
			i > a.count or j > b.count
		loop
			if a[i] <= b[j] then
				c.extend (a[i])
				i := i + 1
			else
				c.extend (b[j])
				j := j + 1
			end
		end
		from
			i := i
		until
			i > a.count
		loop
			c.extend (a[i])
				i := i + 1
		end
		from
			j := j
		until
			j > b.count
		loop
			c.extend (b[j])
				j := j + 1
		end
		Result := c
	end

end
