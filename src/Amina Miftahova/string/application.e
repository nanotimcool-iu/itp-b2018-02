note
	description: "string_reverse application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	s: STRING

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			s := "abcdef"
			print(iterate_reverse(s) + "%N")
			print(recursion_reverse(s))
		end

feature

	iterate_reverse(d: STRING): STRING
	local
		r: STRING
		i: INTEGER
	do
		r := ""
		from
			i := 0
		until
			i >= d.count
		loop
			r := r + d.substring (d.count - i, d.count - i)
			i := i + 1
		end
		Result := r
	end

	recursion_reverse(d: STRING): STRING
	local
		r: STRING
	do
		r := ""
		if d.count = 1 then
			r := d
		else
			r := recursion_reverse(d.substring (2, d.count)) + d.substring (1, 1)
		end
		Result := r
	end

end
