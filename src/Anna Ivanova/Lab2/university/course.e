note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE
create
	create_class
feature
	name: STRING
	identifier: INTEGER
	max_st: INTEGER
	schedule: STRING
	create_class(name_: STRING id: INTEGER max: INTEGER schedule_: STRING)
		require
			min_st: max > 3
		do
			name := name_
			identifier := id
			max_st := max
			schedule := schedule_
		end

end
