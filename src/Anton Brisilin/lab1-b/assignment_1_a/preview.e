note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		local
			i : INTEGER
			station_name : STRING
		do
			station_name:="Zoo"
			Zurich.add_station (station_name, 1000, 400)
			zurich_map.update
			zurich.connect_station (10, station_name)
			zurich.connect_station (5, station_name)
			zurich_map.update
			zurich_map.station_view (zurich.station (station_name)).highlight

			from
				i:=0
			until
				i>=100
			loop
				wait (2)
			zurich_map.station_view (zurich.station (station_name)).unhighlight
			wait (2)
			zurich_map.station_view (zurich.station (station_name)).highlight

			end

			zurich_map.update
			zurich_map.animate
			zurich_map.fit_to_window
		end

end
