class
	BUSINESS_CARD

create
	fill_in

feature {NONE} -- Initialization

	fill_in
			-- Fill in the card and print it.
		local
		do
			io.new_line
			io.put_string ("Please enter your name: ")
			io.read_line()
			set_name (io.last_string)
			io.put_string ("Please enter your job: ")
			io.read_line()
			set_job (io.last_string)
			io.put_string ("Please enter your age: ")
			io.read_integer()
			set_age (io.last_integer)
			print_card()
			-- Add your code here.
		end

feature -- Access

	name: STRING
			-- Owner's name.

	job: STRING
			-- Owner's job.

	age: INTEGER
			-- Owner's age.

feature -- Setting

	set_name (a_name: STRING)
			-- Set `name' to `a_name'.
		require
			name_exists: a_name /= Void
		do
			name := a_name.twin
		end

	set_job (a_job: STRING)
			-- Set `job' to `a_job'.
		require
			job_exists: a_job /= Void
		do
			job := a_job.twin
		end

	set_age (a_age: INTEGER)
			-- Set `age' to `a_age'.
		require
			age_non_negative: a_age >= 0
		do
			age := a_age
		end

feature -- Output

	print_card
	local
		s1,s2,s3:STRING
		do
			io.put_string (line (width))
			Io.new_line
			s1:="|Your name: " + name
			Io.put_string (s1  + spaces (width-s1.count-1)+"|%N")
			s2:= "|Your age: " + age_info()
			Io.put_string ( s2+spaces (width-s2.count-1) + "|")
			io.new_line
			s3:="|Your job: " + job
			Io.put_string ( s3+spaces (width-s3.count-1)+ "|%N")
			io.put_string (line (width))
		end

	age_info: STRING
			-- Text representation of age on the card.
		do
			Result := age.out + " years old"
		end

	Width: INTEGER = 50
			-- Width of the card (in characters), excluding borders.

	line (n: INTEGER): STRING
			-- Horizontal line on length `n'.
		do
			Result := "-"
			Result.multiply (n)
		end
	spaces(n:integer):STRING
		--spaces with length n
		do
			Result:=" "
			Result.multiply(n)
		end
end
