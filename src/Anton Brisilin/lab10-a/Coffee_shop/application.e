note
	description: "Coffee_shop application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			local
			coffee_shop:COFFEE_SHOP
			-- Run application.
		do
				--| Add your code here
			create coffee_shop.make
			coffee_shop.print_menu
		end

end
