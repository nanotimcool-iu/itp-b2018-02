note
	description: "Summary description for {CAKE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAKE

inherit

	PRODUCT
		redefine
			set_description
		end

create
	cook

feature

	set_description
		do
			description := "Tasty cake"
		end

end
