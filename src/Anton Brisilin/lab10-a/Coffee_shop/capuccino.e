note
	description: "Summary description for {CAPUCCINO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAPUCCINO

inherit

	COFFEE
		redefine
			set_description
		end

create
	cook

feature

	set_description
		do
			description := "Smelly capuccino"
		end

end
