note
	description: "Summary description for {COFFEE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COFFEE

inherit

	PRODUCT
		redefine
			set_description
		end

create
	cook

feature

	set_description
		do
			description := "Some coffee"
		end

end
