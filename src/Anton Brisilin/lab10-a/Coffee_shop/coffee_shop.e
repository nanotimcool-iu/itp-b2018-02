note
	description: "Summary description for {COFFEE_SHOP}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COFFEE_SHOP
create make
feature
	menu:LINKED_SET[PRODUCT]
feature
	make
	local
		cake:CAKE
		espresso:ESPRESSO
		capuccino:CAPUCCINO
	do
		create cake.cook (12, 25)
		create espresso.cook (20, 50)
		create capuccino.cook (20, 50)
		create menu.make
		menu.extend (cake)
		menu.extend (espresso)
		menu.extend (capuccino)
	end
	print_menu
	do
		across menu as product
		loop
			print( product.item.description + " for $" + product.item.price_public.out + "%N")
		end
	end
end
