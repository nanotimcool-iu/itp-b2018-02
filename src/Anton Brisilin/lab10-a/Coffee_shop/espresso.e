note
	description: "Summary description for {ESPRESSO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ESPRESSO

inherit

	COFFEE
		redefine
			set_description
		end

create
	cook

feature

	set_description
		do
			description := "Good espresso"
		end

end
