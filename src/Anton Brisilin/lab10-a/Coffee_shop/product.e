note
	description: "Summary description for {PRODUCT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PRODUCT
create
	cook
feature{COFFEE_SHOP}
	price:REAL assign set_price
	set_price(prod_price:REAL)
	do
		price:= prod_price
	end
	set_price_public(pub_price:REAL)
	do
		price_public:=pub_price
	end
	profit:REAL
	do
		Result:=price_public-price
	end
	cook(prod_price:REAL;pub_price:REAL)
	do
		set_description
		price:=prod_price
		price_public:=pub_price
	end
feature
	price_public:REAL assign set_price_public
	description:STRING

feature {NONE}
set_description
do
	description:="Prod_description"
end

invariant price < price_public
end
