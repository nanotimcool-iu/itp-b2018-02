note
	description: "monopoly application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			monopoly: GAME
			i: INTEGER
			p_c: PROPERTY_SQUARE
		do
				--| Add your code here
			create monopoly.make
			monopoly.start_game()
			--across  monopoly.field as field
			--loop
			--	print(field.item)
			--	io.new_line
			--end

			--create dices.make
			--	from
			--		i:=0
			--	until
			--		i>100
			--	loop
			--		dices.throw
			--		print(dices.first)
			--		print(" ")
			--		print(dices.second)
			--		io.new_line
			--
			--		i:=i+1
			--	end

		end

end
