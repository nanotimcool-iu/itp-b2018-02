note
	description: "Summary description for {CHANCE_SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CHANCE_SQUARE

inherit

	SQUARE

create
	make

feature

	make (pos: NATURAL)
		do
			position := pos
			type:="Chance"
		end

feature

	effect (p: PLAYER)
		local
			r: RANDOM
			t: TIME
		do
			print ("Chance square!%N")
			create t.make_now
			create r.set_seed (t.milli_second)
			if (r.item \\ 2 = 1) then
					--lost
				r.forth
				io.put_string (p.name + ", you lost " + ((r.item \\ 30) * 10).out + "rubs :^(%N")
				p.pay (((r.item \\ 30) * 10).as_natural_32)
			else
				r.forth
				io.put_string (p.name + ", you earned " + ((r.item \\ 30) * 10).out + "rubs :^D%N")
				p.add (((r.item \\ 20) * 10).as_natural_32)
			end
		end

end
