note
	description: "Summary description for {DICES}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DICES

create
	make

feature

	first: NATURAL

	second: NATURAL

	rand: RANDOM

feature

	make
		local
			time: TIME
		do
			create time.make_now
			create rand.set_seed (time.milli_second)
		end

	throw()
		do
			first := rand.item.as_natural_32 \\ 6 + 1
			rand.forth
			second := rand.item.as_natural_32 \\ 6 + 1
		end

end
