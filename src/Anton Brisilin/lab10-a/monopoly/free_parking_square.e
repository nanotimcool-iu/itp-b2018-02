note
	description: "Summary description for {FREE_PARKING_SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	FREE_PARKING_SQUARE

inherit

	SQUARE

create
	make

feature

	make (pos: NATURAL)
		do
			type:="Free parking%N"
			position := pos
		end

	effect (p: PLAYER)
		do
			print ("Free park square!")
		end

end
