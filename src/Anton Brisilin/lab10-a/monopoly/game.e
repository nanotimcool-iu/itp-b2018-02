note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME
inherit
	EXECUTION_ENVIRONMENT
create make
feature
	players:ARRAYED_LIST[PLAYER]
	field:ARRAYED_LIST[SQUARE]
feature
	make
	local
		num,i:INTEGER
		player:PLAYER
	do
		print("MONOPOLY game started! How many players will play?%N")
		io.read_integer
		num:=io.last_integer
		create players.make(1)
		from
			i:=1
		until
			i>num
		loop
			print("Player " + i.out + " enter your name:%N")
			io.read_line
			create player.make (io.last_string.twin)
			player.add (500)
			players.extend (player)
			print("Player " + player.name + " successfully created!%N")
			i:=i+1
		end
		load_field("FIELD.txt")
	end
	load_field (filename: STRING)
		local
			file: PLAIN_TEXT_FILE
			go:GO_SQUARE
			chance:CHANCE_SQUARE
			prop_sq:PROPERTY_SQUARE
			property:PROPERTY
			tax:TAX_SQUARE
			jail:JAIL_SQUARE
			to_jail:TO_JAIL_SQUARE

			tmp: STRING
			tokens: LIST [STRING]
			tmp_name: STRING
			tmp_pos, tmp_price, tmp_rent: INTEGER
		do
			create jail.make(0)
			create field.make(1)
			create file.make_open_read (filename)
			from
				file.read_line
			until
				file.end_of_file
			loop
				tmp := file.last_string

				tokens := tmp.split ('%%')
				tokens.go_i_th (1)
					--
				tmp_pos := tokens.item.out.to_integer_32
				tokens.forth
					--
				if(tokens.item ~ "GO") then
					create go.make (tmp_pos.as_natural_32)
					field.extend(go)
				elseif (tokens.item ~ "PROPERTY") then
					file.read_line
					tmp:=file.last_string
					tokens := tmp.split ('%%')
					tokens.go_i_th (1)
					tmp_name := tokens.item
					tokens.forth
					tmp_price := tokens.item.to_integer_32
					tokens.forth
					tmp_rent := tokens.item.to_integer_32
					tokens.forth
					create property.make (tmp_name, tmp_price, tmp_rent)
					create prop_sq.make (property, tmp_pos.as_natural_32)
					field.extend (prop_sq)
				elseif tokens.item ~ "TAX" then
					create tax.make (tmp_pos.as_natural_32)
					field.extend (tax)
				elseif tokens.item ~ "JAIL" then
					jail.set_pos(tmp_pos.as_natural_32)
					field.extend (jail)
				elseif tokens.item ~ "CHANCE" then
					create chance.make (tmp_pos.as_natural_32)
					field.extend (chance)
				elseif tokens.item ~ "TO_JAIL" then
					create to_jail.make (tmp_pos.as_natural_32, jail)
					field.extend (to_jail)
				end
				file.read_line
			end
		end
		start_game
		do
			from
			until
				players.count=1
			loop
				across players as pl
				loop
					do_turn(pl.item)
				end
				across players as pl
				loop
					if pl.item.is_bankrote then
						across field as f
						loop
							if attached{PROPERTY} f.item as prop then
								if prop.owner = pl.item then
									prop.purge_owner
								end
							end
						end
						players.prune (pl.item)
					end
				end
			end
			print (players[1].name + ", YOU ARE WINNER!")
		end
		do_turn(player:PLAYER)
		local
			dices:DICES
			prev_pos,i:NATURAL
		do
			system("clear")
			create dices.make
			print(player.name + ", it's your turn%N")
			print("------------------------------------------------------%N")
			print("Position - " + field[player.cur_position.as_integer_32].position.out +"("+ field[player.cur_position.as_integer_32].type +")%N")
			print("Money:" + player.money.out + "%N")
			print("------------------------------------------------------%N")
			dices.throw
			print("Throwing dices... It's " + dices.first.out + " and " + dices.second.out +"%N")
			prev_pos:=player.cur_position
			player.set_cur_pos (player.cur_position+dices.first+dices.second)
			if(player.cur_position > field.count.as_natural_32) then
				player.cur_position := player.cur_position - field.count.as_natural_32
			end
			if(prev_pos > player.cur_position) then
				i:=1
			else
				i:=prev_pos+1
			end
			from
			until
				i>=player.cur_position-1
			loop
				if(field[i.as_integer_32].type ~ "Go square") then
					field[i.as_integer_32].effect (player)
				end
				i:=i+1
			end
			field[player.cur_position.as_integer_32].effect (player)
			print(player.name + ", your turn finished%N")
			system("sleep 3")
		end
end
