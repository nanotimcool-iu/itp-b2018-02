note
	description: "Summary description for {GO_SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GO_SQUARE

inherit

	SQUARE

create
	make

feature

	make (pos: NATURAL)
		do
			type:="Go square"
			position := pos
		end

feature

	effect (p: PLAYER)
		do
			print ("Go square!%N")
			io.put_string (p.name + " you get 200 rub salary%N")
			p.add (200)
		end

end
