note
	description: "Summary description for {JAIL_SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	JAIL_SQUARE

inherit

	SQUARE

create
	make

feature

	make (pos: NATURAL)
		do
			type:="Jail"
			position := pos
		end

feature

	effect (p: PLAYER)
		local
			dices: DICES
		do
			print ("Jail square!%N")
			if (p.jail_turns = 0) then
				print ("Just visited")
			else
				create dices.make
				print (p.name + "you're in jail. throwing dices...%N")
				dices.throw
				print (dices.first.out + " " + dices.second.out + "%N")
				if (dices.first = dices.second) then
					print ("DOUBLE! You are liberated.")
					p.jail_turns := 0
				end
			end
		end
	set_pos(pos:NATURAL)
	do
		position:=pos
	end
end
