note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create
	make

feature {GAME, PROPERTY, SQUARE}

	jail_turns: NATURAL assign set_jail_turns

	cur_position: NATURAL assign set_cur_pos

	money: NATURAL

feature

	name: STRING

	make (pl_name: STRING)
		do
			name := pl_name
			jail_turns := 0
			cur_position := 1
			money := 0
		end

	is_bankrote: BOOLEAN
		do
			if (money <= 0) then
				Result := true
			end
			Result := false
		end

feature {GAME, PROPERTY, SQUARE}

	pay (sum: NATURAL)
	require
		sum >= 0
		do
			if(sum > money) then
					print(name + " lose!!!")
					money:=0
			end
			money := money - sum
		end

	add (sum: NATURAL)
		require
			sum >= 0
		do
			money := money + sum
		end

	set_cur_pos (pos: NATURAL)
		do
			cur_position := pos
		end

feature {JAIL_SQUARE, TO_JAIL_SQUARE}

	set_jail_turns (n: NATURAL)
		do
			jail_turns := n
		end

end
