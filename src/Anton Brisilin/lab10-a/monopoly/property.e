note
	description: "Summary description for {PROPERTY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROPERTY

create
	make

feature

	name: STRING

	price: NATURAL

	rent: NATURAL

	owner: detachable PLAYER

feature

	make (prop_name: STRING; prop_price: INTEGER; prop_rent: INTEGER)
		require
			prop_price > 0
		do
			name := prop_name
			price := prop_price.to_natural_32
			rent:=prop_rent.as_natural_32
		end

feature

	buy (player: PLAYER)
		require
			attached player
		do
			owner := player
			player.pay (price)
			print (name + " was bought by " + player.name + "%N")
		end

	purge_owner
		do
			owner := Void
		end

end
