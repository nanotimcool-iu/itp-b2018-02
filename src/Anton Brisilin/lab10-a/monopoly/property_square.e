note
	description: "Summary description for {PROPERTY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROPERTY_SQUARE

inherit

	SQUARE

create
	make

feature

	property: PROPERTY

feature

	make (p: PROPERTY; pos: NATURAL)
		do
			type := "Property"
			property := p;
			position := pos
		end

	effect (player: PLAYER)
		do
			print ("Property square!%N")
			if not attached property.owner as p_o then
				if (player.money >= property.price) then
					io.put_string (player.name.out + ", Do you want to buy " + property.name + " for " + property.price.out + "? (y/n):%N")
					io.readline
					if (io.last_string [1].as_lower = 'y') then
						property.buy (player)
					elseif (io.last_string [1].as_lower = 'n') then
						io.put_string ("You refused to buy it. Goodbye!%N")
					else
						io.put_string ("Please, type y or n!")
						effect (player)
					end
				end
			else
				io.put_string (player.name.out + ", square already owned by " + p_o.name + ". You paid " + property.rent.out + ". %N")
				player.pay (property.rent)
				p_o.add(property.rent)
			end
		end

end
