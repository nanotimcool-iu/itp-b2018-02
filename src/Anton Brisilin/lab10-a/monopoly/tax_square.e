note
	description: "Summary description for {TAX_SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	TAX_SQUARE

inherit

	SQUARE

create
	make

feature

	make (pos: NATURAL)
		do
			type:="Tax"
			position := pos
		end

	effect (p: PLAYER)
		do
			print ("Tax square!%N")
			print (p.name + " you lost 10%% of your money - " + (p.money // 10).out)
			io.new_line
			p.pay (p.money // 10)
		end

end
