note
	description: "Summary description for {TO_JAIL_SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	TO_JAIL_SQUARE

inherit

	SQUARE

create
	make

feature

	jail_pos: NATURAL

feature

	make (pos: NATURAL; jail: JAIL_SQUARE)
		do
			type:="To jail%N"
			position := pos
			jail_pos := jail.position
		end

feature

	effect (p: PLAYER)
		do
			print ("To jail square!")
			p.jail_turns := 3
			p.cur_position := jail_pos
		end

end
