note
	description: "ScienceFacility application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			scientist:SCIENTIST
			lab:LAB
		do
			create lab.make
			create{BIOLOGIST} scientist.make (1, "Joe", "Bobik")
			lab.add_member (scientist)
			create{COMPUTER_SCIENTIST} scientist.make (2, "Bertran")
			lab.add_member (scientist)
			create{BIO_INFORMATICS} scientist.make (3, "Oleg", "Murka","Born in Moscow")
			lab.add_member (scientist)
			lab.introduce_all
		end

end
