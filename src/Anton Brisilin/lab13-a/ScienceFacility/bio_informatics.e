note
	description: "Summary description for {BIO_INFORMATICS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIO_INFORMATICS
inherit
	COMPUTER_SCIENTIST
	rename introduce as sc_introduce,
		 	make as sc_make
	end
	BIOLOGIST
	rename
		introduce as bio_introduce,
		make as bio_make
	redefine bio_introduce
	select bio_introduce
	end
create
	make
feature
	bio:STRING
feature
	make(sci_id:INTEGER;sci_name,sci_pet_name:STRING;short_bio:STRING)
	do
		bio_make(sci_id,sci_name,sci_pet_name)
		discipline:="BIO_INFORMATICS"
		bio := short_bio
	end
	bio_introduce
	do
		Precursor {BIOLOGIST}
		print(" "+"bio:"+bio)
	end
end
