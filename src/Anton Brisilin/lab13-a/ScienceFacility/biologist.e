note
	description: "Summary description for {BIOLOGIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIOLOGIST
inherit
	SCIENTIST
	rename
		make as old_make
	redefine
		introduce
	end
create make
feature
	pet_name:STRING
feature
	make(sci_id:INTEGER;sci_name,sci_pet_name:STRING)
	do
		old_make(sci_id,sci_name)
		discipline:="BIOLOGIST"
		pet_name:=sci_pet_name
	end
	introduce
	do
		Precursor
		print(" " + pet_name)
	end
end
