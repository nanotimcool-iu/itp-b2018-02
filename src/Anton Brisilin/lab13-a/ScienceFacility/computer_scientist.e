note
	description: "Summary description for {COMPUTER_SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMPUTER_SCIENTIST
inherit
	SCIENTIST
	rename
		make as old_make
	redefine
		introduce
	end
create
	make
feature
	make(sci_id:INTEGER;sci_name:STRING)
	do
		old_make(sci_id,sci_name)
		discipline:="COMPUTER SCIENTIST"
	end
	introduce
	do
		Precursor
	end
end
