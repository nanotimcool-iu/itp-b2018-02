note
	description: "Summary description for {LAB}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LAB
create make
feature
	employees:ARRAYED_LIST[SCIENTIST]
feature
	add_member(mem:SCIENTIST)
	do
		employees.extend (mem)
	end
	remove_member(mem:SCIENTIST)
	do
		employees.prune (mem)
	end
	introduce_all
	do
		across employees as e
		loop
			e.item.introduce
			print("%N")
		end
	end
	make
	do
		create employees.make (1)
	end
end
