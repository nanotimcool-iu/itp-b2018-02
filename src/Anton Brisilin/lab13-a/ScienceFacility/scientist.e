note
	description: "Summary description for {SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SCIENTIST
feature {SCIENTIST}

	id:INTEGER
	name:STRING
	discipline:STRING
feature

	make(sci_id:INTEGER;sci_name:STRING)
	do
		id:=sci_id
		name:=sci_name
	end
	introduce
	do
		print("name: "+ name+", id:"+id.out + " discipline:" + discipline)
	end
end

