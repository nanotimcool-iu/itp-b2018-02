note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			f: BANK_ACCOUNT
			-- Run application.
		do
				--| Add your code here
			create f.create_class ("F")
			f.deposit (30)
			f.withdraw (500)
		end

end
