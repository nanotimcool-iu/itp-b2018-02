note
	description: "Summary description for {BANK_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

create
	create_class

feature {NONE}

	name: STRING

	balance: INTEGER

	min_balance: INTEGER

feature

	create_class (input_name: STRING)
		do
			name:=input_name
			balance := 100
		end

	deposit (number: INTEGER)
		do
			balance := balance + number
			ensure
				too_much_money: balance < 100000
		end

	withdraw (number: INTEGER)
		do
			balance := balance - number
			ensure
				not_enough_money: balance > 100
		end

end
