note
	description: "university application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
		c : COURSE-- Run application.
	do
		--| Add your code here
		create c.create_class ("I2P", "never", 1, 500)
		c.print_data
	end

end
