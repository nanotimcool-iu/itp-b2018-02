note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

create
	create_class

feature {NONE}

	name: STRING

	identifier: INTEGER

	schedule: STRING

	max_students: INTEGER

	min_students: INTEGER

	students: INTEGER

feature

	set_defaults
		do
			min_students := 3
		end

	create_class (course_name: STRING; course_schedule: STRING; number_of_students: INTEGER; max_number_of_students: INTEGER)
		do
			set_defaults
			name := course_name
			schedule := course_schedule
			students := number_of_students
			max_students := max_number_of_students
			ensure
				number_of_students > min_students
		end
	print_data
	do
		io.put_string ("Subject info: %N")
		io.put_string (name+"%N")
		io.put_string (schedule+"%N")
		io.put_integer (students)
	end
end
