note
	description: "CMS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	c1, c2, c3: CONTACT

	cms: CMS

	make
			-- Run application.
		do
			create cms
			c1 := cms.create_contact("Anton", 1234567891, "Innopolis University", "a.brisilin@innopolis.university")
			c2 := cms.create_contact("Some Professor", 1234567778, "Some University", "s.professor@some.university")
			c3 := cms.create_contact("Some Friend", 1231242141, "Anywhere", "friend@mail.ru")
			cms.add_emergency_contact (c1, c2)
			cms.add_emergency_contact (c2, c3)
			cms.edit_contact (c2, 2, "1234567890")
			cms.remove_emergency_contact (c2)
			cms.remove_emergency_contact (c3)
			print ("Hello Eiffel World!%N")
		end

end
