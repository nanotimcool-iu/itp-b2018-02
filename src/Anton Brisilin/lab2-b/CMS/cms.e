note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

feature

	add_emergency_contact (contact: CONTACT; emergency: CONTACT)
		do
			contact.add_emergency (emergency)
		end

	remove_emergency_contact (contact: CONTACT)
		do
			contact.remove_emergency
		end

	create_contact (input_name: STRING; input_phone_number: INTEGER; input_work_place: STRING; input_email: STRING): CONTACT
		local
			contact: CONTACT
		do
			create contact.make (input_name, input_phone_number, input_work_place, input_email)
			Result := contact
		end

feature

	edit_contact (contact: CONTACT; number_of_parameter: INTEGER; value: STRING)
		do
			contact.edit_contact (number_of_parameter, value)
		end

end
