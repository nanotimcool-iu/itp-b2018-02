note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

create
	make

feature {NONE}

	name: STRING --1

	phone_number: INTEGER_64 --2

	work_place: STRING --3

	email: STRING --4

	call_emergency: CONTACT --5

feature {NONE}

	make (input_name: STRING; input_phone_number: INTEGER_64; input_work_place: STRING; input_email: STRING;)
		require
			number_too_short : input_phone_number >= 1000000000
			number_too_long : input_phone_number <= 9999999999
		do
			name := input_name
			phone_number := input_phone_number
			work_place := input_work_place
			email := input_email
			call_emergency := Current
		end

feature

	error: BOOLEAN

	add_emergency (emergency: CONTACT)
		do
			call_emergency := emergency
		end

	remove_emergency
		do
			call_emergency := Current
		end

	edit_contact (parameter_number: INTEGER; value: STRING)
		do
			error := false
			inspect parameter_number
			when 1 then
				name := value
			when 2 then
				if value.count /= 10 then
					error := true
				else
					phone_number := value.to_integer_64
				end
			when 3 then
				work_place := value
			when 4 then
				email := value
			end
		ensure
			error = false
		end

end
