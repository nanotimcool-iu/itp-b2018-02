note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

create
	make

feature

	create_entry (input_date: DATE_TIME; input_owner: PERSON; input_subject: STRING; input_place: STRING): ENTRY
		local
			new: ENTRY
		do
			create new.make (input_date, input_owner, input_subject, input_place)
			Result := new
		end

	edit_subject (e: ENTRY; new_subject: STRING)
		do
			e.set_subject (new_subject)
		end

	edit_date (e: ENTRY; new_date: DATE_TIME)
		do
			e.set_date (new_date)
		end

	get_owner_name (e: ENTRY): STRING
		do
			Result := e.get_owner.get_name
		end

	in_conflict (e1: ENTRY; e2: ENTRY): BOOLEAN
		do
			if e1.get_date = e2.get_date then
				Result := TRUE
			else
				Result := false
			end
		end

feature

	make
		do
			--Add something if need
		end

end
