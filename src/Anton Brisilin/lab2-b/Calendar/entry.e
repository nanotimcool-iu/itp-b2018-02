note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	make

feature {NONE}

	date: DATE_TIME

	owner: PERSON

	subject: STRING

	place: STRING

feature

	get_date(): DATE_TIME
		do
			Result := date
		end

	get_owner(): PERSON
		do
			Result := owner
		end
	get_subject(): STRING
		do
			Result :=subject
		end
	get_place():STRING
	do
		Result := place
	end
feature
	set_date(new_date:DATE_TIME)
	do
		date := new_date
	end
	set_owner(new_owner:PERSON)
	do
		owner:=new_owner
	end
	set_subject(new_subject:STRING)
	do
		subject:= new_subject
	end
	set_place(new_place:STRING)
	do
		place:=new_place
	end
feature
	make(input_date:DATE_TIME; input_owner:PERSON;input_subject:STRING; input_place:STRING)
	do
		date:=input_date
		owner:=input_owner
		subject:=input_subject
		place:=input_place
	end
end
