note
	description: "Calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
		d:CALENDAR
		dd:DAY
		dt:DATE_TIME
		e:ENTRY
		p:PERSON
		do
			create d.make
			create dd.make (2)
			create dt.make (2018, 09, 03, 18, 30, 0)
			create p.make ("NoOne", 99999999, "Inno", "ddd")
			create e.make (dt, p, "FF", "Somewhere")
			d.days.grow (2)
			d.days.extend (dd)
			d.days[1].add_event (e)
		end

end
