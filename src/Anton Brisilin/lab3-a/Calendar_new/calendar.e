note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

create
	make
feature
	days:ARRAYED_LIST[DAY]
feature
	add_entry(e:ENTRY;day:INTEGER)
	do
		days.at (day).add_event(e)
	end
	create_entry (input_date: DATE_TIME; input_owner: PERSON; input_subject: STRING; input_place: STRING): ENTRY
		local
			new: ENTRY
		do
			create new.make (input_date, input_owner, input_subject, input_place)
			Result := new
		end

	edit_subject (e: ENTRY; new_subject: STRING)
		do
			e.set_subject (new_subject)
		end

	edit_date (e: ENTRY; new_date: DATE_TIME)
		do
			e.set_date (new_date)
		end

	get_owner_name (e: ENTRY): STRING
		do
			Result := e.get_owner.get_name
		end

	in_conflict (e1: ENTRY; e2: ENTRY): BOOLEAN
		do
			if e1.get_date = e2.get_date then
				Result := TRUE
			else
				Result := false
			end
		end
	printable_month(date:DATE):STRING
	local
		a:ARRAY[STRING]
	do
		create a.make_empty
		a[1]:="JANUARY"
		a[2]:="FEBRUARY"
		a[3]:="MARCH"
		a[4]:="APRIL"
		a[5]:="MAY"
		a[6]:="JUNE"
		a[7]:="JULY"
		a[8]:="AUGUST"
		a[9]:="SEPTEMBER"
		a[10]:="OCTOBER"
		a[11]:="NOVEMBER"
		a[12]:="DECEMBER"
		Result:=a.at (date.month)
	end
feature

	make
		do
			create days.make(0)
		end

end
