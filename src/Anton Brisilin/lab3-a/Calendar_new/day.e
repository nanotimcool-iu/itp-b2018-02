note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY
create
	make,
	make_from_list
feature
	date:INTEGER
	events:ARRAYED_LIST[ENTRY]
feature
	make(day:INTEGER)
	do
		date:=day
		create events.make(0)
	end
	make_from_list(day:INTEGER;list:ARRAYED_LIST[ENTRY])
	do
		make(day)
		events:=list
	end
feature
	add_event(e:ENTRY)
	do
		events.extend (e)
	end
end
