note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON
create
	make
feature {NONE}

	name: STRING

	phone_number: INTEGER

	work_place: STRING

	email: STRING

feature

	get_name(): STRING
		do
			Result := name
		end

	get_phone_number(): INTEGER
		do
			Result := phone_number
		end

	get_work_place(): STRING
		do
			Result := work_place
		end

	get_email(): STRING
		do
			Result := email
		end

feature

	set_name (new_name: STRING)
		do
			name := new_name
		end

	set_phone_number (new_number: INTEGER)
		do
			phone_number := new_number
		end

	set_work_place (new_work_place: STRING)
		do
			work_place := new_work_place
		end

	set_email (new_email: STRING)
		do
			email := new_email
		end
feature
	make(input_name:STRING;input_number:INTEGER;input_work_place:STRING;input_email:STRING)
	do
		name:=input_name
		phone_number:=input_number
		work_place:=input_work_place
		email:=input_work_place
	end
end
