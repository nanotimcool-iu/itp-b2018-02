note
	description: "ConnectFour application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature

	field: FIELD

	player1, player2: PLAYER

feature {NONE} -- Initialization

	make
		local
			m, n: INTEGER
			-- Run application.
		do
			--Creating field
			Io.put_string ("Enter field size (2 numbers): %N")
			Io.read_integer
			m := Io.last_integer
			io.read_integer
			n := Io.last_integer
			create field.make (m, n)
			--Creating Players
			create player1.make('#')
			create player2.make('+')
			--Doing turns
			field.draw_field
			from
			until
				 field.filled or player1.win or player2.win
			loop
				Io.put_string ("Player_1,")
				field.turn (player1)
				if not player1.win and not field.filled then
					Io.put_string ("Player_2,")
					field.turn (player2)
				end
			end
			--Checking winner
			if player1.win then
				Io.put_string ("Player_1  WINS!%N")
			else
				if player2.win then
					Io.put_string ("Player_2  WINS!%N")
				else
					Io.put_string ("NOBODY WINS%N")
				end
			end
		end

end
