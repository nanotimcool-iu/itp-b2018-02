﻿note
	description: "Summary description for {FIELD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	FIELD

create
	make

feature

	make (m: INTEGER; n: INTEGER)
		local
			i: INTEGER
			j: INTEGER
			list: ARRAYED_LIST [CHARACTER]
		do
			default_character := '*'
			create columns.make (m)
			from
				i := 1
			until
				i > m
			loop
				create list.make (n)
				columns.extend (list)
				from
					j := 1
				until
					j > n
				loop
					columns.at (i).extend (default_character)
					j := j + 1
				end
				i := i + 1
			end
			width := m
			height := n
		end

feature {NONE}

	columns: ARRAYED_LIST [ARRAYED_LIST [CHARACTER]]

	width, height: INTEGER

	default_character: CHARACTER

feature

	filled: BOOLEAN -- returns TRUE if field if filled
		local
			i, j: INTEGER
		do
			Result := true
			from
				i := 1
			until
				i > width
			loop
				from
					j := 1
				until
					j > height
				loop
					if columns.at (i).at (j) = default_character then
						Result := false
					end
					j := j + 1
				end
				i := i + 1
			end
		end

feature

	is_empty (i: INTEGER; j: INTEGER): BOOLEAN
		do
			if columns.at (i).at (j) = default_character then
				Result := True
			else
				Result := false
			end
		end

	get_character (i: INTEGER; j: INTEGER): CHARACTER
		do
			Result := columns.at (i).at (j)
		end

	turn (player: PLAYER)
		local
			index, i: INTEGER
			error: BOOLEAN
		do
			i := 1
			index := player.turn
			from
			until
				(index <= width) and (index >= 1)
			loop
				io.put_string ("Wrong column number!%N")
				index := player.turn
			end
			if not is_empty (index, 1) then
				error := true
			else
				from
					i := 1
				until
					i > height
				loop
					if not (is_empty (index, i)) then
						if is_empty (index, i - 1) then
							columns.at (index).at (i - 1) := player.character
						end
					else
						if i + 1 > height then
							if is_empty (index, i) then
								columns.at (index).at (i) := player.character
							end
						end
					end
					i := i + 1
				end
			end
			if error then
				io.put_string ("Impossible turn!")
				turn (player)
			else
				check_win (player)
				draw_field
			end
		end

	check_win (player: PLAYER)
		local
			i, j, k, m, n: INTEGER
			counter: INTEGER
		do
				-- checking rows
			from
				i := 1
			until
				i > height or counter >= 4
			loop
				counter := 0
				from
					j := 1
				until
					j > width or counter >= 4
				loop
					if get_character (j, i) = player.character then
						counter := counter + 1
					else
						counter := 0
					end
					j := j + 1
				end
				i := i + 1
			end
			if counter < 4 then
				counter := 0
			end
				--checking cols
			from
				i := 1
			until
				i > width or counter >= 4
			loop
				counter := 0
				from
					j := 1
				until
					j > height or counter >= 4
				loop
					if get_character (i, j) = player.character then
						counter := counter + 1
					else
						counter := 0
					end
					j := j + 1
				end
				i := i + 1
			end
			if counter < 4 then
				counter := 0
			end
				--checking diagonals
			from
				i := 1
			until
				i > width or counter >= 4
			loop
				from
					j := 1
				until
					j > (height - 3) or counter >= 4
				loop
						--↘
						-- ↘
						--  ↘
					from
						k := i
						m := j
						n := 1
					until
						n > 4 or counter >= 4 or k > width
					loop
						if get_character (k, m) = player.character then
							counter := counter + 1
						else
							counter := 0
						end
						k := k + 1
						m := m + 1
						n := n + 1
					end
					if counter < 4 then
						counter := 0
					end
						--  ↙
						-- ↙
						--↙
					from
						k := i
						m := j
						n := 1
					until
						n > 4 or counter >= 4 or k < 1 or m < 1
					loop
						if get_character (k, m) = player.character then
							counter := counter + 1
						else
							counter := 0
						end
						k := k - 1
						m := m + 1
						n := n + 1
					end
					if counter < 4 then
						counter := 0
					end
					j := j + 1
				end
				i := i + 1
			end
			if counter >= 4 then
				player.win := true
			else
				player.win := false
			end
		end

	draw_field
		local
			i, j: INTEGER
		do
			from
				i := 1
			until
				i > width
			loop
				from
					j := 1
				until
					j > height
				loop
					Io.put_character (columns.at (j).at (i))
					j := j + 1
				end
				io.put_new_line
				i := i + 1
			end
		end

end
