note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER
create
	make
feature
	character:CHARACTER
feature
	make(char:CHARACTER)
	do
		character:=char
		win := false
	end
feature
	win : BOOLEAN assign set_win
	set_win(value:BOOLEAN)
	do
		win:=value
	end
feature
	turn:INTEGER
	do
		Io.put_string ("Enter column to turn: %N")
		Io.read_integer
		Result:=IO.last_integer
	end
end
