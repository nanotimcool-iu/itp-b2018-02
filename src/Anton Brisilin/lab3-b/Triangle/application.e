note
	description: "Triangle application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			i, j: INTEGER;
			num: INTEGER;
			stop: INTEGER;
			row_len, max_row_length: INTEGER;
			flag: BOOLEAN
			s: STRING
			spaces: STRING
		do
			io.read_integer
			stop := io.last_integer
			num := 1
			s := ""
			max_row_length := row_length (stop) + 1
			from
				i := 1
			until
				i > stop
			loop
				flag := i \\ 2 = 0
				from
					j := 1
				until
					j > i
				loop
					if (not flag) then
						io.put_integer (num)
						s.append (reverse (num.out))
						num := num + 1
						flag := not flag
					else
						s.append_character (' ')
						io.put_character (' ')
						flag := not flag
					end
					j := j + 1
				end
				spaces := " "
				spaces.multiply (max_row_length - 2 * s.count)
				io.put_string (spaces)
				io.put_string (reverse (s))
				s := ""
				io.new_line
				i := i + 1
			end
		end

feature

	row_length (row_num: INTEGER): INTEGER
		local
			last: INTEGER
			n: INTEGER
			i: INTEGER
		do
			last := 0
			from
				i := 0
			until
				i > row_num
			loop
				n := i // 2 + i \\ 2;
				last := last + n
				i := i + 1
			end
			Result := 2 * (n * length (last) + n - 1)
			if row_num \\ 2 = 0 then
				Result := Result + 2
			end
		end

	length (number: INTEGER): INTEGER
		local
			num: INTEGER
		do
			num := number
			Result := 0
			from
			until
				num = 0
			loop
				num := num // 10
				Result := Result + 1
			end
		end

	reverse (s: STRING): STRING
		local
			i: INTEGER
		do
			Result := ""
			from
				i := s.count
			until
				i < 1
			loop
				Result.append_character (s.at (i))
				i := i - 1
			end
		end

end
