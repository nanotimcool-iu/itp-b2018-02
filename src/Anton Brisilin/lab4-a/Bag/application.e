note
	description: "bag application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
			local
				d,f:CELL_INFO
				b,c:BAG2

		do
			create d.make ('9', 9)
			create f.make ('4', 4)
			create b.make
			create c.make
			b.add ('9', 9)
			b.add ('9', 9)
			c.add ('9', 9)
			io.put_boolean (b.is_equal_bag (c))
		end

end
