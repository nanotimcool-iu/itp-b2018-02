note
	description: "Summary description for {BAG2}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BAG2

create
	make

feature

	make
		do
			create area.make (1)
			size := 0
		end

feature {NONE}

	area: ARRAYED_LIST [CELL_INFO]

feature

	size: INTEGER

feature

	get (i: INTEGER): CELL_INFO
		require
			i <= size
		do
			result := area [i]
		end

	add (val: CHARACTER; num: INTEGER)
		require
			num > 0
		local
			i: INTEGER
			exists: BOOLEAN
			temp: CELL_INFO
		do
			create temp.make (val, num)
			exists := false
			from
				i := 1
			until
				i >= size
			loop
				if (area [i].value = val) then
					exists := true
					area [i].number_copies := area [i].number_copies + num
				end
			end
			if not exists then
				size := size + 1
				area.extend (temp)
			end
		end

	remove (val: CHARACTER; num: INTEGER)
		require
			num > 0
		local
			i: INTEGER
			exists: BOOLEAN
		do
			exists := false
			from
				i := 1
			until
				i >= size
			loop
				if (area [i].value = val) then
					exists := true
					if (area [i].number_copies < num) then
						area.go_i_th (i)
						area.remove
						area.go_i_th (1)
					else
						area [i].value := area [i].value - num
					end
				end
			end
		end

	min: CHARACTER
		require
			size > 0
		local
			i: INTEGER
			c: CELL_INFO
		do
			c := area [1]
			from
				i := 1
			until
				i >= size
			loop
				if area [i].number_copies < c.number_copies then
					c := area [i]
				end
			end
			Result := c.value
		end

	max: CHARACTER
		require
			size > 0
		local
			i: INTEGER
			c: CELL_INFO
		do
			c := area [1]
			from
				i := 1
			until
				i >= size
			loop
				if area [i].number_copies > c.number_copies then
					c := area [i]
				end
			end
			Result := c.value
		end

	is_equal_bag (b: BAG2): BOOLEAN
		local
			i: INTEGER
		do
			Result := true
			if size = b.size then
				from
					i := 1
				until
					i >= size
				loop
					if not area [i].equals (b.get (i)) then
						Result := false
					end
				end
			else
				Result := false
			end
		end

end
