note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO
create
	make
feature
	make(c:CHARACTER;v:INTEGER)
	do
		value:=c
		number_copies:=v
	end
feature

	value: CHARACTER assign set_val

	number_copies: INTEGER assign set_num

feature

	set_val (val: CHARACTER)
		do
			value := val
		end

	set_num (val: INTEGER)
		do
			number_copies := val
		end

feature

	equals (other: CELL_INFO): BOOLEAN
		do
			if value = other.value and number_copies = other.number_copies then
				Result := true
			else
				Result := false
			end
		end

end
