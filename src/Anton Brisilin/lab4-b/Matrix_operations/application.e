note
	description: "Matrix_operations application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	EXECUTION_ENVIRONMENT

create
	make

feature {NONE} -- Initialization

	ddebug: BOOLEAN

	make
		local
			a, b: ARRAY2 [INTEGER]
			-- Run application.
		do
			ddebug := true
			system ("clear")
			create a.make_filled (1, 3, 3)
			a [1, 2] := -1
			a [2, 1] := -1
			a [3, 2] := 5
			a [1, 3] := 51
			pprint (a)
			create b.make_filled (1, 3, 3)
			io.put_integer (det (a))
		end

	pprint (a: ARRAY2 [INTEGER])
		local
			i, j: INTEGER
		do
			from
				i := 1
				j := 1
			until
				i > a.height
			loop
				from
					j := 1
				until
					j > a.width
				loop
					io.put_integer (a [i, j])
					io.put_character (' ')
					j := j + 1
				end
				io.put_character ('%N')
				i := i + 1
			end
		end

	add (m1: ARRAY2 [INTEGER]; m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.count >= 1 and m2.count >= 1
			m1.height = m2.height
			m1.width = m2.width
		local
			i, j: INTEGER;
		do
			create Result.make_filled (0, m1.height, m1.width)
			from
				i := 1
				j := 1
			until
				i > m1.height
			loop
				from
					j := 1
				until
					j > m1.width
				loop
					Result [i, j] := m1 [i, j] + m2 [i, j]
					j := j + 1
				end
				i := i + 1
			end
			if (ddebug) then
				pprint (m1)
				io.put_character ('+')
				io.new_line
				pprint (m2)
				io.put_character ('=')
				io.new_line
				pprint (Result)
			end
		end

	minus (m1: ARRAY2 [INTEGER]; m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.count >= 1 and m2.count >= 1
			m1.height = m2.height
			m1.width = m2.width
		local
			i, j: INTEGER;
		do
			create Result.make_filled (0, m1.height, m1.width)
			from
				i := 1
				j := 1
			until
				i > m1.height
			loop
				from
					j := 1
				until
					j > m1.width
				loop
					Result [i, j] := m1 [i, j] - m2 [i, j]
					j := j + 1
				end
				i := i + 1
			end
			if ddebug then
				pprint (m1)
				io.put_character ('-')
				io.new_line
				pprint (m2)
				io.put_character ('=')
				io.new_line
				pprint (Result)
			end
		end

	prod (m1: ARRAY2 [INTEGER]; m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.width = m2.height
		local
			i, j, k: INTEGER
		do
			create Result.make_filled (0, m1.height, m2.width)
			from
				i := 1
			until
				i > Result.height
			loop
				from
					j := 1
				until
					j > Result.width
				loop
					from
						k := 1
					until
						k > m1.height
					loop
						Result [i, j] := Result [i, j] + m1 [i, k] * m2 [k, j]
						K := k + 1
					end
					j := j + 1
				end
				i := i + 1
			end
			if ddebug then
				pprint (m1)
				io.put_character ('*')
				io.new_line
				pprint (m2)
				io.put_character ('=')
				io.new_line
				pprint (Result)
			end
		end

	minor (a: ARRAY2 [INTEGER]; i: INTEGER; j: INTEGER): ARRAY2 [INTEGER]
		local
			m, n: INTEGER
			c: INTEGER
			l: ARRAYED_LIST [INTEGER]
		do
			create Result.make_filled (0, a.height - 1, a.width - 1)
			create l.make (1)
			from
				m := 1
			until
				m > a.height
			loop
				from
					n := 1
				until
					n > a.width
				loop
					if not (m = i or n = j) then
						l.extend (a [m, n])
					end
					n := n + 1
				end
				m := m + 1
			end
			c := 1
			from
				m := 1
			until
				m > Result.height
			loop
				from
					n := 1
				until
					n > Result.width
				loop
					Result [m, n] := l [c]
					c := c + 1
					n := n + 1
				end
				M := M + 1
			end
		end

	det (a: ARRAY2 [INTEGER]): INTEGER
		require
			a.width = a.height
		local
			i, j: INTEGER
		do
			if (a.height = 1) then
				Result := a [1, 1]
			else
				Result := 0
				from
					j := 1
				until
					j > a.height
				loop
					Result := Result + ((-1).power (j + 1).rounded) * a [1, j] * det (minor (a, 1, j))
					j := j + 1
				end
			end
		end

end
