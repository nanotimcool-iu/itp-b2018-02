note
	description: "Vector_oper application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS
	DOUBLE_MATH
create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			print (sine(pi).rounded)

			print ("Hello Eiffel World!%N")
		end

	cross_product (a: ARRAY [INTEGER]; b: ARRAY [INTEGER]): ARRAY [INTEGER]
		require
			a.count = 3
			b.count = 3
		do
			create Result.make_filled (0, 1, 3)
			Result [1] := a [2] * b [3] - a [3] * b [2]
			Result [2] := a [1] * b [3] - a [3] * b [1]
			Result [3] := a [1] * b [2] - a [2] * b [1]
		end

	dot_product (a: ARRAY [INTEGER]; b: ARRAY [INTEGER]): INTEGER
		require
			a.count <= 3 and a.count > 0
			a.count = b.count
		do
			if (a.count = 3) then
				Result := a [1] * b [1] + a [2] * b [2] + a [3] * b [3]
			else
				if a.count = 2 then
					Result := a [1] * b [1] + a [2] * b [2]
				else
					if (a.count = 1) then
						Result := a [1] * b [1]
					end
				end
			end
		end
	cos(a: ARRAY [INTEGER]; b: ARRAY [INTEGER]):REAL
	do
		Result := dot_product (a, b) /(length (a) * length (b))
	end
	length (a: ARRAY [INTEGER]): REAL
		require
			a.count <= 3 and a.count > 0
		do
			if (a.count = 3) then
				Result := sqrt(a [1].power (2) + a [2].power (2) + a [3].power (2)).rounded
			else
				if a.count = 2 then
					Result := sqrt(a [1].power (2) + a [2].power (2)).rounded
				else
					if (a.count = 1) then
						Result := a [1].abs
					end
				end
			end
		end
	triangle_area (a: ARRAY [INTEGER]; b: ARRAY [INTEGER]): REAL
		do
			Result:= (length (a) * length (b) * sine(cos(a,b)) / 2).truncated_to_real
		end

end
