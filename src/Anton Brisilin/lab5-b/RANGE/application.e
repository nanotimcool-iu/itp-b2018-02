note
	description: "RANGE application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local a,b:RANGE
			-- Run application.
		do
			create a.make (1,3)
			create b.make (2, 6)
			a.substract (b)
			print(a.str)
		end

end
