note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	make

feature

	make (lower: INTEGER; higher: INTEGER)
		do
			left := lower
			right := higher
		end

feature

	left, right: INTEGER

	set: ARRAYED_LIST [INTEGER]
		local
			i: INTEGER
		do
			create Result.make (1)
			from
				I := left
			until
				i > right
			loop
				Result.extend (i)
				i := i + 1
			end
		end

	is_equal_range (Other: like Current): BOOLEAN
		do
			if left = other.left and right = other.right then
				Result := true
			else
				if Current.is_empty and other.is_empty then
					Result := true
				else
					Result := false
				end
			end
		end

	is_empty: BOOLEAN
		do
			if set.count > 0 then
				Result := false
			else
				Result := true
			end
		end

	is_subrange_of_other (other: like Current): BOOLEAN
		do
			Result := false
			if (is_empty) then
				Result := TRUE
			elseif left >= other.left and right <= other.right then
				Result := true
			end
		end

	is_super_range_of (other: like Current): BOOLEAN
		do
			Result := other.is_subrange_of_other (Current)
		end

	left_overlaps (other: like Current): BOOLEAN
		do
			Result := (set.has (left) and other.set.has (left))
		end

	right_overlaps (other: like Current): BOOLEAN
		do
			Result := (set.has (right) and other.set.has (right))
		end

	overlaps (other: like Current): BOOLEAN
		do
			Result := (left_overlaps (other) or right_overlaps (other)) or (is_subrange_of_other (other) or is_super_range_of (other))
		end

	add (other: like Current)
		require
			from_left: other.right >= left - 1
			from_right: other.left <= right + 1
		do
			Current.make (left.min (other.left), right.max (other.right))
		end

	substract (other: like Current)
		require
			from_right: other.left > left implies other.right >= right or other.is_empty
			from_left: other.right < right implies other.left < left or other.is_empty
		do
			if not other.is_empty then
				if other.left > left then
					Current.make (left, other.left - 1)
				elseif other.right < right then
					current.make (other.right - 1, right)
				end
			end
		end

	str: STRING
		do
			Result := "{" + left.out + ".." + right.out + "}"
		end

end
