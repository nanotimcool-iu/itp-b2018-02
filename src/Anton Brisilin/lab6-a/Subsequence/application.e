note
	description: "Subsequence application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			str1 := "fuck"
			str2 := "cuc"
			print (LCS (str1.count, str2.count))
		end

	str1, str2: STRING

	LCS (a: INTEGER; b: INTEGER): STRING
		do
			if (a * b = 0) then
				Result := ""
			else
				if (str1 [a] = str2 [b]) then
					Result :=  LCS (a - 1, b - 1) + str1 [a].out
				else
					Result := max (LCS (a, b - 1), LCS (a - 1, b))
				end
			end
		end

	max (a: STRING; b: STRING): STRING
		do
			if (a.count > b.count) then
				Result := a
			else
				Result := b
			end
		end

end
