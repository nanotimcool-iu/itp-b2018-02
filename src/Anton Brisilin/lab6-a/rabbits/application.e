note
	description: "rabbits application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
		rabbits:INTEGER
			-- Run application.
		do
			rabbits :=recursive_rabbits (12)
			io.new_line
			print (rabbits.out + " rabbits (" + (rabbits/2).out + " pairs)")
		end

	recursive_rabbits (months: INTEGER): INTEGER
		do
			if (months = 1) then
				Result := 2
			else
				if (months = 2) then
					Result := 4
				else
					Result := recursive_rabbits (months - 1) + recursive_rabbits (months - 2)
				end
			end
		end

end
