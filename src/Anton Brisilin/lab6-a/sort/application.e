note
	description: "sort application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			f: ARRAYED_LIST [INTEGER]
		do
			create f.make (1)
			f.extend (5)
			f.extend (4)
			f.extend (3)
			f.extend (2)
			f.extend (1)
			f := sort (f)
			across
				f as d
			loop
				print (d.item)
			end
		end

	sublist (list: ARRAYED_LIST [INTEGER]; start: INTEGER; finish: INTEGER): ARRAYED_LIST [INTEGER]
		require
			finish >= start
		local
			i: INTEGER
		do
			create Result.make (1)
			from
				i := start
			until
				i > finish
			loop
				Result.extend (list [i])
				i := i + 1
			end
		end

	merge (a: ARRAYED_LIST [INTEGER]; b: ARRAYED_LIST [INTEGER]): ARRAYED_LIST [INTEGER]
		local
			i, j: INTEGER
		do
			create Result.make (1)
			from
				i := 1
				j := 1
			until
				i > a.count or j > b.count
			loop
				if a [i] > b [j] then
					Result.extend (b [j])
					J := j + 1
				else
					Result.extend (a [i])
					i := i + 1
				end
			end
			if (j <= b.count) then
				from
				until
					j > b.count
				loop
					Result.extend (b [j])
					J := j + 1
				end
			else
				if (i <= a.count) then
					from
					until
						i > a.count
					loop
						Result.extend (a [i])
						i := i + 1
					end
				end
			end
		end

	sort (v: ARRAYED_LIST [INTEGER]): ARRAYED_LIST [INTEGER]
		local
			a, b: ARRAYED_LIST [INTEGER]
			mid: INTEGER
		do
			create Result.make (1)
			if (v.count > 1) then
				mid := v.count // 2
				a := sublist (v, 1, mid)
				b := sublist (v, mid + 1, v.count)
				a := sort (a)
				b := sort (b)
				Result := merge (a, b)
				else
					Result := v.twin
			end
		end

end
