note
	description: "string_reverse application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			print (string_reverse ("String to reverse"))
		end

	string_reverse (a: STRING): STRING
		require
			a.count >= 1
		do
			if (a.count = 1) then
				Result := a
			else
				Result := string_reverse (a.substring (2, a.count))
				Result.append_character (a.at (1))
			end
		end
end
