note
	description: "POWERSET application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			a: ARRAYED_SET [INTEGER]
			c: ARRAYED_SET [ARRAYED_SET [INTEGER]]
		do
			io.put_string ("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_%N")
			create a.make (3)
			a.put (1)
			a.put (2)
			a.put (3)
			a.put (4)
			c := power_set (a)
			remove_extra (c)
			across
				c as b
			loop
				across
					b.item as d
				loop
					print (d.item)
				end
				print ("%N")
			end
		end

	power_set (set: ARRAYED_SET [INTEGER]): ARRAYED_SET [ARRAYED_SET [INTEGER]]
		local
			i: INTEGER
			tmp: ARRAYED_SET [INTEGER]
		do
			create Result.make (1)
			if (set.count >= 1) then
				Result.extend (set)
				from
					i := 1
				until
					i > set.count
				loop
					create tmp.make (1)
					tmp.copy (set)
					tmp.go_i_th (i)
					tmp.prune (tmp.item)
					Result.merge (power_set (tmp))
					i := i + 1
				end
			end
		end

	equals (a: ARRAYED_SET [INTEGER]; b: ARRAYED_SET [INTEGER]): BOOLEAN
		do
			Result := false
			if a.count = b.count then
				Result := true
				across
					a as f
				loop
					if b.has (f.item) = false then
						Result := false
					end
				end
			end
		end

	remove_extra (a: ARRAYED_SET [ARRAYED_SET [INTEGER]])
		local
			i: INTEGER
			j: INTEGER
			tmp: ARRAYED_SET [INTEGER]
		do
			from
				j := 1
			until
				j > a.count
			loop
				a.go_i_th (j)
				tmp:= a.item.twin
				from
					i := j + 1
				until
					i > a.count
				loop
					a.go_i_th (i)
					if equals (a.item, tmp) then
						a.prune (a.item)
						i := i - 1
					end
					i := i + 1
				end
				j:=j+1
			end
		end

end
