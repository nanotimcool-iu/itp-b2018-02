note
	description: "ROBOT_PATH application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			i, j: INTEGER
		do
			create field.make_filled (0, 4, 5)
			field [1, 2] := 1
			field [2, 2] := 1
			field [3, 1] := 1
			from
				i := 1
			until
				i > field.height
			loop
				from
					j := 1
				until
					j > field.width
				loop
					print (field [i, j])
					j := j + 1
				end
				i := i + 1
				io.new_line
			end
			across
				search (1, 1) as f
			loop
				print (decode_way (f.item) + "%N")
			end
		end

	decode_way (n: INTEGER): STRING
		do
			inspect n
			when 1 then
				Result := "down"
			when 2 then
				Result := "right"
			when -1 then
				Result := "finish"
			when 0 then
				Result := "no way"
			else
				Result := "undefined"
			end
		end

	field: ARRAY2 [INTEGER]
			--0-empty
			--1-obstacle

	more (a: ARRAYED_LIST [INTEGER]; b: ARRAYED_LIST [INTEGER]): BOOLEAN
		do
			if a.count = 0 or a.count > b.count then
				Result := false
			else
				if b.count = 0 or b.count > a.count then
					Result := true
				end
				Result := false
			end
		end

	search (i: INTEGER; j: INTEGER): ARRAYED_LIST [INTEGER]
		require
			field /= Void
		do
			create Result.make (1)
			if (j = field.width and i = field.height) then
				Result.extend (-1)
			else
				if (j = field.width or field [i, j + 1] = 1) and not (field [i + 1, j] = 1) then
					create Result.make (1)
					Result.extend (1)
					Result.append (search (i + 1, j))
				else
					if (i = field.height or field [i + 1, j] = 1) and not (field [i, j + 1] = 1) then
						create Result.make (1)
						Result.extend (2)
						Result.append (search (i, j + 1))
					else
						if (field [i, j + 1] = 1) and (field [i + 1, j] = 1) then
							create Result.make (1)
							Result.extend (0)
						else
							if more (search (i + 1, j), search (i, j + 1)) then
								create Result.make (1)
								Result.extend (1)
								Result.append (search (i + 1, j))
							else
								create Result.make (1)
								Result.extend (2)
								Result.append (search (i, j + 1))
							end
						end
					end
				end
			end
		end

end
