note
	description: "hangman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
	local a: HANGMAN
			i,j:INTEGER
			-- Run application.
		do
			io.put_string ("Enter number of players:%N")
			io.read_integer
			i:=io.last_integer

			io.put_string ("Enter number of lives:%N")
			io.read_integer
			j:=io.last_integer

			create a.make (i, j)
			io.new_line
			a.play
		end

end
