note
	description: "Summary description for {DICTIONARY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DICTIONARY
create
	make_from_array,
	make_from_file

feature {NONE}
	list_of_words:ARRAYED_LIST[STRING]
feature
	make_from_array(a:ARRAYED_LIST[STRING])
	do
		create list_of_words.make(1)
		list_of_words.copy(a)
	end
	make_from_file(a:READABLE_STRING_GENERAL)
	local file:PLAIN_TEXT_FILE
	do
		create list_of_words.make (1)
		create file.make_open_read (a)
		from
			file.read_line
		until
			file.end_of_file
		loop
			list_of_words.extend (file.last_string.twin)
			file.read_line
		end
		file.close
	end
feature
	random_word:STRING
	local
		time : TIME
	do
		create time.make_now
		Result := list_of_words[(time.milli_second \\ list_of_words.count)+1]
	end
end
