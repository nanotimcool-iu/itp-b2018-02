note
	description: "Summary description for {HANGMAN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HANGMAN

create
	make

feature {NONE}

	players: ARRAYED_LIST [PLAYER]

	dictionary: DICTIONARY

	word: STRING

	masked_word: STRING

	cur_player: INTEGER

feature

	make (num_players: INTEGER; max_lives: INTEGER)
	require
		num_players > 1
		max_lives > 0
		local
			i: INTEGER
			player: PLAYER
		do
			create players.make (1)
			from
				i := 1
			until
				i > num_players
			loop
				io.put_string ("Player " + i.out + " enter your name:%N")
				io.read_line
				create player.make (io.last_string.twin, max_lives)
				players.extend (player)
				i := i + 1
			end
			reset_words
			cur_player := 1
		end

	reset_words
		do
			create dictionary.make_from_file ("dictionary.dic")
			word := dictionary.random_word
			masked_word := "_"
			masked_word.multiply (word.count)
		end

feature

	all_dead: BOOLEAN
		do
			Result := true
			across
				players as a
			loop
				if (a.item.is_alive) then
					Result := false
				end
			end
		end

	turn
		local
			i: INTEGER
		do
			io.put_string (masked_word + "%N")
			if players [cur_player].is_alive then
				io.put_string (players [cur_player].name + " it's your turn:%N")
				io.read_character
				io.next_line
				if (word.has (io.last_character)) then
					from
						i := 1
					until
						i > word.count
					loop
						if (word.at (i) = io.last_character) then
							masked_word.at (i) := io.last_character
						end
						i := i + 1
					end
				else
					io.put_string ("No, you're wrong! %N")
					players [cur_player].lives := players [cur_player].lives - 1
					if not players [cur_player].is_alive then
						io.put_string (players [cur_player].name + " you are dead!%N")
					end
				end
			end
			if (cur_player < players.count) then
				cur_player := cur_player + 1
			else
				cur_player := 1
			end
		end

	play
		local
			winner: INTEGER
		do
			from
			until
				not masked_word.has ('_') or all_dead
			loop
				turn
			end
			if all_dead then
				io.put_string ("ALL DEAD -- THERE IS NO WINNER%N")
			else
				from
					winner := cur_player - 1
				until
					winner >= 1 and players [winner].is_alive
				loop
					if (winner <= 1) then
						winner := players.count
					else
						winner := winner - 1
					end
				end
				io.put_string (players [winner].name + " you are winner!%N")
			end
		end

end
