note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER
create
	make
feature
	name:STRING
	lives:INTEGER assign set_lives
feature
	make(pl_name:STRING; max_lives:INTEGER)
	do
		name:=pl_name
		lives:=max_lives
	end
	set_lives(val:INTEGER)
	do
		lives:=val
	end
feature is_alive:BOOLEAN
do
	Result := lives > 0
end
end
