note
	description: "Summary description for {BIN_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIN_TREE [G -> COMPARABLE]

create
	make

feature

	make (a: G)
		do
			value := a
		end

feature {NONE}

	value: G

	left: detachable BIN_TREE [G]

	right: detachable BIN_TREE [G]

feature

	info: G
		do
			Result := value
		end

	height: INTEGER
		do
			if attached {BIN_TREE [G]} left as l and attached {BIN_TREE [G]} right as r then
				Result := 1 + r.height.max (r.height)
			elseif attached {BIN_TREE [G]} right as r then
				Result := 1 + r.height
			elseif attached {BIN_TREE [G]} left as l then
				Result := 1 + l.height
			else
				Result := 1
			end
		end

	add (t: BIN_TREE [G])
		do
			if t.info >= value then
				if attached right as r then
					r.add (t)
				else
					right := t
				end
			else
				if attached left as l then
					l.add (t)
				else
					left := t
				end
			end
		end

end
