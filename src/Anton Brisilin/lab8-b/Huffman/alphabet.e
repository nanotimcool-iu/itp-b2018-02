note
	description: "Summary description for {ALPHABET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ALPHABET

create
	make

feature

	chars: ARRAYED_LIST [CHARACTER]

	frequencies: ARRAYED_LIST [INTEGER]

feature

	Make (str: STRING)
		do
			create chars.make (1)
			create frequencies.make (1)
			across
				str as s
			loop
				if (not chars.has (s.item)) then
					chars.extend (s.item)
					frequencies.extend (1)
				else
					across
						chars as c
					loop
						if c.item = s.item then
							frequencies [c.cursor_index] := frequencies [c.cursor_index] + 1
						end
					end
				end
			end
		end

feature {NONE}

	sort
		local
			i, j: INTEGER
			tmp: INTEGER
			tmp2: CHARACTER
		do
			from
				i := 1
			until
				i > frequencies.count - 1
			loop
				from
					j := 1
				until
					j > frequencies.count - 1
				loop
					if (frequencies [j] > frequencies [1 + j]) then
							--swap (frequencies[i],frequencies[1+i])
						tmp := frequencies [j]
						frequencies [j] := frequencies [j + 1]
						frequencies [j + 1] := tmp
							--swap (chars[i],chars[1+i])
						tmp2 := chars [j]
						chars [j] := chars [j + 1]
						chars [j + 1] := tmp2
					end
					j := j + 1
				end
				i := i + 1
			end
		end

feature

	to_out_str: STRING
		do
			create Result.make (1)
			across
				chars as c
			loop
				Result.append (c.item.out + ":" + frequencies [c.cursor_index].out)
				Result.append ("%N")
			end
		end

end
