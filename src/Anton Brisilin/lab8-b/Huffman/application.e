note
	description: "Haffman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
		s:STRING
		sd:HUFFMAN_CODE
		h:HUFFMAN_ENCODING
		do
			create h
			S:="rendom string"
			print("%N" + "SOURCE  STRING: " + S + "%N")
			sd := h.encode_string (s)
			print("ENCODED STRING: "+sd.out+"%N")
			s:=h.decode (sd)
			print("DECODED STRING: " +s+"%N")
		end
end
