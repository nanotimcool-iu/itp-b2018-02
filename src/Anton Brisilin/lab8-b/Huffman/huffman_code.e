note
	description: "Summary description for {HUFFMAN_CODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN_CODE

inherit

	ANY
		redefine
			out
		end
create make
feature {HUFFMAN_ENCODING}
	prefix_tree:NODE
	code: STRING assign set_code
	set_code(s:STRING)
	do
		code:=s
	end
feature

	make(s:STRING;tree:NODE)
		do
			create code.make (1)
			code := s
			prefix_tree:=tree
		end

	out: STRING
		do
			Result := code
		end
end
