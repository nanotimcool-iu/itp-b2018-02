note
	description: "Summary description for {HUFFMAN_ENCODING}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN_ENCODING

inherit

	ANY
		redefine
			default_create
		end

feature {NONE}

	alphabet: ALPHABET

	available_nodes: ARRAYED_LIST [NODE]

	prefix_tree: NODE

feature

	default_create
		do
			create alphabet.make ("")
			create available_nodes.make (1)
			create prefix_tree.make ("", 0)
		end

	is_valid_char (c: CHARACTER): BOOLEAN
		do
			Result := alphabet.chars.has (c)
		end

	encode_char (c: CHARACTER): STRING
		require
			character_in_alphabet: is_valid_char (c)
		local
			node: NODE
		do
			create Result.make (1)
			create node.make ("", 0)
			node := prefix_tree
				--print (node.name)
			from
			until
				node.name ~ c.out
			loop
				if (attached node.left as left) then
					if left.name.has (c) then
						node := left
						Result := Result + "0"
					else
						if (attached node.right as right) then
							node := right
							Result := Result + "1"
						end
					end
				end
			end
		end

	encode_string (d: STRING): HUFFMAN_CODE
		local
			tmp_node: NODE
		do
			create alphabet.make (d)
			across
				alphabet.chars as ch
			loop
				create tmp_node.make (ch.item.out, alphabet.frequencies [ch.cursor_index])
				available_nodes.extend (tmp_node)
			end
			build_prefix_tree
			create Result.make ("", prefix_tree)
			across
				d as c
			loop
				Result.code := Result.code + encode_char (c.item)
			end
		end

	decode (d: HUFFMAN_CODE): STRING
		local
			node: NODE
			i: INTEGER
			tmp: HUFFMAN_CODE
		do
			from
				i := 1
				node := d.prefix_tree
			until
				not attached node.left and not attached node.right
			loop
				if d.code [i] = '1' then
					if attached node.right as right then
						node := right
						i := i + 1
					end
				elseif d.code [i] = '0' then
					if attached node.left as left then
						node := left
						i := i + 1
					end
				end
			end
			Result := node.name
			if (i < d.code.count) then
				create tmp.make (d.code.substring (i, d.code.count), d.prefix_tree)
				Result := Result + decode (tmp)
			end
		end

	valid_code (d: STRING): BOOLEAN
		do
			Result := true
			across
				d as c
			loop
				if c.item /= '0' and c.item /= '1' then
					Result := false
				end
			end
		end

feature {NONE}

	build_prefix_tree
		local
			node1, node2: NODE
			par_node: NODE
		do
			from
			until
				available_nodes.count <= 1
			loop
				available_nodes := sort (available_nodes)
				node1 := available_nodes [1]
				node2 := available_nodes [2]
				available_nodes.go_i_th (1)
				available_nodes.remove
				available_nodes.remove
				par_node := node1 + node2
				if (node1 < node2) then
					par_node.left := node1
					par_node.right := node2
				else
					par_node.left := node2
					par_node.right := node1
				end
				node1.parent := par_node
				node2.parent := par_node
				available_nodes.extend (par_node)
			end
			prefix_tree := available_nodes [1]
		end

feature {NONE} --Merge sort for nodes made from merge sort from one of our last assignments

	sublist (list: ARRAYED_LIST [NODE]; start: INTEGER; finish: INTEGER): ARRAYED_LIST [NODE]
		require
			finish >= start
		local
			i: INTEGER
		do
			create Result.make (1)
			from
				i := start
			until
				i > finish
			loop
				Result.extend (list [i])
				i := i + 1
			end
		end

	merge (a: ARRAYED_LIST [NODE]; b: ARRAYED_LIST [NODE]): ARRAYED_LIST [NODE]
		local
			i, j: INTEGER
		do
			create Result.make (1)
			from
				i := 1
				j := 1
			until
				i > a.count or j > b.count
			loop
				if a [i] > b [j] then
					Result.extend (b [j])
					J := j + 1
				else
					Result.extend (a [i])
					i := i + 1
				end
			end
			if (j <= b.count) then
				from
				until
					j > b.count
				loop
					Result.extend (b [j])
					J := j + 1
				end
			else
				if (i <= a.count) then
					from
					until
						i > a.count
					loop
						Result.extend (a [i])
						i := i + 1
					end
				end
			end
		end

feature

	sort (v: ARRAYED_LIST [NODE]): ARRAYED_LIST [NODE]
		local
			a, b: ARRAYED_LIST [NODE]
			mid: INTEGER
		do
			create Result.make (1)
			if (v.count > 1) then
				mid := v.count // 2
				a := sublist (v, 1, mid)
				b := sublist (v, mid + 1, v.count)
				a := sort (a)
				b := sort (b)
				Result := merge (a, b)
			else
				Result := v.twin
			end
		end
			--End of the merge sort

end
