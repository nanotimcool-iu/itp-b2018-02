note
	description: "Summary description for {NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NODE

inherit

	COMPARABLE
		redefine
			three_way_comparison
		end

create
	make

feature

	name: STRING

	weight: INTEGER

	parent: detachable NODE assign set_parent

	left: detachable NODE assign set_left

	right: detachable NODE assign set_right

feature -- Comparison

	three_way_comparison (other: like Current): INTEGER
		do
			Result := weight.three_way_comparison (other.weight)
		end

	is_less alias "<" (other: like Current): BOOLEAN
		do
			Result := three_way_comparison (other) = -1
		end

feature

	make (c: STRING; w: INTEGER)
		do
			name := c
			weight := w
		end

feature

	add alias "+" (other: like Current): NODE
		do
			create Result.make (name + other.name, weight + other.weight)
		end

	set_parent (n: detachable NODE)
		require
			attached n
		do
			parent := n
		end

	set_right (n: detachable NODE)
		require
			attached n
		do
			right := n
		end

	set_left (n: detachable NODE)
		require
			attached n
		do
			left := n
		end

end
