note
	description: "Cipher application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			local
				vc:VIGENERE_CIPHER
				sc:SPIRAL_CIPHER
				cc:COMBINED_CIPHER
				cl:ARRAYED_LIST[CIPHER]
				s:STRING
			-- Run application.
		do
			--!!!ALL CIPHERS WORK ONLY WITH NON-CAPITAL LETTERS!!!
			create s.make (1)
			s:="STUDENTS, SOLVE THE ASSIGNMENT WELL AND FAST!"
			create vc.make
			create sc
			s:=s.as_lower
			print("%N"+s+"%N")
			s:=vc.encrypt (s)
			print (s + "%N")
			s:=sc.encrypt (s)
			print(s+"%N")
			s:=sc.decrypt (s)
			print(s+"%N")
			s:=vc.decrypt (s)
			print(s+"%N")

			create cl.make (1)
			cl.extend (vc)
			cl.extend (sc)
			create cc.make (cl)

			s:="STUDENTS, SOLVE THE ASSIGNMENT WELL AND FAST!"
			s:=s.as_lower
			print (s + "%N")
			s:=cc.encrypt (s)
			print (s + "%N")
			s:=cc.decrypt (s)
			print (s + "%N")

			s:="MYLASTASSIGNMENT"
			s:=s.as_lower
			vc.set_key ("busy")
			s:=cc.encrypt (s)

			vc.set_key (s)
			s:=cc.decrypt (("LAGHYH QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF   FAZ2/TGBZKFIREE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIULZIE KFYGEL//:RLH DXE Z"))
			print(s+"%N")
		end

end
