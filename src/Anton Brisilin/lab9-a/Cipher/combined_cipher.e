note
	description: "Summary description for {COMBINED_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMBINED_CIPHER

inherit
	CIPHER
create make
feature{NONE}
	ciph_list:ARRAYED_LIST[CIPHER]
feature
	make(list:ARRAYED_LIST[CIPHER])
	do
		ciph_list:=list.twin
	end
encrypt(s:STRING):STRING

		do
			create Result.make (1)
			Result:=s
			across ciph_list as cipher
			loop
				Result := cipher.item.encrypt (Result)
			end
		end

	decrypt(s:STRING):STRING
local
i:INTEGER
		do
			create Result.make (1)
			Result:=s
			from
				i:=ciph_list.count
			until
				i < 1
			loop
				Result := ciph_list[i].decrypt (Result)
				i:=i-1
			end
		end
end
