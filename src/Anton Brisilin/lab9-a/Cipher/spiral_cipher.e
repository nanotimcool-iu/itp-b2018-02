note
	description: "Summary description for {SPIRAL_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SPIRAL_CIPHER

inherit
	CIPHER
feature
    encrypt(str:STRING):STRING
    local
        spiral_matrix:ARRAY2[CHARACTER]
        i,j,c:INTEGER
        downs,lefts,rights,ups:INTEGER
    do
        create spiral_matrix.make_filled (' ', str.count.power (0.5).ceiling, str.count.power (0.5).ceiling)
        --create spiral_matrix.make_filled (a_default_value: G, nb_rows, nb_columns: INTEGER_32)
        from
        i:=1
        c:=1
        until
        i > spiral_matrix.height or c > str.count
        loop
            from
            j:=1
            until
            j>spiral_matrix.width or c > str.count
            loop
                spiral_matrix[i,j] := str[c]
                c:=c+1
                j:=j+1
            end
            i:=i+1
        end

        --DOWN
        --LEFT
        --UP
        --RIGHT

        --i,j - indexes
        from
        	downs:=0
        	ups:=0
        	lefts:=0
        	rights:=0
        	i:=1
        	j:=spiral_matrix.width
        	create Result.make (1)
        until
        	lefts + rights >= spiral_matrix.height and ups + downs >= spiral_matrix.width
     	loop
        	--down from rights to height-lefts (i,step +1)

        	from
        		i:=rights+1
        	until
        		i > (spiral_matrix.height - lefts)
        	loop
        		Result:=Result+spiral_matrix[i,j].out
        		i:=i+1
        	end
        	i:=i-1
        	downs:=downs+1

        	--left from width-downs BACK TO ups (j,step -1)

    		from
        		j:=spiral_matrix.width - downs
        	until
        		j < (ups+1)
        	loop
        		Result:=Result+spiral_matrix[i,j].out
        		j:=j-1
        	end
        	j:=j+1
        	lefts:=lefts+1

        	--up from height-lefts BACK TO rights (i,step -1)

        	from
        		i:=spiral_matrix.height-lefts
        	until
        		i < (rights+1)
        	loop
        		Result:=Result+spiral_matrix[i,j].out
        		i:=i-1
        	end
        	i:=i+1
        	ups:=ups+1

        	--right from ups to width-downs (j,step +1)

        	from
        		j:=ups+1
        	until
        		j > (spiral_matrix.width - downs)
        	loop
        		Result:=Result+spiral_matrix[i,j].out
        		j:=j+1
        	end
        	j:=j-1
        	rights:=rights+1
        end
    end
    decrypt(str:STRING):STRING
    local
    	spiral_matrix:ARRAY2[CHARACTER]
    	i,j,c:INTEGER
        downs,lefts,rights,ups:INTEGER

    do
    	create spiral_matrix.make_filled (' ', str.count.power (0.5).ceiling, str.count.power (0.5).ceiling)
    	from
        	downs:=0
        	ups:=0
        	lefts:=0
        	rights:=0
        	i:=1
        	j:=spiral_matrix.width
        	c:=1
        until
        	lefts + rights >= spiral_matrix.height and ups + downs >= spiral_matrix.width
     	loop
        	--down from rights to height-lefts (i,step +1)

        	from
        		i:=rights+1
        	until
        		i > (spiral_matrix.height - lefts)
        	loop
        		spiral_matrix[i,j] := str[c]
        		c:=c+1
        		i:=i+1
        	end
        	i:=i-1
        	downs:=downs+1

        	--left from width-downs BACK TO ups (j,step -1)

    		from
        		j:=spiral_matrix.width - downs
        	until
        		j < (ups+1)
        	loop
        		spiral_matrix[i,j] := str[c]
        		c:=c+1
        		j:=j-1
        	end
        	j:=j+1
        	lefts:=lefts+1

        	--up from height-lefts BACK TO rights (i,step -1)

        	from
        		i:=spiral_matrix.height-lefts
        	until
        		i < (rights+1)
        	loop
        		spiral_matrix[i,j] := str[c]
        		c:=c+1
        		i:=i-1
        	end
        	i:=i+1
        	ups:=ups+1

        	--right from ups to width-downs (j,step +1)

        	from
        		j:=ups+1
        	until
        		j > (spiral_matrix.width - downs)
        	loop
        		spiral_matrix[i,j] := str[c]
        		c:=c+1
        		j:=j+1
        	end
        	j:=j-1
        	rights:=rights+1
        end

        create Result.make (1)

        from
        i:=1
        c:=1
        until
        i > spiral_matrix.height
        loop
            from
            j:=1
            until
            j>spiral_matrix.width
            loop
                Result.append_character(spiral_matrix[i,j])
                j:=j+1
            end
            i:=i+1
        end

    end
end
