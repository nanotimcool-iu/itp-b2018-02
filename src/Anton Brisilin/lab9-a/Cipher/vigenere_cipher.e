note
	description: "Summary description for {VIGENERE_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHER

inherit

	CIPHER

create
	make

feature

	make
		local
			i: CHARACTER
		do
			key := "tiger"
			create chars.make (1)
			from
				i := 'a'
			until
				i = 'z' + 1
			loop
				chars.extend (i)
				i := i + 1
			end
		end

feature {NONE}

	key: STRING

	chars: ARRAYED_LIST [CHARACTER]

feature

	set_key (k: STRING)
		do
			key := k
			key:=key.as_lower
		end

	encrypt (str: STRING): STRING
		local
			i: INTEGER
			sstr:STRING
		do
			sstr:=str.as_lower
			i := 1
			Result := str.twin
			across
				str as s
			loop
				if s.item.is_alpha then
					Result [s.cursor_index] := chars [((s.item.code - ('a').code + (key [i].code - ('a').code))) \\ 26 + 1]
					i := i + 1
					if i > key.count then
						i := 1
					end
				end
			end
		end

	decrypt (str: STRING): STRING
		local
			i: INTEGER
			tmp: INTEGER
			sstr:STRING
		do
			sstr:=str.as_lower
			i := 1
			Result := str.twin
			across
				sstr as s
			loop
				if s.item.is_alpha then
					tmp := ((s.item.code - ('a').code - (key [i].code - ('a').code)))
					if tmp < 0 then
						tmp := tmp + 26
					end
					tmp := tmp + 1
					Result [s.cursor_index] := chars [tmp]
					i := i + 1
					if i > key.count then
						i := 1
					end
				end
			end
		end

end
