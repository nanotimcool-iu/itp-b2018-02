note
	description: "Summary description for {BOOK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOOK
create register
feature
	name:STRING
	minimal_age:NATURAL
	is_bestseller:BOOLEAN
	due_to:detachable DATE_TIME
	value:NATURAL
feature
	register(book_name:STRING;min_age:NATURAL;bestseller:BOOLEAN)
	do
		name:=book_name
		minimal_age:=min_age
		is_bestseller:=bestseller
	end
	is_overdue(time:DATE_TIME):BOOLEAN
	do
		if not attached due_to as d_t then
			Result:=false
			else
			Result:= time < d_t
		end
	end
	get_overdue(time:DATE_TIME):NATURAL
	local
		overdue:DATE_TIME_DURATION
		i:INTEGER
	do
		if not is_overdue(time) then
			Result := 0
		else
			if attached due_to as due then


			overdue:= (time.definite_duration (due))
			Result:= overdue.year.as_natural_32 * 365
			from
				i:=1
			until
				i=overdue.month
			loop
				Result:=Result+ overdue.days_in_i_th_month(i,overdue.year).as_natural_32
				i:=i+1
			end

			Result:=Result+due.day.as_natural_32
		end
		end
	end
end
