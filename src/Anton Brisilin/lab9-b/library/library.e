note
	description: "Summary description for {LIBRARY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LIBRARY

create
	make

feature

	make
		do
			create readers.make (1)
			create books.make (1)
			readers_number := 0
			create current_date.make_now
		end

	set_date (new_date: DATE_TIME)
		do
			current_date := new_date
		end

	register_new_reader (person_name: STRING; person_age: NATURAL; person_phone: STRING; person_address: STRING;)
		local
			new: PERSON_CARD
		do
			create new.register (person_name, person_age, person_phone, person_address, readers_number + 1)
			readers_number := readers_number + 1
		end

	add_new_book(book_name:STRING;min_age:NATURAL;bestseller:BOOLEAN)
	local
		tmp:BOOK
	do
		create tmp.register (book_name, min_age, bestseller)
		books.extend (tmp)
	end

	get_overdue (reader: PERSON_CARD): NATURAL
		local
			max_overdue: NATURAL
		do
			max_overdue := 0
			across
				reader.token_books as book
			loop
				if (book.item.get_overdue (current_date) > max_overdue) then
					max_overdue := book.item.get_overdue (current_date)
				end
			end
			Result := max_overdue
		end

	get_fine (reader: PERSON_CARD): NATURAL
		local
			sum_fine: NATURAL
		do
			sum_fine := 0
			across
				reader.token_books as book
			loop
				if book.item.get_overdue (current_date) > 21 then
					sum_fine := sum_fine + (book.item.get_overdue (current_date) * 100).min (book.item.value)
				end
			end
			Result := sum_fine
		end

	give_book (reader: PERSON_CARD; book: STRING)
		require
			can_take: reader.age > 12 implies reader.token_books.count < 5
			book_presented: is_name_presented_in_books (book)
			valid_age: reader.age >= get_book (book).minimal_age
		do
			reader.add_book (books [get_book_number (book)])
			books.prune (books [get_book_number (book)])
		end

	return_book (reader: PERSON_CARD; book: BOOK)
		require
			is_really_book: attached book
			is_taken_book: reader.token_books.has (book)
		do
			books.extend (book)
			reader.token_books.prune (book)
		end

	is_name_presented_in_books (name: STRING): BOOLEAN
		do
			Result := false
			across
				books as book
			loop
				if (book.item.name ~ name) then
					Result := true
				end
			end
		end

	get_book_number (name: STRING): INTEGER
		do
			Result := 0
			books.go_i_th (1)
			across
				books as book
			loop
				if (book.item.name ~ name) then
					Result := book.cursor_index
				end
			end
		end
	get_book(name:STRING):BOOK
	do
		Result := books [get_book_number (name)]
	end
feature {NONE}

	readers: ARRAYED_LIST [PERSON_CARD]

	books: ARRAYED_LIST [BOOK]

	readers_number: INTEGER

	current_date: DATE_TIME

feature {LIBRARY}

	get_sum_overdue (reader: PERSON_CARD): NATURAL
		local
			sum_overdue: NATURAL
		do
			sum_overdue := 0
			across
				reader.token_books as book
			loop
				sum_overdue := sum_overdue + book.item.get_overdue (current_date)
			end
			Result := sum_overdue
		end

end
