note
	description: "Summary description for {PERSON_CARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON_CARD
create
	register
feature
	name:STRING
	age:NATURAL
	address:STRING
	phone_number:STRING
	token_books:ARRAYED_LIST[BOOK]
	ID:INTEGER
feature{LIBRARY}
	register(person_name:STRING;person_age:NATURAL;person_phone:STRING;person_address:STRING;new_ID:INTEGER)
	do
		name:=person_name
		age:=person_age
		address:=person_address
		phone_number:=person_phone
		ID:=new_id
		create token_books.make (1)
	end
	add_book(book:BOOK)
	do
		token_books.extend (book)
	end
feature

end
