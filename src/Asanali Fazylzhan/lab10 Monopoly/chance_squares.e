note
	description: "Summary description for {CHANCE_SQUARES}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CHANCE_SQUARES

inherit

	SQUARE
		redefine
			land
		end

feature {NONE}

	last_earning: INTEGER

	max_value: INTEGER = 200

feature

	land (p: PLAYER)
		local
			rd: RANDOMIZER
		do
			create rd.make
			last_earning := rd.get_number_in_range (max_value)
			p.earn (last_earning)
		end

	describe

		do
			print ("%NChance: you earned " + last_earning.out)
		end

end
