note
	description: "Summary description for {DICE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DICE

inherit

	RANDOMIZER

create
	make

feature

	throw: INTEGER
		do
			print ("%NDice throws...")
			Result := get_number_in_range (7)
			print ("%NYou get: " + Result.out)
		end

end
