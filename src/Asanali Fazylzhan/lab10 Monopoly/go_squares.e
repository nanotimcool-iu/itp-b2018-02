note
	description: "Summary description for {GO_SQUARES}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GO_SQUARES

inherit

	SQUARE
		redefine
			go_through, land
		end

feature {NONE}

	earning: INTEGER = 200

feature

	go_through (p: PLAYER)
		do
			p.earn (earning)
			describe
		end

	land(p: PLAYER)
		do
			p.earn (earning)
		end

feature

	describe
		do
			print ("%N'Go' cell: you earned " + earning.out)
		end

end
