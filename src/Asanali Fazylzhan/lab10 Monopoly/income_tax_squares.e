note
	description: "Summary description for {INCOME_TAX_SQUARES}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	INCOME_TAX_SQUARES

inherit

	SQUARE
		redefine
			land
		end

feature

	land (p: PLAYER)
		local
			lost: REAL_64
		do
			lost := p.get_money * 0.1
			p.pay (lost.rounded)
		ensure then
			old p.get_money > p.get_money
		end

feature

	describe
		do
			print ("%NIncome tax: you lost 10%% of your money")
		end

end
