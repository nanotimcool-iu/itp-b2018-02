note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create
	make

feature

	name: STRING

feature {GAME}

	position: INTEGER

	lose: BOOLEAN

	money: INTEGER


feature {GAME, IN_JAIL_SQUARES}

	in_jail: BOOLEAN

feature {NONE}

	board: GAME_BOARD

	default_sum: INTEGER = 1500

	properties: ARRAY [PROPERTY_SQUARES]

	days_in_jail: INTEGER

feature {IN_JAIL_SQUARES}

	stay_in_jail (stay: BOOLEAN)
		require
			in_jail
		do
			if stay then
				days_in_jail := days_in_jail + 1
			else
				days_in_jail := 0
				in_jail := False
			end
		ensure
			in_jail implies stay
		end

feature

	make (player_name: STRING; new_board: GAME_BOARD)
		do
			name := player_name
			money := default_sum
			position := 1
			create properties.make_empty
			lose := False
			in_jail := False
			days_in_jail := 0
			board := new_board
		ensure
			name = player_name
			money = default_sum
			position = 1
			in_jail or lose = False
			days_in_jail = 0
			board.size = new_board.size
		end

feature {GO_TO_JAIL_SQUARES, GAME, IN_JAIL_SQUARES}

	move_on (steps: INTEGER)
		local
			steps_done: INTEGER
		do
			from
				steps_done := 1
			until
				steps_done > steps
			loop
				move_to (((position + 1) \\ board.size) + 1)
				board.at (position).go_through (Current)
				steps_done := steps_done + 1
			end
			print("%NYour position is " + position.out)
			board.at (position).describe
			board.at (position).land (Current)
		end

	move_to (new_position: INTEGER)
		require
			0 <= new_position
		do
			position := new_position
		ensure
			position = new_position
		end

feature {INCOME_TAX_SQUARES}

	get_money: INTEGER
		do
			Result := money
		ensure
			Result = money
		end

feature {GO_TO_JAIL_SQUARES}

	send_to_a_jail
		do
			in_jail := True
		end

feature {SQUARE}

	add_property (new_property: PROPERTY_SQUARES)
		require
			not new_property.is_owned
		do
			properties.force (new_property, properties.count + 1)
		ensure
			properties.has (new_property)
		end

	pay (sum: INTEGER)
		require
			sum > 0
		do
			change_money (- sum)
			if money < 0 then
				lose := True
				across
					properties as property
				loop
					property.item.remove_owner
				end
				print ("%N" + name + ", you lose :(")
			end
		ensure
			old money = money + sum
		end

	earn (sum: INTEGER)
		require
			sum >= 0
		do
			change_money (sum)
		ensure
			old money + sum = money
		end

feature {NONE}

	change_money (on: INTEGER)
		do
			money := money + on
		ensure
			money = old money + on
		end

invariant
	lose implies money > 0
	days_in_jail < 4

end
