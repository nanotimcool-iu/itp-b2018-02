note
	description: "Summary description for {PROPERTY_SQUARES}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROPERTY_SQUARES

inherit

	SQUARE
		redefine
			land
		end

create
	make

feature {NONE}

	position: INTEGER

	name: STRING

	price: INTEGER

	rent: INTEGER

	owner: detachable PLAYER

feature

	is_owned: BOOLEAN

	make (new_position, new_price, new_rent: INTEGER; cell_name: STRING)
		do
			position := new_position
			price := new_price
			rent := new_rent
			name := cell_name
			is_owned := False
		ensure
			position = new_position
			price = new_price
			rent = new_rent
			name = cell_name
			is_owned = False
		end

	land (p: PLAYER)
		do
			if (not is_owned) and request_the_decision then
				buy (p)
			elseif is_owned then
				pay_rent (p)
			end
		end

feature {NONE}

	request_the_decision: BOOLEAN
		do
			print ("%NWould you like to by it?%N(Y/N)")
			io.read_character
			Result := io.last_character.is_equal ('Y')
		end

	buy (p: PLAYER)
		do
			p.add_property (Current)
			is_owned := True
			owner := p
			p.pay (price)
		end

	pay_rent (p: PLAYER)
		do
			p.pay (rent)
			if attached owner as own then
				own.earn (rent)
			end
		end

feature {PLAYER}

	remove_owner
		do
			owner := Void
			is_owned := False
		ensure
			not is_owned
			owner = Void
		end

feature

	describe
		do
			print ("%NProperty: " + name + "%Nits price: " + price.out + "%Nits rent: " + rent.out)
			if is_owned then
				if attached owner as own then
					print ("%NIts owner is: " + own.name)
				else
					print ("%NNobody owns it: ")
				end
			end
		end

end
