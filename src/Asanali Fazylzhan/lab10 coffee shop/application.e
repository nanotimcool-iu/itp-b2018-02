note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE}

	products:ARRAY[PRODUCT]

	make
	local
		es:ESPRESSO
		cap:CAPPUCCINO
		cak:CAKE
		do
			create products.make_empty
			create products_amount.make_filled (0, 1, 3)
			create es.create_espresso(5,7)
			create cap.create_cappuccino(5,7)
			create cak.create_cake(5,7)
			products.put (es,1)
			products.put(cap,2)
			products.put(cak,3)
			products_amount.item(1):=5
			products_amount.item(2):=5
			products_amount.item(3):=3
			print_menu
		end

	products_amount:ARRAY[INTEGER]

	profit:REAL
	do
		Result:=Result+(products.item (1).price_public-products.item (1).price)*products_amount.item (1)
		Result:=Result+(products.item (2).price_public-products.item (2).price)*products_amount.item (2)
		Result:=Result+(products.item (3).price_public-products.item (3).price)*products_amount.item (3)
	end

	print_menu
	do
		Io.put_string ("Espresso: ")
		Io.put_string (products.item (1).description)
		Io.new_line
		Io.put_string ("Cappuccino: ")
		Io.put_string (products.item (2).description)
		Io.new_line
		Io.put_string ("Cake ")
		Io.put_string (products.item (3).description)
		Io.new_line
	end

end
