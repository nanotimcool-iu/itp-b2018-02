note
	description: "Summary description for {CAKE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAKE

inherit
	PRODUCT

create
	create_cake

feature
	create_cake(p,pb:REAL)
	require
		profitable:pb>p
	do
		price:=p
		price_public:=pb
		description:="delicious dessert"
	end

	set_price(p:REAL)
	do
		price:=p
	end

	set_price_public(p:REAL)
	do
		price_public:=p
	end
end
