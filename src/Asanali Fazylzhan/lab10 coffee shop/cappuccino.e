note
	description: "Summary description for {CAPPUCCINO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAPPUCCINO

inherit
	COFFEE

create
	create_cappuccino

feature

	create_cappuccino(p,pb:REAL)
	require
		profitable:pb>p
	do
		price:=p
		price_public:=pb
		description:="coffee with milk"
	end

	set_price(p:REAL)
	do
		price:=p
	end

	set_price_public(p:REAL)
	do
		price_public:=p
	end

end
