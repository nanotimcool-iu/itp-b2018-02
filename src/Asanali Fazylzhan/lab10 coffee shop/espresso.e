note
	description: "Summary description for {ESPRESSO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ESPRESSO

inherit
	COFFEE

create
	create_espresso

feature

	create_espresso(p,pb:REAL)
	require
		profitable:pb>p
	do
		price:=p
		price_public:=pb
		description:="strong coffee"
	end

	set_price(p:REAL)
	do
		price:=p
	end

	set_price_public(p:REAL)
	do
		price_public:=p
	end

end
