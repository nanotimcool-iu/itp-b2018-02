note
	description: "Summary description for {PRODUCT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PRODUCT

feature

	description:STRING
	price:REAL
	price_public:REAL

	set_price(p:REAL)
	deferred
	end

	set_price_public(p:REAL)
	deferred
	end

	invariant
		price<price_public
end
