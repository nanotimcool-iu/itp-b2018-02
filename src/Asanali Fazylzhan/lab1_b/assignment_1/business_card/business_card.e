class
	BUSINESS_CARD

create
	fill_in

feature {NONE} -- Initialization

	fill_in
			-- Fill in the card and print it.
		do
			Io.put_string ("Your name:")
			Io.read_line
			set_name(Io.last_string)
			Io.new_line
			Io.put_string ("Your job:")
			Io.read_line
			set_job(Io.last_string)
			Io.new_line
			Io.put_string ("Your age:")
			Io.read_integer
			set_age(Io.last_integer)
			Io.new_line
			print_card()
		end

feature -- Access

	name: STRING
			-- Owner's name.

	job: STRING
			-- Owner's job.

	age: INTEGER
			-- Owner's age.

feature -- Setting

	set_name (a_name: STRING)
			-- Set `name' to `a_name'.
		require
			name_exists: a_name /= Void
		do
			name := a_name.twin
		end

	set_job (a_job: STRING)
			-- Set `job' to `a_job'.
		require
			job_exists: a_job /= Void
		do
			job := a_job.twin
		end

	set_age (a_age: INTEGER)
			-- Set `age' to `a_age'.
		require
			age_non_negative: a_age >= 0
		do
			age := a_age
		end

feature -- Output
	print_card()
		require
		do
			Io.put_string (line(Width))
			Io.new_line
			Io.put_string ("|")
			Io.put_string (name)
			Io.put_string(spaces(Width-name.count))
			Io.put_string ("|")
			Io.new_line
			Io.new_line
			Io.put_string ("|")
			Io.put_string (job)
			Io.put_string(spaces(Width-job.count))
			Io.put_string ("|")
			Io.new_line
			Io.new_line
			Io.put_string ("|")
			Io.put_string (age_info)
			Io.put_string(spaces(Width-age_info.count))
			Io.put_string ("|")
			Io.new_line
			Io.put_string (line(Width))
		end
	age_info: STRING
			-- Text representation of age on the card.
		do
			Result := age.out + " years old"
		end

	Width: INTEGER = 50
			-- Width of the card (in characters), excluding borders.

	line (n: INTEGER): STRING
			-- Horizontal line on length `n'.
		do
			Result := "-"
			Result.multiply (n)
		end
	spaces (n:INTEGER):STRING
		do
			Result:=" "
			Result.multiply (n)
		end
end
