class
	APPLICATION

create
	execute

feature {NONE} -- Initialization

	execute
			-- Run application.
		do
			Io.put_string ("Name:Asanali")
			Io.new_line
			Io.put_string ("Age:")
			Io.put_integer (18)
			Io.new_line
			Io.put_string ("Mother language: Kazakh")
			Io.new_line
			Io.put_string ("Has a cat:")
			Io.put_boolean (true)
			Io.new_line
		end

end
