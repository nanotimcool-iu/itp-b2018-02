note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		do
			Zurich.add_public_transport (24)
			Zurich.add_public_transport (10)
			Zurich.add_public_transport (5)
			Zurich.add_public_transport (7)
			Zurich.add_station ("station2", 50, 50)
			Zurich_map.update
			Zurich_map.station_view (Zurich.station ("station2")).highlight
			wait(3)
			Zurich_map.station_view (Zurich.station ("station2")).unhighlight
			Zurich_map.animate
		end

end
