note
	description: "Summary description for {BANK_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

create
	make

feature

	name:STRING
	balance:INTEGER

	make(n:STRING; bal:INTEGER)

	do
		name:=n
		balance:=bal
	end

	depos(am:INTEGER)

	do
		if (balance+am)<1000000 then
			balance:=balance+am
		end
	end

	withdraw(am:INTEGER)

	do
		if (balance-am)>100 then
			balance:=balance-am
		end
	end

end
