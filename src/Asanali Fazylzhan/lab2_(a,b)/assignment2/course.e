note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

create
	create_class

feature

	name:STRING
	id:STRING
	schedule:STRING
	max_st:INTEGER

	create_class(n:STRING;i:STRING;sch:STRING;mx: INTEGER)
	do
		name:=n
		id:=i
		schedule:=sch
		max_st:=mx
	end

end
