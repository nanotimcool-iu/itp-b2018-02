note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

create
	create_calendar

feature
	create_entry(o:PERSON; s:STRING; p:STRING;d:TIME):CALENDAR_ENTRY
	local
		a:CALENDAR_ENTRY

	do
		create a.create_calentr (o, s, p, d)
		Result:=a
	end

	create_calendar
	do

	end

	edit_subject(e:CALENDAR_ENTRY; new_subject:STRING)

	do
		e.set_subject(new_subject)
	end

	edit_date (e: CALENDAR_ENTRY; new_date: TIME)

	do
		e.set_date(new_date)
	end

	get_owner_name (e: CALENDAR_ENTRY): STRING

	do
		Result:=e.owner.name
	end

	in_conflict (e1, e2: CALENDAR_ENTRY): BOOLEAN

	do
		if e1.date=e2.date then
			result:=true

		else result:=false

		end
	end

end
