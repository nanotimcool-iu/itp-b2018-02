note
	description: "Summary description for {CALENDAR_ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR_ENTRY

create
	create_calentr

feature
	owner:PERSON
	subject:STRING
	place:STRING
	date:TIME

	set_owner(o:PERSON)

	do
		owner:=o
	end

	set_subject(s:STRING)

	do
		subject:=s
	end

	set_place(p:STRING)
	do
		place:=p
	end

	set_date(d:TIME)
	do
		date:=d
	end

	create_calentr(o:PERSON; s:STRING; p:STRING;d:TIME)

	do
		owner:=o
		subject:=s
		place:=p
		date:=d

	end

end
