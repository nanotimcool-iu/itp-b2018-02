note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	create_person

feature

	name:STRING
	phone_number:INTEGER
	work_place:STRING
	email:STRING

	create_person(n:STRING; ph:INTEGER; work_pl:STRING; em:STRING)
	do
		name:=n
		phone_number:=ph
		work_place:=work_pl
		email:=em
	end

end
