note
	description: "contact application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			a:CMS
			c1:CONTACT
			c2:CONTACT
			c3:CONTACT
		do
			create a.create_cms
			c1:=a.create_contact ("Asan", 1234,"Gazgolder1", "mail1@gmail.com",0)
			Io.put_string (c1.name)
			Io.new_line
			c2:=a.create_contact ("Zhan", 1254,"Gazgolder2", "mail2@gmail.com",0)
			Io.put_string (c2.name)
			Io.new_line
			c3:=a.create_contact ("San", 123454,"Gazgolder3", "mail3@gmail.com", 0)
			Io.put_string (c3.name)
			Io.new_line
			a.add_emergency_contact (c1, c2)
			Io.put_integer (c1.emergency_contact)
			Io.new_line
			a.add_emergency_contact (c2, c3)
			Io.put_integer (c2.emergency_contact)
			Io.new_line
			a.remove_emergency_contact (c1)
			Io.put_integer (c1.emergency_contact)
			Io.new_line
		end

end
