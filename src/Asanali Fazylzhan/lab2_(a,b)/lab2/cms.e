note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

create
	create_cms

feature

	create_cms
	do

	end

	create_contact(n:STRING; phone_n:INTEGER; work:STRING; mail: STRING; emnum:INTEGER):CONTACT
	local
		n1:CONTACT
	do
		create n1.create_con(n,phone_n,work,mail,emnum)
		Result:=n1
	end

	edit_contact(c:CONTACT; n:STRING; phone_n:INTEGER; work:STRING; mail: STRING; emnum:INTEGER)
	do
		c.set_name(n)
		c.set_phone(phone_n)
		c.set_work(work)
		c.set_mail(mail)
		c.set_emergency(emnum)
	end

	add_emergency_contact (c1: CONTACT; c2: CONTACT)
	do
		c1.set_emergency(c2.phone_number)
	end

	remove_emergency_contact (c: CONTACT)
	do
		c.set_emergency(0)
	end

end
