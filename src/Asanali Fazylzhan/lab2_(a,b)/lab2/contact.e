note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

create
	create_con

feature

	name: STRING
	phone_number: INTEGER
	work_place: STRING
	email: STRING
	emergency_contact: INTEGER

	create_con(n:STRING; phone_n:INTEGER; work:STRING; mail: STRING; emergency: INTEGER)
	do
		name:=n
		phone_number:=phone_n
		work_place:=work
		email:=mail
		emergency_contact:=emergency
	end

	set_name(n:STRING)
	do
		name:=n
	end

	set_work(n:STRING)
	do
		work_place:=n
	end

	set_mail(n:STRING)
	do
		email:=n
	end

	set_emergency(n:INTEGER)
	do
		emergency_contact:=n
	end

	set_phone(n:INTEGER)
	do
		phone_number:=n
	end

end
