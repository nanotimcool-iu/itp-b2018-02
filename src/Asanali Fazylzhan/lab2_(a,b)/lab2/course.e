note
	description: "university application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

inherit
	ARGUMENTS_32

create
	make


feature {NONE} -- Initialization

	make
			-- Run application.
		do
			name:="Intro in prgrm"
			id:="1802"
			schedule:="som schedule"
			max_students:=150
		end
	creat_class(n:STRING; i:STRING; sch:STRING; mx:INTEGER)
		do
			name:=n
			id:=i
			schedule:=sch
			max_students:=mx
		end

	name: STRING

	id: STRING

	schedule: STRING

	max_students: INTEGER
end
