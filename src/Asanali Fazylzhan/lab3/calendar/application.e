note
	description: "calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			calendar1 : CALENDAR
			time1 : TIME
			person1 : PERSON
			entry1: ENTRY
			s: STRING
		do
			create person1.create_person ("Person", 5657 , "Innopolis","p1@mail.com")

			create calendar1.create_calendar

			create time1.make(12,0,0)

			create entry1.create_entry(time1,person1, "coding", "reading hall")

			calendar1.add_entry (entry1, 10)
			calendar1.set_subject (entry1, "reading")
			s := calendar1.outp_month
			io.put_string (s)
		end

end
