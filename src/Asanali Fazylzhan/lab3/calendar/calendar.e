note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

create
	create_calendar

feature
	month: ARRAY [DAY]

	create_calendar
	local
		day1 : DAY
	do

		create day1.create_day (1)
		create month.make_filled (day1, 1, 30)

	end


	add_entry (entry0:ENTRY; day0: INTEGER)
	local
		day1:DAY
	do
		create day1.create_day (day0)
		month[day0] := day1
		month[day0].add_entry (entry0)
	end

	set_subject (entry1: ENTRY; subject1: STRING)
	do
		entry1.set_subject(subject1)
	end

	set_date (entry1: ENTRY; date1: TIME)
	do
		entry1.set_date(date1)
	end

	get_owner_name (entry1: ENTRY): STRING
	do
		Result := entry1.owner.name
	end
--	in_conflict (day: INTEGER): LIST [ENTRY]
--	do

--	end
	outp_month: STRING
	local
		i:INTEGER
		j:INTEGER
		s:STRING
	do
		s:=""
		from
		i:=1
		until
			i>30
		loop
			s:=s+i.out+" "
			from
				j:=1
			until
				j>month[i].entries.count
			loop
				s:=s+month[i].entries[j].subject+" "
				j:=j+1
			end
			i:=i+1
		end
		Result:=s
	end

end
