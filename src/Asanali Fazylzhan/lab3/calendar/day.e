note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	create_day

feature
	date:INTEGER
	entries:LINKED_LIST[ENTRY]

	create_day(d:INTEGER)
	do
		date:=d
		create entries.make
	end

	add_entry(entry1:ENTRY)
	do
		entries.extend(entry1)
	end

end
