note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	create_entry

feature
		owner:PERSON
		subject:STRING
		place:STRING
		date:TIME

		create_entry(date1: TIME; owner1: PERSON; subject1: STRING; place1: STRING)
		do
			date := date1
			owner := owner1
			subject := subject1
			place := place1
		end

		set_subject(subject1: STRING)
		do
			subject := subject1
		end

		set_date(d : TIME)
		do
			date := d
		end

end
