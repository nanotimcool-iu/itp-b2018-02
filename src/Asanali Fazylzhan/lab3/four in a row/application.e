note
	description: "four application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	grid: ARRAY_GRID
	full: ARRAY [INTEGER]
	player,w,h: INTEGER

	make
		local
			tr: BOOLEAN
			col_ch, id_ch: INTEGER
		do
			io.put_string ("Game began!")
			io.new_line
			io.put_string ("input width of the grid:")
			io.read_integer
			w:=io.last_integer
			io.new_line
			io.put_string ("input height of the grid:")
			io.read_integer
			h:=io.last_integer
			create grid.create_grid (w, h, "o")
			create full.make_filled (0, 0,w-1)
			grid.outp_grid
			player:=1
			from
				tr:= FALSE
			until
				tr=TRUE
			loop
				print ("Player number " + player.out + "%N")
				io.read_integer
				col_ch := io.last_integer
				id_ch:=full.at (col_ch)
				full[col_ch]:=full[col_ch] + 1
				if player>0 then
					grid.set (col_ch, id_ch, "a")
				else
					grid.set (col_ch, id_ch, "b")
				end
				tr:=check_win (col_ch, id_ch)
				grid.outp_grid
				player:=player*(-1)
			end
			Io.new_line
			print ("Player "+player.out+"won")
		end

		check_win (c, i: INTEGER): BOOLEAN
		local
			diag, col, row: BOOLEAN
		do
			diag:=(diagonal_check (c, i, 1) + diagonal_check (c, i, -1) > 4)
			col:=(column_check(c, i, 1) + column_check (c, i, -1) > 4)
			row:=(row_check(c, i, 1) + row_check (c, i, -1) > 4)
			Result:=diag or col or row
		end

		row_check (x1, y1, step: INTEGER): INTEGER
		local
			array_str: ARRAY [STRING]
			i: INTEGER
			stop: BOOLEAN
			sign: STRING
		do
			Result:=0
			create array_str.make_empty
			array_str := grid.get_row (y1)
			sign:=grid[x1, y1]
			from
				i:=x1
			until
				i>=array_str.count or stop or i<0
			loop
				if array_str.at(i).is_equal(sign) then
					Result:=Result + 1
				else
					stop := TRUE
				end
				i := i + step
			end
		end

		column_check (x1, y1,step: INTEGER): INTEGER
		local
			array_str: ARRAY [STRING]
			i: INTEGER
			stop: BOOLEAN
			sign: STRING
		do
			Result:=0
			create array_str.make_empty
			array_str:=grid.get_column(x1)
			sign:=grid[x1, y1]
			from
				i:=y1
			until
				i>=array_str.count or stop or i<0
			loop
				if array_str.at(i).is_equal(sign) then
					Result:=Result+1
				else
					stop:=TRUE
				end
				i:=i+step
			end
		end

		diagonal_check(x1, y1, step: INTEGER): INTEGER
		local
			i,j: INTEGER
			stop: BOOLEAN
			sign: STRING
		do
			Result:=0
			stop:=FALSE
			sign:=grid[i, j]
			from
				i:=x1
				j:=y1
			until
				i>=grid.w or stop or i<0 or j<0 or j>=grid.h
			loop
				if grid[i, j].is_equal(sign) then
					Result:=Result+1
				else
					stop:=TRUE
				end
				i:=i+step
				j:=j+step
			end
		end

end
