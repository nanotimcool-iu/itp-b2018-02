note
	description: "Summary description for {ARRAY_GRID}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ARRAY_GRID

create
	create_grid

feature
	arr: ARRAY [ARRAY [STRING]]
	w:INTEGER
	h: INTEGER

	create_grid (w1:INTEGER; h1: INTEGER; inp: STRING)
		local
			sub: ARRAY [STRING]
			i: INTEGER
		do
			w := w1
			h := h1
			create sub.make_filled (" ", 0, w - 1)
			create arr.make_filled (sub, 0, h - 1)
			from
				i := 0
			until
				i = h
			loop
				create sub.make_filled (inp, 0, w - 1)
				arr[i] := sub
				i := i + 1
			end
		end

	outp_grid
		local
			out_str: STRING
		do
			out_str := ""
			across
				arr as a
			loop
				across
					a.item as one
				loop
					out_str := out_str + one.item + " "
				end
				out_str := out_str + "%N"
			end
			Io.put_string (out_str)
		end

feature

	get_row (y: INTEGER): ARRAY [STRING]
		do
			Result := arr [y]
		end

	get_column (x: INTEGER): ARRAY [STRING]
		local
			i: INTEGER
		do
			create Result.make_filled (" ", 0, h - 1)
			from
				i:=0
			until
				i>=h
			loop
				Result [i]:=arr [i].at (x)
				i:=i+1
			end
		end

feature
	set (x, y: INTEGER; v: STRING)
		do
			arr [y].at (x):=v
		end

	at alias "[]" (x, y: INTEGER): STRING
		do
			Result := arr[y].at (x)
		end

end
