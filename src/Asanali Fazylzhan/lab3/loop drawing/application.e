note
	description: "loop_drawing application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization
	sp: BOOLEAN
	num, maxl, n, elems: INTEGER
	grid: ARRAYED_LIST[STRING]
	line: STRING

	make
	local
	i,j:INTEGER
	do
		Io.read_integer
		num:=Io.last_integer
		create grid.make (num)
		create line.make_empty
		n:=1
		sp:=False
		elems:=1

		from
			i:=0
		until
			i>=num
		loop
			line:=""
			if i\\2=1 then
			line:=" "
			end

			from
				j:=0
			until
				j>=elems
			loop
				line:=line+n.out+" "
				n:=n+1
				j:=j+1
			end

			if sp then
				elems:=elems+1
			end
			sp:=not sp
			grid.extend(line)
			i:=i+1
		end

		maxl:=grid.last.count

		from
			i:=num
		until
			i<1
		loop
			from
				j:=grid[i].count
			until
				j>=maxl
			loop
				grid[i]:=grid[i]+"  "
				j:=j+1
			end
			i:=i-1
		end

		n:=1
		sp:=False
		elems:=1

		from
			i:=0
		until
			i>=num
		loop
			line:=grid[i+1]
			n:=n+elems
			from
				j:=0
			until
				j>=elems
			loop
				n:=n-1
				line:=line+(n).out+" "
				j:=j+1
			end
			n:=n+elems

			if sp then
				elems:=elems+1
			end
			sp:=not sp
			grid[i+1]:=line
			i:=i+1
		end

		across grid as gr
		loop
			print(gr.item)
			io.new_line
		end
	end

end
