note
	description: "matrices application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
	local
		a1,a2,a3:ARRAY2[INTEGER]
		op:MATRIX_OPER
		i,j,n:INTEGER
		do
			create a1.make_filled (7, 3, 3)
			create a2.make_filled (5, 3, 3)
			create op.create_oper
			a3:=op.prod (a1, a2)
			n:=op.det(a1)
			from
				i:=1
			until
				i>a3.width
			loop
				from
					j:=1
				until
					j>a3.height
				loop
					Io.put_integer(a3.item (i,j))
					Io.put_string (" ")
					j:=j+1
				end
				Io.new_line
				i:=i+1
			end
			Io.new_line
			Io.put_integer (n)

		end



end
