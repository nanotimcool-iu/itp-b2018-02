note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER

create
	create_oper

feature
	create_oper
	do

	end

	add(a1:ARRAY2[INTEGER];a2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		arrs_equal:a1.upper=a2.upper and a1.lower=a2.lower

	local
		i,j:INTEGER
		a0:ARRAY2[INTEGER]
	do
		create a0.make_filled (0, a1.height, a1.width)
		from
			i:=1
		until
			i>a1.width
		loop
			from
				j:=1
			until
				j>a1.height
			loop
				a0.item(i,j):=a1.item (i,j)+a2.item (i,j)
				j:=j+1
			end
			i:=i+1
		end
		Result:=a0
		ensure
			arrs_equal:a1.upper=a2.upper and a1.lower=a2.lower
	end



	minus(a1:ARRAY2[INTEGER];a2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		arrs_equal:a1.upper=a2.upper and a1.lower=a2.lower

	local
		i,j:INTEGER
		a0:ARRAY2[INTEGER]
	do
		create a0.make_filled (0, a1.height, a1.width)
		from
			i:=1
		until
			i>a1.width
		loop
			from
				j:=1
			until
				j>a1.height
			loop
				a0.item(i,j):=a1.item (i,j)-a2.item (i,j)
				j:=j+1
			end
			i:=i+1
		end
		Result:=a0
		ensure
			arrs_equal:a1.upper=a2.upper and a1.lower=a2.lower
	end

	prod(a1,a2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		width_height: a1.width=a2.height
	local
		i,j,k,val:INTEGER
		a0:ARRAY2[INTEGER]
	do
		create a0.make_filled (0, a1.height, a2.width)
		from
			i:=1
		until
			i>a1.height
		loop
			from
				j:=1
			until
				j>a2.height
			loop
				val:=val+a1[i,j]*a2[j,i]
				j:=j+1
			end
			from
				k:=1
			until
				k>a2.height
			loop
				a0.put(val,i,k)
				k:=k+1
			end
			i:=i+1
			val:=0
		end
		Result:=a0
		ensure
			width_height: a1.width=a2.height
	end

	det(a1:ARRAY2[INTEGER]):INTEGER
		require
			square:a1.width=a1.height

		do
			if a1.height=2 then
				Result:=a1.item (1,1)*a1.item (2,2)-a1.item (1,2)*a1.item (2,1)
			end
			if a1.height=3 then
				Result:=a1.item (1,1)*(a1.item (2,2)*a1.item (3,3)-a1.item (2,3)*a1.item (3,2))-a1.item (1,2)*(a1.item (2,1)*a1.item (3,3)-a1.item (2,3)*a1.item (3,1))+a1.item (1,3)*(a1.item (2,1)*a1.item (3,2)-a1.item (2,2)*a1.item (3,1))
			end
			ensure
				square:a1.width=a1.height
		end

end
