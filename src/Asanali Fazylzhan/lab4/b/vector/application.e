note
	description: "vector application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
	local
		op: VECTOR_OPER
		a1,a2,a3:ARRAY[INTEGER]
		ar:REAL_64
			-- Run application.
		do
			create op.create_oper
			create a3.make(1,3)
			create a1.make(1,3)
			a1[1]:=1
			a1[2]:=2
			a1[3]:=3
			create a2.make(1,3)
			a2[1]:=4
			a2[2]:=5
			a2[3]:=6
			a3:=op.cross_product(a1,a2)
			Io.put_integer (a3[1])
			Io.put_string(" ")
			Io.put_integer (a3[2])
			Io.put_string(" ")
			Io.put_integer (a3[3])
			Io.put_string(" ")
			Io.new_line
			ar:=op.triangle_area(a1,a2)
			print(ar)
		end

end
