note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER

inherit
	DOUBLE_MATH

create
	create_oper

feature
	create_oper
	do

	end

	vector_length(a1:ARRAY[INTEGER]):REAL_64
	require
		a1.count>0

	local
		i:INTEGER
	do
		from
			i:=1
		until
			i>a1.count
		loop
			Result:=Result+a1[i]*a1[i]
			i:=i+1
		end
		Result:=sqrt(Result)
	end

	dot_product(a1:ARRAY[INTEGER]; a2:ARRAY[INTEGER]):INTEGER
	require
		same_length:a1.count=a2.count

	local
		i,n:integer

	do
		from
			i:=1
		until
			i>a1.count
		loop
			n:=n+a1[i]*a2[i]
			i:=i+1
		end
		Result:=n
		ensure
			same_length:a1.count=a2.count
	end

	cross_product(a1:ARRAY[INTEGER]; a2:ARRAY[INTEGER]):ARRAY[INTEGER]

	require
		same_length:a1.count=a2.count
		triple_coord:a1.count=3 and a2.count=3

	local
		i,v:integer
		r:ARRAY[INTEGER]
	do
		create r.make_filled (0, 1, 3)
		v:=a1[2]*a2[3]-a1[3]*a2[2]
		r.put (v, 1)
		v:=(-1)*(a1[1]*a2[3]-a1[3]*a2[1])
		r.put (v, 2)
		v:=a1[1]*a2[2]-a1[2]*a2[1]
		r.put (v, 3)
		Result:=r
		ensure
			same_length:a1.count=a2.count
			triple_coord:a1.count=3 and a2.count=3
	end

	triangle_area(a1,a2:ARRAY[INTEGER]):REAL_64
	require
		same_length:a1.count=a2.count


	local
		sinus:REAL_64
		cosin:REAL_64
	do
		cosin:=dot_product(a1,a2)/(vector_length(a1)*vector_length(a2))
		sinus:=sqrt(1-cosin*cosin)
		Result:=0.5*dot_product(a1,a2)*sinus

	ensure
		area_positive:Result>0
	end


end
