note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	create_range

feature
	right:INTEGER
	left:INTEGER

	create_range(m,n:INTEGER)
	do
		right:=n
		left:=m
	end

	is_equal_range(r1:RANGE):BOOLEAN

	do
		Result:=false
		if right=r1.right and left=r1.left then
			Result:=true
		end
	end

	is_empty:BOOLEAN

	do
		Result:=false
		if left>right then
			Result:=true
		end
	end

	is_sub_range_of(r1:RANGE):BOOLEAN
	require
	emptiness:r1.is_empty=false and is_empty=false
	do
		Result:=false
		if left>=r1.left and right<=r1.right then
			Result:=true
		end
	end

	is_super_range_of(r1:RANGE):BOOLEAN
	require
	emptiness:r1.is_empty=false and is_empty=false
	do
		Result:=false
		if left<=r1.left and right>=r1.right then
			Result:=true
		end
	end

	left_overlaps(r1:RANGE):BOOLEAN
	require
	emptiness:r1.is_empty=false and is_empty=false
	do
		Result:=false
		if left>=r1.left and left<=r1.right then
			Result:=true
		end
	end

	right_overlaps(r1:RANGE):BOOLEAN
	require
	emptiness:r1.is_empty=false and is_empty=false
	do
		Result:=false
		if right>=r1.left and right<=r1.right then
			Result:=true
		end
	end

	overlaps(r1:RANGE):BOOLEAN
	require
	emptiness:r1.is_empty=false and is_empty=false
	do
		Result:=false
		if right_overlaps(r1)=true or left_overlaps(r1)=true then
			Result:=true
		end
	end

	add(r1:RANGE):RANGE
	require
		not_empty: current.is_empty=false and r1.is_empty=false
	local
		l,r:INTEGER
	do
		l:=left
		r:=right
		if r1.left<left then
			l:=r1.left
		end
		if r1.right>right then
			r:=r1.right
		end
		create Result.create_range (l, r)
	end

	subtract(r1:RANGE):RANGE
	require
		sub_not_bigger:is_sub_range_of(r1)=false
	local
		l,r:INTEGER
	do
		l:=left
		r:=right
		if r1.left_overlaps(current) then
			r:=r1.left
		end
		if r1.right_overlaps(current) then
			l:=r1.right
		end
		create Result.create_range (l, r)
	end
end
