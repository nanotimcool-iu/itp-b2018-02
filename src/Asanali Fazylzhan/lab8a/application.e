note
	description: "bin_tree application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
	local
		l,leaf1,leaf2: detachable BIN_TREE[INTEGER]

		do
			create leaf1.create_tree(4,Void,Void)
			create leaf2.create_tree(6,Void,Void)
			create l.create_tree (5, Void,Void)
			l.add (leaf1)
			l.add (leaf2)
			Io.put_integer (l.height)
		end

end
