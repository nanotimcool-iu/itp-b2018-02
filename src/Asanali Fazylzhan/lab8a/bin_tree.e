note
	description: "Summary description for {BIN_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIN_TREE[G->COMPARABLE]

create
	create_tree

feature

	create_tree(i: G;l,r: detachable BIN_TREE[G])
	do
		info:=i
		right:=r
		left:=l
	end

	height:INTEGER
	local
		left_height,right_height:INTEGER
	do
		left_height:=det_height(left)
		right_height:=det_height(right)
		if left_height>right_height then
			Result:=left_height+1
		else
			Result:=right_height+1
		end

	end

	add(t:BIN_TREE[G])
	do
		if t.info>info then
			right:=t
		end

		if t.info<info then
			left:=t
		end
	end

	info: G
	stop:BOOLEAN

feature{NONE}
	left:  detachable BIN_TREE[G]
	right:  detachable BIN_TREE[G]

	det_height(t:detachable BIN_TREE[G]):INTEGER
	do
		if t/=Void then
			Result:=t.height
		else
			Result:=0
		end
	end

end
