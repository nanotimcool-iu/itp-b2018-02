note
	description: "huffman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE}

	coding: HASH_TABLE [STRING, CHARACTER]
	leaves: ARRAY [ELEMENT[CHARACTER]]
	make
		local
			inp,outp:STRING
			freq:HASH_TABLE [INTEGER, CHARACTER]
		do
			print ("Enter the string:")
			io.read_line
			inp:=io.last_string
			freq:=find_frequency(inp)
			leaves:= fill_leaves(freq)
			sort_leave_arr
			huffman_build
			create coding.make (freq.count)
			deep_search ("", leaves [leaves.lower])
			print ("%Ncode:%N" + code (inp))
			print ("%Nencodig-coding:%N")
			outp:= decode (code (inp))
			print (outp.is_equal (inp))
		end

feature {NONE}

	code (s: STRING): STRING
		require
			not s.is_empty
			s_contains_only_sumbols_from_encoding: has_only (s, coding.current_keys)
		do
			Result := ""
			across
				s as ch
			loop
				if attached coding [ch.item] as add then
					Result := Result + add
				end
			end
		end

	decode (s: STRING): STRING
		require
			containd_only_1_and_0: has_only (s, <<'0', '1'>>)
		local
			i: INTEGER
			leaf: detachable ELEMENT [CHARACTER]
		do
			Result := ""
			from
				i := 1
			until
				i > s.count
			loop
				leaf := leaves [leaves.upper]
				from
				until
					leaf /= Void and then not leaf.children
				loop
					if leaf /= Void then
						if s.at (i) = '0' then
							leaf := leaf.left
						else
							leaf := leaf.right
						end
						i := i + 1
					end
				end
				if attached leaf as l then
					Result := Result + l.item.out
				end
			end
		end

feature {NONE}

	deep_search (s: STRING; leaf: detachable ELEMENT [CHARACTER])
		do
			if attached leaf as l and then l.children then
				deep_search (s + "0", l.left)
				deep_search (s + "1", l.right)
			elseif attached leaf as l then
				coding.put (s, l.item)
			end
		end

feature {NONE}

	huffman_build
		local
			leaf: ELEMENT [CHARACTER]
		do
			from
			until
				leaves.count < 2
			loop
				create leaf.create_leaf (' ', leaves [leaves.lower].intval + leaves [leaves.lower + 1].intval)
				leaf.set_leaves (leaves [leaves.lower], leaves [leaves.lower + 1])
				put_leaf (leaf)
				leaves := leaves.subarray (leaves.lower + 2, leaves.upper)
			end
		end

	put_leaf (leaf: ELEMENT [CHARACTER])
		local
			i: INTEGER
		do
			i := leaves.lower
			from
				i := leaves.lower
			until
				i > leaves.upper or else leaves [i].intval > leaf.intval
			loop
				i := i + 1
			end
			shift_right_from (i)
			leaves.force (leaf, i)
		ensure
			is_sorted
		end

	shift_right_from (start: INTEGER)
		local
			i: INTEGER
		do
			from
				i := leaves.upper
			until
				i < start
			loop
				leaves.force (leaves [i], i + 1)
				i := i - 1
			end
		ensure
			leaves.valid_index (start) implies old leaves.upper = leaves.upper - 1
		end

	sort_leave_arr
		require
			leafs_are_not_empty: not leaves.is_empty
		local
			i, j, min_value, min_index: INTEGER
			changer: ELEMENT [CHARACTER]
		do
			from
				i := leaves.lower
			until
				i > leaves.upper
			loop
				min_value := leaves [i].intval
				min_index := i
				from
					j := i + 1
				until
					j > leaves.upper
				loop
					if leaves [j].intval < min_value then
						min_value := leaves [j].intval
						min_index := j
					end
					j := j + 1
				end
				changer := leaves [i]
				leaves [i] := leaves [min_index]
				leaves [min_index] := changer
				i := i + 1
			end
		ensure
			is_sorted
		end

	fill_leaves (freq: HASH_TABLE [INTEGER, CHARACTER]): ARRAY [ELEMENT [CHARACTER]]
		require
			frequency_contais_elements: not freq.is_empty
		local
			leaf: ELEMENT [CHARACTER]
			i, val: INTEGER
			keys: ARRAY [CHARACTER]
		do
			keys := freq.current_keys
			create leaf.create_leaf (keys [1], 1)
			create Result.make_filled (leaf, 1, freq.count)
			from
				i := 1
			until
				i > Result.count
			loop
				val := freq [keys [i]]
				create leaf.create_leaf (keys [i], val)
				Result [i] := leaf
				i := i + 1
			end
		ensure
			leangth_are_the_same: Result.count = freq.count
		end

	find_frequency (s: STRING): HASH_TABLE [INTEGER, CHARACTER]
		require
			string_is_not_empty: not s.is_empty
		do
			create Result.make (s.count)
			across
				s as ch
			loop
				if not Result.has (ch.item) then
					Result.put (1, ch.item)
				else
					Result.force (Result [ch.item] + 1, ch.item)
				end
			end
		ensure
			sum_of_values (Result) = s.count
		end

feature {NONE}

	has_only (s: STRING; chars: ARRAY [CHARACTER]): BOOLEAN
		do
			Result := True
			across
				s as ch
			loop
				if not chars.has (ch.item) then
					Result := False
				end
			end
		end

	is_sorted: BOOLEAN
		local
			i: INTEGER
		do
			Result := True
			from
				i := leaves.lower + 1
			until
				(i > leaves.upper) or not Result
			loop
				if leaves [i - 1].intval > leaves [i].intval then
					Result := False
				end
				i := i + 1
			end
		end

	sum_of_values (hash_table: HASH_TABLE [INTEGER, CHARACTER]): INTEGER
		local
			keys: ARRAY [CHARACTER]
			i: INTEGER
		do
			keys := hash_table.current_keys
			from
				i := 1
			until
				i > keys.count
			loop
				Result := Result + hash_table [keys [i]]
				i := i + 1
			end
		end
end
