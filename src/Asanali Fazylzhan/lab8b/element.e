note
	description: "Summary description for {ELEMENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ELEMENT[G]

create
	create_leaf

feature
	create_leaf (i: G; int: INTEGER)
		require
			more_than_equal: int > 0
		do
			item:=i
			intval:=int
			left:=Void
			right:=Void
			children:=False
		end

	set_leaves(l, r: ELEMENT [G])
		require
			r.intval>=l.intval
		do
			left:=l
			right:=r
			children:=True
		end

	item: G
	intval: INTEGER
	left:detachable  ELEMENT[G]
	right: detachable ELEMENT [G]
	children: BOOLEAN
end
