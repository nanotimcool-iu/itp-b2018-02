note
	description: "bag application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			b1, b2: MY_BAG
		do
			create b1.make

			b1.add ('a', 8)
			b1.remove ('a', 6)
			b1.add ('c', 1)
			b1.remove ('d', 2)
			b1.output

			io.put_character (b1.max)
			io.new_line
			io.put_character (b1.min)
			io.new_line

			create b2.make

			b2.add ('c', 1)
			b2.add ('a', 2)

			io.put_boolean (b1.is_equal_bag (b2))

			b2.add ('d', 1)
			io.new_line
			io.put_boolean (b2.is_equal_bag (b1))
		end

end
