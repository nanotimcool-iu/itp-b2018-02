note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	make

feature -- Initialise

	make(value_: CHARACTER; number_copies_: INTEGER)
		-- Creates a new cell
		do
			value := value_
			number_copies := number_copies_
		end

feature -- Info

	value: CHARACTER
		-- The stored character

	number_copies: INTEGER
		-- The number of stored chatacters

feature -- Setters

	set_value (v: CHARACTER)
		-- Changes 'value'
		do
			value := v
		end

	set_number_copies (n: INTEGER)
		-- Changes 'number_copies'
		do
			number_copies := n
		end

	add_copies (n: INTEGER)
		-- Adds n copies to number_copies
		do
			number_copies := number_copies + n
		end

	remove_copies (n: INTEGER)
		-- Removes n copies from number_copies
		do
			if n <= number_copies then
				number_copies := number_copies - n
			end
		end

end
