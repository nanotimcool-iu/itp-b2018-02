note
	description: "Summary description for {MY_BAG}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_BAG

create
	make

feature -- Initialise

	make
		do
			create elements.make (0)
		end

feature -- Info

	elements: ARRAYED_LIST[CELL_INFO]
		-- Elements of the multiser

feature -- Editting

	add (val: CHARACTER; n: INTEGER)
		-- Add n copies of val to the bag
		local
			some_cell: CELL_INFO
			i, k: INTEGER
		do
			if elements.count = 0 then

				create some_cell.make (val, n)
				elements.extend (some_cell)

			else

				k := 0
				from
					i := 1
				until
					i = elements.count
				loop
					if elements.at (i).value = val then
						k := i
					end
					i := i + 1
				end

				if k = 0 then
					create some_cell.make (val, n)
					elements.extend (some_cell)
				else
					elements.at (k).add_copies (n)
				end

			end
		end

	remove (val: CHARACTER; n: INTEGER)
		-- Remove n copies of val
		local
			k: INTEGER
		do
			if elements.count > 0 then
				k := 0
				across elements as some_element loop
					if some_element.item.value = val then
						k := elements.index_of (some_element.item, 1)
					end
				end

				if k > 0 then
					if elements.at (k).number_copies <= n then
						elements.go_i_th (k)
						elements.remove
					else
						elements.at (k).remove_copies (n)
					end
				end

			end
		end

feature -- Get info

	min: CHARACTER
		-- Returns the minimum element
		require
			not_empty: elements.count > 0
		local
			answer: CHARACTER
		do
			answer := elements.at (1).value
			across elements as element loop
				if element.item.value < answer then
					answer := element.item.value
				end
			end
			Result := answer
		end

	max: CHARACTER
		-- Returns the maximum element
		require
			not_empty: elements.count > 0
		local
			answer: CHARACTER
		do
			answer := elements.at (1).value
			across elements as element loop
				if element.item.value > answer then
					answer := element.item.value
				end
			end
			Result := answer
		end

	is_equal_bag (b: MY_BAG): BOOLEAN
		-- Is bag b equal to the current bag?
		local
			answer: BOOLEAN
			found: BOOLEAN
		do
			if current.elements.count /= b.elements.count then
				answer := FALSE
			else
				answer := TRUE
				across current.elements as cur_element loop

					found := FALSE
					across b.elements as b_element loop

						if cur_element.item.value = b_element.item.value then
							if cur_element.item.number_copies = b_element.item.number_copies then
								found := TRUE
							else
								answer := FALSE
							end
						end

					end
					if not found then
						answer := FALSE
					end

				end
			end
			Result := answer
		end

feature -- Contains

	output
		-- Prints what's inside the bag
		do
			across elements as element loop
				io.put_character (element.item.value)
				io.put_string (" ")
				io.put_integer_32 (element.item.number_copies)
				io.new_line
			end
		end
end
