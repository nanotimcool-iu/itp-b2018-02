class
	CHANCE

inherit

	SQUARE

create
	make

feature {NONE} -- Initialization

	name: STRING

	rand: RANDOMIZER

	make
			-- Initialization for `Current'.
		do
			name := "CHANCE!"
			create rand.make
		end

feature {MONOPOLY}

	process (p: PLAYER)
		local
			delta: INTEGER
		do
			delta := rand.chance
			if delta < 0 then
				print ("Wow, you are unfortunate today.%NYou have lost " + delta.abs.out + "K%N")
			else
				print ("You are really lucky!%NYou have found " + delta.out + "K%N")
			end
			p.add_balance (rand.chance)
		end

end
