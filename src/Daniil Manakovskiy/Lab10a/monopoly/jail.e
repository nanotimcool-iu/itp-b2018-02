class
	JAIL

inherit

	SQUARE

create
	make

feature {NONE}

	name: STRING

	make
		do
			name := "JAIL"
		end

feature {MONOPOLY}

	process (p: PLAYER)
		do
			print ("You are in the jail now!%NGame is over for you%N")
			p.set_balance (-1) -- make him lose
		end

end
