class
	MONOPOLY

inherit

	EXECUTION_ENVIRONMENT

create
	init

feature {NONE}

	field: ARRAY [SQUARE]
			--players: ARRAY[PLAYER]

	players: ARRAYED_LIST [PLAYER]

	init (n: INTEGER)
		require
			good_n: n >= 2 and n <= 6
		local
			i: INTEGER
			t_p: PLAYER
			t_s: JAIL
		do
			create t_s.make
			create t_p.make ("temp")
			create field.make_filled (t_s, 0, 19)
			create players.make (0)
			from
				i := 1
			until
				i > n
			loop
				print ("Input player " + (i).out + "'s name%N")
				io.readline
				create t_p.make (io.laststring.twin)
				players.extend (t_p)
				i := i + 1
			end

				--somewhere here we should fill the field

			field [0] := create {START}.make
			field [1] := create {PROPERTY}.make ("Christ the Redeemer", 60, 2)
			field [2] := create {PROPERTY}.make ("Luang Pho To", 60, 4)
			field [3] := create {TAX}.make
			field [4] := create {PROPERTY}.make ("Alyosha monument", 80, 4)
			field [5] := create {JAIL}.make
			field [6] := create {PROPERTY}.make ("Tokyo Wan Kannon", 100, 6)
			field [7] := create {PROPERTY}.make ("Luangpho Yai", 120, 8)
			field [8] := create {CHANCE}.make
			field [9] := create {PROPERTY}.make ("The Motherland", 160, 12)
			field [10] := create {PARKING}.make
			field [11] := create {PROPERTY}.make ("Awaji Kannon", 220, 18)
			field [12] := create {CHANCE}.make
			field [13] := create {PROPERTY}.make ("Rodina-Mat' Zovyot!", 260, 22)
			field [14] := create {PROPERTY}.make ("Great Buddha of Thailand", 280, 22)
			field [15] := create {JAIL}.make
			field [16] := create {PROPERTY}.make ("Laykyun Setkyar", 320, 28)
			field [17] := create {PROPERTY}.make ("Spring Temple Buddha", 360, 35)
			field [18] := create {CHANCE}.make
			field [19] := create {PROPERTY}.make ("Statue of Unity", 400, 50)
		end

feature

	start
		local
			round, cur_player_id, dice, max_money: INTEGER
			msg: STRING
			rand: RANDOMIZER
			cur_player: PLAYER
		do
			create rand.make
			msg := ""
			from
				round := 0
			until
				round > 100 or else players.count = 1
			loop
				from
					cur_player_id := 1
				until
					cur_player_id > players.count
				loop
					system ("CLS")
					print ("Round " + round.out + "%N")
					cur_player := players [cur_player_id]
					print (cur_player.get_name + ", your balance is " + cur_player.get_balance.out + "%Nyou are at " + field [cur_player.get_cur_pos].out + "[" + cur_player.get_cur_pos.out + "]%NPress Enter to make a turn%N")
					io.readline
					dice := rand.dice_roll
					cur_player.go (dice)
					print ("You have rolled " + dice.out + " and moved to " + field [cur_player.get_cur_pos].out + "[" + cur_player.get_cur_pos.out + "]%N")
					field [cur_player.get_cur_pos].process (cur_player)
					print ("Now your balance is " + cur_player.get_balance.out + "K%N")
					if cur_player.is_lost then
						players.prune (cur_player)
						print ("You have lost =(%N")
					end
					print ("Press Enter to continue%N")
					io.readline
					cur_player_id := cur_player_id + 1
				end
				round := round + 1
			end
			system ("CLS")
			if round > 100 then
				across
					players as pl
				loop
					max_money := max_money.max (pl.item.get_balance)
				end
				print ("Winner(-s): ")
				across
					players as pl
				loop
					if max_money = pl.item.get_balance then
						print (pl.item.get_name + " ")
					end
				end
			else
				print (players [1].get_name + " won!")
			end
		end

end
