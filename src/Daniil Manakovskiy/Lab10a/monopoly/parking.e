class
	PARKING

inherit

	SQUARE

create
	make

feature {NONE} -- Initialization

	name: STRING

	make
			-- Initialization for `Current'.
		do
			name := "PARKING"
		end

feature {MONOPOLY}

	process (p: PLAYER)
		do
				-- does nothing
			print ("Wow! Take a break!%N")
		end

end
