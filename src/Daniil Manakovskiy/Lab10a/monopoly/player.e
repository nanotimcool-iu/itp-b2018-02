class
	PLAYER

create
	make

feature {NONE}

	name: STRING

	balance: INTEGER

	lost: BOOLEAN

	current_field: INTEGER

	make (player_name: STRING)
		require
			existing_string: player_name /= Void
		do
			name := player_name
			balance := 1500
			lost := false
			Current_field := 0
		end

feature {MONOPOLY, SQUARE}

	get_balance: INTEGER
		do
			Result := balance
		end

	get_name: STRING
		do
			Result := name
		end

	get_cur_pos: INTEGER
		do
			Result := current_field
		end

	go (n: INTEGER)
		require
			inbound_n: n > 0 and then n <= 12
		do
			current_field := (current_field + n).integer_remainder (20)
		end

	is_lost: BOOLEAN
		do
			Result := balance <= 0
		end

	set_balance (bal: INTEGER)
		do
			balance := bal
		end

	add_balance (bal: INTEGER)
		do
			balance := balance + bal
				-- bal can be negative
		end

invariant
	inbound: current_field >= 0 and then current_field <= 20

end
