class
	PROPERTY

inherit

	SQUARE

create
	make

feature {NONE}

	name: STRING

	owner: detachable PLAYER

	price, fine: INTEGER

	make (prop_name: STRING; price_a, fine_a: INTEGER)
		do
			name := prop_name
			price := price_a
			fine := fine_a
		end

feature {MONOPOLY}

	set_owner (person: PLAYER)
		require
			real_person: person /= Void
		do
			owner := person
		end

	process (p: PLAYER)
		local
			decision: INTEGER
		do
			if attached owner as ow then
				if p /= ow then
					ow.add_balance (fine)
					p.add_balance (- fine)
				end
			else
				print ("Would you like to buy this field?%NThe price is " + price.out + " K Rub%N1 - Yes%N0-No%N")
				decision := -1
				from
				until
					decision = 0 or else decision = 1
				loop
					io.read_integer
					decision := io.lastint
				end
				inspect decision
				when 1 then
					p.add_balance (- price)
					set_owner (p)
					print ("Congratulations! Now you are an owner of " + name + "!%N")
				else
					print ("Ok =(%N")
				end
			end
		end

end
