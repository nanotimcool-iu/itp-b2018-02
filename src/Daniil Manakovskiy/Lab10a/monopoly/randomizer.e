class
	RANDOMIZER

create
	make

feature {NONE}

	r: RANDOM

	make
		local
			t: TIME
		do
			create t.make_now
			create r.set_seed (t.compact_time)
		end

feature {MONOPOLY}

	dice_roll: INTEGER
		do
			Result := r.item.integer_remainder (12)
			r.forth
			Result := (Result + r.item.integer_remainder (12)).integer_remainder (12) + 1
			r.forth
		ensure
			possible_result: Result > 0 and Result <= 12
		end

feature {CHANCE}

	chance: INTEGER
		local
			tmp: INTEGER
		do
			tmp := r.item
			r.forth
			Result := r.item.integer_remainder (20) * 10
			r.forth
			if tmp.integer_remainder (2) = 1 then
				Result := - Result
			end
		end

end
