deferred class
	SQUARE

inherit

	ANY
		redefine
			out
		end

feature {NONE}

	name: STRING
		deferred
		end

feature {MONOPOLY}

	out: STRING
		do
			Result := name
		end

	process (p: PLAYER)
		require
			real_person: p /= Void
		deferred
		end

end
