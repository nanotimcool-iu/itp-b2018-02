class
	START

inherit

	SQUARE

create
	make

feature {NONE} -- Initialization

	name: STRING

	make
			-- Initialization for `Current'.
		do
			name := "Salary"
		end

feature {MONOPOLY}

	process (p: PLAYER)
		do
			print ("You have received your salary! (+200K )%N")
			p.add_balance (200)
		end

end
