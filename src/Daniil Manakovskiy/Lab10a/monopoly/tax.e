class
	TAX

inherit

	SQUARE

create
	make

feature {NONE} -- Initialization

	name: STRING

	make
			-- Initialization for `Current'.
		do
			name := "TAX Paying"
		end

feature {MONOPOLY}

	process (p: PLAYER)
		do
			print ("You have to pay 10%% of your money to the goverment!%N")
			p.set_balance ((p.get_balance * 0.9).ceiling)
		end

end
