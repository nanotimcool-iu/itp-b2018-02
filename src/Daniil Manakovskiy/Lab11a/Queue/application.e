note
	description: "queue application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			q:EVENT_QUEUE
			e:EVENT
			i:INTEGER
		do
			create q.make
			--TC1
			from
				i:=1
			until
				i = 100_000
			loop
				q.add (create {EVENT}.make (i, "Type"))
				i := i + 1
			end

			print(q.next.put)
			print("%N%N")
			q.clear

			--TC2
			q.add (create {EVENT}.make (-5, "Neg"))
			io.putbool (q.is_empty)

			q.clear
			print("%N%N")

			--TC3
			q.add (create {EVENT}.make (5, "TEST"))
			q.add (create {EVENT}.make (5, "TEST"))
			q.add (create {EVENT}.make (5, "TEST"))

			e := q.extract
			io.putbool (q.is_empty)
		end

end
