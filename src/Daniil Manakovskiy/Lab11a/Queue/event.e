note
	description: "Summary description for {EVENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EVENT

inherit
	COMPARABLE

create
	make

feature {NONE}

	time:INTEGER
	type:STRING

	make(seconds: INTEGER; t: STRING)
	require
		n: not t.is_empty
	do
		time := seconds;
		type := t
	end

feature

	get_time : INTEGER
	do
		Result := time
	end

	is_less alias "<" (other: like Current): BOOLEAN
		do
			Result := Current.get_time > other.get_time
		end

	put: STRING
	do
		Result := "Type: " + type + "; Time: " + time.out + "%N"
	end


end
