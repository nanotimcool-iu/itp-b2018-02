note
	description: "Summary description for {EVENT_QUEUE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EVENT_QUEUE

create
	make

feature {NONE} -- Initialization

queue : LINKED_PRIORITY_QUEUE[EVENT]

	make
			-- Initialization for `Current'.
		do
			create queue.make
		end

feature

	is_empty: BOOLEAN
	do
		Result := queue.is_empty
	end

	clear
	do
		queue.wipe_out
	end

	add(e:EVENT)
	local
		tmp:EVENT
	do
		if e.get_time >= 0  then
			queue.extend (e)
		end
	end


	extract:EVENT
	local
		tmp : EVENT
	do
		Result := queue.item
		queue.remove

		from
		until
			 queue.is_empty or else Result.get_time /= queue.item.get_time
		loop
			queue.remove
		end
	end

	next:EVENT
	do
		Result := queue.item
	end

end
