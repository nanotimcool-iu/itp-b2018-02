note
	description: "Anagrams application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			str: STRING;
			a: ARRAY [STRING]
			i: INTEGER
		do
			print ("Input a string: ")
			io.readline
			str := io.laststring.twin
			print("Anagrams:%N")
			anagram(str.twin, 1, str.count)
		end



		anagram(str:STRING; l,r:INTEGER)
		local
			i :INTEGER
			tmp : STRING
		do
			if (l = r) then
				print("%T"+str + "%N")
			else
				tmp:=str.twin
				from
					i:=l
				until
					i > r
				loop
					tmp := swap(tmp.twin, l, i)
					anagram(tmp.twin, l+1, r)
					tmp := swap(tmp.twin, l ,i)
					i := i + 1
				end
			end
		end

		swap(str:STRING; f,l:INTEGER):STRING
		do
			Result := str.twin
			Result[f] := str[l]
			Result[l] := str[f]
		end



end
