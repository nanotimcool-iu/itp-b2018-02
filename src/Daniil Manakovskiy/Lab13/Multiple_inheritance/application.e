note
	description: "Multiple_inheritance application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			l:LAB
			b:BIOLOGIST
			bi:BIO_INFORMATIC
		do
			--| Add your code here
		create l.make
		create b.make ("Bogdan", "Loves biology", 1)
		b.set_pet_name ("GOP")
		create bi.make("Volodya", "Loves biology and CS", 5)
		bi.set_pet_name ("STOP")
		l.add_member (create {COMPUTER_SCIENTIST}.make ("Homer", "Just a guy", 0))
		l.add_member (b)
		l.add_member (bi)

		l.introduce_all
		end

end
