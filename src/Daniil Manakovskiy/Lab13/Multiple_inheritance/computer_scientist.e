note
	description: "Summary description for {COMPUTER_SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMPUTER_SCIENTIST

inherit

	SCIENTIST
		redefine
			make,
			introduce
		end

create
	make

feature {NONE}

	make (name_a, bio: STRING; i: INTEGER)
		do
			precursor (name_a, bio, i)
			discipline := "Computer science"
		end

	introduce
	do
		precursor
		print("%N")
	end

end
