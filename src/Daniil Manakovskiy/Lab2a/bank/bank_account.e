note
	description: "Summary description for {BANK_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			owner := "BOB"
			balance:= 1000
			deposit(600)
			print(balance)
			io.new_line
			withdraw(100)
			print(balance)
		end

	owner: STRING

	balance: DOUBLE

	deposit (to_put: DOUBLE)
		do
			if to_put >= 0 and balance + to_put <= 1000000 then
				balance := balance + to_put
			else
				print("Error")
			end
		end

	withdraw (to_get:DOUBLE)
	do
		if to_get >= 0 and balance - to_get >= 100 then
			balance := balance - to_get
		else
			print("Error")
		end
	end

end
