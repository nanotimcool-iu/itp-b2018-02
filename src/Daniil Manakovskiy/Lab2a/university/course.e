note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

create
	make


feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			create_class("ITP I", 0, "WED", 180)
		end

	create_class(a_name:STRING; a_id:INTEGER; a_schedule:STRING; a_max_student:INTEGER)
	do
		name:=a_name
		id:=a_id
		schedule:= a_schedule
		max_student:=a_max_student
	end
feature -- fields
	name:STRING

	id:INTEGER

	schedule: STRING

	max_student: INTEGER
end
