note
	description: "CALENDAR application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			p: PERSON
			event,event1:ENTRY
			t:DATE_TIME
			-- Run application.
		do
				--| Add your code here
			create p.make ("Dan", 1234567890, "IU", "IU@IU")
			create t.make_now
			event := create_entry (t , p, "Testing", "IU Room 303")
			event1 := create_entry (t, p, "to test conflict", "somewhere")
			print (event.subject + "%N")
			edit_subject (event, "Tested!!!")
			print (event.subject + "%N")
			print(get_owner_name (event) + "%N")
			io.put_boolean (in_conflict (event, event1))
		end

feature

	create_entry (n_date: DATE_TIME; n_owner: PERSON; n_subject, n_place: STRING_8): ENTRY
		local
			t: ENTRY
		do
			create t.make (n_date, n_owner, n_subject, n_place)
			Result := t
		end

	edit_subject (e: ENTRY; new_subject: STRING)
		do
			e.set_subject (new_subject)
		end

	edit_date (e: ENTRY; new_date: DATE_TIME)
		do
			e.set_date (new_date)
		end

	get_owner_name (e: ENTRY): STRING
		do
			result := e.owner.name
		end

	in_conflict (e1, e2: ENTRY): BOOLEAN
		do
			Result :=e1.owner = e2.owner and  e1.date = e2.date
		end

end
