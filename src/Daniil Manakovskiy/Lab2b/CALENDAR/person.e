note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature {NONE} -- Initialization

	make (n_name: STRING; n_phone_number: INTEGER; n_work_place: STRING; n_email: STRING)
			-- Initialization for `Current'.
		do
			name := n_name
			set_phone (n_phone_number)
			work_place := n_work_place
			email := n_email
		end

feature

	name: STRING

	phone_number: INTEGER assign set_phone

	work_place: STRING

	email: STRING

	set_phone (new_phone: INTEGER)
		require
			len: new_phone.out.count = 10
		do
			phone_number := new_phone
		end

end
