note
	description: "CMS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			c1, c2, c3: CONTACT
		do
				--| Add your code here
			c1:=create_contact ("name1", 1234567890, "work1", "email1")
			c2:=create_contact ("name2", 1234567890, "work2", "email2")
			c3:=create_contact ("name3", 1234567890, "work3", "email3")
			add_emergency_contact (c1, c2)
			add_emergency_contact (c3, c2)
			edit_contact (c2, 2, "1112223330")
			edit_contact (c2, 4, "newemail@r.ru")
			remove_emergency_contact (c2)
			remove_emergency_contact (c3)
			print ("Job's done")
		end

feature

	create_contact (name: STRING; phone: INTEGER; work_place: STRING; email: STRING): CONTACT
		local
			c: CONTACT
		do
			create c.make (name, phone, work_place, email)
			Result := c
		end

	add_emergency_contact(acc:CONTACT; emg:CONTACT)
	do
		acc.set_emergency (emg)
	end

	remove_emergency_contact(acc:CONTACT)
	do
		acc.set_emergency(acc)
	end

	edit_contact(con:CONTACT; param:INTEGER; value:STRING)
	do
		con.edit(param, value)
	end

end
