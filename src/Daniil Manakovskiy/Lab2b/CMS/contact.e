
class
	CONTACT

create
	make

feature {NONE} -- Initialization

	make (a_name:STRING; a_phone_number:INTEGER; a_work_palce:STRING;
	a_email:STRING;)
			-- Initialization for `Current'.
		do
			name := a_name
			set_phone (a_phone_number)
			work_place := a_work_palce
			email:= a_email
			set_emergency (current)
		end
feature -- VARIABLES

	name:STRING
	phone_number:INTEGER
	work_place:STRING
	email:STRING
	call_emergency:CONTACT

	set_phone(new_phone:INTEGER)
	require
		new_phone.out.count = 10
	do
		phone_number := new_phone
	end

	set_emergency(new_emergency:CONTACT)
		do
			call_emergency := new_emergency
		end

	edit(cmd:INTEGER; value:STRING)
	require
		cmd >0
		cmd <5
	do
		inspect cmd
			when 1 then
				name:= value
			when 2 then
				set_phone (value.to_integer)
			when 3 then
				work_place := value
			when 4 then
				email := value

		end

	end
end
