note
	description: "CALENDAR application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			p: PERSON
			event, event1: ENTRY
			t,t1: DATE_TIME
			i: INTEGER
			d: DAY
			dtdr:DATE_TIME_DURATION
			-- Run application.
		do
			create days.make (31)
			from
				i := 1
			until
				i = 31
			loop
				create d.make (i)
				days.extend (d)
				i := i + 1
			end
			create t.make_now
			create t1.make_now
			create p.make ("Vlad", 1234567890, "UI", "UI@IU")
			create event.make (t, p, "Test1", "Place1")
			create event1.make (t, p, "Test2", "Place2")
			create dtdr.make (0, 0, 0, 5, 2, 1)
			add_entry (event, 4)
			add_entry (event1, 4)
			t1.add (dtdr)
			event1.set_date (t1)
			add_entry (event1, 4)
			print ("%N%N" + printable_month)
		end

feature --variables

	days: ARRAYED_LIST [DAY]

feature -- routines

	add_entry (e: ENTRY; day: INTEGER)
		local
			i: INTEGER
			events: ARRAYED_LIST [ENTRY]
			conflict: ENTRY
		do
			events := days.i_th (day).events
			from
				i := 1
			until
				i > events.count
			loop
				if is_two_events_in_conflict (e, events.i_th (i)) = TRUE then
					conflict := events.i_th (i)
					i := events.count -- to stop cycle
				end
				i := i + 1
			end
			if conflict = Void then
				days.i_th (day).add_entry (e)
			else
				print ("Event%N" + e.to_string ("%T") + "%Nwasn't added due to conflict with%N" + conflict.to_string ("%T") + "%N%N")
			end
		end

	create_entry (n_date: DATE_TIME; n_owner: PERSON; n_subject, n_place: STRING_8): ENTRY
		local
			t: ENTRY
		do
			create t.make (n_date, n_owner, n_subject, n_place)
			Result := t
		end

	edit_subject (e: ENTRY; new_subject: STRING)
		do
			e.set_subject (new_subject)
		end

	edit_date (e: ENTRY; new_date: DATE_TIME)
		do
			e.set_date (new_date)
		end

	get_owner_name (e: ENTRY): STRING
		do
			result := e.owner.name
		end

	is_two_events_in_conflict (e1, e2: ENTRY): BOOLEAN
		do
			Result := e1.owner = e2.owner and e1.date = e2.date
		end

	printable_month: STRING
		local
			i: INTEGER
			s: STRING
		do
			s := ""
			from
				i := 1
			until
				i > days.count
			loop
				s := s + "Day " + i.out + ":" + days.i_th (i).to_string ("%T") + "%N"
				i := i + 1
			end
			Result := s
		end

end
