note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	make

feature {NONE} -- Initialization

	make (n_date: INTEGER)
			-- Initialization for `Current'.
		do
			date := n_date
			create events.make (0)
		end

feature -- vars

	date: INTEGER -- eg: date^th day of month

	events: ARRAYED_LIST [ENTRY]

feature -- routins

	add_entry (e: ENTRY)
		do
			events.extend (e)
		end

	to_string (del: STRING): STRING
		local
			s: STRING
			i: INTEGER
		do
			s := ""
			from
				i := 1
			until
				i > events.count
			loop
				s := s + events.i_th (i).to_string ("%T") + "%N%N"
				i := i + 1
			end
			Result := s
		end

end
