note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	make

feature {NONE} -- Initialization

	make (n_date: DATE_TIME; n_owner: PERSON; n_subject: STRING; n_place: STRING)
			-- Initialization for `Current'.
		do
			date := n_date
			owner := n_owner
			set_subject (n_subject)
			place := n_place
		end

feature

	date: DATE_TIME

	owner: PERSON

	subject: STRING

	place: STRING

	set_subject (subj: STRING)
		do
			subject := subj
		ensure
			subject = subj
		end

	set_date (new_date: DATE_TIME)
		do
			date := new_date
		ensure
			date = new_date
		end

	to_string (del: STRING): STRING
		local
			s: STRING
		do
			s := del + "EVENT:%N" + del + "Owner: " + owner.name + "%N" + del + "Subject: " + subject + "%N" + del + "Place: " + place + "%N" + del + "Date: " + date.out
			Result := s
		end

end
