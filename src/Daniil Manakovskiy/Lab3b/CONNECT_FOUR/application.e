﻿class
	APPLICATION

inherit

	EXECUTION_ENVIRONMENT

create
	make

feature

	finish: BOOLEAN

	field: ARRAY2 [INTEGER]

	discs_on_top: ARRAY [INTEGER]

	n, m, cur_player, max_turns, turns_done: INTEGER

	warning: STRING

	make
		local
			cur_column: INTEGER
		do
			warning := ""
			from

			until
				((n >= 4) or (m >= 4)) and m * n >= 8
			loop
				system ("CLS")
				print (warning)
				print ("Enter rows and columns numbers line-by-line:%N")
				io.read_integer
				n := io.last_integer
				io.read_integer
				m := io.last_integer
				warning := "It is impossible to win at this field. Try again.%N"
			end

				--
			create field.make_filled (0, n, m)
			create discs_on_top.make_filled (n, 1, m)
			cur_player := 1
			max_turns := n * m
			turns_done := 0
			warning := ""
				--
			from
				finish := false
			until
				finish = true
			loop
				system ("CLS")
				print (warning)
				warning := ""
				print ("Player" + cur_player.out + " turn:%N")
				print_field
				print ("Enter a column to put disk: ")
				io.read_integer
				cur_column := io.last_integer
					--
					--fix overflow
				if cur_column <= m and cur_column >= 1 then
					if discs_on_top.at (cur_column) > 0 then
						field.put (cur_player, discs_on_top.at (cur_column), cur_column)
							-- check win condition
						if win (cur_player, discs_on_top.at (cur_column), cur_column, 1, 0) = 4 -- down
								-- below Current counts twice
							or win (cur_player, discs_on_top.at (cur_column), cur_column, 0, 1) + win (cur_player, discs_on_top.at (cur_column), cur_column, 0, -1) = 5 --horisontal
							or win (cur_player, discs_on_top.at (cur_column), cur_column, 1, -1) + win (cur_player, discs_on_top.at (cur_column), cur_column, -1, 1) = 5 --tr+bl
							or win (cur_player, discs_on_top.at (cur_column), cur_column, 1, 1) + win (cur_player, discs_on_top.at (cur_column), cur_column, -1, -1) = 5 --tl+br
						then -- down
							warning := "Player" + cur_player.out + " wins!%N"
							finish := true
						end
							--
						discs_on_top.force (discs_on_top.at (cur_column) - 1, cur_column)
						inspect cur_player
						when 1 then
							cur_player := 2
						else
							cur_player := 1
						end
							--
						turns_done := turns_done + 1
						if turns_done = max_turns then
							warning := "DRAW!%N"
							finish := true
						end
					else
						warning := "Column " + cur_column.out + " is full. Chose another.%N"
					end
				else
					warning := "You can chose columns only from 1 to " + m.out + "%N"
				end
			end
			system ("CLS")
			print (warning)
		end

feature

	win (color, row, column, d_row, d_column: INTEGER): INTEGER
		do
			if (0 < row and row <= n) and (0 < column and column <= m) then
				if field.item (row, column) = color then
					Result := 1 + win (color, row + d_row, column + d_column, d_row, d_column)
				else
					Result := 0
				end
			else
				Result := 0
			end
		end

	print_field
		local
			i, j: INTEGER
		do
			from
				i := 1
			until
				i > n
			loop
				from
					j := 1
				until
					j > m
				loop
					inspect field.item (i, j)
					when 0 then
						print ("O ")
					else
						print (field.item (i, j).out + " ")
					end
					j := j + 1
				end
				io.new_line
				i := i + 1
			end
		end

end
