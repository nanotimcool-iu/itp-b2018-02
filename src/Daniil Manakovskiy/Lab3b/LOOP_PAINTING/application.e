note
	description: "LOOP_PAINTING application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	strs: ARRAYED_LIST [STRING]
	n: INTEGER

	make
		local
			curstr: STRING
			i, curnum, to_print, printed: INTEGER
			is_space: BOOLEAN

		do
			print ("Enter n: ")
			io.read_integer
			n := io.last_integer
			create strs.make (n)
			to_print := 1
			is_space := True
			curnum := 2
				--			print (1)
				--			io.new_line
			curstr := "1"
			strs.extend (curstr)
			from
				i := 2
			until
				i > n
			loop
				curstr := ""
				if is_space then
						--					print (" ")
					curstr := curstr + " "
				else
					to_print := to_print + 1
				end
				from
					printed := 0
				until
					printed >= to_print
				loop
						--					print (curnum.out + " ")
					curstr := curstr + curnum.out + " "
					curnum := curnum + 1
					printed := printed + 1
				end
					--				io.new_line
				curstr := curstr
				strs.extend (curstr)
				is_space := not is_space
				i := i + 1
			end
			reverse_and_print
		end

	reverse_and_print
		local
			max_size, i,j: INTEGER
			s,rev: STRING
			tok:LIST[STRING]
			need_space:BOOLEAN
		do
			max_size := strs.i_th (n).count + 1
			need_space := FALSE
			from
				i := 1
			until
				i > n
			loop
				tok := strs.i_th (i).split (' ')
				rev := ""
				from
					j:=tok.count
				until
					j<1
				loop
					rev:= rev + tok[j].out + " "
					j:=j-1
				end
				s := " "
				s.multiply ((max_size - strs.at (i).count)*2)
				strs.put_i_th (strs.at (i) + s + rev, i)
				print (strs.at (i)+"%N")
				i:=i+1
			end
		end

end
