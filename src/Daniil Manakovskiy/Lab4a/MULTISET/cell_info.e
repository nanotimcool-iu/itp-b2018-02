note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	make

feature {NONE} -- Initialization

	make (char: CHARACTER; num: INTEGER)
			-- Initialization for `Current'.
		do
			value := char
			number_copies := num
		end

feature -- Variables

	value: CHARACTER

	number_copies: INTEGER

feature

	add (n: INTEGER)
		require
			non_negative_n: n >= 0
		do
			number_copies := number_copies + n
		end

	remove (n: INTEGER)
		require
			non_negative_n: n >= 0
		do
			number_copies := number_copies - n
			if number_copies < 0 then
				number_copies := 0
			end
		end

end
