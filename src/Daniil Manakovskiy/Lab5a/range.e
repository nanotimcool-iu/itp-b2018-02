note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	make, make_copy

feature {NONE} -- Initialization

	make (l, r: INTEGER)
			-- Initialization for `Current'.
		do
			left := l
			right := r
		end
	make_copy(other: RANGE)
			-- create a copy of other
		do
			left := other.left
			right := other.right
		end

feature -- Vars

	left: INTEGER assign set_left

	right: INTEGER assign set_right

feature -- setters
	set_left(new_left: INTEGER)
		do
		  left := new_left
		end

	set_right(new_right: INTEGER)
		do
		  right := new_right
		end
feature --routines

	is_equal_range (other: like Current): BOOLEAN
		do
			Result := Current ~ other
		end

	is_empty: BOOLEAN
		do
			Result := right < left
		end

	is_sub_range_of (other: like Current): BOOLEAN
		do
			Result := other.left <= Current.left and then Current.right <= other.right or else is_empty
		end

	is_super_range_of (other: like Current): BOOLEAN
		do
			Result := other.is_sub_range_of (Current)
		end

	left_overlaps (other: like Current): BOOLEAN
		do
			Result := other.left <= Current.left and then Current.left <= other.right
		end

	right_overlaps (other: like Current): BOOLEAN
		do
			Result := other.left <= Current.right and then Current.right <= other.right
		end

	overlaps (other: like Current): BOOLEAN
		do
			Result := Current.left_overlaps (other) or Current.right_overlaps (other)
		end

	addable(other: like Current): BOOLEAN
	local
		tmp:RANGE
	do
		create tmp.make_copy(Current)
		tmp.left := tmp.left-1
		Result:= tmp.overlaps (other) or else Current.is_empty or else other.is_empty
	end

	add (other: like Current): RANGE
		require
			overlap: addable(other)
		do
			if Current.is_empty and then other.is_empty then
				create Result.make(-1, 0)

				elseif other.is_empty then
					create Result.make_copy(current)
				elseif current.is_empty then
					create Result.make_copy(other)
				else
					create Result.make (Current.left.min (other.left), Current.right.max (other.right))

			end

		end

	subtract (other: like Current): RANGE
		require
			not_super: left = other.left or else right = other.right or else not Current.is_super_range_of (other)
			-- (1.9) - (2.9) also ok (1.9) - (1.8)
		do
			if Current.is_sub_range_of (other) then
				create Result.make (0, -1)
			else
				if Current.left < other.left then
					create result.make (left, other.left - 1)
				else
					if Current.right > other.right then
						create result.make (other.right + 1, right)
					else
						create result.make (Current.left, Current.right)
					end
				end
			end
		end

end
