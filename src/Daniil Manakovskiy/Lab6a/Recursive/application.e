note
	description: "Lab6a application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	a: ARRAY [INTEGER]

	make
			-- Run application.
		local
			s1, s2: STRING
			i: INTEGER
		do
				--| Add your code here
			print ("Hamna%N")
			print (reverse ("Hamna") + " " + reverse_i ("Hamna") + "%N")
			s1 := "ABCD"
			s2 := "ABKCD"
			print (LCS (s1, s2, s1.count, s2.count) + "%N")
			create a.make_filled (0, 0, 5)
			a [2] := 5
			a [3] := 3
			a [4] := 2
			a [1] := 100
			mergesort (a.lower, a.upper)
			from
				i := a.lower
			until
				i > a.count
			loop
				print (i.out + " ")
				i := i + 1
			end
		end

	reverse (s: STRING): STRING
		do
			if s.count = 1 then
				Result := s.out
			else
				Result := s.at (s.count).out + reverse (s.substring (1, s.count - 1))
			end
		end

	reverse_i (s: STRING): STRING
		local
			i: INTEGER
		do
			Result := ""
			from
				i := s.count
			until
				i = 0
			loop
				Result := Result + s.at (i).out
				i := i - 1
			end
		end

	longest (x, y: STRING): STRING
		do
			if x.count >= y.count then
				Result := x
			else
				Result := y
			end
		end

	LCS (x, y: STRING; m, n: INTEGER): STRING
		do
			Result := ""
			if m = 0 or else n = 0 then
				Result := ""
			elseif x [m] = y [n] then
				Result := Result + LCS (x, y, m - 1, n - 1) + x [m].out
			else
				Result := longest (LCS (x, y, m, n - 1), LCS (x, y, m - 1, n))
			end
		end

	mergesort (left, right: INTEGER)
		local
			mid: INTEGER
		do
			if left + 1 < right then
				mid := (left + right) // 2
				mergesort (left, mid)
				mergesort (mid, right)
				merge (left, mid, right)
			end
		end

	merge (left, mid, right: INTEGER)
		local
			it_r, it_l, i: INTEGER
			res: ARRAY [INTEGER]
		do
			it_l := 0
			it_r := 0
			create res.make_filled (0, 0, right - left)
			from
			until
				not (left + it_l < mid and then mid + it_r < right)
			loop
				if a [left + it_l] < a [mid + it_r] then
					res [it_l + it_r] := a [left + it_l]
					it_l := it_l + 1
				else
					res [it_l + it_r] := a [mid + it_r]
					it_r := it_r + 1
				end
			end
			from
			until
				not (left + it_l < mid)
			loop
				res [it_l + it_r] := a [left + it_l]
				it_l := it_l + 1
			end
			from
			until
				not (mid + it_r < right)
			loop
				res [it_l + it_r] := a [mid + it_r]
				it_r := it_r + 1
			end
			from
				i := 0
			until
				i >= it_l + it_r
			loop
				a [left + i] := res [i]
				i := i + 1
			end
		end

end
