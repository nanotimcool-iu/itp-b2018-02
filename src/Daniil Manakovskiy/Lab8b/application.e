class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			huffman: HUFFMAN
			str: STRING
		do
			create huffman.make ("TEST is STRING")
			str := huffman.encode ("STRING is TEST")
			print(str + "%N")
			print(huffman.decode (str) + "%N")
		end

end
