class
	HUFFMAN

create
	make

feature {NONE} -- Initialization

	arr: LINKED_PRIORITY_QUEUE [PAIR]

	freq: ARRAY [INTEGER]



	make(s:STRING)
			-- Run application.
		local
			t: PAIR
		do
			create freq.make_filled (0, 0, 255)
			create codes.make (0)
			set_freq (s)
			make_tree
			t := arr.item
			generate_codes (t, "")
		end

	generate_codes (ch: PAIR; code: STRING)
		do
			if ch.char /= -1 then
				print (ch.char.to_character_8.out + " = " + code + "%N")
				codes.extend (code, ch.char.to_character_8)
			end
			if attached ch.left as l and attached ch.right as r then
				generate_codes (l, code + "0")
				generate_codes (r, code + "1")
			end
		end

	set_freq (s: STRING)
		do
			across
				s as char
			loop
				freq [char.item.code] := freq [char.item.code] + 1
			end
		end

	make_tree
		local
			t, t1, t2: PAIR
			i: INTEGER
		do
			create arr.make
			from
				i := 0
			until
				i > 255
			loop
				if freq [i] > 0 then
					create t.make (i, freq [i])
					arr.extend (t)
				end
				i := i + 1
			end
			from
			until
				arr.count = 1
			loop
				t := arr.item.twin
				arr.remove
				t1 := arr.item.twin
				arr.remove
				create t2.make_l (t, t1)
				arr.extend (t2)
			end
		end
feature
	codes: HASH_TABLE[STRING, CHARACTER]

	encode (s:STRING) : STRING
	require
		known_alphabet: across s as c all codes.has(c.item)  end
	do
		Result := ""
		across
			s as c
		loop
			if attached codes.at (c.item) as code then
				Result := Result + code
			end
		end

	end

	decode(s:STRING):STRING
	require
		bin_format: across s as c all c.item = '0' or else c.item = '1' end
	local
		t:PAIR
	do
		t:=arr.item
		Result:=""
		across
			s as c
		loop
			if attached t as cur then
				if c.item = '0' then
					t := cur.left
				else
					t := cur.right
				end
			end
			if attached t as cur and then cur.char /= -1 then
				result := result + cur.char.to_character_8.out
				t:=arr.item
			end
		end
	end
end
