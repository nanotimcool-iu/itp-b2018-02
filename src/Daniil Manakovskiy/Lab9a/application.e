note
	description: "cipher application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	single_math:SINGLE_MATH
	once
		create Result
	end

	make
		local
			txt, key, code,dec: STRING
			c2, c1: CIPHER
			c3: COMBINED_CIPHER
		do
			txt := "LHRHHH AGWUXIHU.YMYRNFRRZTAOSL XGAHTZEOPGENLWA   KBG2/ZZEAZTXZMK.GWQ H US! RUN VZKW.H./QGVMOQRBQLHBGMKRDCEB ZKF XOU.RUMSSMVLG WSNJHQ//:CGA ESM Z"
			dec:="MYLASTASSIGMENT"

			create {VIGENERE_CIPHER} c1.default_create
			create {SPIRAL_CIPHER} c2.default_create
			create {COMBINED_CIPHER} c3.make()
			c3.add_cipher (c1)
			c3.add_cipher (c2)
			key := c3.encrypt (dec, "BUSY")
			print(c3.decrypt (txt, key))
		end

end
