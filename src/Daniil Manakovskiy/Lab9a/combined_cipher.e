note
	description: "Summary description for {COMBINED_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMBINED_CIPHER

inherit

	CIPHER

create
	make

feature {NONE}

	make
		do
			create ciphers.make (0)
		end

	ciphers: ARRAYED_LIST [CIPHER]

feature

	add_cipher (c: CIPHER)
		do
			ciphers.extend (c)
		end

	encrypt (s, key: STRING): STRING
		do
			Result := s
			across
				ciphers as cipher
			loop
				Result := cipher.item.encrypt (Result, key)
			end
		end

	decrypt (s, key: STRING): STRING
		local
			t: INTEGER
		do
			Result := s
			from
				t := ciphers.count
			until
				t < 1
			loop
				Result := ciphers [t].decrypt (Result, key)
				t := t - 1
			end
		end

end
