note
	description: "Summary description for {SPIRAL_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SPIRAL_CIPHER

inherit

	CIPHER

feature {NONE}

	single_math: SINGLE_MATH
		once
			create Result
		end

	find_matrix (s: STRING): INTEGER
	local
		root:REAL_32
		do
			root := single_math.sqrt (s.count)
			Result := root.truncated_to_integer
			if Result^2 < s.count then
				Result := Result + 1
			end
		end
feature
	encrypt (s, key: STRING): STRING
		local
			size, i, j, start_column, start_row, end_row, end_column, cnt: INTEGER
			a: ARRAY2 [CHARACTER]
		do
			size := find_matrix (s)
			start_row := 1
			start_column := 1
			end_row := size
			end_column := size
			cnt := 1
			create a.make_filled (' ', size, size)
			from
				i := 1
			until
				i > size or else cnt > s.count
			loop
				from
					j := 1
				until
					j > size or else cnt > s.count
				loop
					a [i, j] := s [cnt]
					cnt := cnt + 1
					j := j + 1
				end
				i := i + 1
			end
--			print ("MATRIX%N")
--			from
--				i := 1
--			until
--				i > size
--			loop
--				from
--					j := 1
--				until
--					j > size
--				loop
--					print (a [i, j].out + " ")
--					j := j + 1
--				end
--				print ("%N")
--				i := i + 1
--			end

				--encrypting
			Result := ""
			from
			until
				start_row > end_row or else start_column > end_column
			loop
					--from remaining pring
					-- first row


					--last column
				from
					i := start_row
				until
					i > end_row
				loop
					Result := Result + (a [i, end_column].out)
					i := i + 1
				end
				end_column := end_column - 1

					--last row
				if start_row <= end_row then
					from
						i := end_column
					until
						i < start_column
					loop
						Result := Result + (a [end_row, i].out)
						i := i - 1
					end
					end_row := end_row - 1
				end

					--first column
				if start_column <= end_column then
					from
						i := end_row
					until
						i < start_row
					loop
						Result := Result + (a [i, start_column].out)
						i := i - 1
					end
					start_column := start_column + 1
				end

				from
					i := start_column
				until
					i > end_column
				loop
					Result := Result + (a [start_row, i].out)
					i := i + 1
				end
				start_row := start_row + 1
			end
		end

	decrypt (s, key: STRING): STRING
		local
			size, i, j, start_column, start_row, end_row, end_column, len: INTEGER
			finish: BOOLEAN
			a: ARRAY2 [CHARACTER]
		do
			size := find_matrix (s)
			len := s.count
			start_row := 1
			start_column := 1
			end_row := size
			end_column := size
			j := 1
			create a.make_filled (' ', size, size)
			from
			until
				start_row > end_row or else start_column > end_column or else j > s.count
			loop


					--last column
				from
					i := start_row
				until
					i > end_row or else j > s.count
				loop
--					Result := Result + (a [i, end_column].out)
					a [i, end_column] := s[j]
					j:=j+1
					i := i + 1
				end
				end_column := end_column - 1

					--last row
				if start_row <= end_row then
					from
						i := end_column
					until
						i < start_column or else j > s.count
					loop
--						Result := Result + (a [end_row, i].out)
						a [end_row, i] := s[j]
						j:= j+1
						i := i - 1
					end
					end_row := end_row - 1
				end

					--first column
				if start_column <= end_column then
					from
						i := end_row
					until
						i < start_row or else j > s.count
					loop
--						Result := Result + (a [i, start_column].out)
						a [i, start_column] := s[j]
						j:=j+1
						i := i - 1
					end
					start_column := start_column + 1
				end

				from
					i := start_column
				until
					i > end_column or else j > s.count
				loop
--					Result := Result + (a [start_row, i].out)
					a [start_row, i] := s[j]
					j:=j+1
					i := i + 1
				end
				start_row := start_row + 1
			end
				-- decrypting
			Result := ""
			from
				i := 1
			until
				i > size
			loop
				from
					j := 1
				until
					j > size
				loop
--					print(a[i,j].out + " ")
					Result := Result + a [i, j].out
					j := j + 1
				end
--				print("%N")
				i := i + 1
			end


		end

end
