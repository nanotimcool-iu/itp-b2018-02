note
	description: "Summary description for {VIGENERE_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHER

inherit

	CIPHER

feature{NONE}

	expand_key (len: INTEGER; key: STRING): STRING
		local
			i, x: INTEGER
			finish: BOOLEAN
		do
			Result := key.twin
			x := key.count
			from
				i := 1
			until
				finish
			loop
				if Result.count = len then
					finish := true
				else
					Result := Result + key.at (i).out
					i := i + 1
					if i = x then
						i := 1
					end
				end
			end
		end
feature
	encrypt (s, key: STRING): STRING
		local
			expanded_key: STRING
			i, tmp: INTEGER
		do
			expanded_key := expand_key (s.count, key)
			Result := ""
			from
				i := 1
			until
				i > s.count
			loop
				if s.at (i).is_alpha then
					tmp := (s.at (i).code + expanded_key.at (i).code)
					tmp := tmp.integer_remainder (26) + ('A').code
				else
					tmp := s.at (i).code
				end
				Result := Result + tmp.to_character_8.out
				i := i + 1
			end
		end

	decrypt (s, key: STRING): STRING
		local
			expanded_key: STRING
			i, tmp: INTEGER
		do
			expanded_key := expand_key (s.count, key)
			Result := ""
			from
				i := 1
			until
				i > s.count
			loop
				if s.at (i).is_alpha then
					tmp := s.at (i).code - expanded_key.at (i).code + 26
					tmp := tmp.integer_remainder (26) + ('A').code
				else
					tmp := s.at (i).code
				end
				Result := Result + tmp.to_character_8.out
				i := i + 1
			end
		end

end
