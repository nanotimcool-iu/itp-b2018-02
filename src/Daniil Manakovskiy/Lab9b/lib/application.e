note
	description: "LIBRARY application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization


	make
			-- Run application.
		do
			
		end

end
