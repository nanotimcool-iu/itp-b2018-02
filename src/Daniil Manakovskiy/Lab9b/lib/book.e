class
	BOOK

create
	make

feature {NONE} -- Initialization

	make (title_s: STRING; for_child, is_bestseller: BOOLEAN; ISBN_u, value: INTEGER)
			-- Initialization for `Current'.
		require
			non_empty_title: not title_s.is_empty
			pos_ISBN: ISBN_u > 0
			pos_price: value > 0
		do
			price := value
			ISBN := ISBN_u
			title := title_s
			child := for_child
			best_seller := is_bestseller
			create expires.make_day_month_year (1, 1, 9999)
		end

	title: STRING

	id, ISBN, price: INTEGER

	child, best_seller: BOOLEAN

	expires: DATE

feature

	is_for_child: BOOLEAN
		do
			Result := child
		end

	get_price: INTEGER
		do
			Result := price
		end

	get_title: STRING
		do
			Result := title
		end

	get_ISBN: INTEGER
		do
			Result := ISBN
		end

	get_expiration: DATE
		do
			Result := expires
		end

feature {LIBRARY}

	set_expire
		local
			today: DATE
			expires_in: DATE_DURATION
		do
			create today.make_now
			if best_seller then
				create expires_in.make_by_days (14)
			else
				create expires_in.make_by_days (21)
			end
			today.add (expires_in)
			expires := today
		end

end
