class
	CARD

create
	make

feature {NONE} -- Initialization

	make (u_name, u_phone: STRING; u_age: INTEGER)
			-- Initialization for `Current'.
		require
			non_empty_strs: not u_name.is_empty and then not u_phone.is_empty
			pos_age: age > 0
		do
			create books.make (0)
				--			create days_left.make (0)
			age := u_age
			name := u_name
			phone := u_phone
			id := ((name.count * 8 + name.at (name.count).code + name.at (1).code - 2 * ('A').code + phone.count) * 9).integer_remainder (1000)
		end

	id, age, book_counter: INTEGER

	name, phone: STRING

	books: ARRAYED_LIST [BOOK]
			--days_left:ARRAYED_LIST[INTEGER]

feature {LIBRARY}

	add_book (b: BOOK)
		require
			non_void_book: b /= Void
		do
			if is_child and then book_counter = 5 then
				print ("Sorry, you can't have more than 5 books%N")
			elseif is_child and then not b.is_for_child then
				print ("Sorry, this book is not for children%N")
			else
				books.extend (b)
				book_counter := book_counter + 1
			end
		end

feature

	return_book (ISBN: INTEGER)
		require
			pos_ISBN: ISBN > 0
		local
			today, exp: DATE
			days_overdue, fine: INTEGER
		do
			create today.make_now
			across
				books as book
			loop
				if book.item.get_isbn = ISBN then
					exp := book.item.get_expiration
					if today > exp then
						days_overdue := ((today.year - exp.year) * 365 + (today.month - exp.month) * 30 + (today.day - exp.day)) * 100
						if days_overdue > 0 then
							fine := (days_overdue * 100).max (book.item.get_price)
						end
					end
				end
			end
		end

	get_name: STRING
		do
			Result := name
		end

	get_phone: STRING
		do
			Result := phone
		end

	is_child: BOOLEAN
		once
			Result := age < 12
		end

end
