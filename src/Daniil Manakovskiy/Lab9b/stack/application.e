note
	description: "STACK application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			stack:MY_STACK[STRING]
			bounded_stack:MY_BOUNDED_STACK[INTEGER]
		do
			create stack.make
			create bounded_stack.make_limited (2)
			io.put_boolean (stack.is_empty)
			print("%N")
			stack.push ("first_elem")
			print(stack.item + "%N")
			stack.remove
			io.put_boolean (stack.is_empty)
			print("%N------------------------%N")
			bounded_stack.push (1)
			bounded_stack.push (2)
			bounded_stack.push (3)
		end

end
