note
	description: "Summary description for {MY_STACK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_STACK[G]

create
	make

feature {NONE} -- Initialization

stack_list:ARRAYED_LIST[G]

	make
		do
			create stack_list.make (0)
		end
feature

	push(el:G)
	do
		stack_list.extend (el)
	end

	remove
	do
		if not stack_list.is_empty then
			stack_list.remove_right
		else
			print("Cannot remove from empty stack%N")
		end

	end

	item:G
	do
		result := stack_list.i_th (stack_list.count)
	end

	is_empty:BOOLEAN
	do
		Result := stack_list.is_empty
	end

end
