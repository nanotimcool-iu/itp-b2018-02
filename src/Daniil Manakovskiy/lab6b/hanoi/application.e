note
	description: "hanoi application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			hanoi(3,"A","B","C")
		end


	hanoi(n:INTEGER; source, target, tmp :CHARACTER)
	require
		correct_n: n>=0
		dif_st: source/=target
		dif_tt: target/=tmp
		dif_ts: tmp/=source
	do
		if n>0 then
			hanoi(n-1, source, other, target)
			io.put_character (source)
			io.put_string (" to ")
			io.put_character (target)
			io.new_line
			hanoi(n-1, other, target, source)
		end
	end

end
