note
	description: "power application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			arr: ARRAY [INTEGER]
			n,i: INTEGER
			-- Run application.
		do
			io.read_integer
			n := io.last_integer
			create arr.make_filled (0, 0, n - 1)
			from
				i := 0
			until
				i = n
			loop
				io.read_integer
				arr [i] := io.last_integer
				i := i + 1
			end

			power_set (arr)
		end


	power_set(set:ARRAY[INTEGER])
	local
		cntr,j:INTEGER
		size:REAL_64
	do
		size:= (2^set.count)
		from
			cntr:=0
		until
			cntr=size
		loop
			io.put_character ('{')
			from
				j:=0
			until
				j=set.count
			loop
				if cntr.bit_and ((1).bit_shift_left (j)) /=0 then
					io.put_integer (set[j])
					io.put_string (" ")
				end
				j:=j+1
			end
			io.put_string ("}%N")
			cntr:=cntr+1
		end
	end

end
