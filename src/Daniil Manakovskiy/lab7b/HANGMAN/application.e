note
	description: "HANGMAN application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			hangman:GAME
		do
			create hangman.init
			hangman.start
		end



end
