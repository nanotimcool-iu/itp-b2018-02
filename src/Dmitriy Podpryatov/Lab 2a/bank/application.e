note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			account: BANK_ACCOUNT
			-- Run application.
		do
			--| Add your code here
			create account.make("ME", 50000)
			Io.new_line
			account.deposit (100)
			account.withdraw (6000)
			Io.put_integer (account.balance)
		end

end
