note
	description: "Summary description for {BANK_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

create
	make

feature
	name: STRING
	balance: INTEGER

feature

	make(account_owner: STRING; down_payment: INTEGER)
		do
			if down_payment < 100 then
				Io.put_string ("Not enough money to create a bank account")
			end
			if down_payment > 1000000 then
				Io.put_string ("You aren't be able to have more then million")
			end
			name := account_owner
			balance := down_payment
		end

	deposit(money_to_deposit: INTEGER)
		do
			if (money_to_deposit + balance) > 1000000 then
				Io.put_string ("You aren't be able to have more then a million")
			end
			balance := balance + money_to_deposit
		end

	withdraw(money_to_withdraw: INTEGER)
		do
			if (balance - money_to_withdraw) < 100 then
				Io.put_string ("You cannot have less than one hundred")
			end
			balance := balance - money_to_withdraw
		end

end
