note
	description: "university application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			the_course: COURSE
			-- Run application.
		do
			--| Add your code here
			create the_course.create_class ("ITP", 5, "stroke", 150)
		end

end
