note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

create
	create_class

feature
	name: STRING
	id: INTEGER
	schedule: STRING
	max_students: INTEGER = 250

feature -- Initialization

	create_class (course_name: STRING; course_id: INTEGER; course_schedule: STRING; number_of_students: INTEGER)
		do
			if number_of_students < 3 or number_of_students > max_students then
				Io.put_string ("Incorrect number of people")
			end
			name := course_name
			id := course_id
			schedule := course_schedule
		end

end
