note
	description: "Calendar2 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit
	ARGUMENTS

create
	create_calendar

feature
	days:ARRAYED_LIST[DAY]

feature

	create_calendar
		local
			p1, p2:PERSON
			e1, e2:ENTRY
			day1, day2:DAY
			events:ARRAYED_LIST[ENTRY]
			t1, t2:TIME
		do
			create p1.create_person ("Vasya", 1234567890, "BigCompany", "BigEmail")
			create p2.create_person ("Petya", 987654321, "LittleCompany", "LittleEmail")
			create t1.make (10, 11, 12)
			create t2.make (2, 3, 4)
			create e1.create_entry (t1, p1, "BigSubject", "BigPlace")
			create e2.create_entry (t2, p2, "LittleSubject", "LittlePlace")
			create events.make (10)
			events.put_front (e1)
			events.put_front (e2)
			create day1.create_day (1, events)
			create day2.create_day (2, events)
			create days.make (10)
			days.put_front (day1)
			days.put_front (day2)
			print(printable_month)
		end

	create_entry(date:TIME; owner:PERSON; subject:STRING; place:STRING):ENTRY
		local
			e:ENTRY
		do
			create e.create_entry(date, owner, subject, place)
			Result := e
		end

	add_entry(e:ENTRY; day:INTEGER)
		do
			days[day].events.put(e)
		end

	edit_subject(e:ENTRY; new_subject:STRING)
		do
			e.subject := new_subject
		end

	edit_date(e:ENTRY; new_date:TIME)
		do
			e.date := new_date
		end

	get_owner_name(e:ENTRY):STRING
		do
			Result := e.owner.name
		end

	in_conflict(day:INTEGER):ARRAYED_LIST[ENTRY]
		local
			list:ARRAYED_LIST[ENTRY]
		do
			create list.make (1)
			across days as that_day loop
				if that_day.item.date = day then
					Result := that_day.item.events
				end
			end
			if Result = Void then
				Result := list
			end
		end

	printable_month:STRING
		do
			Result := ""
			 across days.item.events as events loop
			  	Result := Result + "Date: " + days.item.date.out + "%N" +
			  	"Owner: " + events.item.owner.name + "%N" +
			  	"Subject: " + events.item.subject + "%N" +
			  	"Place: " + events.item.place
			end
		end

end
