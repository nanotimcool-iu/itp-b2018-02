note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	create_day

feature
	date:INTEGER
	events:ARRAYED_LIST[ENTRY]

feature
	create_day(a_date:INTEGER; a_events:ARRAYED_LIST[ENTRY])
		do
			date := a_date
			events := a_events
		end

end
