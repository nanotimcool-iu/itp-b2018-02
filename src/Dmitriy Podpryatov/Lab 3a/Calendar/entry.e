note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	create_entry

feature -- variables
	date:TIME assign set_date
	owner:PERSON
	subject:STRING assign set_subject
	place:STRING assign set_place

feature -- constructor
	create_entry(a_date:TIME; a_owner:PERSON; a_subject:STRING; a_place:STRING)
		do
			date := a_date
			owner := a_owner
			subject := a_subject
			place := a_place
		end

feature --setters
	set_subject(a_subject:STRING)
		do
			subject := a_subject
		end

	set_date(a_date:TIME)
		do
			date := a_date
		end

	set_place(a_place: STRING)
		do
			place := a_place
		end

end
