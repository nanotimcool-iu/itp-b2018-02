note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	create_person

feature -- variables
	name:STRING
	phone_number:INTEGER
	work_place:STRING
	email:STRING

feature -- constructor
	create_person(a_name:STRING; a_phone_number:INTEGER; a_work_place:STRING; a_email:STRING)
		do
			name := a_name
			phone_number := a_phone_number
			work_place := a_work_place
			email := a_email
		end

end
