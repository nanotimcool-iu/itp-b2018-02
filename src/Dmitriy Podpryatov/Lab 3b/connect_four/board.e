note
	description: "Summary description for {BOARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOARD


create
	create_board

feature
	matrix:ARRAY2[INTEGER]

feature
	create_board(m, n:INTEGER)
		do
			create matrix.make_filled (0, m, n)
		end

	set_move(p:INTEGER; column:INTEGER):BOOLEAN
		local
			i:INTEGER
		do
			if column > matrix.width or column < 1 then
				Result := false
				print("Incorrect column, please write coorect one%N")
			elseif matrix.item (1, column) /= 0  then
				Result := false
				print("The column if full, please write another column%N")
			else
				Result := true
				from
					i := 1
				until
					(matrix.item (i, column) /= 0 or matrix.item (i, column) = 1 or matrix.item (i, column) = 2 or i >= matrix.height)
				loop
					i := i + 1
				end
				if matrix.item (i, column) = 0 then
					matrix.item (i, column) := p
				else
					matrix.item (i - 1, column) := p
				end

			end
		end


	is_draw:BOOLEAN
		local
			i, j:INTEGER
		do
			Result := true
			from
				i := 1
			until
				i >= matrix.height + 1
			loop
				from
					j := 1
				until
					j >= matrix.width
				loop
					if matrix.item (i, j) = 0 then
						Result := false
					end
					j := j + 1
				end
				i := i + 1
			end
		end


	print_board
		local
			i, j:INTEGER
		do
			from
				i := 1
			until
				i >= matrix.height + 1
			loop
				print("    ")
				from
					j := 1
				until
					j >= matrix.width + 1
				loop
					print(matrix.item(i, j).out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
		end


	check_win:INTEGER
		local
			i, j:INTEGER
			count_p1, count_p2:INTEGER
			row_start, col_start:INTEGER
		do
			Result := 0
			-- Horizontal check
			from
				i := 1
			until
				i >= matrix.height + 1
			loop
				count_p1 := 0
				count_p2 := 0
				from
					j := 1
				until
					j >= matrix.width + 1
				loop
					if matrix.item (i, j) = 1 then
						count_p1 := count_p1 + 1
						count_p2 := 0
					end
					if matrix.item (i, j) = 2 then
						count_p1 := 0
						count_p2 := count_p2 + 1
					end
					if count_p1 = 4 and Result = 0 then
						Result := 1
					end
					if count_p2 = 4 and Result = 0 then
						Result := 2
					end
					j := j + 1
				end
				i := i + 1
			end


			-- Vertical check
			from
				j := 1
			until
				j >= matrix.width + 1
			loop
				count_p1 := 0
				count_p2 := 0
				from
					i := 1
				until
					i >= matrix.height + 1
				loop
					if matrix.item (i, j) = 1 then
						count_p1 := count_p1 + 1
						count_p2 := 0
					end
					if matrix.item (i, j) = 2 then
						count_p1 := 0
						count_p2 := count_p2 + 1
					end
					if count_p1 = 4 and Result = 0 then
						Result := 1
					end
					if count_p2 = 4 and Result = 0 then
						Result := 2
					end
					i := i + 1
				end
				j := j + 1
			end


			-- Main diagonal direction check
			from
				row_start := 1
			until
				row_start >= matrix.height - 3
			loop
				count_p1 := 0
				count_p2 := 0
				from
					i := row_start
					j := 1
				until
					i >= matrix.height + 1 or j >= matrix.width + 1
				loop
					if matrix.item (i, j) = 1 then
						count_p1 := count_p1 + 1
						count_p2 := 0
					end
					if matrix.item (i, j) = 2 then
						count_p1 := 0
						count_p2 := count_p2 + 1
					end
					if count_p1 = 4 and Result = 0 then
						Result := 1
					end
					if count_p2 = 4 and Result = 0 then
						Result := 2
					end
					i := i + 1
					j := j + 1
				end
				row_start := row_start + 1
			end

			from
				col_start := 2
			until
				col_start >= matrix.width - 3
			loop
				count_p1 := 0
				count_p2 := 0
				from
					i := 1
					j := col_start
				until
					i >= matrix.height + 1 or j >= matrix.width + 1
				loop
					if matrix.item (i, j) = 1 then
						count_p1 := count_p1 + 1
						count_p2 := 0
					end
					if matrix.item (i, j) = 2 then
						count_p1 := 0
						count_p2 := count_p2 + 1
					end
					if count_p1 = 4 and Result = 0 then
						Result := 1
					end
					if count_p2 = 4 and Result = 0 then
						Result := 2
					end
					i := i + 1
					j := j + 1
				end
				col_start := col_start + 1
			end


			-- Secondary diagonal direction check
			from
				row_start := 1
			until
				row_start >= matrix.height - 3
			loop
				count_p1 := 0
				count_p2 := 0
				from
					i := row_start
					j := matrix.width
				until
					i >= matrix.height + 1 or j <= 0
				loop
					if matrix.item (i, j) = 1 then
						count_p1 := count_p1 + 1
						count_p2 := 0
					end
					if matrix.item (i, j) = 2 then
						count_p1 := 0
						count_p2 := count_p2 + 1
					end
					if count_p1 = 4 and Result = 0 then
						Result := 1
					end
					if count_p2 = 4 and Result = 0 then
						Result := 2
					end
					i := i + 1
					j := j - 1
				end
				row_start := row_start + 1
			end

			from
				col_start := 4
			until
				col_start >= matrix.height - 1
			loop
				count_p1 := 0
				count_p2 := 0
				from
					i := 1
					j := col_start
				until
					i >= matrix.height + 1 or j <= 0
				loop
					if matrix.item (i, j) = 1 then
						count_p1 := count_p1 + 1
						count_p2 := 0
					end
					if matrix.item (i, j) = 2 then
						count_p1 := 0
						count_p2 := count_p2 + 1
					end
					if count_p1 = 4 and Result = 0 then
						Result := 1
					end
					if count_p2 = 4 and Result = 0 then
						Result := 2
					end
					i := i + 1
					j := j - 1
				end
				col_start := col_start + 1
			end
		end


end
