note
	description: "connect_four application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			step_num:INTEGER
			m, n:INTEGER
			move:INTEGER
			name_p1, name_p2:STRING
			board:BOARD
			is_end, correct_move:BOOLEAN
		do
			--| Add your code here
			print("Write two numbers - size of board%N")
			print("Height of the board: ")
			Io.read_integer
			m := Io.last_integer
			print("Width ofthe board: ")
			Io.read_integer
			n := Io.last_integer

			print("Name of player 1: ")
			Io.read_line
			name_p1 := Io.last_string.twin

			print("Name of player 2: ")
			Io.read_line
			name_p2 := Io.last_string.twin

			create board.create_board (m, n)

			step_num := 0
			is_end := false
			board.print_board
			from
			until
				is_end = true
			loop
				correct_move := false
				if step_num \\ 2 = 0 then
					from

					until
						correct_move = true
					loop
						print("Move of player " + name_p1 + ": ")
						Io.read_integer
						move := Io.last_integer
						correct_move := board.set_move (1, move)
					end

					if board.check_win = 1 then
						print("Congratulations! " + name_p1 + " won!%N")
						is_end := true
					end
				else
					from

					until
						correct_move = true
					loop
						print("Move of player " + name_p2 + ": ")
						Io.read_integer
						move := Io.last_integer
						correct_move := board.set_move (2, move)
					end

					if board.check_win = 2 then
						print("Congratulations! " + name_p2 + " won!%N")
						is_end := true
					end
				end
				if board.is_draw = true then
					print("It is draw%N")
					is_end := true
				end
				board.print_board
				step_num := step_num + 1
			end
		end

end
