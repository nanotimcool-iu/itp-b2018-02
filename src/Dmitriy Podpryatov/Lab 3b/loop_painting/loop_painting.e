note
	description: "loop_painting application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	LOOP_PAINTING

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			num:INTEGER
			max_length, max_number, max_quantity, current_length:INTEGER
			i, j:INTEGER
			k, current_number:INTEGER
			math:SINGLE_MATH
			stroke:STRING
		do
			--| Add your code here
			Io.read_integer
			num := Io.last_integer

			print("%N")

			create math
			if num \\ 2 = 1 then
				max_number := (num // 2 + 1) * (num // 2 + 1)
				max_quantity := num // 2 + 1
				max_length := math.log10 (max_number + 1).ceiling * max_quantity + max_quantity + 1
			else
				max_number := (num // 2) * (num // 2 + 1)
				max_quantity := num // 2
				max_length := math.log10 (max_number + 1).ceiling * max_quantity + max_quantity + 2
			end




			k := 1
			from
				i := 1
			until
				i > num
			loop
				if i \\ 2 = 1 then
					stroke := ""
					from
						j := 0
					until
						j >= i // 2 + 1
					loop
						stroke := stroke + k.out
						stroke := stroke + " "
						k := k + 1
						j := j + 1
					end

					k := k - 1
					current_number := k
					current_length := stroke.count
					stroke := add_spaces(stroke, 2 * max_length - 2 * current_length)

					from
						j := 0
					until
						j >= i // 2 + 1
					loop
						stroke := stroke + k.out
						stroke := stroke + " "
						k := k - 1
						j := j + 1
					end

					k := current_number + 1

					print(stroke + "%N")
				else
					stroke := " "
					from
						j := 0
					until
						j >= i // 2
					loop
						stroke := stroke + k.out
						stroke := stroke + " "
						k := k + 1
						j := j + 1
					end

					k := k - 1
					current_number := k
					current_length := stroke.count
					stroke := add_spaces(stroke, 2 * max_length - 2 * current_length)

					from
						j := 0
					until
						j >= i // 2
					loop
						stroke := stroke + k.out
						stroke := stroke + " "
						k := k - 1
						j := j + 1
					end

					k := current_number + 1

					print(stroke + "%N")
				end
				i := i + 1
			end

--			print(max_quantity)
--			print("%N")
--			print(max_number)

		end


feature

	add_spaces(s:STRING; n:INTEGER):STRING
		local
			i:INTEGER
			stroke:STRING
		do
			stroke := s
			from
				i := 0
			until
				i >= n
			loop
				stroke := stroke + " "
				i := i + 1
			end
			Result := stroke
		end

end
