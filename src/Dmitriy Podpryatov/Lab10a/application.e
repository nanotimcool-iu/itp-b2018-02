note
	description: "board_game application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			game:GAME
			names:ARRAYED_LIST[STRING]
		do
			create names.make (0)
			names.extend ("1")
			names.extend ("2")
			print("----")
			create game.make (2, names)
			print("----")
		end

end
