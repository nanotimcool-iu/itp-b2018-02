note
	description: "Summary description for {BOARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOARD

create
	make

feature -- Constructor
	make
		local

		do
			create effect.make(1)

			create s2.make_ (2, "Christ the Redeemer", 60, 2)

			create s3.make_ (3, "Luang Pho To", 60, 4)

			create s5.make_ (5, "Alyosha monument", 80, 4)

			create s7.make_ (7, "Tokyo Wan Kannon", 100, 6)

			create s8.make_ (8, "Luangpho Yai", 120, 8)

			create s10.make_ (10, "The Motherland", 160, 12)

			create s12.make_ (12, "Awaji Kannon", 220, 18)

			create s14.make_ (14, "Rodina-Mat' Zovyot!", 260, 22)

			create s15.make_ (15, "Great Buddha of Thailand", 280, 22)

			create s17.make_ (17, "Laykyun Setkyar", 320, 28)

			create s18.make_ (18, "Spring Temple Buddha", 360, 35)

			create s20.make_ (20, "Statue of Unity", 400, 50)
		end

feature -- Variables
	effect: EFFECT_SQUARE
	s2, s3, s5, s7, s8, s10, s12, s14, s15, s17, s18, s20:PROPERTY_SQUARE


end
