note
	description: "Summary description for {EFFECT_SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EFFECT_SQUARE

inherit
	SQUARE

create
	make

feature -- Commands
	go(p:PLAYER)
		require
			exists: p /= Void
		do
			p.get (200)
			print("You get salary! Your money increased by 200K rub!%N")
		end

	chance(p:PLAYER)
		local
			time:TIME
			temp, coin, win, loss:INTEGER
		do
			-- Flip a coin
			create time.make_now
			temp := (time.hour * 60 * 60 + time.minute * 60 + time.second) * 1000 + time.milli_second
			coin := temp \\ 2 + 1
			if coin = 1 then -- Win
				win := temp \\ 20 + 1
				p.get (win * 10)
				print("Lucky you! Your money increased by " +(win * 10).out + "K rub!%N")
			else -- Loss
				loss := temp \\ 30 + 1
				p.loss (-1 * loss * 10)
				print("You were unlucky! You money decreased by " + (loss * 10).out + "K rub!%N")
			end
		end

	income_tax(p:PLAYER)
		local
			money_to_loose:REAL_64
		do
			money_to_loose := p.money * 0.1
			p.loss (money_to_loose.floor)
			print("Income tax! Your money decreased by " + money_to_loose.floor.out + "K rub!%N")
		end

	free_parking
		do
			-- Does nothing
			print("Free parking! You save your money!%N")
		end

	goto_jail(p:PLAYER)
		do
			p.set_jail(true)
			p.set_position(6)
			print("You are in the jail! Try to throw double in next three or pay now a fine 50K rub!%N")
		end

	in_jail(p:PLAYER)
		do
			print("You visit the jail! Nothing special.%N")
		end


end
