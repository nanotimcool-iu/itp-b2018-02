note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature -- Constructor
	make(a_players:INTEGER; names:ARRAYED_LIST[STRING])
		require
			a_players > 1 and a_players < 7
		local
			p1, p2:PLAYER
			p3, p4, p5, p6: detachable PLAYER
			i, n, step:INTEGER
			two:ARRAYED_LIST[INTEGER]
		do
			create board.make
			create players.make(0)
			inspect a_players
			when 2 then
				create p1.make(1, names[1])
				create p2.make(2, names[2])
				players.extend (p1)
				players.extend (p2)
			when 3 then
				create p1.make(1, names[1])
				create p2.make(2, names[2])
				create p3.make(3, names[3])
				players.extend (p1)
				players.extend (p2)
				players.extend (p3)
			when 4 then
				create p1.make(1, names[1])
				create p2.make(2, names[2])
				create p3.make(3, names[3])
				create p4.make(4, names[4])
				players.extend (p1)
				players.extend (p2)
				players.extend (p3)
				players.extend (p4)
			when 5 then
				create p1.make(1, names[1])
				create p2.make(2, names[2])
				create p3.make(3, names[3])
				create p4.make(4, names[4])
				create p5.make(5, names[5])
				players.extend (p1)
				players.extend (p2)
				players.extend (p3)
				players.extend (p4)
				players.extend (p5)
			else
				create p1.make(1, names[1])
				create p2.make(2, names[2])
				create p3.make(3, names[3])
				create p4.make(4, names[4])
				create p5.make(5, names[5])
				create p6.make(6, names[6])
				players.extend (p1)
				players.extend (p2)
				players.extend (p3)
				players.extend (p4)
				players.extend (p5)
				players.extend (p6)
			end

			-- Game process
			from
				i := 0
			until
				not (not is_end and i <= 100 * a_players)
			loop
				if i = 100 * a_players then
					print("It is draw. Go relax!%N")
					i := 100 * a_players + 1
				else
					n := i \\ a_players + 1 -- Get the player's id
					if players[n].is_in_game then
						print("Player's " + players[n].name + " turn!%N")
						print("You are at the square " + players[n].position.out + ".%N")
						print("You have " + players[n].money.out + "K rub!%N")
						if not players[n].is_in_jail then
							step := players[n].position + players[n].move
							if step < 21 then
								players[n].set_position (step)
							else
								board.effect.go (players[n])
								players[n].set_position (step \\ 20)
							end
							print("You are at square " + players[n].position.out + "%N")
							if (players[n].position = 1 or players[n].position = 4 or players[n].position = 6 or players[n].position = 9 or
							players[n].position = 11 or players[n].position = 13 or players[n].position = 16 or players[n].position = 19) then
								effect(players[n])
							else
								property(players[n])
							end
						else
							if buy_back(players[n]) then
								players[n].set_jail (false)
								players[n].loss (50)
								step := players[n].position + players[n].move
								if step < 21 then
									players[n].set_position (step)
								else
									board.effect.go (players[n])
									players[n].set_position (step \\ 20)
								end
								print("You are at square " + players[n].position.out + "%N")
								if (players[n].position = 1 or players[n].position = 4 or players[n].position = 6 or players[n].position = 9 or
								players[n].position = 11 or players[n].position = 13 or players[n].position = 16 or players[n].position = 19) then
									effect(players[n])
								else
									property(players[n])
								end
							else
								two := jail(players[n])
								if two[1] = two[2] then
									players[n].set_jail (false)
									print("You throw a double!%N")
									step := players[n].position + two[1] + two[2]
									if step < 21 then
										players[n].set_position (step)
									else
										board.effect.go (players[n])
										players[n].set_position (step \\ 20)
									end
									print("You are at square " + players[n].position.out + "%N")
									if (players[n].position = 1 or players[n].position = 4 or players[n].position = 6 or players[n].position = 9 or
									players[n].position = 11 or players[n].position = 13 or players[n].position = 16 or players[n].position = 19) then
											effect(players[n])
									else
										property(players[n])
									end
								else
									if players[n].turns_in_jail = 3 then
										print("You must pay 50K rub as a fine.")
										players[n].loss (50)
										players[n].set_jail (false)
										step := players[n].position + two[1] + two[2]
										if step < 21 then
											players[n].set_position (step)
										else
											board.effect.go (players[n])
											players[n].set_position (step \\ 20)
										end
										print("You are at square " + players[n].position.out + "%N")
										if (players[n].position = 1 or players[n].position = 4 or players[n].position = 6 or players[n].position = 9 or
										players[n].position = 11 or players[n].position = 13 or players[n].position = 16 or players[n].position = 19) then
											effect(players[n])
										else
											property(players[n])
										end
									else
										print("You do not throw a double.%N")
										print("You are " + players[n].turns_in_jail.out + " turns in jail.%N")
										players[n].increase_turns_in_jail
									end
								end
							end
						end
						if players[n].money < 0 then
							players[n].set_in_game(false)
							leave_the_game(players[n])
							print("You leave the game!%N")
						end
						print("You have " + players[n].money.out + "K rub!%N")
						print("The end of your turn!%N")
						i := i + 1
						print("%N")
					else
					end
				end
			end

			if i /= 100 * a_players + 1 then
				if p1.money >= 0 then
					print("Congratulations! " + p1.name + "win the game!%N")
				elseif p2.money >= 0 then
					print("Congratulations! " + p2.name + "win the game!%N")
				elseif attached p3 as pp3 then
					if pp3.money >= 0 then
						print("Congratulations! " + pp3.name + "win the game!%N")
					end
				elseif attached p4 as pp4 then
					if pp4.money >= 0 then
						print("Congratulations! " + pp4.name + "win the game!%N")
					end
				elseif attached p5 as pp5 then
					if pp5.money >= 0 then
						print("Congratulations! " + pp5.name + "win the game!%N")
					end
				elseif attached p6 as pp6 then
					if pp6.money >= 0 then
						print("Congratulations! " + pp6.name + "win the game!%N")
					end
				else
				end
			end
		end

feature
	board:BOARD
	players:ARRAYED_LIST[PLAYER]

feature -- Commands
	jail(p:PLAYER):ARRAYED_LIST[INTEGER]
		local
			i:INTEGER
			s:INTEGER_64
		do
			create Result.make (0)
			Result.extend (p.roll_a_dice)
			from
				i := 1
			until
				i <= 100000
			loop
				s := s + i
				i := i + 1
			end
			Result.extend(p.roll_a_dice)
		end

	buy_back(p:PLAYER):BOOLEAN
		do
			print("Do you want to buy back now?%N")
			Io.read_line
			if Io.last_string.as_lower.is_equal ("yes") then
				Result := true
				p.loss (50)
			end
		end

	leave_the_game(p:PLAYER)
		do
			across
				p.list_of_owned as l
			loop
				l.item.set_owned (false)
			end
		end

	effect(p:PLAYER)
		do
			inspect
				p.position
			when 1 then

			when 4 then
				board.effect.income_tax (p)
			when 6 then
				board.effect.in_jail(p)
			when 9, 13, 19 then
				board.effect.chance (p)
			when 11 then
				board.effect.free_parking
			when 16 then
				board.effect.goto_jail (p)
			else
			end
		end

	property(p:PLAYER)
		do
			inspect
				p.position
			when 2 then
				print(board.s2.name + ", Price: " + board.s2.price.out + ", Rent: " + board.s2.rent.out + "%N")
				if board.s2.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s2.rent)
				else
					buy(p, board.s2)
				end
			when 3 then
				print(board.s3.name + ", Price: " + board.s3.price.out + ", Rent: " + board.s3.rent.out + "%N")
				if board.s3.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s3.rent)
				else
					buy(p, board.s3)
				end
			when 5 then
				print(board.s5.name + ", Price: " + board.s5.price.out + ", Rent: " + board.s5.rent.out + "%N")
				if board.s5.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s5.rent)
				else
					buy(p, board.s5)
				end
			when 7 then
				print(board.s7.name + ", Price: " + board.s7.price.out + ", Rent: " + board.s7.rent.out + "%N")
				if board.s7.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s7.rent)
				else
					buy(p, board.s7)
				end
			when 8 then
				print(board.s8.name + ", Price: " + board.s8.price.out + ", Rent: " + board.s8.rent.out + "%N")
				if board.s8.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s8.rent)
				else
					buy(p, board.s8)
				end
			when 10 then
				print(board.s10.name + ", Price: " + board.s10.price.out + ", Rent: " + board.s10.rent.out + "%N")
				if board.s10.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s10.rent)
				else
					buy(p, board.s10)
				end
			when 12 then
				print(board.s12.name + ", Price: " + board.s12.price.out + ", Rent: " + board.s12.rent.out + "%N")
				if board.s12.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s12.rent)
				else
					buy(p, board.s12)
				end
			when 14 then
				print(board.s14.name + ", Price: " + board.s14.price.out + ", Rent: " + board.s14.rent.out + "%N")
				if board.s14.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s14.rent)
				else
					buy(p, board.s14)
				end
			when 15 then
				print(board.s15.name + ", Price: " + board.s15.price.out + ", Rent: " + board.s15.rent.out + "%N")
				if board.s15.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s15.rent)
				else
					buy(p, board.s15)
				end
			when 17 then
				print(board.s17.name + ", Price: " + board.s17.price.out + ", Rent: " + board.s17.rent.out + "%N")
				if board.s17.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s17.rent)

				else
					buy(p, board.s17)
				end
			when 18 then
				print(board.s18.name + ", Price: " + board.s18.price.out + ", Rent: " + board.s18.rent.out + "%N")
				if board.s18.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s18.rent)
				else
					buy(p, board.s18)
				end
			when 20 then
				print(board.s20.name + ", Price: " + board.s20.price.out + ", Rent: " + board.s20.rent.out + "%N")
				if board.s20.is_owned then
					print("It is owned. You should pay rent.%N")
					p.loss (board.s20.rent)
				else
					buy(p, board.s20)
				end
			else
			end
		end


	buy(p:PLAYER; s:PROPERTY_SQUARE)
		do
			print("Do you want to buy it? (Write 'yes' or 'not')%N")
			Io.read_line
			if Io.last_string.as_lower.is_equal ("yes") then
				p.buy (s)
				s.set_owned(true)
				print("You've just bought it!%N")
			end
		end

feature -- Queries
	is_end:BOOLEAN
		local
			i:INTEGER
		do
			across
				players as p
			loop
				if p.item.money >= 0 then
					i := i + 1
				end
			end
			if i <= 1 then
				Result := true
			end
		end

end
