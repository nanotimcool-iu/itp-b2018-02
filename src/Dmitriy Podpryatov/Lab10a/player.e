note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create
	make

feature -- Constructor
	make(a_id:INTEGER; a_name:STRING)
		require
			non_empty: a_name.count > 0
		do
			name := a_name
			id := a_id
			money := 1500
			create list_of_owned.make(0)
			position := 1
			is_in_game := true
		end

feature {NONE} -- Variables
	id:INTEGER

feature
	name:STRING
	money:INTEGER -- in K rubles

	list_of_owned:ARRAYED_LIST[PROPERTY_SQUARE]

	buy(s:PROPERTY_SQUARE)
		do
			s.set_owned(true)
			list_of_owned.extend (s)
			loss(s.price)
		end

	turns_in_jail:INTEGER

	increase_turns_in_jail
		do
			turns_in_jail := turns_in_jail + 1
		end

	is_in_jail:BOOLEAN

	set_jail(b:BOOLEAN)
		do
			is_in_jail := b
		end

	position:INTEGER

	set_position(pos:INTEGER)
		require
			in_game: pos > 0 and pos < 21
		do
			position := pos
		end

	is_in_game:BOOLEAN

	set_in_game(b:BOOLEAN)
		do
			is_in_game := b
		end

feature -- Commands
	move:INTEGER
		do
			Result := roll_a_dice + roll_a_dice
		end

	get(m:INTEGER)
		-- Increses money by 'm'
		do
			money := money + m
		end

	loss(m:INTEGER)
		-- Decreases money by 'm'
		do
			money := money - m
		end

feature -- Querires
	roll_a_dice:INTEGER
		local
			time:TIME
			temp:INTEGER
		do
			create time.make_now
			temp := (time.hour * 60 * 60 + time.minute * 60 + time.second) * 1000 + time.milli_second
			Result := temp \\ 4 + 1
		ensure
			Result > 0 and Result < 5
		end



end
