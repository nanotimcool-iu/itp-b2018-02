note
	description: "Summary description for {PROPERTY_SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROPERTY_SQUARE

inherit
	SQUARE

create
	make_

feature -- Constructor
	make_(a_id:INTEGER; a_name:STRING; a_price, a_rent:INTEGER)
		require
			name_exists: a_name.count > 0
		do
			make(a_id)
			name := a_name
			price := a_price
			rent := a_rent
		end

feature
	name:STRING
	price:INTEGER
	rent:INTEGER
	is_owned:BOOLEAN

	set_owned(b:BOOLEAN)
		do
			is_owned := b
		end

end
