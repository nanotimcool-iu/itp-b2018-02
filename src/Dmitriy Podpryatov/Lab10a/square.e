note
	description: "Summary description for {SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SQUARE

feature -- Constructor
	make(a_id:INTEGER)
		do
			id := a_id
		end

feature {SQUARE}
	id:INTEGER

end
