note
	description: "coffee_shop application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			cs:COFFEE_SHOP
			p1, p2, p3:PRODUCT
		do
			create cs.make
			create {ESPRESSO} p1.make (100, 50)
			create {CAPPUCCINO} p2.make (80, 60)
			p3 := p1
			cs.add_product (p1)
			cs.add_product (p2)
			cs.add_product (p3)

			print("Profit: " + cs.profit.out + "%N")
			print("Menu%N")
			cs.print_menu
		end

end
