note
	description: "Summary description for {CAPPUCCINO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAPPUCCINO

inherit
	COFFEE
		redefine
			make
		end

create
	make

feature -- Constructor
	make(a_price_public, a_price:REAL)
		do
			Precursor(a_price_public, a_price)
			description := "Cappuccino"
		end

end
