note
	description: "Summary description for {COFFEE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	COFFEE

inherit
	PRODUCT

feature
	make(a_price_public, a_price:REAL)
		do
			set_price_public(a_price_public)
			set_price(a_price)
		end

end
