note
	description: "Summary description for {COFFEE_SHOP}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COFFEE_SHOP

create
	make

feature -- Constructor
	make
		do
			create set.make(0)
		end

feature -- Commands
	add_product(p:PRODUCT)
		do
			set.extend (p)
		end

feature -- Variables
	set:ARRAYED_SET[PRODUCT]

feature -- Queries
	profit:REAL
		do
			across
				set as s
			loop
				Result := Result + s.item.profit
			end
		end

	print_menu
		do
			across
				set as s
			loop
				print(s.item.description + " - " + s.item.price_public.out + "%N")
			end
		end


end
