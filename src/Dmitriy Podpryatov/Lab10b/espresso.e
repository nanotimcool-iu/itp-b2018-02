note
	description: "Summary description for {ESPRESSO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ESPRESSO

inherit
	COFFEE
		redefine
			make
		end

create make

feature -- Constructor
	make(a_price_public, a_price:REAL)
		do
			Precursor(a_price_public, a_price)
			description := "Espresso"
		end

end
