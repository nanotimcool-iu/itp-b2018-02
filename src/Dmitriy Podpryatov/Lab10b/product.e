note
	description: "Summary description for {PRODUCT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PRODUCT

feature -- Variables
	price_public:REAL

	price:REAL -- The price the owner paid for having that product

	description:STRING

feature -- Commands
	set_price_public(a_price_public:REAL)
		do
			price_public := a_price_public
		end

	set_price(a_price:REAL)
		do
			price := a_price
		end

feature -- Queries
	profit:REAL
		do
			Result := price_public - price
		end

invariant price < price_public

end
