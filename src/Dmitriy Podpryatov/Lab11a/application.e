note
	description: "testing application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			q, p, m:QUEUE_OF_EVENTS
			r:RANDOM
			i:INTEGER
		do
			create q.make

			create r.make
			from
				i := 1
			until
				not (i <= 2000) -- Change to 1,000,000 or more
			loop
				q.add_tag ((r.i_th (i) \\ 1000 * (-1).power (i)).rounded)
				i := i + 1
			end

			print("Queue: %N")
			q.print_queue
			print("%NSize of the queue " + q.queue.count.out + "%N")

			print("-----%N")

			-- Testing --
			create p.make
			-- Instruction coverage
			print("Instruction coverage%N")
			p.add_tag (10)
			p.add_tag (-10)
			p.add_tag (4)
			p.print_queue
			print("Extracted tag " + q.extract_tag.out + "%N")
			-- Path coverage
			print("Path coverage%N")
			p.add_tag (6)
			p.add_tag (-6)
			p.add_tag (6)
			p.print_queue
			print("Size is " + p.queue.count.out + "%N")
			-- Conditional coverage
			print("Conditional coverage%N")
			create m.make
			--print(m.extract_tag) -- it will cause an error (because it should by precondition)
			m.add_tag (1)
			print(m.extract_tag.out + "%N")
			-- Branch coverage
			print("Branch coverage%N")
			m.add_tag (1)
			m.add_tag (-5)
			m.print_queue
		end

end
