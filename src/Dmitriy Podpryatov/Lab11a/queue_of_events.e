note
	description: "Summary description for {QUEUE_OF_EVENTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	QUEUE_OF_EVENTS

create
	make

feature -- Constructor
	make
		do
			create queue.make(0)
		end

feature
	queue: ARRAYED_LIST[INTEGER]

feature -- Commands
	add_tag(tag:INTEGER)
		do
			if queue.has (tag) or tag < 0 then
				-- nothing
			else
				queue.extend (tag)
			end
		end

feature -- Queries
	extract_tag:INTEGER
		require
			non_empty: queue.count > 0
		do
			Result := queue.at (1)
			across
				queue as q
			loop
				if q.item < Result then
					Result := q.item
				end
			end
		end

feature -- Auxillary feature
	print_queue
		do
			across
				queue as q
			loop
				print(q.item.out + "%N")
			end
		end

end
