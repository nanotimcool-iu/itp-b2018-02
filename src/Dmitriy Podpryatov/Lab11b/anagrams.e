note
	description: "Summary description for {ANAGRAMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ANAGRAMS

feature
	anagrams(word:STRING)
		require
			non_void: word /= Void
		do
			permute(word, 1, word.count)
		end

feature {NONE} -- Auxillary feature
	permute(string:STRING; left, right:INTEGER)
		local
			i:INTEGER
			s:STRING
		do
			s := string
			if left = right then
				print(s + "%N")
			else
				from
					i := left
				until
					not (i <= right)
				loop
					s := swap(s, left ,i)
					permute(s, left + 1, right)
					s := swap(s, left, i)
					i := i + 1
				end
			end
		end

	swap(string:STRING; p1, p2:INTEGER):STRING
		local
			temp:CHARACTER
		do
			temp := string.at (p1)
			string[p1] := string.at (p2)
			string[p2] := temp
			Result := string
		end

end
