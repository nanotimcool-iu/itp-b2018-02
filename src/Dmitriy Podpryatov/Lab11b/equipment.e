note
	description: "Summary description for {EQUIPMENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	EQUIPMENT

feature -- Queries
	price:INTEGER

	description:STRING

	rent_time:detachable TIME

	return_time:detachable TIME

feature -- Commands
	make(a_price:INTEGER; a_description:STRING)
		do
			price := a_price
			description := a_description
		end

	rent(a_rent_time, a_return_time:TIME)
		do
			rent_time := a_rent_time
			return_time := a_return_time
		end

	return
		do
			rent_time := Void
			return_time := Void
		end

end
