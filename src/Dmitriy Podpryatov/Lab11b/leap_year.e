note
	description: "Summary description for {LEAP_YEAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LEAP_YEAR

feature
	is_leap_year(year:INTEGER):BOOLEAN
		do
			if (year \\ 400 = 0) or (year \\ 4 = 0 and year \\ 100 /= 0) then
				Result := true
			end
		end

end
