note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature
	make(a_name:STRING)
		do
			name := a_name
 			create equipment.make(0)
		end

feature
	name:STRING

	balance:INTEGER

	plus_money(money:INTEGER)
		do
			balance := balance + money
		end

	minus_money(money:INTEGER):BOOLEAN
		do
			if money > balance then
				print("Do not enough money for operation%N")
				Result := false
			else
				balance := balance - money
				Result := true
			end
		end

	equipment:ARRAYED_SET[EQUIPMENT]

	add_equipment(eq:EQUIPMENT)
		do
			equipment.extend (eq)
		end

	return_equipment(eq:EQUIPMENT)
		do
			eq.return
		end

end
