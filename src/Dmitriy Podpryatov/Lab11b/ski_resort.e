note
	description: "Summary description for {SKI_RESORT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SKI_RESORT

create
	make

feature
	make
		do
			create stock.make(0)
			create rentals.make(0)
		end

feature
	stock:ARRAYED_SET[EQUIPMENT]

	add_stock(equipment:EQUIPMENT)
		require
			equipment /= Void
		do
			stock.extend(equipment)
		ensure
			stock.has (equipment)
		end

	rentals:ARRAYED_SET[PERSON]

feature
	rent(p:PERSON; equipment_to_rent:ARRAYED_SET[EQUIPMENT])
		-- Suppose time_to_rent = now, time_to_return = now + 6 hours
		local
			time:TIME
			time_return:TIME
		do
			if check_primary(equipment_to_rent) then
				across
					equipment_to_rent as eq
				loop
					if number_of_items(equipment_to_rent, eq.item) > 1 then
						if p.minus_money (eq.item.price) then
							create time.make_now
							time.hour_add (6)
							time_return := time
							eq.item.rent(time, time_return)
							p.add_equipment (eq.item)
						end
					else
						print("Equipment " + eq.item.description + "is not available now%N")
					end
				end
			else
				print("You must rent at least one primary equipment.%N")
			end
		end

	return(p:PERSON; equipment_to_return:ARRAYED_SET[EQUIPMENT])
		do
			across
				equipment_to_return as eq
			loop
				p.return_equipment (eq.item)
			end
		end

feature {NONE}
	check_primary(list:ARRAYED_SET[EQUIPMENT]):BOOLEAN
		do
			across
				list as l
			loop
				if attached {PRIMARY_EQUIPMENT} l.item as pe then
					Result := true
				end
			end
		end

	number_of_items(list:ARRAYED_SET[EQUIPMENT]; e:EQUIPMENT):INTEGER
		do
			across
				list as l
			loop
				if l.item.description.is_equal (e.description) then
					Result := Result + 1
				end
			end
		end

end
