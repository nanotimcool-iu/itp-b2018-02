note
	description: "Summary description for {PIECE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PIECE

feature -- Queries
	colour:INTEGER -- '0' means white and "1" means black

	position:TUPLE[INTEGER, INTEGER] -- first number is vertical and second is horizontal coordinate

	can_move(new_position:TUPLE[INTEGER, INTEGER]):BOOLEAN
		require
			on_the_board(new_position)
		deferred
		end

feature -- Commands
	make(a_colour:INTEGER; a_position:TUPLE[INTEGER, INTEGER])
		do
			colour := a_colour
			position := a_position
		end

	move(new_position:TUPLE[INTEGER, INTEGER])
		require
			on_the_board(new_position)
		do
			if can_move(new_position) then
				position := new_position
			end
		end

	on_the_board(p:TUPLE[INTEGER, INTEGER]):BOOLEAN
		do
			if p.integer_32_item (1) > 0 and p.integer_32_item (1) < 9 and
			p.integer_32_item (2) > 0 and p.integer_32_item (2) < 9 then
				Result := true
			end
		end

	display
		deferred
		end

invariant
	on_the_board(position)
	black_or_white: colour >= 0 and colour <= 1

end
