note
	description: "lab application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			b: BIOLOGIST
			cs:COMPUTER_SCIENTIST
			bi:BIO_INFORMATIC
		do
			create b.init_bio(1, "Bob", "dog")
			b.introduce
			Io.new_line
			create cs.init_comp (2, "Jonh")
			cs.introduce
			Io.new_line
			create bi.init_bio_inf (3, "Steve", "cat", "my bio")
			bi.introduce
		end

end
