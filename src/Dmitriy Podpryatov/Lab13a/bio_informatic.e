note
	description: "Summary description for {BIO_INFORMATIC}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIO_INFORMATIC

inherit
	COMPUTER_SCIENTIST
		undefine
			introduce
		end


	BIOLOGIST
		redefine
			introduce
		select
			make
		end

create
	init_bio_inf

feature
	init_bio_inf(a_id:INTEGER; a_name:STRING; pet_name:STRING; bio:STRING)
		do
			init_bio(a_id, a_name, pet_name)
			short_biography := bio
		end

	introduce
		do
			print("Name: " + name + "%N")
			print("ID: " + id.out + "%N")
			print("Pet: " + pet + "%N")
			print("Biography: " + short_biography + "%N")
		end

feature
	short_biography:STRING

end
