note
	description: "Summary description for {BIOLOGIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIOLOGIST

inherit
	SCIENTIST
		redefine
			introduce
		end

create
	init_bio

feature
	init_bio(a_id:INTEGER; a_name:STRING; pet_name:STRING)
		do
			make(a_id, a_name)
			discipline := "Biologist"
			pet := pet_name
		end

	introduce
		do
			Precursor
			print("Pet: " + pet + "%N")
		end

feature
	pet:STRING

end
