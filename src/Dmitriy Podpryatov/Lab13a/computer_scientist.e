note
	description: "Summary description for {COMPUTER_SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMPUTER_SCIENTIST

inherit
	SCIENTIST
		rename
			make as init_comp
		redefine
			init_comp
		end

create
	init_comp

feature
	init_comp(a_id:INTEGER; a_name:STRING)
		do
			id := a_id
			name := a_name
			discipline := "Computer Scientist"
		end

end
