note
	description: "Summary description for {SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SCIENTIST

feature
	make(a_id:INTEGER; a_name:STRING)
		do
			id := a_id
			name := a_name
		end

	id:INTEGER

	name:STRING

	discipline:STRING

	introduce
		do
			print("Name: " + name + "%N")
			print("ID: " + id.out + "%N")
		end

end
