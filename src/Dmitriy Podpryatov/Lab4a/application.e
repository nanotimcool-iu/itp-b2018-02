note
	description: "bags1 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			b1, b2:MULTISET
		do
			--| Add your code here
			create b1.make
			create b2.make
			b1.add ('a', 5)
			b1.add ('b', 4)
			b1.remove ('a', 3)
			print(b1.elements.count.out + "%N")
			print(b1.min.out + "%N")
			print(b1.max.out + "%N")
			print(b1.elements.i_th (1).number_copies.out + "%N")

			b2.add ('a', 2)
			b2.add ('b', 4)
			print(b1.is_equal_bag (b2))
		end

end
