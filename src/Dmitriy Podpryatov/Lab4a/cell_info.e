note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	create_cell_info

feature
	value:CHARACTER assign set_value
	number_copies:INTEGER assign set_number_copies

feature
	create_cell_info(a_value:CHARACTER; a_number_copies:INTEGER)
		do
			value := a_value
			number_copies := a_number_copies
		end

	set_number_copies(a_number_copies:INTEGER)
		do
			number_copies := a_number_copies
		end

	set_value(a_value:CHARACTER)
		do
			value := a_value
		end

end
