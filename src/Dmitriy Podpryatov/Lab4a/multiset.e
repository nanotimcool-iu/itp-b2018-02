note
	description: "Summary description for {MULTISET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MULTISET

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			--| Add your code here
			create elements.make (0)
		end

feature
	elements:ARRAYED_LIST[CELL_INFO]

feature
	add(val:CHARACTER; n:INTEGER)
		require
			n > 0
		local
			ci:CELL_INFO
			is_in:BOOLEAN
		do
			across elements as e
			loop
				if e.item.value.is_equal (val) then
					is_in := true
				end
			end

			if is_in then
				across elements as e
				loop
					if e.item.value.is_equal (val) then
						e.item.set_number_copies(e.item.number_copies + n)
					end
				end
			else
				create ci.create_cell_info (val, n)
				elements.extend (ci)
			end
		end


	remove(val:CHARACTER; n:INTEGER)
		require
			n > 0
		do
			across
				elements as e
			loop
				if e.item.value.is_equal (val) then
					if e.item.number_copies >= n then
						e.item.number_copies := e.item.number_copies - n
					else
						elements.remove
					end
				end
			end
		end


	min:CHARACTER
		require
			elements.count >= 1
		local
			i:INTEGER
			min_char:CHARACTER
		do
			if elements.count = 1 then
				Result := elements.i_th(1).value
			else
				min_char := elements.i_th (1).value
				from
					i := 2
				until
					i > elements.count
				loop
					if elements.i_th (i).value.code < elements.i_th (i - 1).value.code then
						min_char := elements.i_th(i).value
					end
					i := i + 1
				end
				Result := min_char
			end
		end


	max:CHARACTER
		require
			elements.count >= 1
		local
			i:INTEGER
			max_char:CHARACTER
		do
			if elements.count = 1 then
				Result := elements.i_th(1).value
			else
				max_char := elements.i_th (1).value
				from
					i := 2
				until
					i > elements.count
				loop
					if elements.i_th (i).value.code > elements.i_th (i - 1).value.code then
						max_char := elements.i_th(i).value
					end
					i := i + 1
				end
				Result := max_char
			end
		end

	is_equal_bag(bag:MULTISET):BOOLEAN
		require
			bag /= Void
		local
			coincidence:INTEGER
		do
			coincidence := Current.elements.count
			across
				Current.elements as c
			loop
				across
					bag.elements as b
				loop
					if c.item.number_copies = b.item.number_copies and c.item.value.is_equal (b.item.value) then
						coincidence := coincidence - 1
					end
				end
			end
			if coincidence = 0 then
				Result := true
			else
				Result := false
			end
		end

end
