note
	description: "linear_algebra application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS; MATRIX_OPER; VECTOR_OPER

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			m1, m2, m3, m4:ARRAY2[INTEGER]
			v1, v2, v3:ARRAY[INTEGER]
		do
			--| Add your code here
			create m1.make_filled (0, 3, 3)
			m1.item (1, 1) := 1
			m1.item (1, 2) := 2
			m1.item (1, 3) := 3
			m1.item (2, 1) := 4
			m1.item (2, 2) := 5
			m1.item (2, 3) := 6
			m1.item (3, 1) := 7
			m1.item (3, 2) := 8
			m1.item (3, 3) := 9
			print_m(m1)

			create m2.make_filled (0, 3, 3)
			m2.item (1, 1) := 1
			m2.item (1, 2) := 2
			m2.item (1, 3) := 3
			m2.item (2, 1) := 4
			m2.item (2, 2) := 5
			m2.item (2, 3) := 6
			m2.item (3, 1) := 7
			m2.item (3, 2) := 8
			m2.item (3, 3) := 9
			print_m(m2)

			print(det(m1).out + "%N")
			print(det(m2).out + "%N")

			m3 := add (m1, m2)
			print_m(m3)

			m4 := prod(m1, m3)
			print_m(m4)

			create v1.make_filled (0, 1, 3)
			v1[1] := 1
			v1[2] := 2
			v1[3] := 3
			print_v(v1)

			create v2.make_filled (0, 1, 3)
			v2[1] := 4
			v2[2] := 5
			v2[3] := 6
			print_v(v2)

			create v3.make_filled (0, 1, 3)
			v3[1] := 7
			v3[2] := 8
			v3[3] := 9
			print_v(v3)

			print_v(cross_product(v1, v2))
			print(dot_product(v1, v3).out + "%N")
			print(triangle_area(v2, v3).out + "%N")

		end

	print_m(m:ARRAY2[INTEGER])
		local
			i, j:INTEGER
		do
			from
				i := 1
			until
				i > m.height
			loop
				print("  ")
				from
					j := 1
				until
					j > m.width
				loop
					print(m.item (i, j).out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
			print("%N")
		end

		print_v(v:ARRAY[INTEGER])
			local
				i:INTEGER
			do
				print("( ")
				from
					i := 1
				until
					i > v.count
				loop
					print(v[i].out + " ")
					i := i + 1
				end
				print(")%N")
			end

end
