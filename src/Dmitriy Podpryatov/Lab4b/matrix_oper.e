note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER

feature


	add (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.width = m2.width and m1.height = m2.height
		local
			i, j:INTEGER
			mr:ARRAY2[INTEGER]
		do
			create mr.make_filled (0, m1.height, m1.width)
			from
				i := 1
			until
				i > m1.height
			loop
				from
					j := 1
				until
					j > m1.width
				loop
					mr.item (i, j) := m1.item (i, j) + m2.item (i, j)
					j := j + 1
				end
				i := i + 1
			end
			Result := mr
		end


	minus (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.width = m2.width and m1.height = m2.height
		local
			i, j:INTEGER
			mr:ARRAY2[INTEGER]
		do
			create mr.make_filled (0, m1.height, m1.width)
			from
				i := 1
			until
				i > m1.height
			loop
				from
					j := 1
				until
					j > m1.width
				loop
					mr.item (i, j) := m1.item (i, j) - m2.item (i, j)
					j := j + 1
				end
				i := i + 1
			end
			Result := mr
		end


	prod (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.width = m2.height
		local
			i, j, k:INTEGER
			mr:ARRAY2[INTEGER]
		do
			create mr.make_filled (0, m1.height, m2.width)
			from
				i := 1
			until
				i > mr.height
			loop
				from
					j := 1
				until
					j > mr.width
				loop
					from
						k := 1
					until
						k > m1.width
					loop
						mr.item (i, j) := mr.item (i, j) + m1.item (i, k) * m2.item (k, j)
						k := k + 1
					end
					j := j + 1
				end
				i := i + 1
			end
			Result := mr
		end


	det (m: ARRAY2 [INTEGER]): INTEGER
		-- Only for matrices with size <= 3
		require
			m.height = m.width
		local
			i:INTEGER
		do
			if m.height = 1 then
				Result := m.item (1, 1)
			elseif m.height = 2 then
				Result := m.item (1, 1) * m.item (2, 2) - m.item (1, 2) * m.item (2, 1)
			else
				Result := m.item (1, 1) * (m.item (2, 2)*m.item (3, 3) - m.item (2, 3)*m.item (3, 2))
						- m.item (1, 2) * (m.item (2, 1)*m.item (3, 3) - m.item (2, 3)*m.item (3, 1))
						+ m.item (1, 3) * (m.item (2, 1)*m.item (3, 2) - m.item (2, 2)*m.item (3, 1))
			end
		end

end
