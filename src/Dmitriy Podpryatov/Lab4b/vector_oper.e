note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER

feature

	cross_product(v1, v2:ARRAY[INTEGER]):ARRAY[INTEGER]
	-- Only for vectors with 3 basis vectors in coordinate system
		require
			v1.count = v2.count
			v1.count = 3
		local
			vr:ARRAY[INTEGER]
		do
			create vr.make_filled (0, 1, v1.count)
			vr[1] := v1[2]*v2[3] - v1[3]*v2[2]
			vr[2] := -1 * (v1[1]*v2[3] - v1[3]*v2[1])
			vr[3] := v1[1]*v2[2] - v1[2]*v2[1]
			Result := vr
		end

	dot_product(v1, v2: ARRAY [INTEGER]): INTEGER
		require
			v1.count = v2.count
		local
			i:INTEGER
		do
			Result := 0
			from
				i := 1
			until
				i > v1.count
			loop
				Result := Result + v1[i] * v2[i]
				i := i + 1
			end
		end

	triangle_area (v1, v2: ARRAY [INTEGER]): INTEGER
		require
			v1.count = v2.count
			v1.count = 3
		local
			v_orth:ARRAY[INTEGER]
			m:DOUBLE_MATH
		do
			create m
			v_orth := cross_product(v1, v2)
			Result := m.sqrt (v_orth[1].power (2) + v_orth[2].power (2) + v_orth[3].power (2)).rounded
		end

end
