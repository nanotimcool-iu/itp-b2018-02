note
	description: "range_of_numbers application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			r1, r2, r3, r4, r5, r6:RANGE
			rr1, rr2:RANGE
		do
			--| Add your code here
			create r1.make (1, 3)
			create r2.make (7, 9)
			create r3.make(1, 8)
			create r4.make (1, 3)
			create r5.make (6, 3)
			create r6.make (3, 5)

			r1.print_range
			r2.print_range
			r3.print_range
			r4.print_range
			r5.print_range
			r6.print_range

			print(r1.is_equal_range (r2).out + "%N")
			print(r1.is_equal_range (r4).out + "%N")

			print("%N")

			print(r1.is_empty.out + "%N")
			print(r5.is_empty.out + "%N")

			print("%N")

			print(r1.is_sub_range_of (r3).out + "%N")
			print(r1.is_sub_range_of (r2).out + "%N")

			print("%N")

			print(r3.is_super_range_of (r1).out + "%N")
			print(r3.is_super_range_of (r2).out + "%N")

			print("%N")

			print(r1.left_overlaps (r3).out + "%N")
			print(r2.left_overlaps(r3).out + "%N")
			print(r2.left_overlaps (r6).out + "%N")

			print("%N")

			print(r3.right_overlaps (r2).out + "%N")
			print(r2.right_overlaps (r3).out + "%N")
			print(r2.right_overlaps (r6).out + "%N")

			print("%N")

			print(r3.overlaps (r6).out + "%N")
			print(r1.overlaps (r6).out + "%N")
			print(r1.overlaps (r2).out + "%N")

			rr1 := r4.add (r6)
			rr1.print_range

			rr2 := r2.subtract (r3)
			rr2.print_range
		end

end
