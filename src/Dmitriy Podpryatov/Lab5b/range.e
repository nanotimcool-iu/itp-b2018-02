note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	make

feature -- Constructor
	make(l, r:INTEGER)
		do
			left := l
			right := r
		ensure
			left = l
			right = r
		end

feature -- Attributes
	left: INTEGER
	right: INTEGER

feature -- Boolean operations
	is_equal_range(other: like Current):BOOLEAN
		require
			Current.is_empty = false
			other.is_empty = false
		do
			Result := (left = other.left) and (right = other.right)
		ensure
			Result = ((left = other.left) and (right = other.right))
		end

	is_empty:BOOLEAN
		require
			Current /= Void
		do
			Result := (right - left + 1) <= 0
		ensure
			Result = ((right - left + 1) <= 0)
		end

	is_sub_range_of(other: like Current):BOOLEAN
		require
			Current.is_empty = false
			other.is_empty = false
		do
			Result := (left >= other.left) and (right <= other.right)
		ensure
			Result = ((left >= other.left) and (right <= other.right))
		end

	is_super_range_of(other: like Current):BOOLEAN
		require
			Current.is_empty = false
			other.is_empty = false
		do
			Result := (left <= other.left) and (right >= other.right)
		ensure
			Result = ((left <= other.left) and (right >= other.right))
		end

	left_overlaps(other: like Current):BOOLEAN
		require
			Current.is_empty = false
			other.is_empty = false
		do
			Result := (left >= other.left) and (left <= other.right)
		ensure
			Result = ((left >= other.left) and (left <= other.right))
		end

	right_overlaps(other: like Current):BOOLEAN
		require
			Current.is_empty = false
			other.is_empty = false
		do
			Result := (right <= other.right) and (right >= other.left)
		ensure
			Result = ((right <= other.right) and (right >= other.left))
		end

	overlaps(other: like Current):BOOLEAN
		do
			Result := Current.left_overlaps (other) or Current.right_overlaps (other) or
					other.left_overlaps (Current) or other.right_overlaps (Current)
		ensure
			Result = (Current.left_overlaps (other) or Current.right_overlaps (other) or
					other.left_overlaps (Current) or other.right_overlaps (Current))
		end

feature -- Range operations
	add(other: like Current):RANGE
		require
			Current.overlaps(other)
		local
			range:RANGE
			l, r:INTEGER
		do
			if left <= other.left then
				l := left
			else
				l := other.left
			end

			if right >= other.right then
				r := right
			else
				r := other.right
			end
			create range.make (l, r)
			Result := range
		ensure
			Result /= Void
		end

	subtract(other: like Current):RANGE
		require
			Current.left_overlaps (other) or Current.right_overlaps (other)
		local
			range:RANGE
			l, r:INTEGER
		do
			if other.left <= left and other.right >= left then
				if other.right <= right then
					l := other.right + 1
					r := right
				else
					l := 1
					r := -1
				end
			elseif other.right >= right and other.left <= right then
				if other.left >= left then
					l := left
					r := other.left - 1
				end
			end
			create range.make (l, r)
			Result := range
		ensure
			Result /= Void
		end

	print_range
		do
			if left <= right then
				print("[" + left.out + ", " + right.out + "]")
				print("%N")
			else
				print("Empty range")
				print("%N")
			end
		end

end
