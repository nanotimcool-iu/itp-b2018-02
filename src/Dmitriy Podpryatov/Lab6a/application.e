note
	description: "recurtion application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			r:REVERSE_A_STRING
			l:LONGEST_COMMON_SUBSEQUENCE
			s:STRING
			sa:SORTING_ALGORITHM
			a:ARRAY[INTEGER]
			i:INTEGER
		do
			--| Add your code here
			-- Reverse part
			create r
			s := r.iterative_reverse ("abcd")
			print(s)

			print("%N")

			s := r.recursive_reverse ("ABCD")
			print(s + "%N")
			-- Reverse part

			print("%N")

			-- LCS part
			create l
			s := l.lcs ("abcd", "")
			print(s + "%N")
			print(s.count.out + "%N")

			s := l.lcs ("abcd", "abfg")
			print(s + "%N")
			print(s.count.out + "%N")

			s := l.lcs ("qwertyuiop", "asdqwerfgh")
			print(s + "%N")
			print(s.count.out + "%N")

			s := l.lcs (";;;;", "aaa;;aaa")
			print(s + "%N")
			print(s.count)
			-- LCS part

			print("%N")

			-- Merge part
			a := <<10, 9, 8, 7, 6, 5, 4>>
			create sa
			sa.merge_sort (a, 1, a.count - 1)
			from
				i := 1
			until
				i > a.count
			loop
				print(a[i].out + " ")
				i := i + 1
			end
			-- Merge part
		end

end
