note
	description: "Summary description for {LONGEST_COMMON_SUBSEQUENCE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LONGEST_COMMON_SUBSEQUENCE

feature
	lcs(x, y:STRING):STRING
		local
			sx, sy:STRING
		do
			if x.count = 0 or y.count = 0 then
				Result := ""
			elseif x.item(x.count) = y.item(y.count) then
				Result := lcs(x.substring (1, x.count - 1), y.substring (1, y.count - 1)) + x.item (x.count).out
			else
				sx := lcs(x, y.substring (1, y.count - 1))
				sy := lcs(x.substring (1, x.count - 1), y)
				if sx.count >= sy.count then
					Result := sx
				else
					Result := sy
				end
			end
		end

end
