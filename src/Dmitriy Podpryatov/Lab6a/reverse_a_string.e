note
	description: "Summary description for {REVERSE_A_STRING}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	REVERSE_A_STRING

feature
	iterative_reverse(s:STRING):STRING
		local
			i:INTEGER
		do
			Result := ""
			from
				i := s.count
			until
				i < 1
			loop
				Result := Result + s.item (i).out
				i := i - 1
			end
		end

	recursive_reverse(s:STRING):STRING
		do
			Result := ""
			if s.count <= 1 then
				Result := s
			else
				Result := recursive_reverse (s.substring (2, s.count)) + s.item (1).out
			end
		end

end
