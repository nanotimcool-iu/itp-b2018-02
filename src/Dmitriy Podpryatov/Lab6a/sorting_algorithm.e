note
	description: "Summary description for {SORTING_ALGORITHM}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SORTING_ALGORITHM

feature -- Algorithm
	merge_sort(a:ARRAY[INTEGER]; l, r:INTEGER)
		local
			m:INTEGER
		do
			if l < r then
				m := (l + r) // 2
				merge_sort(a, l, m)
				merge_sort(a, m + 1, r)
				merge(a, l, m, r)
			end
		end

feature -- Utility
	merge(a: ARRAY [INTEGER]; l, m, r: INTEGER)
		local
			b:ARRAY[INTEGER]
			h, i, j, k:INTEGER
		do
			i := l
			j := m + 1
			k := l
			create b.make_filled (0, l, r)
			from

			until
				i > m or j > r
			loop
				-- Begins the merge and copies it into an array `b'
				if a.item (i) <= a.item (j) then
					b.item (k) := a.item (i)
					i := i + 1

				elseif a.item (i) > a.item (j) then
					b.item (k) := a.item (j)
					j := j + 1
				end
				k := k + 1
			end
			-- Finishes the copy of the uncopied part of the array
			if i > m then
				from
					h := j
				until
					h > r
				loop
					b.item (k + h - j) := a.item (h)
					h := h + 1
				end
			elseif j > m then
				from
					h := i
				until
					h > m
				loop
					b.item (k + h - i) := a.item (h)
					h := h + 1
				end
			end
			-- Begins the copy to the real array
			from
				h := l
			until
				h > r
			loop
				a.item (h) := b.item (h)
				h := h + 1
			end
		end

end
