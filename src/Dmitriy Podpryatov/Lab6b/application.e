note
	description: "recursion2 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			t:TOWER_OF_HANOI

			p:POWER_SET
			s:ARRAYED_LIST[INTEGER]
			ss:ARRAYED_LIST[ARRAYED_LIST[INTEGER]]
			i, j: INTEGER

			r:ROBOT_PATH
			a:ARRAY2[INTEGER]
		do
			--| Add your code here
			-- Tower of Hanoi Part
			create t
			t.hanoi (2, 'a', 'b', 'c')
			-- Tower of Hanoi Part

			print("-----%N")

			-- Power set part
			create p
			create s.make (0)
			from
				i := 1
			until
				not (i <= 3)
			loop
				s.extend (i)
				i := i + 1
			end

			p.power_set (s)


			-- Power set part
			print("%N-----%N")
			-- Robot path part
			create a.make_filled (0, 4, 4)
			a.item (1, 2) := -1
			a.item (3, 1) := -1
			create r.make (a)
			-- Robot path part
			print("%N-----%N")
		end

end
