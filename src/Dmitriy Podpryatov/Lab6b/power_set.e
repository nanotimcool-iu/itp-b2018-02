note
	description: "Summary description for {POWER_SET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	POWER_SET

feature
	power_set(a:ARRAYED_LIST[INTEGER])
		local
			n, i, j:INTEGER
		do
			n := a.count
			print("{")
			from
				i := 0
			until
				not (i < (1|<< n ))
			loop
				print("{ ")
				from
					j := 0
				until
					not (j < n)
				loop
					if ((i & (1 |<< j)) > 0) then
						print(a.i_th (j + 1).out + " ")
					end
					j := j + 1
				end
				print("}")
				i := i + 1
			end
			print("}")
		end

end
