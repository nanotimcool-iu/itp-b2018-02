note
	description: "Summary description for {ROBOT_PATH}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ROBOT_PATH

create
	make

feature
	make(a:ARRAY2[INTEGER])
		require
			a /= Void
		--- "0" means empty cell, "-1" means "off limit" cell
		do
			grid := a
			r := a.height
			c := a.width
			path("", 1, 1, false)
		end

feature
	grid:ARRAY2[INTEGER]
	r, c:INTEGER
	flag:BOOLEAN

feature
	path(s:STRING; i, j:INTEGER; f:BOOLEAN)
		do
			if f = false then
				if i = r and j = c then
					print(s)
					flag := true
				elseif i = r then
					right(s, i, j, flag)
				elseif j = c then
					down(s, i, j, flag)
				else
					right(s, i, j, flag)
					down(s, i, j, flag)
				end
			end
		end

	right(s:STRING; i, j:INTEGER; f:BOOLEAN)
		require
			s /= Void
		local
			string:STRING
			j1:INTEGER
		do
			if grid.item (i, j + 1) /= -1 then
				j1 := j + 1
				string := s + "r "
				path(string, i, j1, f)
			end
		end

	down(s:STRING; i, j:INTEGER; f:BOOLEAN)
		require
			s /= Void
		local
			string:STRING
			i1:INTEGER
		do
			if grid.item (i + 1, j) /= -1 then
				i1 := i + 1
				string := s + "d "
				path(string, i1, j, f)
			end
		end

invariant
	grid_exist: r > 0 and c > 0

end
