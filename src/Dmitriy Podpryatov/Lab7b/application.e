note
	description: "hangman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			h:HANG_MAN

		do
			--| Add your code here
			create h.make (2, 10) -- first arg - number of players; second - number of guesses. Enjoy the game!
		end

end
