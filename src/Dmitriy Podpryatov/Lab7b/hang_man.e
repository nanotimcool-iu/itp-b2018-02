note
	description: "Summary description for {HANG_MAN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HANG_MAN

create
	make

feature
	make(players, number_of_guesses:INTEGER)
		require
			players > 0 and number_of_guesses > 0
		do
			ps := players
			gs := number_of_guesses
			word := ""
			create guessed_char.make (0)
			create wrong_char.make(0)
			create dictionary.make
			fill_d()
			game_play()
		end

feature
	ps:INTEGER -- number of players
	gs:INTEGER -- number of guesses
	word:STRING -- word to guess
	guessed_char:ARRAYED_LIST[CHARACTER] -- array of guessed chars
	wrong_char:ARRAYED_LIST[CHARACTER] -- array of incorrect guesses
	dictionary:LINKED_SET[STRING]

feature
	game_play
		-- Game process
		local
			i:INTEGER
			flag:BOOLEAN
			guess:STRING
		do
			print("There are " + ps.out + " players!%N")
			from
				i := 1
			until
				not (i <= ps)
			loop
				print("Player " + i.out + ", ")
				i := i + 1
			end
			print("%N")

			make_a_word()
			print("Computer thinks of a word, start guessing!%N")
			print("%N")

			from
				i := 0
			until
				not (flag = false)
			loop
				if gs = 0 then
					print("You have lost.%N")
					print("Word was: " + word)
					flag := true
				else
					print_word()
					print_wrong_chars()
					print("Attempts left: " + gs.out + "%N")
					print("Player's " + (i \\ ps + 1).out + " guess: ")
					Io.read_line
					guess := Io.last_string
					Io.new_line
					if guessed_char.has (guess.at (1)) then
						print("This char has already been guessed.%N")
						i := i + 1
					elseif word.has (guess.at (1)) then
						print("You have guessed a char!%N%N")
						add_guess(guess.at (1))
						flag := check_win(i)
					else
						print("The word does not containt this char.%N")
						add_wrong(guess.at (1))
						gs := gs - 1
						i := i + 1
					end
					print("%N")
				end
			end
		end

feature
	fill_d
		-- Fills fictionary with words
		local
			a:ARRAY[STRING]
		do
			a := <<"boy", "woman", "student", "girl", "teacher", "president", "man", "mother", "book",
			 "tree", "name", "computer", "bird", "idea","place", "picture", "dog", "love">>
			 across
			 	a as array
			 loop
			 	dictionary.extend (array.item)
			 end
		end

	make_a_word
		-- Makes a word to guess
		local
			random:RANDOM
			flag:BOOLEAN
		do
			create random.make
			random.start
			from
			until
				not (flag = false)
			loop
				random.forth -- Strange random in Eiffel; It always chooses one and only one
				-- You should add as many "random.forth" as you want to change the word to guess. LOL
				if dictionary.at (random.item \\ dictionary.count + 1).count <= gs then
					word := dictionary.at (random.item \\ dictionary.count + 1)
					flag := true
				end
			end
		end

	print_word
		-- Prints a word in format __a__bc_ where "_" is yet unguessed char
		local
			i:INTEGER
		do
			print("A word: ")
			from
				i := 1
			until
				not (i <= word.count)
			loop
				if guessed_char.has (word[i]) then
					print(word[i].out + " ")
				else
					print("_ ")
				end
				i := i + 1
			end
			print("%N")
		end

	print_wrong_chars
		-- Prints list of guessed chars
		local
			i:INTEGER
		do
			print("Guessed chars: ")
			from
				i := 1
			until
				not (i <= wrong_char.count)
			loop
				print(wrong_char.at (i).out + " ")
				i := i + 1
			end
			print("%N")
		end

	add_guess(c:CHARACTER)
		-- Adds character to the guessed_char
		do
			guessed_char.extend (c)
		ensure
			guessed_char.has (c)
		end

	add_wrong(c:CHARACTER)
		-- Adds character to the wrong_char
		do
			wrong_char.extend (c)
		ensure
			wrong_char.has (c)
		end

	check_win(player:INTEGER):BOOLEAN
		-- Checks if victory
		-- Returns true if win
		local
			i:INTEGER
		do
			Result := true
			from
				i := 1
			until
				not (i <= word.count)
			loop
				if guessed_char.has (word[i]) = false then
					Result := false
				end
				i := i + 1
			end
			if Result = true then
				print("Player " + (i \\ ps + 1).out + " has won! Congratulations!")
			end
		end

end
