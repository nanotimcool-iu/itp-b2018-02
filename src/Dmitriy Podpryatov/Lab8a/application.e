note
	description: "bin_tree application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			b, l, r, l1, l2, l3:BIN_TREE[INTEGER]
		do
			create b.make (1)
			create l.make (2)
			create r.make (3)
			create l1.make (4)
			create l2.make (5)
			create l3.make (6)

			b.add_left (l)
			b.add_right (r)
			l.add_left (l1)
			l1.add_left (l2)
			l2.add_left (l3)

			print(b.height.out + "%N")
			print(l.height.out + "%N")
			print(r.height.out + "%N")
			print(l1.height.out + "%N")
			print(l2.height.out + "%N")
			print(l3.height.out + "%N")

			-- Respresents tree
			--      1
			--     2 3
			--    4
			--   5
			--  6

		end

end
