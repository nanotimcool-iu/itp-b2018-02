note
	description: "Summary description for {BIN_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIN_TREE[G]

create
	make

feature	-- Constructor
	make(i:G)
		require
			i /= Void
		do
			info := i
		end

feature
	info:G

	left: detachable BIN_TREE[G]

	right: detachable BIN_TREE[G]

	height:INTEGER
		local
			left_sub, right_sub:INTEGER
		do
			if left = Void and right = Void then
				Result := 1
			else
				if attached left as l then
					left_sub := l.height + 1
				end
				if attached right as r then
					right_sub := r.height + 1
				end
				Result := left_sub.max (right_sub)
			end
		end

	add_left(t: BIN_TREE[G])
		require
			t /= Void
		do
			left := t
		end

	add_right(t: BIN_TREE[G])
		require
			t /= Void
		do
			right := t
		end

end
