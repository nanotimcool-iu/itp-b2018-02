note
	description: "huffman_code application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			hc:HUFFMAN_CODE
			s, encoding_string:STRING
			freqs:HASH_TABLE[INTEGER, CHARACTER]
			bin_code:HASH_TABLE[STRING, CHARACTER]

		do
			s := "hello world"
			create hc.make (s)

			freqs := hc.find_freq (s)
			print("Frequency%N")
			across
				freqs as f
			loop
				print(f.key.out + " " + f.item.out + "%N")
			end

			bin_code := hc.binary_code (s)
			print("Binary code%N")
			across
				bin_code as b
			loop
				print(b.key.out + " " + b.item.out + "%N")
			end

			print("Encoding string%N")
			encoding_string := hc.encode_string (s)
			print(encoding_string + "%N")

			print("Decoding string%N")
			print(hc.decode_string (encoding_string, bin_code) + "%N")

			print("-------------")
		end

end
