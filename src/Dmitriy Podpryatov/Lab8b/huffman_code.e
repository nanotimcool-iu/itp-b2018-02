note
	description: "Summary description for {HUFFMAN_CODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN_CODE

create
	make

feature {NONE}
	make(a_string: STRING)
		require
			non_empty_string: a_string.count > 0
		local
			l_queue: HEAP_PRIORITY_QUEUE[HUFFMAN_NODE[CHARACTER]]
			l_counts: HASH_TABLE[INTEGER, CHARACTER]
			l_node: HUFFMAN_NODE[CHARACTER]
			l_left, l_right: HUFFMAN_NODE[CHARACTER]
		do
			create l_queue.make (a_string.count)
			create l_counts.make (10)

			across a_string as char
			loop
				if not l_counts.has (char.item) then
					l_counts.put (0, char.item)
				end
				l_counts.replace (l_counts.at (char.item) + 1, char.item)
			end

			create leaf_dictionary.make(l_counts.count)

			across l_counts as kv
			loop
				create l_node.leaf_node (kv.item, kv.key)
				l_queue.put (l_node)
				leaf_dictionary.put (l_node, kv.key)
			end

			from
			until
				l_queue.count <= 1
			loop
				l_left := l_queue.item
				l_queue.remove
				l_right := l_queue.item
				l_queue.remove

				create l_node.inner_node (l_left, l_right)
				l_queue.put (l_node)
			end

			root := l_queue.item
			root.is_zero := false
		end


feature
	root: HUFFMAN_NODE[CHARACTER]
	leaf_dictionary: HASH_TABLE[HUFFMAN_NODE[CHARACTER], CHARACTER]

	encode(a_value: CHARACTER): STRING
	require
		encodable: leaf_dictionary.has (a_value)
	local
		l_node: HUFFMAN_NODE[CHARACTER]
	do
		Result := ""
		if attached  leaf_dictionary.item (a_value) as attached_node then
			l_node := attached_node
			from

			until
				l_node.is_root
			loop
				Result.append_integer (l_node.bit_value)
				if attached l_node.parent as parent then
					l_node := parent
				end
			end

			Result.mirror
		end
	end


feature
	find_freq(s:STRING):HASH_TABLE[INTEGER, CHARACTER]
		require
			s /= Void
		local
			c:CHARACTER
		do
			create Result.make(0)
			across
				s as st
			loop
				c := st.item
				if c.is_alpha or c.is_space then
					if Result.has(c) then
						Result.force(Result.at(c) + 1, c)
					else
						Result.put(1, c)
					end
				end
			end
		end

	binary_code(s:STRING):HASH_TABLE[STRING, CHARACTER]
		require
			s /= Void
		local
			chars:BINARY_SEARCH_TREE_SET[CHARACTER]
		do
			create Result.make (0)

			create chars.make
			chars.fill (s)

			from
				chars.start
			until
				chars.off
			loop
				Result.put (encode (chars.item), chars.item)
				chars.forth
			end
		end

	encode_string(s:STRING):STRING
		do
			Result := ""
			across
				s as c
			loop
				Result := Result + encode(c.item)
			end
		end

	decode_string(encoding_tring:STRING; bin_code:HASH_TABLE[STRING, CHARACTER]):STRING
		require
			bin_code /= Void
		local
			i: INTEGER
			substring, letter:STRING
			reverse_bin_code:HASH_TABLE[CHARACTER, STRING]
		do
			create reverse_bin_code.make (0)
			across
				bin_code as b
			loop
				reverse_bin_code.put (b.key, b.item)
			end

			Result := ""
			substring := ""
			from
				i := 1
			until
				not (i <= encoding_tring.count)
			loop
				substring := substring + encoding_tring.at (i).out
				if reverse_bin_code.has_key (substring) then
					Result := Result + reverse_bin_code.item (substring).out
					substring := ""
				end
				i := i + 1
			end
		end

end
