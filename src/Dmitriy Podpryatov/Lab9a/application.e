note
	description: "ciphers application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			vc:VIGENERE_CIPHER
			string, key, encrypting_string, decrypting_string:STRING

			sc:SPIRAL_CIPHER
			i, j:INTEGER
			a:ARRAY2[STRING]

			cc:COMBINED_CIPHER
			list:ARRAYED_LIST[CIPHER]
			pass_phrase:STRING
			code:STRING
		do
			--- Test of Vigener Cipher
			string := "STUDENTS, SOLVE THE ASSIGNMENT WELL AND FAST!"
			key := "TIGER"

			create vc.make (key)

			print("Vigenere cipher%N")
			print("String: " + string + "%N")
			print("Key: " + vc.string_of_keys (string) + "%N")

			encrypting_string := vc.encrypt (string)

			print("Encrypt: " + encrypting_string + "%N")
			print("---%N")

			decrypting_string := vc.decrypt (encrypting_string)
			print("Decrypt: " + decrypting_string + "%N")
			print("---%N")
			--- Test of Spiral Cipher
			create sc
			print("Spiral cipher%N")
			print("Encrypt: " + sc.encrypt (encrypting_string) + "%N")
			print("---%N")

			print("Decrypt: " + sc.decrypt (sc.encrypt (encrypting_string)) + "%N")
			print("---%N")
			-- Test of Combined Cipher
			vc.make ("BUSY")
			create list.make(0)
			list.extend (vc)
			list.extend (sc)
			create cc.make(list)
			print("Combined Cipher%N")
			print("String: " + "MYLASTASSIGNMENT" + "%N")
			print("Key: " + "BUSY" + "%N")
			pass_phrase := cc.encrypt ("MYLASTASSIGNMENT")
			print("Encrypt: " + pass_phrase + "%N")

			code := "LAGHYH QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF   FAZ2/TGBZKFIREE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIULZIE KFYGEL//:RLH DXE Z"
			print("%N")
			print("Pass phrase: " + pass_phrase + "%N")
			print("Code: " + code + "%N")

			vc.make (pass_phrase)
			print(cc.decrypt (code))
		end

end
