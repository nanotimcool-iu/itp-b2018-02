note
	description: "Summary description for {CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	CIPHER

feature
	encrypt(string:STRING):STRING
		deferred
		end

	decrypt(string:STRING):STRING
		deferred
		end

end
