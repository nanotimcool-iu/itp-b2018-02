note
	description: "Summary description for {COMBINED_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMBINED_CIPHER

inherit
	CIPHER
		redefine
			encrypt, decrypt
		end

create
	make

feature
	list:LIST[CIPHER]

feature -- Constructor
	make(a_list:LIST[CIPHER])
		require
			non_empty: a_list /= Void
		do
			list := a_list
		ensure
			list = a_list
		end

feature
	encrypt(string:STRING):STRING
		require else
			non_empty: string.count > 0
		local
			i:INTEGER
		do
			Result := string
			from
				i := 1
			until
				not (i <= list.count)
			loop
				Result := list.at (i).encrypt (Result)
				i := i + 1
			end
		end


	decrypt(string:STRING):STRING
		require else
			non_empty: string.count > 0
		local
			i:INTEGER
		do
			Result := string
			from
				i := list.count
			until
				not (i >= 1)
			loop
				Result := list.at (i).decrypt (Result)
				i := i - 1
			end
		end

end
