note
	description: "Summary description for {SPIRAL_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SPIRAL_CIPHER

inherit
	CIPHER
		redefine
			encrypt, decrypt
		end

feature
	encrypt(string:STRING):STRING
		require else
			upper_case: string.is_equal (string.as_upper)
			npn_empty: string.count > 0
		local
			a:ARRAY2[STRING]
			math:SINGLE_MATH
			i, row, col:INTEGER
		do
			Result := ""
			create math
			create a.make_filled (" ", math.sqrt (string.count).ceiling, math.sqrt (string.count).ceiling)
			-- Fill in an array

			i := 1
			from
				row := 1
			until
				not (row <= a.height)
			loop
				from
					col := 1
				until
					not (col <= a.width)
				loop
					if i <= string.count then
						a.item (row, col) := string.at (i).out
						col := col + 1
						i := i + 1
					else
						row := a.height + 1
						col := a.width + 1
					end
				end
				row := row + 1
			end
			Result := spiral_reading(a)
		end


	decrypt(string:STRING):STRING
		require else
			upper_case: string.is_equal (string.as_upper)
			non_empty: string.count > 0
		local
			a:ARRAY2[STRING]
			math:SINGLE_MATH
			i, j:INTEGER
		do
			Result := ""
			create math
			a := spiral_writing(string)

			from
				i := 1
			until
				not (i <= a.height)
			loop
				from
					j := 1
				until
					not (j <= a.width)
				loop
					Result := Result + a.item (i, j)
					j := j + 1
				end
				i := i + 1
			end
		end


feature -- Auxiliary algorythms
	spiral_reading(a:ARRAY2[STRING]):STRING
		-- Read a matrix in spyral way and copy it into string
		require
			not_void: a /= Void
			quadratic: a.width = a.height
		local
			sub_array2:ARRAY2[STRING]
			i, j:INTEGER
		do
			Result := ""
			if a.width = 1 then
				Result := a.item (1, 1)
			elseif a.width = 2 then
				Result := a.item (1, 2) + a.item (2, 2) + a.item (2, 1) + a.item (1, 1)
			else
				create sub_array2.make_filled ("", a.height - 2, a.width - 2)
				-- Right Vertical reading
				from
					i := 1
				until
					not (i <= a.height)
				loop
					Result := Result + a.item (i, a.width)
					i := i + 1
				end
				-- Low Horizontal reading
				from
					i := a.width - 1
				until
					not (i >= 1)
				loop
					Result := Result + a.item (a.height, i)
					i := i - 1
				end
				-- Left Vertical reading
				from
					i := a.height - 1
				until
					not (i >= 1)
				loop
					Result :=  Result + a.item (i, 1)
					i := i - 1
				end
				-- High Horizontal reading
				from
					i := 2
				until
					not (i <= a.width - 1)
				loop
					Result := Result + a.item (1, i)
					i := i + 1
				end
				-- Make a submatrix (a.heigt - 1) x (a.width - 1)
				from
					i := 2
				until
					not (i <= a.height - 1)
				loop
					from
						j := 2
					until
						not (j <= a.width - 1)
					loop
						sub_array2.item (i - 1, j - 1) := a.item (i, j)
						j := j + 1
					end
					i := i + 1
				end
				Result := Result + spiral_reading(sub_array2)
			end
		end


	spiral_writing(string:STRING):ARRAY2[STRING]
		-- Write a string into matrix in spyral way
		local
			a:ARRAY2[STRING]
			math:SINGLE_MATH
			i, j, k, s, l:INTEGER
		do
			create math
			create a.make_filled ("", math.sqrt (string.count).ceiling, math.sqrt (string.count).ceiling)
			s := string.count -- Index to go through string
			i := a.height // 2
			j := a.width // 2
			-- Go in spiral way to write in matrix beginning from its center
			if a.width \\ 2 = 0 then
				-- In case of even number of rows
				a.item (i, j) := string[s].out
				i := i + 1
				s := s - 1
				a.item (i, j) := string[s].out
				j := j + 1
				s := s - 1
				a.item (i, j) := string[s].out
				i := i - 1
				s := s - 1
				a.item (i, j) := string[s].out
				from
					k := 3
				until
					not (i /= 1 and j /= a.width)
				loop
					-- Left:
					i := i - 1
					from
						l := 1
					until
						not (l <= k)
					loop
						s := s - 1
						a.item (i, j) := string[s].out
						j := j - 1

						l := l + 1
					end
					j := j + 1
					-- Down:
					i := i + 1
					from
						l := 1
					until
						not (l <= k)
					loop
						s := s - 1
						a.item (i, j) := string[s].out
						i := i + 1

						l := l + 1
					end
					i := i - 1
					-- Right:
					j := j + 1
					from
						l := 1
					until
						not (l <= k)
					loop
						s := s - 1
						a.item (i, j) := string[s].out
						j := j + 1

						l := l + 1
					end
					j := j - 1
					-- Up:
					i := i - 1
					from
						l := 1
					until
						not (l <= k)
					loop
						s := s - 1
						a.item (i, j) := string[s].out
						i := i - 1

						l := l + 1
					end
					i := i + 1

					k := k + 2
				end
			else
				-- In case of odd number of rows
				i := i  + 1
				j := j + 1
				a.item (i, j) := string[s].out
				from
					k := 2
				until
					not (i /= 1 and j /= a.width)
				loop
					-- Left:
					i := i - 1
					from
						l := 1
					until
						not (l <= k)
					loop
						s := s - 1
						a.item (i, j) := string[s].out
						j := j - 1

						l := l + 1
					end
					j := j + 1
					-- Down:
					i := i + 1
					from
						l := 1
					until
						not (l <= k)
					loop
						s := s - 1
						a.item (i, j) := string[s].out
						i := i + 1

						l := l + 1
					end
					i := i - 1
					-- Right:
					j := j + 1
					from
						l := 1
					until
						not (l <= k)
					loop
						s := s - 1
						a.item (i, j) := string[s].out
						j := j + 1

						l := l + 1
					end
					j := j - 1
					-- Up:
					i := i - 1
					from
						l := 1
					until
						not (l <= k)
					loop
						s := s - 1
						a.item (i, j) := string[s].out
						i := i - 1

						l := l + 1
					end
					i := i + 1

					k := k + 2
				end
			end
			Result := a
		end


end
