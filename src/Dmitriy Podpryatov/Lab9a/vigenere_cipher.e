note
	description: "Summary description for {VIGENERE_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHER

inherit
	CIPHER
		redefine
			encrypt, decrypt
		end

create
	make

feature
	make(a_key:STRING)
		require
			not_void: a_key /= Void
			upper_case: a_key.is_equal (a_key.as_upper)
		do
			key := a_key
		ensure
			key.is_equal(a_key)
		end

feature {NONE}
	key:STRING

feature
	encrypt(string:STRING):STRING
		require else
			upper_case: string.is_equal (string.as_upper)
			non_empty: string.count > 0
		local
			alphabet:ARRAY[STRING]
			key_string:STRING
			i, j, code_string, code_key:INTEGER
		do
			alphabet := <<"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z">>
			Result := ""
			key_string := ""
			-- Make a string of copies of 'key'
			from
				i := 1
				j := 1
			until
				not (i <= string.count)
			loop
				if string.at (i).is_alpha then
					key_string := key_string + key.at((j - 1) \\ key.count + 1).out
					j := j + 1
				else
					key_string := key_string + string.at (i).out
				end
				i := i + 1
			end
			-- Find code of i-th symbol of string and corresponding code of key's symbol
			from
				i := 1
			until
				not (i <= string.count)
			loop
				if string.at (i).is_alpha then
					from
						j := 1
					until
						not (j <= alphabet.count)
					loop
						if alphabet[j].is_equal (string.at (i).out) then
							code_string := j
						end
						if alphabet[j].is_equal (key_string.at (i).out) then
							code_key := j
						end
						j := j + 1
					end
					Result := Result + alphabet[(code_string + code_key - 2) \\ alphabet.count + 1] -- '-2' and '+1' appear because
																									-- arrays in Eiffel start from 1
				else
					Result := Result + string.at (i).out
				end
				i := i + 1
			end
		end


	decrypt(string:STRING):STRING
		require else
			upper_case: string.is_equal (string.as_upper)
			non_empty: string.count > 0
		local
			alphabet:ARRAY[STRING]
			key_string:STRING
			i, j, code_string, code_key:INTEGER
		do
			alphabet := <<"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z">>
			Result := ""
			key_string := ""
			-- Make a string of copies of 'key'
			from
				i := 1
				j := 1
			until
				not (i <= string.count)
			loop
				if string.at (i).is_alpha then
					key_string := key_string + key.at((j - 1) \\ key.count + 1).out
					j := j + 1
				else
					key_string := key_string + string.at (i).out
				end
				i := i + 1
			end
			-- Find code of i-th symbol of string and corresponding code of key's symbol
			from
				i := 1
			until
				not (i <= string.count)
			loop
				if string.at (i).is_alpha then
					from
						j := 1
					until
						not (j <= alphabet.count)
					loop
						if alphabet[j].is_equal (string.at (i).out) then
							code_string := j
						end
						if alphabet[j].is_equal (key_string.at (i).out) then
							code_key := j
						end
						j := j + 1
					end
					if code_string - code_key >= 0 then
						Result := Result + alphabet[(code_string - code_key) \\ alphabet.count + 1]
					else
						Result := Result + alphabet[(code_string - code_key + alphabet.count) \\ alphabet.count + 1]
					end
				else
					Result := Result + string.at (i).out
				end
				i := i + 1
			end
		end


feature -- To check algorythm of making a string of copies of 'key'
	string_of_keys(string:STRING):STRING
		local
			i, j:INTEGER
		do
			Result := ""
			-- Make a string of copies of 'key'
			from
				i := 1
				j := 1
			until
				not (i <= string.count)
			loop
				if string.at (i).is_alpha then
					Result := Result + key.at((j - 1) \\ key.count + 1).out
					j := j + 1
				else
					Result := Result + string.at (i).out
				end
				i := i + 1
			end
		end

end
