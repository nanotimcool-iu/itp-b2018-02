note
	description: "library_system application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			ls:LIBRARY_SYSTEM
			b:BOOK
			p:PERSON
		do
			create ls.make
			create b.make ("title", "author")
			create p.make ("name", "address", "phone_number", "library_card_number")
			b.price := 99
			p.deposit (98)
			ls.add_book (b)
			ls.add_user (p)
			ls.check_out (p, b)
			print("Is overdue: " + p.is_overdue (b).out + "%N")
			p.is_child := true
			print("Balance of p: " + p.balance.out)
		end

end
