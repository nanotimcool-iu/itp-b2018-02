note
	description: "Summary description for {BOOK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOOK

create
	make

feature -- Constructor
	make(a_title, a_author:STRING)
		require
			non_empty: (a_title /= Void) and (a_author /= Void)
		do
			title := a_title
			author := a_author
			id := title + " " + author
			is_adult := false
		ensure
			title = a_title
			author = a_author
			id.is_equal(title + " " + author)
			is_adult = false
		end

feature -- Variables
	author:STRING

	title:STRING

	id:STRING

	is_adult:BOOLEAN assign set_adult

	set_adult(a_is_adult:BOOLEAN)
		do
			is_adult := a_is_adult
		end

	is_best_celler:BOOLEAN

	date: detachable DATE assign set_date

	set_date(a_date:detachable DATE)
		require
			non_empty: a_date /= Void
		do
			date := a_date
		ensure
			date = a_date
		end

	price:INTEGER assign set_price

	set_price(a_price:INTEGER)
		do
			price := a_price
		end

end
