note
	description: "Summary description for {LIBRARY_BOOK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LIBRARY_SYSTEM

create
	make

feature -- Constructor
	make
		do
			create users.make(0)
			create books.make(0)
		end

feature {NONE}-- Variables
	users:ARRAYED_LIST[PERSON] -- All users in the system

feature
	books:HASH_TABLE[INTEGER, STRING] -- All books in the library. Key = 'id' of the book, value = number of available books

feature -- Queries
	add_user(p:PERSON)
		require
			non_void: p /= Void
		do
			users.extend (p)
		ensure
			users.has (p)
		end

	add_book(b:BOOK)
		require
			non_void: b /= Void
		do
			if books.has (b.id) then
				books.force (books.item (b.id) + 1, b.id)
			else
				books.extend (1, b.id)
			end
		ensure
			books.has (b.id)
		end

	check_out(p:PERSON; b:BOOK)
		require
			book_exist: books.has (b.id)
			person_exist: p /= Void
		do
			p.check_out_book (b)
			if books.item (b.id) = 1 then
				books.remove (b.id)
			else
				books.force (books.item (b.id) - 1, b.id)
			end
		end

end
