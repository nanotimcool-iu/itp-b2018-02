note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature {NONE} -- Variables
	name:STRING

	address:STRING

	phone_number:STRING

	library_card_number:STRING

	items:INTEGER -- Number of books checked out

	checked_books:HASH_TABLE[BOOK, STRING] -- Hash table of checked out books. Key = 'id' of the book, value = the book

feature
	is_child:BOOLEAN assign set_child

	set_child(a_is_child:BOOLEAN)
		do
			is_child := a_is_child
		ensure
			is_child = a_is_child
		end

	balance:INTEGER

feature -- Constructor
	make(a_name, a_address, a_phone_number, a_library_card_number:STRING)
		require
			non_empty_data: (a_name /= Void) and (a_address /= Void) and (a_phone_number /= Void) and (a_library_card_number /= Void)
		do
			name := a_name
			address := a_address
			phone_number := a_phone_number
			library_card_number := a_library_card_number
			is_child := false
			items := 0
			create checked_books.make (0)
		ensure
			name = a_name
			address = a_address
			phone_number = a_phone_number
			library_card_number = a_library_card_number
			is_child = false
			items = 0
		end

feature -- Commands
	check_out_book(book:BOOK)
		require
			non_void: book /= Void
		local
			date:DATE
			b:BOOK
		do
			if is_child and items > 5 then
				-- Do nothing or print that "you cannot have more books"
			elseif is_child and book.is_adult then
				-- Do nothing or print that "you cannot have this book"
			else
				create date.make_now
				b := book.twin
				b.date := date
				items := items + 1
				checked_books.extend (b, b.id)
				balance := balance - b.price
			end
		end

	deposit(money:INTEGER)
		do
			balance := balance + money
		end

feature {NONE}
	fine(book:BOOK)
		do
			if book.price > 100 then
				balance := balance - 100
			else
				balance := balance - book.price
			end
		end

feature -- Queries

	has_overdue:BOOLEAN
		do
			across
				checked_books as cb
			loop
				if is_overdue(cb.item) then
					Result := true
				end
			end
		end

	is_overdue(book:BOOK):BOOLEAN
		require
			non_empty: book /= Void
		local
			three_weeks, two_weeks:DATE_DURATION
			now:DATE
		do
			if attached book.date as d then
				create three_weeks.make_by_days (21)
				create two_weeks.make_by_days (14)
				create now.make_now
				if book.is_best_celler then
					Result := (d.plus (two_weeks)).is_less (now)
				else
					Result := (d.plus (three_weeks)).is_less (now)
				end
			end
			if Result then
				fine(book)
			end
		end

end
