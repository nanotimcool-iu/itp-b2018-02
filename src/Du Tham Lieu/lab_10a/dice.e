note
	description: "Summary description for {DICE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DICE

create
	make_random

feature -- {NONE} -- Initialization

	make_random
		local
			l_time: TIME
			l_seed: INTEGER
		do
			create l_time.make_now
			l_seed := l_time.hour
			l_seed := l_seed * 60 + l_time.minute
			l_seed := l_seed * 60 + l_time.second
			l_seed := l_seed * 1000 + l_time.milli_second
			create random_sequence.set_seed (l_seed)
		end

--	two_new_dice: TUPLE [INTEGER, INTEGER]
--			-- Returns a tuple containing two unrelated random numbers between 1 and 6.
--		do
--			Result := [new_random \\ 6 + 1, new_random \\ 6 + 1]
--		end

		two_dice: TUPLE [INTEGER, INTEGER]
			-- A new tuple containing two unrelated random numbers between 1 and 6.
		do
			create Result
			random_sequence.forth
			Result [1] := random_sequence.item \\ 6 + 1
			random_sequence.forth
			Result [2] := random_sequence.item \\ 6 + 1
		end

--		sum_dice: INTEGER
--		local
--			d: DICE
--		do
--			create d.two_dice
--			result := result [1] + result [2]
--		end

feature {NONE} -- Implementation

	random_sequence: RANDOM

	new_random: INTEGER
			-- Random integer
			-- Each call returns another random number.
		do
			random_sequence.forth
			Result := random_sequence.item
		end

end
