note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	MONOPOLY

inherit
	ARGUMENTS_32

create
	make
feature {NONE} -- Initialization

	make

		local
			d: DICE
		do
			create d.make_random
			print(d.two_dice)
		end

end
