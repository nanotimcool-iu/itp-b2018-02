note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			the_coffee_house :THE_COFFEE_SHOP
			p1, p2, p3:PRODUCT
		do
			create the_coffee_house.open
			create {ESPRESSO} p1.set_cof_price (35000, 15000)
			create {CAPPUCINO} p2.set_cof_price (70000, 35000)
			p3 := p1
			the_coffee_house.restock (p1)
			the_coffee_house.restock (p2)
			the_coffee_house.restock (p3)
			print("Menu"); io.new_line; the_coffee_house.print_menu
			io.new_line
			print("Cha-ching: " + the_coffee_house.profit.out)


		end
end
