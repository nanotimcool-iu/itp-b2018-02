note
	description: "Summary description for {CAPPUCINO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAPPUCINO

inherit
	COFFEE
		redefine
			set_cof_price
		end
create
	set_cof_price

feature {NONE} -- Initialization

	set_cof_price(new_pub_price, new_price: REAL)
			-- Initialization for `Current'.
		do
			precursor(new_pub_price, new_price)
			description:= "Making Cappucino, decaf is for the weak.%N"
		end

end
