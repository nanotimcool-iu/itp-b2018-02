note
	description: "Summary description for {COFFEE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	COFFEE

inherit
	PRODUCT

feature
	set_cof_price(new_pub_price, new_price: REAL)
	do
		set_pub_price(new_pub_price)
		set_price(new_price)
	end
end
