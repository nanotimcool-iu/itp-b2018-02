note
	description: "Summary description for {ESPRESSO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ESPRESSO

inherit
	COFFEE
		redefine
			set_cof_price
		end
create
	set_cof_price

feature {NONE} -- Initialization

	set_cof_price(new_pub_price, new_price: REAL)
			-- Initialization for `Current'.
		do
			precursor(new_pub_price, new_price)
			description:= "Making Espresso, because I coded until 4am in the mornig, I hate my life.%N"
		end

end
