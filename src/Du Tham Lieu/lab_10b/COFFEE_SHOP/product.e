note
	description: "Summary description for {PRODUCT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PRODUCT
feature
	price, pub_price: REAL
	description: STRING
feature
	set_price(new_price: REAL)
	do
		price:= new_price
	end

	set_pub_price(new_price: REAL)
	do
		pub_price:= new_price
	end

	profit: REAL
	do
		Result:= pub_price - price
	end
invariant
	price < pub_price
end
