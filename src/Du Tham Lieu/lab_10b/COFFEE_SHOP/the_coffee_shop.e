note
	description: "Summary description for {THE_COFFEE_SHOP}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	THE_COFFEE_SHOP

create
	open
feature
	menu: ARRAYED_SET[PRODUCT]

feature {APPLICATION} -- Initialization

	open
			-- Initialization for `Current'.
		do
			create menu.make(0)
		end

	restock(new_stock: PRODUCT)
	do
		menu.put(new_stock)
	end

	profit:REAL
		do
			across menu as i
			loop
				Result := Result + i.item.profit
			end
		end

	print_menu
		do
			across menu as i
			loop
				print(i.item.description + " - " + i.item.pub_price.out)
				menu.forth
				io.new_line
			end
		end
end
