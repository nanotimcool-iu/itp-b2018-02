class
	BUSINESS_CARD

create
	fill_in

feature {NONE} -- Initialization

	fill_in
			-- Fill in the card and print it.
		do
				-- Add your code here.
			print ("Who's my daddy?")
			Io.new_line
			Io.read_line
			set_name (Io.last_string)

			print ("Job?")
			Io.new_line
			Io.read_line
			set_job (Io.last_string)

			print ("Age?")
			Io.new_line
			Io.read_integer
			set_age (Io.last_integer)

			print_card
		end

feature -- Access

	name: STRING
			-- Owner's name.

	job: STRING
			-- Owner's job.

	age: INTEGER
			-- Owner's age.

feature -- Setting

	set_name (a_name: STRING)
			-- Set `name' to `a_name'.
		require
			name_exists: a_name /= Void
		do
			name := a_name.twin
		end

	set_job (a_job: STRING)
			-- Set `job' to `a_job'.
		require
			job_exists: a_job /= Void
		do
			job := a_job.twin
		end

	set_age (a_age: INTEGER)
			-- Set `age' to `a_age'.
		require
			age_non_negative: a_age >= 0
		do
			age := a_age
		end

feature -- Output

	age_info: STRING
			-- Text representation of age on the card.
		do
			Result := "I'm : " + age.out + " years old"
		end

		job_info: STRING
			-- Text representation of job on the card.
		do
			Result := "Job: " + job
		end

		name_info: STRING
			-- Text representation of name on the card.
		do
			Result := "Hi, my Name is: " + name
		end

	print_card

		do
			io.put_string (line (width))
			io.new_line
			io.put_string("|" + name_info + spaces(width -(name_info.count + 1))+ "|%N")
			io.put_string("|" + job_info + spaces(width -(job_info.count + 1)) + "|%N")
			io.put_string("|" + age_info)
			io.put_string ( spaces (width - (age_info.out.count) -1 ) + "|%N")
			io.put_string (line (width))
			io.new_line

		end

	Width: INTEGER = 50
--			 Width of the card (in characters), excluding borders.
	spaces (n: INTEGER): STRING
		do
			Result := " "
			Result.multiply (n)
		end
	line (n: INTEGER): STRING
			-- Horizontal line on length `n'.
		do
			Result := "-"
			Result.multiply (n)
		end

end
