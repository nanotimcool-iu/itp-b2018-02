class
	APPLICATION

create
	execute
feature {NONE} -- Initialization

	execute
			-- Run application.
		do
			print("Name: John Smith")
			Io.new_line
			print("Age: 20")
			Io.new_line
			print("Mother tongue: English")
			Io.new_line
			print("Has a cat: True")
		end

end
