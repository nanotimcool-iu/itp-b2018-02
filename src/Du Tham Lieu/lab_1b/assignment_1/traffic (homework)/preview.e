note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		do
			Zurich.add_station ("The Void", 300,-1500)
			Zurich.connect_station ( 4, "The Void")
			Zurich_map.update


		    from

		    until
				False
		    loop
		    	Zurich_map.station_view(Zurich.station ("The Void")).highlight
		    	Wait(1)
		    	Zurich_map.station_view(Zurich.station ("The Void")).unhighlight
		    	Wait(1)
		    end

		end

end
