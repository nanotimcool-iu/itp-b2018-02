note
	description: "BANK application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

inherit
	ARGUMENTS_32

create
	make -- call it what ever I want
feature
	owner: STRING
	balance: DOUBLE
feature
	make-- define and tell the computer what to do

	do
		owner := "John Doe"
		print ("Owner: " + owner)
		io.new_line

		balance := -999998
		print("Balance: " + balance.out)
		io.new_line

		deposit (999999)
		withdraw (1)
	end

feature
	withdraw(difference: DOUBLE)
		do
			balance := balance - difference -- the function is defined here
			print("Withdraw: " + difference.out)
			Io.new_line
			print("Balance: " + balance.out)
			io.new_line
			Io.put_string ("John is broke")

		end

feature
	deposit (sum: DOUBLE)
		do
			balance := balance + sum
			print("Deposit: " + sum.out)
			Io.new_line
			print("Balance: " + balance.out)
			io.new_line

		end

end

