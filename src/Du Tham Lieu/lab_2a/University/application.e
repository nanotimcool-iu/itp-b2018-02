note
	description: "University application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			course_creator : COURSE-- Run application.
		do
			--| Add your code here
			create course_creator.create_class("STFU", "IU2018", 1101, 249)
			Io.put_string ("Done!")
		end

end
