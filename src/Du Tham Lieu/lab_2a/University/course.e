note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"


class
	COURSE
create
	create_class
feature -- declaring stuffs
	course_name : STRING
	course_schedule : STRING
	course_id : INTEGER
	course_max : INTEGER = 250
	course_min : INTEGER = 3
	number_of_students : INTEGER
feature
	create_class (name : STRING; schedule: STRING; id: INTEGER; applicants : INTEGER )
	-- ten o day khong nen trung voi ten o tren, lien ket chung lai o phan duoi
	do
		if number_of_students > course_min or number_of_students > course_max then
			Io.put_string ("Invalid number of students")
		end
		course_name := name;
		course_schedule := schedule;
		number_of_students := applicants

	end
end
