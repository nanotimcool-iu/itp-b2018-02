note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR
create
	make
feature
	make
	local
		e1, e2, e3 : ENTRY
	do
		e1:= create_entry (3112)
	end
feature
	create_entry (d: INTEGER; o: PERSON; s: STRING; pl: STRING): ENTRY
	do
		create e.create_entry(d,o,s,pl)
		Result := e
	end

	edit_subject(e: ENTRY; new_subject: STRING)
		do
			e.set_subject (new_subject)
		end

	edit_date(e: ENTRY; new_date: TIME)
		do
			e.set_date (new_date)
		end

	get_owner_name(e:ENTRY):STRING
		do
			print(e.set_owner (e))
		end

	in_conflict(e1, e2: ENTRY): BOOLEAN
		do
			Result := print((e1 ~ e2).out)
		end
end
