note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY
feature
	date: TIME assign set_date
	owner: PERSON assign set_owner
	subject: STRING assign set_subject
	place: STRING assign set_place

feature -- Setters

	set_date (d: TIME)
		do
			date := d
		end

	set_owner (o: PERSON)
		do
			owner := o
		end

	set_subject (s: STRING)
		do
			subject := s
		end

	set_place (pl: STRING)
		do
			place := pl
		end
end
