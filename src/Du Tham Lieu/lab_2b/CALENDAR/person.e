note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON
create
	create_person
	
feature --Status
	name: STRING
	work_place: STRING
	email: STRING
	phone_number: INTEGER
feature
	create_person (n: STRING; w: STRING; m: STRING; ph: INTEGER)
	do
		name := n
		work_place := w
		email := m
		phone_number:= ph

	end

feature-- Setters

	set_name (n: STRING)
	do
		name := n
	end

	set_work_place (wp: STRING)
	do
		work_place := wp
	end

	set_email (e: STRING)
	do
		email := e
	end

	set_phone_number (ph: INTEGER)
	do
		phone_number := ph
	end

end
