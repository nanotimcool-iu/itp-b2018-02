note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS
create
	make
feature {NONE} -- Initialization

	make
			-- Run application.
		local
			c1, c2, c3: CONTACT
		do
				--| Add your code here
			c1:=create_contact ("name1", "work1", "email1", 1234567890)
			c2:=create_contact ("name2", "work2", "email2", 1234567891)
			c3:=create_contact ("name3", "work3", "email3", 1234567892)
			add_emergency_contact (c1, c2)
			add_emergency_contact (c3, c2)
			edit_contact (c2, "name1", "work1", "an_email", 0123456789)
			remove_emergency_contact (c2)
			remove_emergency_contact (c3)
			print(c1)
			io.new_line
			print(c2)
			io.new_line
			print(c3)
			io.new_line

		end
feature
	create_contact(n: STRING; wp: STRING; m: STRING; ph: INTEGER): CONTACT
	local
		c: CONTACT
  	do
		create c.create_contact(n, wp, m, ph)
		Result := c -- Store value from function.
  	end

	edit_contact (c: CONTACT; n: STRING; wp: STRING; m: STRING; ph: INTEGER)
	do
		c.set_name (n)
		c.set_work_place (wp)
		c.set_email (m)
		c.set_phone_number (ph)

	end

	remove_emergency_contact (c: CONTACT)
	do
		c.set_call_emergency (VOID)
	end

	add_emergency_contact (c, c1: CONTACT)
	do
		c.set_call_emergency (c1)
	end
end
