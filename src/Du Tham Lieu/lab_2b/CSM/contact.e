note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT
create
	create_contact
feature
	name : STRING assign set_name

	work_place : STRING assign set_work_place

	email : STRING assign set_email

	phone_number : INTEGER_64 assign set_phone_number

	call_emergency : detachable CONTACT assign set_call_emergency
	
feature
	create_contact (n : STRING; wp : STRING; m : STRING; ph : INTEGER_64)
	do
		name := n
		work_place := wp
		email := m
		phone_number := ph
		call_emergency :=  void
	end

	set_name (n: STRING)
	do
		name := n
	end

	set_work_place (wp: STRING)
	do
		work_place := wp
	end

	set_email (m: STRING)
	do
		email :=  m
	end

	set_phone_number (ph: INTEGER_64)
	do
		phone_number := ph
	end
	set_call_emergency(c: detachable CONTACT)
	do
		call_emergency := c
	end


end
