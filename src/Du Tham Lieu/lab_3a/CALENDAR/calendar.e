note
	description: "root class of the application"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit

	ARGUMENTS

create
	make

feature -- Initialization

	make
		local
			to_do_list1, to_do_list2: LINKED_LIST [ENTRY]
			p1, p2, p3: PERSON;
			e1, e2, e3: ENTRY;
			d1, d2: SOMETIMES
			i: INTEGER
		do
			create day.make
			t1:= create_time(22, 24)
			t2:= create_time(22, 02)
			t3:= create_time(22, 12)
			create p1.make_person ("name1", "name1@gmail.com", "work_place1", 0123456781)
			create p2.make_person ("name2", "name2@gmail.com", "work_place2", 0123456782)
			create p3.make_person ("name3", "name3@gmail.com", "work_place3", 0123456783)
			e1 := create_entry (t1, p1, "subject1", "place1")
			e2 := create_entry (t2, p2, "subject2", "place2")
			e3 := create_entry (t3, p3, "subject3", "place3")

			create to_do_list1.make
			to_do_list1.put_right(e1)
			to_do_list1.put_right(e2)
			to_do_list1.put_right (e3)

			create to_do_list12.make
			to_do_list2.put_right(e3)

			create d1.make_good_day (2, to_do_list1_list1)
			create d2.make_good_day (4, to_do_list1_list2)

			day.put_right(d1)
			day.put_right(d2)

			res := in_conflict(d1)
			from
				i := 1
			until
				i > res.count
			loop
				print(res[i].printable_entry)
				i := i + 1
			end
			end

	add_entry (e: ENTRY; day: DAY)
		do
			day.get_event.put(e)

		end

	printable_month: STRING
		local
			i: INTEGER
		do
			Result := ""
			from
				i := 1
			until
				i > day.count
			loop
				Result.append (day [i].printable_day + "%N")
				i := i + 1
			end
		end
		in_conflict(day: DAY): LINKED_LIST[ENTRY]
		local
			arr: LINKED_LIST
			i: INTEGER
			j: INTEGER
		do
			create Result.make
			arr := day.get_event
			from
				i:= 1
			until
				i > arr.count
			loop
				from
					j:= 1
				until
					j > arr.count
				loop
					if i /= j then
						if arr[i].get_place.is_same (arr[j].get_place) or arr[i].get_date.is_same(arr[j].get_date) then
							Result.put_right(arr[i])
						end

					end
					j := j + 1
				end
				i := i + 1
			end
		end

feature -- Edits

	edit_date (e: ENTRY; new_date: SOMETIMES)
		do
			e.set_date (new_date)
		end

	edit_owner (e: ENTRY; new_owner: PERSON)
		do
			e.set_owner (new_owner)
		end

	edit_subject (e: ENTRY; new_subject: STRING)
		do
			e.set_subject (new_subject)
		end

	edit_place (e: ENTRY; new_place: STRING)
		do
			e.set_place (new_place)
		end

	get_owner (e: ENTRY): STRING
		do
			Result := e.get_owner.get_name
		end
feature
	create_time(a_hour, a_minute: INTEGER): SOMETIMES
		local
			a_time: SOMETIMES
		do
			create a_time.make_some (a_hour, a_minute)
			Result := new_time
		end

		create_entry(b_time: SOMETIMES; a_owner: PERSON; a_subject, a_place: STRING): ENTRY
		local
			new_entry: ENTRY
		do
			create new_entry.make_entry (b_time, a_owner, a_subject, a_place)
			Result := new_entry
		end
feature
	day: LINKED_LIST[DAY]
end
