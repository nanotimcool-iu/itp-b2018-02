note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	make_good_day

feature

	date: INTEGER

	event: LINKED_LIST [ENTRY]

feature {NONE} -- Initialization

	make_good_day (a_date: INTEGER; a_event: LINKED_LIST [ENTRY])
		do
			date := a_date
			event := a_event
		end

feature -- Getters

	get_event: LINKED_LIST [ENTRY]
		do
			Result := event
		end

	get_date: INTEGER
		do
			Result := date
		end

	printable_day: STRING
		local
			i: INTEGER
		do
			Result := ""
			Result.append ("Day: " + date.out + "%N")
			from
				i := 1
			until
				i > event.count
			loop
				Result.append (event [i].printable_entry + "%N")
				i := i + 1
			end
		end

end
