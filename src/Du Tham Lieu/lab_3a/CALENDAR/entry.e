class
    ENTRY
create
    make_entry
feature
    date: SOMETIMES
    owner: PERSON
    subject: STRING
    place: STRING


    make_entry(a_date: SOMETIMES; a_owner: PERSON; a_subject, a_place: STRING)
    do
        set_date(a_date)
        set_owner(a_owner)
        set_subject(a_subject)
        set_place(a_place)
    end

feature -- Setters

    set_date(a_date: SOMETIMES)
        do
            date:= a_date
        end

    set_owner(a_owner: PERSON)
        do
            owner:= a_owner
        end

    set_subject(a_subject: STRING)
        do
            subject:= a_subject
        end

    set_place(a_place: STRING)
        do
            place:= a_place
        end
feature --Getters
	get_date: SOMETIMES
	do
		Result:= date
	end

	get_owner: PERSON
	do
		Result:= owner
	end

	get_place: STRING
	do
		Result:= place
	end

	get_subject: STRING
	do
		Result:= subject
	end

	printable_entry: STRING
	do
		Result:= ""
		Result.append("Entry: %N")
		Result.append("Time: " + date.printable_time + "%N")
		Result.append("Owneer: " + owner.printable_person + "%N")
		Result.append("Subject: " + subject + "N")
		Result.append("Place: " + place + "%N")
	end
end
