class
	PERSON

inherit

	ARGUMENTS

create
	make_person

feature -- Fields

	a_name: STRING

	a_phone_num: INTEGER_32

	a_work_place: STRING

	a_email: STRING

feature

	make_person (name, email, work_place: STRING; phone_num: INTEGER_32)
			-- Run application.
		do
				-- Add your code here
			set_name (name)
			set_email (email)
			set_a_work_place (work_place)
			set_phone (phone_num)
		end

feature -- Setters

	set_name (name: STRING)
		do
			a_name := name
		end

	set_email (email: STRING)
		do
			a_email := email
		end

	set_a_work_place (work: STRING)
		do
			a_work_place := work
		end

	set_phone (phone_num: INTEGER_32)
		do
			a_phone_num := phone_num
		end

feature -- Getters

	get_name: STRING
		do
			Result := a_name
		end

	get_email: STRING
		do
			Result := a_email
		end

	get_work: STRING
		do
			Result := a_work_place
		end

	get_phone: INTEGER
		do
			Result := a_phone_num
		end

	printable_person: STRING
		do
			Result := a_name + " (" + a_phone_num.out + "; " + a_work_place + "; " + a_email + ")"
		end

end
