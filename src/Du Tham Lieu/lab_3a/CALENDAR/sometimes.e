note
	description: "Summary description for {SOMETIMES}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SOMETIMES

create
	make_some

feature

	hour: INTEGER

	minute: INTEGER

feature {NONE} -- Initialization

	make_some (a_hour, a_minute: INTEGER)
			-- Initialization for `Current'.
		require
			valid_hour: is_valid_hour (a_hour)
			valid_minute: is_valid_minute (a_minute)
		do
			hour := a_hour
			minute := a_minute
		end

feature -- Dunno how to say this

	is_valid_hour (a_hour: INTEGER): BOOLEAN
		do
			Result := 0 <= a_hour and 23 >= a_hour
		end

	is_valid_minute (a_minute: INTEGER): BOOLEAN
		do
			Result := 0 <= a_minute and 60 >= a_minute
		end

	printable_time: STRING
		do
			Result := "" -- to ensure target is not VOID
			if hour.out.count < 2 then
				Result.append ("0")
			end
			Result.append (hour.out + ":")
			if minute.out.count < 2 then
				Result.append ("0")
			end
			Result.append (minute.out)
		end

	is_same(other: SOMETIMES): BOOLEAN
		do
			Result:= hour = other.hour and minute = other.minute
		end

invariant
	valid_hour: is_valid_hour (hour)
	valid_minute: is_valid_minute (minute)

end
