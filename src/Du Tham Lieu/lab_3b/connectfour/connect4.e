note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CONNECT4

inherit

	ARGUMENTS_32

create
	make

feature

	board: ARRAY2 [INTEGER]

	n, m, i, j, turn, column: INTEGER

	player1, player2, is_dropped, is_field_correct: BOOLEAN

feature -- Initialization

	make
			-- Run application.
		do -- Getting board size.
			from
			until
				m > 4 and n > 4
			loop
				io.put_string ("Enter correct number of rows (>4): %N")
				io.read_integer
				m := io.last_integer
				io.put_string ("Enter correct number of columns (>4): %N")
				io.read_integer
				n := io.last_integer
			end

				-- Creating the board.
			create board.make_filled (0, m, n)
			io.put_string ("%NThis is your board: %N")
			print_board
			from
			until
				player1 or player2
			loop
				turn := 1
				from
				until
					is_dropped
				loop
					print ("%NPlayer 1, enter the column number:%N")
					Io.read_integer
					column := Io.last_integer
					if is_droppable then
						drop_disc (column)
						is_dropped := true
					else
						print ("%NYou can't drop a disc here. Try again.")
					end
				end
				is_dropped := false
				print_board
				player1 := has_player_won
				if not player1 then
					turn := 2
					from
					until
						is_dropped
					loop
						print ("%NPlayer 2, enter the column number:%N")
						Io.read_integer
						column := Io.last_integer
						if is_droppable then
							drop_disc (column)
							is_dropped := true
						else
							print ("%NYou can't drop a disc here. Try again.")
						end
					end
					is_dropped := false
					print_board
					player2 := has_player_won
				end
			end
		end

feature

	has_player_won: BOOLEAN
		do
			if check_horizontally or check_vertically or check_diagonally then
				Result := true
			else
				Result := false
			end
		end

	print_board
		do
			io.put_string ("%N")
			from
				i := 1
			until
				i > m
			loop
				from
					j := 1
				until
					j > n
				loop
					print (board.item (i, j).out + "  ")
					j := j + 1
				end
				i := i + 1
				print ("%N")
			end
		end

	is_droppable: BOOLEAN
		do
			if (column <= n and column >= 1) and board.item (1, column) = 0 then
				Result := true
			else
				Result := false
			end
		end

	drop_disc (x_coord: INTEGER)
		local
			y_check: INTEGER
		do
			from
				y_check := m
			until
				board.item (y_check, x_coord) = 0
			loop
				y_check := y_check - 1
			end
			board.force (turn, y_check, x_coord)
		end

feature -- Checkers

	check_horizontally: BOOLEAN
		local
			counter: INTEGER
			success: BOOLEAN
		do
			counter := 0
			from
				i := 1
			until
				i > m or success
			loop
				from
					j := 1
				until
					j > n
				loop
					if board.item (i, j) = turn then
						counter := counter + 1
					else
						counter := 0
					end
					if counter = 4 then
						success := true
					end
					j := j + 1
				end
				i := i + 1
			end
			if success then
				Result := true
			else
				Result := false
			end
		end

	check_vertically: BOOLEAN
		local
			counter: INTEGER
			success: BOOLEAN
		do
			counter := 0
			from
				j := 1
			until
				j > n or success
			loop
				from
					i := 1
				until
					i > m
				loop
					if board.item (i, j) = turn then
						counter := counter + 1
					else
						counter := 0
					end
					if counter = 4 then
						success := true
					end
					i := i + 1
				end
				j := j + 1
			end
			if success then
				Result := true
			else
				Result := false
			end
		end

	check_diagonally: BOOLEAN
		local
			counter, x, y: INTEGER
			success: BOOLEAN
		do
			success := false
			x := column
			from
				i := 1
			until
				board.item (i, column) /= 0
			loop
				i := i + 1
			end
			y := i

				-- From top-left
			counter := 1
			i := y
			j := x
			from
			until
				i = 1 or j = 1
			loop
				i := i - 1
				j := j - 1
				if board.item (i, j) = turn then
					counter := counter + 1
					if counter = 4 then
						success := true
					end
				else
					counter := 0
				end
			end

				-- From Bottom-left
			counter := 1
			i := y
			j := x
			from
			until
				i = m or j = 1
			loop
				i := i + 1
				j := j - 1
				if board.item (i, j) = turn then
					counter := counter + 1
					if counter = 4 then
						success := true
					end
				else
					counter := 0
				end
			end

				-- From top-right
			counter := 1
			i := y
			j := x
			from
			until
				i = 1 or j = n
			loop
				i := i - 1
				j := j + 1
				if board.item (i, j) = turn then
					counter := counter + 1
					if counter = 4 then
						success := true
					end
				else
					counter := 0
				end
			end

				-- From bottom-right
			counter := 1
			i := y
			j := x
			from
			until
				i = m or j = n
			loop
				i := i + 1
				j := j + 1
				if board.item (i, j) = turn then
					counter := counter + 1
					if counter = 4 then
						success := true
					end
				else
					counter := 0
				end
			end
			if success then
				Result := true
			else
				Result := false
			end
		end

end
