note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	a:BAG2
	b:BAG2

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create a.create_obj
			create b.create_obj
			a.add ('a', 2)
			a.add ('b',5)
			a.add ('a', 5)

			print(a.min.out+"%N")

			a.remove ('a',7)
			print(a.min.out+"%N")
			b.add ('b',7)
			print(a.is_equal_bag (b).out)
		end

end
