note
	description: "Summary description for {BAG2}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BAG2

create
	create_obj

feature
	elements:ARRAY[CELL_INFO]
	ind:INTEGER


feature

	add(val:CHARACTER; n:INTEGER)
	require
		Value_is_lower_letter: val.is_lower
		Number_is_greater_than_zero: n > 0
	local
		i_c:INTEGER
	do
		ind := val.difference ('a')
		elements.at (ind).value := val
		elements.item (ind).number_copies := elements.item (ind).number_copies + n
--	ensure
--		succesful_adding: (elements.item (ind).number_copies = (old elements.item (ind).number_copies)+n)
	end


	remove(val:CHARACTER; n:INTEGER)
	require
		Value_is_lower_letter: val.is_lower
		Number_is_greater_than_zero: n > 0
	do
		ind := val.difference ('a')
		elements.item (ind).value := val
		elements.item (ind).number_copies := (elements.item (ind).number_copies-n).max (0)
	ensure
--		succesful_removing: (elements.item (ind).number_copies = (old elements.item (ind).number_copies-n).max (0))
		copies_not_equal_zero: (elements.item (ind).number_copies >= 0)
	end

	min:CHARACTER
	local
		i_c:INTEGER

	do
		from
			i_c := 27
		until
			i_c < 0
		loop
			if elements.item(i_c).number_copies > 0
			then
				Result := elements.item (i_c).value
			end
			i_c := i_c-1
		end

	ensure
		result_is_alpha: Result.is_alpha
	end

	max:CHARACTER
	local
		i_c:INTEGER
	do
		from
			i_c := 0
		until
			i_c > 27
		loop

			if elements.item(i_c).number_copies > 0
			then
				Result := elements.item (i_c).value
			end
			i_c := i_c+1
		end
	ensure
		result_is_alpha: Result.is_alpha
	end



	is_equal_bag(b:BAG2):BOOLEAN
	local
		i_c:INTEGER
	do
		Result := true
		from
			i_c := 0
		until
			i_c > 27
		loop
			if elements.item(i_c).number_copies /= b.elements.item(i_c).number_copies
			then
				Result := false
			end
			i_c := i_c+1
		end
	end


feature
	create_obj
	local
		i_c:INTEGER
	do
		create elements.make_empty
		from
			i_c := 0
		until
			i_c > 27
		loop
			elements.force (create {CELL_INFO}.create_obj (' ',0), elements.count)
			i_c := i_c+1
		end
	end
end
