note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	create_obj

feature
	value:CHARACTER assign set_value
	set_value(v:CHARACTER)
	do
		value := v
	end
	number_copies:INTEGER assign set_number_copies
	set_number_copies(c:INTEGER)
	do
		number_copies := c
	end

feature
	create_obj(v:CHARACTER;num:INTEGER)
	do
		value := v
		number_copies := num
	end

end
