note
	description: "Matrices application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	v1:ARRAY[INTEGER]
	v2:ARRAY[INTEGER]
	m1:ARRAY2[INTEGER]
	m2:ARRAY2[INTEGER]
	vOP:VECTOR_OPER
	mOP:MATRIX_OPER
	m3:ARRAY2[INTEGER]
	i_c:INTEGER
	j_c:INTEGER

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create vOP
			create mOP
			create v1.make_filled (0, 0,2)
			create v2.make_filled (0, 0,2)

			v1[0]:= 0; v1[1]:= 1; v1[2]:= 0
			v2[0]:= 1; v2[1]:= 2; v2[2]:= 3

			print(vOP.dot_product (v1, v2).out+"%N")
			print(vOp.cross_product (v1, v2))

--			print({VECTOR_OPER}.dot_product(v1,v2).out)

--			create m1.make_filled (0, 3, 3)
--			create m2.make_filled (0, 3,3)


--			m1[1,1]:= 0; m1[1,2]:= 1; m1[1,3]:= 0
--			m1[2,1]:= 5; m1[2,2]:= 4; m1[2,3]:= 0
--			m1[3,1]:= 1; m1[3,2]:= 1; m1[3,3]:= 0

--			m2[1,1]:= 0; m2[1,2]:= 1; m2[1,3]:= 1
--			m2[2,1]:= 1; m2[2,2]:= 2; m2[2,3]:= 0
--			m2[3,1]:= 1; m2[3,2]:= 1; m2[3,3]:= 2


--			print(mOP.det (m1).out+"%N")
--			print(mOP.det (m2))

		create m1.make_filled (2,2, 6)
		create m2.make_filled (3, 3, 4)
		m3:= mOP.prod (m1, m2)
		from
			i_c := 1
		until
			i_c > m3.height
		loop
			from
				j_c:= 1
			until
				j_c > m3.width
			loop
				print(m3[i_c,j_c].out+" ")
				j_c := j_c+1
			end
			print("%N")
			i_c := i_c +1

		end

		end

end
