note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER

feature


feature
	add(m1:ARRAY2[INTEGER];m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		right_matrices: (m1.width = m2.width and m1.height = m2.height)
	local
		m:ARRAY2[INTEGER]
		j_c:INTEGER
		i_c:INTEGER
	do
		create m.make_filled (0, m1.height, m1.width)
		from
			i_c := 0
		until
			i_c > m1.height
		loop
			from
				j_c := 0
			until
				j_c > m1.width
			loop
				m.item (i_c,j_c) := m1.item (i_c,j_c) + m2.item (i_c,j_c)
				j_c := j_c+1
			end
			i_c := i_c+1
		end
		Result := m

		ensure
			right_final_matrix: (Result.width = m1.width and Result.height = m1.height)
	end

	minus(m1:ARRAY2[INTEGER];m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		right_matrices: (m1.width = m2.width and m1.height = m2.height)
	local
		m:ARRAY2[INTEGER]
		j_c:INTEGER
		i_c:INTEGER
	do
		create m.make_filled (0, m1.height, m1.width)
		from
			i_c := 0
		until
			i_c > m1.height
		loop
			from
				j_c := 0
			until
				j_c > m1.width
			loop
				m.item (i_c,j_c) := m1.item (i_c,j_c) - m2.item (i_c,j_c)
				j_c := j_c+1
			end
			i_c := i_c+1
		end

		Result := m

		ensure
			right_final_matrix: (Result.width = m1.width and Result.height = m1.height)

	end


	prod(m1:ARRAY2[INTEGER];m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		right_matrices: (m1.width = m2.height)
	local
		m:ARRAY2[INTEGER]
		j_c:INTEGER
		i_c:INTEGER
		c_c:INTEGER
		cnt:INTEGER
		sum:INTEGER
	do
		create m.make_filled (0, m1.height, m2.width)
		c_c := 0
		from
			i_c := 1
		until
			i_c > m1.height
		loop

			from
				j_c := 1
			until
				j_c > m2.width
			loop
				from
					c_c := 1
				until
					c_c > m2.height
				loop
					cnt  := cnt + (m1.item (i_c, c_c)*m2.item (c_c, j_c))
					c_c := c_c+1
				end
				m.item (i_c, j_c) := cnt
				cnt:= 0
				j_c := j_c+1
			end
			i_c := i_c+1
		end

		Result := m
		ensure
			right_final_matrix:	(Result.height = m1.height and Result.width = m2.width)
	end

	det(m:ARRAY2[INTEGER]):INTEGER
	require
		square_matrix: m.height = m.width
	do
		if m.height = 2
		then
			Result:= m[1,1]*m[2,2] - m[1,2]*m[2,1]
		elseif m.height = 3
		then
			Result:= m[1,1]*(m[2,2]*m[3,3]-m[2,3]*m[3,2])-m[1,2]*(m[2,1]*m[3,3]-m[2,3]*m[3,1])+m[1,3]*(m[2,1]*m[3,2]-m[2,2]*m[3,1])
		end
	end

end
