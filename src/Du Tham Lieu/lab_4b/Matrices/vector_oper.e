note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER

feature
	cross_product(v1,v2:ARRAY[INTEGER]):ARRAY[INTEGER]
	require
		right_vectors: v1.count = v2.count and v1.count = 3
	local
		v:ARRAY[INTEGER]
	do
		create v.make_from_array (v1)
		v[0] := v1[1]*v2[2]-v1[2]*v2[1]
		v[1] := v1[0]*v2[2]-v1[2]*v2[0]
		v[2] := v1[0]*v2[1]-v1[1]*v2[0]
		Result := v
	ensure
		right_output_size: Result.count = 3
	end

	dot_product(v1,v2:ARRAY[INTEGER]):INTEGER
	require
		right_vectors: v1.count = v2.count
	local
		i_c:INTEGER
		cnt: INTEGER
	do
		from
			i_c:= 0
		until
			i_c >= v1.count
		loop
			cnt := cnt + (v1[i_c]*v2[i_c])
			i_c := i_c+1
		end
		Result:= cnt

	end

	triangle_area(v1,v2:ARRAY[INTEGER]):INTEGER
	require
		right_vectors: v1.count = v2.count = 3
	local
		v:ARRAY[INTEGER]
		cnt:INTEGER
		i_c:INTEGER
	do
		v := cross_product(v1,v2)
		from
			i_c:= 0
		until
			i_c >= v.count
		loop
			cnt := cnt + v[i_c].power (2).floor
			i_c := i_c+1
		end
		cnt := cnt.power (0.5).floor
		Result:= cnt//2
	end

end
