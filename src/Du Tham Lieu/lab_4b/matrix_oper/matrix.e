class
	MATRIX

create
	make, make_filled, make_by_tuple

feature -- Creating
	make(rows, cols: INTEGER)
		-- Creates a matrix filled with 0's
		require
			valid_dimensions: are_valid_dimensions(rows, cols)
		do
			create matrix.make_filled(0, rows, cols);
		end

	make_filled(filler, rows, cols: INTEGER)
		-- Creates a matrix filled with a number(filler)
		require
			valid_dimensions: are_valid_dimensions(rows, cols)
		do
			create matrix.make_filled(filler, rows, cols);
		end

	make_by_tuple(tuple: TUPLE[INTEGER]; rows, cols: INTEGER)
		-- Creates a matrix by packing a tuple in it
		require
			valid_dimensions: are_valid_dimensions(rows, cols)
			valid_tuple: tuple.count = rows * cols
		do
			create matrix.make_filled(0, rows, cols);

			across 1 |..| tuple.count as i loop
				matrix[(i.item - 1) // cols + 1, (i.item - 1) \\ cols + 1] := tuple.integer_item (i.item)
			end
		end

feature -- Fields
	matrix: ARRAY2[INTEGER]

feature -- Commands
	set(item, i, j: INTEGER)
		-- Set the value of matrix[i, j]
		require
			valid_index: (1 <= i and i <= height) and (1 <= j and j <= width)
		do
			matrix[i, j] := item
		ensure
			valid_set: matrix[i, j] = item
		end

feature -- Querries
	height: INTEGER
		-- Returns number of rows
		do
			Result := matrix.height
		end

	width: INTEGER
		-- Returns number of columns
		do
			Result := matrix.width
		end

	get alias "[]"(i, j: INTEGER): INTEGER assign set
		-- Returns matrix[i, j]
		require
			valid_index: (1 <= i and i <= height) and (1 <= j and j <= width)
		do
			Result := matrix[i, j]
		end

	add alias "+"(m: MATRIX): MATRIX
		-- Adds 2 matrices
		require
			non_empty: is_non_empty and m.is_non_empty
			equal_size: have_equal_size(m)
		do
			create Result.make(height, width)

			across 1 |..| height as i loop
				across 1|..| width as j loop
					Result[i.item, j.item] := get(i.item, j.item) + m[i.item, j.item]
				end
			end
		end

	subtract alias "-"(m: MATRIX): MATRIX
		-- Subtract one matrix from another
		require
			non_empty: is_non_empty and m.is_non_empty
			equal_size: have_equal_size(m)
		do
			Result := current + m.multiply_by_number(-1)
		end

	multiply_by_number(n: INTEGER): MATRIX
		-- Multiplies a matrix by a given number
		do
			create Result.make(height, width)

			across 1 |..| height as i loop
				across 1|..| width as j loop
					Result[i.item, j.item] := get(i.item, j.item) * n
				end
			end
		end

	multiply_by_matrix alias "*" (m: MATRIX): MATRIX
		-- Multiplies one matrix by another
		require
			non_empty: is_non_empty and m.is_non_empty
			correct_dimensions: width = m.height
		local
			i, j, k, sum: INTEGER
		do
			create Result.make(height, m.width)
			from
				i := 1
			until
				i > height
			loop
				from
					j := 1
				until
					j > m.width
				loop
					from
						k := 1
						sum := 0
					until
						k > width
					loop
						sum := sum + get(i, k) * m[k, j]
						k := k + 1
					end
					Result[i, j] := sum
					j := j + 1
				end
				i := i + 1
			end
		ensure
			correct_dimensions: Result.height = height and Result.width = m.width
		end

	det: INTEGER
		-- Returns a demerminant of a matrix
		require
			non_empty: is_non_empty
			square_matrix: width = height
		local
			i, j, k: INTEGER
			temp: MATRIX
		do
			if width = 2 then
				Result := get(1, 1) * get(2, 2) - get(1, 2) * get(2, 1)
			else
				Result := 0
				from
					k := 1
				until
					k > width
				loop
					create temp.make(height - 1, width - 1)
					from
						i := 2
					until
						i > height
					loop
						from
							j := 1
						until
							j > width
						loop
							if j < k then
								temp[i - 1, j] := get(i, j)
							elseif j > k then
								temp[i - 1, j - 1] := get(i, j)
							end
							j := j + 1
						end
						i := i + 1
					end
					Result := Result + get(1, k) * factor(1, k) * temp.det
					k := k + 1
				end
			end
		end

	are_valid_dimensions(rows, cols: INTEGER): BOOLEAN
		-- Checks if dimensions are natural numbers
		do
			Result := rows >= 1 and cols >= 1
		end

	is_non_empty: BOOLEAN
		-- Checks if matrix is not empty
		do
			Result := height /= 0 and width /= 0
		end

	have_equal_size(m: MATRIX): BOOLEAN
		-- Checks if matrix has the same size as another one
		do
			Result := height = m.height and width = m.width
		end

	factor(row, col: INTEGER): INTEGER
		require
			natural_indeces: row >= 1 and col >= 1
		local
			i: INTEGER
		do
			Result := 1
			from
				i := 1
			until
				i > row + col
			loop
				Result := Result * (-1)
				i := i + 1
			end
		ensure
			correct_calculation: Result = 1 or Result = -1
		end

	to_string: STRING
		-- Returns a string representation of a matrix
		local
			i, j: INTEGER
		do
			Result := ""
			from
				i := 1
			until
				i > matrix.height
			loop
				from
					j := 1
				until
					j > matrix.width
				loop
					Result.append(matrix[i, j].out + " ")
					j := j + 1
				end
				Result.append("%N")
				i := i + 1
			end
		end
end
