class
	MATRIX_OPER

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			m1, m2, m3, m4: MATRIX
		do
			m1 := create_matrix([2, 2, 2, 2], 2, 2)
			m2 := create_matrix([3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3], 3, 4)

			print((m1 * m2).to_string)
--			m1 := create_matrix([1, 2, 3, 4, 5, 6, 7, 8], 2, 4)
--			m2 := create_matrix([9, 10, 11, 12, 13, 14, 15, 16], 2, 4)
--			m3 := create_matrix([1, 2, 3, 4, 5, 6, 7, 8], 4, 2)
--			m4 := create_matrix([1, 2, 3, 4, 5, 6, 7, 8, 9], 3, 3)

--			print(m1.to_string + "%N")
--			print(m2.to_string + "%N")

--			print("m1 + m2 = %N")
--			print((m1 + m2).to_string + "%N")

--			print("m1 - m2 = %N")
--			print((m1 - m2).to_string + "%N")

--			print("m1 * 2 = %N")
--			print(m1.multiply_by_number (2).to_string + "%N")

--			print(m3.to_string + "%N")
--			print("m2 * m3 = %N")
--			print((m2 * m3).to_string + "%N")

--			print(m4.to_string + "%N")
--			print("det(m4) = " + m4.det.out + "%N")
		end

	create_matrix(tuple: TUPLE[INTEGER]; rows, cols: INTEGER): MATRIX
		require
			correct_dimensions: rows >= 1 and cols >= 1
			correct_tuple: rows * cols = tuple.count
		do
			create Result.make_by_tuple(tuple, rows, cols)
		ensure
			correct_dimensions: Result.height = rows and Result.width = cols
		end

end
