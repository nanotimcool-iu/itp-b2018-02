note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	r1:RANGE
	r2:RANGE


feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create r1.create_obj (1,1)
			create r2.create_obj (2,5)
			print("linukasbiud nuoa ")
			print(r1.is_empty.out)

		end

end
