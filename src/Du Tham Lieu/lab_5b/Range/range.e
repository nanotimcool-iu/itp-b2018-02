note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	create_obj

feature
	left:INTEGER
	right:INTEGER


feature
	is_equal_range(other:RANGE):BOOLEAN
	do
		Result:= false
		if other.left = Current.left and other.right = Current.right
		then
			Result:= true
		end

	ensure
		right_value: other.left = Current.left and other.right = Current.right
	end


	is_empty:BOOLEAN
	do
		Result:= false
		if Current.right = Current.left
		then
			Result:= true
		end

	ensure
		right_value: Current.right = Current.left
	end

	is_sub_range_of(other:RANGE):BOOLEAN
	do
		Result:= false
		if other.right <= Current.right and other.left >= Current.left
		then
			Result := true
		end
	end

	is_super_range_of(other:RANGE):BOOLEAN
	do
		Result:= other.is_sub_range_of(Current)
	end

	left_overlaps(other:RANGE):BOOLEAN
	do
		Result:= false
		if Current.left <= other.left and (Current.right >= other.left and Current.right <= other.right)
		then
			Result:= true
		end
	end

	right_overlaps(other:RANGE):BOOLEAN
	do
		Result:= false
		if Current.right <= other.right and (Current.left >= other.right and Current.left <= other.left)
		then
			Result:= true
		end
	end

	overlaps(other:RANGE):BOOLEAN
	do
		Result:= true
		if Current.right < other.left or Current.left > other.right
		then
			Result:= false
		end

--		Result:= Current.right_overlaps(other) and Current.left_overlaps(other)
	end

	add(other:RANGE):RANGE
	local
		r:RANGE
	do
		create r.create_obj(Current.right.min(other.right),Current.left.max(other.left))
		Result:= r
	end

	substract(other:RANGE):RANGE
	local
		r:RANGE
	do
		create r.create_obj(Current.right.max(other.right),Current.left.min(other.left))
		Result:= r
	end




feature
	create_obj(l,r:INTEGER)
	require
		right_range: l <= r
	do
		right:= r
		left:= l
	end

end
