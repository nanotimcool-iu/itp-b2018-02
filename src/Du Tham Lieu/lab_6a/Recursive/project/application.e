note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	reverse_iter(s:STRING):STRING
	local
		i_c :INTEGER
		c:INTEGER
		su:STRING
	do
		su := " "
		su.copy (s)
		c:= 1
		from
			i_c := s.count
		until
			i_c <= 0
		loop

			su.item (c) := s.item (i_c)
			print(c.out+su.item (c).out+","+i_c.out+s.item (i_c).out+"%N")
			i_c := i_c-1
			c:= c+1
		end
		Result := su
	end

	merge_string(s:STRING)
	do
		s.mirror
		--we could use iterative way here instead of mirror
		--using tmp var

	end

	reverse_rec(s:STRING)
	do
		if s.count > 1
		then
			print(s.out+" ")
			reverse_rec(s.substring (1, s.count//2))
			reverse_rec(s.substring (s.count//2+1, s.count))
			merge_string(s)
--			print("----"+s.out)
--			print("---%N")
		else

			print(s.out+" lst ")
		end

	end

	reverse_rec2(s:STRING):STRING
	do
		if
			s.count <= 1
		then
			Result := s
		else
			Result := reverse_rec2(s.substring (2,s.count))
			Result.append_character (s.item (1))

		end

	end



	merge(a:ARRAY[INTEGER];l,r:INTEGER)
	local
		a1:ARRAY[INTEGER]
		a2:ARRAY[INTEGER]
		a3:ARRAY[INTEGER]
		p1:INTEGER
		p2:INTEGER
		c:INTEGER
		flag:BOOLEAN
	do
		create a1.make_from_array (a.subarray (l,l+(r-l)//2))
		create a2.make_from_array (a.subarray (l+(r-l)//2+1, r))

		print("%N###MERGE##%N")
		print(a1)
		print("%N")
		print(a2)
		print(a2[a2.lower])
		print(l.out+" , "+r.out)
		print("%N###MERGE##%N")

		--TODO:
		--use two pointers approach to compare and sort
		p1 := a1.lower
		p2 := a2.lower
		c := l

		from
		until
			p1 = a1.upper+1 and p2 = a2.upper+1
		loop

			print("LOOP"+p1.out+",p2: "+p2.out+"%N")
			if
				p1 < a1.upper+1 and p2 < a2.upper+1
			then

				if
					a1[p1] >= a2[p2]
				then

					a[c]:= a2[p2]
					p2 := p2 +1
				else
					a[c]:= a1[p1]
					p1 := p1 +1
				end

			elseif
				p1 >= a1.upper+1
			then
				a[c]:= a2[p2]
				p2 := p2 +1
			elseif
				p2 >= a2.upper+1
			then
				a[c]:= a1[p1]
				p1 := p1 +1

			end

			print("Finish Loop%N")
			c := c+1
		end

	end

	sort_merge_recursion(a:ARRAY[INTEGER];start:INTEGER;end_:INTEGER)
	do
		if
			end_ <= start
		then
			print("%N$$$$$$$$$$%N")
		else
			print(start.out+" , "+end_.out+"%N")
			print("-----------------%N")
			Io.read_character
			sort_merge_recursion(a,start,end_//2)
			sort_merge_recursion(a,start+(end_-start)//2+1,end_)
			merge(a,start,end_)
		end

	end



feature
	read_string:STRING
	rec_string:STRING
	result_string:STRING
	arr:ARRAY[INTEGER]
	x_c:INTEGER

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			--| Add your code here
			create rec_string.make_empty
			Io.readline
			read_string := io.last_string
--			read_string := reverse_iter(read_string)
--			print(read_string)


			--it is working
			--but I think this is not the right implementation as we destroyed the concept of Query or command only
--			print("%N")
--			reverse_rec(read_string)
--			print("-------------------")
--			print(read_string)


			result_string := reverse_rec2(read_string)
			print(result_string)




			create arr.make_empty

			from
				x_c := 4
			until
				x_c = 0
			loop
				arr.force (x_c,arr.count)
				x_c := x_c-1
			end
			print("arr Size:")
			print(arr.count.out+"%N")
			sort_merge_recursion(arr,0,arr.count-1)

			print("Finish")
			Io.read_character
			print(arr)
		end

end
