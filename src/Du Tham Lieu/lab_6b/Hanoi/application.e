note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	mp:ARRAY2[INTEGER]
	i_c:INTEGER
	disks_number:INTEGER
	source:INTEGER
	target:INTEGER
	other:INTEGER

feature
	print_tower(a:ARRAY2[INTEGER])
	local
		li_c:INTEGER
		lx_c:INTEGER
	do
		print("----------------------------%N")
		from
			li_c:= 1
		until
			li_c > a.height
		loop
			from
				lx_c:= 1
			until
				lx_c > a.width
			loop
				print(a[li_c,lx_c].out+"	")
				lx_c:= lx_c+1
			end
			print("%N")
			li_c := li_c+1
		end
		print("----------------------------%N")
	end

feature
	move_disk(a:ARRAY2[INTEGER];current_,target_:INTEGER)
	local
		li_c:INTEGER
		lstc:INTEGER
		lstt:INTEGER
	do
		lstc := a.height+1
		lstt := a.height+1
		from
			li_c := a.height
		until
			li_c <= 0
		loop
			if a[li_c, target_] /= 0
			then
				lstt := li_c
			end
			if a[li_c, current_] /= 0
			then
				lstc := li_c
			end

			li_c := li_c - 1
		end

		a[lstt-1, target_] := a[lstc, current_]
		a[lstc, current_] := 0
		print("###"+current_.out+" to "+target_.out+"%N")
		print_tower(a)

	end

feature
	hanoi_tower(a:ARRAY2[INTEGER];n:INTEGER;source_,target_,other_:INTEGER)
	do

--		recursive_hanoi way
		if
			n > 0
		then
			hanoi_tower(a,n-1,source_,other_,target_)
			move_disk(a,source_,other_)
			hanoi_tower(a,n-1,other_,target_,source_)
		end


	end



feature {NONE} -- Initialization

	make
			-- Run application.
		do
			print("Enter #disks: ")
			Io.read_integer
			disks_number := Io.last_integer
			print("%NEnter Source: ")
			Io.read_integer
			source:= Io.last_integer
			print("%NEnter Target: ")
			Io.read_integer
			target:= Io.last_integer
			print("%N")

			disks_number := 3
			source := 1
			target := 2
			create mp.make_filled (0, disks_number, 3)
			from
				i_c:= 1
			until
				i_c > disks_number
			loop
				mp[i_c , source] := i_c
				i_c := i_c+1
			end
			print_tower(mp)
			if
				(source = 1 and target = 2) or (source = 2 and target = 1)
			then
				other := 2
			else
				other := (6-(source+target))
			end

			hanoi_tower(mp,4,1,2,3)

--			
--			move_disk(mp,1,2)
--			print_tower(mp)
--			move_disk(mp,1,2)
--			print_tower(mp)
--			move_disk(mp,1,2)
--			print_tower(mp)
--			move_disk(mp,1,2)
--			print_tower(mp)
--			move_disk(mp,1,2)
--			print_tower(mp)
		end

end
