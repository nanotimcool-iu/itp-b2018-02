note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	HANGME

inherit
	EXECUTION_ENVIRONMENT

create
	make

feature {NONE}
	word: STRING
	is_guessed: ARRAY[BOOLEAN]
	max_len, guessed, has_guesses_left: INTEGER
	suckers: ARRAYED_LIST[STRING]
	dict: ARRAY [ARRAYED_LIST [STRING]]

feature -- Settings
	make
		local
			i, n: INTEGER
			tmp: ARRAYED_LIST [STRING]
		do
			from max_len	:= 40
			until n > 0 and n <= max_len
			loop
				io.put_string ("Enter correct number of suckers: %N")
				io.read_integer
				n := io.lastint
			end

			-- Deciding how long to play.
			from has_guesses_left := 0
			until
				has_guesses_left >= n and has_guesses_left <= 26
			loop
				print ("How many turns?: %N")
				io.readint
				has_guesses_left := io.lastint
			end

			-- Making room for players.
			create suckers.make (n)
			from
				i := 0
			until
				i =  n
			loop
				print("Enter #" + (i + 1).out + " sucker's name: %N")
				io.read_line
				suckers.extend (io.last_string.twin)
				i := i + 1
			variant
				n - 1
			end

			-- Aquiring player's name.
			create tmp.make (0)
			create dict.make_filled (tmp, 1, max_len)
			from
				i := 1
			until
				i > max_len
			loop
				create tmp.make (0)
				dict [i] := tmp
				i := i + 1
			end
			fill_dict
			word := choose_word (n)
			word.to_lower
			create is_guessed.make_filled (false, 0, 25)
end
feature
	    choose_word (n: INTEGER): STRING
		local
			t: TIME
			a_seed, i: INTEGER
			r: RANDOM
		do
			create t.make_now; a_seed := t.hour; a_seed := a_seed * 60 + t.minute; a_seed := a_seed * 60 + t.second; a_seed := a_seed * 1000 + t.milli_second
			create r.set_seed (a_seed)
			i := n + r.item \\ (max_len - n + 1)
			r.forth
			Result := dict [i].at (1 + r.item \\ dict [i].count)
			print ("len: " + i.out + " word #" + (1 + r.item \\ dict [i].count).out + " r: " + r.item.out + "%N")
			across
				Result as aids
			loop
				if not aids.item.is_alpha then
					guessed := guessed + 1
				end
			end
		end

	    fill_dict
		local
			src: PLAIN_TEXT_FILE
			w: STRING
		do
			create src.make_open_read ("rockyou.txt")
			from
			until
				src.exhausted
			loop
				src.readline
				w := src.laststring
				dict [w.count].extend (w.twin)
			end
			src.close
		end

		make_guess (char: CHARACTER): INTEGER
		local
			c: CHARACTER
			occurrences: INTEGER
		do
			c := char.lower
			if is_guessed [c.code - ('a').code] then -- Has been guessed
				Result := 2
			else
				occurrences := word.occurrences (c)
				if occurrences > 0 then -- Has been guessed or win
					Result := 0
					guessed := guessed + occurrences
					is_guessed [c.code - ('a').code] := True
					if guessed = word.count then
						Result := 3
					end
				else
					Result := 1
				end
			end
		end

		print_word
		local
			i: INTEGER
		do
			from
				i := 1
			until
				i > word.count
			loop
				if not word [i].is_alpha or else is_guessed.at (word [i].code - ('a').code) then
					print (word [i])
				else
					print ("_")
				end
				i := i + 1
			end
			print ("%N")
			end
feature{ANY}
		start_game
		local
			turn, guess_result: INTEGER
			win: BOOLEAN
			msg: STRING
		do
			msg := ""
			from
				turn := 0
			until
				has_guesses_left = 0 or win
			loop
--				system ("CLS")
				print (msg)
				print ("Now it is " + suckers [turn \\ suckers.count + 1] + "'s turn%N")
				print ("guesses left: " + has_guesses_left.out + "%N")
				print_word
				print ("Make a guess: ")
				io.read_character
				guess_result := make_guess (io.lastchar) -- 0 - guessed, 1 - wrong,
					--2 letter have alredy been guessed, 3 - player win
				io.read_character
				if guess_result = 0 then
					msg := "Correct!%N"
				elseif guess_result = 1 then
					msg := "There's no such letter. %N"
					has_guesses_left := has_guesses_left - 1
				elseif guess_result = 2 then
					msg := "%NTry again.%N"
					turn := turn - 1
				elseif guess_result = 3 then
					win := true
				end
				turn := turn + 1
			end
--			system ("CLS")
			if win then
				print ("Sucker number 442092 " + suckers [(turn - 1) \\ suckers.count + 1] + " has successfully guessed the last letter!")
			else
				print ("The word was:" + word + ". Better luck next time, fool!")
			end
		end
end
