note
	description: "Summary description for {PLAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAY

create
	make

feature {NONE} -- Initialization

	make
		local
			g: HANGME
		do
			create g.make
			g.start_game
		end

end
