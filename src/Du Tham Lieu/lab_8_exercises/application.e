note
	description: "bin_tree application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature -- Initialization

	make
		local
			n1, n2, n3: NODE[INTEGER]
		do
			create n1.create_node(42)
			create n2.create_node(72)
			create n3.create_node(108)
			n1.set_left(n2)
			n2.set_right(n3)
			print(n1.is_leaf)
			print(n1.height)

		end

end
