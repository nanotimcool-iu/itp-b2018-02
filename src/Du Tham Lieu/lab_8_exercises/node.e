note
	description: "Summary description for {NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NODE [G]

create
	create_node

feature

	left, right: detachable NODE [G]

	info: G

	height: INTEGER
		local
			left_height, right_height: INTEGER
		do
			result := 1 --in EIFFEl func not stop after returning the result.
			if attached left as l -- this is to void safe, EIFFEL is sure "l" is not void
			then
				left_height := l.height
			end
			if attached right as r then
				left_height := r.height
			end

				--			result:= result + left_height.max (right_height)
			if left_height > right_height then
				result := result + left_height
			else
				result := result + right_height
			end
		end

feature {APPLICATION} -- Initialization

	set_right (i: NODE [G])
		do
			right := i
		end

	set_left (i: NODE [G])
		do
			left := i
		end

	has_right: BOOLEAN
		do
			Result := right /= VOID
		end

	has_left: BOOLEAN
		do
			Result := left /= VOID
		end

	is_leaf: BOOLEAN
		do
			Result := not has_right and not has_left
		end

	create_node (a_value: G)
		do
			info := a_value
		end

end
