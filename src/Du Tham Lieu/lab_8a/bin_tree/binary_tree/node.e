note
	description: "Summary description for {NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NODE

create
	make

feature {NONE} -- Initialization

	node_name: STRING

	left: NODE

	right: NODE

	set_name (n: STRING)
		do
			node_name := n
		end

	set_left (i: NODE)
		do
			left := i
		end

	set_right (i: NODE)
		do
			right := i
		end

	make
		do
			create create_node.make (80)
		end

end
