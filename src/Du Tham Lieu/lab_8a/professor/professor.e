note
	description: "Summary description for {PROFESSOR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROFESSOR

create
	make

feature

	a_name, an_email: STRING

feature {NONE} -- Initialization

	make (name, email, password: STRING)
		do
			set_name (name)
			set_email (email)
			set_password (password)
		end

	set_name (name: STRING)
		do
			a_name:= name
		end

	set_email (email: STRING)
		do
			an_email := email
		end

feature {NONE}

	a_password: STRING

	set_password (password: STRING)
		do
			a_password := password
		end

end
