class
	CHAR_CELL
create
	make

feature
	make(number_a: INTEGER; char_a: CHARACTER)
	do
		number := number_a
		char := char_a
	end

feature
	number: INTEGER
	char: CHARACTER

feature
	set_number(new_number: INTEGER)
	require
		new_number >= 1
	do
		number := new_number
	ensure
		number = new_number
	end

	increase
	do
		number := number + 1
	end

invariant
	number >= 1
end
