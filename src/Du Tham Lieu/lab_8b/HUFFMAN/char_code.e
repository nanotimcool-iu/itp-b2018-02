class CHAR_CODE inherit
	ANY
		redefine
			out
		end
create
	make

feature
	make(char_a: CHARACTER; code_a: STRING)
	do
		char := char_a
		code := code_a
	end

feature
	char: CHARACTER
	code: STRING

feature
	out: STRING
	do
		Result := char.out + ": " + code
	end
end
