class
	MY_TREE

create
	make

feature
	make(value_a: CHAR_CELL)
	do
		value := value_a
	end

feature
	add_left(left_a: detachable MY_TREE)
	require
		left_a /= Void
	do
		left := left_a
	end

	add_right(right_a: detachable MY_TREE)
	require
		right_a /= Void
	do
		right := right_a
	end

feature
	is_leaf: BOOLEAN
	do
		Result := not (has_left or has_right)
	end

	has_left: BOOLEAN
	do
		Result := left /= Void
	end

	has_right: BOOLEAN
	do
		Result := right /= Void
	end

	form_haffman_code_list(start_code: STRING; list: LINKED_LIST[CHAR_CODE])
	do
		if is_leaf then
			list.extend (create {CHAR_CODE}.make (value.char, start_code))
		else
			if has_left then
				left.form_haffman_code_list(start_code + "0", list)
			end
			if has_right then
				right.form_haffman_code_list(start_code + "1", list)
			end
		end
	end

feature
	value: CHAR_CELL
	left: detachable MY_TREE assign add_left
	right: detachable MY_TREE assign add_right
end
