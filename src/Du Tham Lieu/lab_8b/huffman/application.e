note
	description : "Huffman_code application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE}

	make
	local
		code: LINKED_LIST[CHAR_CODE]
		str: STRING
	do
		print("Input the string: ")
		Io.read_line
		str := Io.last_string
		code := get_code(str)
		across code as j loop
			print(j.item.out + "%N")
		end
		print("Encoded: " + encode(str, code) + "%N")
		print("Decoded: " + decode(encode(str, code), code) + "%N")
	end
feature -- Initialization

	get_code(str: STRING): LINKED_LIST[CHAR_CODE]
		local
			encoded: ARRAY[CHAR_CELL]
			nodes: ARRAY[MY_TREE]
			lowest1, lowest2: INTEGER
			lowest_node1, lowest_node2: MY_TREE
			i: INTEGER
			temp_node: MY_TREE
			codes: LINKED_LIST[CHAR_CODE]
		do
			encoded := get_frequencies (str)
			--print_frequency_array (encoded)
			nodes := form_array_of_nodes (encoded)
			from
			until
				nodes.count <= 1
			loop
				lowest1 := 1
				lowest2 := 2
				from i := 3 until i > nodes.count  loop
					if nodes[i].value.number < nodes[lowest1].value.number then
						lowest2 := lowest1
						lowest1 := i
					elseif nodes[i].value.number <= nodes[lowest2].value.number then
						lowest2 := i
					end
					i := i + 1
				end

				lowest_node1 := nodes[lowest1]
				lowest_node2 := nodes[lowest2]

				create temp_node.make (create {CHAR_CELL}.make (lowest_node1.value.number + lowest_node2.value.number, '0'))

				temp_node.add_left (lowest_node1)
				temp_node.add_right (lowest_node2)
				nodes := form_array_without_two (nodes, lowest1, lowest2)
				nodes.force (temp_node, nodes.count + 1)
			variant
				nodes.count
			end

			temp_node := nodes[1]
			create codes.make

			temp_node.form_haffman_code_list ("", codes)
			Result := codes
		end

	form_array_without_two(array: ARRAY[MY_TREE]; one, two: INTEGER): ARRAY[MY_TREE]
	local
		i, j: INTEGER
	do
		create Result.make_filled(create {MY_TREE}	.make (create {CHAR_CELL}.make (1, '0')), 1, array.count - 2)
		from
			i := 1
			j := 1
		until
			i > array.count
		loop
			if i /= one and i /= two then
				Result[j] := array[i]
				j := j + 1
			end
			i := i + 1
		end
	ensure
		Result.count + 2 = array.count
	end

	get_frequencies(string: STRING): ARRAY[CHAR_CELL]
	local
		index: INTEGER
	do
		create Result.make_empty
		across string as i loop
			index := index_in_array(i.item, Result)
			if index = -1 then
				Result.force(create {CHAR_CELL}.make (1, i.item), Result.count + 1)
			else
				Result[index].increase
			end
		end
	end

	index_in_array(char: CHARACTER; array: ARRAY[CHAR_CELL]): INTEGER
	local
		i: INTEGER
	do
		Result := -1
		from i := 1 until i > array.count or Result /= -1 loop
			if array[i].char = char then
				Result := i
			end
			i := i + 1
		end
	end

	print_frequency_array(array: ARRAY[CHAR_CELL])
	do
		across array as i loop
			print(i.item.char.out + ": " + i.item.number.out + "%N")
		end
	end

	form_array_of_nodes(array: ARRAY[CHAR_CELL]): ARRAY[MY_TREE]
	local
		i: INTEGER
	do
		create Result.make_filled(create {MY_TREE}.make(create {CHAR_CELL}.make(1, '0')), 1, array.count)
		from i := 1 until i > array.count loop
			Result[i] := create {MY_TREE}.make(array[i])
			i := i + 1
		end
	end

	encode(str: STRING; code_list: LINKED_LIST[CHAR_CODE]): STRING
	do
		Result := ""
		across str as char loop
			across code_list as code loop
				if code.item.char = char.item then
					Result.append(code.item.code)
				end
			end
		end
	end

	decode(str: STRING; code_list: LINKED_LIST[CHAR_CODE]): STRING
	local
		part_of_code: STRING
		i: INTEGER
	do
		from
			i := 1
			part_of_code := ""
			Result := ""
		until
			i > str.count
		loop
			part_of_code.append (str[i].out)
			across code_list as code loop
				if code.item.code ~ part_of_code then
					Result.append(code.item.char.out)
					part_of_code := ""
				end
			end
			i := i + 1
		end

		across str as char loop
			across code_list as code loop
				if code.item.char = char.item then
					Result.append(code.item.code)
				end
			end
		end
	end
end
