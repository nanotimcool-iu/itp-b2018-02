class COMBINED_CIPHER
	inherit CIPHER

create
	make, make_empty

feature
	make(ciphers_a: ARRAY[CIPHER])
	do
		ciphers := ciphers_a
	end

	make_empty
	do
		ciphers := create {ARRAY[CIPHER]}.make_empty
	end

feature
	ciphers: ARRAY[CIPHER]

feature
	add_cipher(cipher: CIPHER)
	do
		ciphers.force(cipher, ciphers.count + 1)
	end

feature
	encrypt(text: STRING): STRING
	local
		i: INTEGER
	do
		 Result := text
		 from
		 	i := ciphers.lower
		 until
		 	i > ciphers.upper
		 loop
		 	Result := ciphers[i].encrypt (Result)
		 	i := i + 1
		 end
	end

	decrypt(text: STRING): STRING
	local
		i: INTEGER
	do
		 Result := text
		 from
		 	i := ciphers.upper
		 until
		 	i < ciphers.lower
		 loop
		 	Result := ciphers[i].decrypt (Result)
		 	i := i - 1
		 end
	end
end
