class
	MAIN

create
	make

feature {NONE}
	make
	local
		encrypted: STRING
		cipher: COMBINED_CIPHER
		cipher_v: VIGENERE_CIPHER
		cipher_s: SPIRAL_CIPHER
	do
		create cipher_v.make ("TIGER")
		print(cipher_v.encrypt ("STUDENTS, SOLVE THE ASSIGNMENT WELL AND FAST!") + "%N")

		create cipher_s
		print(cipher_s.encrypt ("LBAHVGBY, WFEDK XYX IYWZZVSIEM EKPC TVJ JRLB!") + "%N")

		create cipher.make_empty
		cipher.add_cipher (create {VIGENERE_CIPHER}.make ("TIGER"))
		cipher.add_cipher (create {SPIRAL_CIPHER})
		encrypted := cipher.encrypt ("STUDENTS, SOLVE THE ASSIGNMENT WELL AND FAST!")
		print(encrypted + "%N")

		create cipher.make_empty
		cipher.add_cipher (create {VIGENERE_CIPHER}.make ("BUSY"))
		cipher.add_cipher (create {SPIRAL_CIPHER})
		encrypted := cipher.encrypt ("MYLASTASSIGNMENT")
		print(cipher.decrypt (encrypted) + "%N")

		create cipher.make_empty
		cipher.add_cipher (create {VIGENERE_CIPHER}.make (encrypted))
		cipher.add_cipher (create {SPIRAL_CIPHER})
		print(cipher.decrypt ("LAGHYH QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF FAZ2/TGBZKFI REE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIUL ZIE KFYGEL//:RLH DXE Z"))
	end
end
