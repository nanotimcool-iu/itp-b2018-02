class SPIRAL_CIPHER
	inherit CIPHER

feature
	encrypt(text: STRING): STRING
	local
		size: INTEGER
		table: ARRAY2[CHARACTER]
		i, j: INTEGER
		left, right, top, bottom: INTEGER
	do
		Result := ""
		if not text.is_empty then
			size := find_closest_square_root (text.count)
			create table.make_filled (' ', size, size)
			from
				i := 1
			until
				i > text.count
			loop
				table[(i - 1) // size + 1, (i - 1) \\ size + 1] := text[i]
				i := i + 1
			end
			from
				left := 0
				right := 0
				top := 0
				bottom := 0
				i := 0
			until
				i = size * size
			loop
				if Result.count < size * size then
					from j := 1 + top until j > size - bottom loop
						Result.append(table[j, size - right].out)
						i := i + 1
						j := j + 1
					end
					right := right + 1
				end
				if Result.count < size * size then
					from j := size - right until j < left + 1 loop
						Result.append(table[size - bottom, j].out)
						i := i + 1
						j := j - 1
					end
					bottom := bottom + 1
				end
				if Result.count < size * size then
					from j := size - bottom until j < top + 1 loop
						Result.append(table[j, 1 + left].out)
						i := i + 1
						j := j - 1
					end
					left := left + 1
				end
				if Result.count < size * size then
					from j := 1 + left until j > size - right loop
						Result.append(table[1 + top, j].out)
						i := i + 1
						j := j + 1
					end
					top := top + 1
				end
			end
		end
	end

	decrypt(text: STRING): STRING
	require else
		size_is_square: find_closest_square_root (text.count) ^ 2 = text.count
	local
		i, j, size: INTEGER
		table: ARRAY2[CHARACTER]
		left, right, top, bottom: INTEGER
		string: STRING
	do
		Result := ""
		if not text.is_empty then
			size := find_closest_square_root (text.count)
			create table.make_filled (' ', size, size)
			from
				left := 0
				right := 0
				top := 0
				bottom := 0
				i := 1
				string := spaces(size * size - text.count) + text
			until
				i > size * size
			loop
				if i <= size * size then
					from j := 1 + top until j > size - bottom loop
						table[j, size - right] := string[i]
						j := j + 1
						i := i + 1
					end
					right := right + 1
				end
				if i <= size * size then
					from j := size - right until j < left + 1 loop
						table[size - bottom, j] := string[i]
						j := j - 1
						i := i + 1
					end
					bottom := bottom + 1
				end
				if i <= size * size then
					from j := size - bottom until j < top + 1 loop
						table[j, 1 + left] := string[i]
						j := j - 1
						i := i + 1
					end
					left := left + 1
				end
				if i <= size * size then
					from j := 1 + left until j > size - right loop
						table[1 + top, j] := string[i]
						j := j + 1
						i := i + 1
					end
					top := top + 1
				end
			end
			from
				i := 1
			until
				i > size
			loop
				from
					j := 1
				until
					j > size
				loop
					Result.append(table[i, j].out)
					j := j + 1
				end
				i := i + 1
			end
		end
	end

feature
	find_closest_square_root(number: INTEGER): INTEGER
	require
		number >= 0
	local
		integer_root: INTEGER
	do
		integer_root := (number ^ 0.5).truncated_to_integer
		if integer_root = number ^ 0.5 then
			Result := integer_root
		else
			Result := integer_root + 1
		end
	ensure
		number <= Result ^ 2
	end

	reverse_string(string: STRING): STRING
	do
		Result := ""
		across string as c loop
			Result := c.item.out + Result
		end
	end

	spaces(n: INTEGER): STRING
	require
		n >= 0
	do
		if n >= 1 then
			Result := " "
			Result.multiply(n)
		else
			Result := ""
		end
	end
end
