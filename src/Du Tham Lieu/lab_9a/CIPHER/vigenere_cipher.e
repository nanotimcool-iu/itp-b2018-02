class VIGENERE_CIPHER
	inherit CIPHER

create
	make

feature {NONE}
	key: STRING

feature
	make(key_a: STRING)
	do
		key := key_a
	end

feature
	encrypt(text: STRING): STRING
	local
		current_char, i, offset: INTEGER
		START_CHAR: CHARACTER
	do
		START_CHAR := 'A'
		Result := ""
		from
			i := 1
			current_char := 1
		until
			i > text.count
		loop
			if ('A' <= text[i]) and (text[i] <= 'Z') then
				offset := (text[i].code - START_CHAR.code + key[current_char].code - START_CHAR.code) \\ 26
				Result.append ((START_CHAR + offset).out)
				if current_char < key.count then
					current_char := current_char + 1
				else
					current_char := 1
				end
			else
				Result.append (text[i].out)
			end
			i := i + 1
		end
	end

	decrypt(text: STRING): STRING
	local
		current_char, i, offset: INTEGER
		START_CHAR: CHARACTER
	do
		START_CHAR := 'A'
		Result := ""
		from
			i := 1
			current_char := 1
		until
			i > text.count
		loop
			if ('A' <= text[i]) and (text[i] <= 'Z') then
				offset := (text[i].code - START_CHAR.code - key[current_char].code + START_CHAR.code) \\ 26
				if offset < 0 then
					offset := offset + 26
				end
				Result.append ((START_CHAR + offset).out)
				if current_char < key.count then
					current_char := current_char + 1
				else
					current_char := 1
				end
			else
				Result.append (text[i].out)
			end
			i := i + 1
		end
	end
end
