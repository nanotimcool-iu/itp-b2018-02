note
	description: "library application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			lib: LIBRARY
			card1, card2: CARD
			start_time, return_time1, return_time2: DATE
		do
			create card1.make ("P1", "AYE St.", "+1337", 12, 1)
			create card2.make ("P2", "AYE St.", "+1337", 18, 2)
			create lib.make
			create start_time.make (2018, 1, 1)
			create return_time1.make (2018, 1, 16)
			create return_time2.make (2019, 1, 1)
			lib.add_card (card1)
			lib.add_card (card2)
			lib.add_book (create {BOOK}.make ("A1", "B1", "D1", true, false, 1, 1000))
			lib.add_book (create {BOOK}.make ("A2", "B2", "D2", true, false, 2, 1000))
			lib.add_book (create {BOOK}.make ("A3", "B3", "D3", true, true, 3, 1000))
			lib.print_books
			lib.print_cards
			lib.check_out (1, 1, start_time)
			lib.check_out (1, 2, start_time)
			lib.check_out (1, 3, start_time)
			lib.return (1, return_time1)
			lib.return (2, return_time1)
			lib.return (3, return_time2)
		end

end
