class
	BOOK

inherit

	ANY
		redefine
			out
		end

create
	make

feature

	Days_common: INTEGER = 14

	Days_bestseller: INTEGER = 21

feature

	make (author_a, name_a, description_a: STRING; for_children_a, is_bestseller_a: BOOLEAN; id_a, price_a: INTEGER)
		require
			price_is_positive (price_a)
		do
			author := author_a
			name := name_a
			description := description_a
			for_children := for_children_a
			is_bestseller := is_bestseller_a
			id := id_a
			price := price_a
			start_time := create {DATE}.make_now
			end_time := create {DATE}.make_now
		end

feature {NONE}

	author: STRING

	name: STRING

	description: STRING

	start_time: DATE

	end_time: DATE

	price: INTEGER

feature

	price_is_positive (price_a: INTEGER): BOOLEAN
		do
			Result := price_a > 0
		end

	check_out (card: detachable CARD; start_time_a: DATE)
		require
			owner: was_taken_by = Void
		local
			days: INTEGER
		do
			was_taken_by := card
			start_time := start_time_a
			if is_bestseller then
				days := days_bestseller
			else
				days := days_common
			end
			end_time := create {DATE}.make (start_time.year, start_time.month, start_time.day)
			end_time.day_add (days)
		ensure
			valid_time: start_time < end_time
		end

	return (return_time: DATE; fine_per_day: INTEGER)
		require
			was_taken_by: was_taken_by /= Void
			valid_fine: fine_per_day >= 0
		local
			days: INTEGER
			fine: INTEGER
		do
			if start_time > return_time then
				print ("- Invalid return time%N")
			elseif attached was_taken_by as card then
				days := return_time.relative_duration (end_time).days_count
				fine := fine_per_day * days
				if fine > price then
					fine := price
				end
				if days >= 1 then
					print ("- Card ID " + card.id.out + " has to pay fine (" + (fine).out + " rubles) because of delay for " + days.out + " day(s)%N")
				end
				print ("- Book ID " + id.out + " has been returned successfully (by Card ID " + card.id.out + ")%N")
			end
		end

feature

	for_children, is_bestseller: BOOLEAN

	id: INTEGER

	was_taken_by: detachable CARD

	get_author: STRING
		do
			Result := create {STRING}.make_from_string (author)
		end

	get_name: STRING
		do
			Result := create {STRING}.make_from_string (name)
		end

	get_description: STRING
		do
			Result := create {STRING}.make_from_string (description)
		end

	out: STRING
		do
			Result := "(ID " + id.out + ") " + name + " by " + author + "%NDescription: " + description + "%N"
			if attached was_taken_by as card then
				Result.append ("Was taken by " + card.get_name + ".%N")
				Result.append ("From " + start_time.out + " to " + end_time.out + "%N")
			else
				Result.append ("Available.%N")
			end
			if for_children then
				Result.append ("#for_children%N")
			end
			if is_bestseller then
				Result.append ("#bestseller%N")
			end
		end

end
