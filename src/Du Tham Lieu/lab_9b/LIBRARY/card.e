class
	CARD

inherit

	ANY
		redefine
			out
		end

create
	make

feature

	make (name_a, address_a, phone_number_a: STRING; age_a, id_a: INTEGER)
		do
			name := name_a
			address := address_a
			phone_number := phone_number_a
			age := age_a
			id := id_a
		end

feature {NONE}

	name, address, phone_number: STRING

feature

	id, age: INTEGER

	get_name: STRING
		do
			Result := create {STRING}.make_from_string (name)
		end

	get_address: STRING
		do
			Result := create {STRING}.make_from_string (address)
		end

	get_phone_number: STRING
		do
			Result := create {STRING}.make_from_string (phone_number)
		end

	out: STRING
		do
			Result := "(ID " + id.out + ") " + name + "; " + address + "%N" + phone_number + "%N" + age.out + " y.o.%N"
		end

end
