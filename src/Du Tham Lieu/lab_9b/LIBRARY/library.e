class
	LIBRARY

create
	make

feature

	make
		do
			create books.make
			create cards.make
		end

feature {NONE}

	books: LINKED_LIST [BOOK]

	cards: LINKED_LIST [CARD]

feature

	fine_per_day: INTEGER = 100

feature

	add_book (book: BOOK)
		do
			if find_book_by_id (book.id) = Void then
				books.extend (book)
				print ("- Book ID " + book.id.out + " has been added to the library%N")
			else
				print ("! Failure. Book ID " + book.id.out + " already exists in the library%N")
			end
		end

	add_card (card: CARD)
		do
			if find_card_by_id (card.id) = Void then
				cards.extend (card)
				print ("- Card ID " + card.id.out + " has been added to the library%N")
			else
				print ("! Failure. Card ID " + card.id.out + " already exists in the library%N")
			end
		end

	print_books
		do
			print ("--- BOOKS ---%N")
			across
				books as b
			loop
				print (b.item.out)
				print ("-------------%N")
			end
		end

	print_cards
		do
			print ("--- CARDS ---%N")
			across
				cards as c
			loop
				print (c.item.out)
				print ("-------------%N")
			end
		end

	check_out (card_id, book_id: INTEGER; start_time: DATE)
		do
			if attached find_card_by_id (card_id) as card then
				if attached find_book_by_id (book_id) as book then
					if card.age <= 12 and not book.for_children then
						print ("! Failure. Book ID " + book_id.out + " contains 12+ content. Checking out has been failed.%N")
					elseif attached book.was_taken_by as owner then
						print ("- Book ID " + book_id.out + " is not available%N")
					else
						book.check_out (card, start_time)
						print ("- Book ID " + book_id.out + " has been checked out successfully (by Card ID" + card_id.out + ")%N")
					end
				else
					print ("! Failure. Invalid book ID " + book_id.out + "%N")
				end
			else
				print ("! Failure. Invalid card ID " + card_id.out + "%N")
			end
		end

	return (book_id: INTEGER; return_date: DATE)
		do
			if attached find_book_by_id (book_id) as book then
				if attached book.was_taken_by as card then
					book.return (return_date, fine_per_day)
				else
					print ("! Failure. Book ID " + book_id.out + " hasn't been checked out yet%N")
				end
			else
				print ("! Failure. Invalid book ID " + book_id.out + "%N")
			end
		end

	find_book_by_id (book_id: INTEGER): detachable BOOK
		do
			Result := Void
			across
				books as b
			loop
				if b.item.id = book_id then
					Result := b.item
				end
			end
		end

	find_card_by_id (card_id: INTEGER): detachable CARD
		do
			Result := Void
			across
				cards as c
			loop
				if c.item.id = card_id then
					Result := c.item
				end
			end
		end

end
