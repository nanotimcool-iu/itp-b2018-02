note
	description: "STACK application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature

	new_stack: MY_STACK [STRING]

	new_bounded_stack: MY_BOUNDED_STACK [INTEGER]

feature {NONE} -- Initialization

	make
			-- Run application.
		do
				--| Add your code here
			create new_stack.make
			create new_bounded_stack.make_limited (100)
				--print ("Hello Eiffel World!%N")
			print ("Is My stack empty?%N")
			print (new_stack.is_empty.out + "%N")
			new_stack.push ("That's racist!%N")
			print ("Is My stack still empty? " + new_stack.is_empty.out + " What does the Asian said? " + new_stack.item + "%N")
			new_stack.remove
			new_bounded_stack.push (42)
			print (new_bounded_stack.item.out)
		end

end
