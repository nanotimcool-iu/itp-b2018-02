note
	description: "Summary description for {MY_BOUNDED_STACK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_BOUNDED_STACK [G]

inherit

	MY_STACK [G]
		redefine
			push,
			make
		end

create
	make, make_limited

feature

	upper_bound: INTEGER

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			create My_stack.make (0)
			upper_bound := My_stack.capacity
		end

	make_limited (limit: INTEGER)
		require
			valid_limit: limit > 0
		do
			upper_bound := limit
			create My_stack.make (0)
		end

feature

	push (v: G) -- Push & Fill
		do
			if My_stack.count < (upper_bound - 1) then
				My_stack.extend (v)
			else
				print ("Nomas!")
			end
		end

end
