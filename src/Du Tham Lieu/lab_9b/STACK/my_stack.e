note
	description: "Summary description for {MY_STACK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_STACK [G]

create
	make

feature {NONE} -- Initialization

	My_stack: ARRAYED_LIST [G]

	make
			-- Initialization for `Current'.
		do
			create my_stack.make (0)
		end

feature

	push (v: G)
		do
			My_stack.extend (v)
		end

	remove
		do
			if not My_stack.is_empty then
				My_stack.remove_right
			else
				print ("Stack is now empty%N")
			end
		end

	item: G
		do
			result := My_stack.i_th (My_stack.count)
		end

	is_empty: BOOLEAN
		do
			Result := My_stack.is_empty
		end

end
