note
	description: "University application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	course1:COURSE
	make
		do
			create course1.create_course("ITP" , 3 , "EVERY MONDAY 12:10" , 130 , 131)
		end

end
