note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

create
	create_course

feature {NONE} -- Initialization
	name:STRING
	identifier:INTEGER
	schedule:STRING
	max_student:INTEGER
	num_of_students:INTEGER
	create_course(a_name:STRING ; a_identifier:INTEGER ; a_schedule:STRING ; a_max_student:INTEGER ; a_num_of_students:INTEGER)
		do
			if
				a_num_of_students < 3 or a_num_of_students > a_max_student
			then
				name := "NONE"
				identifier := 0
				schedule := "NONE"
				max_student := 0
				num_of_students := 0
				IO.put_string ("Not available , less than 3 students, or more than max number of students")
			else
				name := a_name
				identifier := a_identifier
				schedule := a_schedule
				max_student := a_max_student
				num_of_students := a_num_of_students
			end
		end
end
