note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	card1:BUSINESS_CARD
	make
		do
			create card1.make("AAA" , 130)
			card1.deposit (100)
			card1.withdraw (131)
		end
end
