note
	description: "Summary description for {BUSINESS_CARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BUSINESS_CARD
create
	make
feature
	name:STRING
	balance:INTEGER
	make(a_name:STRING ; a_balance:INTEGER)
		do
			name := a_name
			balance := a_balance
		end
feature
	deposit(a:INTEGER)
	do
		if (balance + a) <= 1000000
		then
			balance := balance + a
		else
			IO.put_string ("Too much money")
		end
	end
feature
	withdraw(a:INTEGER)
	do
		if (balance - a) >= 100
		then
			balance := balance - a
		else
			IO.put_string("HOMELESS!")
		end
	end
end
