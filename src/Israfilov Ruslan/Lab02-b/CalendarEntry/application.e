note
	description: "CalendarEntry application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	make
		do
			print ("Hello Eiffel World!%N")
		end

end
