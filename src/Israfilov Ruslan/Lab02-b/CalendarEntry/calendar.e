note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

create
	create_calendar

feature {NONE}
	create_calendar
		do
		end
feature
	create_entry(year:INTEGER ; month:INTEGER; day:INTEGER; hour:INTEGER; minute:INTEGER; second:INTEGER; dur_hours:INTEGER; dur_minutes:INTEGER; dur_seconds:INTEGER; a_owner:PERSON; a_subject:STRING; a_place:STRING):ENTRY
	local
		a:ENTRY
	do
		create a.create_entry(year , month , day , hour , minute, second , dur_hours, dur_minutes, dur_seconds, a_owner , a_subject , a_place)
		Result := a
	end

	edit_subject(e:ENTRY ; new_subject:STRING)
	do
		e.set_subject(new_subject)
	end

	edit_date(e:ENTRY ; new_date:DATE_TIME)
	do
		e.set_date(new_date)
	end

	get_owner_name(e:ENTRY):STRING
	do
		Result := e.get_owner.get_name
	end

	in_conflict(e1 , e2:ENTRY):BOOLEAN
	local
		a:INTEGER
	do
		if
			e1.get_owner /= e2.get_owner
		then
			Result := false
		end
		if
			e1.get_date.year = e2.get_date.year and e1.get_date.month = e2.get_date.month and e1.get_date.day = e2.get_date.day
		then
			if
				e1.get_date.seconds < e2.get_date.seconds
			then
				a := 1
			else
				a := 2
			end

			if
				a = 1
			then
				if
					e1.get_date.seconds + e1.get_duration.seconds_count > e2.get_date.seconds
				then
					Result := true
				else
					Result := false
				end
			else
				if
					e2.get_date.seconds + e2.get_duration.seconds_count > e1.get_date.seconds
				then
					Result := true
				else
					Result := false
				end
			end
		else
			Result := false
		end
	end
end
