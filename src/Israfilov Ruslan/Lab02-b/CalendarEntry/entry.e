note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	create_entry

feature {NONE} -- Initialization
	date:DATE_TIME
	long:DATE_TIME_DURATION
	owner:PERSON
	subject:STRING
	place:STRING
	create_entry(year:INTEGER ; month:INTEGER; day:INTEGER; hour:INTEGER; minute:INTEGER; second:INTEGER; dur_hours:INTEGER; dur_minutes:INTEGER; dur_seconds:INTEGER; a_owner:PERSON; a_subject:STRING; a_place:STRING)
		do
			create date.make(year , month, day, hour , minute , second)
			create long.make(0 , 0 , 0 , dur_hours , dur_minutes , dur_seconds)
			owner := a_owner
			subject := a_subject
			place := a_place
		end
feature
	set_subject(new:STRING)
	do
		subject := new
	end

	get_duration:DATE_TIME_DURATION
	do
		Result := long
	end

	get_date:DATE_TIME
	do
		Result := date
	end
	set_date(new:DATE_TIME)
	do
		date := new
	end

	get_owner:PERSON
	do
		Result := owner
	end
end
