note
	description: "ContactOfManagmentSystem application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	CMS1:CMS
	c1:CONTACT
	c2:CONTACT
	c3:CONTACT
	make
		do
			create CMS1.make
			c1 := CMS1.create_contact ("1", 1000000000 , "0" , "0")
			c2 := CMS1.create_contact("2" , 1000000001 , "0" , "0")
			c3 := CMS1.create_contact("3" , 1000000002 , "0" , "0")

			CMS1.add_emergency_contact (c1, c2)
			CMS1.add_emergency_contact (c2, c3)
			CMS1.edit_contact (c2, c2.get_name, 1000000003, c2.get_work_place, "00")
			CMS1.remove_emergency_contact (c2)
			CMS1.remove_emergency_contact (c3)
		end

end
