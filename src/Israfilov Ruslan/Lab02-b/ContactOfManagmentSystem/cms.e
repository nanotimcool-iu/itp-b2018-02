note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS
create
	make
feature
	make
		do
		end
feature -- operations with contacts
	create_contact(a_name:STRING ; a_phone_number:INTEGER; a_work_place:STRING; a_email:STRING):CONTACT
	local
		new_contact:CONTACT
	do
		create new_contact.create_contact(a_name, a_phone_number, a_work_place, a_email)
		Result := new_contact
	end

	edit_contact(c:CONTACT ; a_name:STRING ; a_phone_number:INTEGER; a_work_place:STRING; a_email:STRING)
	do
		c.set_name(a_name)
		c.set_phone_number(a_phone_number)
		c.set_work_place(a_work_place)
		c.set_email(a_email)
	end

	add_emergency_contact(c:CONTACT ; c1:CONTACT)
	do
		c.set_call_emergency (c1)
	end

	remove_emergency_contact(c:CONTACT)
	do
		c.set_call_emergency_as_void
	end
end
