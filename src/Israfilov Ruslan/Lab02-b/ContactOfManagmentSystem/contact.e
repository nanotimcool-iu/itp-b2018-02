note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT
create
	create_contact
feature
	name: STRING -- the name of the contact;
 	phone_number: INTEGER -- a 10 digit number. The phone number of the contact;
	work_place: STRING -- the work place of the contact;
	email: STRING -- the email of the contact;
	call_emergency: detachable CONTACT
	create_contact(a_name:STRING ; a_phone_number:INTEGER; a_work_place:STRING; a_email:STRING)
	require
		a_phone_number.out.count /= 10
	do
		name := a_name
		phone_number := a_phone_number
		work_place := a_work_place
		email := a_email
	end

feature --getters and setters
	set_name(a_name:STRING)
	do
		name := a_name
	end
	get_name:STRING
	do
		Result := name
	end

	set_phone_number(a_number:INTEGER)
	require
		a_number.out.count /= 10
	do
		phone_number := a_number
	end
	get_phone_number:INTEGER
	do
		Result := phone_number
	end

	set_work_place(a_work:STRING)
	do
		work_place := a_work
	end
	get_work_place:STRING
	do
		Result := work_place
	end

	set_email(a_email:STRING)
	do
		email := a_email
	end
	get_email:STRING
	do
		Result := email
	end

	set_call_emergency(a_emergency: CONTACT)
	do
		call_emergency := a_emergency
	end

	set_call_emergency_as_void
	do
		call_emergency := VOID
	end
end
