note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

create
	create_calendar

feature {NONE}
	days:ARRAYED_LIST[DAY]
	create_calendar
		do
			create days.make (31)
		end
feature
	add_entry(e:ENTRY ; day:INTEGER)
	require
		day <= 30
	do
		days[day].add_entry (e)
	end

	edit_subject(e:ENTRY ; new_subject:STRING)
	do
		e.set_subject(new_subject)
	end

	edit_date(e:ENTRY ; new_date:DATE_TIME)
	do
		e.set_date(new_date)
	end

	get_owner_name(e:ENTRY):STRING
	do
		Result := e.get_owner.get_name
	end

	in_conflict(day:INTEGER):ARRAYED_LIST[ENTRY]
	local
		arr:ARRAYED_LIST[ENTRY]
		set1:LINKED_SET[ENTRY]
		i , j :INTEGER
	do
		create arr.make (0)
		create set1.make
		if
			days[day].get_number_of_events < 2
		then
			Result := arr
		else
			from
				i := 0
			until
				i >= days.count
			loop
				from
					j := i
				until
					j >= days.count
				loop
					if
						i /= j
					then
						if
							in_conflict_by_entries(days[day].get_events[i] , days[day].get_events[j])
						then
							set1.put (days[day].get_events[i])
							set1.put (days[day].get_events[j])
						end
					end
				end
			end

			from
				i := 0
			until
				i >= set1.count
			loop
				arr.extend (set1[i])
			end
			Result := arr
		end


		Result := arr
	end

	in_conflict_by_entries(e1 , e2:ENTRY):BOOLEAN
	do
		if
			e1.get_owner /= e2.get_owner
		then
			Result := false
		end

		if
			e1.get_place.is_equal (e2.get_place)
		then
			Result := true
		end

		if
			e1.get_date.is_less (e2.get_date)
		then
			if
				e1.get_date.plus (e1.get_duration).is_less (e2.get_date)
			then
				Result := false
			else
				Result := true
			end
		else
			if
				e2.get_date.plus (e2.get_duration).is_less (e1.get_date)
			then
				Result := false
			else
				Result := true
			end
		end
	end

	printable_month:STRING
	local
		i , j:INTEGER
		temp:STRING
	do
		temp := ""
		from
			i := 0
		until
			i >= 31
		loop
			temp := temp + "DAY " + (i+1).out + ":%N"
			from
				j := 0
			until
				j >= days[i].get_number_of_events
			loop
				temp := temp + days[i].get_events[j].into_string (j + 1)
			end
		end
		Result := temp
	end
end
