note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY
create
	make
feature
	day:INTEGER
	events:ARRAYED_LIST[ENTRY]
  make(day_num:INTEGER)
	do
		day := day_num
		create events.make(0)
	end

feature
	add_entry(a:ENTRY)
	do
		events.extend(a)
	end

	get_number_of_events:INTEGER
	do
		Result := events.count
	end

	get_events:ARRAYED_LIST[ENTRY]
	do
		Result := events
	end
end
