note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	create_entry

feature {NONE} -- Initialization
	date:DATE_TIME
	long:DATE_TIME_DURATION
	owner:PERSON
	subject:STRING
	place:STRING
	create_entry(hour:INTEGER; minute:INTEGER; second:INTEGER; dur_hours:INTEGER; dur_minutes:INTEGER; dur_seconds:INTEGER; a_owner:PERSON; a_subject:STRING; a_place:STRING)
		do
			create date.make(0 , 0, 0, hour , minute , second)
			create long.make(0 , 0 , 0 , dur_hours , dur_minutes , dur_seconds)
			owner := a_owner
			subject := a_subject
			place := a_place
		end
feature
	set_subject(new:STRING)
	do
		subject := new
	end

	get_duration:DATE_TIME_DURATION
	do
		Result := long
	end

	get_date:DATE_TIME
	do
		Result := date
	end
	set_date(new:DATE_TIME)
	do
		date := new
	end

	get_owner:PERSON
	do
		Result := owner
	end

	get_place:STRING
	do
		Result := place
	end

	into_string(entry_num:INTEGER):STRING
	local
		temp:STRING
	do
		temp := ""
		temp := temp + "Entry: " + entry_num.out + "%NTime: " + date.hour.out  + ":" + date.minute.out + ":" + date.second.out + "%NDuration: " + long.hour.out + ":" + long.minute.out + ":" + long.second.out + "%NBelongs to: " + owner.get_name + "%Nsubject: " + subject + "%NPlace: " + place + "%N"
		Result := temp
	end
end
