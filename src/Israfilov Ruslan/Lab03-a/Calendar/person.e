note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	create_person

feature {NONE} -- Initialization
	name: STRING -- the name of the person;
	phone_number: INTEGER -- a 10 digit number. The phone number of the contact;
	work_place: STRING -- the work place of the contact;
	email: STRING -- the email of the contact.
	create_person(a_name: STRING ; a_phone_number: INTEGER; a_work_place: STRING ; a_email: STRING)
		do
			name := a_name
			phone_number := a_phone_number
			work_place := a_work_place
			email := a_email
		end
feature
	get_name:STRING
	do
		Result := name
	end
end
