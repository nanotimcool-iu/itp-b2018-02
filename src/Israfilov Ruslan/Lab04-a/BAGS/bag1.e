note
	description: "Summary description for {BAG1}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BAG1

create
	make_bag

feature {NONE}
	elements:ARRAY[CELL_INFO]
	amount:INTEGER
	make_bag
		do
			create elements.make_empty
			amount := 0
		end
feature
	get_elements:ARRAY[CELL_INFO]
	do
		Result := elements
	end

	amount_of_elements
	local
		i:INTEGER
		ct:INTEGER
	do
		ct := 0
		from
			i := 1
		until
			i > elements.count
		loop
			if
				elements.item (i).get_number_of_characters > 0
			then
				ct := ct + 1
			end
			i := i + 1
		end
		amount := ct
	end

	get_amount:INTEGER
	do
		Result := amount
	end

	add(val:CHARACTER ; n:INTEGER)
	require
		n_not_allowed: n >= 1
	local
		i:INTEGER
		a:CELL_INFO
	do
		create a.make_cell (val, n)
		if
			CURRENT.is_inside(val)
		then
			from
				i := 1
			until
				i > elements.count
			loop
				if
					elements.item (i).get_character = val
				then
					elements.item(i).add_number_characters (n)
				end
				i := i + 1
			end
		else
			elements.put (a, elements.count + 1)
		end
		amount_of_elements
	end

	is_inside(val:CHARACTER):BOOLEAN
	local
		i:INTEGER
	do
		from
			i := 1
		until
			i > elements.count
		loop
			if
				elements.item (i).get_character = val
			then
				Result := true
			end
			i := i + 1
		end
	end

	get_number_of_val(val:CHARACTER):INTEGER
	local
		i:INTEGER
	do
		from
			i := 1
		until
			i > elements.count
		loop
			if
				elements.item (i).get_character = val
			then
				Result := elements.item (i).get_number_of_characters
			end
			i := i + 1
		end
	end

	remove(val:CHARACTER ; n:INTEGER)
	require
		n_not_allowed: n >=1
	local
		i:INTEGER
	do
		from
			i := 1
			until
				i > elements.count
			loop
				if
					elements.item (i).get_character = val
				then
					if
						elements.item (i).get_number_of_characters >= n
					then
						elements.item (i).remove_number_characters(n)
					else
						elements.item (i).remove_number_characters(elements.item (i).get_number_of_characters)
					end
				end
			i := i + 1
			end
		amount_of_elements
	end

	min:CHARACTER
	local
		i:INTEGER
		c:CHARACTER
	do
		c := elements.item (1).get_character
		from
			i := 1
		until
			i > elements.count
		loop
			if
				elements.item (i).get_character < c
			then
				c := elements.item (i).get_character
			end
		i := i + 1
		end
		Result := c
	end

	max:CHARACTER
	local
		i:INTEGER
		c:CHARACTER
	do
		c := elements.item (1).get_character
		from
			i := 1
		until
			i > elements.count
		loop
			if
				elements.item (i).get_character > c
			then
				c := elements.item (i).get_character
			end
		i := i + 1
		end
		Result := c
	end

	is_equal_to(b:BAG1):BOOLEAN
	local
		ct , i:INTEGER
	do
		ct := 0
		if
			CURRENT.get_amount /= b.get_amount
		then
			Result := false
		else
			Result := true
			from
				i := 1
			until
				i > amount
			loop
				if
					elements.item (i).get_number_of_characters > 0
				then
					if
						not b.is_inside (elements.item (i).get_character)
					then
						Result := false
					else
						if
							b.get_number_of_val(elements.item (i).get_character) /= elements.item (i).get_number_of_characters
						then
							Result := false
						end
					end
					i := i + 1
				end
			end
		end
	end
end
