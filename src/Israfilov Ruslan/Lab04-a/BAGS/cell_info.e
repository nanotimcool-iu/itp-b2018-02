note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO
create
	make_cell
feature
	value:CHARACTER
	number_characters:INTEGER
 	make_cell(n:CHARACTER; n1:INTEGER)
	require
		not_allowed_n: n1 >= 1
	do
		value := n
		number_characters := n1
	end
feature
	add_number_characters(n:INTEGER)
	require
		not_allowed_n: n >= 1
	do
		number_characters := number_characters + n
	end

	remove_number_characters(n:INTEGER)
	require
		not_allowed_n: n <= number_characters
	do
		number_characters := number_characters - n
	end

	get_character:CHARACTER
	do
		Result := value
	end

	get_number_of_characters:INTEGER
	do
		Result:= number_characters
	end

invariant
	not_valid_number: number_characters >= 1
end
