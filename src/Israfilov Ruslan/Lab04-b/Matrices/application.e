note
	description: "Matrices application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	arr1 , arr2 , arr3:ARRAY2[INTEGER]
	mtrx:MATRIX_OPER
	make
			-- Run application.
		do
			create arr1.make_filled (2, 3, 3)
			create arr2.make_filled (3 , 3 , 3)
			create mtrx.make
			arr3 := mtrx.prod(arr1 , arr2)
			print(mtrx.det (arr3))
		end

end
