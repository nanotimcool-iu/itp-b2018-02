note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER
create
	make
feature{NONE}
	make
	do
	end
feature
	add(m1 , m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		sizes_of_matrices_are_not_equal: m1.height = m2.height and m1.width = m2.width
	local
		a:ARRAY2[INTEGER]
		i , j:INTEGER
	do
		create a.make_filled (0, m1.height, m1.width)
		from
			i:= 1
		until
			i > a.height
		loop
			from
				j:= 1
			until
				j > a.width
			loop
				a.item (i, j) := m1.item (i , j) + m2.item (i,j)
				j := j + 1
			end
			i := i + 1
		end
		Result := a
	end
	minus(m1 , m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		sizes_of_matrices_are_not_equal: m1.height = m2.height and m1.width = m2.width
	local
		a:ARRAY2[INTEGER]
		i , j:INTEGER
	do
		create a.make_filled (0, m1.height, m1.width)
		from
			i:= 1
		until
			i > a.height
		loop
			from
				j:= 1
			until
				j > a.width
			loop
				a.item (i, j) := m1.item (i , j) - m2.item (i,j)
				j := j + 1
			end
			i := i + 1
		end
		Result := a
	end

	prod(m1 , m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		width_of_m1_is_not_equal_to_height_m2: m1.width = m2.height
	local
		i , j , k , sum:INTEGER
		a:ARRAY2[INTEGER]
	do
		create a.make_filled (0, m1.height, m2.width)
		from
			i := 1
		until
			i > a.height
		loop
			from
				j := 1
			until
				j > a.width
			loop
				sum := 0
				from
					k := 1
				until
					k > m1.width
				loop
					sum  := sum + m1.item(i , k)*m2.item(k , j)
					k := k + 1
				end
				a.item (i , j) := sum
				j := j + 1
			end
			i := i + 1
		end
		Result := a
	end
	det(m:ARRAY2[INTEGER]):INTEGER
	require
		not_square_matrix: m.width = m.height
		size_bigger_than_3: m.width <= 3
	local
		arr1 , arr2 , arr3:ARRAY2[INTEGER]
	do
		create arr1.make_filled (0, m.height, m.width)
		create arr2.make_filled (0, m.height, m.width)
		create arr3.make_filled (0, m.height, m.width)
		if
			m.width = 1
		then
			Result := m.item(1 , 1)
		elseif
			m.width = 2
		then
			Result := det2(m)
		else
			arr1.item(1 , 1) := m.item(2 , 2); arr1.item(1 , 2) := m.item(2 , 3); arr1.item(2 , 1) := m.item(3 , 2); arr1.item(2 , 2) := m.item(3 , 3)
			arr2.item(1, 1) := m.item(2 , 1); arr2.item(1, 2) := m.item(2 , 3); arr2.item(2, 1) := m.item(3 , 1); arr2.item(2, 2) := m.item(3 , 3)
			arr3.item(1 , 1) := m.item(2 , 1); arr3.item(1 , 2) := m.item(2 , 2); arr3.item(2 , 1) := m.item(3 , 1); arr3.item(2 , 2) := m.item(3 , 2)
			Result := m.item(1 , 1)*det2(arr1) - m.item(1 , 2)*det2(arr2) + m.item(1 , 3)*det2(arr3)
		end
	end
	det2(m:ARRAY2[INTEGER]):INTEGER
	do
		Result := m.item(1 , 1)*m.item(2 , 2) - m.item(1 , 2)*m.item(2 , 1)
	end
end
