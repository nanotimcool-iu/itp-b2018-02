note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER
create
	make
feature{NONE}
	make
	do
	end
feature
	cross_prod(v1 , v2:ARRAY[INTEGER]):ARRAY[INTEGER]
	require
		size_of_vectors_is_not_3: v1.count = 3 and v2.count = 3
	local
		a:ARRAY[INTEGER]
	do
		create a.make_filled (0, 1, 3)
		a[1] := v1[2]*v2[3] - v1[3]*v2[2]
		a[2] := v1[3]*v2[1] - v1[1]*v2[3]
		a[3] := v1[1]*v2[2] - v1[2]*v2[1]
		Result := a
	end

	dot_prod(v1 , v2:ARRAY[INTEGER]):INTEGER
	require
		size_of_vectors_is_not_allowed: v1.count = v2.count and v1.count >1 and v1.count < 4
	do
		if
			v1.count = 2
		then
			Result := v1[1]*v2[1] + v1[2]*v2[2]
		else
			Result := v1[1]*v2[1] + v1[2]*v2[2] + v1[3]*v2[3]
		end
	end

	triangle_area(v1 , v2:ARRAY[INTEGER]):DOUBLE
	require
		size_of_vectors_is_not_3: v1.count = 3 and v2.count = 3
	local
		a:ARRAY[INTEGER]
	do
		a := cross_prod(v1 , v2)
		Result := ((a[1].power (2) + a[2].power (2) + a[3].power (2)).power(0.5))*0.5
	end
end
