note
	description: "LCS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	longest(str1, str2: STRING): STRING
		do
			if str1.count >= str2.count
			then
				result := str1.out
			else
				result := str2.out
			end
		end

	lcs(str1, str2: STRING; i, j: INTEGER): STRING
		do
			if i = 0 or j = 0
			then
				Result := ""
			elseif
				str1[i] = str2[j]
			then
				Result := lcs(str1, str2, i - 1, j - 1).out + str1[i].out
			else
				Result := longest(lcs(str1, str2, i - 1, j), lcs(str1, str2, i, j - 1)).out
			end
		end

	longest_common_subsequence(str1, str2: STRING): STRING
		do
			Result := lcs(str1, str2, str1.count, str2.count).out
		end

	make
		local
			a , b: STRING
		do
			a := "abcdefg"
			b := "b123e"
			print(longest_common_subsequence(a , b))
		end

end
