note
	description: "MergeSort application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE}
	b , c:ARRAY[INTEGER]
	make
		do
			create b.make_filled(1 , 1 , 10)
			b[3] := 5
			b[5] := 7
			c := merge_sort(b)
			print(c)
		end
feature
	merge_sort(a:ARRAY[INTEGER]):ARRAY[INTEGER]
	local
		left , right:ARRAY[INTEGER]
		mid:INTEGER
		i , j:INTEGER
	do
		if
			a.count = 1
		then
			Result := a
		else
			mid := a.count//2
			create left.make_filled (0, 1, mid)
			create right.make_filled (0, 1, a.count - mid)
			from
				i := 1
			until
				i > mid
			loop
				left[i] := a[i]
				i := i + 1
			end

			from
				j := mid + 1
				i := 1
			until
				j > a.count
			loop
				right[i] := a[j]
				i := i + 1
				j := j + 1
			end
			left := merge_sort(left)
			right := merge_sort(right)
			Result := merge(left , right)
		end
	end

	merge(left , right:ARRAY[INTEGER]):ARRAY[INTEGER]
	local
		l , r , ct:INTEGER
		res:ARRAY[INTEGER]
	do
		create res.make_filled (0, 1, left.count + right.count)
		l := 1; r := 1; ct := 1
		from
		until
			l > left.count or r > right.count
		loop
			if
				left[l] < right[r]
			then
				res[ct] := left[l]
				l := l + 1
				ct := ct + 1
			else
				res[ct] := right[r]
				r := r + 1
				ct := ct + 1
			end
		end

		from
		until
			not(l <= left.count)
		loop
			res[ct] := left[l]
			l := l + 1
			ct := ct + 1
		end

		from
		until
			not(r <= right.count)
		loop
			res[ct] := right[r]
			r := r + 1
			ct := ct + 1
		end
		Result := res
	end
end
