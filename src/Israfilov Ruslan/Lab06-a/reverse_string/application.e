﻿note
	description: "reverse_string application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	reverser:REVERSE
	s , s1:STRING
	make
		do
			s := "1234567890qwerty"
			create reverser
			print(s)
			print("%N")
			s1 := reverser.reverse_recursive (s)
			print(s1)
			print("%N")
			s1 := reverser.common_reverse (s1)
			print(s1)
		end

end
