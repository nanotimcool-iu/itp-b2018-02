note
	description: "Summary description for {REVERSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	REVERSE
feature
	reverse_recursive(a:STRING):STRING
	require
		string_not_allowed: a /= VOID and a.count /= 0
	local
		b:STRING
	do
		if
			a.count = 1
		then
			Result := a
		else
			b := a.out
			b.remove_tail (1)
			Result := a[a.count].out + reverse_recursive(b)
		end
	end

	common_reverse(a:STRING):STRING
	require
		string_not_allowed: a /= VOID and a.count /= 0
	local
		b:STRING
		i:INTEGER
	do
		b := a.out
		from
			i := 1
		until
			i > b.count
		loop
			b[i] := a[a.count + 1 - i]
			i := i + 1
		end
		Result := b
	end
end
