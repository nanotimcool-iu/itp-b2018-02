note
	description: "PowerSet application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE}
	make
		do
		end
feature
	power_set(a:ARRAY[INTEGER])
	require
		a /= VOID
	local
		i , j , n , w:INTEGER
	do
		n := power(2 , a.count)
		w := a.count
		from
			i := 1
		until
			i > n
		loop
			print("{")
			from
				j := 1
			until
				j > w
			loop
				if
					(i & (1 << j)) /= 0
				then
					print(a[j].out + " ")
				end
				j := j + 1
			end
			print("}")
			i := i + 1
		end
	end

	power(a , b:INTEGER):INTEGER
	local
		i:INTEGER
	do
		Result := 1
		from
			i := b
		until
			i = 0
		loop
			Result := Result*a
			i := i - 1
		end
	end
	
end
