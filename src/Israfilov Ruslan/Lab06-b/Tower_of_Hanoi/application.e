note
	description: "Tower_of_Hanoi application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE}
  make
    do
      ToH(5)
    end
feature
  Routine(n: INTEGER; source,other,target: STRING)
    do
      if n>0 then
        Routine(n-1,source,target,other)
        print("%NMove the disk from the rod " + source + " to the rod " + other)
        Routine(n-1,target,other,source)
      end
    end
  ToH(n: INTEGER)
    do
      Routine(n, "Source", "Other", "Target")
    end
end
