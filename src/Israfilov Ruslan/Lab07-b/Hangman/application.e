note
	description: "Hangman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE}
	number_players:INTEGER
	number_attempts:INTEGER
	dictionary:ARRAY[STRING]
	word1:ARRAY[CHARACTER]
	current_word:STRING
	max_number_of_players:INTEGER
	i:INTEGER
	check_n_pl:BOOLEAN
	char:CHARACTER
	number_diff_chars_in_sur_word:INTEGER
	isEnd:BOOLEAN
	player:INTEGER
	make
		do
			create dictionary.make_filled("asasasad" , 1 , 5) -- read from file algorithm

			max_number_of_players:= 0
			from i := 1 until i > dictionary.count loop
				if dictionary[i].count > max_number_of_players then
					max_number_of_players := dictionary[i].count
				end
				i := i + 1
			end
			check_n_pl:= FALSE
			from until check_n_pl loop
				print("Enter number of players:")
				IO.read_integer()
				number_players := IO.last_integer
				if number_players > max_number_of_players or number_players < 1 then
					print("Wrong number of players, please try again%N")
				else
					check_n_pl := TRUE
				end
			end

			current_word := dictionary[2].out --some random generator algorithm
			isEnd := FALSE
			player := 0
			create word1.make_filled('_' , 1 , current_word.count)
			from until isEnd loop
				IO.put_string ("Player " + (player \\ number_players + 1).out + " , your character:")
				IO.read_character
				char := IO.last_character
			end

		end

end
