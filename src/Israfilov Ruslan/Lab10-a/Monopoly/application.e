note
	description: "Monopoly application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE}
	check_pl:BOOLEAN
	number_of_players:INTEGER
	Board:ARRAY[SQUARE]
	Players:ARRAY[PLAYER]
	square_pat:SQUARE
	player_pat:PLAYER
	input_file:PLAIN_TEXT_FILE
	k ,s ,ct , j , i , p , r:INTEGER
	n:STRING
	dice1:RANDOM
	dice2:RANDOM
	rand:RANDOM
	dice1_state , dice2_state:INTEGER
	is_in_jail_now:BOOLEAN
	make
		do
			--read number of players(2-6)
			check_pl := FALSE
			from until check_pl loop
				IO.put_string ("Please, enter number of players (2-6 players): ")
				IO.read_integer
				number_of_players := IO.last_integer
				if number_of_players < 2 or number_of_players > 6 then
					IO.put_string ("Something went wrong, please, try again%N")
				else
					check_pl := TRUE
				end
			end

			--create empty board and patterns
			create square_pat.make(0 , 0 ,"")
			create player_pat.make
			n := ""
			p := 0
			r := 0
			create Board.make_empty

			--read data from file and create board
			create input_file.make_open_read("input.txt")
			from
				i := 1
			until
				i > 20
			loop
				input_file.read_line
				n := input_file.last_string.out
				input_file.read_line
				p := input_file.last_string.to_integer
				input_file.read_line
				r := input_file.last_string.to_integer
				create square_pat.make(p , r , n)
				Board.force (square_pat , i)
				i := i + 1
			end
			input_file.close
--			print(Board[20].get_name.out + " " + Board[20].get_price.out + " " + Board[20].get_rent.out)

			--set players
			create Players.make_empty
			from j := 1 until j > number_of_players loop
				print("Please, enter name of " + j.out + " player: ")
				IO.read_line
				create player_pat.make
				player_pat.set_name (IO.last_string)
				Players.force(player_pat , j)
				j := j + 1
			end
			print("----------------------------------%N")
--			print(Players[2].name + " " + Players[1].name)

			--GAME
			ct := 0
			create dice1.make
			create dice2.make
			create rand.make
			from k := 1 until k > 100 loop
				from s := 1 until s > number_of_players loop
					if Players[s].money > 0 then
						print(Players[s].name.out + "'s turn")
						IO.read_line
						if Players[s].is_in_jail then
							is_in_jail_now := TRUE
							if Players[s].jail_state < 3 then
								print(Players[s].name.out + ", sorry, but you're in jail, drop the dice.")
								IO.read_line
								dice1.forth
								dice2.set_seed (dice1.item)
								dice2.forth
								dice1_state := dice1.item\\6 + 1
								dice2_state := dice2.item\\6 + 1
								print("You have " + dice1_state.out + " and " + dice2_state.out)
								IO.read_line
								if dice1_state = dice2_state then
									print("Congratulations, you have double! You can escape from the jail.")
									IO.read_line
									Players[s].set_jail_state(0)
									Players[s].in_jail (FALSE)
									is_in_jail_now := FALSE
								else
									print("Sorry, but you can't leave jail.")
									IO.read_line
									Players[s].set_jail_state(Players[s].jail_state + 1)
								end
							else
								print("You are in jail during 3 attempts, you have to pay 50k and leave.")
								IO.read_line
								Players[s].set_money(Players[s].money - 50)
								Players[s].set_jail_state(0)
								Players[s].in_jail (FALSE)
								is_in_jail_now := FALSE
							end
						else
							is_in_jail_now := FALSE
						end
						if not is_in_jail_now then
							print("Your stats:%N")
							print("Name: " + Players[s].name.out + "%N")
							print("Money: " + Players[s].money.out + "k%N")
							print("Position: " + Board[Players[s].position].get_name.out + "%N")
							print("Squares:%N")
							if not Players[s].get_squares.is_empty then
								from i := 1 until i > Players[s].get_squares.count loop
									print(i.out + ") " + Players[s].get_squares[i].get_name + "%N")
									i := i + 1
								end
							else
								print("...")
							end
							IO.read_line
							print("Drop the dice:")
							IO.read_line
							dice1.forth
							dice2.set_seed (dice1.item)
							dice2.forth
							dice1_state := dice1.item\\6 + 1
							dice2_state := dice2.item\\6 + 1
							print("You have " + dice1_state.out + " and " + dice2_state.out)
							IO.read_line
							Players[s].set_position (Players[s].position + dice1_state + dice2_state)
							if Players[s].position > 20 then
								Players[s].set_position (Players[s].position\\20)
								print("You have passed one lap and get 200k rub!")
								Players[s].set_money (Players[s].money + 200)
								IO.read_line
							end
							if Players[s].position = 4 then
								print("Sorry, but you are in 'Taxes' square, you have to pay 10 procents of your money to bank.")
								IO.read_line
								Players[s].set_money (Players[s].money - Players[s].money//10)
							elseif Players[s].position = 6 or Players[s].position = 11 or Players[s].position = 1 then
								print("You are in '" + Board[Players[s].position].get_name.out + "' square, you can't do anything")
								IO.read_line
							elseif Players[s].position = 16 then
								print("Sorry, but you have to go to the jail")
								IO.read_line
								Players[s].set_position (6)
								Players[s].in_jail (TRUE)
							elseif Players[s].position = 9 or Players[s].position = 13 or Players[s].position = 19 then
								print("You are in 'CHANCE' square! You can get 10-200k or lose 10-300k.")
								IO.read_line
								print("And you will...")
								IO.read_line
								rand.set_seed(dice2.item)
								rand.forth
								if rand.item\\2 = 0 then
									rand.forth
									print("Get " + ((rand.item\\20 + 1)*10).out + "k rub! Congratulations!")
									Players[s].set_money (Players[s].money + (rand.item\\20 + 1)*10)
									IO.read_line
								elseif rand.item\\2 = 1 then
									rand.forth
									print("Lose " + ((rand.item\\30 + 1)*10).out + "k rub. Loser!")
									Players[s].set_money (Players[s].money - (rand.item\\30 + 1)*10)
									IO.read_line
								end
							else
								print("You are in '" + Board[Players[s].position].get_name.out + "' square.")
								IO.read_line
								if Board[Players[s].position].get_owner = Players[s] then
									print("You are the owner of this square.")
									IO.read_line
								else
									if Board[Players[s].position].is_owned and then Board[Players[s].position].get_owner.money > 0 then
										print("You are in " + Board[Players[s].position].get_owner.name.out + "'s suqare")
										IO.read_line
										print("You have to pay " + Board[Players[s].position].get_rent.out + "k rub to owner")
										IO.read_line
										Players[s].set_money (Players[s].money - Board[Players[s].position].get_rent)
										Board[Players[s].position].owner.set_money (Board[Players[s].position].owner.money + Board[Players[s].position].get_rent)
									elseif not Board[Players[s].position].is_owned or else Board[Players[s].position].get_owner.money < 0 then
										if Board[Players[s].position].get_owner.money < 0 then
											print("Owner of this square lost the game")
										else
											print("This square doesn't belong to anyone")
										end
										IO.read_line
										print("Price: " + Board[Players[s].position].get_price.out + "k rub")
										IO.read_line
										print("Do you wanna buy it? (if so print '1', otherwise print anything)")
										IO.read_integer
										from until IO.last_integer = 0 or IO.last_integer = 1 loop
											print("Something went wrong, please, try again")
											IO.read_integer
										end
										if IO.last_integer = 1 then
											Players[s].set_money (Players[s].money - Board[Players[s].position].get_price)
											Board[Players[s].position].set_owner (Players[s])
											Board[Players[s].position].set_is_owned(TRUE)
											Players[s].put_square(Board[Players[s].position])
											print("You successfully have bought this square")
										else
											print("Ok, you haven't bought this square")
										end
										IO.read_line
									end
								end
							end
						end
						print("----------------------------------")
						IO.read_line
						if Players[s].money < 0 then
						ct := ct + 1
						print("Sorry, but you have negative amount of money, you lose.")
						IO.read_line
						end
					end
					s := s + 1
				end
				if ct = number_of_players - 1 then
					from i := 1 until i > number_of_players loop
						if Players[i].money > 0 then
							print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%N")
							print("CONGRATULATIONS!%N" + Players[i].name.out + " win!")
							k := 101
						end
						i := i + 1
					end
				end
				k := k + 1
			end
			if ct /= number_of_players - 1 then
				print("Sorry, game has been too long, we decided to finish it%N")
				print("WINNERS:%N")
				from i := 1 until i > number_of_players loop
						if Players[i].money > 0 then
							print(Players[i].name.out + "%N")
						end
						i := i + 1
				end
			end
		end
end
