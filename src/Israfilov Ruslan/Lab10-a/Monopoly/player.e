note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER
create
	make
feature
	position:INTEGER
	money:INTEGER
	Squares: ARRAY[SQUARE]
	is_in_jail:BOOLEAN
	name:STRING
	jail_state:INTEGER
	make
	do
		position := 1
		money := 1500
		is_in_jail := FALSE
		create Squares.make_empty
		name := ""
		jail_state := 0
	end
feature
	set_name(k:STRING)
	do
		name := k.out
	end

	in_jail(k:BOOLEAN)
	do
		is_in_jail := k
	end

	set_position(k:INTEGER)
	do
		position := k
	end

	set_jail_state(k:INTEGER)
	do
		jail_state := k
	end

	set_money(k:INTEGER)
	do
		money := k
	end

	get_squares:ARRAY[SQUARE]
	do
		Result := Squares
	end

	put_square(k:SQUARE)
	do
		Squares.force (k, Squares.count + 1)
	end
end
