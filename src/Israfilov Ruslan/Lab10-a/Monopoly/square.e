note
	description: "Summary description for {SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SQUARE
create
	make
feature
	name:STRING
	price:INTEGER
	is_owned:BOOLEAN
	rent:INTEGER
	owner: PLAYER
	make(a_price , a_rent:INTEGER ; a_name:STRING)
	do
		price := a_price
		rent := a_rent
		name := a_name.out
		is_owned := FALSE
		create owner.make
	end
feature
	set_price(k:INTEGER)
	do
		price := k
	end

	set_rent(k:INTEGER)
	do
		rent := k
	end

	set_name(k:STRING)
	do
		name := k
	end

	set_owner(k:PLAYER)
	do
		owner := k
	end

	owned
	do
		is_owned := TRUE
	end

	set_is_owned(k:BOOLEAN)
	do
		is_owned := k
	end

	get_name:STRING
	do
		Result := name
	end

	get_price:INTEGER
	do
		Result := price
	end

	get_rent:INTEGER
	do
		Result := rent
	end

	get_owner:PLAYER
	do
		Result := owner
	end
end
