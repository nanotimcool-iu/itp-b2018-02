note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
		p : PRODUCT
		e : ESPRESSO
		c : CAPPUCCINO
		pirog : CAKE
		shop : COFFEE_SHOP
	do
		create shop.make
		shop.print_menu
		shop.profit
	end

end
