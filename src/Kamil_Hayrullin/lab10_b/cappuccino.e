note
	description: "Summary description for {CAPPUCCINO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAPPUCCINO
inherit COFFEE
rename set_price as make end
create
	make
feature
	description: STRING
		do
			Result := "A coffee with milk and milk foam"
		end
end
