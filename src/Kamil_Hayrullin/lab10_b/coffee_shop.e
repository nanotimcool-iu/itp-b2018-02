class
	COFFEE_SHOP
create
	make

feature
	menu: ARRAYED_LIST [PRODUCT]

	make
		local
			espresso, cappuccino: COFFEE
			cake: CAKE
		do
			create menu.make (0)
			create {ESPRESSO} espresso.make (10)
			create {CAPPUCCINO} cappuccino.make (15)
			create cake.set_price (5)
			espresso.set_price_public (50)
			cappuccino.set_price_public (75)
			cake.set_price_public (25)
			menu.extend (espresso)
			menu.extend (cappuccino)
			menu.extend (cake)
		end

		profit
		do
			print ("Profit:%N")
			print ("Espresso: " + menu [1].get_profit.out)
			print ("%NCappuccino: " + menu [2].get_profit.out)
			print ("%NCake: " + menu [3].get_profit.out)
		end

	print_menu
		do
			print ("Menu:%N")
			print ("Product: description, price/item%N")
			print ("Espresso: " + menu [1].description + "     " + menu [1].public_price.out)
			print ("%NCappuccino: " + menu [2].description + "     " + menu [2].public_price.out)
			print ("%NCake: " + menu [3].description + "     " + menu [3].public_price.out)
		end
end


