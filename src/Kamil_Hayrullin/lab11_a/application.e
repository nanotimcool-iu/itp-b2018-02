note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
	 	queue : EVENT_QUEUE
	 	i : INTEGER

	do
		create queue.make

		from i := 1 until i > 100000 loop
			queue.add (create {EVENT}.make (i.out, i))
			i := i + 1
		end

		print("size : " + queue.queue.count.out + "%N")

		queue.clear

		print("size : " + queue.queue.count.out + "%N")

		queue.add (create {EVENT}.make ("oldest", 1))
		queue.add (create {EVENT}.make ("middle", 2))
		queue.add (create {EVENT}.make ("newest", 3))
		print("extracted : + %N")
		print(queue.extract.get_info)
	end

end
