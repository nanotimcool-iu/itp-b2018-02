class
	EVENT	inherit COMPARABLE

create
	make

feature

	tag :STRING
	time : INTEGER

feature

	make(i_tag : STRING; i_time : INTEGER)
	require
		pre2: i_tag.is_empty = false
	do
		time := i_time
		tag := i_tag
	end

feature

	get_info : STRING
	do
		Result := "Tag : " + tag.out + "%N" + "Time : " + time.out + "%N"
	end

	is_less alias "<"(other : like Current) : BOOLEAN
	do
		Result := time > other.get_time
	end

	get_time : INTEGER
	do
		Result := time
	end

end
