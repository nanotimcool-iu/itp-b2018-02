class
	EVENT_QUEUE

create
	make

feature

	queue : LINKED_QUEUE[EVENT]

feature

	make
		do
			create queue.make
		end

	is_empty: BOOLEAN
	do
		Result := queue.is_empty
	end

	clear
	do
		queue.wipe_out
		print("%Ncleared.%N")
	end

	add(e:EVENT)
	do
		if(e.get_time >= 0) then
			queue.extend (e)
		end
	end

	extract : EVENT
	do
		Result := queue.item
	end
end
