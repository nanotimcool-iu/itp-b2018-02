note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			lab1 : LAB
				comp : COMPUTER_SCIENTIST
				bio : BIOLOGIST
				bi : BIO_INFORMATIC
		do
			create lab1.make
			create comp.make (1, "Vasya")
			create bio.make (2, "Misha")
			bio.name_pet ("Gena")
			create bi.make (3, "Ivan")
			bi.edit_bio ("Live in Moscow")
			lab1.add_member (comp)
			lab1.add_member (bio)
			lab1.add_member (bi)
			lab1.introduce_all
		end

end
