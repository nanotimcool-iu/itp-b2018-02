note
	description: "Summary description for {BIO_INFORMATIC}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIO_INFORMATIC
inherit
	SCIENTIST

redefine
	make,
	introduce
end

create

	make

feature

	short_biography : STRING

	make (i_id: INTEGER; i_name: STRING)
		do
			id := i_id
			name := i_name
			discipline := "Bio Informatic"
			short_biography := ""
 		end

 	edit_bio(bio : STRING)
 	do
 		short_biography := bio
 	end

 	introduce : STRING
 	do
 		Result := ("Hi! My name is " + name + ".%N id : " + id.out + "%N discipline : " + discipline.out + "%N" + " Bio: " + short_biography.out)
 	end
end

