note
	description: "Summary description for {BIOLOGIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIOLOGIST

inherit
	SCIENTIST

redefine
	make,
	introduce
end

create make

feature

	petty : PET

feature

	make(i_id : INTEGER; i_name : STRING;)
	do
		create petty.make ("", 1)
		id := i_id
		name := i_name
		discipline := "Biologist"
	end

	name_pet(i_name : STRING)
	do
		petty.give_name (i_name)
	end

	introduce : STRING
		do
			Result := ("Hi! My name is " + name + ".%N id : " + id.out + "%N discipline : " + discipline.out + petty.taunt)
		end
end
