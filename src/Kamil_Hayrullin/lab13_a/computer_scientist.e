note
	description: "Summary description for {COMPUTER_SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMPUTER_SCIENTIST

inherit
	SCIENTIST

redefine
	make
end

create

	make

feature

	make (i_id: INTEGER; i_name: STRING)
		do
			id := i_id
			name := i_name
			discipline := "Computer Scientist"
		end
end
