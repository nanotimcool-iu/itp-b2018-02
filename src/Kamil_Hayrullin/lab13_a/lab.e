note
	description: "Summary description for {LAB}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LAB

create
	make

feature
	members: ARRAYED_LIST [SCIENTIST]

	make
		do
			create members.make (0)
		end

	add_member (m: SCIENTIST)
		do
			members.extend (m)
		end

	remove_member (id: INTEGER)
		do
			across members as member loop
				if member.item.id = id then
					members.prune (member.item)
				end
			end
		end

	introduce_all
		do
			across members as member loop
				print(member.item.introduce.out)
				print ("%N%N")
			end
		end

end
