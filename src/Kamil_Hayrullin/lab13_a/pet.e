note
	description: "Summary description for {PET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PET

create

	make

feature

	name : STRING
	age : INTEGER

feature

	make(i_name : STRING; i_age : INTEGER)
	do
		name := i_name
		age := i_age
	end

	give_name(i_name : STRING)
	do
		name := i_name
	end

	taunt : STRING
	do
		Result := ("%N Pet:  %N  Hr fr fr fr meow awoooo" + "%N")
	end
end
