
deferred class

	SCIENTIST

feature

	id: INTEGER
	name :STRING
 	discipline: STRING

feature

	make (i_id: INTEGER; i_name: STRING)
		do
			id := i_id
			name := i_name
			discipline := ""
		end


	introduce : STRING
		do
			Result := ("Hi! My name is " + name + ".%N id : " + id.out + "%N discipline : " + discipline.out)
		end

end
