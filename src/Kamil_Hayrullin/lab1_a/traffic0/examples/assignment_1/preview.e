note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		do
			zurich.add_station ("Zoo", 500, 500)
			zurich.connect_station (1, "Zoo")
			zurich_map.update


		zurich_map.station_view (zurich.station ("zoo")).highlight
		wait(1)
		zurich_map.station_view (zurich.station ("zoo")).unhighlight
		wait(1)
		end

end
