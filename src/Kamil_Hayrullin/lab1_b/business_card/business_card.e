class
	BUSINESS_CARD

create
	fill_in

feature {NONE} -- Initialization

	fill_in
			-- Fill in the card and print it.
		do
			print("Your name: " + "%N")
			io.read_line
			set_name (io.last_string)

			print("Enter your job: " + "%N")
			io.read_line
			set_job (io.last_string)

			io.put_string ("Enter your age : " + "%N")
			io.read_integer
			set_age (io.last_integer)

			print_card
		end

feature

	name: STRING

	job: STRING

	age: INTEGER

feature -- Setting

	set_name (a_name: STRING)
			-- Set `name' to `a_name'.
		require
			name_exists: a_name /= Void
		do
			name := a_name.twin
		end

	set_job (a_job: STRING)
			-- Set `job' to `a_job'.
		require
			job_exists: a_job /= Void
		do
			job := a_job.twin
		end

	set_age (a_age: INTEGER)
			-- Set `age' to `a_age'.
		require
			age_non_negative: a_age >= 0
		do
			age := a_age
		end

feature -- Output

	print_card
			-- procedure used to print a business card using data stored in the object
		do
			print (line (Width))
            print ("%N|" + "Your name: " + name + spaces(Width - name.count - 13))
            print ("|%N|" + "Your job: " + job + spaces(Width - job.count - 12))
            print ("|%N|" + "Your age: " + age_info + spaces(Width - age_info.count - 12))
            print ("|%N" + line(Width) + "%N")
		end

	age_info: STRING
			-- Text representation of age on the card.
		do
			Result := age.out
		end

	Width: INTEGER = 40
			-- Width of the card (in characters), excluding borders.

	spaces (n: INTEGER): STRING
			-- Returns string of n spaces
		do
			Result := " "
			Result.multiply (n)
		end

	line (n: INTEGER): STRING
			-- Horizontal line on length `n'.
		do
			Result := "-"
			Result.multiply (n)
		end

end
