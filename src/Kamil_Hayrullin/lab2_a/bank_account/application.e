note
	description: "bank_account application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make
feature
	my_account : BANK_ACCOUNT

feature {NONE} -- Initialization

	make
		do
			create my_account.create_account("Ann", 10000)
			my_account.deposit (1000)
			print(my_account.balance.out)
		end

end
