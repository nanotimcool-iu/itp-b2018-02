note
	description: "Summary description for {BANK_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

create
	create_account

feature
	owner_name : STRING
	balance : INTEGER

feature

	create_account(i_name : STRING; i_balance : INTEGER)

	require
		balance_limit: i_balance >= 100 and i_balance <= 1000000

	do
		owner_name := i_name
		balance := i_balance
	end

feature

	deposit(amount : INTEGER)

	require
		positive : amount > 0
	do
		balance := balance + amount

	ensure
		balance <= 1000000
	end


	withdraw(amount : INTEGER)

	require
		positive : amount > 0

	do
		balance := balance - amount

		ensure

		balance >= 0
	end
end
