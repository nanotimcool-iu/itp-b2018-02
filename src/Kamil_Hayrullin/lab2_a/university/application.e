note
	description: "Universtiy application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
		    new_course : COURSE
		do
			create new_course.create_class("IUBS", 1, "Every Friday", 2)
		end

end
