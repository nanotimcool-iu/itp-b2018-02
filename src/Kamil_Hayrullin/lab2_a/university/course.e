class
	COURSE

create
	create_class

feature
	name : STRING
	id : INTEGER
	schedule : STRING
	maxStudents : INTEGER

feature
	-- Initialization

	create_class(a_name : STRING; a_id : INTEGER; a_schedule : STRING; a_maximum_of_students : INTEGER)

	do
		if(a_maximum_of_students < 3) then
			Io.put_string ("ERROR")
		    name := a_name
            id := a_id
	        schedule := a_schedule
	        maxStudents := a_maximum_of_students
		else
	        name := a_name
            id := a_id
	        schedule := a_schedule
	        maxStudents := a_maximum_of_students
	        Io.put_string ("OK")
	    end
	end
end
