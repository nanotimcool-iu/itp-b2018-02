note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature

	name: STRING

	phone_number: INTEGER

	work_place: STRING

	email: STRING

feature

	make(i_name : STRING; i_phone_number : INTEGER; i_work_place : STRING; i_email : STRING)
	
		do
			name := i_name
			phone_number := i_phone_number
			work_place := i_work_place
			email := i_email
		end
end
