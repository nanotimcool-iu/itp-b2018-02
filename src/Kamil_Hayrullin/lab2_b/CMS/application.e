note
	description: "Lab2_b_CMS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
	    local
	    	NULL : CONTACT
	        Mother : CONTACT
	        Father : CONTACT
	        --Son : CONTACT
	        --Daughter : CONTACT
	        --Grandma : CONTACT
	        --Grandpa : CONTACT
	        --Uncle : CONTACT

		do
			create NULL.create_contact (" ", 0, " ", " ")
			create Father.create_contact("Alex", 322131, "Medical center", "alex@mail.ru")
			create Mother.create_contact("Lara", 5553535, "office", "lara@mail.com")

			Father.add_emergency (Mother)
			Io.put_string (Father.call_emergency.name)
			Io.new_line

		    Io.put_string (Father.name)
		    Io.new_line


		    Io.put_integer (Father.phone_number)
		    Io.new_line


		    Io.put_string (Father.work_place)
		    Io.new_line


		    Io.put_string (Father.email)
		    Io.new_line

		    Father.edit_contact ("John", 2281337, "Zavod", "dadada@mail.com")

            Io.new_line
            Io.new_line

		    Io.put_string (Father.name)
		    Io.new_line


		    Io.put_integer (Father.phone_number)
		    Io.new_line


		    Io.put_string (Father.work_place)
		    Io.new_line


		    Io.put_string (Father.email)
		    Io.new_line

		    Io.new_line
            Io.new_line

            Father.delete_emergency (NULL)
            Io.put_string (Father.call_emergency.name)
			

		end

end
