note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

create

create_contact

feature

    name : STRING
    phone_number : INTEGER
    work_place : STRING
    email : STRING
    call_emergency : CONTACT


feature

    create_contact(i_name : STRING; i_phone_number : INTEGER; i_work_place : STRING; i_email : STRING)

        do
        	name := i_name
        	phone_number := i_phone_number
        	work_place := i_work_place
        	email := i_email
        	call_emergency := current
        end

     edit_contact(i_name : STRING; i_phone_number : INTEGER; i_work_place : STRING; i_email : STRING)

         do

         	name := i_name
        	phone_number := i_phone_number
        	work_place := i_work_place
        	email := i_email

         end

     add_emergency(c2 : CONTACT)

         do
			call_emergency := c2
         end

     delete_emergency(null_contact : CONTACT)

         do
         	call_emergency := null_contact
         	Io.put_string ("No emergancy")
         end
end
