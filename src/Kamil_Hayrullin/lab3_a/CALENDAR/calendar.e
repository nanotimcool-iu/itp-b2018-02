note
    description : "root class of the application"
    date        : "$Date$"
    revision    : "$Revision$"

class
    CALENDAR

inherit
    ARGUMENTS

create
    make

feature {NONE} -- Initialization

    make
        local
        	me : PERSON
        	birthday, weekend : ENTRY
        	time : DATE_TIME

        do
        	create time.make (2018, 01, 01, 12, 12, 12)
        	create me.make ("Kate",1234567890,"shop", "kate@mail.com")
        	create birthday.make (time, me,"Greetings! Birthday!")
        	print(birthday.subject)
        	Io.new_line
        	edit_subject(birthday, "bd!")
        	print(birthday.subject)
        	Io.new_line
        	print(get_owner_name(birthday))
        	create weekend.make (time, me, "Wohoo! Weekends!")
        	print(weekend.subject)
        	Io.new_line
        	print(if_conflict(birthday, weekend))
        	Io.new_line
        end


    edit_subject(e1: ENTRY; new_sub : STRING)

    do
        e1.new_subject(new_sub)
    end

    edit_date(e1: ENTRY; new_dat : DATE_TIME)

    do
        e1.new_date(new_dat)
    end

    get_owner_name(e1: ENTRY) : STRING

    do
        Result := e1.get_name
    end

    if_conflict(e1 : ENTRY; e2 : ENTRY) : BOOLEAN

    do
        Result := e1.owner = e2.owner and e1.date = e2.date
    end
end
