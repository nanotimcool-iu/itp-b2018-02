class
    ENTRY

create
    make

feature
    date : DATE_TIME
    owner : PERSON
    subject : STRING

feature

    make(i_date : DATE_TIME; i_owner : PERSON; i_subject : STRING)

    do
        date := i_date
        owner := i_owner
        subject := i_subject
    end

    new_subject(new_subject1 : STRING)

    do
        subject := new_subject1
    end

    new_date(new_date1 : DATE_TIME)

    do
        date := new_date1
    end

    get_name : STRING

    do
        Result := owner.name
    end


end
