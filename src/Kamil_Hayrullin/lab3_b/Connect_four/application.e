note
	description: "Connect_four application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature

	gaming_field : FIELD
	i : INTEGER

feature {NONE} -- Initialization

	make
	do
		create gaming_field.create_field
		gaming_field.print_field
		Io.new_line

		from
			i := 1
		until
			i >= 1000000
		loop
			if(i \\ 2 = 1) then
				print("Player #1, choose the column : ")
				Io.read_integer
				gaming_field.pull (1, Io.last_integer)
				gaming_field.update (1)
				gaming_field.print_field
				if(gaming_field.win = 1) then
					print("PLAYER 1 WON!!!")
					i := 1000000
				end
				i := i + 1
				end
			if(i \\ 2 = 0) then
				print("Player #2, choose the column : ")
				Io.read_integer
				gaming_field.pull (2, Io.last_integer)
				gaming_field.update (1)
				gaming_field.print_field
				if(gaming_field.win = 2) then
					i := 1000000
					print("PLAYER 2 WON!!!")
				end
				i := i + 1
				end
		end
	end
end
