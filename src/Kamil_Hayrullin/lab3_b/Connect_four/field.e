note
	description: "Summary description for {FIELD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	FIELD

create
	create_field

feature
	matrix : ARRAY2[INTEGER]
	i : INTEGER
	j : INTEGER
	count : INTEGER
	win : INTEGER
feature

	create_field

	do
		create matrix.make_filled (0, 6, 7)
	end

	update(current_player : INTEGER)
		-- check if where is winner
	do
		win := 0
		count := 0
		-- horizontalCheck

		from i := 1 until i > 6 loop
			from j := 1 until j > 7 loop
				if(matrix.item(i, j) = 1) then
					count := count + 1
					if(count = 4) then
						win := 1
					end
				else
					count := 0
				end
				j := j + 1
			end
			count := 0
			i := i + 1
		end

		from i := 1 until i > 6 loop
			from j := 1 until j > 7 loop
				if(matrix.item(i, j) = 2) then
					count := count + 1
					if(count = 4) then
						win := 2
					end
				else
					count := 0
				end
				j := j + 1
			end
			count := 0
			i := i + 1
		end

		-- vertical check

		from i := 1 until i > 7 loop
			from j := 1 until j > 6 loop
				if(matrix.item(j, i) = 1) then
					count := count + 1
					if(count = 4) then
						win := 1
					end
				else
					count := 0
				end
				j := j + 1
			end
			count := 0
			i := i + 1
		end

		from i := 1 until i > 7 loop
			from j := 1 until j > 6 loop
				if(matrix.item(j, i) = 2) then
					count := count + 1
					if(count = 4) then
						win := 2
					end
				else
					count := 0
				end
				j := j + 1
			end
			count := 0
			i := i + 1
		end

		-- diagonal check

		from i:= 1 until i > 4 loop
			from j := 1 until j > 3 loop
				if(matrix.item (j, i) = 1 and matrix.item (j + 1, i + 1) = 1 and matrix.item (j + 2, i + 2) = 1 and matrix.item (j + 3, i + 3) = 1) then
					win := 1
				end
				if (matrix.item (j, i) = 2 and matrix.item (j + 1, i + 1) = 2 and matrix.item (j + 2, i + 2) = 2 and matrix.item (j + 3, i + 3) = 2) then
					win := 2
				end
				j := j + 1
			end
			i := i + 1
		end

		from i:= 1 until i > 4 loop
			from j := 3 until j > 6 loop
				if(matrix.item (j, i) = 1 and matrix.item (j - 1, i + 1) = 1 and matrix.item (j - 2, i + 2) = 1 and matrix.item (j - 3, i + 3) = 1) then
					win := 1
				end
				if (matrix.item (j, i) = 2 and matrix.item (j - 1, i + 1) = 2 and matrix.item (j - 2, i + 2) = 2 and matrix.item (j - 3, i + 3) = 2) then
					win := 2
				end
				j := j + 1
			end
			i := i + 1
		end
	end


	pull(player : INTEGER; column : INTEGER)
		-- push to the column
	do
		from
			i := 6
		until
			i <= 0
		loop
			if ((matrix.item(i, column) = 1) or (matrix.item(i, column) = 2)) then
				i := i - 1
				if(i = 0) then
					Io.new_line
					print("No free space!!! Player#" + player.out + ", choose another column: ")
					Io.read_integer
					pull (player, Io.last_integer)
				end
			else
				matrix.force(player, i, column)
				i := 0
			end
		end
	end

	print_field
		-- print field
	do
		Io.new_line
		from i := 1 until i > 6 loop
			print("      ")
			from j := 1 until j > 7 loop
				print(matrix.item (i, j).out + " ")
				j := j + 1
			end
			i := i + 1
			Io.new_line
		end
		Io.new_line
	end
end
