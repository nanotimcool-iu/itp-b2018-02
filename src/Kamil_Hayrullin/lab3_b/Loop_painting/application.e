note
	description: "Loop_painting application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature

	count : INTEGER
	countMirror : INTEGER
	matrixSize : INTEGER
	checkpoint : INTEGER
	currentString : STRING
	save : INTEGER

	i : INTEGER
	j : INTEGER

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			print("enter : ")
			Io.read_integer
			print_loop(Io.last_integer)
		end

	print_loop(num : INTEGER)

	do
		count := 1
		checkpoint := 1
		currentString := ""

		from
			i := 1
		until
			i > num
		loop
			from
				j := 1
			until
				j > checkpoint
			loop
				count := count + 1
				j := j + 1
				if(i = num) then
					currentString := currentString + count.out + " "
				end
			end
			if(i \\ 2 = 0) then
				checkpoint := checkpoint + 1
			end
			i := i + 1
		end

		matrixSize := currentString.count * 2 + 1
		count := 1
		checkpoint := 1
		currentString := ""
		from
			i := 1
		until
			i > num
		loop
			if(i \\ 2 = 0 ) then
				print(" ")
			end

			save := count

			from
				j := 1
			until
				j > checkpoint
			loop
				print(count)
				print(" ")
				currentString := currentString + count.out + " "
				count := count + 1
				j := j + 1
			end

			countMirror := count

			from
				j := 0
			until
				j > matrixSize - 2 * (currentString.count)
			loop
				print(" ")
				j := j + 1
			end

			if(i \\ 2 = 1) then
				print("  ")
			end

			from
				j := countMirror - 1
			until
				j < save
			loop
				print(j.out)
				print(" ")
				j := j - 1
			end
			Io.new_line
			currentString := ""
			if(i \\ 2 = 0 ) then
				checkpoint := checkpoint + 1
			end
			i := i + 1
		end

	end
end
