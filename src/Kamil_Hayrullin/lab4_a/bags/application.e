note
	description: "Lab2_b_CMS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
		set : BAG_MULTISET
	do
		create set.create_set (10)
		set.add ('v', 3)
		set.add ('v', 50)
		set.add ('b', 1)
		set.add ('z', 1)
		set.add ('x', 1)
		set.add ('f', 3)

		print(set.elements[1].value.out + " " + set.elements[1].number_of_copies.out + "%N")

		print(set.min.out + "%N")
		print(set.max.out + "%N")
	end
end
