note
	description: "Summary description for {BAG_MULTISET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BAG_MULTISET

create
	create_set

feature
	elements : ARRAYED_LIST[CELL_INFO]

feature
	-- Constructor

	create_set(size : INTEGER)
	do
		create elements.make (size)
	end


	-- add n copies of val
	add(val : CHARACTER; n : INTEGER)

	require
		n_is_positive : n > 0

	local
		char_num : INTEGER
	do
		char_num := search(val)

		if(char_num = 0) then
			elements.extend (create {CELL_INFO}.new_cell(val, n))

		else
			elements[char_num].add_copies (n)
		end
	end

	-- remove n copies of val, if n >= number of copies, remove cell

	remove(val : CHARACTER; n : INTEGER)

	require
		n_positive : n > 0
	local
		char_num : INTEGER
	do
		char_num := search(val)

		if(char_num /= 0) then
			elements[char_num].delete_copies(n)

			if(elements[char_num].number_of_copies = 0) then
				elements.prune(elements[char_num])
			end
		end
	end

	max : CHARACTER
	do
		Result := max_element
	end

	min : CHARACTER
	do
		Result := min_element
	end

	is_equal_bag(another_bag : BAG_MULTISET) : BOOLEAN
	local
		i : INTEGER
	do
		Result := true
		across elements as l loop
			if (elements[i].value /= another_bag.elements[i].value or elements[i].number_of_copies /= another_bag.elements[i].number_of_copies) then
				Result := false
			end
			i := i + 1
		end
	end
feature
	-- Utilities

	search(val : CHARACTER) : INTEGER
	local
		num : INTEGER
		i : INTEGER
	do
		i := 1
		num := 0
		across elements as l loop
			if (elements[i].value = val) then
				num := i
			end
			i := i + 1
		end
		Result := num
	end

	max_element : CHARACTER
	local
		maximum : CHARACTER
		i : INTEGER
	do
		maximum := 'a'
		i := 1
		across elements as l loop
			if(elements[i].value > maximum) then
				maximum := elements[i].value
			end
			i := i + 1
		end
		Result := maximum
	end

	min_element : CHARACTER
	local
		minimum : CHARACTER
		i : INTEGER
	do
		minimum := 'z'
		i := 1
		across elements as l loop
			if(elements[i].value < minimum) then
				minimum := elements[i].value
			end
			i := i + 1
		end
		Result := minimum
	end
end
