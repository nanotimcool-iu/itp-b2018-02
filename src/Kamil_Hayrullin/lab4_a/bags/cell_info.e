note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	new_cell

feature
	value : CHARACTER
	number_of_copies : INTEGER

feature

	new_cell(input_value : CHARACTER; input_copies : INTEGER)

	do
		value := input_value
		number_of_copies := input_copies
	end

feature

	add_copies(n : INTEGER)

	require
		positive_n : n > 0
	do
		number_of_copies := number_of_copies + n
	end

	delete_copies(n : INTEGER)

	require
		positive_n : n > 0
	do
		number_of_copies := number_of_copies - n

		if(number_of_copies <= 0) then
			number_of_copies := 0
		end
	end
end
