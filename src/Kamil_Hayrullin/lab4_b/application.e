note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local	-- Run application.
			operations : MATRIX_OPER
			oper_vec : VECTOR_OPER
			matrice1, matrice2: ARRAY2[INTEGER]
			determ : INTEGER
			v1, v2 : ARRAY[INTEGER]
		do
			create oper_vec
			create operations
			create matrice1.make_filled (0, 4, 4)
			matrice1.item (1, 1) := 3
			matrice1.item (1, 2) := 5
			matrice1.item (1, 3) := 7
			matrice1.item (1, 4) := 6
			matrice1.item (2, 1) := 5
			matrice1.item (2, 2) := 11
			matrice1.item (2, 3) := 11
			matrice1.item (2, 4) := 12
			matrice1.item (3, 1) := 17
			matrice1.item (3, 2) := 14
			matrice1.item (3, 3) := 11
			matrice1.item (3, 4) := 19
			matrice1.item (4, 1) := 7
			matrice1.item (4, 2) := 14
			matrice1.item (4, 3) := 131
			matrice1.item (4, 4) := 19


			create matrice2.make_filled (0, 4, 4)
			matrice2.item (1, 1) := 33
			matrice2.item (1, 2) := 51
			matrice2.item (1, 3) := 7
			matrice2.item (1, 4) := 63
			matrice2.item (2, 1) := 5
			matrice2.item (2, 2) := 11
			matrice2.item (2, 3) := 12
			matrice2.item (2, 4) := 16
			matrice2.item (3, 1) := 27
			matrice2.item (3, 2) := 44
			matrice2.item (3, 3) := 19
			matrice2.item (3, 4) := 19
			matrice2.item (4, 1) := 7
			matrice2.item (4, 2) := 12
			matrice2.item (4, 3) := 11
			matrice2.item (4, 4) := 139

			print("matrice 1" + "%N")
			operations.print_matrix (matrice1)
			print("%N")

			print("matrice 2" + "%N")
			operations.print_matrix (matrice2)

			print("%N")
			print("matrice1 + matrice2 " + "%N")
			operations.print_matrix(operations.add (matrice1, matrice2))


			print("%N")
			print("matrice1 - matrice2 " + "%N")
			operations.print_matrix(operations.minus (matrice1, matrice2))

			print("%N")
			print("matrice1 * matrice2 " + "%N")
			operations.print_matrix(operations.product (matrice1, matrice2))

			print("%N")
			print("Determenant of 1: " + operations.det (matrice1).out + "%N")
			print("Determenant of 2: " + operations.det (matrice2).out + "%N")

			create v1.make_filled (0, 1, 3)
			v1.item (1) := 1
			v1.item (2) := 2
			v1.item (3) := 3

			create v2.make_filled (0, 1, 3)
			v2.item (1) := 7
			v2.item (2) := 2
			v2.item (3) := 9
			print("%N")
			print("v1 {" + v1.item (1).out + " " + v1.item (2).out + " " + v1.item (3).out + "} " + "v2 {" + v2.item (1).out + " " + v1.item (2).out + " " + v1.item (3).out + "}" + "%N")
			print("Cross prod of v1 and v2 :" + "%N")
			print(oper_vec.cross_product (v1, v2).item (1).out + " " + oper_vec.cross_product (v1, v2).item (2).out + " " + oper_vec.cross_product (v1, v2).item (3).out + "%N")
			print("Dot prod of v1 and v2 :" + "%N")
			print(oper_vec.dot_product (v1, v2))
			print("%N")
			print("Area of triangle between v1 and v2 :" + "%N")
			print(oper_vec.triangle_area (v1, v2))
		end



		-- ans := operations.product (matrice1, matrice2)

end
