note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER

feature
	-- product

	product(m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
	require
		matrices_height : m1.width = m2.height
	local
		output_matrice : ARRAY2[INTEGER]
		i, j, k : INTEGER
	do
		create output_matrice.make_filled (0, m1.height, m2.width)
		from i := 1 until i >  output_matrice.height loop
			from j := 1 until j > output_matrice.width loop
				from k := 1 until k > m1.width loop
					output_matrice.item (i, j) :=output_matrice.item (i, j) +  m1.item (i, k) * m2.item (k, j)
					k := k + 1
				end
				j := j + 1
			end
			i := i + 1
		end
		Result := output_matrice
	end


	add(m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
	require
		matrices_height : m1.height = m2.height
		matrices_width : m1.width = m2.width
	local
		output_matrice : ARRAY2[INTEGER]
		i, j : INTEGER
	do
		create output_matrice.make_filled (0, m1.width, m1.height)
		from i := 1 until i > m1.height loop
			from j:= 1 until j > m1.width loop
				output_matrice.item (i, j) := m1.item (i, j) + m2.item (i, j)
				j := j + 1
			end
			i := i + 1
		end
		Result := output_matrice
	end

	-- minus
	minus(m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
	require
		matrices_height : m1.height = m2.height
		matrices_width : m1.width = m2.width
	local
		output_matrice : ARRAY2[INTEGER]
		i, j : INTEGER
	do
		create output_matrice.make_filled (0, m1.width, m1.height)
		from i := 1 until i > m1.height loop
			from j:= 1 until j > m1.width loop
				output_matrice.item (i, j) := m1.item (i, j) - m2.item (i, j)
				j := j + 1
			end
			i := i + 1
		end
		Result := output_matrice
	end

	-- determenant

	det(m : ARRAY2[INTEGER]) : INTEGER
	require
		size : m.height = m.width
	local
		small_m : ARRAY2[INTEGER]
		i, k, j, l, b : INTEGER
		determ : INTEGER
	do
		create small_m.make_filled (0, m.height - 1 , m.width - 1)
		if(m.height = 2) then
			determ := m.item (1, 1) * m.item (2, 2) - m.item (2, 1) * m.item (1, 2)
		else
			from i := 1 until i > m.width loop
				if(i \\ 2 = 1) then
					k := 1
				else
					k := -1
				end

				from j := 1 until j > m.width loop
					from l := 2 until l > m.height loop
						if(j = i) then
							b := 1
						else
							small_m.item (l - 1, j - b) := m.item (l, j)
						end
						l := l + 1
					end
					j := j + 1
				end
				b := 0
				determ := determ + k * m.item (1, i) * (det(small_m))
				i := i + 1
			end
		end
		Result := determ
	end

	-- print

	print_matrix(matrix : ARRAY2[INTEGER])
	local
		i, j : INTEGER
	do
		from i := 1 until i > matrix.height loop
			from j:= 1 until j > matrix.width loop
				print(matrix.item (i, j).out + " ")
				j := j + 1
			end
			print("%N")
			i := i + 1
		end
	end
end
