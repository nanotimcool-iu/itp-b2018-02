note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER


feature

	dot_product(v1, v2 : ARRAY[INTEGER]) : INTEGER
	require
		size : v1.count = v2.count
	local
		answer, i : INTEGER
	do
		answer := 0
		from
			i := 1
		until
			i > v1.count
		loop
			answer := answer + v1.item (i) * v2.item (i)
			i := i + 1
		end
		Result := answer
	end

	cross_product(v1, v2 : ARRAY[INTEGER]) : ARRAY[INTEGER]
	require
		size : v1.count = v2.count
	local
		ans : ARRAY[INTEGER]
		matrix : ARRAY2[INTEGER]
		operations : MATRIX_OPER
	do
		create operations
		create ans.make_filled (0, 1, 3)
		create matrix.make_filled (0, 2, 2)
		matrix.item (1, 1) := v1.item (2)
		matrix.item (1, 2) := v1.item (3)
		matrix.item (2, 1) := v2.item (2)
		matrix.item (2, 2) := v2.item (3)

		ans.item (1) := operations.det(matrix)

		matrix.item (1, 1) := v1.item (1)
		matrix.item (1, 2) := v1.item (3)
		matrix.item (2, 1) := v2.item (1)
		matrix.item (2, 2) := v2.item (3)

		ans.item (2) := -1 * operations.det(matrix)

		matrix.item (1, 1) := v1.item (1)
		matrix.item (1, 2) := v1.item (2)
		matrix.item (2, 1) := v2.item (1)
		matrix.item (2, 2) := v2.item (2)

		ans.item (3) := operations.det(matrix)

		Result := ans
	end


	triangle_area(v1, v2 : ARRAY[INTEGER]) : REAL
	require
		size : v1.count = v2.count
	local
		v3 : ARRAY[INTEGER]
		operations : VECTOR_OPER
		math: SINGLE_MATH
	do
		create math
		create operations
		create v3.make_filled (0, 1, 3)
		v3 := operations.cross_product (v1, v2)
		Result := math.sqrt (v3.item (1) * v3.item (1) + v3.item (2) * v3.item (2) + v3.item (3) * v3.item (3))
	end
end
