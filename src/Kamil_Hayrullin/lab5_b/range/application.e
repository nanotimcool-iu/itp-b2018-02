note
	description: "range application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			first, second : RANGE
		do
			create first.make (1, 10)
			create second.make (-5, 8)
			print("First : "  + first.printl)
			print("Second : " + second.printl)
			print("Is first empty?: ")
			print(first.is_empty)
			print("%N")
			print("Is second empty?: ")
			print(second.is_empty)

		    print("%N" + "Is first == second? :")
			print(first.is_equal_range (second))
			print("%N")
			second.set_left (1)
			second.set_right (10)
			print("Second = " + second.printl)
			print("Is first == second? :")
			print(first.is_equal_range (second))

			print("%N")
			second.set_left (-5)
			second.set_right (11)
			print("Second = " + second.printl)
			print("Is first a sub range of second? : ")
			print(first.is_sub_range_of (second))

			print("%N")
			second.set_left (-5)
			second.set_right (8)
			print("Second = " + second.printl)
			print("Is first a sub range of second? : ")
			print(first.is_sub_range_of (second))

			print("%N")
			print("Is first a super range of second? :")
			print(first.is_super_range_of (second))
			first.set_left (-6)
			print("%N" + "First = " + first.printl)
			print("Is first a super range of second? :")
			print(first.is_super_range_of (second))

			print("%N")
			print("___________________")
			print("%N")

			create first.make (1, 10)
			create second.make (-5, 8)
			print("First : "  + first.printl)
			print("Second : " + second.printl)

			print("%N")
			print("Is first a left overlap of second? :")
			print(first.left_overlaps (second))

			print("%N")
			print("Is first a right overlap of second? :")
			print(first.right_overlaps (second))

			print("%N")
			print("Is first a overlap of second? :")
			print(first.overlaps (second))

			print("%N")
			print("First + second = ")
			print(first.add (second).printl)

			print("%N")
			print("First - second = ")
			print(first.subtract(second).printl)

		end

end
