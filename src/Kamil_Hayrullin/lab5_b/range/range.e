note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE
create
	make

feature
	left : INTEGER
	right : INTEGER

feature

	make(i_left : INTEGER; i_right : INTEGER)
	do
		left := i_left
		right := i_right
	end

	set_left(i_left : INTEGER)
	do
		left := i_left
	end


	set_right(i_right : INTEGER)
	do
		right := i_right
	end

	is_equal_range(other : like current) : BOOLEAN
	do
		Result := current ~ other
	end

	is_empty : BOOLEAN
	do
		Result := right < left
	end

	is_sub_range_of(other : like current) : BOOLEAN
	do
		Result := (other.left < left and other.right > right) or current.is_empty
	end

	is_super_range_of(other : like current) : BOOLEAN
	do
		Result := (other.left > left and other.right < right) or other.is_empty
	end

	left_overlaps(other : like current) : BOOLEAN
	do
		Result := (left >= other.left and left <= other.right)
	end

	right_overlaps(other : like current) : BOOLEAN
	do
		Result := (right >= other.left and right <= other.right)
	end

	overlaps(other : like current) : BOOLEAN
	require
		null : other.is_empty = false and current.is_empty = false
	do
		Result := (right <= other.right or left >= other.left)
	end

	add(other : like current) : RANGE
	require
		overlaps : current.overlaps (other)
	local
		output : RANGE
	do
		create output.make (0, -1)
		if(current.is_empty) then
			Result := other
		elseif(other.is_empty) then
			Result := current
		else
			if(current.left_overlaps (other)) then
				output.set_left (other.left)
			else
				output.set_left (left)
			end

			if(current.right_overlaps (other)) then
				output.set_right (other.right)
			else
				output.set_right (right)
			end

			Result := output
		end
	end

	subtract(other : like current) : RANGE
	require
		overlaps : current.overlaps (other)
	local
		output : RANGE
	do
		create output.make (0, -1)
		if(current.is_empty and other.is_empty) then
			Result := output
		elseif(current.is_empty) then
			Result := current
		elseif(other.is_empty) then
			Result := current
		else
			if(current.left_overlaps (other)) then
				output.set_left (left)
			else
				output.set_left (other.left)
			end

			if(current.right_overlaps (other)) then
				output.set_right (right)
			else
				output.set_right (other.right)
			end

			Result := output
		end
	end

	printl : STRING
	do
		 Result := "[" + left.out + "," + right.out + "]" + "%N"
	end

end
