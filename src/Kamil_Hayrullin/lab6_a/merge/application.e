note
	description: "merge_sort application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make
feature
	array : ARRAYED_LIST[INTEGER]
	num : INTEGER

feature {NONE}

	make
		local
			i: INTEGER
		do
			create array.make(0)
			print("Enter length :")
			Io.read_integer
			num := Io.last_integer
			from
				i := 1
			until
				i > num
			loop
				print("%N" + "Enter array[" + i.out + "] : ")
				Io.read_integer
				array.extend(Io.last_integer)
				i := i + 1
			end

			array := merge_sort(array)
			printArr
		end
feature
		printArr
		local
			i : INTEGER
		do
			from i := 1 until i > num loop
				print(array[i].out + " ")
				i := i + 1
			end
		end

		merge_sort(arr: ARRAYED_LIST[INTEGER]): ARRAYED_LIST[INTEGER]
		local
			left, ans, right: ARRAYED_LIST[INTEGER]
			i, j: INTEGER
		do
			create left.make (0);
			create right.make (0);
			create ans.make (0);
			if arr.count = 1 then
				Result := arr
			else
				from
					i := 1
				until
					i > arr.count // 2
				loop
					left.extend (arr[i])
					i := i + 1
				end

				left := merge_sort(left)
				from
					i := arr.count // 2 + 1
				until
					i > arr.count
				loop
					right.extend (arr[i])
					i := i + 1
				end

				right := merge_sort(right)
				from
					i := 1; j := 1
				until
					i > left.count and j > right.count
				loop
					if i > left.count then
						ans.extend (right[j])
						j := j + 1
					elseif j > right.count then
						ans.extend (left[i])
						i := i + 1
					else
						if left[i] < right[j] then
							ans.extend (left[i])
							i := i + 1
						else
							ans.extend (right[j])
							j := j + 1
						end
					end
				end
				Result := ans
			end
		end
end
