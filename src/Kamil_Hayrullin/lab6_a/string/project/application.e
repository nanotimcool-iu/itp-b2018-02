note
	description: "string application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		-- Run application.
		local
			test : STRING
		do
			test := "abcdef"
			print(loopInverse(test) + "%N")
			print(recurseInverse(test))
		end


feature

	loopInverse(a : STRING) : STRING
	local
		i  : INTEGER
		b  : STRING
	do
		create b.make_filled ('0', a.count)
		from
			i := a.count
		until
			i < 1
		loop
			b.item (a.count - i + 1) := a.item (i)
			i := i - 1
		end

		Result := b
	end

	recurseInverse(a : STRING) : STRING
		do
			if a.capacity < 1 then
				Result := ""
			else
				Result := recurseInverse(a.substring (2, a.capacity)) + a.at(1).out
			end
		end
end
