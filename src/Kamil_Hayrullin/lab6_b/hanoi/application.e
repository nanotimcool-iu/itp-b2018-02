class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization


	make
			-- Run application.
		do
			print("Enter number of disks: " + "%N")
			Io.read_integer
			hanoi(Io.last_integer, 'Q', 'W', 'E')
		end


	hanoi(n: INTEGER; source : CHARACTER; target : CHARACTER; buf: CHARACTER)
		require
			non_negative:
				n >= 0
			different_disks:
				source /= target
				source /= buf
				target /= buf
		do
			if n > 0 then
				hanoi(n - 1, source, buf, target)
				move(source, target)
				hanoi(n - 1, buf, target, source)
			end
		end

	move(source, target: CHARACTER)
		require
			a : source /= target
		do
			print(source.out + " moving to " + target.out + "%N")
		end

end
