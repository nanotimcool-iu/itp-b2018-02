note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
		myframe : FRAME
		i : INTEGER
	do
		create myframe.make(true)
		myframe.game
	end

end
