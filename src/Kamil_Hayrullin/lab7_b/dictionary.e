note
	description: "Summary description for {DICTIONARY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DICTIONARY
create
	standart_words

feature
	words : ARRAYED_LIST[STRING]

feature

	standart_words(a: BOOLEAN)
	require
		a = true
	do
		create words.make (20)
		words.force ("car")
		words.force ("cat")
		words.force ("dog")
		words.force ("pear")
		words.force ("lime")
		words.force ("rose")
		words.force ("plum")
		words.force ("apple")
		words.force ("olive")
		words.force ("guava")
		words.force ("lemon")
		words.force ("horse")
		words.force ("house")
		words.force ("nance")
		words.force ("salad")
		words.force ("cherry")
		words.force ("danson")
		words.force ("quince")
		words.force ("elderberry")
		words.force ("dragonfruit")
	end

	add_word(i_word : STRING)
	local
		a : INTEGER
		i : INTEGER
		save : STRING
	do
		a := binary_search(words, i_word.count, 1, words.count) + 1
		save := words[a]
		from
			i := words.count
		until
			i < a + 1
		loop
			words[i] := words[i - 1]
			i := i - 1
		end
		words[a] := i_word
	end

	binary_search(arr : ARRAYED_LIST[STRING]; string : INTEGER; l : INTEGER; r : INTEGER) : INTEGER
	local
		m : INTEGER
		left : INTEGER
		right : INTEGER
	do
		left := l
		right := r
		from until right - left <= 1 loop
			m := (left + right) // 2

			if(arr[m].count < string) then
				left := m
			else
				right := m
			end
		end
		Result := left
	end

	printDict
	local
		i : INTEGER
	do
		Io.new_line
		Print("Dictionary: " + "%N")
		Io.new_line
		from
			i := 1
		until
			i > words.count
		loop
			print(words[i].out + "%N")
			i := i + 1
		end
		Io.new_line
	end

end
