note
	description: "Summary description for {FRAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	FRAME

create
	make

feature
	player_number : INTEGER
	guess_number : INTEGER
	players : ARRAYED_LIST[PLAYER]
	seed : INTEGER
	dict : DICTIONARY
	current_word : STRING
feature

	make(a : BOOLEAN)
	local
		i : INTEGER
	do
		create players.make (0)
		create dict.standart_words (true)
		print("Enter number of players: " + "%N")
		Io.read_integer
		set_player(Io.last_integer)
		print("Enter number of guesses: " + "%N")
		Io.read_integer
		set_guess (Io.last_integer)
		print("Enter seed (number from " + dict.binary_search (dict.words, player_number, 1, dict.words.count).out + " to " + dict.words.count.out + ") : ")
		Io.read_integer
		seed := Io.last_integer
		current_word := dict.words[seed]
		from
			i := 1
		until
			i > player_number
		loop
			print("Enter name of player #" + i.out + "%N")
			Io.readline
			set_names (Io.last_string.twin)
			i := i + 1
		end


	end

	set_player(n : INTEGER)
	require
		n_positive : n > 0
	do
		player_number := n
		players.make (n)
	end

	set_guess(n : INTEGER)
	require
		n_positive : n > 0
	do
		guess_number := n
	end

	set_names(name : STRING)
	do
		players.extend (create{PLAYER}.make (name))
	end

	game
	local
		i : INTEGER
		finish : BOOLEAN
		word : STRING
	do
		create word.make_filled ('_', current_word.count)
		finish := false
		from
			i := 1
		until
			finish = true
		loop
			print("%N" + players[i].name + ", enter character : ")
			Io.readline
			word := checkout(Io.last_string[1].twin, current_word, word)
			if checkTwo(Io.last_string[1], current_word, word) = false then
				guess_number := guess_number - 1
			end
			print("%N" + "Word is : " + word + "%N")
			if(word ~ current_word) then
				print(players[i].name + " WON!!! CONGRATULATIONS!")
				finish := true
			end
			print("Guesses left : " + guess_number.out)
			i := i + 1
			if(i > player_number) then
				i := 1
			end

			if(guess_number = 0) then
				print("%N" + "You lost. Try again")
				finish := true
			end

		end
	end

	checkout(char : CHARACTER; word : STRING; currentW : STRING) : STRING
	local
		i : INTEGER
	do
		Result := currentW
		from
			i := 1
		until
			i > word.count
		loop
			if(word.item (i) = char) then
				Result.item (i) := char
			end
			i := i + 1
		end
	end

	checkTwo(char : CHARACTER; word : STRING; currentW : STRING) : BOOLEAN
	local
		i : INTEGER
	do
		from
			i := 1
		until
			i > word.count
		loop
			if(word.item (i) = char) then
				Result := true
			end
			i := i + 1
		end
	end
end
