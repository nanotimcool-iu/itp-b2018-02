note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create
	make
	
feature
	name : STRING

feature

	make(i_string : STRING)
	do
		name := i_string
	end
end
