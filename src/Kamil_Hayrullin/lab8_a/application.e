note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			i : INTEGER
			root : BIN_TREE[INTEGER]
			one, two, three, four : BIN_TREE[INTEGER]
		do
			create root.make (15)
			print(root.left.info.out)
		end

end
