class BIN_TREE [G]

create
	make

feature

	info : G
	left : BIN_TREE[G]
	right : BIN_TREE[G]
	height : INTEGER

feature

	make(i_info : G)
	do
		height := 1
		info := i_info
		left := Current
		right := Current
	end

	set_height(a : INTEGER)
	do
		height := a
	end

	add(i : BIN_TREE[G])
	do
		if(left = Current) then
			left := i
			i.set_height(height + 1)
		elseif (right = Current) then
			right := i
			i.set_height(height + 1)
		else
			left.add (i)
		end
	end
end
