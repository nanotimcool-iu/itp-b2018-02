note
	description: "lab8b_2 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			i : INTEGER
			string : STRING
			treeS : HUFFMAN_TREE
		do
			string := "abracadabraa"
			print(string.out + "%N")
			create treeS.make_tree (string)
			Io.new_line
			treeS.code (treeS.tree[1], "")
			print("%Nencoded string : ")
		 	treeS.code_str (string)
		 	Io.new_line
	end
end
