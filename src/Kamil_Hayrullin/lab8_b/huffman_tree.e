note
	description: "Summary description for {HUFFMAN_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN_TREE

create

	make_tree

feature

	tree : ARRAYED_LIST[TREE_NODE]
	savepoint : ARRAYED_LIST[SAVE]

feature

	make_tree(string : STRING)
	local
		j : INTEGER
		count : INTEGER
		min1, min2 : TREE_NODE
		copyList : ARRAYED_LIST[TREE_NODE]
	do
		create savepoint.make (0)
		create tree.make (0)
		across string as i loop
			if(i.item /= " ") then
				count := search(i.item)
				if(count = -1) then
					tree.extend (create{TREE_NODE}.make_node (i.item, 1))
				else
					tree[count].increase (1)
				end
			end
		end

		sort

		from j := 1 until j > tree.count loop
			print("word " + tree[j].value.out + " has " + tree[j].number_of_copies.out +" copies" + "%N")
			j := j + 1
		end

		from until tree.count = 1 loop
			min1 := tree[1]
			min2 := tree[2]
			tree[1] := create{TREE_NODE}.make_node ('n', min1.number_of_copies + min2.number_of_copies)
			tree[1].set_left (min1)
			tree[1].set_right (min2)
			min1.set_parent (tree[1])
			min2.set_parent (tree[2])

			from j := 2 until j > tree.count - 1 loop
				tree[j] := tree[j + 1]
				j := j + 1
			end
			create copyList.make (0)
			from j := 1 until j > tree.count - 1 loop
				copyList.extend (tree[j])
				j := j + 1
			end
			tree.make (0)
			tree := copyList
			sort
		end

	end

	search(val : CHARACTER) : INTEGER
	local
		num : INTEGER
	do
		num := -1
		across tree as i loop
			if i.item.value = val then
				num := i.target_index
			end
		end
		Result := num
	end

	sort
	local
		i, j : INTEGER
		temp : TREE_NODE
	do
		from i := 1 until i > tree.count loop
			from j := 1 until j > tree.count - i loop
				if(tree[j].number_of_copies > tree[j + 1].number_of_copies) then
					temp := tree[j]
					tree[j] := tree[j + 1]
					tree[j + 1] := temp
				end
				j := j + 1
			end
			i := i + 1
		end
	end

	code_str(str : STRING)
	local
		i : INTEGER
	do
		across str as l loop
			from i := 1 until i > savepoint.count loop
				if l.item = savepoint[i].name then
					print(savepoint[i].code.out)
				end
				i := i + 1
			end
		end
	end


	code(n : TREE_NODE; str  : STRING)
	do
		if(n.value /= 'n') then
			print(n.value.out + " " + str.out + "%N")
			savepoint.extend (create{SAVE}.make (n.value, str))
		elseif attached n.left as l and attached n.right as r then
			code(l, str + "0")
			code(r, str + "1")
		end
	end
end
