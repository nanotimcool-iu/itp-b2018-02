note
	description: "Summary description for {SAVE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SAVE

create

	make

feature

	name : CHARACTER
	code : STRING

	make(i_name : CHARACTER; i_code : STRING)
	do
		name := i_name
		code := i_code
	end
end
