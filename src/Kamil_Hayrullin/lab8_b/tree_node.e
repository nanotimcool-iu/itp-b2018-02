note
	description: "Summary description for {TREE_NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	TREE_NODE

create
	make_node

feature

	value : CHARACTER
	number_of_copies : INTEGER
	code : STRING
	left : detachable TREE_NODE
	right : detachable TREE_NODE
	parent : detachable TREE_NODE

feature

	make_node(i_value : CHARACTER; num : INTEGER)
	do
			value := i_value
			number_of_copies := num
			left := void
			right := void
			parent := void
			code := ""
	end

	increase(num : INTEGER)
	do
		number_of_copies := number_of_copies + num
	end

	set_left(i_left : detachable TREE_NODE)
	do
		left := i_left
	end

	set_right(i_right : detachable TREE_NODE)
	do
		right := i_right
	end

	set_parent(i_parent : detachable TREE_NODE)
	do
		parent := i_parent
	end

	set_code(str : STRING)
	do
		code := str
	end
end
