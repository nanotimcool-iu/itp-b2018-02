note
	description: "lab9_a application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			str, key : STRING
			c : COMBINED_CIPHET
			a : VIGENERE_CIPHET
			b : SPIRAL_CIPHET
		do
			create c
			create a
			create b
			str := "MYLASTASSIGNMENT"
			key := "BUSYAAAAAAAAAAAA"
			print("encrypted : " + "%N")
			print(c.crypt (str, key))
			Io.new_line
			print("decrypted : " + "%N")
			print(c.decrypt (c.crypt (str, key), key))
			Io.new_line
		
		end

end
