note
	description: "Summary description for {CIPHET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	CIPHET


feature

	encrypt(str, key : STRING) : STRING
	deferred
	end

	decrypt(key, code : STRING) : STRING
	deferred
	end

end
