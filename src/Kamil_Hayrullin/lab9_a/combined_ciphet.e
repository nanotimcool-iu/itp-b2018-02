note
	description: "Summary description for {COMBINED_CIPHET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMBINED_CIPHET

feature

	crypt(str, key : STRING) : STRING
	local
		s : SPIRAL_CIPHET
		v : VIGENERE_CIPHET
	do
		create s
		create v
		Result := s.encrypt (v.encrypt (str, key), v.encrypt (str, key))
	end

	decrypt(str, key : STRING) : STRING
	local
		s : SPIRAL_CIPHET
		v : VIGENERE_CIPHET
	do
		create s
		create v
		Result := v.decrypt (s.decrypt (str, key), key)
	end
end
