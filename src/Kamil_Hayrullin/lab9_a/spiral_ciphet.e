note
	description: "Summary description for {SPIRAL_CIPHET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SPIRAL_CIPHET

inherit
	CIPHET
	redefine
		encrypt,
		decrypt
	end

feature

	encrypt(str, key : STRING) : STRING
	local
		i, j, count, m, n : INTEGER
		math : SINGLE_MATH
		matrix : ARRAY2[CHARACTER]
		ans : STRING
	do
		ans := ""
		create math
		create matrix.make_filled (' ', math.sqrt (str.count.to_real).truncated_to_integer + 1,
		math.sqrt (str.count.to_real).truncated_to_integer + 1)

		count := 1
		from i := 1 until i > matrix.width loop
			from j := 1 until j > matrix.height loop
				if(count <= str.count) then
					matrix.item (j, i) := str.item (count)
					count := count + 1
				else
					matrix.item (j, i) := ' '
				end
				j := j + 1
			end
			i := i + 1
		end

		j := 1
		i := 1
		m := matrix.width
		n := matrix.height

		from until i > m and j > n loop

			if(i < m) then -- right side s
				from count := j  until count > m loop
					ans := ans + matrix.item (m, count).out
					count := count + 1
				end
				m := m - 1
			end

			from count := m until count <= i loop -- low side bez
				ans := ans + matrix.item (count, n).out
				count := count - 1
			end
			n := n - 1

			if(j < n) then            -- left side s
				from count := m + 1 until count < i loop
					ans := ans + matrix.item (j, count).out
					count := count - 1
				end
				j := j + 1
			end

			from count := j until count > n loop   -- top side bez
				ans := ans + matrix.item (count, i).out
				count := count + 1
			end
			i := i + 1
		end
		if(matrix.width \\ 2 = 1) then
			ans := ans + matrix.item (matrix.width // 2 + 1, matrix.width // 2 + 1).out
		end
		Result := ans
	end

	decrypt (s, key: STRING): STRING
		local
			i, j, q: INTEGER
			arr: ARRAY2[CHARACTER]
			math : SINGLE_MATH
		do
			create math
			Result := ""
			create arr.make_filled (' ', math.sqrt(s.count).ceiling, math.sqrt(s.count).ceiling)

			from
				i := 1
				q := 1
			until
				2 * i > arr.width + 1
			loop
				from
					j := i
				until
					j > arr.width + 1 - i
				loop
					arr[j,arr.width + 1 - i] := s[q]
					q := q + 1
					j := j + 1
				end
				from
					j := arr.width - i
				until
					j < i
				loop
					arr[arr.width + 1 - i, j] := s[q]
					q := q + 1
					j := j - 1
				end
				from
					j := arr.width - i
				until
					j < i
				loop
					arr[j,i] := s[q]
					q := q + 1
					j := j - 1
				end
				from
					j := i + 1
				until
					j > arr.width - i
				loop
					arr[i,j] := s[q]
					q := q + 1
					j := j + 1
				end
				i := i + 1
			end


			from
				i := 1
			until
				i > arr.width * arr.width
			loop
				Result := Result + arr[(i - 1) // arr.width + 1, (i - 1) \\ arr.width + 1].out
				i := i + 1
			end
		end


end
