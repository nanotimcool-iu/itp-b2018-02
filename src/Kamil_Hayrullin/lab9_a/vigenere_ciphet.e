note
	description: "Summary description for {VIGENERE_CIPHET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHET

inherit
	CIPHET
	redefine
		encrypt,
		decrypt
	end


feature

	encrypt(str, key : STRING) : STRING
	local
		ans : STRING
	do
		ans := ""
		across str as i loop
			if(i.item /= ',' and i.item /= '.' and i.item /= '!' and i.item /= ' ' and i.item /= '?') then
				ans := ans + add(i.item, key.item (i.cursor_index)).out
			else
				ans := ans + i.item.out
			end
		end
		Result := ans
	end

	decrypt(code, key : STRING) : STRING
	local
		ans : STRING
	do
		ans := ""
		across key as i loop
			if(i.item /= ',' and i.item /= '.' and i.item /= '!' and i.item /= ' ' and i.item /= '?') then
				ans := ans + sub(code.item (i.cursor_index), i.item).out
			else
				ans := ans + i.item.out
			end
		end
		Result := ans
	end


	add(a : CHARACTER; b : CHARACTER) : CHARACTER
	local
		c1 : CHARACTER
		c2 : CHARACTER
		key : INTEGER
	do
		c1 := a
		c2 := b
		key := c2.code - 66
		c1 := 'A'
		c1 := c1 + key + (a.code - 64)
		if(c1.code > 90) then
			c1 := c1 - 26
		end
		Result := c1
	end

	sub(a, b : CHARACTER) : CHARACTER
	local
		c1 : CHARACTER
		c2 : CHARACTER
		key : INTEGER
	do
		c1 := a
		c2 := b
		key := c1.code - c2.code
		if(key < 0) then
			key := key * -1
			c1 := 'Z'
			c1 := c1 - key + 1
			if(c1.code > 90) then
				c1 := c2
			end
		else
			c1 := 'A'
			c1 := c1 + key
		end
		Result := c1
	end
end
