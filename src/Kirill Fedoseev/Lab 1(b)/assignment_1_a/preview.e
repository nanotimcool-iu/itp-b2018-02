note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature

	explore
		do
			Zurich.add_station ("Zoo", 1000, -500)
			Zurich.connect_station (4, "Zoo")
			Zurich_map.update
			from
			until
				false
			loop
				Zurich_map.station_view (Zurich.station ("Zoo")).highlight
				wait(1)
				Zurich_map.station_view (Zurich.station ("Zoo")).unhighlight
				wait(1)
			end
		end
end
