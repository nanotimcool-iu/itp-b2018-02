class
	APPLICATION

create
	execute

feature {NONE}

	execute
		do
			Io.new_line
			Io.put_string ("Name: Kirill Fedoseev")
			Io.new_line
			Io.put_string ("Age: ")
			Io.put_integer (17)
			Io.new_line
			Io.put_string ("Mother tongue: Russian")
			Io.new_line
			Io.put_string ("Has a cat: ")
			Io.put_boolean (false)
			Io.new_line
		end

end
