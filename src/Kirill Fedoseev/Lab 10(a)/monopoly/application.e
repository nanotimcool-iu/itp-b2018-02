note
	description: "monopoly application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			board: BOARD
		do
			create board.make
			board.add_player ("Kirill")
			board.add_player ("Artem")
			board.start_game
		end

end
