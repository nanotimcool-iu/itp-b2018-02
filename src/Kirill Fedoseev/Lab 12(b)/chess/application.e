class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature

	make
		local
			b: BOARD
		do
			create b.make
			b.move ([2, 2], [2, 4])
			b.move ([3, 1], [2, 2])
		end

end
