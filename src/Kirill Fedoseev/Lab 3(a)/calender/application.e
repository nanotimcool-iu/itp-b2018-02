class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature

	make
		local
			calender: CALENDER
			person: PERSON
			e1, e2: ENTRY
			t1, t2: TIME
		do
			create calender.make(31)
			create person.make("Kirill", "Innopolis", "k.fedoseev@innopolis.university", 1234567890)
			create t1.make (10, 35, 0)
			create t2.make (10, 35, 0)
			create e1.make (t1, person, "Subject1", "108")
			create e2.make (t2, person, "Subject2", Void)
			calender.add_entry (e1, 10)
			calender.add_entry (e2, 10)
			print(calender.printable_month)
			print(calender.in_conflict (10))
		end

end
