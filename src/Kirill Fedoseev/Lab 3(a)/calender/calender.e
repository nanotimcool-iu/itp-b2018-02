class
	CALENDER
create make

feature
	days: LIST[DAY]

	make(num: INTEGER)
		local
			i: INTEGER
			day: DAY
		do
			days := create {ARRAYED_LIST[DAY]}.make(num)
			from
				i := 1
			until
				i > num
			loop
				create day.make (i)
				days.extend (day)
				i := i + 1
			end
		end

	add_entry(e: ENTRY; day: INTEGER)
		do
			days[day].events.extend(e)
		end

	edit_subject (e: ENTRY; new_subject: STRING)
		do
			e.set_subject(new_subject)
		end

	edit_date (e: ENTRY; new_date: TIME)
		do
			e.set_date(new_date)
		end

	get_owner_name (e: ENTRY): STRING
		do
			Result := e.owner.name
		end


	in_conflict (day: INTEGER): LIST[ENTRY]
		local
			i, j: INTEGER
			events: LIST[ENTRY]
			done: ARRAYED_LIST[BOOLEAN]
			res: LINKED_LIST[ENTRY]
		do
			create res.make
			events := days[day].events
			create done.make_filled (events.count)
			from
				i := 1
			until
				i = events.count + 1
			loop
				from
					j := i + 1
				until
					j = events.count + 1
				loop
					if events[i].in_conflict (events[j]) then
						if not done[i] then
							res.extend (events[i])
							done[i] := true
						end
						if not done[j] then
							res.extend (events[j])
							done[j] := true
						end
					end
					j := j + 1
				end
				i := i + 1
			end
			Result := res
		end

	printable_month: STRING
		local
			i: INTEGER
		do
			Result := "--------------------------------------------------"
			across days as day loop
				if not day.item.events.is_empty then
					Result := Result + "%N" + day.item.out + "%N"
				end
			end
			Result := Result + "--------------------------------------------------%N"
		end
end
