class
	DAY
inherit ANY
	redefine
		out
	end
create
	make
feature
	date: INTEGER
	events: LIST[ENTRY]

	make(new_date: INTEGER)
		do
			date := new_date
			events := create {LINKED_LIST[ENTRY]}.make
		end

	out: STRING
		do
			Result := "Day #" + date.out + ":%N"
			across events as event loop
				Result := Result + "%T" + event.item.subject + " at " + event.item.date.out
				if attached event.item.place as p then
					Result := Result + " in " + p
				end
				Result := Result + "%N"
			end
		end
end
