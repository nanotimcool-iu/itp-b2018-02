class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature
	board: ARRAY2[INTEGER]
	winner, n, m: INTEGER

	make
		local
			turn, cur: INTEGER
			sizes: ARRAY[INTEGER]
		do
			io.read_integer
			n := io.last_integer -- # of rows
			io.read_integer
			m := io.last_integer -- # of columns
			create board.make_filled (0, n + 6, m + 6)
			create sizes.make_filled (0, 1, m)

			from
				turn := 1
			until
				winner > 0
			loop
				print("Turn of " + turn.out + " player%N")
				print_board
				io.read_integer
				cur := io.last_integer
				if sizes[cur] < n and cur >= 1 and cur <= m then
					board[sizes[cur] + 4, cur + 3] := turn
					turn := 3 - turn
					check_winner(sizes[cur] + 4, cur + 3)
					sizes[cur] := sizes[cur] + 1
				else
					print("Column is already full%N")
				end
			end
			print("%N" + (3 - turn).out + " player won%N")
			print_board
		end

	check_winner(x, y: INTEGER)
		local
			i, j, cur: INTEGER
		do
			cur	:= board[x, y]
			if board[x, y - 3] = board[x, y] and board[x, y - 2] = board[x, y] and board[x, y - 1] = board[x, y] or
			   board[x, y - 2] = board[x, y] and board[x, y - 1] = board[x, y] and board[x, y + 1] = board[x, y] or
			   board[x, y - 1] = board[x, y] and board[x, y + 1] = board[x, y] and board[x, y + 2] = board[x, y] or
			   board[x, y + 1] = board[x, y] and board[x, y + 2] = board[x, y] and board[x, y + 3] = board[x, y] or

			   board[x - 3, y] = board[x, y] and board[x - 2, y] = board[x, y] and board[x - 1, y] = board[x, y] or
			   board[x - 2, y] = board[x, y] and board[x - 1, y] = board[x, y] and board[x + 1, y] = board[x, y] or
			   board[x - 1, y] = board[x, y] and board[x + 1, y] = board[x, y] and board[x + 2, y] = board[x, y] or
			   board[x + 1, y] = board[x, y] and board[x + 2, y] = board[x, y] and board[x + 3, y] = board[x, y] or

			   board[x - 3, y - 3] = board[x, y] and board[x - 2, y - 2] = board[x, y] and board[x - 1, y - 1] = board[x, y] or
			   board[x - 2, y - 2] = board[x, y] and board[x - 1, y - 1] = board[x, y] and board[x + 1, y + 1] = board[x, y] or
			   board[x - 1, y - 1] = board[x, y] and board[x + 1, y + 1] = board[x, y] and board[x + 2, y + 2] = board[x, y] or
			   board[x + 1, y + 1] = board[x, y] and board[x + 2, y + 2] = board[x, y] and board[x + 3, y + 3] = board[x, y] or

			   board[x - 3, y + 3] = board[x, y] and board[x - 2, y + 2] = board[x, y] and board[x - 1, y + 1] = board[x, y] or
			   board[x - 2, y + 2] = board[x, y] and board[x - 1, y + 1] = board[x, y] and board[x + 1, y - 1] = board[x, y] or
			   board[x - 1, y + 1] = board[x, y] and board[x + 1, y - 1] = board[x, y] and board[x + 2, y - 2] = board[x, y] or
			   board[x + 1, y - 1] = board[x, y] and board[x + 2, y - 2] = board[x, y] and board[x + 3, y - 3] = board[x, y] then
				winner := cur
			end
		end

	print_board
		local
			i, j: INTEGER
		do
			from
				i := n + 3
			until
				i = 3
			loop
				from
					j := 4
				until
					j = m + 4
				loop
					print(board[i, j])
					j := j + 1
				end
				print("%N")
				i := i - 1
			end
		end

end
