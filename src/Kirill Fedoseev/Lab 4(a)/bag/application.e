class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature

	make
		local
			bag, bag1: MY_BAG
		do
			create bag.make
			create bag1.make
			bag.add ('1', 5)
			bag.add ('a', 3)
			bag1.add ('1', 5)
			print (bag.is_equal_bag (bag1))
			print ("%N")
			bag1.add ('a', 3)
			print (bag.is_equal_bag (bag1))
			print ("%N")
			bag1.remove ('a', 1)
			print (bag.is_equal_bag (bag1))
			print ("%N")
			print ((bag.min.code).to_character_8)
			print ("%N")
			print ((bag.max.code).to_character_8)
		end

end
