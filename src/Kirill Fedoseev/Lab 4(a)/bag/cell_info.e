class
	CELL_INFO

create
	make

feature

	value: CHARACTER assign set_value

	number_copies: INTEGER assign set_number

	set_value (new_value: CHARACTER)
		do
			value := new_value
		end

	set_number (new_number: INTEGER)
		do
			number_copies := new_number
		end

	make (new_value: CHARACTER; new_number_copies: INTEGER)
		do
			value := new_value
			number_copies := new_number_copies
		end

	invariant
		positive_number: number_copies >= 0

end
