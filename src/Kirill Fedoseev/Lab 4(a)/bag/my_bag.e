class
	MY_BAG

create
	make

feature

	elements: ARRAY [CELL_INFO]

	make
		local
			i: INTEGER
		do
			create elements.make_filled (create {CELL_INFO}.make (' ', 0), 0, 255)
			from
				i := 0
			until
				i = 256
			loop
				elements [i] := create {CELL_INFO}.make (i.to_character_8, 0)
				i := i + 1
			end
		end

	add (val: CHARACTER; n: INTEGER)
		require
			positive_n: n > 0
		do
			elements [val.code].number_copies := elements [val.code].number_copies + n
		ensure
			elements [val.code].number_copies = old elements [val.code].number_copies + n
		end

	remove (val: CHARACTER; n: INTEGER)
		require
			positive_n: n > 0
		do
			elements [val.code].number_copies := elements [val.code].number_copies - n
		ensure
			elements [val.code].number_copies = old elements [val.code].number_copies - n
		end

	min: CHARACTER
		local
			found: BOOLEAN
			i: INTEGER
		do
			from
				i := 0
			until
				i = 256
			loop
				if elements [i].number_copies > 0 and not found then
					Result := elements [i].value
					found := true
				end
				i := i + 1
			end
		end

	max: CHARACTER
		local
			found: BOOLEAN
			i: INTEGER
		do
			from
				i := 255
			until
				i = -1
			loop
				if elements [i].number_copies > 0 and not found then
					Result := elements [i].value
					found := true
				end
				i := i - 1
			end
		end

	is_equal_bag (b: MY_BAG): BOOLEAN
		local
			i: INTEGER
		do
			Result := true
			from
				i := 0
			until
				i = 256
			loop
				Result := Result and elements [i].number_copies = b.elements [i].number_copies
				i := i + 1
			end
		end

end
