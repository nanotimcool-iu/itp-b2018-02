class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature

	make
		local
			m1, m2: ARRAY2 [INTEGER]
			v1, v2: ARRAY[INTEGER]
			mop: MATRIX_OPER
			vop: VECTOR_OPER
		do
			create mop
			create vop
			create m1.make_filled (0, 3, 3)
			create m2.make_filled (0, 3, 3)
			m1 [1, 1] := 1
			m1 [1, 2] := 2
			m1 [1, 3] := 3
			m1 [2, 1] := 4
			m1 [2, 2] := 5
			m1 [2, 3] := 6
			m1 [3, 1] := 7
			m1 [3, 2] := 8
			m1 [3, 3] := 9
			m2 [1, 1] := 1
			m2 [1, 2] := 2
			m2 [1, 3] := 3
			m2 [2, 1] := 4
			m2 [2, 2] := 5
			m2 [2, 3] := 6
			m2 [3, 1] := 7
			m2 [3, 2] := 8
			m2 [3, 3] := 9
			create v1.make_filled (0, 1, 3)
			create v2.make_filled (0, 1, 3)
			v1[1] := 3
			v2[2] := 2
			print (mop.det (m1))
			print(vop.triangle_area (v1, v2))
		end


end
