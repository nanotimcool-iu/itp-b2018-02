class
	MATRIX_OPER

feature

	add (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			equal_size: m1.width = m2.width and m1.height = m2.height
		local
			i, j: INTEGER
		do
			create Result.make_filled (0, m1.height, m1.width)
			from
				i := 1
				j := 1
			until
				i = m1.height + 1
			loop
				Result [i, j] := m1 [i, j] + m2 [i, j]
				i := i + j // m1.width
				j := (j \\ m1.width) + 1
			end
		end

	minus (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			equal_size: m1.width = m2.width and m1.height = m2.height
		local
			i, j: INTEGER
		do
			create Result.make_filled (0, m1.height, m1.width)
			from
				i := 1
				j := 1
			until
				i = m1.height + 1
			loop
				Result [i, j] := m1 [i, j] - m2 [i, j]
				i := i + j // m1.width
				j := (j \\ m1.width) + 1
			end
		end

	prod (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			correct_size: m1.width = m2.height
		local
			i, j, k: INTEGER
		do
			create Result.make_filled (0, m1.height, m1.width)
			from
				i := 1
				j := 1
			until
				i = m1.height + 1
			loop
				from
					k := 1
				until
					k = m1.width + 1
				loop
					Result [i, j] := Result [i, j] + m1 [i, k] * m2 [k, j]
					k := k + 1
				end
				i := i + j // m1.width
				j := (j \\ m1.width) + 1
			end
		end

	det (m1: ARRAY2 [INTEGER]): INTEGER
		require
			is_square: m1.width = m1.height
		local
			i, j, k: INTEGER
			m2: ARRAY2 [INTEGER]
		do
			Result := 0
			if m1.width = 1 then
				Result := m1 [1, 1]
			elseif m1.width = 2 then
				Result := m1 [1, 1] * m1 [2, 2] - m1 [1, 2] * m1 [2, 1]
			else
				from
					i := 1
				until
					i = m1.width + 1
				loop
					create m2.make_filled (0, m1.width - 1, m1.height - 1)
					from
						j := 2
					until
						j = m1.height + 1
					loop
						from
							k := 1
						until
							k = m1.width + 1
						loop
							if k > i then
								m2 [j - 1, k - 1] := m1 [j, k]
							elseif k < i then
								m2 [j - 1, k] := m1 [j, k]
							end
							k := k + 1
						end
						j := j + 1
					end
					if i \\ 2 = 1 then
						Result := Result + m1 [1, i] * det (m2)
					else
						Result := Result - m1 [1, i] * det (m2)
					end
					i := i + 1
				end
			end
		end

end
