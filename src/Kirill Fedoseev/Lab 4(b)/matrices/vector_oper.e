class
	VECTOR_OPER

feature

	dot_product (v1, v2: ARRAY [INTEGER]): INTEGER
		require
			same_size: v1.count = v2.count
		local
			i: INTEGER
		do
			Result := 0
			from
				i := 1
			until
				i = v1.count + 1
			loop
				Result := Result + v1 [i] * v2 [i]
			end
		end

	cross_product (v1, v2: ARRAY [INTEGER]): ARRAY[INTEGER]
		require
			threeD: v1.count = 3 and v2.count = 3
		do
			create Result.make_filled (0, 1, 3)
			Result[1] := v1[2] * v2[3] - v1[3] * v2[2]
			Result[2] := v1[1] * v2[3] - v1[3] * v2[1]
			Result[3] := v1[1] * v2[2] - v1[2] * v2[1]
		end

	triangle_area (v1, v2: ARRAY [INTEGER]): DOUBLE
		require
			threeD: v1.count = 3 and v2.count = 3
		local
			cp: ARRAY[INTEGER]
		do
			cp := cross_product(v1, v2)
			Result := (cp[1] ^ 2 + cp[2] ^ 2 + cp[3] ^ 2) ^ .5 / 2
		end

end
