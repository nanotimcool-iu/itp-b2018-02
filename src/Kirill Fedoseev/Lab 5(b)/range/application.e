class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature

	make
		local
			r1, r2: RANGE
		do
			create r1.make(5, 10)
			create r2.make (5, 7)
			print(r1.subtract (r2))
		end

end
