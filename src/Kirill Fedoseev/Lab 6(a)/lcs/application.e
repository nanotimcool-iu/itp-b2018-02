note
	description: "lcs application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	lcs(s1, s2: STRING): STRING
		local
			s3,s4: STRING
		do
			if s1.count = 0 or s2.count = 0 then
				Result := ""
			elseif s1[s1.count] = s2[s2.count] then
				Result := lcs(s1.substring (1, s1.count - 1), s2.substring (1, s2.count - 1)) + s1[s1.count].out
			else
				s3 := lcs(s1.substring (1, s1.count - 1), s2)
				s4 := lcs(s1, s2.substring (1, s2.count - 1))
				Result := if s3.count < s4.count then s4 else s3 end
			end
		end
	make
			-- Run application.
		do
			print (lcs("12888345", "23412342345"))
		end

end
