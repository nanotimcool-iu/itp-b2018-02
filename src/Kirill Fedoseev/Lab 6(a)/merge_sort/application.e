note
	description: "merge_sort application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	merge_sort(a: ARRAY[INTEGER]): ARRAY[INTEGER]
		local
			b, c: ARRAY[INTEGER]
		do
			create b.make_filled (0, 1, a.count // 2)
			create c.make_filled (0, 1, a.count - a.count // 2)
			if a.count = 1 then
				Result := a
			else
				b.subcopy (a, 1, a.count // 2, 1)
				c.subcopy (a, a.count // 2 + 1, a.count, 1)

				Result := merge(merge_sort(b), merge_sort(c))
			end
		end

	merge(a,b: ARRAY[INTEGER]): ARRAY[INTEGER]
		local
			i, j: INTEGER
		do
			create Result.make_filled(0, 1, a.count + b.count)
			from
				i := 1
				j := 1
			until
				j = b.count + 1 and i = a.count + 1
			loop
				if j = b.count + 1 then
					Result[i + j - 1] := a[i]
					i := i + 1
				elseif i = a.count + 1 or else a[i] > b[j] then
					Result[i + j - 1] := b[j]
					j := j + 1
				else
					Result[i + j - 1] := a[i]
					i := i + 1
				end
			end
		end

	make
			-- Run application.
		do
			--| Add your code here
			print(merge_sort(<< 5, 8, 2, 1, 4, 10, 3, 9, 7, 6 >>))
			io.read_line
		end

end
