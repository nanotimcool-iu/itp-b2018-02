note
	description: "string_reverse application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	reverse_iterative(s: STRING): STRING
		local
			i: INTEGER
		do
			Result := ""
			from
				i := s.count
			until
				i = 0
			loop
				Result := Result + s[i].out
				i := i - 1;
			end
		end

	reverse_recursive(s: STRING): STRING
		do
			if s.count = 0 then
				Result := ""
			else
				Result := reverse_recursive(s.substring (2, s.count)) + s[1].out
			end
		end
	make
		do
			--| Add your code here
			print (reverse_iterative("Hello Eiffel World!%N"))
			print (reverse_recursive("Hello Eiffel World!%N"))
		end

end
