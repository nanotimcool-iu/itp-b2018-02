note
	description: "hanoi application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization
	hanoi(n: INTEGER; source, target, other: CHARACTER)
		do
			if n = 1 then
				print(source.out + " to " + target.out + "%N")
			else
				hanoi(n - 1, source, other, target)
				hanoi(1, source, target, other)
				hanoi(n - 1, other, target, source)
			end
		end
	make
			-- Run application.
		do
			hanoi(3, 'A', 'B', 'C')
		end

end
