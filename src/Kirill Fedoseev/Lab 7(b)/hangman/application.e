note
	description: "hangman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	n, turn, guesses, guessed, dead, i, l_seed: INTEGER
	lives: ARRAY[INTEGER]
	words: ARRAY[STRING]
	word: STRING
	field: ARRAY[CHARACTER]
	used: ARRAY[BOOLEAN]
	random: RANDOM
	guess: CHARACTER
	found: BOOLEAN
	input_file: PLAIN_TEXT_FILE
	l_time: TIME

	make
		do
			create input_file.make_open_read ("input.txt")
			create words.make_empty
			from until input_file.exhausted loop
				input_file.read_line
				words.force (input_file.last_string.twin, words.count + 1)
			end

			io.put_string ("Number of players:%N")
			io.read_integer
			n := io.last_integer
			io.put_string ("Number of guesses:%N")
			io.read_integer
			guesses := io.last_integer
			guessed := 0
			dead := 0

			create l_time.make_now
      		l_seed := l_time.hour
      		l_seed := l_seed * 60 + l_time.minute
      		l_seed := l_seed * 60 + l_time.second
      		l_seed := l_seed * 1000 + l_time.milli_second
			create random.set_seed (l_seed)
			from until words[1 + random.item \\ words.count].count >= n loop
				random.forth
			end
			word := words[1 + random.item \\ words.count]
			turn := n

			create lives.make_filled (guesses, 1, n)
			create used.make_filled (false, 1, 26)
			create field.make_filled ('_', 1, word.count)
			from i := 1 until i > word.count loop
				if word[i] = ' ' or word = '-' then
					field[i] := word[i]
					guessed := guessed + 1
				end
				i := i + 1
			end

			from until guessed = word.count or dead = n loop
				turn :=  1 + turn \\ n
				from until lives[turn] > 0 loop
					turn := 1 + turn \\ n
				end
				io.new_line
				from i := 1 until i > field.count loop
					io.put_character (field[i])
					io.put_character (' ')
					i := i + 1
				end
				io.put_string ("%NPlayer #" + turn.out + " turn:")
				io.put_string ("%N" + lives[turn].out + " attempts left%N")
				io.read_character
				from until ('a').code <= io.last_character.code and io.last_character.code <= ('z').code and not used[io.last_character.code - ('a').code + 1] loop
					io.read_character
				end
				guess := io.last_character
				used[guess.code - ('a').code + 1] := true

				found := false
				from i := 1 until i > word.count loop
					if word[i] = guess then
						found := true
						field[i] := guess
						guessed := guessed + 1
					end
					i := i + 1
				end
				if not found then
					lives[turn] := lives[turn] - 1
					if lives[turn] = 0 then
						dead := dead + 1
						io.put_string ("%NYou are dead :)%N")
					end
				end
			end
			from i := 1 until i > field.count loop
					io.put_character (field[i])
					io.put_character (' ')
					i := i + 1
			end
			io.new_line
			if guessed = word.count then
				io.put_string ("%NPlayer #" + turn.out + " won")
			else
				io.put_string("%NEverybody lost, correct word is: " + word)
			end
			io.read_character
		end

	invariant
		n >= 2
		word.count >= n

end
