note
	description: "bin_tree application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization
	b1, b2, b3, b4: BIN_TREE[INTEGER]
	make
			-- Run application.
		do
			create b1.make(0)
			create b2.make(1)
			create b3.make(2)
			create b4.make(3)

			b2.add(b3)
			b1.add(b4)
			b1.add(b2)
			print (b1.height)
			print (b2.height)
			print (b3.height)
			print (b4.height)
		end

end
