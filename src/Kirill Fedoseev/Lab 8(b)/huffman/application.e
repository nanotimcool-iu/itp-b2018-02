note
	description: "haffman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			huffman: HUFFMAN
			str: STRING
		do
			create huffman.make ("kirill fedoseev")
			str := huffman.encode ("fedoseev kirill")
			print(str + "%N")
			print(huffman.decode (str) + "%N")
		end

end
