note
	description: "cipher application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			cc: COMPOUND_CIPHER
			a, b: CIPHER
			key, s, s1: STRING
		do
			a := create {VIGENERE_CIPHER}
			b := create {SPIRAL_CIPHER}
			create cc.make (<< a, b >>)
			key := "yqlrfynttnsdsycn"
			print (cc.decode ("LAGHYH QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF   FAZ2/TGBZKFIREE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIULZIE KFYGEL//:RLH DXE Z", key))
		end

end
