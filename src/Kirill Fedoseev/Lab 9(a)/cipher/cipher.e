note
	description: "Summary description for {CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	CIPHER

feature

	encode (s, key: STRING): STRING
		deferred
		end

	decode (s, key: STRING): STRING
		deferred
		end

end
