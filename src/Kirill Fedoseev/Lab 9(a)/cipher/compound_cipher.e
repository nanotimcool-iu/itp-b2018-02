note
	description: "Summary description for {COMPOUND_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMPOUND_CIPHER

inherit
	CIPHER

create
	make

feature
	ciphers: ARRAY[CIPHER]
	make(new_ciphers: ARRAY[CIPHER])
		do
			ciphers := new_ciphers
		end
	encode(s, key: STRING): STRING
		do
			Result := s
			across ciphers as c loop
				Result := c.item.encode(Result, key)
			end
		end
	decode(s, key: STRING): STRING
		local
			i: INTEGER
		do
			Result := s
			from
				i := ciphers.count
			until
				i = 0
			loop
				Result := ciphers[i].decode(Result, key)
				--print(Result + "%N%N%N")
				i := i - 1
			end
		end
end
