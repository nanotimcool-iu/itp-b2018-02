note
	description: "Summary description for {SPIRAL_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SPIRAL_CIPHER

inherit

	CIPHER
	DOUBLE_MATH

feature

	encode (s, key: STRING): STRING
		local
			i, j: INTEGER
			arr: ARRAY2[CHARACTER]
		do
			Result := ""
			create arr.make_filled (' ', sqrt(s.count).ceiling, sqrt(s.count).ceiling)
			from
				i := 1
			until
				i > s.count
			loop
				arr[(i - 1) // arr.width + 1, (i - 1) \\ arr.width + 1] := s[i]
				i := i + 1
			end
			from
				i := 1
			until
				2 * i > arr.width + 1
			loop
				from
					j := i
				until
					j > arr.width + 1 - i
				loop
					Result := Result + arr[j,arr.width + 1 - i].out
					j := j + 1
				end
				from
					j := arr.width - i
				until
					j < i
				loop
					Result := Result + arr[arr.width + 1 - i, j].out
					j := j - 1
				end
				from
					j := arr.width - i
				until
					j < i
				loop
					Result := Result + arr[j,i].out
					j := j - 1
				end
				from
					j := i + 1
				until
					j > arr.width - i
				loop
					Result := Result + arr[i,j].out
					j := j + 1
				end
				i := i + 1
			end
		end

	decode (s, key: STRING): STRING
		local
			i, j, q: INTEGER
			arr: ARRAY2[CHARACTER]
		do
			Result := ""
			create arr.make_filled (' ', sqrt(s.count).ceiling, sqrt(s.count).ceiling)

			from
				i := 1
				q := 1
			until
				2 * i > arr.width + 1
			loop
				from
					j := i
				until
					j > arr.width + 1 - i
				loop
					arr[j,arr.width + 1 - i] := s[q]
					q := q + 1
					j := j + 1
				end
				from
					j := arr.width - i
				until
					j < i
				loop
					arr[arr.width + 1 - i, j] := s[q]
					q := q + 1
					j := j - 1
				end
				from
					j := arr.width - i
				until
					j < i
				loop
					arr[j,i] := s[q]
					q := q + 1
					j := j - 1
				end
				from
					j := i + 1
				until
					j > arr.width - i
				loop
					arr[i,j] := s[q]
					q := q + 1
					j := j + 1
				end
				i := i + 1
			end

			from
				i := 1
			until
				i > arr.width * arr.width
			loop
				print(arr[(i - 1) // arr.width + 1, (i - 1) \\ arr.width + 1])

				if i \\ arr.width = 0 then
					print("%N")
				end
				i := i + 1

			end

			from
				i := 1
			until
				i > arr.width * arr.width
			loop
				Result := Result + arr[(i - 1) // arr.width + 1, (i - 1) \\ arr.width + 1].out
				i := i + 1
			end
		end

end
