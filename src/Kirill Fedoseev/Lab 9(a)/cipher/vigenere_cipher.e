note
	description: "Summary description for {VIGENERE_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHER

inherit

	CIPHER

feature

	encode (s, key: STRING): STRING
		local
			i, j: INTEGER
		do
			Result := ""
			from
				i := 1
				j := 1
			until
				i > s.count
			loop
				if s [i].is_alpha then
					Result := Result + ((s[i].as_lower.code - ('a').code * 2 + key [(j - 1) \\ key.count + 1].as_lower.code) \\ 26 + ('a').code).to_character_8.out
					j := j + 1
				else
					Result := Result + s [i].out
				end
				i := i + 1
			end
		end

	decode (s, key: STRING): STRING
		local
			i, j: INTEGER
		do
			Result := ""
			from
				i := 1
				j := 1
			until
				i > s.count
			loop
				if s [i].is_alpha then
					Result := Result + ((s[i].as_lower.code - key [(j - 1) \\ key.count + 1].as_lower.code + 26) \\ 26 + ('a').code).to_character_8.out
					j := j + 1
				else
					Result := Result + s [i].out
				end
				i := i + 1
			end
		end
end
