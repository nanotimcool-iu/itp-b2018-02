class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature

	make
		local
			stack: MY_STACK[INTEGER]
			bs: MY_BOUNDED_STACK[INTEGER]
		do
			create stack.make
			create bs.make_limited (3)
			stack.push (1)
			print (stack.item)
			stack.remove
			print (stack.is_empty)
			bs.push (1)
			bs.push (2)
			bs.push (3)
			bs.push (4)
		end

end
