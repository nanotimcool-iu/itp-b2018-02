note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		do
		zurich.add_station ("ZOOOOO", 10, 10)
		zurich.connect_station (15, "ZOOOOO")
		zurich_map.update

		zurich_map.station_view (zurich.station ("ZOOOOO")).highlight
		wait(1)
		zurich_map.station_view (zurich.station ("ZOOOOO")).unhighlight
		wait(1)
		zurich_map.station_view (zurich.station ("ZOOOOO")).highlight
		wait(1)
		zurich_map.station_view (zurich.station ("ZOOOOO")).unhighlight
		wait(1)
		zurich_map.station_view (zurich.station ("ZOOOOO")).highlight
		wait(1)
		zurich_map.station_view (zurich.station ("ZOOOOO")).unhighlight
		wait(1)
		zurich_map.station_view (zurich.station ("ZOOOOO")).highlight
		wait(1)
		zurich_map.station_view (zurich.station ("ZOOOOO")).unhighlight
		zurich_map.station_view (zurich.station ("ZOOOOO")).highlight
		end

end
