class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			m: MONOPOLY
		do
			create m.make (2)
			m.start
		end

end
