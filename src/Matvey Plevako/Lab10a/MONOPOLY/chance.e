class
	CHANCE

inherit

	FIELD

create
	make

feature {NONE} -- Initialization

	name: STRING

	rand: RANDOMIZER

	make
			-- Initialization for `Current'.
		do
			name := "CHANCE"
			create rand.make
		end

feature {MONOPOLY}

	process (p: PLAYER)
		local
			delta: INTEGER
		do
			delta := rand.chance
			if delta < 0 then
				print ("You lose " + delta.abs.out + "K%N")
			else
				print ("You get " + delta.out + "K%N")
			end
			p.add_balance (rand.chance)
		end

end
