class
	JAIL

inherit

	FIELD

create
	make

feature {NONE}

	name: STRING

	make
		do
			name := "JAIL"
		end

feature {MONOPOLY}

	process (p: PLAYER)
		do
			print ("You are in the jail%NGame is over%N")
			p.set_balance (-1) -- make him lose
		end

end
