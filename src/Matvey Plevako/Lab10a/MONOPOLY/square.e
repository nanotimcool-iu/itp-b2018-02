deferred class
	FIELD

inherit

	ANY
		redefine
			out
		end

feature

	name: STRING
		deferred
		end

feature {MONOPOLY}

	out: STRING
		do
			Result := name
		end

	process (p: PLAYER)
		require
			real_person: p /= Void
		deferred
		end

end
