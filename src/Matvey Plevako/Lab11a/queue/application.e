class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature

	make
		local
			q:EVENT_QUEUE
			e:EVENT
			i:INTEGER
		do
			create q.make
			from
				i:=1
			until
				i = 100000
			loop
				q.add (create {EVENT}.make (i, "t"))
				i := i + 1
			end

			print(q.next.put)
			q.clear

			io.putbool (q.is_empty)

			q.clear
			q.add (create {EVENT}.make (5, "TEST"))
			q.add (create {EVENT}.make (5, "TEST"))
			q.add (create {EVENT}.make (5, "TEST"))

			e := q.extract
			io.putbool (q.is_empty)
		end
end
