note
	description: "spooky application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			l:LEAP_YEAR
			year1, year2, year3:INTEGER
			a:ANAGRAMS
			array:ARRAYED_SET[STRING]
			word:STRING
		do
			-- Leap Year
			print("Leap year module%N")
			create l
			year1 := 1000
			print(year1.out + " is leap year: " + l.is_leap_year(year1).out + "%N")
			year2 := 2000
			print(year2.out + " is leap year: " + l.is_leap_year(year2).out + "%N")
			year3 := 2016
			print(year3.out + " is leap year: " + l.is_leap_year(year3).out + "%N")
			-- Anagrams
			print("%NAnagrams module%N")
			create a
			word := "abc"
			print("Anagrams of word " + word + ":%N")
			a.anagrams (word)
			-- Ski Resort			
		end

end
