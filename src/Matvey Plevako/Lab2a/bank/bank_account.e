note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			balance := 256
			owner := "Matvey Plevako"
			deposit(744)
			withdraw(499)
			print(Current)
		end


	owner : STRING
				--the name of the owner

	balance : INTEGER
				--the balance of the owner

	deposit (value: INTEGER)
		require
			positive_value: value >= 0
		do
			balance := balance + value
		end

	withdraw (value: INTEGER)
		require
			positive_value: value >= 0
		do
			balance := balance - value
		end

invariant
	greater_than_100: balance >= 100
	less_than_1000000: balance <= 1000000

end
