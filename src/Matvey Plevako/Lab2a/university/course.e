note
	description: "university application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization



	make
			-- Run application.
		do
			create_class("Introduction to Programming I", 1801, "Wednesdays", 150, 3)
		end



	name: STRING -- Course name

	identifier: INTEGER -- Course identifier

	schedule: STRING -- Course schedule

	max_students: INTEGER -- Maximum number of students that can be enrolled to the course

	min_students: INTEGER -- Maximum number of students that can be enrolled to the course


	create_class (name_: STRING; identifier_: INTEGER; schedule_: STRING; max_students_ : INTEGER; min_students_: INTEGER) -- Creates course
		do
			name := name_
			identifier := identifier_
			schedule := schedule_
			max_students := max_students_
			min_students := min_students_
		end

end

