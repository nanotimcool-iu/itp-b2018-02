note
	description: "CMS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			c1, c2, c3 : CONTACT
		do
			c1 := create_contact ("first", 1234567890, "coder", "coder@mail.com")
			c2 := create_contact ("second", 1234567890, "teacher", "teacher@mail.com")
			c3 := create_contact ("third", 1234567890, "doctor", "doctor@mail.com")
			add_emergency_contact (c1, c2)
			add_emergency_contact (c3, c2)
			edit_contact(c2, c2.name, 1987654321, c2.work_place, "newmail@mail.com", c2.call_emergency)
			remove_emergency_contact(c2)
			remove_emergency_contact(c3)
			print(c1)
			print(c2)
			print(c3)
		end

feature
	create_contact(name_ : STRING; phone_number_ : INTEGER; work_place_ : STRING; email_ : STRING) : CONTACT
		local
			U1 : CONTACT
		do
			create U1.make (name_, phone_number_, work_place_, email_)
			Result := U1
		end

feature
	edit_contact(c: CONTACT; name_ : STRING; phone_number_ : INTEGER; work_place_ : STRING; email_ : STRING; call_emergency_ : CONTACT)
		do
			c.name := name_
			c.phone_number := phone_number_
			c.work_place := work_place_
			c.email := email_
			c.call_emergency := call_emergency_
		end

feature
	add_emergency_contact(c1:CONTACT;c2:CONTACT)
		do
			c1.call_emergency := c2
		end

feature
	remove_emergency_contact(c:CONTACT)
		do
			c.call_emergency := c
		end

end
