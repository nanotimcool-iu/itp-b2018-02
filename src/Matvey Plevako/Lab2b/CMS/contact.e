class
	CONTACT

create
	make

feature {NONE} -- Initialization

	make (name_ : STRING; phone_number_ : INTEGER; work_place_ : STRING; email_ : STRING)
			-- Run application.
		do
			--| Add your code here
			name := name_
			phone_number := phone_number_
			work_place := work_place_
			email := email_
			call_emergency := Current

		end


feature

	set_name(name_ : STRING)
	do
		name := name_
	end

	set_phone_number(phone_number_ : INTEGER)
	do
		phone_number := phone_number_
	end

	set_work_place(work_place_ : STRING)
	do
		work_place := work_place_
	end

	set_email(email_ : STRING)
	do
		email := email_
	end

	set_call_emergency(call_emergency_ : CONTACT)
	do
		call_emergency := call_emergency_
	end



	name : STRING assign set_name
	phone_number : INTEGER assign set_phone_number
	work_place : STRING assign set_work_place
	email : STRING assign set_email
	call_emergency : CONTACT assign set_call_emergency



invariant
	right_phne_lenght: phone_number.out.count = 10

end
