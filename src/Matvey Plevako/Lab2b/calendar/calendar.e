note
	description: "CALENDAR application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			someone : PERSON
			e1,e2 : ENTRY
			date : DATE_TIME
			-- Run application.
		do
				--| Add your code here
			create someone.make("Matvey Plevako", 1234567890, "hotel", "hotel@mail.com")
			create date.make_now
			e1 := create_entry (date , someone, "Building new hotel", "Innopolis")
			e2 := create_entry (date, someone, "Inviting new people to book rooms", "Innopolis")
			print (e1.subject + "%N")
			print (e1.subject + "%N")
			print(get_owner_name (e1) + "%N")
			io.put_boolean (in_conflict (e1, e2))
		end

feature
	create_entry (date: DATE_TIME; owner: PERSON; subject, place: STRING): ENTRY
		local
			E: ENTRY
		do
			create E.make (date, owner, subject, place)
			Result := E
		end

feature
	edit_subject (e: ENTRY; new_subject: STRING)
		do
			e.set_subject(new_subject)
		end

feature
	edit_date (e: ENTRY; new_date: DATE_TIME)
		do
			e.set_date(new_date)
		end

feature
	get_owner_name (e: ENTRY): STRING
		do
			Result := e.owner.name
		end

feature
	in_conflict (e1, e2: ENTRY): BOOLEAN
		do
			Result := e1.owner = e2.owner and e1.date = e2.date
		end

end
