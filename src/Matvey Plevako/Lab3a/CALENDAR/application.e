note
	description: "CALENDAR application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			-- Run application.
			Me : PERSON
			Calendar : CALENDAR
			some_day : DAY
			some_action_on_this_day1, some_action_on_this_day2 : ENTRY
			some_day1, some_day2 : DATE_TIME
		do
			--| Add your code here
			create ME.make ("Matvey Plevako", 1234567890, "Innopolis", "m.plevako@innopolis.university")
			create Calendar.make (29)
			create some_day.make (1)
			create some_day1.make (2018, 1, 1, 1, 1, 1)
			create some_day2.make (2018, 1, 1, 1 ,1 ,1)
			create some_action_on_this_day1.make (some_day1, Me, "Some action", "Inno")
			create some_action_on_this_day2.make (some_day2, Me, "Something more", "Innoo" )
			Calendar.add_entry (some_action_on_this_day1, 29)
			Calendar.add_entry (some_action_on_this_day2, 29)


			print(Calendar.printable_month)
			print(Calendar.in_conflict (29))


		end

end
