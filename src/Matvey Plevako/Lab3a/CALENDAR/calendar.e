note
	description: "CALENDAR application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit
	ARGUMENTS

create
	make

feature -- Initialization

	make(days_: INTEGER)
			-- Run application.
		local
			i : INTEGER
			days_arr : LINKED_LIST[DAY]
			day : DAY
		do
			create days_arr.make
			from
				i := 1
			until
				i > days_
			loop
				create day.make(i)
				days_arr.extend(day)
				i := i + 1
			end

			days := days_arr
		end

feature
	days: LIST[DAY]

	add_entry(e:ENTRY;day:INTEGER)
		do
			days[day].add_event(e)
		end

	edit_subject (e: ENTRY; new_subject: STRING)
		do
			e.set_subject (new_subject)
		end

	create_entry (date: DATE_TIME; owner: PERSON; subject, place: STRING): ENTRY
		local
			E: ENTRY
		do
			create E.make (date, owner, subject, place)
			Result := E
		end

	edit_date (e: ENTRY; new_date: DATE_TIME)
		do
			e.set_date(new_date)
		end

	get_owner_name (e: ENTRY): STRING
		do
			Result := e.owner.name
		end

	is_conflict (e1, e2: ENTRY): BOOLEAN
		do
			Result := e1.owner = e2.owner and e1.date.is_equal(e2.date)
		end

	in_conflict (day: INTEGER): LIST[ENTRY]
		local
			ret_list : LINKED_LIST[ENTRY]
			i, j : INTEGER
			events: LIST[ENTRY]
			added : ARRAYED_LIST[BOOLEAN]
		do
			create ret_list.make
			events := days[day].events
			create added.make_filled(events.count)

			from
				i := 1
			until
				i > events.count
			loop
				from
					j := 1
				until
					j > events.count
				loop
					if is_conflict(events[i], events[j]) and i /= j then
						if not added[i] then
							added[i] := True
							ret_list.extend (events[i])
						end
						if not added[j] then
							added[j] := True
							ret_list.extend (events[j])
						end
					end
					j := j + 1
				end
				i := i + 1
			end

			Result := ret_list
		end

	printable_month : STRING
		local
			i, j  : INTEGER
			events : LIST[ENTRY]
			events_of_day : STRING
		do
			Result := "Printable version of month:%N"
			from
				i := 1
			until
				i > days.count
			loop
				events := days[i].events
				events_of_day := "  "
				from
					j := 1
				until
					j > events.count
				loop
					events_of_day := events_of_day + events[j].subject + "%N  "
					j := j + 1
				end
				Result := Result + i.out + ":%N" + events_of_day + "%N%N"
				i := i + 1
			end
		end

end
