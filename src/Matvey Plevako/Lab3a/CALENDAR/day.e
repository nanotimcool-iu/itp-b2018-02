note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	make

feature
	make(date_: INTEGER)
		local
			events_ : LINKED_LIST[ENTRY]
		do
			date := date_
			create events_.make
			events := events_
		end

	date: INTEGER
	events: LIST[ENTRY]

feature
	add_event(event : ENTRY)
		do
			events.extend(event)
		end

end
