note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	make

feature
	make (date_: DATE_TIME; owner_: PERSON; subject_: STRING; place_: STRING)
			-- Initialization
		do
			date := date_
			owner := owner_
			set_subject(subject_)
			place := place_
		end

feature

	date: DATE_TIME
	owner: PERSON
	subject: STRING
	place: STRING
	set_subject(subject_ : STRING)
	do
		subject := subject_
	ensure
		subject = subject_
	end

	set_date(date_ : DATE_TIME)
	do
		date := date_
	ensure
		date = date_
	end

end
