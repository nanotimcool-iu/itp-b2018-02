note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature
	make(name_ : STRING; phone_number_ : INTEGER; work_place_ : STRING; email_ : STRING)
	do
		name := name_
		phone_number := phone_number_
		work_place := work_place_
		email := email_
	end

feature

	name:STRING
	phone_number:INTEGER
	work_place:STRING
	email:STRING

invariant
	right_phne_lenght: phone_number.out.count = 10

end
