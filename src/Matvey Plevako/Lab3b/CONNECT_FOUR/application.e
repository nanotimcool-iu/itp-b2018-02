note
	description: "CONNECT_FOUR application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	make
			-- Run application.
		local
			n, m : INTEGER
		do
			--| Add your code here
			print ("Hello Eiffel World! This is a connect four game.%N")
			print("Please, input size of field you want to play:%N")
			io.read_integer
			n := io.last_integer
			io.read_integer
			m := io.last_integer
			create field.make(n, m)
			create game.make(field)
			print("Starting%N")
			game.start_game
		end

	field : FIELD
	game : GAME

end
