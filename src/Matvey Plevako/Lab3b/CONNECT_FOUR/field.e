note
	description: "Summary description for {FIELD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	FIELD

create
	make

feature


	make(n_, m_: INTEGER)
		local
			i : INTEGER
			empty_list : ARRAYED_LIST[INTEGER]
		do
			create desk.make (0)
			from
				i := 1
			until
				i > n_
			loop
				create empty_list.make_filled (m_)
				desk.extend(empty_list)
				i := i + 1
			end
			create dots_in_row.make_filled(m_)
			n := desk.count
			m := desk[1].count
		end

	desk : ARRAYED_LIST[ARRAYED_LIST[INTEGER]]
	dots_in_row : ARRAYED_LIST[INTEGER]
	n, m : INTEGER

feature
	put_dot(row, player : INTEGER) : BOOLEAN
		require
			player <= 2 and player >= 1

		local
			line : ARRAYED_LIST[INTEGER]
		do
			if not (row >= 1 and row <= m) then
				print("%N WARNING: wrong row%N")
			else
				if not(dots_in_row[row] < n) then
					print("%N WARNING: row is full%N")
				else
					line := desk[dots_in_row[row] + 1]
					line[row] := player
					dots_in_row[row] := dots_in_row[row] + 1
					Result := True
				end
			end
		end

feature
	string_field : STRING
		local
			i, j : INTEGER
			line : ARRAYED_LIST[INTEGER]
		do
			Result := ""
			from
				i := n
			until
				i < 1
			loop
				line := desk[i]
				from
					j := 1
				until
					j > m
				loop
					Result := Result + line[j].out + " "
					j := j + 1
				end
				Result := Result + "%N"
				i := i - 1
			end
		end

end
