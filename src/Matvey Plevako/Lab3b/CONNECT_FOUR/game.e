note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature
	make(field_ : FIELD)
		do
			field := field_
			player := 0
			winning_length := 4
		end

	field : FIELD
	player : INTEGER
	winning_length : INTEGER

feature
	start_game
		local
			cmd : STRING
			online : BOOLEAN
			row, line : INTEGER
		do
			from
				online := True
			until
				online = False
			loop
				print("%Ninput: ")
				io.read_line
				cmd := io.last_string
				if cmd.is_integer then
					row := cmd.to_integer
					next_step(row)
					if row >= 1 and row <= field.m then
						line := field.dots_in_row[row]
						print("%N%N")
						print(field.string_field)
						if check_winner(line, row) then
							print(((player + 1) \\ 2  + 1).out + " - winner!%N")
							online := False
						end
					end

				end
				if cmd.is_equal("stop") then
					print("bye%N")
					online := False
				end
			end

		end

feature
	next_step(row : INTEGER)
		do
			if field.put_dot (row, player + 1) then
				player := (player + 1) \\ 2
			end
		end

feature
	check_winner(i, j : INTEGER) : BOOLEAN
		do
			Result := check_line(i) or check_row(j) or check_diagonal(i, j) or check_reverse_diagonal(i, j)
		end

	check_line(n : INTEGER) : BOOLEAN
		local
			i : INTEGER
			cur : INTEGER
			length : INTEGER
			line : ARRAYED_LIST[INTEGER]
		do
			line := field.desk[n]
			cur := line[1]
			if cur /= 0 then
				length := 1
			else
				length := 0
			end
			from
				i := 2
			until
				i > field.m
			loop
				if line[i] /= cur or line[i] = 0 then
					cur := line[i]
					if cur /= 0 then
						length := 1
					else
						length := 0
					end
				else
					length := length + 1
					if length = winning_length then
						Result := True
					end
				end
				i := i + 1
			end
		end

	check_row(n : INTEGER) : BOOLEAN
		local
			line : ARRAYED_LIST[INTEGER]
			i : INTEGER
			length : INTEGER
			cur : INTEGER
		do
			line := field.desk[1]
			cur := line[n]
			if cur /= 0 then
				length := 1
			else
				length := 0
			end
			from
				i := 2
			until
				i > field.n
			loop
				line := field.desk[i]
				if line[n] /= cur or line[n] = 0 then
					cur := line[n]
					if cur /= 0 then
						length := 1
					else
						length := 0
					end
				else
					length := length + 1
					if length = winning_length then
						Result := True
					end
				end
				i := i + 1
			end
		end

	check_diagonal(n, m: INTEGER) : BOOLEAN
		local
			i, j, c : INTEGER
			line : ARRAYED_LIST[INTEGER]
			length : INTEGER
			cur : INTEGER
		do
			if n > m then
				i := n - m + 1
				j := 1
			else
				j := m - n + 1
				i := 1
			end

			line := field.desk[i]
			cur := line[j]
			if cur /= 0 then
				length := 1
			else
				length := 0
			end
			from
				i := i + 1
				j := j + 1
			until
				i > field.n or j > field.m
			loop
				line := field.desk[i]
				if line[j] /= cur or line[j] = 0 then
					cur := line[j]
					if cur /= 0 then
						length := 1
					else
						length := 0
					end
				else
					length := length + 1
					if length = winning_length then
						Result := True
					end
				end
				i := i + 1
				j := j + 1

			end

		end


		check_reverse_diagonal(n, m: INTEGER) : BOOLEAN
		local
			i, j, c : INTEGER
			line : ARRAYED_LIST[INTEGER]
			length : INTEGER
			cur : INTEGER
		do
			if n - 1 > field.m - m then
				c := field.m - m
			else
				c := n - 1
			end

			i := n - c
			j := n + c

			line := field.desk[i]
			cur := line[j]
			if cur /= 0 then
				length := 1
			else
				length := 0
			end
			from
				i := i + 1
				j := j - 1
			until
				i > field.n or j < 1
			loop
				line := field.desk[i]
				if line[j] /= cur or line[j] = 0 then
					cur := line[j]
					if cur /= 0 then
						length := 1
					else
						length := 0
					end
				else
					length := length + 1
					if length = winning_length then
						Result := True
					end
				end
				i := i + 1
				j := j - 1

			end

		end

end
