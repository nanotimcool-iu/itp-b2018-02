note
	description: "LOOP_PAINTING application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			num, last_line_calc, last_line_size, last_line_num : INTEGER
			last_string_len, i, cur, c, to_add, len: INTEGER
			space, line : STRING
		do
			--| Add your code here
			print ("Hello Eiffel World!%N")
			print("Size: ")

			io.read_integer
			num := io.last_integer


			last_line_calc := num // 2 + num \\ 2
			print(last_line_calc.out + "%N")
			last_line_num := (last_line_calc * last_line_calc) + last_line_calc * ((num + 1) \\ 2)
			print(last_line_num.out + "%N")

			from
				i := last_line_num
			until
				i = last_line_num - last_line_calc
			loop
				last_string_len := last_string_len + i.out.count + 1
				i := i - 1
			end

			last_string_len := last_string_len - (num \\ 2) + 2

			print(last_string_len.out + "%N")
			cur := 1
			from
				i := 1
			until
				i > num
			loop
				line := ""
				len := i // 2 + i \\ 2
				if i \\ 2 = 0 then
					line := " "
				end
				from
					c := len
				until
					c < 1
				loop
					line := line + cur.out + " "
					cur := cur + 1
					c := c - 1
				end
				to_add := last_string_len - line.count
				space := " "
				space.multiply (to_add * 2)
				line := line + space
				from
					c := len
				until
					c < 1
				loop
					cur := cur - 1
					line := line + cur.out + " "
					c := c - 1
				end
				cur := cur + len
				i := i + 1
				print(line + "%N")
			end


		end

end
