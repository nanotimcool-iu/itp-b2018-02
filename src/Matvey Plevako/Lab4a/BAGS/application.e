note
	description: "BAGS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
		bag, bag1 :BAG_LIST
			-- Run application.
		do
			--| Add your code here
			create bag.make
			bag.add ('a', 10)
			bag.add ('b', 1)
			bag.add('b', 2)
			print(bag.string_bag)
			print(bag.min.out + "%N")
			print(bag.max.out + "%N")
			create bag1.make
			bag1.add ('a', 10)
			bag1.add ('b', 3)
			print(bag.is_equal_bag (bag1))
			print("%N")
		end

end
