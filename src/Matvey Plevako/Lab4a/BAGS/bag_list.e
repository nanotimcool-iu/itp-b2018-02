note
	description: "Summary description for {BAG_LIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BAG_LIST

create
	make

feature
	make
	local
		i : INTEGER
		do
			create entry.make('a')
			create elements.make_filled (entry, 1, 256)
			from
				i := 0
			until
				i = 256
			loop
				create entry.make(i.to_character_8)
				elements.enter (entry, i + 1)
				i := i + 1
			end
		end


	add(val:CHARACTER;n:INTEGER)
		do
			elements[val.code + 1].change(n)
		end

	remove (val:CHARACTER; n:INTEGER)
		do
			elements[val.code + 1].change(-n)
		end

	min : CHARACTER
		local
			i : INTEGER
			b : BOOLEAN
		do
			from
				i := 1
			until
				i = 257
			loop
				if (not b) and elements[i].number_copies > 0 then
					Result := elements[i].value
					b := True
				end
				i := i + 1
			end
		end

	max : CHARACTER
		local
			i : INTEGER
		do
			from
				i := 1
			until
				i = 257
			loop
				if elements[i].number_copies > 0 then
					Result := elements[i].value
				end
				i := i + 1
			end
		end

	string_bag : STRING
		local
			i : INTEGER
		do
			Result := "elemens:%N"
			from
				i := 1
			until
				i = 257
			loop
				if elements[i].number_copies > 0 then
					Result := Result + elements[i].value.out + " : " + elements[i].number_copies.out + "%N"
				end
				i := i + 1
			end
		end

	elements : ARRAY[CELL_INFO]
	entry : CELL_INFO

	is_equal_bag (b: BAG_LIST): BOOLEAN
		do
			Result := b.string_bag.is_equal(Current.string_bag)
		end

end
