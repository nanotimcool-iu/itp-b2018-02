note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	make

feature
	value : CHARACTER
	number_copies : INTEGER

	make(c : CHARACTER)
		do
			value := c
			number_copies := 0
		end

	change(val : INTEGER)
		do
			if number_copies + val < 0 then
				number_copies := 0
			else
				number_copies := number_copies + val
			end

		end

end
