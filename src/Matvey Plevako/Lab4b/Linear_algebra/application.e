note
	description: "Linear_algebra application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			m : MATRIX_OPER
			ar1, ar2 : ARRAY2[INTEGER]
			i, j, k : INTEGER
				-- Run application.
		do
			--| Add your code here
			create ar1.make_filled (3, 6, 6)
			create ar2.make_filled (3, 6, 6)
			create m.make
			ar1 := m.minus(ar1, ar2)
			print("%N")
			k := 1
			from
				i := 1
			until
				i = ar1.height + 1
			loop
				from
					j := 1
				until
					j = ar1.width + 1
				loop
					ar1[i, j] := k
					k := k + 1
					j := j + 1
				end
				i := i + 1
			end

			m.to_string (ar1)

			k := m.det (ar1)
			print(k.out + "%N%N")

			create ar1.make_filled (1, 2, 2)
			create ar2.make_filled (0, 2, 2)

			ar1[1, 1] := 1
			ar1[1, 2] := 2
			ar1[2, 1] := 3
			ar1[2, 2] := 4

			ar2[1, 1] := 8
			ar2[2, 2] := 5

			m.to_string (m.prod (ar1, ar2))


		end


end
