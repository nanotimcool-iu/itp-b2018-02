note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER

create
	make

feature
	make
		do

		end

	add(m1,m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
		require
			same_size: m1.width = m2.width and m1.height = m2.height
		local
			i, j : INTEGER
		do
			create Result.make_filled (0, m1.height, m1.width)
			from
				i := 1
			until
				i = m1.height + 1
			loop
				from
					j := 1
				until
					j = m1.width + 1
				loop
					Result[i, j] := m1[i, j] + m2[i, j]
					j := j + 1
				end
				i := i + 1
			end
		end

	minus (m1,m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
		require
			same_size: m1.width = m2.width and m1.height = m2.height
		local
			i, j : INTEGER
		do
			create Result.make_filled (0, m1.height, m1.width)
			from
				i := 1
			until
				i = m1.height + 1
			loop
				from
					j := 1
				until
					j = m1.width + 1
				loop
					Result[i, j] := m1[i, j] - m2[i, j]
					j := j + 1
				end
				i := i + 1
			end
		end

	prod(m1,m2:ARRAY2[INTEGER]): ARRAY2[INTEGER]
		require
			m1.width = m2.height
		local
			i, j, k, s: INTEGER
		do
			create Result.make_filled (1, m1.height, m2.width)
			from
				i := 1
			until
				i = Result.height + 1
			loop
				from
					j := 1
				until
					j = Result.width + 1
				loop
					s := 0
					from
						k := 1
					until
						k = m1.width + 1
					loop
						s := s +  m1[i, k] * m2[k, j]
						k := k + 1
					end
					Result[i, j] := s
					j := j + 1
				end
				i := i + 1
			end
		end

	det (m:ARRAY2[INTEGER]): INTEGER
		require
			right_size : m.width = m.height
		local
			i, j, k, t : INTEGER
			s_m : ARRAY2[INTEGER]
		do
			Result := 0
			if m.width = 1 then
				Result := m[1, 1]
			else
				from
					i := 1
				until
					i = m.width + 1
				loop

					create s_m.make_filled (0, m.height - 1, m.width - 1)
					from
						j := 2
					until
						j = m.height + 1
					loop
						t := 0
						from
							k := 1
						until
							k = m.width + 1
						loop
							if k = i then
								t := 1
							else
								s_m[j - 1, k - t] := m[j, k]
							end
							k := k + 1
						end
						j := j + 1

					end
					if i \\ 2 = 0 then
						Result := Result + -m[1, i] * det(s_m)
					else
						Result := Result + m[1, i] * det(s_m)
					end
					i := i + 1
				end
			end
		end


	to_string(ar : ARRAY2[INTEGER])
		local
			i, j : INTEGER
		do
			from
				i := 1
			until
				i = ar.height + 1
			loop
				from
					j := 1
				until
					j = ar.width + 1
				loop
					print(ar[i, j].out + "  ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
			print("%N%N%N")
		end




end
