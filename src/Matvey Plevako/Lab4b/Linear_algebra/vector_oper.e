note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER

create
	make

feature

	make
		do
		end

	cross_product(v1,v2:ARRAY[INTEGER]): ARRAY[INTEGER]
		require
			right_len : v1.count = 3 and v2.count = 2
		do
			create Result.make_filled(0, 1, 3)
			Result[1] := v1[2] * v2[3] - v1[3] * v2[2]
			Result[2] := v1[1] * v2[3] - v1[3] * v2[1]
			Result[3] := v1[1] * v2[2] - v1[2] * v2[1]
		end

	dot_product(v1,v2:ARRAY[INTEGER]):INTEGER
		require
		  equlal : v1.count = v2.count
		local
			i : INTEGER
		do
			Result := 0
			from
				i := 1
			until
				i = v1.count + 1
			loop
				Result := Result + v1[i] * v2[i]
			end
		end

	triangle_area(v1,v2:ARRAY[INTEGER]):INTEGER
		local
			ar : ARRAY[INTEGER]
			s : DOUBLE
		do
			ar := cross_product(v1, v2)
			s := ((ar[1] ^ 2 + ar[2] ^ 2 + ar[3] ^ 3) ^ 0.5) / 2
			Result := s.truncated_to_integer
		end

end
