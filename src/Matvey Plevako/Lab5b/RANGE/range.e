note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	make

feature
	left, right : INTEGER

	make(left_, right_ : INTEGER)
		do
			left := left_
			right := right_
		end

	is_equal_range(other : like Current) : BOOLEAN
		do
			Result := left = other.left and right = other.right
		end

	is_empty:BOOLEAN
		do
			Result := left > right
		end

	is_sub_range_of(other : like Current):BOOLEAN
		do
			Result := is_empty or left >= other.left and right <= other.right
		end

	is_super_range_of(other: like Current):BOOLEAN
		do
			Result := other.is_empty or other.is_sub_range_of(Current)
		end

	left_overlaps (other: like Current): BOOLEAN
		do
			Result := other.left <= left and other.right >= left
		end

	right_overlaps (other: like Current): BOOLEAN
		do
			Result := other.left <= right and other.right >= right
		end

	overlaps (other: like Current): BOOLEAN
		do
			Result := left_overlaps (other) or right_overlaps (other) or other.left_overlaps (Current) or other.right_overlaps (Current)
		end


	add (other: like Current): RANGE
		require
			can_add: other.is_empty or is_empty or (other.left + 1 <= right and other.right + 1 >= left or left + 1 <= other.right and right + 1 >= other.left)
		do
			if is_empty then
				create Result.make(other.left, other.right)
			elseif other.is_empty then
				create Result.make(Current.left, Current.right)
			else
				create Result.make (left.min (other.left), right.max (other.right))
			end
		end

	subtract (other: like Current): RANGE
		require
			not_inner: not (other.left > left and other.right < right)
		do
			if other.left_overlaps (Current) then
				create Result.make (left, right.min (other.left - 1))
			elseif other.right_overlaps (Current) then
				create Result.make (left.max (other.right + 1), right)
			elseif is_sub_range_of(other) then
				create Result.make (1, 0)
			else
				create Result.make (left, right)
			end
		end
end
