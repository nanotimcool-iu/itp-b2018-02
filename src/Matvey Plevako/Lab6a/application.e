note
	description: "lab6 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
		c : FIB
		reverse : REVERSE_STRING
		lcs : LCS
		s, s1, s2 : STRING
		m_s : MERGER_SORT
		ar : ARRAY[INTEGER]
		i : INTEGER

	make
			-- Run application.
		do
			--| Add your code here
			print ("Hello Eiffel World!%N")
			create c.make
			create reverse.make
			create lcs.make
			create m_s.make
			create ar.make_filled (1, 1, 10)
			print(c.i_th (14).out + "%N")
			s := "ABCDEFG"
			print(reverse.reverse(s) + "%N")
			print(reverse.iterative (s) + "%N")

			s1 := "ABCFG"
			s2 := "ABDDDDDBCFDDDDDD"


			print(lcs.find(s1, s2) + "%N")

			from
				i := 1
			until
				i = 11
			loop
				ar[i] := 20 - i
				i := i + 1
				print(ar[i-1].out + " ")
			end
			print("%N")

			ar := m_s.sort(ar)
			print("%NRESULT:%N")
			from
				i := 1
			until
				i = ar.count + 1
			loop
				print(ar[i].out + " ")
				i := i + 1
			end
			print("%N")
		end

end
