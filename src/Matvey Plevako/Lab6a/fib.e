note
	description: "Summary description for {FIB}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	FIB

create
	make

feature
	make
		do
		end

	i_th(f : INTEGER) : INTEGER --returns i_th fib number
		do
			if f < 3 then
				Result := 1
			else
				Result := i_th(f - 1) + i_th(f - 2)
			end

		end

end
