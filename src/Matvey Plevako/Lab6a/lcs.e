note
	description: "Summary description for {LCS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LCS

create
	make

feature
	make
		do
			s1 := ""
			s2 := ""
		end

	s1, s2 : STRING

	find(s1_, s2_ : STRING) : STRING
		do
			s1 := s1_
			s2 := s2_
			Result := rec(s1.count, s2.count)
		end

	rec(i, j : INTEGER) : STRING
		local
			left, right : STRING
		do
			Result := ""
			if i /= 0 and j /= 0 then
				if s1[i] = s2[j] then
					Result := rec(i - 1 , j - 1) + s1[i].out
				else
					left := rec(i, j - 1)
					right := rec(i - 1, j)
					if left.count > right.count then
						Result := left
					else
						Result := right
					end
				end
			end
		end

end
