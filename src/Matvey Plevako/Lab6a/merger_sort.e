note
	description: "Summary description for {MERGER_SORT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MERGER_SORT

create
	make

feature
	make
		do
		end

	sort(list : ARRAY[INTEGER]) : ARRAY[INTEGER]
		local
			i, j, n, p, f: INTEGER
			left, right, to_fill : ARRAY[INTEGER]
		do
			n := list.count
			if n < 2 then
				Result := list
			else
				create left.make_filled (1, 1, n // 2)
				create right.make_filled (1, 1, n - n // 2)
				left.subcopy (list, 1, n // 2, 1)
				right.subcopy(list, n // 2 + 1, n, 1)
				left := sort(left)
				right := sort(right)

				create Result.make_filled (1, 1, n)
				p := 1
				from
					i := 1
					j := 1
				until
					i > left.count or j > right.count
				loop
					if left[i] > right[j] then
						Result[p] := right[j]
						j := j + 1
					else
						Result[p] := left[i]
						i := i + 1
					end
					p := p + 1
				end
				if i <= left.count then
					to_fill := left
					f := i
				else
					to_fill := right
					f := j
				end
				from
					p := p
				until
					p = n + 1
				loop
					Result[p] := to_fill[f]
					f := f + 1
					p := p + 1
				end
			end
		end

	print_l(l : ARRAY[INTEGER])
		local
			i : INTEGER
		do
			from
				i := 1
			until
				i = l.count + 1
			loop
				print(l[i].out + " ")
				i := i + 1
			end
			print("%N%N")
		end

end
