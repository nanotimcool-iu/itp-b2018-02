note
	description: "Summary description for {REVERSE_STRING}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	REVERSE_STRING

create
	make

feature
	make
		do
		end

	reverse(s : STRING) : STRING
		do
			if s.count = 1 then
				Result := s
			else
				Result := s[s.count].out + reverse(s.substring (1, s.count - 1))
			end
		end

	iterative(s : STRING) : STRING
		local
			i : INTEGER
		do
			Result := ""
			from
				i := s.count
			until
				i = 0
			loop
				Result.append_character(s[i])
				i := i - 1
			end
		end

end
