note
	description: "HANOI application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			n : INTEGER
			-- Run application.
		do
			--| Add your code here
			print ("Hello Eiffel World!%N")
			n := 4
			tower_of_Hanoi(n, 'A', 'C', 'B')
		end

	tower_of_Hanoi(n : INTEGER; from_rod, to_rod, aux_rod : CHARACTER)
		do
			if (n = 1) then
				print("%N Move disk 1 from rod " + from_rod.out  + " to rod " + to_rod.out)
			else
				tower_of_Hanoi(n-1, from_rod, aux_rod, to_rod)
	    		print("%N Move disk " + n.out + " from rod " + from_rod.out + " to rod " + to_rod.out)
	    		tower_of_Hanoi(n-1, aux_rod, to_rod, from_rod)
			end

		end

end
