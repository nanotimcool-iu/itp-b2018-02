note
	description: "POWER_SET application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			--| Add your code here
			print ("Hello Eiffel World!%N")
		end


	power_set (s: LINKED_SET [INTEGER]): LINKED_SET [LINKED_SET [INTEGER]]
		local
			s1: LINKED_SET [INTEGER]
			x: INTEGER
		do
			create Result.make
			if not s.is_empty then
				s1 := s.twin
				x := s [1]
				s1.prune (x)
				Result.append (power_set (s1))
				across
					power_set (s1) as s2
				loop
					s2.item.put (x)
					Result.put (s2.item)
				end
			else
				Result.put (create {LINKED_SET [INTEGER]}.make)
			end
		end
end
