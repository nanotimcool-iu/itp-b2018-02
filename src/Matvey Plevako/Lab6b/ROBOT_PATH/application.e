class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		do
			map := <<<<0, 0, 0, 0, 0>>,
					 <<1, 0, 0, 0, 0>>,
					 <<0, 1, 1, 0, 1>>,
					 <<0, 0, 0, 0, 1>>,
					 <<0, 0, 0, 0, 0>>>>
			row := 5
			column := 5
			rec_print (1, find_path (1, 1))

		end

	row, column: INTEGER
	map: ARRAY [ARRAY [INTEGER]]

	find_path (cur_row, cur_column: INTEGER): ARRAY [STRING]
		local
			a_right, a_down: ARRAY [STRING]
		do
			if cur_row > row or else cur_column > column or else map [cur_row].item (cur_column) = 1 then
				Result := <<>>
			elseif cur_row = row and cur_column = column then
				Result := <<" END">>
			else
				a_right := find_path (cur_row + 1, cur_column)
				a_down := find_path (cur_row, cur_column + 1)
				if a_down.count /= 0 and (a_right.count = 0 or a_down.count < a_right.count) then
					a_down.force ("RIGHT%N", a_down.count + 1)
					Result := a_down
				elseif a_right.count /= 0 and (a_down.count = 0 or a_right.count < a_down.count) then
					a_right.force ("DOWN%N", a_right.count + 1)
					Result := a_right
				else
					Result := <<>>
				end
			end
		end

	rec_print (i: INTEGER; a: ARRAY [STRING])
		do
			if i /= a.count then
				rec_print (i + 1, a)
			end
			print (a [i])

		end



end
