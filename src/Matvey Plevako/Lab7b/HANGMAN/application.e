note
	description: "HANGMAN application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	make
		local
			word : STRING
			number_of_players, guesses : INTEGER
		do
			print ("Hello Eiffel World!%N")
			print("Enter number of players:%N")
			io.read_integer
			number_of_players := io.last_integer

			print("Enter number of guesses:%N")
			io.read_integer
			guesses := io.last_integer

			word := get_word("dictionary.txt", number_of_players)
			start_game(word, number_of_players, guesses)
		end

feature

	start_game(word : STRING; number_of_players, guesses : INTEGER)
		local
			i, wrong, c_player : INTEGER
			guessed : LINKED_LIST[CHARACTER]
			c : CHARACTER
			all_ : BOOLEAN
		do
			c_player := 0
			wrong := 0
			create guessed.make
			all_ := display_word(word, guessed)

			from
				i := 1
			until
				i = 2
			loop
				print("Now ")
				print(c_player + 1)
				print(" guesses a letter%N")
				print("Enter a character: ")
				io.read_character
				print("%N")
				c := io.last_character.as_lower
				io.next_line

				if guessed.has(c) then
					print("Already was%N")
				else
					if word.has(c) then
						print("%NCorrect%N%N")
						guessed.extend(c)
					else
						print("%NWrong%N")
						print("Guesses left: ")
						wrong := wrong + 1
						print(guesses - wrong)
						print("%N")
						if wrong = guesses then
							print("You lose%N")
							print("Word: ")
							print(word)
							print("%NYour progress: ")
							i := 2
						else
							c_player := (c_player + 1) \\ number_of_players
						end
					end
				end
				all_ := display_word(word, guessed)
				if all_ then
					print(c_player + 1)
					print(" won!%N")
					i := 2
				end
			end
		end

	display_word(word : STRING; guessed : LINKED_LIST[CHARACTER]) : BOOLEAN
		local
			i, j : INTEGER
			has : BOOLEAN
		do
			Result := true
			from
				i := 1
			until
				i = word.count + 1
			loop
				has := false
				from
					j := 1
				until
					j = guessed.count + 1
				loop
					if word[i] = guessed[j] then
						print(word[i])
						has := true
					end
					j := j + 1
				end
				i := i + 1
				if not has then
					print("_")
					Result := false
				end
			end
			print("%N")

		end

	get_word(a_path : STRING; len: INTEGER) : STRING
		local
		  l_file: PLAIN_TEXT_FILE
		  rand : RANDOM
		  words : LINKED_LIST[STRING]
		  line : STRING
		  p, c : INTEGER
		  l_time: TIME
      	  l_seed: INTEGER
		do
			create words.make
			Result := ""
		    create l_file.make_open_read (a_path)
		    -- We perform several checks until we make a real attempt to open the file.
		    if not l_file.exists then
		      print ("error: '" + a_path + "' does not exist%N")
		    else
		      if not l_file.file_readable then
		        print ("error: '" + a_path + "' is not readable.%N")
		      else
		    	l_file.read_line
		    	from
		    		line := l_file.last_string.twin
		    	until
		    		l_file.exhausted
		    	loop
		    		words.extend(line)
		    		l_file.read_line
		    		line := l_file.last_string.twin
		    	end
		        l_file.close

			    create l_time.make_now
		        l_seed := l_time.hour
		        l_seed := l_seed * 60 + l_time.minute
		        l_seed := l_seed * 60 + l_time.second
		        l_seed := l_seed * 1000 + l_time.milli_second
		        create rand.set_seed (l_seed)
				rand.forth
				p := rand.item
				print("%N")
				c := (p \\ words.count) + 1
		        Result := words[c]
		        Result.to_lower

		      end
		    end
		    if Result.count < len then
		    	print("short word, generating new word...%N")
		    	Result := get_word(a_path, len)
		    	Result.to_lower
		    end
		end

end
