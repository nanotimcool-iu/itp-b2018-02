note
	description: "BIN_TREE_G application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	b1, b2, b3, b4, b5: BIN_TREE[STRING]

	make
		do
			create b1.make ("A")
			create b2.make ("B")
			create b3.make ("C")
			create b4.make ("D")
			create b5.make ("E")

			b1.add (b2)
			b1.add (b3)
			b1.add (b4)
			b1.add (b5)


			print(b1.height)
			print("%N")
			print(b2.height)
			print("%N")
		end

end
