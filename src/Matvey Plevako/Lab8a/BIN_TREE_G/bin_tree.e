note
	description: "Summary description for {BIN_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIN_TREE[G]

create
	make

feature
	info : detachable G
	left, right : detachable BIN_TREE[G]
	height : INTEGER

	make(value: G)
		do
			info := value
			height := 0
		end

	add(t : BIN_TREE[G])
		do
			if left = Void then
				left := t.twin
				if attached right as right1 then
					height := right1.height.max(t.height)
				else
					height := t.height
				end

			elseif right = Void then
				right := t.twin
				if attached left as left1 then
					height := left1.height.max(t.height)
				else
					height := t.height
				end
			else
				if attached left as left1 and attached right as right1 then
					if left1.height.is_less(right1.height) then
						left1.add(t)
					else
						right1.add (t)
					end
					height := left1.height.max (right1.height)
				end
			end
			height := height + 1
		end



end
