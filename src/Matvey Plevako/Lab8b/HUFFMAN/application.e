note
	description: "HUFFMAN application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	h : HUFFMAN
	res : HASH_TABLE[STRING, CHARACTER]
	make
			-- Run application.
		do
			create h.make
			res := h.encode ("abcdefghijklmnopqrstuvwxyz")
			print("%N")



			from
				res.start
			until
				res.off
			loop
				print(res.key_for_iteration)
				print(": ")
				print(res.item_for_iteration)
				print("%N")
				res.forth
			end

			print(h.decode ("1001110100", res))
			--print(h.freq[1].value)
		end

end
