note
	description: "Summary description for {HUFFMAN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN

create
	make

feature
		make
			do
			end



feature {NONE}
	get_freq(word: STRING):LINKED_LIST[NODE]
		local
			i : INTEGER
			hash_table : HASH_TABLE [INTEGER, CHARACTER]
		do
			create hash_table.make(word.count)
			from
				i := 1
			until
				i = word.count + 1
			loop
				if not word[i].is_space then
					hash_table[word[i]] := hash_table[word.item (i)] + 1
				end
				i := i + 1
			end

			create Result.make

			from
				hash_table.start
			until
				hash_table.off
			loop

				Result.extend(create {NODE}.make (hash_table.key_for_iteration, hash_table.item_for_iteration))
				hash_table.forth
			end
		end

	build_tree(freq: LINKED_LIST[NODE]): NODE
		local
			min1, min2 : NODE
			n : NODE
		do
			from
			until
				freq.count = 1
			loop
				create min1.make (' ', {INTEGER}.max_value)
				create min2.make (' ', {INTEGER}.max_value)
				from
					freq.start
				until
					freq.exhausted
				loop
					if min1.value > freq.item.value then
						min2 := min1
						min1 := freq.item
					elseif min2.value > freq.item.value then
						min2 := freq.item
					end
					freq.forth
				end
				create n.make (' ', min1.value + min2.value)
				n.connect (min1, min2)
				freq.extend (n)
				freq.start
				freq.prune(min1)
				freq.start
				freq.prune(min2)
			end
			freq.start
			Result := freq.item
		end

	dfs(vertex : NODE; len : INTEGER; path: STRING) : HASH_TABLE[STRING, CHARACTER]
		do
			print(vertex.letter)
			create Result.make(len)
			if attached vertex.right as right and attached vertex.left as left then
				Result.merge(dfs(left, len, path + "0"))
				Result.merge(dfs(right, len, path + "1"))
			else
				Result[vertex.letter] := path.twin
			end
		end

feature

	encode(word_: STRING) : HASH_TABLE[STRING, CHARACTER]
		local
			freq : LINKED_LIST[NODE]
			vertex : NODE
			word : STRING
		do
			word := word_
			freq := get_freq(word)
			vertex := build_tree(freq.twin)

			Result := dfs(vertex, freq.count, "")

			from
				freq.start
			until
				freq.exhausted
			loop
				print(freq.item.letter)
				print(":")
				print(freq.item.value)
				print(", ")
				freq.forth
			end
			print("%N")
		end

	decode(code : STRING; codes: HASH_TABLE[STRING, CHARACTER]) : STRING
		local
			st, nd: INTEGER
			code_to_char : HASH_TABLE[CHARACTER, STRING]
		do
			create code_to_char.make(codes.count)
			from
				codes.start
			until
				codes.off
			loop
				code_to_char[codes.item_for_iteration] := codes.key_for_iteration
				codes.forth
			end
			st := 1
			Result := ""
			from
				nd := 1
			until
				nd = code.count + 1
			loop
				if code_to_char.has(code.substring(st, nd)) then
					Result := Result + code_to_char[code.substring (st, nd)].out
					st := nd + 1
				end
				nd := nd + 1
			end
		end


end
