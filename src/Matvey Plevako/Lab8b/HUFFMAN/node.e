note
	description: "Summary description for {NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NODE

create
	make

feature
	letter : CHARACTER
	value : INTEGER
	left, right, conneted_to : detachable NODE
	make(c : CHARACTER; p : INTEGER)
		do
			letter := c
			value := p
		end

	connect(left_, right_ : NODE)
		do
			left := left_
			right := right_
		end


end
