note
	description: "CIPHER application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
		m, key, d_m, u_m: STRING
		v : VIGENERE_CIPHER
		s : SPIRAL_CIPHER
		c : COMBINED_CIPHER
		list : LINKED_LIST[CIPHER]

	make
			-- Run application.
		do
			--| Add your code here
			create v
			create s
			create list.make
			list.extend (v)
			list.extend (s)
			create c.make (list)
			m := "STUDENTS, SOLVE THE ASSIGNMENT WELL AND FAST!"
			key := "TIGER"
			d_m := v.encrypt (m, key)
			print(d_m)
			print("%N")
			u_m := v.decrypt (d_m, key)
			print(u_m)
			print("%N")
			d_m := c.encrypt ("MYLASTASSIGNMENT", "BUSY")
			print(d_m)
			print("%N")
			m := "LAGHYH  QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF FAZ2/TGBZKFI REE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIUL ZIE KFYGEL//:RLH DXE Z"
			print(c.decrypt (m, d_m))
			print("%N")
		end

end
