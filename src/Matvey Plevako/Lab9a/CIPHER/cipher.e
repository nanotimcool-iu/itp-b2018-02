note
	description: "Summary description for {CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	CIPHER

feature
	encrypt(mes, key: STRING) : STRING
		deferred
		end

	decrypt(mes, key: STRING) : STRING
		deferred
		end

end
