note
	description: "Summary description for {COMBINED_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMBINED_CIPHER

inherit
	CIPHER

create
	make

feature
	ciphers: LINKED_LIST[CIPHER]

	make(ciphers_ : LINKED_LIST[CIPHER])
		do
			ciphers := ciphers_
		end

feature
	encrypt(mes, key : STRING) : STRING
		do
			Result := mes
			from
				ciphers.start
			until
				ciphers.exhausted
			loop
				Result := ciphers.item.encrypt (Result, key)
				ciphers.forth
			end
		end

	decrypt(mes, key : STRING) : STRING
		do
			Result := mes
			from
				ciphers.finish
			until
				ciphers.exhausted
			loop
				Result := ciphers.item.decrypt (Result, key)
				ciphers.back
			end
		end

end
