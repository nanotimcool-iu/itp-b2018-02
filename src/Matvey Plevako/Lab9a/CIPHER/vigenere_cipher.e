note
	description: "Summary description for {VIGENERE_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHER

inherit
	CIPHER

feature
	encrypt(mes, key : STRING) : STRING
		local
			i, j : INTEGER
		do
			print("%N")
			Result := ""
			from
				i := 1
				j := 0
			until
				i = mes.count + 1
			loop
				if (mes.item_code(i)) - 65 < 26 and (mes.item_code(i)) - 65 >= 0 then
					Result.extend(((((key.item_code((j \\ key.count) + 1) - 65 + mes.item_code(i) - 65)) \\ 26) + 65).to_character_8)
					j := j + 1
				else
					Result.extend (mes.item_code(i).to_character_8)
				end
				i := i + 1
			end
		end

	decrypt(mes, key: STRING) : STRING
		local
			i, j : INTEGER
			k_c, m_c : INTEGER
		do
			Result := ""
			from
				i := 1
				j := 0
			until
				i = mes.count + 1
			loop
				if (mes.item_code(i)) - 65 < 26 and (mes.item_code(i)) - 65 >= 0 then
					k_c := key.item_code((j \\ key.count) + 1) - 65
					m_c := mes.item_code(i) - 65
					if m_c - k_c < 0 then
						m_c := m_c + 26
					end
					Result.extend(((m_c - k_c) + 65).to_character_8)
					j := j + 1
				else
					Result.extend (mes.item_code(i).to_character_8)
				end
				i := i + 1
			end
		end
end
