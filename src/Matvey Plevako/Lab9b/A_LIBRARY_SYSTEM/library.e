class
	LIBRARY

create
	make

feature {NONE}

	make
		do
			create users.make (0)
			create books.make (0)
		end

	users: ARRAYED_LIST [PERSON]

	books: ARRAYED_LIST [BOOK]

feature

	add_book (title_s: STRING; for_child, is_bestseller: BOOLEAN; ISBN, price: INTEGER)
		local
			b: BOOK
		do
			create b.make (title_s, for_child, is_bestseller, ISBN, price)
			books.extend (b)
		end

	give_book (ISBN: INTEGER; person: PERSON)
		require
			person_exists: person /= VOID
			pos_ISBN: ISBN > 0
		local
			t: BOOK
		do
			across
				books as book
			loop
				if book.item.get_ISBN = ISBN then
					t := book.item.twin
					t.set_expire
					person.add_book (book.item)
				end
			end
		end

end
