note
	description: "MY_STACK application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			stack:MY_STACK[STRING]
			bounded_stack:MY_BOUNDED_STACK[INTEGER]
		do
			create stack.make
			create bounded_stack.make
			bounded_stack.set_capacity (2)
			io.put_boolean (stack.is_empty)
			print("%N")
			stack.push ("elem")
			print(stack.item + "%N")
			stack.remove
			io.put_boolean (stack.is_empty)
			bounded_stack.push (1)
			bounded_stack.push (100)
			--bounded_stack.push (-123)
		end

end
