note
	description: "Summary description for {MY_BOUNDED_STACK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_BOUNDED_STACK[G]

inherit
	MY_STACK[G]

	redefine
		push,
		make
	end

create
	make


feature

	capacity : INTEGER
	actual_len : INTEGER

	make
		do
			create stack.make
			capacity := 0
			actual_len := 0
		end

	set_capacity(new : INTEGER)
		do
			capacity := new
		end

	is_enought_space
		require
			actual_len < capacity
		do
		end


	push(elem : G)
		do
			is_enought_space
			actual_len := actual_len + 1
		end

end
