note
	description: "Summary description for {MY_STACK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class MY_STACK [G]

create
	make

feature
	stack : LINKED_LIST[G]

	make
		do
			create stack.make
		end

	push(elem : G)
		do
			stack.extend(elem)
		end

	remove
		do
			stack.finish
			stack.remove
		end

	item : G
		do
			stack.finish
			Result := stack.item
		end

	is_empty : BOOLEAN
		do
			Result := stack.is_empty
		end

end
