note
	description: "Summary description for {PRODUCT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PRODUCT

feature

	public_price : REAL
	price : REAL
	description : STRING
	deferred
	end

feature

	set_price_public(i_price : REAL)
	do
		public_price := i_price
	end

	set_price(i_price : REAL)
	do
		price := i_price
	end

	get_profit : REAL
	do
		Result := public_price - price
	end
end
