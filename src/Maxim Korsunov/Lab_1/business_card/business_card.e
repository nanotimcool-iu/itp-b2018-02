class
	BUSINESS_CARD

create
	fill_in

feature {NONE} -- Initialization

	fill_in
		do
			Io.put_string ("Your name:")
			Io.readline
			Io.new_line
			set_name(Io.last_string)

			Io.put_string ("Your age:")
			Io.read_integer
			Io.new_line
			set_age(Io.last_integer)

			Io.put_string ("Your job:")
			Io.readline
			Io.new_line
			set_job(Io.last_string)

			Io.put_string (print_card)


		end

feature -- Access

	name: STRING
			-- Owner's name.

	job: STRING
			-- Owner's job.

	age: INTEGER
			-- Owner's age.

feature -- Setting

	set_name (a_name: STRING)
			-- Set `name' to `a_name'.
		require
			name_exists: a_name /= Void
		do
			name := a_name.twin
		end

	set_job (a_job: STRING)
			-- Set `job' to `a_job'.
		require
			job_exists: a_job /= Void
		do
			job := a_job.twin
		end

	set_age (a_age: INTEGER)
			-- Set `age' to `a_age'.
		require
			age_non_negative: a_age >= 0
		do
			age := a_age
		end

feature
	print_card: STRING
		do
			Result := "|" + line(width) + "|"
			 + "%N| Your name: " + name.out + space(width - name.count - 12) +
			"|%N| Your age: " + age.out + space(width - 2 - 11) +
			"|%N| Your job: " + job.out + space(width - job.count - 11) +
			"|%N|" + line(width) + "|"
		end

	width: INTEGER
		do
			Result := name.count + 25
		end
	line (n: INTEGER): STRING
		do
			Result := "-"
			Result.multiply (n)
		end
	space (n: INTEGER): STRING
		do
			Result := " "
			Result.multiply (n)
		end

end
