note
	description: "shop1 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APP

create
	make

feature
	coffee: COFFEE
	espresso: ESPRESSO
	cappuccino: CAPPUCCINO
	cake: CAKE
	list: PRODUCTS

feature

	make
		do
			create coffee.make(100, 250, "Perfect coffee. Don't know the kind")
			create espresso.make(200, 500, "The best of all - eXpresso")
			create cappuccino.make(30, 80, "Not so good, but you can try")
			create cake.make(70, 130, "Absolutely great! With pineapple!")
			create list.make
			list.add(coffee)
			list.add(espresso)
			list.add(cappuccino)
			list.add(cake)
			print("%NPRODUCTS%N______________%N")

			coffee.print_menu
			espresso.print_menu
			cappuccino.print_menu
			cake.print_menu

		end

end
