note
	description: "Summary description for {CAKE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAKE
inherit
	ITEM
	redefine
		print_menu
	end
create
	make

feature
	print_menu
	do
		print("%NTry this cake::%N" + description)
	end

end
