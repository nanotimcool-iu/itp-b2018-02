note
	description: "Summary description for {CAPPUCCINO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAPPUCCINO
inherit
	COFFEE
	redefine
		print_menu
	end
create
	make

feature
	print_menu
	do
		print("%NCappucino:%N" + description)
	end

end
