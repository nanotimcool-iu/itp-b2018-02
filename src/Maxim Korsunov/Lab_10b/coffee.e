note
	description: "Summary description for {COFFEE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COFFEE
inherit
	ITEM
	redefine
		print_menu
	end
create
	make

feature
	print_menu
	do
		print("%NThe awesome coffee:%N" + description)
	end

end
