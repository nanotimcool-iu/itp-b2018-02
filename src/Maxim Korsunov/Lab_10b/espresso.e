note
	description: "Summary description for {ESPRESSO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ESPRESSO
inherit
	COFFEE
	redefine
		print_menu
	end
create
	make

feature
	print_menu
	do
		print("%NEespresso:%N" + description)
	end

end
