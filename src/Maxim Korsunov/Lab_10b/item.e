note
	description: "Summary description for {ITEM}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ITEM
create
	make

feature
	make(price1, price_public1:INTEGER; description1: STRING)
	do
		set_price(price1)
		price_public := price_public1
		description := description1
		profit := price_public1 - price1
	end

feature {ITEM, APP}
	price: REAL

feature
	price_public: REAL
	profit: REAL
	description: STRING
	set_price(the_price: INTEGER)
	do
		price := the_price
	end
	set_price_public(the_price:INTEGER)
	do
		price_public := the_price
	end
	print_menu
	do
		print("%NSOME PRODUCTS%N")
	end

end
