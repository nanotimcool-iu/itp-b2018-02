note
	description: "Summary description for {PRODUCTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PRODUCTS
create
	make
feature
	arr: ARRAYED_LIST[ITEM]
feature
	make
	do
		create arr.make(5)
	end
	add(item: ITEM)
	do
		arr.extend(item)
	end
	--get_item(index: INTEGER): ITEM
	--do
	--	Result := arr.item (index)
	--end
	count: INTEGER
	do
		Result := arr.count
	end
end
