note
	description: "Leap application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION


create
	make

feature {NONE} 

	make
		local m: INTEGER
		do
			print ("Input a year: ")
			io.readint
			m := io.lastint
			print ("Is year a leap?: ")
			io.put_boolean (not ((m.integer_remainder (4) /= 0) or else (m.integer_remainder (100) = 0 and m.integer_remainder (400) /= 0)))
		end
end
