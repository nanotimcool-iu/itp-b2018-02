note
	description: "ScienceFair application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APP

create
	make

feature {NONE}

	make
		local
			l: LAB
			b: BIOLOGIST
			bi: BIO_INFORMATIC
		do
		create l.make
		create b.make ("Sasha", "Keen on anatomy", 1)
		b.set_pet_name ("GAV")
		create bi.make("Petr I", "Loves biology and CS", 5)
		bi.set_pet_name ("STORM")
		l.add_member (create {COMPUTER_SCIENTIST}.make ("Ivan", "Some person", 0))
		l.add_member (b)
		l.add_member (bi)

		l.introduce_all
		end

end
