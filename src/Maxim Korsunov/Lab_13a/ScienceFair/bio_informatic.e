note
	description: "Summary description for {BIO_INFORMATIC}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIO_INFORMATIC

inherit
	BIOLOGIST
	redefine
		make
	select
		make
	end

	COMPUTER_SCIENTIST
	rename
		make as make_cs
	undefine
		introduce
	end

create
	make

feature {NONE}

	make (name_a, bio: STRING; i: INTEGER)
		do
			precursor{BIOLOGIST} (name_a, bio, i)
			discipline := "Biology&CS"
			pet_name := ""
		end
feature

end
