note
	description: "Summary description for {BIOLOGIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIOLOGIST

inherit
	SCIENTIST
	redefine
		introduce,
		make
	end

create
	make

feature {NONE}
	pet_name:STRING
	make (name_a, bio: STRING; i: INTEGER)
		do
			precursor (name_a, bio, i)
			discipline := "Biology"
			pet_name := ""
		end
feature
	set_pet_name(pname:STRING)
	require not pname.is_empty
	do
		pet_name := pname
	end

	get_pet_name:STRING
	do
		Result := pet_name
	end

	introduce
	do
		precursor
		print("%T" + pet_name + "%N")
	end

end
