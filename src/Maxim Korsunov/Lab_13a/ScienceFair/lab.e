note
	description: "Summary description for {LAB}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LAB

create
	make

feature {NONE}
	people:ARRAYED_LIST[SCIENTIST]

	make
	do
		create people.make(0)
	end
	
feature
	add_member(s:SCIENTIST)
	do
		people.extend (s)
	end

	remove_member(s:SCIENTIST)
	do
		people.prune_all (s)
	end

	introduce_all
	do
		across
			people as p
		loop
			p.item.introduce
		end
	end

end
