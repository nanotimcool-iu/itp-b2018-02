note
	description: "Summary description for {SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SCIENTIST

feature {NONE}

	name, biography, discipline: STRING

	id: INTEGER

	make (name_a, bio: STRING; i: INTEGER)
		require
			non_void: name_a /= Void and then bio /= Void and then not (name_a.is_empty or else bio.is_empty)
		do
			name := name_a
			biography := bio
			id := i
		end
feature
	introduce
		do
			print (id.out + "%T" + name + "%T" + discipline)
		end

end
