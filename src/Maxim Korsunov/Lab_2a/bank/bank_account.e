note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

inherit
	ARGUMENTS_32

create
	make

feature

	name: STRING
	balance: INTEGER

	make
		do
			balance := 100
			name := "VanishMax"
			add(100)
			take(10)
			print(balance)
		end

	add (value: INTEGER)
		require
			positive_value: value >= 0
		do
			balance := balance + value
		end

	take (value: INTEGER)
		require
			positive_value: value >= 0
		do
			balance := balance - value
		end

invariant
	greater_than_100: balance >= 100
	less_than_1000000: balance <= 1000000

end
