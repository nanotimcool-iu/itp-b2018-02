note
	description: "university application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

create
	create_class

create
	make

feature {NONE}

	name: STRING
	id: INTEGER
	schedule: STRING
	max_students: INTEGER

	make
		do
			create_class("English for Academic purpose", 101, "Tuesday at 10:35", 164)
			print(Current)
		end

feature

	create_class (name1: STRING; id1: INTEGER; schedule1: STRING; max_students1: INTEGER)
		require
			min_students: max_students1 >= 3
		do
			id := id1
			name := name1
			schedule := schedule1
			max_students := max_students1
		end
end
