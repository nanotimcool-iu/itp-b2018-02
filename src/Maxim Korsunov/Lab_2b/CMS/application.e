note
	description: "CMS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE}

	make
		local
			c1, c2, c3: CONTACT
		do
			c1:=create_contact ("Mommy", 1234567890, "Teacher", "mommy@gmail.com")
			c2:=create_contact ("Daddy", 1234567890, "Police", "daddy@gmail.com")
			c3:=create_contact ("Kitten", 1234567890, "Mew-Mew", "kitten@gmail.com")
			add_emergency_contact (c1, c2)
			add_emergency_contact (c3, c2)
			edit_contact (c2, 2, "89000001122")
			edit_contact (c2, 4, "father@mail.ru")
			remove_emergency_contact (c2)
			remove_emergency_contact (c3)
			print (c1)
		end

feature

	create_contact (name: STRING; phone_num: INTEGER; work: STRING; email: STRING): CONTACT
		local
			c: CONTACT
		do
			create c.make (name, phone_num, work, email)
			Result := c
		end

	edit_contact(contact:CONTACT; key:INTEGER; val:STRING)
	do
		contact.edit(key, val)
	end

	add_emergency_contact(ac:CONTACT; emg:CONTACT)
	do
		ac.set_emergency (emg)
	end

	remove_emergency_contact(ac:CONTACT)
	do
		ac.set_emergency(ac)
	end
end
