
class
	CONTACT

create
	make

feature {NONE}

	make (name1:STRING; phone_num:INTEGER; work:STRING;
	email1:STRING;)
		do
			name := name1
			set_phone (phone_num)
			work_place := work
			email:= email1
			set_emergency (current)
		end
feature

	name:STRING
	phone_number:INTEGER
	work_place:STRING
	email:STRING
	call_emergency:CONTACT

	set_phone(new_phone:INTEGER)
	require
		new_phone.out.count = 10
	do
		phone_number := new_phone
	end

	set_emergency(new_emergency:CONTACT)
		do
			call_emergency := new_emergency
		end

	edit(key:INTEGER; val:STRING)
	require
		key >0
		key <5
	do
		inspect key
			when 1 then
				name:= val
			when 2 then
				set_phone (val.to_integer)
			when 3 then
				work_place := val
			when 4 then
				email := val
		end

	end
end
