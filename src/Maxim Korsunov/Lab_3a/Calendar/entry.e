note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	make

feature {NONE}

	make(a_place, a_subject: STRING; a_owner: PERSON; a_date: DATE_TIME; a_duration: DATE_TIME_DURATION)
		do
			place := a_place
			subject := a_subject
			owner := a_owner
			date := a_date
			duration := a_duration
		end


feature
	place: STRING
	subject: STRING
	owner: PERSON
	date: DATE_TIME
	duration: DATE_TIME_DURATION
	printable_entry:STRING
		do
			Result := "Where: " + place + "%N%T%Tsubject: " + subject + "%N%T%Towner: " + owner.name + "%N%T%Tdate: " + date.out +"%N%N"
		end

	set_place(new:STRING)
		do
			place := new
		end
	set_subject(new:STRING)
		do
			subject := new
		end
	set_date(new:DATE_TIME)
		do
			date := new
		end
	set_duration(new:DATE_TIME_DURATION)
		do
			duration := new
		end
end
