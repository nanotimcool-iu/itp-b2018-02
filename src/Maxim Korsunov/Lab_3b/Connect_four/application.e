note
	description: "chess application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE}

	make
		local
			board: BOARD
		do
			Io.put_string ("Write n: ")
			Io.read_integer
			n:= Io.last_integer
			Io.put_string ("Write m: ")
			Io.read_integer
			m:= Io.last_integer

			create board.doit(n, m)
			board.print_board

			play_game(board)

		end

feature
	n: INTEGER
	m: INTEGER

feature
	play_game(b : BOARD)
		local
			col: INTEGER
			i: INTEGER
		do
			from i := 1
			until b.check_win (1) or b.check_win (2)
			loop
				if i \\ 2 = 1 then
					print("Choose column, player 1: ")
					Io.read_integer
					col := Io.last_integer
					b.makestep (col, i \\ 2)
					if b.check_win (1) then
						print("PLAYER 1 WON, CONGRATULATIONS!!!")
					end
				else
					print("Choose column, player 2: ")
					Io.read_integer
					col := Io.last_integer
					b.makestep (col, i \\ 2)
					if b.check_win (2) then
						print("PLAYER 2 WON, CONGRATULATIONS!!!")
					end
				end
				i := i + 1

			end
		end

end
