note
	description: "Summary description for {BOARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOARD
create
	doit

feature {NONE}
	doit(n1, m1: INTEGER)
		do
			n:= n1
			m:= m1
			create arr.make_filled(0, n1, m1)
		end

feature
	n: INTEGER
	m: INTEGER
	arr: ARRAY2[INTEGER]

	line (n2: INTEGER): STRING
		do
			Result := "-"
			Result.multiply (n2)
		end

feature

	makeStep(col, player: INTEGER)
		local
			j: INTEGER
			c: INTEGER
		do
			c := 0
			from j := m
			until j = 0
			loop
				if arr.item (j, col) = 0 and c /= 1 then
					if player = 1 then
						arr.item (j, col) := 1
					else
						arr.item (j, col) := 2
					end

					c := c + 1
				end
				j := j - 1
			end
			print_board
		end


	print_board
		local
			st: STRING
			i: INTEGER
			k: INTEGER
		do
			st := "|" + line(n*4+3) + "|%N"
			print(st)
			from i := 1
			until i = m + 1
			loop
				print("|   ")
				from k := 1
				until k = n + 1
				loop
					print(arr.item (i, k).out + "   ")
					k := k + 1
				end
				print("|%N")
				i := i + 1
			end
			print(st)
		end

	check_win(pl:INTEGER) : BOOLEAN
		local
			i: INTEGER
			k: INTEGER
		do
			Result := False

			-- Horizontal check
			from i := m
			until i = 0
			loop
				from k := 1
				until k = n - 2
				loop
					if arr.item (i, k) = pl and  arr.item (i, k + 1) = pl and
					   arr.item (i, k + 2) = pl and  arr.item (i, k + 3) = pl then
						Result := True
					end
					k := k + 1
				end
				i := i - 1
			end

			-- Vertical check
			from k := 1
			until k = n + 1
			loop
				from i := m
				until i = 3
				loop
					if arr.item (i, k) = pl and arr.item (i - 1, k) = pl and
					   arr.item (i - 2, k) = pl and arr.item (i - 3, k) = pl  then
						Result := True
					end
					i := i - 1
				end
				k := k + 1
			end

			-- Diagonal 45 degree check
			from i := m
			until i = 3
			loop
				from k := 1
				until k = n - 2
				loop
					if arr.item(i, k) = pl and arr.item(i - 1, k + 1) = pl and
					   arr.item(i - 2, k + 2) = pl and arr.item(i - 3, k + 3) = pl then
						Result := True
					end
					k := k + 1
				end
				i := i - 1
			end

			-- Diagonal -45 degree
			from i := 1
			until i = m - 2
			loop
				from k := 1
				until k = n - 2
				loop
					if arr.item (i, k) = pl and arr.item (i + 1, k + 1) = pl and
					   arr.item (i + 2, k + 2) = pl and arr.item (i + 3, k + 3) = pl then
					   	Result := True
					end
					k := k + 1
				end
				i := i + 1
			end

		end
end
