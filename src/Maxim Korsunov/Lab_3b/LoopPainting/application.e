note
	description: "LoopPainting application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE}

	mirror (st: STRING): STRING
		local
			s1: LIST [STRING]
			ans: STRING
		do
			ans := ""
			s1 := st.split (' ')
			across s1 as str
			loop
				ans := str.item + " " + ans
			end
			Result := ans
		end

	blank (len: INTEGER): STRING
		local
			st: STRING
		do
			st := " "
			st.multiply (len)
			Result := st
		end

	make
		local
			j: INTEGER
			i: INTEGER
			n: INTEGER
			c: INTEGER
			s: STRING
			s1: STRING
			len: INTEGER
			str: ARRAYED_LIST [STRING]
		do
			print("Positive number is ")
			create str.make (0)
			Io.read_integer
			io.new_line
			n := Io.last_integer
			c := 1
			s := ""
			from i := 0
			until (n - 1) / 2 < i
			loop
				s := ""
				i := i + 1
				from j := 0
				until j = i
				loop
					j := j + 1
					s := s + (c.out + " ")
					c := c + 1
				end
				str.extend (s)
				if n.integer_remainder (2) = 0 OR ((n - 1) / 2 >= i) then
					s := " "
					from j := 0
					until j = i
					loop
						j := j + 1
						s := s + (c.out + " ")
						c := c + 1
					end
					str.extend (s)
				end
			end
			len := str.i_th (n).count * 2 + 1
			across str as st
			loop
				Io.put_string (st.item + blank(len - (2 * st.item.count)) + mirror (st.item) + "%N")
				i := i + 1
			end
		end

end
