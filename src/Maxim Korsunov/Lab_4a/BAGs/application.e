note
	description: "BAGs application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
		b1, b2:BAG1
		do
			create b1.make
			create b2.make

			b1.add ('a', 3)
			b1.add ('m', 9)
			b1.add ('z', 8)

			b2.add ('p', 35)

			b1.print_bag
			io.put_integer(('a').code)
		end

end
