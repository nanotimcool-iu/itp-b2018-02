note
	description: "Summary description for {BAG1}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BAG1

create
	make

feature {NONE} -- Initialization

	make
		local
			t: CELL_INFO
			i: INTEGER
		do
			create t.make (i.to_character_8, 0)
			create elements.make_filled (t, 1, 256)
			from
				i := 0
			until
				i > 255
			loop
				create t.make (i.to_character_8, 0)
				elements.enter (t, i + 1)
				i := i + 1
			end
		end

feature -- Variables

	elements: ARRAY [CELL_INFO]

feature -- Routines

	add (val: CHARACTER; n: INTEGER)
		require
			non_negative_n: n >= 0
		do
			elements [val.code+1].add (n)
		end

	remove (val: CHARACTER; n: INTEGER)
		require
			non_negative_n: n >= 0
		do
			elements [val.code+1].remove (n)
		end

	min: CHARACTER
		local
			i: INTEGER
		do
			from
				i := 1
			until
				i > 256
			loop
				if elements [i].number_copies > 0 then
					Result := elements [i].value
					i := 257
				end
				i := i + 1
			end
		end

	max: CHARACTER
		local
			i: INTEGER
		do
			from
				i := 256
			until
				i < 1
			loop
				if elements [i].number_copies > 0 then
					Result := elements [i].value
					i := 0
				end
				i := i - 1
			end
		end

	is_equal_bag (b: BAG1): BOOLEAN
		require
			b /= Void
		local
			i: INTEGER
		do
			Result := TRUE
			from
				i := 1
			until
				i > 256
			loop
				if elements [i].number_copies /= b.elements [i].number_copies then
					Result := FALSE
					i := 257
				end
				i := i + 1
			end
		end

	print_bag
		do
			across
				elements as e
			loop
				if e.item.number_copies > 0 then
					print (e.item.value.out + ": " + e.item.number_copies.out + "%N")
				end
			end
		end

end
