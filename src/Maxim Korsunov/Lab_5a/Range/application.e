note
	description: "Range application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE}

	make
		do
			create range.make(3, 10)
			create range2.make(4,9)
			create range3.make(7, 20)
			print(range.is_equal_range(range2))
			print(range.overlaps(range2))
			print(range.add(range3))
		end
		
feature
	range: RANGE
	range2: RANGE
	range3: RANGE
end
