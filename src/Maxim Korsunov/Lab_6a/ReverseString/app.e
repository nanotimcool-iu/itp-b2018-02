note
	description: "recursion application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APP

create
	make

feature {NONE}

	make
		do
			Io.put_string ("Your name: ")
			Io.readline
			Io.new_line
			str := Io.last_string

			inv_str := ""

			print("%N" + inverse_string(str))
		end

feature
	str: STRING
	inv_str:STRING

feature
	inverse_string(st: STRING):STRING
	do
		inv_str := inv_str + st.item (st.count).out
		st.remove_tail (1)
		if st.count = 0 then
			Result := inv_str
		else
			Result := inverse_string(st)
		end
	end

end
