note
	description: "BinaryTree application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APP

create
	make

feature {NONE}

	make
		local
			tree: BINARYTREE[INTEGER]
		do
			create tree.make (5)
			tree.set_left_value (7)

			if attached tree.get_left as left then
				left.set_left_value (8)
			end

			print(tree.height.out)
		end

end
