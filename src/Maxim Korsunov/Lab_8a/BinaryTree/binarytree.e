note
	description: "Summary description for {BINARYTREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BINARYTREE[G]

create
	make

feature {NONE}

	make(info: G)
		do
			value := info
		end

	value: G

	right:detachable BINARYTREE[G]
	left: detachable BINARYTREE[G]
feature


	get_value:G
	do
		Result:=value
	end

	get_left: detachable BINARYTREE[G]
	do
		Result:= left
	end

	get_right: detachable BINARYTREE[G]
	do
		Result:= right
	end

	set_left_value(v:G)
	local tmp:BINARYTREE[G]
	do
		create tmp.make (v)
		set_left_tree(tmp)
	end

	set_left_tree(tree: BINARYTREE[G])
	require non_void: tree /= Void
	do
		left := tree
	end

	set_right_value(v:G)
	local tmp:BINARYTREE[G]
	do
		create tmp.make (v)
		set_right_tree(tmp)
	end

	set_right_tree(tree: BINARYTREE[G])
	require non_void: tree /= Void
	do
		right := tree
	end

	height: INTEGER
	local l,r:INTEGER
	do
		if attached left as a_left then
			l := 1 + a_left.height
		end

		if attached right as a_right then
			r := 1 + a_right.height
		end

		Result := l.max (r)

	end

end
