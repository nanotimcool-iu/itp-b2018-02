note
	description: "Huffman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APP

create
	make

feature {NONE}

	make
		local
			huffman: HUFFMAN
			str: STRING
		do
			create huffman.make ("TEST is STRING")
			str := huffman.encode ("STRING is TEST")
			print(str + "%N")
			print(huffman.decode (str) + "%N")
		end
end

