note
	description: "Summary description for {PAIR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PAIR

inherit
	COMPARABLE

create
	make,
	make_l

feature {NONE}

	make (s, c: INTEGER)
		do
			char := s
			cnt := c
			left := VOID
			right := VOID
		end
	make_l(l,r:PAIR)
	do
		char := -1
		cnt := l.cnt + r.cnt
		left := l
		right := r
	end

feature

	char: INTEGER
	cnt: INTEGER
	left: detachable PAIR
	right: detachable PAIR

	is_less alias "<" (other: like Current): BOOLEAN
		do
			Result := Current.cnt > other.cnt
		end

	set_left (s,c: INTEGER)
		local t: PAIR
		do
			create t.make (s, c)
			left := t
		end

	set_right (s,c: INTEGER)
		local t: PAIR
		do
			create t.make (s, c)
			right := t
		end

end
