note
	description: "Summary description for {MY_BOUNDED_STACK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_BOUNDED_STACK[G]

inherit
	MY_STACK [G]
		redefine
			push,
			make
		end

create
	make,
	make_limited

feature {NONE}
	upper_bound:INTEGER

	make
	do
		create stack_list.make(0)
		upper_bound := stack_list.capacity
	end

	make_limited(bound:INTEGER)
	require
		positive_bound: bound > 0
	do
		create stack_list.make(0)
		upper_bound := bound
	end

feature
	push(el:G)
	do
		if stack_list.count < upper_bound then
			stack_list.extend (el)
		else
			print("Can't add more elements%N")
		end
	end


end
