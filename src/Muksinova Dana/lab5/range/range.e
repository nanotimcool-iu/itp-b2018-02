note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE
create
	make
feature
	left: INTEGER
	right: INTEGER
feature
	is_equal_range(other: RANGE): BOOLEAN
		do
	        Result := FALSE
			if ((other.left=left) and (other.right=right))
			 then
				Result := TRUE
			end
		end
	is_empty: BOOLEAN
		do
			Result:=TRUE
			if (left<=right) then
				Result:=FALSE
			end
		end
	is_sub_range_of(other:RANGE):BOOLEAN
		do
			Result:=FALSE
			if ((left>=other.left) and (right<=other.right))
			 then
				Result:=TRUE
			end
		end
	is_super_range_of(other:RANGE):BOOLEAN
		do
			Result:=FALSE
			if ((left<=other.left) and (right>=other.right))
			then
				Result:=TRUE
			end
		end
	left_overlaps(other:RANGE):BOOLEAN
		do
			Result:=FALSE
				if ((left<=other.right) and (left>=other.left)) then
					Result:=TRUE
				end
		end
	right_overlaps (other: RANGE): BOOLEAN
		do
			Result:=FALSE
				if ((right>=other.left) and (right<=other.right)) then
					Result:=TRUE
				end
		end
	overlaps (other:RANGE):BOOLEAN
		do
			Result:=FALSE
				if ((other.left>=left)and ()) then

				end
		end
end
