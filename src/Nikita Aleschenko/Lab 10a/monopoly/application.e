note
	description: "monopoly application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
      local
         game: GAME
      do
         io.new_line
         io.new_line
         io.put_string("Welcome to Monopoly game")
         
         create game.setup
         game.start
		end

end
