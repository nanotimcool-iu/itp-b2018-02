note
description: "Summary description for {CHANCE}."
author: ""
date: "$Date$"
revision: "$Revision$"

class
   CHANCE

inherit
   ACTION

feature
   do_action(player: PLAYER)
      local
         random: RANDOM_GENERATOR
         amount: INTEGER
      do
         create random.new

         if random.from_to(0, 1) = 1 then
            amount := random.from_to(1, 20) * 10

            io.new_line
            io.put_string("You got ")
            io.put_integer(amount)
            io.put_string("k")

            player.give_money(amount)
         else
            amount := random.from_to(1, 30) * 10

            io.new_line
            io.put_string("You lose ")
            io.put_integer(amount)
            io.put_string("k")

            player.take_money(amount)
         end
      end

   print_info(player: PLAYER)
      do
         io.new_line
         io.put_string("Now you on chance field")
      end

end
