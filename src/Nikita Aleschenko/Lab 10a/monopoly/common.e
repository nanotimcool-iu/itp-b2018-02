note
	description: "Summary description for {COMMON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMMON

inherit
   FIELD

create
   new

feature
   name: STRING
   price: INTEGER
   rent: INTEGER
   owner: PLAYER
      attribute
         Result := Void
      end
   
   new(n: STRING; p: INTEGER; r: INTEGER)
      do
         name := n
         price := p
         rent := r
      end

   change_owner(o: PLAYER)
      do
         owner := o
      end

   print_info(player: PLAYER)
      do
         io.new_line
         io.put_string("Now you are on ")
         io.put_string(name)
         io.put_string(" field")
      end
   
   do_action(player: PLAYER)
      local
         action: STRING
      do
         io.new_line

         if owner = Void or owner.lose then
            io.put_string("You can buy this field or skip (buy, skip)")
            io.new_line
            io.put_string("It costs ")
            io.put_integer(price)
            io.put_string("k")
            io.new_line
            io.read_line
            action := io.last_string.twin
            
            if action.count = 3 then -- if buy doesn't work
               if player.money > price then
                  owner := player
                  player.take_money(price)
                  
                  io.put_string("You bought this field")
                  io.new_line
                  io.put_integer(player.money)
                  io.put_string("k left")
               else
                  io.put_string("You don't have enough money")
               end
            else
               io.new_line
               io.put_string(action)
               io.put_integer(action.count)
            end
         else
            if owner = player then
               io.put_string("It's your property")
            else
               io.put_string("It's ")
               io.put_string(owner.name)
               io.put_string("'s property")
               io.new_line
               io.put_string("You will pay ")
               io.put_integer(rent)
               io.put_string("k for the rent")
               player.take_money(rent)
               owner.give_money(rent)
            end
         end
      end
   
end
