note
	description: "Summary description for {DICE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DICE

create
   new

feature {NONE}
   random: RANDOM_GENERATOR
   
feature
   state: INTEGER
   
   new
      do
         create random.new
         state := random.from_to(1, 4)
      end

   roll: INTEGER
      do
         state := random.from_to(1, 4)
         Result := state
      ensure
         correct: state >= 1 and state <= 4
      end
         
end
