note
description: "Summary description for {FIELD}."
author: ""
date: "$Date$"
revision: "$Revision$"

deferred class
   FIELD

feature
   print_info(player: PLAYER)
      deferred
   end

   do_action(player: PLAYER)
      deferred
   end

end
