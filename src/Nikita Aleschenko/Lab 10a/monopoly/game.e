note
description: "Summary description for {GAME}."
author: ""
date: "$Date$"
revision: "$Revision$"

class
   GAME

create
   setup

feature {NONE}
   fields: ARRAYED_LIST[FIELD]
   players: ARRAYED_LIST[PLAYER]
   dice1: DICE
   dice2: DICE
   players_left: INTEGER
   rounds: INTEGER
   turn: INTEGER

feature
   setup
      local
         action: ACTION
         common: COMMON
         player: PLAYER
         i: INTEGER
         amount: INTEGER
      do
         -- Players section
         io.new_line
         io.put_string("How many players will play? (2-6): ")
         io.read_integer
         amount := io.last_integer.twin
         io.new_line

         -- Base creation
         create fields.make(20)
         create players.make(amount)

         from
            i := 0
         until
            i = amount
         loop
            io.put_string("Player number ")
            io.put_integer(i+1)
            io.put_string(", enter your name: ")
            io.read_line
            create player.new(io.last_string.twin)
            players.extend(player)
            i := i + 1
         end
         players_left := players.count

         -- Fields section
         create {GO} action
         fields.extend(action)

         create common.new("Christ the Redeemer", 60, 2)
         fields.extend(common)

         create common.new("Luang Pho To", 60, 4)
         fields.extend(common)

         create {TAX} action
         fields.extend(action)

         create common.new("Alyosha monument", 80, 4)
         fields.extend(common)

         create {JAIL} action
         fields.extend(action)

         create common.new("Tokyo Wan Kannon", 100, 6)
         fields.extend(common)

         create common.new("Luangpho Yai", 120, 8)
         fields.extend(common)

         create {CHANCE} action
         fields.extend(action)

         create common.new("The Motherland", 160, 12)
         fields.extend(common)

         create {PARKING} action
         fields.extend(action)

         create common.new("Awaji Kannon", 220, 18)
         fields.extend(common)

         create {CHANCE} action
         fields.extend(action)

         create common.new("Rodina-Mat’ Zovyot!", 260, 22)
         fields.extend(common)

         create common.new("Great Buddha of Thailand", 280, 22)
         fields.extend(common)

         create {GO_TO_JAIL} action
         fields.extend(action)

         create common.new("Laykyun Setkyar", 320, 28)
         fields.extend(common)

         create common.new("Spring Temple Buddha", 360, 35)
         fields.extend(common)

         create {CHANCE} action
         fields.extend(action)

         create common.new("Statue of Unity", 400, 50)
         fields.extend(common)

         -- Dices init
         create dice1.new
         create dice2.new

         -- Others init
         rounds := 0
         turn := 1
      end

   whos_turn: PLAYER
      do
         Result := players[turn]

         turn := turn + 1
         if turn = players.count + 1 then
            turn := 1
            rounds := rounds + 1
         end
      end

   next_position(player: PLAYER)
      local
         next: INTEGER
      do
         next := player.position + dice1.roll + dice2.roll

         if next > 21 then
            next := next - 20
            fields[1].do_action(player)
         end

         player.change_position(next)
      end

   start
      local
         player: PLAYER 
      do
         io.new_line
         io.put_string("Game started")
         from
         until
            rounds = 100 or players_left = 1
         loop
            player := whos_turn
            if not player.lose then
               io.new_line
               io.new_line
               io.put_string(player.name)
               io.put_string(", it's your turn.")
               if player.in_jail then
                  fields[6].do_action(player)
               else
                  next_position(player)
                  fields[player.position].print_info(player)
                  fields[player.position].do_action(player)
               end
            end
         end
      end
end
