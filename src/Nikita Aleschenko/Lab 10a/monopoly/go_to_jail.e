note
	description: "Summary description for {GO_TO_JAIL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GO_TO_JAIL

inherit
   ACTION

feature
   do_action(player: PLAYER)
      do
         player.put_to_jail
      end

   print_info(player: PLAYER)
      do
         io.new_line
         io.put_string("Now you on Go to jail field")
      end
         
end
