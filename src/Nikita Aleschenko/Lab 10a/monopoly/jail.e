note
description: "Summary description for {JAIL}."
author: ""
date: "$Date$"
revision: "$Revision$"

class
   JAIL

inherit
   ACTION

feature
   do_action(player: PLAYER)
      local
         dice1: DICE
         dice2: DICE
         choice: STRING
      do
         create dice1.new
         create dice2.new

         if player.in_jail then
            io.new_line
            io.put_string("Pay 50k or roll dices? (pay / roll)")
            io.new_line
            io.put_string("You have ")
            io.put_integer(3 - player.turns_in_jail)
            io.put_string(" chances to get double")
            io.read_line

            choice := io.last_string.twin
            if choice = "pay" then
               player.take_money(50)
            else
               if dice1.state = dice2.state then
                  io.new_line
                  io.put_string("You got double")
                  player.out_of_jail
               else
                  io.new_line
                  io.put_string("You got ")
                  io.put_integer(dice1.state)
                  io.put_string(" and ")
                  io.put_integer(dice2.state)

                  player.jail_tic
                  if player.turns_in_jail = 3 then
                     io.new_line
                     io.put_string("Your chances ends")
                     player.take_money(50)
                     player.out_of_jail
                  end
               end
            end
         end
      end

   print_info(player: PLAYER)
      do
         io.new_line
         if player.in_jail then
            io.put_string("Now you are in jail")
         else
            io.put_string("Now you are visiting jail")
         end
      end
end
