note
description: "Summary description for {PLAYER}."
author: ""
date: "$Date$"
revision: "$Revision$"

class
   PLAYER

create
   new

feature
   name: STRING
   money: INTEGER
   position: INTEGER
   in_jail: BOOLEAN
   turns_in_jail: INTEGER
   lose: BOOLEAN
   
   new(n: STRING)
      do
         name := n
         money := 1500
         position := 1
         in_jail := false
         lose := false
      end

   change_position(i: INTEGER)
      require
         posible_values: i>0 and i<=20
      do
         position := i
      end
   
   put_to_jail
      do
         in_jail := true
         position := 6
         turns_in_jail := 0
      ensure
         locked: in_jail
         right_position: position = 6
         turns: turns_in_jail = 0
      end

   out_of_jail
      do
         in_jail := false
      ensure
         out: not in_jail
      end

   jail_tic
      do
         turns_in_jail := turns_in_jail + 1
      ensure
         tic: turns_in_jail = old turns_in_jail + 1
         less_than_3: turns_in_jail <= 3
      end
   
   give_money(amount: INTEGER)
      require
         positive: amount > 0
      do
         money := money + amount
      ensure
         right_amount: money = old money + amount
      end

   take_money(amount: INTEGER)
      require
         positive: amount > 0
      do
         money := money - amount

         if money < 0 then
            lose := true
            
            io.new_line
            io.put_string("Sorry, but you lose")
         end
      ensure
         right_amount: money = old money - amount
      end

   print_name_and_balance
      do
         io.new_line
         io.put_string(name)
         io.put_string(", your current balance: ")
         io.put_integer(money)
         io.put_string("k")
      end

end
