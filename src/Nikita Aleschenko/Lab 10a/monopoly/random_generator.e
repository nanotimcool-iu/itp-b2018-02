note
description: "Summary description for {RANDOM_GENERATOR}."
author: ""
date: "$Date$"
revision: "$Revision$"

class
   RANDOM_GENERATOR

create
   new
   
feature {NONE}
   sequence: RANDOM

feature
   new
      local
         time: TIME
         seed: INTEGER
      do
         create time.make_now
         seed := time.hour
         seed := seed * 60 + time.minute
         seed := seed * 60 + time.second
         seed := seed * 1000 + time.milli_second
         create sequence.set_seed(seed)
      end

   from_to(f: INTEGER; t: INTEGER): INTEGER
      require
         positive: f >= 0 and t >= 0
         t_is_greater: t > f
      do
         sequence.forth
         Result := sequence.item \\ t + f
      ensure
         correct: Result >= f and Result <= t
      end
   
end
