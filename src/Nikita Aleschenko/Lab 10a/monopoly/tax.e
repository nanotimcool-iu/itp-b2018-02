note
	description: "Summary description for {TAX}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	TAX

inherit
   ACTION
feature
   do_action(player: PLAYER)
      local
         take_amount: INTEGER
      do
         take_amount := player.money // 10

         io.new_line
         io.put_string("You lose ")
         io.put_integer(take_amount)
         io.put_string("k")
         io.new_line
         io.put_string("Your current balance is: ")
         io.put_integer(player.money)
         io.put_string("k")
         
         player.take_money(take_amount)
      end

   print_info(player: PLAYER)
      do
         io.new_line
         io.put_string("Now you on Income tax field")
      end
end
