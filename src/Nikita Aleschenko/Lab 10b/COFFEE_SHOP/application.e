class
    APPLICATION

inherit
    ARGUMENTS

create
    make

feature {NONE} -- Initialization

    make
            -- Run application.
		local
			b:ESPRESSO
			c:CAPPUCCINO
			d:CAKE
        do
			create a.make
			create b.make
			b.set_public_price (100.33)
			b.set_price (50.0)
			b.calc_p (b.public_price,b.price)
			a.force (b)

			create c.make
			c.set_public_price (120.77)
			c.set_price (60.0)
			c.calc_p (c.public_price,c.price)
			a.force (c)

			create d.make
			d.set_public_price (150.99)
			d.set_price (50.0)
			d.calc_p (d.public_price,d.price)
			a.force (d)

			profit
			print("%N")
			print_menu
        end

feature
	a:LINKED_LIST[PRODUCT]

profit
local
	i:INTEGER
	pr:REAL
do
	from
		i:=1
	until
		i=a.count+1
	loop
		a[i].menu
		print("'s profit is  " + a[i].profit.out + "%N")
		pr:=a[i].profit+pr
		i:=i+1
	end
end

print_menu
local
	i:INTEGER
do
	from
		i:=1
	until
		i=a.count+1
	loop
		a[i].menu
		print(" - " + a[i].public_price.out + "%N")
		i:=i+1
	end
end
end
