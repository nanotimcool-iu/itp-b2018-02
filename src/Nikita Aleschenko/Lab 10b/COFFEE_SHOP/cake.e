note
	description: "Summary description for {CAKE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAKE
inherit
	PRODUCT
redefine
	menu,public_price,profit
end
create
	make

feature

	price:REAL assign set_price
	set_price(a_price:REAL)
	do
		price:=a_price
	end
	profit:REAL
	calc_p(a_public_price,a_price:REAL)
	do
		profit:=a_public_price-a_price
	end

	public_price:REAL assign set_public_price
	set_public_price(a_public_price:REAL)
	do
		public_price:=a_public_price
	end
	menu()
	do
		print("Cake")
	end
	make()
	do
		print("It's cake!%N")
	end
end
