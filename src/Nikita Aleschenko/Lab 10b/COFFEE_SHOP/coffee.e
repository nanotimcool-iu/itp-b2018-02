note
	description: "Summary description for {COFFEE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COFFEE
inherit
	PRODUCT
redefine
	menu,public_price,profit
end
create
	make

feature
	menu()
	do
		print("Coffee!Don't know what exactly is!%N")
	end
	price:REAL assign set_price
	set_price(a_price:REAL)
	do
		price:=a_price
	end
	profit:REAL
	calc_p(a_public_price,a_price:REAL)
	do
		profit:=a_public_price-a_price
	end

	public_price:REAL assign set_public_price
	set_public_price(a_public_price:REAL)
	do
		public_price:=a_public_price
	end
	make()
	do
		print("It's obviosly coffee!But we don't know which one!%N")
	end
end
