note
	description: "testing1 application root class"
	date: "$Date$"
	revision: "$Revision$"

class

	APPLICATION



inherit



	ARGUMENTS_32



create

	make



feature {NONE} -- Initialization



	make

			-- Run application.

		local

			str: STR

			a: ARRAY [STRING]

			i: INTEGER

		do

			print ("Input a string: ")

			io.readline

			create str.make ( io.laststring.twin )

			print("Anagrams:%N")

			str.anagram(str.s.twin, 1, str.s.count)

		end







	

end
