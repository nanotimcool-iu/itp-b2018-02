class
	APPLICATION

create
	execute

feature {NONE} -- Initialization

	execute
			-- Run application.
		do
			io.new_line()
			io.new_line()
			io.put_string ("Name: Nikita Aleschenko")
			io.new_line()
			io.put_string ("Age: ")
			io.put_integer (18)
			io.new_line()
			io.put_string ("Mother tongue: Russian")
			io.new_line()
			io.put_string ("Has a cat: ")
			io.put_boolean (False)

		end

end
