note
	description: "Summary description for {BANK_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

create
	make

feature
	name: STRING
		attribute
			Result := "Nikita"
		end
	balance: INTEGER
		attribute
			Result := 100
		end

	deposit (amount: INTEGER)
		do
			if balance + amount > 1000000 then
				io.new_line()
				print("Sorry, you can't deposit this amount of money")
			else
				balance := balance + amount
			end
		end

	withdraw (amount: INTEGER)
		do
			if balance - amount < 100 then
				io.new_line()
				print("Sorry, you can't withdraw this amount of money")
			else
				balance := balance - amount
			end
		end

	make
		do
			deposit(100)
			io.new_line()
			print(balance)
			deposit(1000000)
			withdraw(1000000)
		end

end
