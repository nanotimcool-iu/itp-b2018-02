note
	description: "university application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			the_course: COURSE
		do
			create the_course.create_class ("Programming", 236, "Monday", 2)
			io.new_line
			io.new_line
			print (the_course.name)
			io.new_line
			print (the_course.identifier)
			io.new_line
			print (the_course.schedule)
			io.new_line
			print (the_course.max_student)
		end

end
