note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

create
	create_class

feature
	name: STRING
	identifier: INTEGER
	schedule: STRING
	max_student: INTEGER

	create_class ( new_name: STRING; new_id: INTEGER; new_schedule: STRING; new_max: INTEGER )
		do
			if new_max < 3 then
					max_student := 3
				else
					max_student := new_max
			end
			name := new_name
			identifier := new_id
			schedule := new_schedule
		end

end
