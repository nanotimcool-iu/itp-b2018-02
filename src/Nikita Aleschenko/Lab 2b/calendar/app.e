note
	description: "lab2bcalendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APP

inherit

	ARGUMENTS



create

	make



feature

	day1, day2, day3: DATE_TIME

	k, k2: PERSON

	event1, event2, event3: ENTRY

	calend: CALENDAR



feature {NONE} -- Initialization



	make

			-- Run application.

		do

			create day1.make (2018, 09, 13, 12, 30, 00)

			create day2.make(2018, 09, 13, 12, 20, 00)

			create day3.make(2018,09,13,9,30,00)

			create k.c_person ("Shamil", 12, "qwr", "S@gmail.com")

			create k2.c_person ("Kamil", 1634, "firefighter", "K@gmail.com")

			create calend.c_calendar

			event1 := calend.create_entry (day1, k)

			event2 := calend.create_entry (day2, k)

			event3 := calend.create_entry (day2, k2)

			print(calend.in_conflict(event1, event2))

			print("%N")

			print(calend.in_conflict(event2, event3))

			print("%N")

			calend.edit_subject (event1, "playing football")

			calend.edit_date (event2, day3)

			print(calend.in_conflict (event1, event2))

			print("%N")

			print(calend.get_owner_name (event1))

		end



end
