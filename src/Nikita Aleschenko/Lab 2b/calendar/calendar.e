note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR
create

	c_calendar



feature {NONE}

	c_calendar

	do

		io.put_string ("new calendar" + "%N")

	end



feature

	create_entry(d: DATE_TIME; o: PERSON): ENTRY

	local

		s: STRING

		p: STRING

		a: ENTRY

	do

		io.put_string ("please, enter the subject of an entry: ")

		io.read_line

		s := io.last_string.twin

		io.put_string ("please, enter the place of an entry: ")

		io.read_line

		p := io.last_string.twin

		create a.c_entry (d, o, s, p)

		Result := a

	end



feature

	edit_subject(e: ENTRY; new_subject: STRING)

	do

		e.set_subject (new_subject)

	end



feature

	edit_date(e: ENTRY; new_date: DATE_TIME)

	do

		e.set_date (new_date)

	end



feature

	get_owner_name(e: ENTRY):STRING

	do

		Result := e.owner.name

	end



feature

	--i assume that every event's duration is 1 hour

	in_conflict(e1,e2: ENTRY):BOOLEAN

	local

		t1: INTEGER

		t2: INTEGER

		temp: INTEGER

	do

		temp := 0

		if e1.owner = e2.owner then

			if e1.date.year = e2.date.year and e2.date.month = e1.date.month and e1.date.day = e2.date.day then

				t1 := e1.date.hour * 3600 + e1.date.minute * 60 + e1.date.second

				t2 := e2.date.hour * 3600 + e2.date.minute * 60 + e2.date.second

				if t1 > t2 then

					temp := t2.twin

					t2 := t1.twin

					t1 := temp.twin

				end

			if (t1 + 3600) > t2 then

				Result := true

				else

				Result := false

			end

			else

				Result := false

			end

		end
end
end
