note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON
create

	c_person



feature

	name: STRING

	phone_number: INTEGER

	work_place: STRING

	email: STRING



feature

	c_person(n: STRING; p: INTEGER; w: STRING; e: STRING)

	do

		name := n

		phone_number := p

		work_place := w

		email := e

	end



end
