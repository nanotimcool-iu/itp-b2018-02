note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

feature
	create_entry(new_date: TIME; new_owner: PERSON; new_subject: STRING; new_place: STRING): ENTRY
		local
			e: ENTRY
		do
			create e.new_entry(new_date, new_owner, new_subject, new_place)
			Result := e
		end

	edit_subject(e: ENTRY; new_subject: STRING)
		do
			e.subject := new_subject
		end

	edit_date(e: ENTRY; new_date: STRING)
		do
			e.date := new_date
		end

end
