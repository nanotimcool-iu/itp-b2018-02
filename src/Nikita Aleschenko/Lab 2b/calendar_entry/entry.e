note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	new_entry

feature
	date: TIME assign set_date
	owner: PERSON
	subject: STRING assign set_subject
	place: STRING

	new_entry(new_date: TIME; new_owner: PERSON; new_subject: STRING; new_place: STRING)
		do
			date := new_date
			owner := new_owner
			subject := new_subject
			place := new_place
		end

	set_subject(new_subject: STRING)
		do
			subject := new_subject
		end

	set_date(new_date: TIME)
		do
			date := new_date
		end
end
