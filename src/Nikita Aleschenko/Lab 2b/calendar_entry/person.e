note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	new_person

feature
	name: STRING
	phone_number: INTEGER_64
	work_place: STRING
	email: STRING

	new_person(new_name: STRING; new_phone_number: INTEGER_64; new_work_place: STRING; new_email: STRING)
		do
			name := new_name
			phone_number := new_phone_number
			work_place := new_work_place
			email := new_email
		end

end
