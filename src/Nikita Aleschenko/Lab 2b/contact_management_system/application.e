note
	description: "contact_management_system application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			c1: CONTACT
			c2: CONTACT
			c3: CONTACT
			control: CMS
		do
			create control

			c1 := control.create_contact("Nikita", 9600565007, "Innopolis", "supercreed2@gmail.com")
			c2 := control.create_contact("Toby", 987270936, "USA", "toby@gmail.com")
			c3 := control.create_contact("Mark", 9033402255, "Russia", "mark@gmail.com")

			control.add_emergency_contact (c1, c2)
			control.add_emergency_contact (c2, c3)

			control.edit_contact (c2, "Toby", 999999999, "USA", "toby_is_the_best@gmail.ru")

			control.remove_emergency_contact (c2)
			control.remove_emergency_contact (c3)
		end

end
