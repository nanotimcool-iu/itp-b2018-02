note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

feature
	create_contact (name: STRING; phone_number: INTEGER_64; work_place: STRING; email: STRING): CONTACT
		local
			c: CONTACT
		do
			create c
			c.name := name
			c.phone_number := phone_number
			c.work_place := work_place
			c.email := email
			Result := c
		end

	edit_contact (c: CONTACT; name: STRING; phone_number: INTEGER_64; work_place: STRING; email: STRING)
		do
			c.name := name
			c.phone_number := phone_number
			c.work_place := work_place
			c.email := email
		end

	add_emergency_contact(c1: CONTACT; c2: CONTACT)
		do
			c1.call_emergency := c2
		end

	remove_emergency_contact(c: CONTACT)
		do
			c.set_empty_emergency
		end

end
