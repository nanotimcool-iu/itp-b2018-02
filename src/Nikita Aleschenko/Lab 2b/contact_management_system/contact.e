note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

feature

	name: STRING assign set_name
		attribute
			Result := ""
		end

	phone_number: INTEGER_64 assign set_phone_number
		attribute
			Result := 0
		end

	work_place: STRING assign set_work_place
		attribute
			Result := ""
		end

	email: STRING assign set_email
		attribute
			Result := ""
		end

	call_emergency: CONTACT assign set_call_emergency
		attribute
			Result := Void
		end

	set_name (new_name: STRING)
		do
			name := new_name
		end

	set_phone_number (new_phone_number: INTEGER_64)
		do
			phone_number := new_phone_number
		end

	set_work_place (new_work_place: STRING)
		do
			work_place := new_work_place
		end

	set_email (new_email: STRING)
		do
			email := new_email
		end

	set_call_emergency (new_call_emergency: CONTACT)
		do
			call_emergency := new_call_emergency
		end

	set_empty_emergency
		local
			c: CONTACT
		do
			create c
			call_emergency := c.call_emergency
		end

end
