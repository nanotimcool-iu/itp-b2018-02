note
	description: "calendarB application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			--| Add your code here
		--	print ("Hello Eiffel World!%N")

			create p1.make_person("Mihail", 001, "SuperMuxa", "Zef@mail.ru")
			create p2.make_person("Roman", 002, "AGAJ", "AjadjanianRoma@mail.ru")
			create p3.make_person("Ivan", 003, "Chmonia", "An_Van@mail.ru")

			create calendar.make

			create t1.make(10,45,0)
			create t2.make(14,30,0)
			create t3.make(16,20,0)
			create t4.make(18,55,0)
			create t5.make(21,00,0)


			create e1.make(t1, p1, "football", "IU")
			create e2.make(t2, p2, "basketball", "IU")
			create e3.make(t3, p3, "volleyball", "IU")
			create e4.make(t4, p1, "hockey", "IU")
			create e5.make(t1, p3, "pingpong", "IU")
			create e6.make(t5, p1, "Gym", "IU")

			calendar.add_entry (e1, 23)
			calendar.add_entry (e2, 6)
			calendar.add_entry (e3, 11)
			calendar.add_entry (e4, 2)
			calendar.add_entry (e5, 15)
			calendar.add_entry (e6, 9)


	print(calendar.printable_month)

	create a.make(0)

	a := calendar.in_conflict (23)

	from
		i := 1
	until
		i > a.count
	loop
		print(a[i].date.out + "%N")

		i := i + 1
	end


		end



feature
	p1, p2, p3 : PERSON
	calendar : CALENDAR
	e1, e2, e3, e4, e5, e6: ENTRY

	t1, t2, t3, t4, t5 : TIME
	i : INTEGER
	a : ARRAYED_LIST[ENTRY]

end
