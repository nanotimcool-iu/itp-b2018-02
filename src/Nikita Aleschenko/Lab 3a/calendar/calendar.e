	note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

create
	make

feature
	days : ARRAYED_LIST[DAY]
	i : INTEGER
	j : INTEGER
	c : INTEGER
	d : DAY

feature
	make
	do
		create days.make(0)
		create d.make (1)
		from
			i := 1
		until
			i > 31
		loop
			create d.make(i)
			days.extend (d)
			i := i + 1
		end
	end

add_entry(e:ENTRY; day: INTEGER)
	do
		days[day].events.extend(e)
	end

edit_subject(e:ENTRY;new_subject:STRING)
do
	e.set_subject (new_subject)
end

edit_date(e:ENTRY;new_date:TIME)
do
	e.set_date (new_date)
end

get_owner_name(e:ENTRY):STRING
do
	Result := e.owner.name
end

in_conflict(day:INTEGER):ARRAYED_LIST[ENTRY]
	local
		ans : ARRAYED_LIST[ENTRY]
do

	create ans.make(0)

	from
		i := 1
	until
		i >= days[day].events.count
	loop
		if not ans.has (days[day].events[i]) then

			c := 0

			from
				j := i + 1
			until
				j > days[day].events.count
			loop
				if days[day].events[i].date.hour = days[day].events[j].date.hour and days[day].events[i].date.minute = days[day].events[j].date.minute and days[day].events[i].date.second = days[day].events[j].date.second then

					ans.extend (days[day].events[j])
					c := c + 1
				end

				j := j + 1
			end

			if c > 0 then

				ans.extend (days[day].events[i])

			end

		end

		i := i + 1
	end

	Result := ans
end

printable_month : STRING
	local
		ans : STRING
	do
		ans := ""
		from
			i := 1
		until
			i > 31
		loop
	--		print(j.out + "%N")

			ans := ans + "DAY " + i.out + " events: "
	--		print(j.out + "%N")
	--		print(j.out + "%N")


			from
				j := 1
			until
				j > (days[i].events.count - 1)
			loop
--				print(j.out + "%N")

				ans := ans + "for " + days[i].events[j].owner.name + " " + days[i].events[j].subject + " in " + days[i].events[j].place + " at " + days[i].events[j].date.out + ", "
				j := j + 1
			end

		--	print(j.out + "%N")

			if days[i].events.count > 0 then
				ans := ans + "for " + days[i].events[j].owner.name + " " + days[i].events[j].subject + " in " + days[i].events[j].place + " at " + days[i].events[j].date.out

			end

		--	print(j.out + "%N")

			ans := ans + "%N"


			i := i + 1

		end

		Result := ans
	end

end
