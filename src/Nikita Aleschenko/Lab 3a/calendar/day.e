note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	make

feature
	date : INTEGER
	events : ARRAYED_LIST[ENTRY]


feature
	make(d:INTEGER)
	do
		date := d
		create events.make(0)
	end

	set_date(d:INTEGER)
	do
		date := d
	end


end
