note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	make

feature
	date : TIME
	owner : PERSON
	subject : STRING
	place : STRING

feature
	make(d:TIME; ow : PERSON; sub : STRING; pl : STRING)
	do
		set_date(d)
		set_owner(ow)
		set_subject(sub)
		set_place(pl)
	end

	set_date(d:TIME)
	do
		create date.make (0, 0, 0)
		date := d
	end

	set_owner(own:PERSON)
	do
		create owner.make_person ("", 0, "", "")
		owner := own
	end

	set_subject(sub:STRING)
	do
		subject := sub
	end

	set_place(pl:STRING)
	do
		place := pl
	end
end
