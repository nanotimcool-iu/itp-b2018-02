note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make_person

feature
	name : STRING
	phone_number: INTEGER
	work_place: STRING
	email : STRING


feature
	make_person(n:STRING; pn:INTEGER; wp:STRING; em:STRING)
	do
		name := n
		phone_number := pn
		work_place := wp
		email := em
	end

end
