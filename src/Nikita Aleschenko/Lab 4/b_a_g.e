note
	description: "Summary description for {B_A_G}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	B_A_G
	create
		make



feature
	elements:ARRAY[CELL_INFO]
	make
	local

		i:INTEGER

	do
		create elements.make_filled(create {CELL_INFO}.make_empty,0,255)
		from
			i:=0
		until
			i=255
		loop
			elements[i]:=create {CELL_INFO}.make(i.to_character_8,i)
			i:=i+1
		end

	end
	add(val:CHARACTER;n:INTEGER)
	require
		n>=0
	do
		elements[val.code].add(n)
	end
	remove (val: CHARACTER; n: INTEGER)
	require
		n>=0
	do
		elements[val.code].remove(n)
	end
	min: CHARACTER

	local
		i:INTEGER
		cnt:INTEGER
		q:CHARACTER
	do
		cnt:=0
		from
			i:=0
		until
			i=255
		loop
			if
				elements[i].numb_copies>0 and cnt=0
			then
				q:=elements[i].value
				cnt:=cnt+1
			end
			i:=i+1
		end
		Result:=q
	--ensure

	end
	max: CHARACTER
	local
		i:INTEGER
		cnt:INTEGER
		q:CHARACTER
	do
		cnt:=0
		from
			i:=255
		until
			i=-1
		loop
			if
				elements[i].numb_copies>0 and cnt=0
			then
				q:=elements[i].value
				cnt:=cnt+1
			end
			i:=i-1
		end
		Result:=q
end
		is_equal_bag (b: B_A_G): BOOLEAN
		require
			b/=Void
		local
			i:INTEGER
			cnt:INTEGER
			flag:BOOLEAN
		do
			flag:=true
			from
				i:=1
			until
				i=255
			loop
				if
					b.elements[i].numb_copies/=elements[i].numb_copies
				then
					flag:=false
				end
				i:=i+1
			end
			if
				flag=true
			then
				Result:=true
			else
				Result:=false
			end
		end
end
