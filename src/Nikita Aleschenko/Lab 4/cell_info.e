debug
	note
end
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create	make,make_empty
	feature
	value:CHARACTER
	numb_copies:INTEGER
	--	elements:ARRAY[CELL_INFO]
	make(c:CHARACTER;a:INTEGER)
		do
			value:=c
			numb_copies:=a
		end
	make_empty
	do

	end
	add(n:INTEGER)
	do
		numb_copies:=numb_copies+n
	end
	remove(n:INTEGER)
	do
		if
			numb_copies-n>0
		then
			numb_copies:=numb_copies-n
		else
			numb_copies:=0
		end
	end




end
