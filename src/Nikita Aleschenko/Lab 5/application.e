note
	description: "lab5_4 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local x:ARRAY2[INTEGER]
		i:INTEGER
		j:INTEGER
		q:MATRIX_OPER
		w:INTEGER
		a:INTEGER
		b:INTEGER
			-- Run application.

		do
			--| Add your code here
			--print ("Hello Eiffel World!%N")
			io.putstring ("input n and m for matrix%N")
			io.read_integer
			a:=io.last_integer
			io.read_integer
			b:=io.last_integer
			create x.make_filled (0, a,b)

			create q.make

			--create MATRIX_OPER.make
			from
				i:=1
			until
				i=a+1
			loop
				from
					j:=1
				until
					j=b+1
				loop
					io.read_integer
					x.item (i,j):=io.last_integer
					j:=j+1
				end
				i:=i+1
			end

		w:=q.det(x)
		print("det= " + w.out)

		end

end
