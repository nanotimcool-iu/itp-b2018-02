note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER


create
	make



feature
	m:INTEGER
	n:INTEGER
	a:ARRAY2[INTEGER]
	feature
	make
	do
		create a.make_filled (0,n,m)

	end


add(m1,m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		m1.width=m2.width
		m1.height=m2.height
	local
	i:INTEGER
	j:INTEGER

	do

	from
		i:=1
	until
		i=m1.height
	loop
		from
			j:=0
		until
			j=m1.width
		loop
			a.item (i,j):=m1.item (i,j)+m2.item (i,j)
			j:=j+1
		end
		i:=i+1
	end
	Result:=a
end

minus(m1,m2:ARRAY2[INTEGER]):ARRAY2[INTEGER]
	require
		m1.width=m2.width
		m1.height=m2.height
	local
	i:INTEGER
	j:INTEGER

	do

	from
		i:=1
	until
		i=m1.height
	loop
		from
			j:=0
		until
			j=m1.width
		loop
			a.item (i,j):=m1.item (i,j)+m2.item (i,j)
			j:=j+1
		end
		i:=i+1
	end
	Result:=a
end

prod (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
require
	m1.width=m2.height
local
	i:INTEGER
	j:INTEGER
	b:ARRAY2[INTEGER]
do
	create b.make_filled (0,m2.width,m1.height)
	from
		i:=1
	until
		i=m2.width
	loop
		from
			j:=1
		until
			j=m1.height
		loop
			b.item (i,j):=b.item (i,j)+m1.item (i,j)*m2.item (j,i)
			j:=j+1
		end
		i:=i+1
	end
	Result:=b
end


det (input: ARRAY2 [INTEGER]): INTEGER
		require
			input.width = input.height
		local
			i: INTEGER
		do
			result := 0
			if (input.height = 1) then
				result := input [1, 1]
			else
				from
					i := 1
				until
					i = input.width + 1
				loop
					Result := Result + input [1, i] * ((-1).power (i + 1)).ceiling * det (min(input, 1, i))
					i := i + 1
				end
			end
		end

min (input: ARRAY2 [INTEGER]; x, y: INTEGER): ARRAY2 [INTEGER]
		local
			ini, inj, i, j: INTEGER
		do
			create result.make_filled (0, input.height - 1, input.height - 1)
			from
				i := 1;
				ini := 1
			until
				i = input.height + 1
			loop
				if i /= x then
					from
						j := 1;
						inj := 1
					until
						j = input.height + 1
					loop
						if j /= y then
							result [ini, inj] := input [i, j]
							inj := inj + 1
						end
						j := j + 1
					end
					ini := ini + 1;
				end
				i := i + 1
			end
		end

end
