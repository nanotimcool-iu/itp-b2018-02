note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER
create
	make
	feature
a:ARRAY[INTEGER]
n:INTEGER
feature
	make
	do
		create a.make_filled (0,1,n)
	end
cross_product(v1,v2:ARRAY[INTEGER]):ARRAY[INTEGER]
require
	v1.count=v2.count
	v1.count=3
local
	x:ARRAY[INTEGER]
do
		create x.make_filled (0,1,3)
		x[1]:=v1[2]*v2[3]-v1[3]*v2[2]
		x[2]:=v1[1]*v2[3]-v1[3]*v2[1]
		x[3]:=v1[1]*v2[2]-v1[2]*v2[1]


	Result:=x
end

dot_product(v1,v2:ARRAY[INTEGER]):INTEGER
require
v1.count=v2.count
local
	i:INTEGER
	res:INTEGER
do
from
	i:=1
until
	i=v1.count
loop
	res:=res+v1[i]*v2[i]
	i:=i+1
end
Result:=res
end

triangle_area(v1,v2:ARRAY[INTEGER]):DOUBLE
require
v1.count=v2.count
v1.count=3
local
	x:ARRAY[INTEGER]
	res:DOUBLE
do
	x:=cross_product(v1,v2)
	res:=(x[1]^2 + x[2]^2 + x[3]^2)^0.5
	Result:= 0.5*res
end



end
