note
	description: "reverse_st application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make

			-- Run application.


		do
			--| Add your code here
	--		print ("Hello Eiffel World!%N")
			print(reverse_iter("qwerty"))
		end

reverse_iter(input: STRING): STRING

		local
			i: INTEGER
		do
			result := ""

			from
				i := input.count
			until
				i < 1
			loop
				result := result + input[i].out

				i := i - 1
			end
		end
reverse (s: STRING): STRING
		do
			if s.count = 1 then
				Result := s.out
			else
				Result := s.at (s.count).out + reverse (s.substring (1, s.count - 1))
			end
		end

end
