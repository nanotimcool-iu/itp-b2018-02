class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature
	row, column: INTEGER
	map: ARRAY [ARRAY [INTEGER]]
feature {NONE} -- Initialization

	make
		do
			map := <<<<0, 0, 0>>,
					 <<1, 1, 0>>,
					 <<1, 1, 0>>>>

			row := 3
			column := 3
			rec_print (1, find_path (1, 1))

		end



find_path (r1, c1: INTEGER): ARRAY [STRING]
		local
			a1, a2: ARRAY [STRING]
		do
			if r1 = row and c1 = column then
				Result := <<"END%N">>
			elseif r1 > row or else c1 > column or else map[r1].item (c1) = 1 then
				Result := <<>>

			else
				a1 := find_path (r1 + 1, c1)
				a2 := find_path (r1, c1 + 1)
				if a2.count /= 0 and (a1.count = 0 or a2.count < a1.count) then
					a2.force ("RIGHT%N", a2.count + 1)
					Result := a2
				elseif a1.count /= 0 and (a2.count = 0 or a1.count < a2.count) then
					a1.force ("DOWN%N", a1.count + 1)
					Result := a1
				else
					Result := <<>>
				end
			end
		end

	rec_print (i: INTEGER; a: ARRAY [STRING])
		do
			if i /= a.count then
				rec_print (i + 1, a)
			end
			print (a [i])

		end


end
