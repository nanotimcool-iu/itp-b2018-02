note
	description: "Hangman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

h:GAME
	make
			-- Run application.
		do
				create h.init
				h.start
		end

end
