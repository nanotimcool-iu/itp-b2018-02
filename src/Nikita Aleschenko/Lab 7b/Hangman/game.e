note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME



inherit

	EXECUTION_ENVIRONMENT



create

	init





feature {NONE}--Vars



	players: ARRAYED_LIST[STRING] -- names of player



	dictionary: ARRAY[ARRAYED_LIST[STRING]]



	guesses_left, guessed, max_len: INTEGER



	word: STRING



	is_letter_guessed:ARRAY[BOOLEAN]



feature {NONE} -- Initialization



	init

			-- Initialization for `Current'.

		local

			n, i: INTEGER

			tmp:ARRAYED_LIST[STRING]

		do

			max_len := 40

			from

			until

				n > 0 and n <= max_len

			loop

				print ("Input number of players (<=" + max_len.out + "): ")

				io.readint

				n := io.lastint

			end



			create players.make (n)

			from

				i := 0

			until

				i = n

			loop

				print ("Input " + (i + 1).out + " player's name: ")

				io.readline

				players.extend (io.laststring.twin)

				i := i + 1

			variant

				n - i

			end



			from

			until

				guesses_left >= n and then guesses_left <= 26

			loop

				print ("Input maximum number of guesses (number of players <= x <= 26): ")

				io.readint

				guesses_left := io.lastint

			end





			create tmp.make (0)

			create dictionary.make_filled (tmp, 1, max_len)

			from

				i := 1

			until

				i > max_len

			loop

				create tmp.make (0)

				dictionary[i] := tmp

				i:=i+1

			end

			fill_dict

			word:=choose_word(n)

			word.to_lower





			create is_letter_guessed.make_filled (false, 0, 25)

			guessed := 0

		end



		choose_word(n:INTEGER) :STRING

			local

				r:RANDOM

				t:TIME

				i:INTEGER

			do

				create t.make_now

				create r.set_seed (t.hour+t.minute+t.second*3)

				i := n + r.item\\(max_len-n+1)

				r.forth

				Result := dictionary[i].at (1+r.item\\dictionary[i].count)

				print("len: "+i.out + " word #"+(1+r.item\\dictionary[i].count).out + " r: " + r.item.out+"%N")

				across

					Result as el

				loop

					if not el.item.is_alpha then

						guessed := guessed + 1

					end

				end

			end



		fill_dict

			local

				inp:PLAIN_TEXT_FILE

				s:STRING

			do



				create inp.make_open_read ("words.txt")

				from until inp.exhausted

				loop

					inp.readline

					s:=inp.laststring

					dictionary[s.count].extend (s.twin)



				end

				inp.close

			end





feature {ANY}

	start

		local

			turn, guess_result: INTEGER

			win: BOOLEAN

			msg:STRING

		do

			msg := ""

			from

				turn := 0

			until

				guesses_left = 0 or win

			loop

				system("CLS")

				print(msg)

				print("Now it is " + players[turn \\ players.count + 1] + "'s turn%N")

				print("guesses left: " + guesses_left.out + "%N")

				print_word

				print("Make a guess: ")

				io.read_character

				guess_result := make_guess(io.lastchar) -- 0 - guessed, 1 - wrong,

				--2 letter have alredy been guessed, 3 - player win

				io.read_character

				if guess_result = 0 then

					msg := "Correct!%N"

				elseif guess_result = 1 then

					msg := "Unfortunately, there is no such letter%N"

					guesses_left := guesses_left - 1

				elseif guess_result = 2 then

					msg := "Your letter have already been tried to guess.%NTry again with another one%N"

					turn := turn - 1

				elseif guess_result = 3 then

					win := true

				end

				turn := turn + 1

			end



			system("CLS")

			if win then

				print ("Player " +  players[(turn-1) \\ players.count + 1] + " guessed last letter!")

			else

				print("You die =(%NThe word was:" + word)

			end

		end



feature {NONE}

	make_guess(char:CHARACTER) :INTEGER

		local

			c:CHARACTER

			occurrences : INTEGER

		do

			c := char.lower

			if is_letter_guessed[c.code - ('a').code]  then --c guessed before

				Result := 2

			else

				occurrences :=word.occurrences (c)

				if occurrences > 0 then --c guessed or win

					Result := 0

					guessed := guessed + occurrences

					is_letter_guessed[c.code - ('a').code] := True

					if guessed = word.count then

						Result := 3

					end

				else

					Result := 1

				end

			end



		end



	print_word

		local

			i:INTEGER

		do

			from

				i:=1

			until

				i>word.count

			loop

				if not word[i].is_alpha or else is_letter_guessed.at (word[i].code - ('a').code) then

					print(word[i])

				else

					print("_")

				end

				i:=i+1

			end

			print("%N")

		end



end
