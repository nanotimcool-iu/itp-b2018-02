note
	description: "bt application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	root: NODE
    node1,node2,node3: NODE
    st:STRING

	make
			-- Run application.
		do
			--| Add your code here
			--print ("hello%N")
       		 create root.make()
       		 root.setname ("ROOT")
       		 create node1.make()
       		 create node2.make()
       		 create node3.make()
        	 build_tree(root,node1)
			 build_tree(node1,node2)
			 build_tree(node2,node3)
			 print("Height:%N" + height(root).out)




    end




build_tree(rt,node:NODE)
    do
        io.put_string ("Name: ")
        io.read_line
        st:=io.last_string
        if st /= Void then
        	node.setname(st)
        	add(rt,node)
        end

    end


add(rt,a:NODE)
    do
        if rt.name<a.name then
            if rt.left = Void then
                rt.setleft(a)
            else
                add(rt.left,a)
            end
        else
            if rt.right = Void then
                root.setright(a)
            else
                add(rt.right,a)
            end
        end
    end

max(a, b: INTEGER):INTEGER
		do
			if a >= b then
				result :=a
			else
				result :=b
			end
		end


height(rt:NODE):INTEGER
require
	rt /= Void
local
	l,r:INTEGER
do
	if
		rt.left /=Void and rt.right /= Void
	then
		Result:= max(1+height(rt.left),1+height(rt.right))
	else
		if rt.left /=Void or rt.right /= Void then
			if
				rt.left /=Void
			then
				Result:=1+height(rt.left)
			else
				Result:=1+height(rt.right)
			end
		else
			Result:=1
		end
	end

end

end


