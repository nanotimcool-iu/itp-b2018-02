note
	description: "Summary description for {NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
    NODE
create
    make
feature

    name: STRING -- value?

    right : NODE assign setright
    	attribute
    	Result:=Void
    	end
    left : NODE assign setleft
    attribute
    	Result:=Void
    end
    setname(n:STRING)
    do
        name:= n
    end
    setleft(i:NODE)
    do
        left:=i
    end
    setright(i:NODE)
    do
        right:=i
    end
make
    do
        create name.make(80)

    end

end
