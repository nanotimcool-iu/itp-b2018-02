note
	description: "haffman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization
			huffman: CODE

			str: STRING


	make
			-- Run application.
		do
			--| Add your code here

			create huffman.make ("aaaaaaaaaabbbc")


			str := huffman.encode ("aaaaaaaaaabbbc")

			print(str + "%N")

			print(huffman.decode (str) + "%N")


		end

end
