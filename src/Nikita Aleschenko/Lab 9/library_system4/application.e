note
	description: "library_system application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
file: PLAIN_TEXT_FILE
dictionary: ARRAY [LIBRARY]
person1,person2,person3:PERSONAL_CARD
a_name,a_address,a_phone_number,a_library_card:STRING
a_age:INTEGER
data,next:D
data2:POL
	make
		local
			st,s:STRING
		do
			create data2.make_empty
			create file.make_open_read ("input.txt")
			create dictionary.make_empty
			print("Today's date(dd.mm.yy - format) :")
			io.read_line
			s:=io.last_string.twin
			--print(s.substring (1,2))
			create data.make (s)
			get_info
			if
				a_age>12
			then
				create person1.make(a_name,a_address,a_phone_number,a_library_card,a_age)
			else
				create person1.make2(a_name,a_address,a_phone_number,a_library_card,a_age)
			end

			dict
			io.readline
			st:=io.laststring.twin
			check_book_in_library(person1,st)

			print("Date,when you pass(dd.mm.yy - format) :")
			io.read_line
			s:=io.last_string.twin
			create next.make(s)
			compare(data2,next)

		end

compare(must:POL;cur:D)
local
	c,m:INTEGER
	flag:BOOLEAN
do
	c:=cur.year*365+cur.month*30 + cur.day
	m:=must.year*365 + must.month*30 + must.day
	print("%N")
	if
		m-c>=0
	then
		print("Everything is fine,you passed in time.Goodbye!%N")
	else
		c:=c-m
		print("You had to pass the book eairlier, " + c.out + " days ago!%N")
		c:=c*100
		print("Now,you should pay tax: " + c.out + " rubles. Next time be more careful!%N")
	end
end

set_data(i:INTEGER)
do
	if i=2 then
		if data.day<=16 then
			io.put_integer(data.day+14)
			data2.set_day (data.day+14)
			data2.set_month (data.month)
			data2.set_year (data.year)
			print("."+data.month.out + "." + data.year.out + "%N")
		else
			if data.month/=12 then
				io.put_integer(14+data.day-30)
				data2.set_day (14+data.day-30)
				data2.set_month (data.month+1)
				data2.set_year (data.year)
				print(".")
				io.put_integer (data.month+1)
				print("."+ data.year.out +"%N")
			else
				io.put_integer(14+data.day-30)
				data2.set_day (14+data.day-30)
				data2.set_month (1)
				data2.set_year (data.year+1)
				print(".")
				io.put_integer (1)
				print(".")
				io.put_integer (data.year+1)
				print("%N")
			end
		end
	else
		if data.day<=9 then
			io.put_integer(data.day+21)

			data2.set_day (data.day+21)
			data2.set_month (data.month)
			data2.set_year (data.year)
			print("."+data.month.out + "." + data.year.out + "%N")
		else
			if data.month/=12 then
				io.put_integer(21+data.day-30)

				data2.set_day (21+data.day-30)
				data2.set_month (data.month+1)
				data2.set_year (data.year)
				print(".")
				io.put_integer (data.month+1)
				print("."+ data.year.out +"%N")
			else
				io.put_integer(21+data.day-30)

				data2.set_day (14+data.day-30)
				data2.set_month (1)
				data2.set_year (data.year+1)
				print(".")
				io.put_integer (1)
				print(".")
				io.put_integer (data.year+1)
				print("%N")
			end
		end
	end

end

check_book_in_library(person:PERSONAL_CARD;s:STRING)
local
	i:INTEGER
	flag:BOOLEAN
do
	across dictionary as d
	loop
		if d.item.st ~ s then
			flag:=true
			i:=d.cursor_index
		end
	end

	if
		flag
	then
		if
			dictionary[i].best_seller or dictionary[i].for_adults
		then
			if dictionary[i].best_seller then
				if person.current_books<5 then
					print("The book - " + dictionary[i].st + " will be given you (" + person.name +"," +  person.age.out + " years old"  + ") for 2 weeks,until ")
					set_data(2)
					person.set_current_books(1)
				else
					if person.children then
						print("You can't bring more than 5 books!")
					else
						print("The book - " + dictionary[i].st + " will be given you (" + person.name +"," +  person.age.out + " years old"  + ") for 2 weeks,until ")
						set_data(2)
						person.set_current_books(1)
					end
				end
			else
				if person.children then
					print("It's restricted for you.%N")
				else
					print("The book - " + dictionary[i].st + " will be given you ( " + person.name +"," +  person.age.out + " years old"  + ") for 3 weeks,until ")
					set_data(3)
					person.set_current_books(1)
				end

			end

		else
			if
				person.current_books<5
			then
				print("The book - " + dictionary[i].st + " is given you (" + person.name + "," +  person.age.out + " years old"  + ") for 3 weeks,until ")
				set_data(3)
				person.set_current_books(1)
			else
				print("You can't bring more than 5 books!")
			end

		end
	else
		print("There isn't have such book.Sorry.%N")
	end
end



get_info
do
	io.putstring ("Give personal information about the reader.%N")
	io.putstring("Name:  ")
	io.read_line

	a_name:=io.last_string.twin
	io.putstring ("Age:  ")
	io.read_integer
	a_age:=io.last_integer.twin
	io.putstring ("Address:  ")
	io.read_line
	a_address:=io.last_string.twin
	io.putstring ("Phone Number:  ")
	io.read_line
	a_phone_number:=io.last_string.twin
	io.putstring ("Library card:  ")
	io.read_line
	a_library_card:=io.last_string.twin

end

dict
local
	s1:STRING
	cnt:INTEGER
	a:LIBRARY
do
	cnt:=1

			print("%N")
			from
				file.readline
			until
				file.exhausted
			loop

				s1 := file.laststring.twin
				--print(s1+"%N")
				if cnt=1 then
					create a.make_usual (s1)
					print(s1+"       ---   without any restrictions.%N")

					end
				if cnt=2 then
					create a.make_best_seller (s1)
					print(s1+"       ---   best-seller%N")

					end
				if cnt=3 then
					create a.make_for_adults (s1)
					print(s1+"    ---      only for adults%N")
					cnt:=0
					end
				cnt:=cnt+1
				dictionary.force (a.twin, dictionary.count + 1)

				file.readline
			end
			print("Choose a book:%N")
			file.close
end


end
