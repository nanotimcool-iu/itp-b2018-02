note
	description: "Summary description for {D}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	D
create
	make
feature
	day,month,year:INTEGER
	make(s:STRING)
	do
		day:=s.substring (1,2).to_integer
		month:=s.substring (4,5).to_integer
		year:=s.substring (7,8).to_integer
	end

end
