note
	description: "Summary description for {LIBRARY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LIBRARY
create
	make_usual,make_best_seller,make_for_adults
feature


best_seller:BOOLEAN
for_adults:BOOLEAN
st:STRING
make_usual(a_st:STRING)
do
	st:=a_st
end
make_best_seller(a_st:STRING)
do
	st:=a_st
	best_seller:=true
end
make_for_adults(a_st:STRING)
do
	st:=a_st
	for_adults:=true
end






end
