note
	description: "Summary description for {PERSONAL_CARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSONAL_CARD

create
	make,make2
feature
age:INTEGER
children:BOOLEAN
current_books:INTEGER
name,address,phone_number,library_card:STRING
	make(a_name,a_address,a_phone_number,a_library_card:STRING;a_age:INTEGER)
	do
		name:=a_name
		age:=a_age
		address:=a_address
		phone_number:=a_phone_number
		library_card:=a_library_card
	end
	make2(a_name,a_address,a_phone_number,a_library_card:STRING;a_age:INTEGER)
	do
		children:=true
		name:=a_name
		age:=a_age
		address:=a_address
		phone_number:=a_phone_number
		library_card:=a_library_card
	end
set_current_books(a:INTEGER)
do
	current_books:=current_books+a
end
end
