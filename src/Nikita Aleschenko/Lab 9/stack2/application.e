note
	description: "stack application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			a:MY_STACK[INTEGER]
			b:BOU_STACK[INTEGER]
		do
			--| Add your code here
			--print ("Hello Eiffel World!%N")
			create a.make
			a.push (5)
			a.push (3)
			print(a.head.out + "%N")
			a.remove
			print(a.head.out + "%N")
			a.remove
			print(a.lenght.out)
		--	a.remove
			print("%N")
			print("%N")
			create b.make
			b.set_capacity (2)
			b.push (2)
			print(b.head.out + "%N")
			b.push (3)
			print(b.head.out + "%N")
			b.push (1)


		end

end
