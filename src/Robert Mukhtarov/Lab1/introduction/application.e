class
	APPLICATION

create
	execute

feature {NONE} -- Initialization

	execute
			-- Run application.
		do
			Io.put_string ("Name: Robert Mukhtarov")
			Io.new_line
			Io.put_string ("Age: 18")
			Io.new_line
			Io.put_string ("Mother Tongue: Russian")
			Io.new_line
			Io.put_string ("Has a cat: True")
		end

end
