﻿class
	BOARD

create
	make

feature
	board: ARRAY [SQUARE]
	properties: ARRAY [PROPERTY]

	make
		local
			prop: PROPERTY
			go, free_parking, go_to_jail, jail, tax: SQUARE
			chance1, chance2, chance3: CHANCE
		do
			create go.make ("Go"); create free_parking.make ("Free Parking"); create go_to_jail.make ("Go to Jail"); create jail.make ("Jail")
			create board.make_filled (go, 1, 20)
			create prop.make_property ("null", 1, 1)
			create properties.make_filled (prop, 1, 20)
			board [11] := free_parking; board [16] := go_to_jail; board [6] := jail

			properties [2] := create {PROPERTY}.make_property ("Christ the Redeemer", 60, 2)
			properties [3] := create {PROPERTY}.make_property ("Luang Pho To", 60, 4)
			properties [5] := create {PROPERTY}.make_property ("Alyosha monument", 80, 4)
			board [2] := properties [2]; board [3] := properties [3]; board [5] := properties [5]

			properties [7] := create {PROPERTY}.make_property ("Tokyo Wan Kannon", 100, 6)
			properties [8] := create {PROPERTY}.make_property ("Luangpho Yai", 120, 8)
			properties [10] := create {PROPERTY}.make_property ("The Motherland", 160, 12)
			board [7] := properties [7]; board [8] := properties [8]; board [10] := properties [10]

			properties [12] := create {PROPERTY}.make_property ("Awaji Kannon", 220, 18)
			properties [14] := create {PROPERTY}.make_property ("Rodina-Mat' Zovyot!", 260, 22)
			properties [15] := create {PROPERTY}.make_property ("Great Buddha of Thailand", 280, 22)
			board [12] := properties [12]; board [14] := properties [14]; board [15] := properties [15]

			properties [17] := create {PROPERTY}.make_property ("Laykyun Setkyar", 320, 28)
			properties [18] := create {PROPERTY}.make_property ("Spring Temple Buddha", 360, 35)
			properties [20] := create {PROPERTY}.make_property ("Statue of Unity", 400, 50)
			board [17] := properties [17]; board [18] := properties [18]; board [20] := properties [20]

			create chance1.make_chance; create chance2.make_chance; create chance3.make_chance
			board [9] := chance1; board [13] := chance1; board [19] := chance1

			create tax.make ("Income tax")
			board [4] := tax

		end
end
