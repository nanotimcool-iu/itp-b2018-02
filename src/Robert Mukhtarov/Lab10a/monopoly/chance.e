class
	CHANCE

inherit
	SQUARE

create
	make_chance

feature
	make_chance
		do
			title := "Chance"
		end

	chance_result: INTEGER
		local
			selected: INTEGER
		do
			selected := random (2)
			if selected = 1 then
				Result := gain
			else Result := lose
			end
		end

	gain: INTEGER
		do
			Result := random (200)
			from until Result \\ 10 = 0 loop
				Result := Result + 1
			end
		end

	lose: INTEGER
		do
			Result := random (300)
			from until Result \\ 10 = 0 loop
				Result := Result + 1
			end
			Result := -Result
		end

	random (max: INTEGER): INTEGER
	-- Random number from 1 to max.
			local
				r: RANDOM
				l_time: TIME -- From Time library
	     		l_seed: INTEGER
			do
				create l_time.make_now
		     	l_seed := l_time.hour
		      	l_seed := l_seed * 60 + l_time.minute
		      	l_seed := l_seed * 60 + l_time.second
		      	l_seed := l_seed * 1000 + l_time.milli_second
		     	create r.set_seed (l_seed)
				r.start
		        Result := r.item \\ max + 1
			end
end
