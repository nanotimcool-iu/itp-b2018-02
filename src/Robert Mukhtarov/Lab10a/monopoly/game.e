class
	GAME

create
	make

feature
	board: BOARD
	players: ARRAY [PLAYER]
	setup_correct, enough_rounds, one_player_left: BOOLEAN
	num_of_players, rounds, max_money: INTEGER

	make
		local
			i, j, dice1, dice2, chance_result, tax: INTEGER
			player: PLAYER
			player_position: SQUARE
			prop: PROPERTY
			chance: CHANCE
			winners: ARRAYED_LIST [INTEGER]
		do
			-- Create the board
			create board.make; create chance.make_chance
			print ("Welcome to Monopoly!%N")

			-- Check whether a user entered a correct number of players
			from until setup_correct loop
				print ("Enter the number of players: ")
				Io.read_integer
				num_of_players := Io.last_integer
				if num_of_players < 2 or num_of_players > 6 then
					print ("Please enter a correct value. The number of players must be 2 to 6.%N")
				else setup_correct := True
				end
			end

			-- Fill players array with objects of class PLAYER
			create player.make -- object that is cloned
			create players.make_filled (player, 1, num_of_players)
			from i := 1 until i > num_of_players loop
				players [i] := player.standard_twin
				i := i + 1
			end

			-- The game itself
			i := 1 -- Current player number
			from until enough_rounds or one_player_left loop
				from until not players [i].bankrupt loop i := i + 1 end
				dice1 := random_number
				-- Check if a player is in jail.
				print ("Player " + i.out + ", it's your turn! ")
				if players [i].in_jail then
					print ("%NYou are in jail. You can either pay the 50K fine or try to throw doubles%N")
					print ("You have " + (3 - players [i].jail_counter).out + " attempts left%N")
					print ("Would you like to try your luck? (y / n)%N ")
					Io.read_line
					dice2 := random_number
					if Io.last_string.is_equal ("y") then
						print ("You got " + dice1.out + " and " + dice2.out + "%N")
						if dice1 = dice2 then
							players [i].return_from_jail
							print ("Congratulations! You broke out of jail!%N")
						else
							players [i].inc_jail_counter
							print ("You didn't manage to break out of jail%N")
							if players [i].jail_counter = 3 then
								print ("You had to pay the 50K fine%N")
								players [i].return_from_jail
								players [i].subtract_money (50)
							end
						end
					else
						print ("You've paid the 50K fine%N")
						players [i].return_from_jail
						players [i].subtract_money (50)
					end
				else -- Not in jail
					print ("Roll dice (Press Enter)")
					Io.read_line
					dice2 := random_number
					print ("You got " + dice1.out + " and " + dice2.out + "%N")
				end

				if not players [i].in_jail then
					players [i].move (dice1 + dice2)
					player_position := board.board [players [i].position]
					prop := board.properties [players [i].position]
					print ("You're in " + player_position.title + "%N")

					-- Conditions
					if player_position.title.is_equal ("Go") then -- Go
						players [i].add_money (200)
						print ("Get 200K salary!")

					elseif  player_position.title.is_equal ("Chance") then -- Chance
						chance_result := chance.chance_result
						if chance_result > 0 then
							players [i].add_money (chance_result)
							print ("Congratulations! You got " + chance_result.out + "K")
						else
							-- Here chance_result is negative, so we use add_money in both cases
							players [i].add_money (chance_result)
							chance_result := -chance_result
							print ("You have to pay " + chance_result.out + "K")
						end

					elseif player_position.title.is_equal ("Income tax") then -- Tax
						tax := (players [i].money * 0.1).ceiling
						from until tax \\ 10 = 0 loop
							tax := tax + 1
						end
						players [i].subtract_money (tax)
						print ("You have to pay " + tax.out + "K")

					elseif player_position.title.is_equal ("Free Parking") then -- Free Parking
						print ("Have a good rest.")

					elseif player_position.title.is_equal ("Jail") then -- Jail
						print ("Just visiting")

					elseif player_position.title.is_equal ("Go to Jail") then -- Go to Jail
						players [i].send_to_jail
						print ("You've been sent to jail")

					else
						if prop.owner = Void then
							print ("Would you like to buy it for " + prop.price.out + "K? (y / n)%N")
							Io.read_line
							if Io.last_string.is_equal ("y") then
								if players [i].money < prop.price then
									print ("Unfortunately, you do not have enough money")
								else
									prop.attach_to_player (players [i])
									players [i].subtract_money (prop.price)
									print ("You've bought " + prop.title)
								end
							end
						else
							if prop.owner = players [i] then
								print ("It's your property")
							else
								players [i].subtract_money (prop.rent)
								if attached prop.owner as ow then
									ow.add_money (prop.rent)
								end
								print ("You've paid " + prop.rent.out + "K to the owner of this property")
							end
						end
					end
					-- End of conditions
				end

				print ("%NYour current amount of money is " + players [i].money.out + "K%N%N")

				-- Check if bankrupt
				if players [i].money <= 0 then
					players [i].make_bankrupt
					print ("Player " + i.out + " went bankrupt.%N%N")
					from j := 1 until j > 20 loop
						if board.properties [j].owner = players [i] then
							board.properties [j].make_unowned
						end
					end
				end

				if i = num_of_players then
					i := 1
					rounds := rounds + 1
				else
					i := i + 1
				end

				one_player_left := check_bankrupts
				if rounds > 100 then
					enough_rounds := True
				end
			end

			if one_player_left then
				from i := 1 until not players [i].bankrupt loop i := i + 1 end
				print ("Player " + i.out + " is the winner!")
			else
				from i := 1 until i > players.count loop
					if players [i].money > max_money and players [i].bankrupt = False then
						max_money := players [i].money
					end
					i := i + 1
				end
				create winners.make (0)
				from i := 1 until i > players.count loop
					if players [i].money = max_money and players [i].bankrupt = False then
						winners.extend (i)
					end
					i := i + 1
				end
				if winners.count = 1 then
					print ("Player " + winners [1].out + " is the winner!")
				else
					print ("Players ")
					from i := 1 until i > winners.count loop
						print (winners [i].out)
						if i /= winners.count then print (", ")
					end
					print (" are the winners!")
				end
			end
		end
	end


		check_bankrupts: BOOLEAN
		-- Returns True if only one player left
			local
				counter, i: INTEGER
			do
				from i := 1 until i > players.count loop
					if not players [i].bankrupt then
						counter := counter + 1
					end
					i := i + 1
				end
				if counter = 1 then Result := True end
			end

		random_number: INTEGER
		-- Returns a random number from 1 to 4 (for 4-sided dice)
			local
				r: RANDOM
				l_time: TIME -- From Time library
	     		l_seed: INTEGER
			do
				create l_time.make_now
		     	l_seed := l_time.hour
		      	l_seed := l_seed * 60 + l_time.minute
		      	l_seed := l_seed * 60 + l_time.second
		      	l_seed := l_seed * 1000 + l_time.milli_second
		     	create r.set_seed (l_seed)
				r.start
		        Result := r.item \\ 4 + 1
			end
end
