class
	PLAYER

create
	make

feature
	money, position, jail_counter: INTEGER
	in_jail, bankrupt: BOOLEAN

	make
		do
			money := 1500
			in_jail := False
			bankrupt := False
			position := 1
			jail_counter := 0
		end

	set_money (m: INTEGER)
		do
			money := m
		end

	send_to_jail
		do
			in_jail := True
			position := 6
		end

	return_from_jail
		do
			in_jail := False
			jail_counter := 0
		end

	inc_jail_counter
		do
			jail_counter := jail_counter + 1
		end

	add_money (m: INTEGER)
		do
			money := money + m
		end

	subtract_money (m: INTEGER)
		do
			money := money - m
		end

	make_bankrupt
		do
			bankrupt := True
		end

	move (n: INTEGER)
		do
			position := position + n
			if position > 20 then
				position := position - 20
				if position /= 1 then
					add_money (200)
					print("You've passed through Go and got 200K salary.%N")
				end
			end
		end
end
