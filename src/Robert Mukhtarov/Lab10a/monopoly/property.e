class
	PROPERTY

inherit
	SQUARE

create
	make_property

feature
	price, rent: INTEGER
	owner: detachable PLAYER

	make_property (t: STRING; p, r: INTEGER)
		do
			title := t
			price := p
			rent := r
		end

	attach_to_player (p: PLAYER)
		do
			owner := p
		end

	make_unowned
		do
			owner := Void
		end

end
