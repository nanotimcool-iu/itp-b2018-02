class
	SQUARE

create
	make

feature
	title: STRING

	make (a_title: STRING)
		do
			title := a_title
		end

end
