class
	APPLICATION

create
	make

feature {NONE}
	make
		local
			coffee_shop: COFFEE_SHOP
		do
			create coffee_shop.make
			coffee_shop.print_menu
			print ("%N%N")
			coffee_shop.menu [1].buy (7)
			coffee_shop.menu [2].buy (15)
			coffee_shop.menu [3].buy (3)
			coffee_shop.profit
		end

end
