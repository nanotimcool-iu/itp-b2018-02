class
	COFFEE_SHOP
create
	make

feature
	menu: ARRAYED_LIST [PRODUCT]

	make
		local
			espresso, cappuccino: COFFEE
			cake: CAKE
		do
			create menu.make (0)
			create {ESPRESSO} espresso.make (50, 80)
			create {CAPPUCCINO} cappuccino.make (60, 100)
			create cake.make (100, 200)
			menu.extend (espresso)
			menu.extend (cappuccino)
			menu.extend (cake)
		end

	profit
		do
			print ("Profit:%N")
			print ("Espresso: " + menu [1].profit.out)
			print ("%NCappuccino: " + menu [2].profit.out)
			print ("%NCake: " + menu [3].profit.out)
		end

	print_menu
		do
			print ("Menu:%N")
			print ("Product: description, price/item%N")
			print ("Espresso: " + menu [1].description + "     " + menu [1].price_public.out)
			print ("%NCappuccino: " + menu [2].description + "     " + menu [2].price_public.out)
			print ("%NCake: " + menu [3].description + "     " + menu [3].price_public.out)
		end
end
