deferred class
	PRODUCT
feature
	make (r: REAL; public: REAL)
		require
			r_non_negative: r >= 0
		do
			price := r
			price_public := public
			profit := 0
		ensure
			price_set: price = r
			price_public_set: price_public = public
		end

feature {COFFEE_SHOP}
	price, price_public, profit: REAL
	items_sold: INTEGER

	description: STRING
		deferred end

	set_price (r: REAL)
		do
			price := r
		end

	set_public_price (public: REAL)
		do
			price_public := public
		end

feature
	buy (num_of_items: INTEGER)
		local
			i: INTEGER
		do
			from i := 1 until i > num_of_items loop
				items_sold := items_sold + 1
				profit := profit + (price_public - price)
				i := i + 1
			end
		end

invariant
	non_negative_price: price >= 0.0
	public_is_more: price_public > price
	valid_description: not description.is_empty

end
