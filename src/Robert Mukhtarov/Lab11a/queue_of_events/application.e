note
	description: "Testing the queue of events implementation"
class
	APPLICATION

create
	make

feature {NONE}
	event_queue: EVENT_QUEUE

	make
		local
			i: INTEGER
		do
			create event_queue.make
			-- Adding event
			event_queue.add_event (create {EVENT}.make (1))
			event_queue.add_event (create {EVENT}.make (-1))
			event_queue.add_event (create {EVENT}.make (2))
			event_queue.add_event (create {EVENT}.make (3))
			event_queue.add_event (create {EVENT}.make (3))
			event_queue.add_event (create {EVENT}.make (1))
			test ("1 2 3 ")
			--Discard test
			event_queue.discard_event
			event_queue.discard_event
			test ("3 ")
			-- Test that program discards no more than a queue has
			event_queue.discard_event
			event_queue.discard_event
			test ("")
			-- Test 100 000 events acceptance
			from i := 1 until i > 100000 loop
				event_queue.add_event (create {EVENT}.make (i))
				i := i + 1
			end
			if event_queue.get_length = 100000 then print ("SUCCESS%N") else print ("FAIL%N") end
			-- Test extracting
			test_extract (1)
			event_queue.discard_event
			test_extract (2)
			event_queue.discard_event
		end

	test (s: STRING)
		do
			if event_queue.get_events.is_equal (s) then
				print ("SUCCESS%N")
			else
				print ("FAIL%N")
			end
		end

	test_extract (n: INTEGER)
		do
			if event_queue.extract.time_tag  = n then
				print ("SUCCESS%N")
			else
				print ("FAIL%N")
			end
		end

end
