class
	EVENT
create
	make

feature
	time_tag: INTEGER

	make (tag: INTEGER)
		do
			time_tag := tag
		end

end
