class
	EVENT_QUEUE
create
	make

feature
	queue: ARRAYED_LIST [EVENT]

	make
		do
			create queue.make (0)
		end

	extract: EVENT
		do
			Result := queue [1]
		end

	add_event (e: EVENT)
		do
			if e.time_tag >= 0 then
				if queue.count > 0 then
					if e.time_tag > queue.last.time_tag then
						queue.extend (e)
					end
				else
					queue.extend (e)
				end
			end
		end

	discard_event
		do
			if queue.count >= 1 then
				queue.prune (queue [1])
			end
		end

	get_length: INTEGER
		do
			Result := queue.count
		end

	get_events: STRING
		local
			i: INTEGER
		do
			Result := ""
			from i := 1 until i > queue.count loop
				Result := Result + queue [i].time_tag.out + " "
				i := i + 1
			end
		end
end
