class
	APPLICATION

create
	make

feature {NONE}
	make
		do
			test (anagram ("ab", 1), "ab ba ")
			test (anagram ("abc", 1), "abc acb bac bca cba cab ")
			test (anagram ("abcd", 1), "abcd abdc acbd acdb adcb adbc bacd badc bcad bcda bdca bdac cbad cbda cabd cadb cdab cdba dbca dbac dcba dcab dacb dabc ")
		end

	anagram (word: STRING; i: INTEGER): STRING
		local
			j: INTEGER
			c: CHARACTER
	-- First i characters will remain unchanged. Pass 1 for all anagrams.
		do
			Result := ""
			if i = word.count then Result := word + " "
			else
				from j := i until j > word.count loop
					c := word [i]
					word [i] := word [j]
					word [j] := c
					Result := Result + anagram (word, i + 1)
					c := word [i]
					word [i] := word [j]
					word [j] := c
					j := j + 1
				end
			end
		end

		test (res, expected: STRING)
			do
				if res.is_equal (expected) then
					print ("PASS%N")
				else
					print ("FAIL%N")
				end
			end
end
