class
	APPLICATION

create
	make

feature
	make
		do
			test (0, "leap")
			test (1600, "leap")
			test (1896, "leap")
			test (1968, "leap")
			test (2000, "leap")
			test (2008, "leap")
			test (2012, "leap")
			test (2016, "leap")
			test (100, "not leap")
			test (401, "not leap")
			test (1862, "not leap")
			test (1709, "not leap")
			test (1970, "not leap")
			test (1843, "not leap")
			test (2017, "not leap")
			test (2018, "not leap")
		end

	is_year_leap (year: INTEGER): STRING
		do
			if year \\ 400 = 0  or (year \\ 4 = 0 and year \\ 100 /= 0) then
				Result := "leap"
			else
				Result := "not leap"
			end
		end

	test (year: INTEGER; expected: STRING)
		do
			if is_year_leap (year).is_equal (expected) then
				print ("PASS%N")
			else
				print ("FAIL%N")
			end
		end
end
