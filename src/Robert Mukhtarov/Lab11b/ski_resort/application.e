class
	APPLICATION

create
	make

feature
	make
	-- Testing
		local
			ski_resort: SKI_RESORT
			i: INTEGER
		do
			create	ski_resort.make
			ski_resort.stock
			ski_resort.show_rentals -- Must print an empty table
			ski_resort.rent ("John", "November 1", <<"Skies">>)
			print ("%N")
			ski_resort.stock -- Skies should be 99
			ski_resort.show_rentals -- Should print 1 rental info (John, Nov 1, Skies)
			ski_resort.rent ("Jack", "November 2", <<"Snowboard">>)
			print("%N")
			ski_resort.stock -- Snowboard must be 99
			ski_resort.show_rentals -- Should print 2 rentals
			ski_resort.rent ("Ivan", "November 3", <<"Snowboard", "Helmet", "Goggles", "Snowboard boots">>)
			print("%N")
			ski_resort.stock -- Selected items must decrease
			ski_resort.show_rentals
			ski_resort.rent ("Sergey", "November 3", <<"Skies", "Helmet", "Goggles", "Ski boots", "Ski pass", "Ski sticks">>)
			print("%N")
			ski_resort.stock -- Selected items must decrease
			ski_resort.show_rentals
			-- Trying wrong secondary equipment with snowboard, error expected
			ski_resort.rent ("Misha", "November 4", <<"Snowboard", "Helmet", "Goggles", "Ski boots", "Ski sticks">>)
			print("%N")
			ski_resort.stock -- Selected items shouldn't change
			ski_resort.show_rentals
			-- Trying wrong secondary equipment with skies, error expected
			ski_resort.rent ("Misha", "November 4", <<"Skies", "Helmet", "Goggles", "Snowboard boots">>)
			print("%N")
			ski_resort.stock -- Selected items shouldn't change
			ski_resort.show_rentals
			-- Trying secondary equipment without primary, error expected
			ski_resort.rent ("Alex", "November 5", <<"Helmet", "Goggles", "Snowboard boots">>)
			print("%N")
			ski_resort.stock -- Selected items shouldn't change
			ski_resort.show_rentals
			ski_resort.return (ski_resort.rentals [1])
			print("%N")
			ski_resort.stock -- Items from rental #1 must be back
			ski_resort.show_rentals -- First rental must be deleted
			ski_resort.return (ski_resort.rentals [3])
			print("%N")
			ski_resort.stock -- Items from rental #3 must be back
			ski_resort.show_rentals -- First rental must be deleted
			from i := 1 until i > 50 loop -- Should print that there are no ski passes left.
				ski_resort.rent ("Alex", "January 1", <<"Snowboard", "Ski pass">>)
				i := i + 1
			end
		end

end
