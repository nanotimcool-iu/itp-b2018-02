class
	EQUIPMENT
create
	make

feature
	quantity: INTEGER
	name: STRING
	for_snowboard, for_skies: BOOLEAN

	make (n: STRING; q: INTEGER; p, fsn, fsk: BOOLEAN)
		do
			quantity := q
			name := n
			for_snowboard := fsn
			for_skies := fsk
		end

	add (q: INTEGER)
		do
			quantity := quantity + q
		end

	subtract (q: INTEGER)
		do
			quantity := quantity - q
		end
end
