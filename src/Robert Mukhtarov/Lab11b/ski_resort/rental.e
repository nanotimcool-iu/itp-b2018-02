class
	RENTAL
create
	make

feature
	person, date: STRING
	equipment: ARRAY [STRING]

	make (p, d: STRING; e: ARRAY [STRING])
		do
			person := p
			date := d
			equipment := e
		end
end
