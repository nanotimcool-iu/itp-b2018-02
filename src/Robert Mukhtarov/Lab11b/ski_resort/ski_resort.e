class
	SKI_RESORT

create
	make

feature
	equipment: ARRAYED_LIST [EQUIPMENT]
	rentals: ARRAYED_LIST [RENTAL]

	make
		do
			create equipment.make (0)
			create rentals.make (0)
			equipment.extend (create {EQUIPMENT}.make ("Skies", 100, True, False, True))
			equipment.extend (create {EQUIPMENT}.make ("Snowboard", 100, True, True, False))
			equipment.extend (create {EQUIPMENT}.make ("Helmet", 90, False, True, True))
			equipment.extend (create {EQUIPMENT}.make ("Goggles", 50, False, True, True))
			equipment.extend (create {EQUIPMENT}.make ("Ski sticks", 70, False, False, True))
			equipment.extend (create {EQUIPMENT}.make ("Ski boots", 90, False, False, True))
			equipment.extend (create {EQUIPMENT}.make ("Snowboard boots", 90, False, True, False))
			equipment.extend (create {EQUIPMENT}.make ("Ski pass", 50, False, True, True))
		end

	add_equipment (e: EQUIPMENT)
		do
			equipment.extend (e)
		end

	stock
		local
			i: INTEGER
		do
			print ("Equipment: Quantity%N")
			from i := 1 until i > equipment.count loop
				print (i.out + ". " + equipment [i].name + ": " + equipment [i].quantity.out + "%N")
				i := i + 1
			end
		end

	show_rentals
		local
			i, j: INTEGER
		do
			print ("Current rentals:%NPerson / Date / Equipment%N")
			from i := 1 until i > rentals.count loop
				print (i.out + ". " + rentals [i].person + " / " + rentals [i].date + " / ")
				from j := 1 until j > rentals [i].equipment.count loop
					print (rentals [i].equipment [j] + " ")
					j := j + 1
				end
				print ("%N")
				i := i + 1
			end
		end

		rent (person, date: STRING; eq: ARRAY [STRING])
			local
				i, j: INTEGER
				primary_rented: STRING
			do
				primary_rented := "none"
				from i := 1 until i > eq.count loop
					if eq [i].is_equal ("Skies") then
						if primary_rented.is_equal ("none") or primary_rented.is_equal ("Skies") then
							primary_rented := "Skies"
						else
							print ("A person can rent primary equipment of only 1 type.")
						end
					end
					if eq [i].is_equal ("Snowboard") then
						if primary_rented.is_equal ("none") or primary_rented.is_equal ("Snowboard") then
							primary_rented := "Snowboard"
						else
							print ("A person can rent primary equipment only of only 1 type.")
						end
					end
					i := i + 1
				end

				if not primary_rented.is_equal ("none") then
					if selected_correctly (eq, primary_rented) then
						give_to_person (eq, primary_rented)
						rentals.extend (create {RENTAL}.make (person, date, eq))
					end
				else
					print ("Primary equipment has to be rented.")
				end
			end

		return (rental: RENTAL)
			local
				i: INTEGER
				j: INTEGER
			do
				from i := 1 until i > equipment.count loop
					from j := 1 until j > rental.equipment.count loop
						if rental.equipment [j].is_equal (equipment [i].name) then
							equipment [i].add (1)
						end
						j := j + 1
					end
					i := i + 1
				end
				rentals.prune (rental)
			end

feature {SKI_RESORT}
		selected_correctly (person_choice: ARRAY [STRING]; primary_type: STRING): BOOLEAN
			local
				i, j: INTEGER
			do
				Result := True
				if primary_type.is_equal ("Skies") then
					from i := 1 until i > person_choice.count loop
						from j := 1 until j > equipment.count loop
							if equipment [j].name.is_equal (person_choice [i])  then
								if equipment [j].for_skies then
									if equipment [j].quantity <= 1 then
										print ("We have run out of " + equipment [j].name)
										Result := False
									end
								else
									print ("%NYou cannot select " + equipment [j].name + " with skies")
									Result := False
								end
							end
							j := j + 1
						end
						i := i + 1
					end
			else
				from i := 1 until i > person_choice.count loop
					from j := 1 until j > equipment.count loop
						if equipment [j].name.is_equal (person_choice [i]) then
							if equipment [j].for_snowboard then
								if equipment [j].quantity <= 1 then
									print ("No " + equipment [j].name + " left.")
									Result := False
								end
							else
								print("%NYou cannot select " + equipment [j].name + " with snowboard")
								Result := False
							end
						end
						j := j + 1
					end
					i := i + 1
				end
			end
		end

		give_to_person (person_choice: ARRAY [STRING]; primary_type: STRING)
			local
				i, j: INTEGER
			do
				from i := 1 until i > person_choice.count loop
					from j := 1 until j > equipment.count loop
						if equipment [j].name.is_equal (person_choice [i]) then
							equipment [j].subtract (1)
						end
						j := j + 1
					end
					i := i + 1
				end
			end
end
