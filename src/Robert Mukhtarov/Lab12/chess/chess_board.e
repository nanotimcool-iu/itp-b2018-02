class
	CHESS_BOARD
create
	make
feature
	chess_board: ARRAY2 [detachable PIECE]
	-- white pieces
	w_king, w_queen, w_rook1, w_rook2, w_knight1, w_knight2, w_bishop1, w_bishop2,
	w_pawn1, w_pawn2, w_pawn3, w_pawn4, w_pawn5, w_pawn6, w_pawn7, w_pawn8: PIECE
	-- black pieces
	b_king, b_queen, b_rook1, b_rook2, b_knight1, b_knight2, b_bishop1, b_bishop2,
	b_pawn1, b_pawn2, b_pawn3, b_pawn4, b_pawn5, b_pawn6, b_pawn7, b_pawn8: PIECE

	make
		local
			black_pieces1, black_pieces2, white_pieces1, white_pieces2: ARRAY [PIECE]
		do
			create chess_board.make_filled (Void, 8, 8)
			create {PAWN} w_pawn1.make (2, 1, "white")
			create {PAWN} w_pawn2.make (2, 2, "white")
			create {PAWN} w_pawn3.make (2, 3, "white")
			create {PAWN} w_pawn4.make (2, 4, "white")
			create {PAWN} w_pawn5.make (2, 5, "white")
			create {PAWN} w_pawn6.make (2, 6, "white")
			create {PAWN} w_pawn7.make (2, 7, "white")
			create {PAWN} w_pawn8.make (2, 8, "white")
			white_pieces2 := <<w_pawn1, w_pawn2, w_pawn3, w_pawn4, w_pawn5, w_pawn6, w_pawn7, w_pawn8>>

			create {PAWN} b_pawn1.make (7, 1, "black")
			create {PAWN} b_pawn2.make (7, 2, "black")
			create {PAWN} b_pawn3.make (7, 3, "black")
			create {PAWN} b_pawn4.make (7, 4, "black")
			create {PAWN} b_pawn5.make (7, 5, "black")
			create {PAWN} b_pawn6.make (7, 6, "black")
			create {PAWN} b_pawn7.make (7, 7, "black")
			create {PAWN} b_pawn8.make (7, 8, "black")
			black_pieces2 := <<b_pawn1, b_pawn2, b_pawn3, b_pawn4, b_pawn5, b_pawn6, b_pawn7, b_pawn8>>

			create {KING} w_king.make (1, 5, "white")
			create {QUEEN} w_queen.make (1, 4, "white")
			create {BISHOP} w_bishop1.make (1, 3, "white")
			create {BISHOP} w_bishop2.make (1, 6, "white")
			create {KNIGHT} w_knight1.make (1, 2, "white")
			create {KNIGHT} w_knight2.make (1, 7, "white")
			create {ROOK} w_rook1.make (1, 1, "white")
			create {BISHOP} w_rook2.make (1, 8, "white")
			white_pieces1 := <<w_rook1, w_knight1, w_bishop1, w_queen, w_king, w_bishop2, w_knight2, w_rook2>>

			create {KING} b_king.make (8, 5, "black")
			create {QUEEN} b_queen.make (8, 4, "black")
			create {BISHOP} b_bishop1.make (8, 3, "black")
			create {BISHOP} b_bishop2.make (8, 6, "black")
			create {KNIGHT} b_knight1.make (8, 2, "black")
			create {KNIGHT} b_knight2.make (8, 7, "black")
			create {ROOK} b_rook1.make (8, 1, "black")
			create {BISHOP} b_rook2.make (8, 8, "black")
			black_pieces1 := <<b_rook1, b_knight1, b_bishop1, b_queen, b_king, b_bishop2, b_knight2, b_rook2>>
			place_on_board (white_pieces1, white_pieces2, black_pieces1, black_pieces2)
		end

feature
	can_move (r1, c1, r2, c2: INTEGER): BOOLEAN
		do
			if chess_board.item (r1, c1) /= Void then
				if attached chess_board.item (r1, c1) as piece then
					if piece.can_move (r2, c2) then
						if chess_board.item (r2, c2) = Void then
							Result := True
						elseif attached chess_board.item (r2, c2) as second_piece then
							if not (piece.colour.is_equal (second_piece.colour)) then
								Result := True
							end
						else
							Result := False
						end
					end
				end
			end
		end

	move_piece (r1, c1, r2, c2: INTEGER)
		do
			if can_move (r1, c1, r2, c2) then
				if attached chess_board.item (r1, c1) as piece then
					piece.move (r2, c2)
					chess_board.item (r2, c2) := piece.twin
				end
				chess_board.item (r1, c1) := Void
			end
		end

feature {CHESS_BOARD}
	place_on_board (w1, w2, b1, b2: ARRAY [PIECE])
		local
			i: INTEGER
		do
			from i := 1 until i > 8 loop
				chess_board.item (1, i) := w1 [i]
				chess_board.item (2, i) := w2 [i]
				chess_board.item (7, i) := b2 [i]
				chess_board.item (8, i) := b1 [i]
				i := i + 1
			end
		end
end
