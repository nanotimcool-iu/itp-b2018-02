class
	KNIGHT
inherit
	PIECE
		redefine can_move end
create
	make
feature
	can_move (r, c: INTEGER): BOOLEAN
		do
			if Precursor (r, c) = True then
				if ((r - row).abs = 2 and (c - column).abs = 1) or ((r - row).abs = 1 and (c - column).abs = 2) then
					Result :=True
				else
					Result := False
				end
			end
		end

end
