class
	QUEEN
inherit
	PIECE
		redefine can_move end
create
	make

feature
	can_move (r, c: INTEGER): BOOLEAN
		do
			if Precursor (r, c) = True then
				if (r > row and c = 0) or (r < row and c = 0) or (c > column and r = 0) or (c < column and r = 0) or
				((row - r).abs = (column - c).abs) then
					Result := True
				else
					Result := False
				end
			end
		end
end
