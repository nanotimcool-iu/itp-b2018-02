class
	BIO_INFORMATIC
inherit
	BIOLOGIST redefine introduce end
	COMPUTER_SCIENTIST undefine make, introduce end
create
	make_bio_inf
feature
	short_biography: STRING

	make_bio_inf (a_id: INTEGER; n, bio: STRING; p: PET)
		do
			id := a_id
			name := n
			discipline := "Bio-Informatician"
			pet := p
			short_biography := bio
		end

		introduce
			do
				Precursor {BIOLOGIST}
				print ("%NShort biography: " + short_biography)
			end


end
