class
	BIOLOGIST
inherit
	SCIENTIST redefine introduce end
create
	make_biologist
feature
	pet: PET

	make_biologist (a_id: INTEGER; n: STRING; p: PET)
		do
			id := a_id
			name := n
			discipline := "Biologist"
			pet := p
		end

	introduce
		do
			Precursor
			print ("%NPet: " + pet.name)
		end

end
