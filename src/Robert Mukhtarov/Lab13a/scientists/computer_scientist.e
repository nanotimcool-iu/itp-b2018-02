class
	COMPUTER_SCIENTIST
inherit
	SCIENTIST redefine make end
create
	make

feature
	make (a_id: INTEGER; n: STRING)
		do
			id := a_id
			name := n
			discipline := "Computer Scientist"
		end


end
