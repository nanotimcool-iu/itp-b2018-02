class
	LAB
create
	make
feature
	members: ARRAYED_LIST [SCIENTIST]

	make
		do
			create members.make (0)
			add_member (create {COMPUTER_SCIENTIST}.make (1, "John"))
			add_member (create {BIOLOGIST}.make_biologist (2, "Ivan", create {PET}.make ("Kitty", "Cat")))
			add_member (create {BIO_INFORMATIC}.make_bio_inf (3, "Mark", "Have been working in Innopolis for 5 years.",  create {PET}.make ("Jack", "Dog")))
			add_member (create {COMPUTER_SCIENTIST}.make (4, "Martin"))
			remove_member (4)
			introduce_all
		end

	add_member (m: SCIENTIST)
		do
			members.extend (m)
		end

	remove_member (id: INTEGER)
		do
			across members as member loop
				if member.item.id = id then
					members.prune (member.item)
				end
			end
		end

	introduce_all
		do
			across members as member loop
				member.item.introduce
				print ("%N%N")
			end
		end

end
