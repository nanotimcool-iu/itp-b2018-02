deferred class
	SCIENTIST
feature
	id: INTEGER
	name, discipline: STRING

	make (a_id: INTEGER; n: STRING)
		do
			id := a_id
			name := n
			discipline := "N/A"
		end

	introduce
		do
			print ("I'm " + name + ".%NID: " + id.out + "%NDiscipline: " + discipline)
		end

end
