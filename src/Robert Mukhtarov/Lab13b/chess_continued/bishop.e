class
	BISHOP
inherit
	PIECE
		redefine can_move end

create
	make

feature
	can_move (r, c: INTEGER): BOOLEAN
		do
			if Precursor (r, c) = True then
				if (row - r).abs = (column - c).abs then
					Result := True
				else
					Result := False
				end
			end
		end

end
