class
	KING
inherit
	PIECE
		redefine can_move end
create
	make

feature
	can_move (r, c: INTEGER): BOOLEAN
		do
			if Precursor (r, c) = True then
				if ((r - row).abs = 1 and c = 0) or ((c - column).abs = 1 and r = 0) or ((row - r).abs = 1 and (column - c).abs = 1) then
					Result := True
				else
					Result := False
				end
			end
		end
end
