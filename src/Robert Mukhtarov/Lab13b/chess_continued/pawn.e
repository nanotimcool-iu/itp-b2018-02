class
	PAWN
inherit
	PIECE
		redefine can_move, move end
create
	make
feature
	moved: BOOLEAN

	can_move (r, c: INTEGER): BOOLEAN
		do
			if Precursor (r, c) = True then
				if not moved and (r - row = 2) then
					Result := True
				elseif r - row = 1 then
					Result := True
				else
					Result := False
				end
			end
		end

	 move (r, c: INTEGER)
	 	do
	 		Precursor (r, c)
	 		if not moved then moved := True end
	 	end
end
