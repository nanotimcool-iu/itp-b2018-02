deferred class
	PIECE

feature
	row, column: INTEGER
	colour: STRING

	make (r, c: INTEGER; col: STRING)
		do
			row := r; column := c; colour := col
		end

	move (r, c: INTEGER)
		do
			if Current.can_move (r, c) then
				row := r; column := c
			end
		end

	can_move (r, c: INTEGER): BOOLEAN
		do
			Result := True
			if not ((row <= 8 and row >= 1) and (column <= 8 and column >= 1)) then
				Result := False
			end
		end

	position
		do
			print (row.out + " " + column.out)
		end


invariant
	bounded_row: row <= 8 and row >= 1
	bounded_column: column <= 8 and column >= 1
end
