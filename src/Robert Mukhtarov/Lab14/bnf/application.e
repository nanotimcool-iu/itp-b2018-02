class
	APPLICATION

create
	make

feature
	index: INTEGER
	s: STRING

	make
		do
			bnf_check ("((x)))") -- Incorrect
			bnf_check ("(x or y implies z) ") -- Correct
		end

	bnf_check (str: STRING)
		local
			correct: BOOLEAN
		do
			index := 1
			s := str
			if s.substring (index, index + 2).is_equal ("not") and (index + 4 <= s.count) then
				index := index + 4
			end
			if s [index] = '(' and (index + 1 <= s.count) then
				index := index + 1
				correct := check_var
			else correct := check_var end
			if correct then
				correct := check_operation
			end
			if correct and s [index] = ' ' then
				do_nothing
			elseif correct and (s.count - index = 2 and (s [index] = ')' and s [index + 1] = ' ')) then
				do_nothing
			else
				correct := False
			end
			if correct then
				print ("Correct%N")
			else
				print ("Incorrect%N")
			end
		end

	check_var: BOOLEAN
		do
			if s.substring (index, index + 2).is_equal ("not") and (index + 4 <= s.count) then
				index := index + 4
			end
			if s [index] = '(' and (index + 3 >= s.count) then
				if s [index + 1] = 'x' or s [index + 1] = 'y' or s [index + 1] = 'z' then
					if s [index + 2] = ')' then
						Result := True
						index := index + 3
					end
				end
			else
				if (s [index] = 'x' or s [index] = 'y' or s [index] = 'z') and (index + 1 <= s.count) then
					Result := True
					index := index + 2
				end
			end
		end

	check_operation: BOOLEAN
		do
			if s.substring (index, index + 1).is_equal ("or") then
				index := index + 3
				Result := check_var
			end
			if s.substring (index, index + 2).is_equal ("and") then
				index := index + 4
				Result := check_var
			end
			if s.substring (index, index + 6).is_equal ("implies") then
				index := index + 8
				Result := check_var
			end
		end
end
