class
	BANK_ACCOUNT

create
	make

feature
	name: STRING
	balance: INTEGER

feature
	make (a_name: STRING; a_balance: INTEGER)
		do
			if a_balance < 100 then
				balance := 100
				name := a_name
				Io.put_string("Error: balance can't be less than 100. Set to 100.")
			elseif a_balance > 1000000 then
				balance := 1000000
				name := a_name
				Io.put_string("Error: balance can't be more than 1000000. Set to 1000000.")
			else
				name := a_name
				balance := a_balance
			end
		end

	deposit(money: INTEGER)
		do
			if balance + money > 1000000
			then balance := balance + money
			else Io.put_string ("Error: exceeded maximum balance.")
			end
		end

	withdraw(money: INTEGER)
		do
			if balance - money < 100
			then balance := balance - money
			else Io.put_string ("Error: balance cannot be less than 100 rubles.")
			end
		end

	get_name: STRING
		do
			Result := name
		end

	get_balance: INTEGER
		do
			Result := balance
		end
end
