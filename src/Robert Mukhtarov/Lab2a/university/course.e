class
	COURSE

create
	create_class

feature
	name: STRING
	identifier: INTEGER
	schedule: STRING
	max_number: INTEGER

feature
	create_class (a_name: STRING; a_id: INTEGER; a_schedule: STRING; a_max_number: INTEGER)
		do
			if a_max_number >= 3 then
				set_name(a_name)
				set_id(a_id)
				set_schedule(a_schedule)
				set_max_number(a_max_number)
			else
				set_name(a_name)
				set_id(a_id)
				set_schedule(a_schedule)
				set_max_number(3)
				Io.put_string ("Error: minimum 3 students have to enrolled. The maximum number was set to 3.")
			end
		end

	set_name(a_name: STRING)
		do
			name := a_name
		end

	set_id(a_id: INTEGER)
		do
			identifier := a_id
		end

	set_schedule(a_schedule: STRING)
		do
			schedule := a_schedule
		end

	set_max_number(a_max_number: INTEGER)
		do
			if a_max_number >= 3 then
				max_number := a_max_number
			else Io.put_string ("Error: minimum 3 students have to enrolled.")
			end
		end

	get_name: STRING
		do
			Result := name
		end

	get_id: INTEGER
		do
			Result :=  identifier
		end

	get_schedule: STRING
		do
			Result := schedule
		end

	get_max_number: INTEGER
		do
			Result := max_number
		end
end
