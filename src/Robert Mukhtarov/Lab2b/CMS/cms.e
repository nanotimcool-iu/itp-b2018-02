class
	CMS

create
	make

feature
	make
		local
			c1: CONTACT
			c2: CONTACT
			c3: CONTACT
		do
			--Trying out implementation
			c1 := create_contact("John", 8912345678, "Innopolis University", "john@gmail.com")
			c2 := create_contact("Ivan", 8957327845, "Yandex", "ivan@gmail.com")
			c3 := create_contact("Alex", 8468375947, "JetBrains", "alex@gmail.com")
			add_emergency_contact(c1, c2)
			add_emergency_contact(c2, c3)
			Io.put_string ("C1: " + contact_info(c1) + "%N")
			Io.put_string ("C2: " + contact_info(c2) + "%N")
			Io.put_string ("C3: " + contact_info(c3) + "%N")
			Io.put_string ("C1's emergency contact: " + emergency_contact_info(c1.get_emergency) + "%N")
			Io.put_string ("C2's emergency contact: " + emergency_contact_info(c2.get_emergency) + "%N" + "%N")

			edit_contact(c2, "", 8236487623, "", "ivanthebestcoder@gmail.com")
			Io.put_string ("Edited C2: " +  contact_info(c2) + "%N" + "%N")

			remove_emergency_contact(c2)
			remove_emergency_contact(c3)
			Io.put_string ("C2's updated emergency contact: " + emergency_contact_info(c2.get_emergency) + "%N")
			Io.put_string ("C3's updated emergency contact: " + emergency_contact_info(c3.get_emergency) + "%N")
		end

feature
	create_contact(a_name: STRING; a_phone_number: INTEGER_64; a_work_place: STRING; a_email: STRING): CONTACT
		local
			contact: CONTACT
		do
			create contact.make(a_name, a_phone_number, a_work_place, a_email)
			Result := contact
		end

	edit_contact(c: CONTACT; a_name: STRING; a_phone_number: INTEGER_64; a_work_place: STRING; a_email: STRING)
		do
			if not a_name.is_equal ("") then
				c.set_name(a_name)
			end

			if a_phone_number /= 0 then
				c.set_number(a_phone_number)
			end

			if not a_work_place.is_equal ("") then
				c.set_work_place(a_work_place)
			end

			if not a_email.is_equal ("") then
				c.set_email(a_email)
			end
		end

	add_emergency_contact(c1: CONTACT; c2: CONTACT)
		do
			c1.set_emergency (c2)
		end

	remove_emergency_contact(c: CONTACT)
		do
			c.remove_emergency
		end

	contact_info(c: CONTACT): STRING
		do
			Result := "Name: " + c.get_name + "; Phone Number: " + c.get_number.out + "; Work Place: " + c.get_work_place + "; Email: " + c.get_email
		end

	emergency_contact_info(c: detachable CONTACT): STRING
		do
			if c /= Void then
			Result := "Name: " +  c.get_name + ", Phone Number: " + c.get_number.out + "; Work Place: " + c.get_work_place + "; Email: " + c.get_email
			else Result := "no emergency contact"
			end
		end
end
