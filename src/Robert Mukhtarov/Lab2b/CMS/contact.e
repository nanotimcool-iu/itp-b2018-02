class
	CONTACT

create
	make

feature
	name: STRING
	phone_number: INTEGER_64
	work_place: STRING
	email: STRING
	call_emergency: detachable CONTACT

feature
	make (a_name: STRING; a_phone_number: INTEGER_64; a_work_place: STRING;	 a_email: STRING)
		do
			set_name(a_name)
			set_number(a_phone_number)
			set_work_place(a_work_place)
			set_email(a_email)
		end

feature
	set_name (a_name: STRING)
		do
			name := a_name
		end

	set_number (a_number: INTEGER_64)
		do
			phone_number := a_number
		end

	set_work_place (a_work_place: STRING)
		do
			work_place := a_work_place
		end

	set_email (a_email: STRING)
		do
			email := a_email
		end

	set_emergency(c: CONTACT)
		do
			call_emergency := c
		end

	remove_emergency
		do
			call_emergency := Void
		end

	get_name: STRING
		do
			Result := name
		end

	get_number: INTEGER_64
		do
			Result := phone_number
		end

	get_work_place: STRING
		do
			Result := work_place
		end

	get_email: STRING
		do
			Result := email
		end

	get_emergency: detachable CONTACT
		do
			if call_emergency /= Void then
				Result := call_emergency
			end
		end
end
