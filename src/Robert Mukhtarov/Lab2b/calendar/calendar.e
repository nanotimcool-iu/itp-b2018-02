class
	CALENDAR

create
	make

feature
	make
		--Trying out implementation
		local
			date1: DATE
			date2: DATE
			person1: PERSON
			entry1: ENTRY
			entry2: ENTRY
		do
			create date1.make (2019, 6, 9)
			create date2.make (2019, 7, 15)
			create person1.make ("Robert", 8905123457, "Innopolis", "r.muhtarov@innopolis.university")
			entry1 := create_entry(person1, "My Birthday", "Home", date1)
			Io.put_string("Entry 1:%N" + print_entry(entry1))
			entry2 := create_entry(person1, "Trip to Moscow", "Kazan Airport", date1)
			Io.put_string("Entry 2:%N" + print_entry(entry2))
			if in_conflict(entry1, entry2) then
				Io.put_string ("Entries 1 and 2 are in conflict.")
			end
			edit_subject(entry1, "Party at University")
			edit_place(entry1, "University")
			edit_date(entry2, date2)
			Io.put_string("%N%NEdited entry 1:%N" + print_entry(entry1))
			Io.put_string("Edited entry 2:%N" + print_entry(entry2))
			if in_conflict(entry1, entry2) then
				Io.put_string ("Entries 1 and 2 are in conflict.")
			else Io.put_string("Entries 1 and 2 are not in conflict.%N")
			end
		end

feature
	create_entry (owner: PERSON; subject: STRING; place: STRING; date: DATE): ENTRY
		local
			New_entry: ENTRY
		do
			create New_entry.make(owner, subject, place, date)
			Result := New_entry
		end

	 edit_subject (e: ENTRY; new_subject: STRING)
	 	do
	 		e.set_subject(new_subject)
	 	end

	 edit_date (e: ENTRY; new_date: DATE)
	 	do
	 		e.set_date(new_date)
	 	end

	 get_owner_name (e: ENTRY): STRING
	 	do
	 		Result := e.get_owner_name
	 	end

	 in_conflict (e1, e2: ENTRY): BOOLEAN
	 	do
	 		if e1.get_place.is_equal(e2.get_place) then
	 			Result := true
	 		elseif e1.get_date.is_equal(e2.get_date) then
	 			Result := true
	 		else Result := false
	 		end
	 	end

	 edit_place (e: ENTRY; new_place: STRING)
	 	do
	 		e.set_place(new_place)
	 	end

	 print_date (d: DATE): STRING
	 	do
	 		Result := d.day.out + "/" + d.month.out + "/" + d.year.out
 	 	end

 	 print_entry (e: ENTRY): STRING
 	 	do
 	 		Result := ("Owner: " + e.get_owner_name + "%N" + "Subject: " + e.get_subject + "%N" + "Place: " + e.get_place + "%N" + "Date: " + print_date(e.get_date) + "%N%N")
 	 	end
end
