class
	ENTRY

create
	make

feature
	date: DATE
	owner: PERSON
	subject: STRING
	place: STRING

feature
	make (o: PERSON; s: STRING; p: STRING; d: DATE)
		do
			set_owner(o)
			set_subject(s)
			set_place(p)
			set_date(d)
		end

	set_owner(o: PERSON)
		do
			owner := o
		end

	set_subject(s: STRING)
		do
			subject := s
		end

	set_place(p: STRING)
		do
			place := p
		end

	get_owner_name: STRING
		do
			Result := owner.get_name
		end

	get_place: STRING
		do
			Result := place
		end

	set_date(d: DATE)
		do
			date := d
		end

	get_date: DATE
		do
			Result := date
		end

	get_subject: STRING
		do
			Result := subject
		end

end
