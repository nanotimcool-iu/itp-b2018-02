class
	PERSON

create
	make

feature
	name: STRING
	phone_number: INTEGER_64
	work_place: STRING
	email: STRING

feature
	make(a_name: STRING; a_phone_number: INTEGER_64; a_work_place: STRING; a_email: STRING)
		do
			set_name(a_name)
			set_phone_number(a_phone_number)
			set_work_place(a_work_place)
			set_email(a_email)
		end

	set_name(a_name: STRING)
		do
			name := a_name
		end

	set_phone_number(a_phone_number: INTEGER_64)
		do
			phone_number := a_phone_number
		end

	set_work_place(a_work_place: STRING)
		do
			work_place := a_work_place
		end

	set_email(a_email: STRING)
		do
			email := a_email
		end

	get_name: STRING
		do
			Result := name
		end
end
