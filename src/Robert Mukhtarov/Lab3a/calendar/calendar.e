class
	CALENDAR

create
	make

feature
	days: ARRAYED_LIST[DAY]

feature
	make
	--Trying out implementation
		local
			o1: PERSON
			t1: TIME
			t2: TIME
			t3: TIME
			t4: TIME
			dur1: TIME
			dur2: TIME
			dur3: TIME
			dur4: TIME
			d1: DAY
			d2: DAY
			d3: DAY
		do
			--days
			create days.make (0)
			create d1.make (5)
			days.extend (d1)
			create d2.make (15)
			days.extend (d2)
			create d3.make (20)
			days.extend (d3)
			--time
			create t1.make(16, 0, 0)
			create dur1.make(2, 0, 0)
			create t2.make(17, 0, 0)
			create dur2.make(1, 30, 0)
			create t3.make(19, 40, 0)
			create dur3.make(3, 30, 0)
			create t4.make(20, 0, 0)
			create dur4.make(1, 15, 0)
			--owners
			create o1.make ("John", 8905123467, "Innopolis", "john@innopolis.university")
			--events
			add_entry(o1, "Conference", "Innopolis, Reading Hall", t1, dur1, d1)
			add_entry(o1, "Presentation", "Innopolis, 108", t2, dur2, d1)
			add_entry(o1, "Birthday Party", "Kazan", t3, dur3, d2)
			add_entry(o1, "Business Conversation", "Moscow", t4, dur4, d3)
			--actions
			print(printable_month)
			if in_conflict(d1.events.array_item (0), d1.events.array_item (1)) then
				print("Entries 1 and 2 of day " + d1.get_date.out + " are in conflict.%N")
			end
			if not in_conflict(d2.events.array_item (0), d3.events.array_item (0)) then
				print("Entry 1 of day " + d2.get_date.out + " and entry 1 of day " + d3.get_date.out + " are not in conflict.%N")
			end
		end

feature
	add_entry (owner: PERSON; subject: STRING; place: STRING; time: TIME; duration: TIME; day: DAY)
		local
			New_entry: ENTRY
		do
			create New_entry.make(owner, subject, place, time, duration, day)
			day.add_event(New_entry)
		end

	 edit_subject (e: ENTRY; new_subject: STRING)
	 	do
	 		e.set_subject(new_subject)
	 	end

	 edit_time (e: ENTRY; new_time: TIME)
	 	do
	 		e.set_time(new_time)
	 	end

	 get_owner_name (e: ENTRY): STRING
	 	do
	 		Result := e.get_owner_name
	 	end

	 in_conflict (e1, e2: ENTRY): BOOLEAN
	 	local
	 		e1_beginning: INTEGER_64
	 		e1_ending: INTEGER_64
	 		e2_beginning: INTEGER_64
	 		e2_ending: INTEGER_64
	 	do
	 		if e1.get_date.is_equal (e2.get_date) then
				e1_beginning := e1.get_time_seconds
				e1_ending := e1_beginning + e1.get_duration_seconds
				e2_beginning := e2.get_time_seconds
				e2_ending := e2_beginning + e2.get_duration_seconds
				if (e2_beginning >= e1_beginning and e2_beginning <= e1_ending) or (e1_beginning >= e2_beginning and e1_beginning <= e2_ending)
				then
					if e1.get_place.is_equal (e2.get_place) then
						Result := false
					else Result := true
					end
				end
			else Result := false
	 		end
	 	end

	 edit_place (e: ENTRY; new_place: STRING)
	 	do
	 		e.set_place(new_place)
	 	end

	 printable_month: STRING
	 	do
	 		Result := "Days of this month:%N%N"
			across days as day loop
				Result := Result + day.item.get_events + "-----------%N"
	 		end
	 	end
end
