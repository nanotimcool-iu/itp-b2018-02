class DAY create make

feature
	date: INTEGER
	events: ARRAYED_LIST[ENTRY]

feature
	make (a_date: INTEGER)
		do
			date := a_date
			create events.make (0)
		end

	add_event(e: ENTRY)
		do
			events.extend (e)
		end

	set_date (d: INTEGER)
		do
			date := d
		end

	get_date: INTEGER
		do
			Result := date
		end

	get_events: STRING
		do
			Result := "Events on day " + date.out + ":%N"
			across events as entry loop
				Result := Result + "%NEvent #" + entry.cursor_index.out + "%N"
				Result := Result + entry.item.printable_entry + "%N"
			end
	end
end
