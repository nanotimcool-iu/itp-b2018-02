class
	ENTRY

create
	make

feature
	time: TIME
	duration: TIME
	owner: PERSON
	subject: STRING
	place: STRING
	date:  DAY

feature
	make (o: PERSON; s: STRING; p: STRING; t: TIME; d: TIME; dt: DAY)
		do
			set_owner(o)
			set_subject(s)
			set_place(p)
			set_time(t)
			set_duration(d)
			set_date(dt)
		end

	set_owner(o: PERSON)
		do
			owner := o
		end

	set_subject(s: STRING)
		do
			subject := s
		end

	set_duration(d: TIME)
		do
			duration := d
		end

	set_place(p: STRING)
		do
			place := p
		end

	set_date(d: DAY)
		do
			date := d
		end

	get_date:  DAY
		do
			Result := date
		end

	get_owner_name: STRING
		do
			Result := owner.get_name
		end

	get_place: STRING
		do
			Result := place
		end

	set_time(t: TIME)
		do
			time := t
		end

	get_time: TIME
		do
			Result := time
		end

	get_time_seconds: INTEGER
		do
			Result := time.hour * 60 * 60 + time.minute * 60 + time.second
		end

	get_duration_seconds: INTEGER
		do
			Result := duration.hour * 60 * 60 + duration.minute * 60 + duration.second
		end

	get_subject: STRING
		do
			Result := subject
		end

	printable_time: STRING
		do
			Result := time.hour.out + ":" + time.minute.out + ":" + time.second.out
		end

	printable_duration: STRING
		do
			Result := duration.hour.out + " hours " + duration.minute.out + " minutes " + duration.second.out +  " seconds"
		end

	printable_entry: STRING
		do
			Result := "Owner: " + owner.get_name + "%NSubject: " + subject + "%NPlace: " + place + "%NTime: " + printable_time + "%NDuration: " + printable_duration
		end
end
