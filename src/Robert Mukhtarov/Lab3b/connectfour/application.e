class
	APPLICATION

create
	make

feature
	field: ARRAY2[INTEGER]
	n, m, i, j, turn, column: INTEGER
	player1, player2, is_dropped, is_field_correct: BOOLEAN

	make
		do
			--creating field requiring correct values
			from until is_field_correct loop
				print ("Enter n and m line-by-line:%N")
				io.read_integer
				n := Io.last_integer
				io.read_integer
				m := Io.last_integer
				if n < 4 or m < 4 then
					print ("%NThe minimum size of the field is 4 x 4. Enter correct values.%N")
				else is_field_correct := true
				end
			end
			create field.make_filled (0, m, n)
			print("%NThis is your field:")
			print_field

			--game itself
			from until player1 or player2 loop
				turn := 1
				from until is_dropped loop
					print ("%NPlayer 1, enter the column number:%N")
					Io.read_integer
					column := Io.last_integer
					if is_droppable then
					 	drop_disc (column)
						is_dropped := true
					else print("%NYou can't drop a disc here. Try again.")
					end
				end
				is_dropped := false
				print_field
				player1 := has_player_won

				if not player1 then
					turn := 2
					from until is_dropped loop
						print ("%NPlayer 2, enter the column number:%N")
						Io.read_integer
						column := Io.last_integer
						if is_droppable then
							drop_disc (column)
							is_dropped := true
						else print("%NYou can't drop a disc here. Try again.")
						end
					end
					is_dropped := false
					print_field
					player2 := has_player_won
				end
			end

			if player1 then
				print ("%NPlayer 1 has won!%N")
			else print ("%NPlayer 2 has won!%N")
			end
		end

	print_field
		do
			print ("%N")
			from i := 1 until i > m loop
				from j := 1 until j > n loop
					print(field.item (i, j).out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
		end

	is_droppable: BOOLEAN
		do
			if (column <= n and column >= 1) and field.item (1, column) = 0 then
				Result := true else Result := false
			end
		end

	drop_disc (x_coord: INTEGER)
		local
			y_check: INTEGER
		do
			from y_check := m until field.item (y_check, x_coord) = 0 loop
				y_check := y_check - 1
			end
			field.force (turn, y_check, x_coord)
		end

	has_player_won: BOOLEAN
		do
			if check_horizontally or check_vertically or check_diagonally then
				Result := true
			else Result := false
			end
		end

feature
	--checkers
	check_horizontally: BOOLEAN
		local
			counter: INTEGER
			success: BOOLEAN
		do
			counter := 0
			from i := 1 until i > m or success loop
				from j := 1 until j > n  loop
					if field.item (i, j) = turn then
						counter := counter + 1
					else counter := 0 end
					if counter = 4 then success := true end
					j := j + 1
				end
				i := i + 1
			end
			if success then Result := true
			else Result := false end
		end

	check_vertically: BOOLEAN
		local
			counter: INTEGER
			success: BOOLEAN
		do
			counter := 0
			from j := 1 until j > n or success loop
				from i := 1 until i > m  loop
					if field.item (i, j) = turn then
						counter := counter + 1
					else counter := 0 end
					if counter = 4 then success := true end
					i := i + 1
				end
				j := j + 1
			end
			if success then Result := true
			else Result := false end
		end

	check_diagonally: BOOLEAN
		local
			counter, x, y: INTEGER
			success: BOOLEAN
		do
			success := false
			x := column
			from i := 1 until field.item (i, column) /= 0 loop
				i := i + 1
			end
			y := i

			--going top-left
			counter := 1
			i := y
			j := x
			from until i = 1 or j = 1 loop
				i := i - 1
				j := j - 1
				if field.item (i, j) = turn then
					counter := counter + 1
					if counter = 4 then
						success := true
					end
				else counter := 0
				end
			end

			--going bottom-left
			counter := 1
			i := y
			j := x
			from until i = m or j = 1 loop
				i := i + 1
				j := j - 1
				if field.item (i, j) = turn then
					counter := counter + 1
					if counter = 4 then
						success := true
					end
				else counter := 0
				end
			end

			--going top-right
			counter := 1
			i := y
			j := x
			from until i = 1 or j = n loop
				i := i - 1
				j := j + 1
				if field.item (i, j) = turn then
					counter := counter + 1
					if counter = 4 then
						success := true
					end
				else counter := 0
				end
			end

			--going bottom-right
			counter := 1
			i := y
			j := x
			from until i = m or j = n loop
				i := i + 1
				j := j + 1
				if field.item (i, j) = turn then
					counter := counter + 1
					if counter = 4 then
						success := true
					end
				else counter := 0
				end
			end

		if success then Result := true else Result := false end
	end
end
