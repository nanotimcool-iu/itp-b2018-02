class
	APPLICATION

create
	make

feature
	make
		local
		num: INTEGER
		combined_rows: INTEGER
		current_num: INTEGER
		spaces: INTEGER
		i: INTEGER
		d: INTEGER
		s: INTEGER
		k: INTEGER
		j: INTEGER

		do
			print("Write a positive number which will represent the height:%N")
			Io.read_integer
			print("%N")
			num := Io.last_integer
			combined_rows := num // 2
			if num \\ 2 = 1 then
				combined_rows := combined_rows + 1
			end

			current_num := 1
			if num > 5 then spaces := num * 2 + 8
			else spaces := num * 2 - 2
			end

			from i := 1 until i > combined_rows
			loop
				if i = combined_rows and combined_rows > num // 2 then
					from d := 0 until d >= i loop
						print (current_num.out + " ")
						current_num := current_num + 1
						d := d + 1
					end

					from s := 0 until s > spaces loop
						print (" ")
						s := s + 1
					end

					from d := 0 until d >= i loop
						current_num := current_num - 1
						print (current_num.out + " ")
						d := d + 1
					end
				else
					from k := 0 until k > 1 loop
						from j := 0 until j > i - 1 loop
							print (current_num.out + " ")
							current_num := current_num + 1
							j := j + 1
						end

						from s := 0 until s > spaces loop
							print (" ")
							s := s + 1
						end

						if current_num = 10 then
							spaces := spaces - 8
						elseif current_num > 10 then
							spaces := spaces - 3
						else spaces := spaces - 2
						end

						from j := 0 until j > i - 1 loop
							current_num := current_num - 1
							print (current_num.out + " ")
							j := j + 1
						end
						current_num := current_num + i

						print("%N")
						if k = 0 then print(" ") end

						k := k + 1
					end
				end
				i := i + 1
			end
	end
end
