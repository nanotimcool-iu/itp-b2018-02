class
	APPLICATION

create
	make

feature
	bag: MULTISET
	bag2: MULTISET

	make
		do
			--Trying out implementation
			create bag.make
			bag.add ('a', 5)
			bag.add ('b', 7)
			bag.add ('c', 35)
			print("Initial bag #1: ")
			bag.print_bag
			print("%NUpdated bag #1: ")
			bag.add ('b', 5)
			bag.remove('a', 4)
			bag.add ('d', 8)
			bag.print_bag
			print ("%NMin character: " + bag.min.out)
			print ("%NMax character: " + bag.max.out)

			create bag2.make
			bag2.add ('a', 1)
			bag2.add ('b', 12)
			bag2.add ('c', 35)
			bag2.add ('d', 8)
			print ("%N%NBag #2: ")
			bag2.print_bag

			if bag.is_equal_bag (bag2) then
				print("%NBags #1 and #2 are equal.")
			end

			bag2.remove ('c', 3)
			if not bag.is_equal_bag (bag2) then
				print("%N%NBag #1 and updated bag #2 are not equal.")
			end
		end
end
