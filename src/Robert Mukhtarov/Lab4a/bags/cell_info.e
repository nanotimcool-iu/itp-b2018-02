class
	CELL_INFO

create
	make

feature
	value: CHARACTER
	number_copies: INTEGER

	make (val: CHARACTER; num: INTEGER)
		do
			value := val
			number_copies := num
		end

	add_copies  (num: INTEGER)
		do
			number_copies := number_copies + num
		ensure
			number_copies = old number_copies + num
		end

	remove_copies  (num: INTEGER)
		do
			number_copies := number_copies - num
		ensure
			number_copies = old number_copies - num
		end

	get_value: CHARACTER
		do
			Result := value
		end

	set_value (val: CHARACTER)
		do
			value := val
		end

	get_copies: INTEGER
		do
			Result := number_copies
		end

	set_copies  (num: INTEGER)
		do
			number_copies := num
		end
end
