class
	MULTISET

create
	make

feature
	elements: ARRAY[CELL_INFO]

	make
		do
			create elements.make_empty
		end

	add (val: CHARACTER; n: INTEGER)
		require
			number_of_copies: n > 0
			valid_character: val.code <= 255 and val.code >= 0
		local
			New_cell_info: CELL_INFO
			exists: BOOLEAN
		do
			across elements as element loop
				if element.item.get_value = val then
					exists := true
				end
			end

			if exists then
				across elements as element loop
					if element.item.get_value = val then
						element.item.add_copies(n)
					end
				end
			else
				create New_cell_info.make (val, n)
				elements.force (New_cell_info, elements.capacity + 1)
			end
		end

	remove (val: CHARACTER; n: INTEGER)
		require
			number_of_copies: n > 0
			valid_character: val.code <= 255 and val.code >= 0
		do
			across elements as element loop
				if element.item.get_value = val then
					element.item.remove_copies(n)
					if element.item.get_copies < 0 then
						element.item.set_copies (0)
					end
				end
			end
		end

	min: CHARACTER
		local
			min_char_code: INTEGER
		do
			min_char_code := 256
			across elements as element loop
				if element.item.get_value.code < min_char_code then
					min_char_code :=  element.item.get_value.code
				end
			end
			Result := min_char_code.to_character_8
		end

	max: CHARACTER
		local
			max_char_code: INTEGER
		do
			max_char_code := -1
			across elements as element loop
				if element.item.get_value.code > max_char_code then
					max_char_code :=  element.item.get_value.code
				end
			end
			Result := max_char_code.to_character_8
		end

	is_equal_bag (b: MULTISET): BOOLEAN
		local
			counter: INTEGER
			i: INTEGER
			j: INTEGER
		do
			counter := 0
			if elements.capacity /= b.elements.capacity then
				Result := False
				else
					from i := 1 until i > elements.capacity loop
						from j := 1 until j > b.elements.capacity loop
							if elements.at (i).is_equal (b.elements.at (j)) then
								counter := counter + 1
							end
							j := j + 1
						end
						i := i + 1
					end
					if counter = elements.capacity then
						Result := True
					else Result := False
					end
			end
		end

	print_bag
		do
			print ("[ ")
			across elements as element loop
				print("(" + element.item.get_value.out + "," + element.item.get_copies.out + ") ")
			end
			print ("]")
		end
end
