class
	APPLICATION

inherit
	MATRIX_OPER; VECTOR_OPER

create
	make

feature
	make
		local
			a, b, i: ARRAY2 [INTEGER]
			a1, b1: ARRAY2 [INTEGER]
			vec_a, vec_b, vec_c: ARRAY [INTEGER]
		do
			print("Assignment 1, exercise 1.1%N%N")
			create a.make_filled (0, 2, 2)
			a.item(1, 1) := 3; a.item(1, 2) := 1
			a.item(2, 1) := 5; a.item(2, 2) := -2

			create b.make_filled (0, 2, 2)
			b.item(1, 1) := -2; b.item(1, 2) := 1
			b.item(2, 1) := 3; b.item(2, 2) := 4

			create i.make_filled (0, 2, 2)
			i.item(1, 1) := 1; i.item(1, 2) := 0
			i.item(2, 1) := 0; i.item(2, 2) := 1

			print ("A + B:")
			print_matrix (add(a, b))
			print ("%N2A - 3B + I: ")
			print_matrix (add((minus(prod_const(a, 2), prod_const(b, 3))), i))

			print("%NAssignment 1, exercise 2.1%N%N")
			create a1.make_filled (0, 1, 3)
			a1.item(1, 1) := 2; a1.item(1, 2) := -1; a1.item(1, 3) := -1
			create b1.make_filled (0, 3, 1)
			b1.item (1, 1) := -2; b1.item (2, 1) := -1; b1.item (3, 1) := 3;
			print ("A * B:")
			print_matrix (prod(a1, b1))
			print ("%NB * A:")
			print_matrix (prod(b1, a1))

			print("%NAssignment 3, exercise 1%N%N")
			create vec_a.make_filled (0, 1, 3)
			vec_a.item (1) := 3; vec_a.item (2) := -2; vec_a.item (3) := 1;
			create vec_b.make_filled (0, 1, 3)
			vec_b.item (1) := 2; vec_b.item (2) := -5; vec_b.item (3) := -3;
			create vec_c.make_filled (0, 1, 3)
			vec_c.item (1) := -18; vec_c.item (2) := 12; vec_c.item (3) := -6;
			print ("a x b: ")
			print_vector(cross_product(vec_a, vec_b))
			print ("%Na x c: ")
			print_vector (cross_product(vec_a, vec_c))

			print("%N%NAssignment 3, exercise 3%N%N")
			vec_a.item (1) := 2; vec_a.item (2) := 4; vec_a.item (3) := -1;
			vec_b.item (1) := -2; vec_b.item (2) := 1; vec_b.item (3) := 1;
			print ("Area: ")
			print (triangle_area(vec_a, vec_b))
		end
end
