class
	MATRIX_OPER

feature
	add (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.height = m2.height
			m1.width = m2.width
		local
			result_matrix: ARRAY2 [INTEGER]
			i, j: INTEGER
		do
			create result_matrix.make_filled (0, m1.height, m1.width)
			from i := 1 until i > m1.height loop
				from j := 1 until j > m1.width loop
					result_matrix.item (i, j) := m1.item (i, j) + m2.item (i, j)
					j := j + 1
				end
				i := i + 1
			end
			Result := result_matrix
		end

	minus (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.height = m2.height
			m1.width = m2.width
		local
			result_matrix: ARRAY2 [INTEGER]
			i, j: INTEGER
		do
			create result_matrix.make_filled (0, m1.height, m1.width)
			from i := 1 until i > m1.height loop
				from j := 1 until j > m1.width loop
					result_matrix.item(i, j) := m1.item (i, j) - m2.item (i, j)
					j := j + 1
				end
				i := i + 1
			end
			Result := result_matrix
		end

	prod (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			m1.width = m2.height
		local
			result_matrix: ARRAY2 [INTEGER]
			i, j, k, value: INTEGER
		do
			create result_matrix.make_filled (0, m1.height, m2.width)
			from i := 1 until i > result_matrix.height loop
				from j := 1 until j > result_matrix.width loop
					from k := 1 until k > m2.height loop
						value := value + m1.item (i, k) * m2.item (k, j)
						k := k + 1
					end
					result_matrix.item (i, j) := value
					value := 0
					j := j + 1
				end
				i := i + 1
			end
			Result := result_matrix
		end

	prod_const (m1: ARRAY2 [INTEGER]; c1: INTEGER): ARRAY2 [INTEGER]
		local
			result_matrix: ARRAY2 [INTEGER]
			i, j: INTEGER
		do
			create result_matrix.make_filled(0, m1.height, m1.width)
			from i := 1 until i > m1.height loop
				from j := 1 until j > m1.width loop
					result_matrix.item(i, j) := m1.item (i, j) * c1
					j := j + 1
				end
				i := i + 1
			end
			Result := result_matrix
		end

	det (m: ARRAY2 [INTEGER]): INTEGER
		require
			m.width = m.height
		local
			sign, i, j, row, column: INTEGER
			det_matrix: ARRAY2 [INTEGER]
		do
			if m.width = 1 then
				Result := m.item(1, 1)
			elseif m.width = 2 then
				Result := m.item (1, 1) * m.item (2, 2) - m.item (2, 1) * m.item (1, 2)
			else
				sign := 1
				from i := 1 until i > m.width loop
					create det_matrix.make_filled (0, m.height - 1, m.width - 1)
					column := 1
					from j := 1 until j > m.width loop
						if j = i then j := j + 1
						else
							from row := 2 until row > m.height loop
								det_matrix.item (row - 1, column) := m.item (row, j)
								row := row + 1
							end
							column := column + 1
							j := j + 1
						end
					end
					Result := Result + sign * m.item (1, i) * det (det_matrix)
					i := i + 1
					sign := - sign
				end
			end
		end

	print_matrix (m: ARRAY2 [INTEGER])
		local
			i, j: INTEGER
		do
			print ("%N")
			from i := 1 until i > m.height loop
				from j := 1 until j > m.width loop
					print(m.item (i, j).out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
		end
end
