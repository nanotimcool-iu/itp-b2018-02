class
	VECTOR_OPER

inherit
	MATRIX_OPER; SINGLE_MATH

feature
	dot_product (v1, v2: ARRAY [INTEGER]): INTEGER
		do
			Result := v1.item (1) * v2.item (1) + v1.item (2) * v2.item (2) + v1.item (3) * v2.item (3)
		end

	cross_product (v1, v2: ARRAY [INTEGER]): ARRAY [INTEGER]
		local
			matrix: ARRAY2 [INTEGER]
			product: ARRAY [INTEGER]
		do
			create product.make_filled (0, 1, 3)
			create matrix.make_filled (0, 2, 2)
			matrix.item (1, 1) := v1.item (2); matrix.item (1, 2) := v1.item (3)
			matrix.item (2, 1) := v2.item (2); matrix.item (2, 2) := v2.item (3)
			product.item (1) := det (matrix)
			matrix.item (1, 1) := v1.item (1); matrix.item (1, 2) := v1.item (3)
			matrix.item (2, 1) := v2.item (1); matrix.item (2, 2) := v2.item (3)
			product.item (2) := - det (matrix)
			matrix.item (1, 1) := v1.item (1); matrix.item (1, 2) := v1.item (2)
			matrix.item (2, 1) := v2.item (1); matrix.item (2, 2) := v2.item (2)
			product.item (3) := det (matrix)
			Result := product
		end

	 triangle_area (v1, v2: ARRAY [INTEGER]): REAL
		local
			v: ARRAY [INTEGER]
		do
			create v.make_from_array (cross_product (v1, v2))
			Result := (sqrt (v.item(1) * v.item (1) + v.item(2) * v.item (2) + v.item(3) * v.item (3))) / 2
		end

	print_vector (v: ARRAY [INTEGER])
		do
			print ("(" + v.item (1).out + ", " + v.item (2).out + ", " + v.item (3).out + ")")
		end
end
