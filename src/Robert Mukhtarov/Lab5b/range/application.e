class
	APPLICATION

create
	make

feature
	make
		local
			range1: RANGE
			range2: RANGE
			range3: RANGE
		do
			-- Trying out the implementation
			create range1.make (1, 4)
			create range2.make (3, 7)
			create range3.make (0, 0)
			print (range1.is_equal_range (range2).out + "%N")
			print (range1.is_empty.out + "%N")
			print (range1.overlaps (range2).out + "%N")
			print (range1.left_overlaps (range2).out + "%N")
			print (range1.right_overlaps (range2).out + "%N")
			range3 := range1.add (range2)
			range3.print_range
			range3 := range1.subtract (range2)
			range3.print_range
			print (range1.is_super_range_of (range3).out + "%N")
			print (range3.is_sub_range_of (range1).out + "%N")
		end
end
