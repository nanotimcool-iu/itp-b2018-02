class
	RANGE

create
	make

feature
	left: INTEGER
	right: INTEGER

	make (l, r: INTEGER)
		do
			left := l
			right := r
		end

	is_equal_range (other: like Current): BOOLEAN
	-- Check whether 2 ranges are equal.
		do
			Result := other.left = left and other.right = right
		end

	is_empty: BOOLEAN
	-- Check whether the range is empty.
		do
			Result := right < left
		end

	is_super_range_of (other: like Current): BOOLEAN
	-- Check whether the range is a super range of the other one.
		do
			Result := (other.left >= left and other.right <= right) or other.is_empty
		end

  	is_sub_range_of (other: like Current): BOOLEAN
  	-- Check whether the range is a sub range of the other one.
  		do
			Result := other.is_super_range_of (Current)
		end

	left_overlaps (other: like Current): BOOLEAN
	-- Check whether the left element of the range is inside the overlap between 2 ranges.
		do
			if Current.overlaps (other) then
				Result := (left >= other.left and left <= other.right) or Current.is_empty or other.is_empty
			else
				Result := False
			end
		end

	right_overlaps (other: like Current): BOOLEAN
	-- Check whether the right element of the range is inside the overlap between 2 ranges.
		do
			if Current.overlaps (other) then
				Result := (right <= other.right and right >= other.left) or Current.is_empty or other.is_empty
			else
				Result := False
			end
		end

	overlaps (other: like Current): BOOLEAN
	-- Check whether 2 ranges overlap.
		do
			Result := (other.left >= left and other.left <= right) or (left >= other.left and left <= other.right) or Current.is_empty or other.is_empty
		end

	add (other: like Current): RANGE
	-- Add a range to the current range.
		require
			Current.overlaps (other) = True
		local
			range: RANGE
		do
			create range.make (left.min (other.left), right.max (other.right))
			Result := range
		ensure
			Result.is_super_range_of (Current) and Result.is_super_range_of (other)
		end

	subtract (other: like Current): RANGE
	-- Subtract a range from the current range.
		local
			New_range: RANGE
		do
			if Current.overlaps (other) then
				if other.left >= left then
					create New_range.make (left, other.left - 1)
					Result := New_range
				else
					create New_range.make (other.right + 1, right)
					Result := New_range
				end
			else Result := Current
			end
		end

	print_range
	-- Print the range.
		do
			print ("[" + left.out + "..." + right.out + "]%N")
		end
end
