class
	APPLICATION

create
	make

feature
	str: STRING

	make
		do
			print("Enter string: ")
			Io.read_line
			str := Io.last_string
			print ("Reversed iteratively: " + reverse_iteratively)
			print ("%NReversed recursively: " + reverse_recursively (str))
		end

	reverse_iteratively: STRING
		local
			i: INTEGER
			res: STRING
		do
			res := ""
			from i := str.capacity until i = 0 loop
				res := res + str.at (i).out
				i := i - 1
			end
			Result := res
		end

		reverse_recursively (s: STRING): STRING
			do
				if (s.capacity <= 1) then
					Result := s
				else
					Result := reverse_recursively (s.substring (2, s.capacity)) + s.at (1).out
				end
			end
end
