class
	APPLICATION

create
	make

feature
	make
		-- Trying out implementation
		do
			print (lcs ("12636124", "783612893"))
		end

	lcs (x, y: STRING): STRING
		local
			str1, str2: STRING
		do
			if x.is_empty or y.is_empty then
				Result := ""
			elseif x [x.count] = y [y.count] then
				Result := lcs (x.substring (1, x.count - 1), y.substring (1, y.count - 1)) + x [x.count].out
			else
				str1 := lcs (x, y.substring (1, y.count - 1))
				str2 := lcs (x.substring (1, x.count - 1), y)
				if str1.count > str2.count then
					Result := str1
				else
					Result := str2
				end
			end
		end
end
