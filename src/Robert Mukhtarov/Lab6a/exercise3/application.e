class
	APPLICATION

create
	make

feature
	make
		local
			arr: ARRAY [INTEGER]
		do
			-- Trying out implementation
			create arr.make_filled (0, 1, 7)
			arr [1] := 38; arr [2] := 27; arr [3] := 43; arr [4] := 3; arr [5] := 9; arr [6] := 82; arr [7] := 10
			print_arr (merge_sort (arr))
		end

	merge_sort (arr: ARRAY [INTEGER]): ARRAY [INTEGER]
		local
			middle: INTEGER
			arr1, arr2: ARRAY [INTEGER]
		do
			if arr.count < 2 then Result := arr
			else
				middle := arr.count // 2
				create arr1.make_filled (0, 1, middle)
				create arr2.make_filled (0, 1, arr.count - middle)
				arr1.subcopy (arr, 1, middle, 1)
				arr2.subcopy (arr, middle + 1, arr.count, 1)
				Result := merge (merge_sort (arr1), merge_sort (arr2))
			end
		end

	merge (arr1, arr2: ARRAY [INTEGER]): ARRAY [INTEGER]
		local
			res: ARRAYED_LIST [INTEGER]
			i, j: INTEGER
		do
			i := 1; j := 1
			create res.make_filled (0)
			from until i > arr1.count or j > arr2.count loop
				if arr1 [i] <= arr2 [j] then
					res.extend (arr1[i])
					i := i + 1
				else
					res.extend (arr2[j])
					j := j + 1
				end
			end
			from until i > arr1.count loop
				res.extend (arr1 [i])
				i := i + 1
			end
			from until j > arr2.count loop
				res.extend (arr2 [j])
				j := j + 1
			end
			Result := res.to_array
		end

		print_arr (arr: ARRAY [INTEGER])
			local
				i: INTEGER
			do
				from i := 1 until i > arr.count loop
					print (arr [i].out + " ")
					i := i + 1
				end
			end
end
