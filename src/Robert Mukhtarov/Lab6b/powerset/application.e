class
	APPLICATION
create
	make

feature
	make
		-- Trying out implementation
		local
			set: ARRAY [INTEGER]
		do
			create set.make_filled (0, 1, 4)
			set [1] := 1; set [2] := 2; set [3] := 3; set [4] := 4
			power_set (set)
		end

		power_set (set: ARRAY [INTEGER])
		local
			i, digit, num, base: INTEGER
		do
			base := 2
			print ("{")
			from i := 0 until i > base.power (set.count) - 1 loop
				num := i
				print (" { ")
				from digit := 1 until digit > set.count loop
					if num \\ 2 = 1 then
						print (set [digit])
						print (" ") end
					num := num // 2
					digit := digit + 1
				end
				print ("}")
				i := i + 1
			end
			print (" }")
		end
end
