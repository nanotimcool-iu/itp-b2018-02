class
	APPLICATION

create
	make

feature
	make
		do
			print ("Enter the number of discs: ")
			Io.read_integer
			hanoi (Io.last_integer, "A", "B", "C")
		end

	hanoi (discs: INTEGER; source, other, target: STRING)
		require
			non_negative: discs >= 0
			different1: source /= target
			different2: target /= other
			different3: source /= other
		do
			if discs = 1 then
				print (source + " -> " + target + "%N")
			else
				hanoi (discs - 1, source, target, other)
				print (source + " -> " + target + "%N")
				hanoi (discs - 1, other, source, target)
			end
		end
end
