class
	APPLICATION

create
	make

feature
	input_file: PLAIN_TEXT_FILE
	number_of_animals: INTEGER
	animal: STRING
	word: STRING
	animal_number: INTEGER
	guesses: INTEGER
	player_won: BOOLEAN
	used_letters: ARRAYED_LIST [CHARACTER]

	make
		local
			letter: CHARACTER
		do
			set_game
			print ("Welcome to the Hangman game! Topic: Animals. Guess the word:")
			from until guesses <= 0 or player_won loop
				print_word
				if guesses > 1 then
					print ("%NYou have " + guesses.out + " guesses left.")
				else print ("%NYou have 1 guess left.")
				end
				print ("%NEnter a letter: ")
				Io.read_character
				letter := Io.last_character
				check_letter (letter)
				if word.is_equal (animal) then
					player_won := True
				end
				Io.next_line
			end
			if player_won then
				print_word
				print ("%N%NCongratulations! You won!")
			else print ("%N%NGame over :(%NThe answer was " + animal)
			end
		end

	set_game
		local
			i: INTEGER
		do
			number_of_animals := count_animals
			animal_number := random_number
			create used_letters.make_filled (0)
			input_file.open_read
			from i := 1 until i > animal_number loop
				input_file.read_line
				i := i + 1
			end
			animal := input_file.last_string.out
			create word.make_filled ('_', animal.count)
			guesses := animal.count
			input_file.close
		ensure
			word_is_assigned: animal /= Void and word /= Void
			word_is_properly_set: word.count = animal.count
		end

	check_letter (letter: CHARACTER)
		local
			found: BOOLEAN
			i: INTEGER
		do
			if word.has (letter) then
				print ("The letter has already been guessed. Type another one.")
			elseif used_letters.has (letter) then
				print ("You've already tried typing this letter. Try another one.")
			else
				if letter.code >= ('a').code and letter.code <= ('z').code then
					from i := 1 until i > animal.count loop
						if animal [i] = letter then
							found := True
							word [i] := letter
						end
						i := i + 1
					end
					if not found then
						print ("Nope!")
						guesses := guesses - 1
						used_letters.extend (letter)
					end
				else print ("Enter a valid character.")
				end
			end
		end

	print_word
	-- Prints the word separating letters with spaces.
		local
			i: INTEGER
		do
			print ("%N")
			from i := 1 until i > word.count loop
				print (word [i].out + " ")
				i := i + 1
			end
		end

	count_animals: INTEGER
	-- Returns the number of animals listed in Animals.txt
	local
		counter: INTEGER
	do
		create input_file.make_open_read ("Animals.txt")
			from input_file.read_character until input_file.exhausted loop
				input_file.read_line
				counter := counter + 1
			end
			input_file.close
			Result := counter
	end

	random_number: INTEGER
	-- Returns a random number from 1 to the total number of animals.
		local
			r: RANDOM
			l_time: TIME -- From Time library
     		l_seed: INTEGER
		do
			create l_time.make_now
	     	l_seed := l_time.hour
	      	l_seed := l_seed * 60 + l_time.minute
	      	l_seed := l_seed * 60 + l_time.second
	      	l_seed := l_seed * 1000 + l_time.milli_second
	     	create r.set_seed (l_seed)
			r.start
	        Result := r.item \\ number_of_animals + 1
	    ensure
	    	valid_range: Result >= 1 and Result <= number_of_animals
		end
end
