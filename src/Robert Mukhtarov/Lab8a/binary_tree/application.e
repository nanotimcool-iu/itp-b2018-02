class
	APPLICATION

create
	make

feature {NONE}

	make
		local
			t, t_left, t_right, tl_left: BIN_TREE [INTEGER]
		do
			create t.make (10)
			create t_left.make (1)
			create t_right.make (2)
			create tl_left.make (3)

			t.add_left (t_left)
			t.add_right (t_right)
			if attached t.left as left then
				left.add_left (tl_left)
			end

			print (t.height)
		end

end
