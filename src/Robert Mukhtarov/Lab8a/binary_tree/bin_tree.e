class
	BIN_TREE [G]

create
	make

feature
	info: G
	left, right: detachable BIN_TREE [G]

	make (x: G)
		do
			info := x
		end

	add_left (t: BIN_TREE [G])
		require
			no_left_child_behind: left = Void
		do
			left := t
		end

	add_right (t: BIN_TREE [G])
		require
			no_left_child_behind: right = Void
		do
			right := t
		end

	height: INTEGER
		local
			lh, rh: INTEGER
		do
			if attached left as l then
				lh := l.height
			end
			if attached right as r then
				rh := r.height
			end
			Result := 1 + lh.max (rh)
		end
end
