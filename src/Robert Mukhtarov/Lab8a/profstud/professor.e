class
	PROFESSOR

create
	make

feature {NONE}
	name, exam_text, exam_grade: STRING

	make (a_name, a_exam_text, a_exam_grade: STRING)
		do
			set_name (a_name)
			set_exam_text (a_exam_text)
			set_exam_grade (a_exam_grade)
		end

	set_name (a_name: STRING)
		do
			name := a_name
		end

	set_exam_text (text: STRING)
		do
			exam_text := text
		end

	set_exam_grade (grade: STRING)
		do
			exam_grade := grade
		end


feature {STUDENT}
	get_exam_text: STRING
		do
			Result := exam_text
		end

	get_exam_grade: STRING
		do
			Result := exam_grade
		end

feature
	get_name: STRING
		do
			Result := name
		end
end
