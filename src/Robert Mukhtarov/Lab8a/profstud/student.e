class
	STUDENT

create
	make

feature {NONE}
	name: STRING

	make (a_name: STRING)
		do
			set_name (a_name)
		end

	set_name (a_name: STRING)
		do
			name := a_name
		end

	exam_status (prof: PROFESSOR): STRING
		do
			Result := prof.get_exam_text
		end

	exam_grade (prof: PROFESSOR): STRING
		do
			Result := prof.get_exam_grade
		end

feature
	get_name: STRING
		do
			Result := name
		end
end
