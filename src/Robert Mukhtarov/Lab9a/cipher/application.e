class
	APPLICATION

create
	make

feature {NONE}
	make
		local
			cc: COMBINED_CIPHER
		do
			create cc.make ("BUSY")
			cc.set_key (cc.encrypt ("MYLASTASSIGNMENT"))
			print (cc.decrypt ("LAGHYH QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF FAZ2/TGBZKFI REE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIUL ZIE KFYGEL//:RLH DXE Z"))
		end

end
