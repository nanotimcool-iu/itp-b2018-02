class
	COMBINED_CIPHER
inherit
	CIPHER
create
	make

feature
	key: STRING
	vc: VIGENERE_CIPHER
	sp: SPIRAL_CIPHER

	make (k: STRING)
		do
			set_key (k)
			create vc.make (key)
			create sp.make
		end

	encrypt (message: STRING): STRING
		do
			Result := sp.encrypt (vc.encrypt (message))
		end

	decrypt (message: STRING): STRING
		do
			Result := vc.decrypt (sp.decrypt (message))
		end

	set_key (k: STRING)
		do
			key := k
		ensure
			key = k
		end


end
