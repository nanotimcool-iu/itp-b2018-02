class
	SPIRAL_CIPHER
inherit
	CIPHER
create
	make

feature
	make
		do  end

	create_matrix (message: STRING): ARRAY2 [STRING]
		local
			size, i, j, k: INTEGER
			matrix: ARRAY2 [STRING]
		do
			size := matrix_size (message)
			create matrix.make_filled ("", size, size)
			k := 1
			from i := 1 until i > size loop
				from j := 1 until j > size loop
					if k <= message.count then
						matrix.item (i, j) := message [k].out
					end
					k := k + 1
					j := j + 1
				end
				i := i + 1
			end
			Result := matrix
		end

		matrix_size (message: STRING): INTEGER
			local
				size: INTEGER
			do
				from until size * size > message.count loop
					size := size + 1
				end
				Result := size
			end

	encrypt (message: STRING): STRING
			local
				matrix: ARRAY2 [STRING]
				i, n, k: INTEGER
				finish: BOOLEAN
			do
				matrix := create_matrix (message)
				n := matrix_size (message)
				k := 1
				Result := ""
				from i := 1 until i > n loop Result := Result + matrix.item (i, n); matrix.item (i, n) := "0"; i := i + 1 end
				from i := n - 1 until i < 1 loop Result := Result + matrix.item (n, i); matrix.item (n, i) := "0"; i := i - 1 end
				from i := n - 1 until i < 1 loop Result := Result + matrix.item (i, 1); matrix.item (i, 1) := "0"; i := i - 1 end
				from until finish loop
					from i := k + 1 until matrix.item (k, i).is_equal ("0") loop
						Result := Result + matrix.item (k, i); matrix.item (k, i) := "0"; i := i + 1
					end

					from i := k + 1 until matrix.item (i, n - k).is_equal ("0") loop
						Result := Result + matrix.item (i, n - k); matrix.item (i, n - k) := "0"; i := i + 1
					end

					from i := n - k - 1 until matrix.item (n - k, i).is_equal ("0") loop
						Result := Result + matrix.item (n - k, i); matrix.item (n - k, i) := "0"; i := i - 1
					end

					from i := n - k - 1 until matrix.item (i, k + 1).is_equal ("0") loop
						Result := Result + matrix.item (i, k + 1); matrix.item (i, k + 1) := "0"; i := i - 1
					end
					if matrix.item (i + 1, k + 2).is_equal ("0") then finish := True else k := k + 1 end
				end
			end

		decrypt (message: STRING): STRING
			local
				matrix: ARRAY2 [STRING]
				i, k, r, c, n, index, blanks, blanks_start_index: INTEGER
				msg: STRING
				finish: BOOLEAN
			do
				Result := ""
				n := matrix_size (message)
				create matrix.make_filled ("", n, n)
				msg := reverse (message)
				blanks := n * n - msg.count
				blanks_start_index := n - blanks
				r := (n / 2).ceiling
				c := r
				k := 1
				matrix.item (r, c) := msg [1].out
				if n \\ 2 = 0 then
					from until finish loop
						from i := 1 until i > k loop
							r := r + 1
							index := index + 1
							matrix.item (r, c) := msg [index].out
							i := i + 1
						end

						from i := 1 until i > k loop
							if r = n and c >= blanks_start_index then
								c := c + 1
								matrix.item (r, c) := ""
							else
								c := c + 1
								index := index + 1
								matrix.item (r, c) := msg [index].out
							end
							i := i + 1
						end
						k := k + 1

						from i := 1 until i > k or finish loop
							r := r - 1
							index := index + 1
							matrix.item (r, c) := msg [index].out
							if r <= 1 then
								finish := True
							end
							i := i + 1
						end

						if not finish then
							from i := 1 until i > k loop
								c := c - 1
								index := index + 1
								matrix.item (r, c) := msg [index].out
								i := i + 1
							end
							k := k + 1
						end
					end

				else
					from until finish loop
						from i := 1 until i > k or finish loop
							r := r - 1
							index := index + 1
							matrix.item (r, c) := msg [index].out
							if r <= 1 then
								finish := True
							end
							i := i + 1
						end

						if not finish then
							from i := 1 until i > k loop
								c := c - 1
								index := index + 1
								matrix.item (r, c) := msg [index].out
								i := i + 1
							end
							k := k + 1

							from i := 1 until i > k loop
								r := r + 1
								index := index + 1
								matrix.item (r, c) := msg [index].out
								i := i + 1
							end

							from i := 1 until i > k loop
								if r = n and c >= blanks_start_index then
									c := c + 1
									matrix.item (r, c) := ""
								else
									c := c + 1
									index := index + 1
									matrix.item (r, c) := msg [index].out
								end
								i := i + 1
							end
							k := k + 1
						end
					end
				end

				from i := 1 until i > n loop
					from k := 1 until k > n loop
						Result := Result + matrix.item (i, k)
						k := k + 1
					end
					i := i + 1
				end
			end

		reverse (str: STRING): STRING
			local
				i: INTEGER
				res: STRING
			do
				res := ""
				from i := str.count until i = 0 loop
					res := res + str.at (i).out
					i := i - 1
				end
				Result := res
			end
end
