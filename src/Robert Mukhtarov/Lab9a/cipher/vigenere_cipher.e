class
	VIGENERE_CIPHER
inherit
	CIPHER
create
	make

feature
	key: STRING
	letters: ARRAY [CHARACTER]
	letter_nums, key_nums: ARRAY [INTEGER]

	make (k: STRING)
		local
			i, a, j: INTEGER
		do
			key := k

			a := 65
			create letters.make_filled ('A', 0, 25)
			from i := 0 until i > 25 loop
				letters [i] := a.to_character_8
				i := i + 1
				a := a + 1
			end

			create letter_nums.make_filled (0, 0, 25)
			from i := 0 until i > 25 loop
				letter_nums [i] := i
				i := i + 1
			end

			create key_nums.make_filled (0, 1, key.count)
			from i := 1 until i > key.count loop
				from j := 0 until j > letters.count - 1 loop
					if letters [j] = key [i] then
						key_nums [i] := letter_nums [j]
					end
					j := j +1
				end
				i := i + 1
			end
			ensure
				key = k
		end

	encrypt (message: STRING): STRING
		local
			i, j, char_num, key_num: INTEGER
			found: BOOLEAN
		do
			i := 1; key_num := 1; Result := ""
			from until i > message.count loop
				found := False
				from j := 0 until j > letters.count - 1 loop
					if letters [j] = message [i] then
						char_num := letter_nums [j]
						found := True
					end
					j := j + 1
				end
				if found then
					char_num := char_num + key_nums [key_num]
					if char_num > letters.count - 1 then
						char_num := char_num - letters.count
					end

					Result := Result + letters [char_num].out

					key_num := key_num + 1
					if key_num > key.count then
						key_num := 1
					end
				else Result := Result + message [i].out
				end
				i := i + 1
			end
		end

	decrypt (message: STRING): STRING
		local
			i, j, char_num, key_num: INTEGER
			found: BOOLEAN
		do
			i := 1; key_num := 1; Result := ""
			from until i > message.count loop
				found := False
				from j := 0 until j > letters.count - 1 loop
					if letters [j] = message [i] then
						char_num := letter_nums [j]
						found := True
					end
					j := j + 1
				end
				if found then
					char_num := char_num - key_nums [key_num]
					if char_num < 0 then
						char_num := char_num + letters.count
					end

					Result := Result + letters [char_num].out

					key_num := key_num + 1
					if key_num > key.count then
						key_num := 1
					end
				else Result := Result + message [i].out
				end
				i := i + 1
		end
	end
end
