class
	BOOK
create
	make

feature {NONE}
	title, author: STRING
	bestseller, available_to_children: BOOLEAN
	quantity, overdue_days, cost: INTEGER

	make (a_title, a_author: STRING; a_bestseller, a_available_to_children: BOOLEAN; a_quantity, a_cost: INTEGER)
		do
			title := a_title
			author := a_author
			if a_bestseller then make_bestseller else make_not_bestseller end
			available_to_children := a_available_to_children
			quantity := a_quantity
			cost := a_cost
		end

feature
	increase_quantity (num: INTEGER)
		do
			quantity := quantity + num
		ensure
			quantity = old + num
		end

	decrease_quantity (num: INTEGER)
		do
			quantity := quantity - num
		ensure
			quantity = old - num
		end

	get_quantity: INTEGER
		do
			Result := quantity
		end

	get_title: STRING
		do
			Result := title
		end

	get_author: STRING
		do
			Result := author
		end

	get_cost: INTEGER
		do
			Result := cost
		end

	make_bestseller
		do
			bestseller := True
			overdue_days := 14
		end

	make_not_bestseller
		do
			bestseller := False
			overdue_days := 21
		end

	get_overdue_days: INTEGER
		do
			Result := overdue_days
		end
end
