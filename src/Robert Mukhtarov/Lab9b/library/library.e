class
	LIBRARY
create
	make

feature
	books_list: ARRAYED_LIST [BOOK]
	users: ARRAYED_LIST [PERSON]
	current_id: INTEGER

	make
		do
			create books_list.make (0)
			create users.make (0)
			current_id := 1
		end

	add_user (p: PERSON)
		require
			p.id = Void
		do
			p.set_id (current_id)
			current_id := current_id + 1
		end

	user_info (p: PERSON): STRING
		require
			p.id /= Void
		local
			i: INTEGER
		do
			Result := p.get_name + "'s checked out books:"
			from i := 1 until i > p.get_books.count loop
				Result := Result + p.get_books [i].get_title + " by " + p.get_books [i].get_author + ": " + p.get_books_days [i].out + " days%N"
				i := i + 1
			end
		end

	available_books (p: PERSON): STRING
		local
			i: INTEGER
		do
			Result := "The list of books:"
			from i := 1 until i > books_list.count loop
				if available (p, books_list [i]) then
					Result := books_list [i].get_title + " by " + books_list [i].get_author + "%N"
				end
				i := i + 1
			end
		end

	available (p: PERSON; b: BOOK): BOOLEAN
		require
			p.id /= Void
		do
			if p.child = False and books_list.has (b) and b.get_quantity > 0 then
				Result := True
			end
		end

	check_out (p: PERSON; b: BOOK)
		require
			available (p, b) = True
		do
			if not (p.child and p.checked_out_books = 5) then
				p.add_book (b)
				b.decrease_quantity (1)
			end
		end

	return (p: PERSON; b: BOOK)
		require
			p.books.has (b)
		do
			p.delete_book (b)
			b.increase_quantity (1)
		end

	add_days (d: INTEGER)
		local
			i, j: INTEGER
		do
			from i := 1 until i > d loop
				across users as user loop
					from j := 1 until j > user.item.books.count loop
						user.item.books_days [j] := user.item.books_days [j] + 1
						if user.item.books_days [j] > user.item.books [j].get_overdue_days then
							if user.item.fine [j] < user.item.books [j].get_cost then
								user.item.fine [j] := user.item.fine [j] + 100
								if  user.item.fine [j] > user.item.books [j].get_cost then
									user.item.fine [j] := user.item.books [j].get_cost
								end
							end
						end
						j := j + 1
					end
				end
				i := i + 1
			end
		end
end
