class
	PERSON
create
	make

feature
	name, address, password: STRING
	number, age: INTEGER
	child: BOOLEAN
	books: ARRAYED_LIST [BOOK]
	books_days, fine: ARRAYED_LIST [INTEGER]
	id: detachable INTEGER

	make (a_name, a_address, a_password: STRING; a_number, a_age: INTEGER)
		do
			name := a_name
			address := a_address
			password := a_password
			number := a_number
			age := a_age
			if age <= 12 then
				child := True
			end
			create books.make (0)
			create books_days.make (0)
			create fine.make (0)
		end

feature
	add_book (b: BOOK)
		do
			books.extend (b)
			books_days.extend (0)
			fine.extend (0)
		end

	delete_book (b: BOOK)
		require
			books.has (b)
		local
			book_index: INTEGER
		do
			book_index := books.index_of (b, 1)
			books.prune (b)
			books_days.prune (books_days [book_index])
			fine.prune (fine [book_index])
		ensure
			books.has (b) = False
		end

	set_id (a_id: INTEGER)
		do
			id := a_id
		end

	checked_out_books: INTEGER
		do
			Result := books.count
		end

	get_books: ARRAYED_LIST [BOOK]
		do
			Result := books
		end

	get_books_days: ARRAYED_LIST [INTEGER]
		do
			Result := books_days
		end

	get_name: STRING
		do
			Result := name
		end
end
