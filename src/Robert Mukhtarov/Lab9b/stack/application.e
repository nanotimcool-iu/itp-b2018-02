class
	APPLICATION

create
	make

feature {NONE}
	make
		local
			s: MY_STACK [INTEGER]
			s_b: MY_BOUNDED_STACK [INTEGER]
		do
			create s.make (1)
			s.push (3)
			s.push (5)
			s.remove
			s.remove
			create s_b.make (1)
			s_b.set_capacity (2)
			s_b.push (4)
		end

end
