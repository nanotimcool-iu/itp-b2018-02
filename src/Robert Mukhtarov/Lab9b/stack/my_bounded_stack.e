class
	MY_BOUNDED_STACK [G]

inherit
	MY_STACK [G]
		redefine
			make, push
		end

create
	make

feature
	capacity: INTEGER

	make (element: G)
	-- Creates a stack and sets capacity to 1.
		do
			create items.make (0)
			items.extend (element)
			capacity := 1
		end

	set_capacity (cap: INTEGER)
	-- Sets capacity to the value of cap.
		require
			cap >= size
		do
			capacity := cap
		end

	push (element: G)
	-- Adds an element to the collection.
		require else
			size < capacity
		do
			items.extend (element)
		end

end
