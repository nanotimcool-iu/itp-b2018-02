class
	MY_STACK [G]

create
	make

feature {NONE}
	items: ARRAYED_LIST [G]

	make (element: G)
		do
			create items.make (0)
			items.extend (element)
		end

feature
	push (element: G)
	-- Adds an element to the collection.
		do
			items.extend (element)
		end

	remove
	-- Removes the most recently added element.
		require
			size > 0
		do
			items.remove_right
		end

	item: G
	-- Gives access to the top of the stack.
		require
			is_empty = False
		do
			Result := items.last
		end

	is_empty: BOOLEAN
	-- Checks whether the stack has elements or not.
		do
			if items.is_empty then
				Result := True
			end
		end

	size: INTEGER
	-- Counts items in the stack.
		do
			Result := items.count
		end
end
