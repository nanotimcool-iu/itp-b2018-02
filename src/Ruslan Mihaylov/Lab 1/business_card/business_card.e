class
	BUSINESS_CARD

create
	fill_in

feature {NONE} -- Initialization

	fill_in
			-- Fill in the card and print it.
		do
			-- Add your code here.
			Io.putstring("Enter your name: ")
			Io.readline
			set_name(Io.last_string)
			Io.putstring("Enter your job: ")
			Io.readline
			set_job(Io.last_string)
			Io.putstring("Enter your age: ")
			Io.read_integer
			set_age(Io.last_integer)
			print_card

		end

feature -- Access

	name: STRING
			-- Owner's name.

	job: STRING
			-- Owner's job.

	age: INTEGER
			-- Owner's age.

feature -- Setting

	set_name (a_name: STRING)
			-- Set `name' to `a_name'.
		require
			name_exists: a_name /= Void
		do
			name := a_name.twin
		end

	set_job (a_job: STRING)
			-- Set `job' to `a_job'.
		require
			job_exists: a_job /= Void
		do
			job := a_job.twin
		end

	set_age (a_age: INTEGER)
			-- Set `age' to `a_age'.
		require
			age_non_negative: a_age >= 0
		do
			age := a_age
		end

feature -- Output
	age_info: STRING
			-- Text representation of age on the card.
		do
			Result := age.out + " years old"
		end

	Width: INTEGER = 50
			-- Width of the card (in characters), excluding borders.

	line (n: INTEGER): STRING
			-- Horizontal line on length `n'.
		do
			Result := "-"
			Result.multiply (n)
		end

	spaces (n: INTEGER): STRING
			-- Return n spaces
		do
			Result := " "
			Result.multiply(n)
		end

	print_card
			-- Arranges your personal information in a presentable way (or maybe not)
		do
			print(line(width) + "%N")
			print("| " + "Your name: " + name + spaces(width - 14 - name.count) + "|" + "%N")
			print("| " + "Your job: " + job + spaces(width - 13 - job.count) + "|" + "%N")
			print("| " + "Your age: " + age.out + spaces(width - 13 - age.out.count) + "|" + "%N")
			print(line(width))
		end


end
