class
	APPLICATION

create
	execute

feature {NONE} -- Initialization

	name: STRING = "Ruslan Mikhailov"
	age: INTEGER = 17
	mother_tongue: STRING = "Russian"
	has_a_cat: BOOLEAN = False

	execute
			-- Run application.
		do
			print("Name: " + name + "%N")
			print("Age: " + age.out + "%N")
			print("Mother tongue: " + mother_tongue + "%N")
			print("Has a cat: " + has_a_cat.out)
		end

end
