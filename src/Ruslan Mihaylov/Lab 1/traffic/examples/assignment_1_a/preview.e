note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	i: INTEGER
	explore
			-- Modify the map.
		do
			Zurich.add_station("Zoo", 500, 500)
			Zurich.connect_station(24, "Zoo")
			Zurich_map.update
			from
				i := 1
			until
				i = 0
			loop
				Zurich_map.station_view(Zurich.station("Zoo")).highlight
				wait(1)
				Zurich_map.station_view(Zurich.station("Zoo")).unhighlight
				wait(1)
				i := i + 1
			end

		end

end
