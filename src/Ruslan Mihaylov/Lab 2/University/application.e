note
	description: "University application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	ITP: COURSE

	make
			-- Run application.
		do
			create ITP.create_course ("ITP", 192192, "Every day", 49)
			print(ITP.name + "%N")
			print(ITP.id.out + "%N")
			print(ITP.schedule + "%N")
			print(ITP.st.out + "%N")
		end

end
