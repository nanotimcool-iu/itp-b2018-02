note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

create
	create_course

feature
	create_course(n_name: STRING; n_id: INTEGER; n_schedule: STRING; n_st: INTEGER)
	local
		max_st, min_st: INTEGER
	do
		max_st := 50
		min_st := 3
		if n_st <= max_st and n_st >= min_st then --creating a course
			set_name(n_name)
			set_id(n_id)
			set_schedule(n_schedule)
			set_st(n_st)
		else --creating an empty course
			set_name("")
			set_id(0)
			set_schedule("")
			set_st(0)
		end
	end

feature
	--initialization of variables
	name, schedule: STRING
	id, st: INTEGER

	set_name(s: STRING)
	do
		name := s
	end

	set_schedule(s: STRING)
	do
		schedule := s
	end

	set_id(n: INTEGER)
	do
		id := n
	end

	set_st(n: INTEGER)
	do
		st := n
	end

end
