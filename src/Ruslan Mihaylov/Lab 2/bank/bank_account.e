note
	description: "Summary description for {BANK_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

create
	make
feature


	set_name(s: STRING)
	do
		name := s
	end

	set_balance(n: INTEGER)
	do
		balance := n
	end

	check_info
	do
		print("Your balance is " + balance.out + " rubles" + "%N")
	end

	deposit(n: INTEGER)
	do
		if balance + n > max_b then
			set_balance(max_b)
		else
			set_balance(balance + n)
		end
		check_info
	end

	withdraw(n: INTEGER)
	do
		if balance - n < min_b then
			print("Error! You cannot have that little money :c" + "%N")
		else
			set_balance(balance - n)
		end
		check_info
	end

feature
	name: STRING
	balance: INTEGER
	min_b: INTEGER = 100
	max_b: INTEGER = 10000000

	make
	do
		set_name("Ruslan")
		set_balance(0)
		check_info
		deposit(10000)
		withdraw(1337)
		withdraw(100000)
	end
end
