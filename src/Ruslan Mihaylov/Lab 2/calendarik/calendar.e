note
	description: "calendarik application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	me, max, alex: PERSON
	a1, a2: ENTRY
	t: TIME

	make
			-- Run application.
		do
			create t.make_now
			me := create_person("Ruslan", 85672345464, "Innopolis", "rus.lan@gmail.com")
			alex := create_person("Alex", 89764975646, "Innopolis", "leha1leha@mail.ru")
			max := create_person("Max", 89593751232, "Innopolis", "max.apple@apple.com")
			a1 := create_entry (t, alex, "Meeting with Ruslan", void)
			a2 := create_entry (t, me, "Meeting with Alex", "Innopolis")
			print("These two entries {a1, a2} are in conflict: " + in_conflict(a1, a2).out + "%N")
			edit_date (a1, t.origin)
			print("New date of a1: " + a1.date.out + "%N")
			print(get_owner_name(a1) + " is the owner of e1 entry")
		end

feature
	create_entry(a: TIME; b: PERSON; c: STRING; d: detachable STRING): ENTRY
	do
		create Result.add_entry(a, b, c, d)
	end

	create_person(a: STRING; b: INTEGER_64; c, d: STRING): PERSON
	do
		create Result.add_person(a, b, c, d)
	end

	edit_subject(e: ENTRY; new_subject: STRING)
	do
		e.set_subject(new_subject)
	end

	edit_date(e: ENTRY; new_date: TIME)
	do
		e.set_date(new_date)
	end

	get_owner_name(e: ENTRY): STRING
	do
		Result := e.owner.name
	end

	in_conflict(e1, e2: ENTRY): BOOLEAN
	do
		if e1.date = e2.date or e1.place = e2.place then
		Result := True
		else
		Result := False
		end
	end

end
