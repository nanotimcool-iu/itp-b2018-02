note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

create
	make

feature --contacts
	contact_1, contact_2, contact_3: CONTACT
feature
	make
	do
		contact_1:=create_contact("Ruslan", 89794657307, "4-309", "ruslan256@mail.ru", void)
		contact_1.info
		contact_2:=create_contact("Leha", 89524765878, "4-310", "ale.xe.y@mail.ru", void)
		contact_2.info
		contact_3:=create_contact("Max", 89135672345, "4-311", "mackos@mail.ru", contact_1)
		contact_3.info
		add_emergency_contact(contact_1, contact_2)
		contact_1.info
		add_emergency_contact(contact_2, contact_3)
		contact_2.info
		edit_contact(contact_2, contact_2.name, 89561745656, contact_2.work_place, "fistyfifty@mail.ru")
		contact_2.info
		remove_emergency_contact(contact_2)
		contact_2.info
		remove_emergency_contact(contact_3)
		contact_3.info

	end

feature
	create_contact(a: STRING; b: INTEGER_64; c, d: STRING; e: detachable CONTACT): CONTACT
	do
		create Result.add_contact(a, b, c, d, e)
	end

	edit_contact(c: CONTACT; a: STRING; b: INTEGER_64; d, e: STRING)
	do
		c.set_name(a)
		c.set_phone_number(b)
		c.set_work_place(d)
		c.set_email(e)
	end

	add_emergency_contact(c1, c2: CONTACT)
	do
		c1.set_call_emergency(c2)
	end

	remove_emergency_contact(c: CONTACT)
	do
		c.set_call_emergency(void)
	end
end
