note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

create
	add_contact

feature
	name: STRING
	phone_number: INTEGER_64
	work_place: STRING
	email: STRING
	call_emergency: detachable CONTACT

	--just some setters here

	set_name(s: STRING)
	do
		name := s
	end

	set_email(s: STRING)
	do
		email := s
	end

	set_work_place(s: STRING)
	do
		work_place := s
	end

	set_phone_number(n: INTEGER_64)
	do
		phone_number := n
	end

	set_call_emergency(c: detachable CONTACT)
	do
		if attached c as c_local then
		call_emergency := c_local
		else
		call_emergency := void
		end
	end
	info
	do
		print("Name: " + name + "%N")
		print("Number: " + phone_number.out + "%N")
		print("Work place: " + work_place + "%N")
		print("Email: " + email + "%N")
		if attached call_emergency as call_em_new then
		print("Name of emergency contact: " + call_em_new.name + "%N")
		else
		print("No emergency contact"  + "%N")
		end
		print("-----------------------------------" + "%N")
	end

feature
	add_contact(n_name: STRING; n_phone_number: INTEGER_64; n_work_place, n_email: STRING; n_call_emergency: detachable CONTACT)
	do
		set_name(n_name)
		set_phone_number(n_phone_number)
		set_work_place(n_work_place)
		set_email(n_email)
		set_call_emergency(n_call_emergency)
	end

end
