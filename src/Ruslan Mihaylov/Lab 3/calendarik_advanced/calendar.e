 note
	description: "calendarik_advanced application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	me, max, alex: PERSON
	e1, e2, e3: ENTRY

	make
		local
			events: LIST[ENTRY]
			init_events: LINKED_LIST[ENTRY]
			t: TIME
			test: LIST[ENTRY]
			-- Run application.
		do
			amount_of_days := 31
			create init_days.make
			create init_events.make
			days := init_days
			events := init_events
			--creating the list of days
			from
				c := 1
			until
				c = amount_of_days + 1
			loop
				days.extend(create_day(c, events))
				c := c + 1
			end
			create t.make_now
			me := create_person("Ruslan", 89265767879, "Innopolis", "i_love_eiffel@eiffel_lover.net")
			alex := create_person("Alex", 89768675646, "Innopolis", "strelni_sijku@neblagodarnaya")
			max := create_person ("Max", 89465768989, "Innopolis", "prigotov_edoo@nadoel")

			e1 := create_entry(t, alex, "meeting with max", "Koorilka")
			e2 := create_entry(t, me, "eiffel lab", "3-309")
			e3 := create_entry(t, max, "sijka", "Koorilka")

			add_entry(e1, 4)
			add_entry(e2, 4)

		    print(get_owner_name(e2))








		end

feature
	--vars
	amount_of_days, c: INTEGER
	days: LIST[DAY]
	init_days: LINKED_LIST[DAY]

	create_person(a: STRING; b: INTEGER_64; just_a_variable,d: STRING): PERSON
	local
		p: PERSON
	do
		create p.add_person(a, b, just_a_variable, d)
		Result := p
	end

	create_day(d: INTEGER; l: LIST[ENTRY]): DAY
	local
		i_hate_eiffel: DAY
	do
		create i_hate_eiffel.add_day(d, l)
		Result := i_hate_eiffel
	end

	create_entry(a: TIME; b: PERSON; hj: STRING; d: detachable STRING): ENTRY
	local
		life_is_cool_but_eiffel_is_not: ENTRY
	do
		create life_is_cool_but_eiffel_is_not.n_create_entry(a, b, hj, d)
		Result := life_is_cool_but_eiffel_is_not
	end

	add_entry(e: ENTRY; day: INTEGER)
	do
		days[day].events.extend(e)
	end

	edit_subject(e: ENTRY; new_subject: STRING)
	do
		e.set_subject(new_subject)
	end

	edit_date(e: ENTRY; new_date: TIME)
	do
		e.set_date(new_date)
	end

	get_owner_name(e: ENTRY): STRING
	do
		Result := e.owner.name + "%N"
	end

	in_conflict(day: INTEGER): LIST[ENTRY]
	local
		a: LIST[ENTRY]
		main: LIST[ENTRY]
		bool: LIST[BOOLEAN]
		init_a: LINKED_LIST[ENTRY]
		init_bool: LINKED_LIST[BOOLEAN]
		i, j, d: INTEGER
	do
		create init_a.make
		create init_bool.make
		a := init_a
		bool := init_bool
		main := days[day].events
		d := days[day].events.count

		--setting a bool arrrrrray of falses
		from
			i := 1
		until
			i = d + 1
		loop
			bool[i] := False
		end
		if d < 2 then
			print("Not enough events" + "%N")
		else
		--the main cycle of perebor
		from
			i := 1
		until
			i = d
		loop
			--cycle inside the cycle WOW
			from
				j := i + 1
			until
				j = d + 1
			loop
				if main[i].date = main[j].date or main[i].place = main[j].place then
				bool[i] := True
				bool[j] := True
				end
			end
		end
		--forming an output based on BOOL array
		from
			i := 1
		until
			i = d + 1
		loop
			if bool[i] = True then
				a.extend(main[i])
			end
		end
		end
		Result := a
	end

	printable_month: STRING
	local
		s: STRING
		i, j, m: INTEGER
		e: LIST[ENTRY]
	do
		s := ""
		m := days[i].events.count
		e := days[i].events
		from
			i := 1
		until
			i = amount_of_days
		loop
			s := s + "Day " + i.out + ":" + "%N"
			s := s + " "
			--adding some info about meetings
			if m > 0 then
			from
				j := 1
			until
				j = m
			loop
				s := s + "Meeting with " + e[j].owner.name + " at " + e[j].date.out + " about " + e[j].subject
				if
					attached{STRING} e[j].place as s_local
				then
					s := s + "in " + s_local
				end
				print(s + "%N")

			end

			else
			s := s + "No meetings"
			end
			s := s + "%N"
		end
		Result := s
	end
end
