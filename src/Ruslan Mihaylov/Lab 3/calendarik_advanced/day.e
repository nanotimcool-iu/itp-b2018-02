note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	add_day

feature
	date: INTEGER
	events: LIST[ENTRY]

feature
	--SETTERS ONLY
	set_date(d: INTEGER)
	do
		date := d
	end

	set_events(e: LIST[ENTRY])
	do
		events := e
	end

	add_day(n_date: INTEGER; n_events: LIST[ENTRY])
	do
		set_date(n_date)
		set_events(n_events)
	end

end
