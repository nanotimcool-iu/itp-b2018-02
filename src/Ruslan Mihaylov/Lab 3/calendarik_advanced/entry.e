note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	n_create_entry
feature
	--variabLes
	date: TIME
	owner: PERSON
	subject: STRING
	place: detachable STRING

feature
	--I know u want sum setrs hear
	set_date(d: TIME)
	do
		date := d
	end

	set_owner(p: PERSON)
	do
		owner := p
	end

	set_subject(s: STRING)
	do
		subject := s
	end

	set_place(s: detachable STRING)
	do
		if attached s as locas_s then
		place := s
		else
		place := void
		end
	end

feature
	--ayo wassup wanna some construKtoRZ?
	n_create_entry(n_date: TIME; n_owner: PERSON; n_subject: STRING n_place: detachable STRING)
	do
		set_date(n_date)
		set_owner(n_owner)
		set_subject(n_subject)
		set_place(n_place)
	end

end
