note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	add_person

feature
	--some vars here
	name: STRING
	phone_number: INTEGER_64
	work_place: STRING
	email: STRING

feature
	--time to sEt some sEttErs
	set_name(s: STRING)
	do
		name := s
	end

	set_phone_number(n: INTEGER_64)
	do
		phone_number := n
	end

	set_work_place(s: STRING)
	do
		work_place := s
	end

	set_email(s: STRING)
	do
		email := s
	end

	add_person(n_name: STRING; n_number: INTEGER_64; n_work_place, n_email: STRING)
	do
		set_name(n_name)
		set_phone_number(n_number)
		set_work_place(n_work_place)
		set_email(n_email)
	end

end
