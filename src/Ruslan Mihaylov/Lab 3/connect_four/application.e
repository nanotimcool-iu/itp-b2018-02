note
	description: "connect_four application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	board: ARRAY2[INTEGER]
	m, n, turn_max, turn, turn_last, a: INTEGER
	start: BOOLEAN

	make
			-- Run application.
		do
			print("Rows: " + "%N")
			Io.read_integer
			m := Io.last_integer
			print("Columns: " + "%N")
			Io.read_integer
			n := Io.last_integer

			if (m < 4 and n >= 4) or (m >= 4 and n < 4) or (m < 4 and n < 4) then
			start := False
			else
			start := True
			end

			create board.make_filled (0, m, n)

			if start = False then

			print("Invalid (you)" + "%N")

			else


			--game
			print_board
			turn_max := m * n

			from
				turn := 1
			until
				turn = turn_max + 1
			loop
				from
					a := n + 1
				until
					a > 0 and a <= n and available(a) > 0
				loop
					turn_info
					Io.read_integer
					a := Io.last_integer
				end

				if turn \\ 2 = 0 then
				board[available(a), a] := 2
				else
				board[available(a), a] := 1
				end




				print_board

				turn := turn + 1
				turn_last := turn
			end


			print("%N" + "GAME OVER" + "%N")
			end
		end

feature
	print_board
	local
		s: STRING
		i, j: INTEGER
	do
		from
			i := 1
		until
			i = m + 1
		loop
			s := ""
			from
				j := 1
			until
				j = n + 1
			loop
				s := s + board[i, j].out + " "

				j := j + 1
			end
			print(s + "%N")

			i := i + 1
		end
	end

	turn_info
	do
		if turn \\ 2 = 0 then
			print("%N" + "Player's " + "2" + " turn: " + "%N")
		else
			print("%N" + "Player's " + "1" + " turn: " + "%N")
		end
	end

	available(r: INTEGER): INTEGER
	local
		l, l_last: INTEGER
	do
		if board[1, r] /= 0 then
		Result := 0
		else
		from
			l := 1
		until
			l = m + 1
		loop
			if board[l, r] = 0 then
			l_last := l
			end
		end
		Result := l_last
		end
	end
end
