note
	description: "triangle application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	last, l: INTEGER

	make
			-- Run application.
		do
			print("The height of the triangle: ")
			Io.read_integer
			print("%N")
			l := Io.last_integer
			last := count_last(l + 1)
			print_line(l + 1)


		end

feature
	k : INTEGER
	count_last(num: INTEGER): INTEGER
	local
		s, prev: STRING
		i, b: INTEGER
	do
		k := 1
		prev := ""
		s := ""
		from
			i := 1
		until
			i = num + 1
		loop
			b := s.count
			from
			until
				s.count > prev.count
			loop
				s := s + k.out + " "
				k := k + 1
			end
			prev := s
			--print(s + "%N")
			s := ""
			i := i + 1


		end
		k := 1
		Result := prev.count - 1
	end
	print_line(num: INTEGER)
	local
		s, prev, actual: STRING
		i, a, b, a_a: INTEGER
		used: LIST[INTEGER]
		init_used: LINKED_LIST[INTEGER]
	do
		create init_used.make
		used := init_used

		k := 1
		prev := ""
		s := ""
		from
			i := 1
		until
			i = num
		loop
			b := s.count
			a := 0
			from
			until
				s.count > prev.count
			loop
				used.extend(k)
				s := s + k.out + " "
				k := k + 1
				a := a + 1

			end


			actual := s + spaces(2 * (last - s.count) - 1)
			a_a := a

			from
				a := 0
			until
				a = a_a
			loop
				actual := actual + (k - a - 1).out + " "
				a := a + 1
				used.prune(used.last)
			end

			actual := actual + "%N"
			prev := s
			print(actual)
			s := ""
			used := init_used
			i := i + 1


		end
		k := 1
		print("%N" + "OH MY GOD I DID IT" + "%N")
	end
	spaces(n: INTEGER): STRING
	local
		s: STRING
		i: INTEGER
	do
		s := ""
		from
			i := 1
		until
			i = n + 1
		loop
			s := s + " "
			i := i + 1
		end
		Result := s
	end
end
