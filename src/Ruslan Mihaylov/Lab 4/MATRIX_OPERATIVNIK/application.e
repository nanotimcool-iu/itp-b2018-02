note
	description: "MATRIX_OPERATIVNIK application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	a1, a2: ARRAY2[INTEGER]
	r, c, k, l: INTEGER

	make
			-- Run application.
		do
			--size of determinant
			r := 3
			c := r
			create a1.make_filled(0, r, c)
			create a2.make_filled(0, r, c)

			print("Enter "+ (r * c * 2).out + " numbers for a1 and a2 matrices: " + "%N")

			from
				k := 1
			until
				k = r + 1
			loop
				from
					l := 1
				until
					l = c + 1
				loop
					Io.read_integer
					a1[k, l] := Io.last_integer

					Io.read_integer
					a2[k, l] := Io.last_integer

					l := l + 1
				end

				k := k + 1
			end

			print_matrix(a1)

			print_matrix(a2)

			print_matrix(add(a1, a2))

			print_matrix(prod(a1, a2))

			print_matrix(column_out(1, a2))

			print(count_matrix(a1).out + "%N")
			print(count_matrix(a2).out + "%N")


		end

feature
	add(m1, m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	require
		equal_height: m1.height = m2.height
		equal_width: m1.width = m2.width

	local
		i, j, h, w: INTEGER
		m: ARRAY2[INTEGER]
	do
		h := m1.height
		w := m1.width

		create m.make_filled(0, h, w)

		from
			i := 1
		until
			i = h + 1
		loop
			from
				j := 1
			until
				j = w + 1
			loop
				m[i, j] := m1[i, j] + m2[i, j]

				j := j + 1
			end

			i := i + 1
		end
		Result := m
	end

	minus(m1, m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	require
		equal_height: m1.height = m2.height
		equal_width: m1.width = m2.width

	local
		i, j, h, w: INTEGER
		m: ARRAY2[INTEGER]
	do
		h := m1.height
		w := m1.width

		create m.make_filled(0, h, w)

		from
			i := 1
		until
			i = h + 1
		loop
			from
				j := 1
			until
				j = w + 1
			loop
				m[i, j] := m1[i, j] - m2[i, j]

				j := j + 1
			end

			i := i + 1
		end
		Result := m
	end

	prod(m1, m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	require
		equal_sides: m1.width = m2.height
	local
		i, j, kk, s, h, w: INTEGER
		m: ARRAY2[INTEGER]
	do
		h := m1.height
		w := m2.width
		s := m1.width

		create m.make_filled(0, h, w)

		from
			i := 1
		until
			i = h + 1
		loop
			from
				j := 1
			until
				j = w + 1
			loop
				from
					kk := 1
				until
					kk = s + 1
				loop
					m[i, j] := m[i, j] + m1[i, kk] * m2[kk, j]

					kk := kk + 1
				end

				j := j + 1
			end

			i := i + 1
		end
		Result := m
	end

	print_matrix(m: ARRAY2[INTEGER])
	local
		i, j, rows, columns: INTEGER
		s: STRING
	do
		print_line

		rows := m.height
		columns := m.width
		from
			i := 1
		until
			i = rows + 1
		loop
			s := ""
			from
				j := 1
			until
				j = columns + 1
			loop
				s := s + m[i, j].out + " "

				j := j + 1
			end

			i := i + 1
			print(s + "%N")
		end
		print_line
	end

	print_line
	do
		print("-------------------" + "%N")
	end

	count_matrix(m: ARRAY2[INTEGER]): INTEGER
	require
		equal_sides: m.width = m.height
	local
		n: INTEGER
		m_next: ARRAY2[INTEGER]
		i, j: INTEGER
	do
		n := m.width
		from
			i := 1
		until
			i = n + 1
		loop
			if n = 2 then
				r := m[1, 1] * m[2, 2] - m[1, 2] * m[2, 1]
				Result := r
			else
				m_next := m
				m_next := first_row_out(m_next)
				m_next := column_out(i, m_next)

				if
					(i + 2) \\ 2 = 0
				then
					Result := Result - m[1, i] * count_matrix(m_next)
				else
					Result := Result + m[1, i] * count_matrix(m_next)
				end

			end

			i := i + 1
		end
	end

	first_row_out(m: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	local
		m_res: ARRAY2[INTEGER]
		i, j, rows, columns: INTEGER
	do
		rows := m.height
		columns := m.width
		create m_res.make_filled(0, rows - 1, columns)

		from
			i := 2
		until
			i = rows + 1
		loop
			from
				j := 1
			until
				j = columns + 1
			loop
				m_res[i - 1, j] := m[i, j]

				j := j + 1
			end

			i := i + 1
		end

		Result := m_res
	end

	column_out(p: INTEGER; m: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	local
		m_res: ARRAY2[INTEGER]
		i, j, rows, columns: INTEGER
	do
		rows := m.height
		columns := m.width

		create m_res.make_filled(0, rows, columns - 1)

		from
			i := 1
		until
			i = rows + 1
		loop
			from
				j := 1
			until
				j = p
			loop
				m_res[i, j] := m[i, j]

				j := j + 1
			end

			from
				j := p + 1
			until
				j = columns + 1
			loop
				m_res[i, j - 1] := m[i, j]

				j := j + 1
			end


			i := i + 1
		end

		Result := m_res
	end

end
