note
	description: "VECTOR_OPERATIVNIK application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization


	dim: INTEGER
	v1: ARRAY[INTEGER]
	v2: ARRAY[INTEGER]


	make
		local
			i: INTEGER
			-- Run application.
		do
			dim := 3

			
			create v1.make_filled(0, 1, dim)
			create v2.make_filled(0, 1, dim)

			--init the v1
			print("Initialize v1: " + "%N")
			from
				i := 1
			until
				i = dim + 1
			loop
				Io.read_integer
				v1[i] := Io.last_integer

				i := i + 1
			end

			--init the v2
			print("Initialize v2: " + "%N")
			from
				i := 1
			until
				i = dim + 1
			loop
				Io.read_integer
				v2[i] := Io.last_integer

				i := i + 1
			end

			print("Dot product: " + dot_product(v1, v2).out + "%N")
			print("Cross product: ")
			print_vector(cross_product(v1, v2))
			print("Are of triangle: " + triangle_area(v1, v2).out + "%N")



		end

	print_vector(v: ARRAY[INTEGER])
	local
		s: STRING
		i: INTEGER
	do
		s := ""
		from
			i := 1
		until
			i = dim + 1
		loop
			s := s + v[i].out + " "

			i := i + 1
		end
		s := s + "%N"
		print(s)
	end


	cross_product(vector_1, vector_2: ARRAY[INTEGER]): ARRAY[INTEGER]
	require
		three_dimensional_vector: dim = 3
	local
		v_res: ARRAY[INTEGER]
	do
		create v_res.make_filled(0, 1, dim)

		v_res[1] := vector_1[2] * vector_2[3] - vector_1[3] * vector_2[2]
		v_res[2] := - (vector_1[1] * vector_2[3] - vector_1[3] * vector_2[1])
		v_res[3] := vector_1[1] * vector_2[2] - vector_1[2] * vector_2[1]


		Result := v_res
	end

	dot_product(vector_1, vector_2: ARRAY[INTEGER]): INTEGER
	local
		i: INTEGER
	do
		from
			i := 1
		until
			i = dim + 1
		loop
			Result := Result + vector_1[i] * vector_2[i]

			i := i + 1
		end
	end


	length(vector: ARRAY[INTEGER]): REAL_64
	local
		i: INTEGER
		s: REAL
	do
		s := 0
		from
			i := 1
		until
			i = dim + 1
		loop
			s := s + vector[i] * vector[i]

			i := i + 1
		end
		Result := (s ^ 1/2)
	end


	triangle_area(vector_1, vector_2: ARRAY[INTEGER]): REAL_64
	local
		la: REAL_64
	do
		la := length(cross_product(vector_1, vector_2))
		Result := la / 2
	end

end
