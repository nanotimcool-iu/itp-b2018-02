note
	description: "bags application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	bag: BAGG
	bagg: BAGG

	make
			-- Run application.
		do
			create bag.add_bag

			bag.add('c', 4)
			bag.add('b', 15)
			bag.add('z', 1)
			bag.show

			print(bag.min.out + "%N")
			print(bag.max.out + "%N")

			create bagg.add_bag
			bagg.add('b', 15)
			bagg.add('c', 4)
			bagg.add('z', 1)
			print(bag.is_equal_bag(bag).out + "%N")

			bag.remove('b', 16)
			bag.show


		end

end
