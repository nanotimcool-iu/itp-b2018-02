note
	description: "Summary description for {BAGG}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BAGG

create
	add_bag

feature
	--Vars
	elements: ARRAY[CELL_INFO]

feature
	--Functions
	show
	local
		i: INTEGER
		b: INTEGER
		s, sub: STRING
		k: INTEGER
	do
		if
			elements.is_empty
		then
			print(" " + "%N")
		else
			b := elements.count
			s := ""
			from
				i := 1
			until
				i = b + 1
			loop
				k := elements[i].number_copies
				if k /= 0 then
					sub := elements[i].value.out
					sub.multiply(elements[i].number_copies)
					s := s + sub
				end


				i := i + 1
			end
			print(s + "%N")
		end
	end

	add(val: CHARACTER; n: INTEGER)
	require
		positive_amount: n > 0
	local
		i: INTEGER
		index: INTEGER
		b: INTEGER
		cell: CELL_INFO
	do
		b := elements.count
		index := -1

		from
			i := 1
		until
			i = b + 1
		loop
			if elements[i].value = val then
				index := i
			end

			i := i + 1
		end



		if index /= -1 then
			elements[i].increase_number_copies(n)
		else
			create cell.add_cell_info(val, n)
			elements.force(cell, b + 1)
		end
	end

	remove(val: CHARACTER; n: INTEGER)
	require
		positive_amount: n > 0
	local
		i: INTEGER
		index: INTEGER
		b: INTEGER
		cell: CELL_INFO
	do
		b := elements.count
		index := -1

		from
			i := 1
		until
			i = b + 1
		loop
			if elements[i].value = val then
				index := i
			end

			i := i + 1
		end



		if index /= -1 then
			if
				elements[index].number_copies > n
			then
				elements[index].decrease_number_copies(n)
			else
				elements[index].set_number_copies(0)
			end
		else
			print("No such element found")
		end
	end

	max: CHARACTER
	require
		not_empty: elements.is_empty.negated
	local
		m: CHARACTER
		i, b: INTEGER
	do
		b := elements.count

		from
			i := 1
		until
			i = b + 1
		loop
			if
				i = 1
			then
				m := elements[i].value
			else
				if
					elements[i].value >= m
				then
					m := elements[i].value
				end
			end

			i := i + 1
		end

		Result := m
	end

	min: CHARACTER
	require
		not_empty: elements.is_empty.negated
	local
		m: CHARACTER
		i, b: INTEGER
	do
		b := elements.count

		from
			i := 1
		until
			i = b + 1
		loop
			if
				i = 1
			then
				m := elements[i].value
			else
				if
					elements[i].value <= m
				then
					m := elements[i].value
				end
			end

			i := i + 1
		end

		Result := m
	end

	is_equal_bag(b: BAGG): BOOLEAN
	do
		if
			current.elements = b.elements
		then
			Result := True
		else
			Result := False
		end
	end
feature
	add_bag
	do
		create elements.make_empty
	end
end
