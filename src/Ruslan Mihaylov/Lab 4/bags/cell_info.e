note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	add_cell_info

feature
	--Vars
	value: CHARACTER
	number_copies: INTEGER

feature
	--Functions
	set_value(c: CHARACTER)
	do
		value := c
	end

	set_number_copies(n: INTEGER)
	do
		number_copies := n
	end

	increase_number_copies(n: INTEGER)
	require
		positive_amount: n > 0
	do
		set_number_copies(number_copies + n)
	end

	decrease_number_copies(n: INTEGER)
	require
		negative_amount: n < 0
		less_than_exist: number_copies >= n
	do
		set_number_copies(number_copies - n)
	end


feature
	--Constructor
	add_cell_info(c: CHARACTER; n: INTEGER)
	do
		set_value(c)
		set_number_copies(n)
	end

end
