note
	description: "range application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	r1, r2: RANGE

	make
			-- Run application.
		do
			create r1.create_range(1, 3)
			create r2.create_range(-1, 1)
			r1.show
			r2.show
			r1.add(r2).show
			create r1.create_range(1, 9)
			create r2.create_range(1, 9)
			r1.show
			r2.show
			r1.subtract(r2).show
		end

end
