note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	create_range

feature
	left, right: INTEGER

feature
	show
	do
		print(left.out + " " + right.out + "%N")
	end

	is_equal_range(other: like current): BOOLEAN
	do
		if
			left = other.left and right = other.right
		then
			Result := True
		else
			Result := False
		end
	end

	is_empty: BOOLEAN
	do
		if
			right <= left
		then
			Result := True
		else
			Result := False
		end
	end

	is_sub_range_or(other: like current): BOOLEAN
	do
		if
			left >= other.left and right <= other.right
		then
			Result := True
		else
			Result := False
		end
	end

	is_super_range_of(other: like current): BOOLEAN
	do
		if
			left <= other.left and right >= other.right
		then
			Result := True
		else
			Result := False
		end
	end

	left_overlaps(other: like current): BOOLEAN
	do
		if
			left >= other.left and left <= other.right
		then
			Result := True
		else
			Result := False
		end
	end

	right_overlaps(other: like current): BOOLEAN
	do
		if
			(right >= other.left and right <= other.right) or (other.right >= left and other.right <= right)
		then
			Result := True
		else
			Result := False
		end
	end

	overlaps(other: like current): BOOLEAN
	do
		if
			(left_overlaps(other) or right_overlaps(other)) or (other.left >= left and other.left <= right)
		then
			Result := True
		else
			REsult := False
		end
	end

	add(other: like current): RANGE
	require
		non_empty: current.is_empty.negated and other.is_empty.negated
	local
		r: RANGE
	do
		if
			current.overlaps(other)
		then
			create r.create_range(left.min(other.left), right.max(other.right))
			Result := r
		elseif
			left = other.right + 1
		then
			create r.create_range(other.left, right)
			Result := r
		elseif
			other.left = right + 1
		then
			create r.create_range(left, other.right)
			Result := r
		else
			create r.create_range (0, 0)
			Result := r
		end
	end

	subtract(other: like current): RANGE
	require
		non_empty: current.is_empty.negated and other.is_empty.negated
		subset: other.is_sub_range_or(current)
	local
		r: RANGE
	do
		if
			current.is_equal_range(other)
		then
			create r.create_range (0, 0)
			Result := r
		elseif
			left = other.left
		then
			create r.create_range (other.right + 1, right)
			Result := r
		elseif
			right = other.right
		then
			create r.create_range (left, other.left - 1)
			Result := r
		else
			create r.create_range (0, 0)
			Result := r
		end
	end

feature
	--Constructor
	create_range(l, r: INTEGER)
	do
		left := l
		right := r
	end
end
