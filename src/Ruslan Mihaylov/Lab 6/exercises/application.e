note
	description: "exercises application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			print(reverse_iter("start") + "%N")
			print(reverse_recur("startello") + "%N")
		end

feature
	--Functions
	reverse_iter(s: STRING): STRING
	local
		res: STRING
		i, b: INTEGER
	do
		b := s.count
		res := ""
		from
			i := 1
		until
			i = b + 1
		loop
			res := res + s.at(b + 1 - i).out

			i := i + 1
		end

		Result := res
	end

	reverse_recur(s: STRING): STRING
	local
		res: STRING
		i, b: INTEGER
	do
		b := s.count
		if b <= 1 then
			Result := s
		else
			Result := reverse_recur(s.substring(2, b)) + s.at(1).out
		end
	end

end
