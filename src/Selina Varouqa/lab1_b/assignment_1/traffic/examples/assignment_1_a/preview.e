note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		do
			Zurich.add_station ("Zoo", 1800, -500)
			Zurich.append_station (3, "Zoo")
			Zurich_map.update
			Zurich_map.fit_to_window
			Zurich_map.station_view (Zurich.station ("Zoo")).highlight
			wait(3)
			Zurich_map.station_view (Zurich.station ("Zoo")).unhighlight


		end



end
