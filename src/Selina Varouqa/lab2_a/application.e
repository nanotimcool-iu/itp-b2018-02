note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE}
	my_account:BANK_ACCOUNT

	make
		do
			create my_account.create_account("GEROGE",10000)
			my_account.deposit(5000)
			my_account.withdrawal(2000)

			my_account.print_info
		end

end
