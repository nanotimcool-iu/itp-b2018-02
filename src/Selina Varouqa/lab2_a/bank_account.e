note
	description: "Summary description for {BANK_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT
create
	create_account
feature
	owner_name: STRING
	balance_amount: INTEGER
feature
	create_account(owner: STRING; balance: INTEGER)
	do
		owner_name:= owner
		balance_amount:= balance
	end
feature
	deposit(add_num:INTEGER)
	do
		balance_amount := balance_amount + add_num
	end
feature
	withdrawal(subtract_num:INTEGER)
	do
		balance_amount:= balance_amount - subtract_num
	end
feature
	print_info
	do
		io.put_string (owner_name + "%N" + balance_amount.out)
	end
end
