note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS
feature
	create_contact(name: STRING; phonenum: INTEGER;	workplace:STRING; email:STRING):CONTACT
	local
		contact :CONTACT
	do
		create contact.info_contact (name,email,workplace,phonenum)
		Result := contact
	end
feature
	edit_contact(contact: CONTACT;name: STRING; phonenum: INTEGER;	workplace:STRING; email:STRING)
	do
		contact.set_name(name)
		contact.set_phonenum(phonenum)
		contact.set_workplace(workplace)
		contact.set_email(email)
	end
feature
	add_emergency_contact (contact1: CONTACT; contact2: CONTACT)
	do
		contact1.set_emergency_contact (contact2)
	end
feature
	delete_emergency_contact (contact: CONTACT)
	do
		contact.set_emergency_contact(void)
	end
end
