note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

create
	info_contact

feature -- data
	contact_name: STRING
	contact_phonenum: INTEGER
	contact_workplace: STRING
	contact_email: STRING
	contact_emergency: detachable CONTACT

feature -- setters

	info_contact(name,email,workplace: STRING; phonenum: INTEGER)
		do
			set_name(name)
			set_phonenum(phonenum)
			set_workplace(workplace)
			set_email(email)
		end
	set_name(name: STRING)
		do
			contact_name:= name
		end
	set_phonenum (phonenum: INTEGER)
		do
			contact_phonenum:= phonenum
		end
	set_workplace (workplace: STRING)
		do
		    contact_workplace:= workplace
		end
	set_email (email: STRING)
		do
		    contact_email:= email
		end
	set_emergency_contact(emergency: detachable CONTACT)
		do
			contact_emergency:= emergency
		end
end
