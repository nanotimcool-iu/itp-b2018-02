note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY
create
	set_info

feature
	date: DATE_TIME
	owner: PERSON
	subject: STRING
	place: STRING

feature
	set_info(date1: DATE_TIME; owner1: PERSON ; subject1: STRING ; place1: STRING)
	do
		set_date(date1)
		set_owner(owner1)
		set_subject(subject1)
		set_place(place1)
	end

	set_date( date1 : DATE_TIME)
	do
		date:= date1
	end

	set_owner( owner1: PERSON)
	do
		owner:= owner1
	end

	set_subject( subject1: STRING)
	do
		subject:= subject1
	end

	set_place (place1: STRING)
	do
		place:= place1
	end
end
