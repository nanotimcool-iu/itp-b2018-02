note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON
create
	set_info

feature
	name: STRING
	phone_number: INTEGER
	work_place: STRING
	email: STRING

feature set_info(name1: STRING; phone_number1: INTEGER;	work_place1: STRING; email1: STRING)
	do
		set_name(name1)
		set_phone_number(phone_number1)
		set_work_place(work_place1)
		set_email(email1)
	end

feature set_name(name1 : STRING)
	do
		name:= name1
	end
feature set_phone_number(phone_number1 : INTEGER)
	do
		phone_number:= phone_number1
	end
feature set_work_place(work_place1 : STRING)
	do
		work_place:= work_place1
	end
feature set_email(email1 : STRING)
	do
		email:= email1
	end
end
