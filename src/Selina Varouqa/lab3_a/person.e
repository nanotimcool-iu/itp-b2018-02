note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature
	name: STRING
	number: INTEGER_64
	work_place: STRING
	email: STRING

	get_owner_name: STRING
		do
			Result := name
		end

feature {NONE} -- Initialisation

	make(a_name: STRING; a_number: INTEGER_64; a_work_place: STRING; a_email: STRING)
		do
			name := a_name
			number := a_number
			work_place := a_work_place
			email := a_email
		end


end
