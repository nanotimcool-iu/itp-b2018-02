note
	description: "Summary description for {CONNECTFOUR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONNECTFOUR
create
	make

feature
	player1, player2: PLAYER
	g_board: GAME_BOARD
	cur_player: BOOLEAN --if true than 1st player is current else 2nd
	winner: INTEGER
	finished: BOOLEAN

feature
	make(p1,p2: PLAYER)
	do
		player1 := p1
		player2 := p2
		cur_player := true
		winner := 0
		finished := false
		create g_board.make
	end

feature
	show_board
	local
		i, j: INTEGER
	do
		from
			i := 1
		until
			i > 6
		loop
			from
				j := 1
			until
				j > 7
			loop
				print(g_board.disc_board[i, j].state.out + " ")
				j := j + 1
			end
			print("%N")
			i := i + 1
		end
	end

	check_if_won(i, j: INTEGER)
	local
		k,num: INTEGER
	do
		num := 1
		if i < 4 then
			if g_board.disc_board[i,j].state = g_board.disc_board[i + 1,j].state and g_board.disc_board[i,j].state = g_board.disc_board[i + 2,j].state and g_board.disc_board[i,j].state = g_board.disc_board[i + 3,j].state then
				finished := true
				winner := g_board.disc_board[i,j].state
			end
			if j > 3 then
				if g_board.disc_board[i,j].state = g_board.disc_board[i + 1,j - 1].state and g_board.disc_board[i,j].state = g_board.disc_board[i + 2,j - 2].state and g_board.disc_board[i,j].state = g_board.disc_board[i + 3,j - 3].state then
					finished := true
					winner := g_board.disc_board[i,j].state
				end
			elseif j < 5 then
				if g_board.disc_board[i,j].state = g_board.disc_board[i + 1,j + 1].state and g_board.disc_board[i,j].state = g_board.disc_board[i + 2,j + 2].state and g_board.disc_board[i,j].state = g_board.disc_board[i + 3,j + 3].state then
					finished := true
					winner := g_board.disc_board[i,j].state
				end
			end
		end
		from
			k := j + 1
		until
			k >7
		loop
			if g_board.disc_board[i,k].state = g_board.disc_board[i,j].state and g_board.disc_board[i,k - 1].state = g_board.disc_board[i,j].state then
				num := num + 1
			else
				k := 15
			end
			k := k + 1
		end
		from
			k := j - 1
		until
			k < 1
		loop
			if g_board.disc_board[i,k].state = g_board.disc_board[i,j].state and g_board.disc_board[i,k + 1].state = g_board.disc_board[i,j].state then
				num := num + 1
			else
				k := 0
			end
			k := k - 1
		end
		if num > 3 then
			finished := true
			winner := g_board.disc_board[i,j].state
		end
		num := 1
	end

	put_disc(n: INTEGER)
	local
		p: INTEGER
	do
		if cur_player then
			p := 1
			cur_player := false
		else
			p := 2
			cur_player := true
		end
		g_board.add_disc (n, p)
		show_board
		check_if_won(g_board.last_added_position, n)
	end
end
