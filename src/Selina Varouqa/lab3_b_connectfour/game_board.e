note
	description: "Summary description for {GAME_BOARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME_BOARD
create
	make

feature
	disc_board: ARRAY2[DISC]
	last_added_position: INTEGER

feature
	make
	local
		d: DISC
	do
		create d.make (0)
		create disc_board.make_filled (d, 6, 7)
	end

feature
	add_disc(n,k: INTEGER)
	local
		i: INTEGER
		d: DISC
	do
		create d.make (k)
		from
			i := 6
		until
			i < 1
		loop
			if disc_board[i, n].state = 0 then
				disc_board[i, n] := d
				last_added_position := i
				i := 0
			end
			i := i - 1
		end
	end
end
