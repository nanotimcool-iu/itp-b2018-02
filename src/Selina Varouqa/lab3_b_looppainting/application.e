note
	description: "LOOP_PAINTING application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	numbers: ARRAYED_LIST [STRING]
	n: INTEGER

	make
		local
			currents: STRING
			i, num, to_print, printed: INTEGER
			place: BOOLEAN

		do
			print ("Enter num: ")
			io.read_integer
			n := io.last_integer
			create numbers.make (n)
			to_print := 1
			place := True
			num := 2
			currents := "1"
			numbers.extend (currents)
			from
				i := 2
			until
				i > n
			loop
				currents := ""
				if place then
					currents := currents + " "
				else
					to_print := to_print + 1
				end
				from
					printed := 0
				until
					printed >= to_print
				loop

					currents := currents + num.out + " "
					num := num + 1
					printed := printed + 1
				end
				currents := currents
				numbers.extend (currents)
				place:= not place
				i := i + 1
			end
			reverse
		end

	reverse
		local
			max_size, i: INTEGER
			s,r: STRING
			t:LIST[STRING]
			space:BOOLEAN
		do
			max_size := numbers.i_th (n).count + 1
			space := FALSE
			from
				i := 1
			until
				i > n
			loop
				t := numbers.i_th (i).split (' ')
				r := ""
				across
					t as ta
				loop
					r:= r + ta.item.out + ""
				end
				s := " "
				s.multiply ((max_size - numbers.at (i).count)*2)
				numbers.put_i_th (numbers.at (i) + s + r, i)
				print (numbers.at (i)+"%N")
				i:=i+1
			end
		end

end
