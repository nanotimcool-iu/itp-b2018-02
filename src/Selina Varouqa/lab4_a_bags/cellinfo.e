note
	description: "Summary description for {CELLINFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELLINFO
create
	init
feature
		value: CHARACTER
		number_copies: INTEGER
feature
		init(a_value: CHARACTER; a_number_copies: INTEGER)
			do
				set_value(a_value)
				set_number_copies(a_number_copies)
			end
		set_value(x: CHARACTER)
			do
				value := x
			end
		set_number_copies(x: INTEGER)
			do
				number_copies := x
			end
		get_number_copies: INTEGER
			do
				Result := number_copies
			end
		get_value: CHARACTER
			do
				Result := value
			end

end
