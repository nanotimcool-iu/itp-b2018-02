note
	description: "lab5b application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE}

	make
	local
		r1,r2,r3:RANGE
		do
			create r1.create_range (1,4)
			create r2.create_range (2, 8)
			Io.put_boolean (r1.is_sub_range_of (r2))
			Io.new_line
			Io.put_boolean (r1.is_super_range_of (r2))
			Io.new_line
			Io.put_boolean (r1.is_empty)
			Io.new_line
			Io.put_boolean (r1.is_equal_range (r2))
			Io.new_line
			Io.put_boolean (r1.left_overlaps (r2))
			Io.new_line
			Io.put_boolean (r1.right_overlaps(r2))
			Io.new_line
			r3:=r1.add(r2)
			Io.put_integer (r3.left)
			Io.new_line
			Io.put_integer (r3.right)
		end
	end
