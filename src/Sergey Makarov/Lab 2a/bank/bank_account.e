note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

inherit
	ARGUMENTS

create
	make

feature
	name: STRING
	balance: INTEGER
	deposit(value: INTEGER)
		do
			if balance  + value > 1000000 then
				print("too much money%N")

			else
					balance := balance + value
				end

		end
	withdraw(value: INTEGER)
		do
		if balance  - value < 100 then
				print("not enough money%N")

			else
					balance := balance - value
				end


		end

	make

		do
			print("%N%N%N%N%N%N")
			balance := 0
			name := "default"
			deposit(100000)
			print (balance.out+"%N")
			withdraw(100)
			print(balance.out+"%N")
			deposit(12313123)
			print(balance.out+"%N")

		end

end
