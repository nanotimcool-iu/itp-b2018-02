note
	description: "university application root class"
	date: "$Date$"
	revision: "$Revision$"
class
	COURSE

inherit
	ARGUMENTS

create
	create_class

feature
	name: STRING
	identifier: INTEGER
	scedule: STRING
	max_student: INTEGER
	create_class
			-- Run application.
		do
			init("ITP", 12, "today", 164)
		end

	init(a_name: STRING; id: INTEGER; a_scedule: STRING; max: INTEGER)
		do
			name := a_name
			identifier := id
			scedule := a_scedule
			max_student := max
		end

end
