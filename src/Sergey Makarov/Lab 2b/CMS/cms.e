note
	description: "CMS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			cont1: CONTACT
			cont2: CONTACT
			cont3: CONTACT
		do
			print("%N%N%N")
			cont1 := create_contact("a", 1, "there", "a@b.com")
			cont2 := create_contact("b", 2, "there", "a@b.com")
			cont3 := create_contact("c", 3, "there", "a@b.com")
			add_emergency_contact(cont2, cont1)
			add_emergency_contact(cont3, cont2)
			print(cont3.call_emergency.phone_number.out+"%N")
			print(cont2.name+"%N")
			edit_contact(cont2, 1, "B")
			print(cont2.name+"%N")

		end


	create_contact(name: STRING; phone_number: INTEGER; work_place: STRING; email: STRING):CONTACT
		local n: CONTACT
		do
			create
				n.constructor(name, phone_number, work_place, email)
			result:=n
		end

	edit_contact(c:CONTACT; field:INTEGER; value:STRING)
		do
			inspect field
				when 1 then
					c.set_name(value)
		 		when 2 then
					c.set_number(value.to_integer)
		 		when 3 then
					c.set_work(value)
				when 4 then
		        	c.set_email(value)
		        else
		        	print("error%N")
		    end
		end



		add_emergency_contact (c1: CONTACT; c2: CONTACT)
			do
				c1.set_call_emergency(c2)

			end

			remove_emergency_contact (c: CONTACT)
				do
					c.set_call_emergency_d
				end

end
