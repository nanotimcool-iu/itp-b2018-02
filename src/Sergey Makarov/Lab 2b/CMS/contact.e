note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

create
	constructor

feature
	name: STRING
	phone_number: INTEGER
 	work_place: STRING
 	email: STRING
 	call_emergency: CONTACT
feature
	set_name(a_name:STRING)
		do
			name := a_name
		end
	set_number(num:INTEGER)
		do
			phone_number := num
		end
	set_work(work:STRING)
		do
			work_place := work
		end
		set_email(a_email:STRING)
			do
				email := a_email
			end

 	set_call_emergency(c:CONTACT)
 		do
 			call_emergency:=c
 		end
 	set_call_emergency_d()
 		do
			call_emergency := current
 		end

	constructor(a_name: STRING; num: INTEGER; work: STRING;mail: STRING)

		do
			name:= a_name
			phone_number := num
			work_place := work
			email := mail
			set_call_emergency_d()
		end

end
