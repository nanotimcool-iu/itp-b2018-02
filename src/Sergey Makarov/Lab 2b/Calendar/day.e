note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	make

feature  -- Initialization
	entries:ARRAYED_LIST[ENTRY]
	date: INTEGER

	make(a_date:INTEGER)
			-- Initialization for `Current'.
		do
			date := a_date
			create entries.make (0)
		end
--------------------------------------	
	printable_day: STRING
	local ans:STRING
		  i:INTEGER
		do
			ans := ""
			from i := 1
					until i = entries.count+1
					loop
						ans := ans + ("%Tentry: ")
						ans := ans + (i).out
						ans := ans + ("%N%T%T"+entries.i_th (i).printable_entry)
						i := i + 1


					end
					result := ans
		end
	in_conflict (e1, e2: ENTRY): BOOLEAN
		do
			if e1.owner /= e2.owner then
				result:=false
			else
				if  e1.date < e2.date then
					if e2.date.minus(e1.date).duration >= e1.duration then
						result := false
					else result := true
					end
				else
					if e1.date.minus(e2.date).duration >= e2.duration then
										result := false
									else result := true

				end


			end
			end
			end
add_entry (a_place: STRING; a_subject: STRING; a_owner: PERSON; a_date: DATE_TIME; a_duration: DATE_TIME_DURATION)
	local e1:ENTRY
	ok:BOOLEAN
		do
			ok := true
			create e1.make (a_place,a_subject, a_owner, a_date, a_duration)
			across entries as e loop
				if in_conflict(e.item, e1) then
					ok := false
				end
			end
				if
					ok
				then
					entries.extend (e1)
				end


		end
add_entry_created (e1: ENTRY)
	local
	ok:BOOLEAN
		do
			ok := true
		--	create e1.make (a_place,a_subject, a_owner, a_date, a_duration)
			across entries as e loop
				if in_conflict(e.item, e1) then
					ok := false
				end
			end
				if
					ok
				then
					entries.extend (e1)
				end

end
end
