note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature {NONE} -- Initialization

	make(a_name: STRING; num: INTEGER; work: STRING;mail: STRING)

		do
			name:= a_name
			phone_number := num
			work_place := work
			email := mail
		end
feature
	name: STRING
	phone_number: INTEGER
 	work_place: STRING
 	email: STRING


end
