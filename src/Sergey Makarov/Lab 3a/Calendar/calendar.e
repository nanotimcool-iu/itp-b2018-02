note
	description: "Calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit
	ARGUMENTS

create
	make

feature {NONE}
	days:ARRAYED_LIST[DAY]

	add_entry (day: INTEGER; a_place: STRING; a_subject: STRING; a_owner: PERSON; a_date: DATE_TIME; a_duration: DATE_TIME_DURATION)
	local e1:ENTRY
	ok:BOOLEAN
		do
			days.i_th (day).add_entry( a_place, a_subject, a_owner, a_date, a_duration)
		end

add_entry_created (day: INTEGER; e1: ENTRY)
	local
	ok:BOOLEAN
		do
			days.i_th (day).add_entry_created (e1)


end



	 create_entry (a_place: STRING; a_subject: STRING; a_owner: PERSON; a_date: DATE_TIME; a_duration: DATE_TIME_DURATION): ENTRY
	 	do
	 		create
	 			result.make (a_place, a_subject, a_owner, a_date, a_duration)

	 	end


 	edit_subject (e: ENTRY; new_subject: STRING)
		 do
 			e.set_subject(new_subject)
 		end


 	edit_date (e: ENTRY; new_date: DATE_TIME)
 		do
 			e.set_date(new_date)
 		end


	get_owner_name (e: ENTRY): STRING
		do
			result := e.owner.name
		end


 	in_conflict (e1, e2: ENTRY): BOOLEAN
		do
			if e1.owner /= e2.owner then
				result:=false
			else
				if  e1.date < e2.date then
					if e2.date.minus(e1.date).duration >= e1.duration then
						result := false
					else result := true
					end
				else
					if e1.date.minus(e2.date).duration >= e2.duration then
										result := false
									else result := true

				end


			end
			end
			end


	printable_month: STRING
	local ans:STRING
		  i:INTEGER
		do
			ans:= ""
			from i := 1
					until i = 32
					loop
						ans := ans + ("day: ")
						ans := ans + (i).out
						ans := ans + ("%N"+days.i_th (i).printable_day)
						i := i + 1
					end
					result := ans
		end
	make
		local
			e1: ENTRY
			p1: PERSON
			e2: ENTRY
			p2: PERSON
			date1: DATE_TIME
			date2: DATE_TIME
			dur1: DATE_TIME_DURATION
			dur2: DATE_TIME_DURATION
			day:DAY
			i: INTEGER

		do
			print("-------------------------%N")
			create days.make(32)

			from i := 1
			until i = 32
			loop
				create day.make(i)
				days.extend (day)
			--days.array_put (day, i)
				i := i + 1
			end
			create p1.make ("name", 012345678,"work", "mail")
			create p2.make ("name1", 0123456789,"work1", "mail1")
			create date1.make (2018, 9, 3, 7, 1, 0)
			create date2.make (2018, 9, 3, 8, 16, 0)
			create dur1.make (0, 0, 0, 0, 15, 0)
			create dur2.make (0, 0, 0, 0, 1, 0)
			create e1.make ("place1", "subj", p1, date1, dur1)
			create e2.make ("place2", "subj1", p1, date2, dur2)
			add_entry_created(9, e1)
			add_entry_created(9, e2)
			print(printable_month)




			print("-------------------------%N")


		end


end
