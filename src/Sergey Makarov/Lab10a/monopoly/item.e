note
	description: "Summary description for {ITEM}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ITEM

create
	make

feature -- Initialization

	name: STRING
	owner: detachable PLAYER
	price, rent:INTEGER
	buy(p:PLAYER)
	do
		bought:= true
		owner:=p
	end
	bought: BOOLEAN
	players_staying: ARRAYED_LIST [PLAYER]

	make (a_string: STRING)
		do
			price := 0
			rent := 0
			bought := false
			name := a_string
			create players_staying.make (0)
		end

	prin: ARRAYED_LIST [STRING]
		local
			i: INTEGER
		do
			create result.make (0)
				result.extend("---------------------------------")
		--	result.extend ("|                               |")
			result.extend ("| " + name + spaces (30 - name.capacity) + "|")
			result.extend ("|                               |")
				--		result.extend(		"|                               |")
				--		result.extend(		"|                               |")
			result.extend ("|                               |")
			from
				i := 0
			until
				i = 7 - players_staying.count
			loop
				result.extend ("|                               |")
				i := i + 1
			end
			across
				players_staying as p
			loop
				result.extend ("| " + p.item.name+ ", "+p.item.money.out+ spaces (28 - p.item.money.out.count - p.item.name.count) + "|")
			end
			result.extend ("---------------------------------")
		end

	spaces (i: INTEGER): STRING
		do
			result := " "
			result.multiply (i)
		end

	mul (s: STRING; i: INTEGER): STRING
		do
			result := s
			result.multiply (i)
		end

end
