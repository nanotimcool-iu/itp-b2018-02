note
	description: "Summary description for {PROPERTY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROPERTY

inherit

	ITEM
		redefine
			prin
		end

create
	a_make

feature -- Initialization



	a_make (a: STRING; y, x: INTEGER)
			-- Initialization for `Current'.
		do
			name := a
			price := y
			rent := x
			bought := false
			create players_staying.make (0)
		end

	prin: ARRAYED_LIST [STRING]
		local
			i: INTEGER
		do
			create result.make (0)
			result.extend ("---------------------------------")
				--		result.extend("|                               |")
			result.extend ("| " + name + spaces (30 - name.capacity) + "|")
			result.extend ("| rent: " + rent.out + spaces (24 - rent.out.count) + "|")
			result.extend ("| price: " + price.out + spaces (23 - price.out.count) + "|")
				--	result.extend(	"|                               |")
				--	result.extend(	"|                               |")
			from
				i := 0
			until
				i = 6 - players_staying.count
			loop
				result.extend ("|                               |")
				i := i + 1
			end
			across
				players_staying as p
			loop
				result.extend ("| " + p.item.name+ ", "+p.item.money.out+ spaces (28 - p.item.money.out.count - p.item.name.count) + "|")
			end
			if attached owner as o and bought then

				result.extend("| owned by: "+ o.name+ spaces(20-o.name.count)+"|")
			else
				result.extend ("|                               |")
				end
			result.extend ("---------------------------------")
		end

end
