note
	description: "Lab11b application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			test:  LINKED_SET [STRING]
		do
			io.new_line
			test := anagrams ("decimal")
			across
				test as p
			loop
				print (p.item) io.new_line
			end
			print (is_leap (10000))
		end








	is_leap (year: NATURAL): BOOLEAN
		do
			result := year \\ 400 = 0 or (year \\ 4 = 0 and year \\ 100 /= 0)
		end

	anagrams (word: STRING):  LINKED_SET [STRING]
		local
			tmp, res:  LINKED_SET [STRING]
			i, j: INTEGER
			s: STRING
		do
			create result.make
			result.compare_objects
			if word.count = 2 then
				result.extend(word [1].out + word [2].out)
				result.extend (word [2].out + word [1].out)
			else
				if word.count = 1 then
					result.extend (word)
				else
					create res.make
					res.compare_objects
					create tmp.make
					tmp.compare_objects
					tmp.merge(anagrams (word.substring (2, word.count).twin))
					across
						tmp as t
					loop


						res.extend (word [1].out + t.item)
						res.extend (t.item + word [1].out.out)
					end

					create tmp.make
					tmp.compare_objects
					tmp.merge (anagrams (word.substring (1, word.count - 1).twin))
					across
						tmp as t
					loop
						res.extend (word [word.count].out + t.item)
						res.extend (t.item + word [word.count].out)
					end
						--asdf

					from
						i := 2
					until
						i > word.count
					loop
						create tmp.make
						tmp.compare_objects
						tmp.merge (anagrams (word.substring (1, i-1).twin + word.substring (i + 1, word.count).twin))
						across
							tmp as t
						loop
							res.extend (word [i].out + t.item)
							res.extend (t.item + word [i].out)
						end
						i := i + 1
					end

					result.append (res)




				end
			end
		end

end
