note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
		local
			i : INTEGER	-- Modify the map.
		do

			Zurich.add_station ("Simferopol", 5000, -900)
			Zurich.connect_station (3, "Simferopol")
			Zurich_map.update
			Zurich_map.animate()
			from
				i := 0
			until
				i = 1
			loop

				Zurich_map.station_view (Zurich.station("Simferopol")).highlight
				wait(1)
				Zurich_map.station_view (Zurich.station ("Simferopol")).unhighlight
				wait(1)
			end


		end

end
