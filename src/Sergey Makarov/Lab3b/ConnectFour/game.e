﻿note
	description: "ConnectFour application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

inherit
	EXECUTION_ENVIRONMENT
	--ATTENTION
	--for correct execution on Windows OS all "system("clear")" should be replaced with "system("cls")"
create
	make

feature {NONE} -- Initialization

	game_over: BOOLEAN

	board: ARRAY2 [CHARACTER]

	left: ARRAY [INTEGER]

	n, m: INTEGER

	print_board
		local
			s1, s2: STRING
			i, j: INTEGER
		do
			s1 := "-"
			s2 := "|"
			s1.multiply (board.width*3 + 2)
			print (s1 + "%N")
			from
				i := 1
			until
				i = board.height + 1
			loop
				print (s2)
				from
					j := 1
				until
					j = board.width + 1
				loop
					print("[")
					print (board.item (i, j))
					print ("]")
					j := j + 1
				end
				i := i + 1
				print (s2)
				io.put_new_line
			end
			print (s1 + "%N")
		end

	check_down (i, j: INTEGER): INTEGER
		do
			if i < n AND board [i, j] = board [i + 1, j] then
				result := 1 + check_down (i + 1, j)
			else
				result := 0
			end
		end

	check_d1 (i, j: INTEGER): INTEGER
		do
			if j > 1 and i > 1 AND board [i, j] = board [i - 1, j - 1] then
				result := 1 + check_d1 (i - 1, j - 1)
			else
				result := 0
			end
		end

	check_d2 (i, j: INTEGER): INTEGER
		do
			if j > 1 and i < n AND board [i, j] = board [i + 1, j - 1] then
				result := 1 + check_d2 (i + 1, j - 1)
			else
				result := 0
			end
		end

	check_d3 (i, j: INTEGER): INTEGER
		do
			if j < m and i > 1 AND board [i, j] = board [i - 1, j + 1] then
				result := 1 + check_d3 (i - 1, j + 1)
			else
				result := 0
			end
		end

	check_d4 (i, j: INTEGER): INTEGER
		do
			if j < m and i < n AND board [i, j] = board [i + 1, j + 1] then
				result := 1 + check_d4 (i + 1, j + 1)
			else
				result := 0
			end
		end

	check_right (i, j: INTEGER): INTEGER
		do
			if j < m AND board [i, j] = board [i, j + 1] then
				result := 1 + check_right (i, j + 1)
			else
				result := 0
			end
		end

	check_left (i, j: INTEGER): INTEGER
		do
			if j > 1 AND board [i, j] = board [i, j - 1] then
				result := 1 + check_left (i, j - 1)
			else
				result := 0
			end
		end

	put_disk (i, j, num: INTEGER): BOOLEAN
		do
			if num = 1 then
				board [i, j] := 'o'
			else
				board [i, j] := '+'
			end
			if check_down (i, j) > 2 OR (check_left (i, j) + check_right (i, j)) > 2 OR (check_d1 (i, j) + check_d4 (i, j)) > 2 or (check_d2 (i, j) + check_d3 (i, j)) > 2 then
				result := false
			else
				result := true
			end
		end

	make
		local
			ok: BOOLEAN
			current_column, i, j, k, current_player: INTEGER
		do
			system ("clear")
			io.putstring ("enter dimensions%N")
			io.input.read_integer
			n := io.last_integer
			io.input.read_integer
			m := io.lastint
			create board.make_filled (' ', n, m)
			create left.make_filled (n, 1, m)
			if not (m * n = 8 or m * n > 9) then
				print ("wrong dimensions, nobody wins")
			else
				from
					k := 0
				until
					k = n * m or game_over
				loop
					system ("clear")
					if not ok then
						print ("wrong input%N")
					end
					print_board
					io.putstring ("%Nplayer# " + ((k) \\ (2) + 1).out + " your column%N")
					io.read_integer
					current_column := io.last_integer
					if current_column > m OR current_column < 1 then
						system ("clear")
						ok := false
						k := k - 1
					else
						j := current_column
						i := left.item (current_column)
						print ((i).out + " " + j.out)
						if i > 0 then
							if put_disk (i, j, k \\ 2 + 1) then
								left [j] := i - 1
								ok := true
							else
								system ("clear")
								print_board
								print ("%N the winner is player #" + (k \\ 2 + 1).out)
								game_over := true
							end
						else
							ok := false
							k := k - 1
						end
					end
					k := k + 1
				end
				if (not game_over) then
					system ("clear")
					print_board
					print ("nobody wins")
				end
			end
		end

end
