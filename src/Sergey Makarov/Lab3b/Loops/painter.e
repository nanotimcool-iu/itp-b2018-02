note
	description: "LoopPainting application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	PAINTER

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	reverse (s: STRING): STRING
		local
			s1: LIST [STRING]
			ans: STRING
		do
			ans := ""
			s1 := s.split (' ')
			across
				s1 as str
			loop
				ans := str.item + " " + ans
			end
			result := ans
		end

	spaces (i: INTEGER): STRING
		local
			s: STRING
		do
			s := " "
			s.multiply (i)
			result := s
		end

	make
		local
			j: INTEGER
			i: INTEGER
			n: INTEGER
			c: INTEGER
			s: STRING
			s1: STRING
			len: INTEGER
			str: ARRAYED_LIST [STRING]
		do
			io.put_new_line
			create str.make (0)
			io.input.read_integer
			n := io.last_integer
			c := 1
			s := ""
			from
				i := 0
			until
				(n - 1) / 2 < i
			loop
				s := ""
				i := i + 1
				from
					j := 0
				until
					j = i
				loop
					j := j + 1
					s := s + (c.out + " ")
					c := c + 1
				end
				str.extend (s)
				if n.integer_remainder (2) = 0 OR ((n - 1) / 2 >= i) then
					s := " "
					from
						j := 0
					until
						j = i
					loop
						j := j + 1
						s := s + (c.out + " ")
						c := c + 1
					end
					str.extend (s)
				end
			end
			len := str.i_th (n).count * 2 + 1
			across
				str as st
			loop
				io.putstring (st.item + spaces (len - (2 * st.item.count)) + reverse (st.item) + "%N")
				i := i + 1
			end
		end

end
