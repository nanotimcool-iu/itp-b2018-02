note
	description: "MULTISET application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local b1, b2: MY_BAG
		do
			create b1.make
			create b2.make
			b1.add ('1', 1)
			print( b1.max)
			io.put_new_line
			print( b1.min)

		end

end
