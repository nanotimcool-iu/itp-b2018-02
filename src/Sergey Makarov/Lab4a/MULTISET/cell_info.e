note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	make

feature  -- Initialization

	value:CHARACTER
	number_copies: INTEGER

	make(val:CHARACTER; num:INTEGER)
		do
			value := val
			number_copies := num
		end
	add(count:INTEGER)
	do
		number_copies := number_copies + count
		if number_copies  < 0 then
			number_copies := 0
		end
	end
	invariant
	pos_num:number_copies >=0

end
