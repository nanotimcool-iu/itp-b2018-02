note
	description: "Summary description for {MY_BAG}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_BAG



create
	make

feature  -- Initialization
 bag: ARRAY[CELL_INFO]
 add (val: CHARACTER; n: INTEGER)
 do
	bag[val.code].add (n)
 end
 remove (val: CHARACTER; n: INTEGER)
 do
	bag[val.code].add (-n)
 end
 min: CHARACTER
 local i:INTEGER
 	do
 		from
 			i := 255
 		until
 			bag[i].number_copies /= 0
 		loop
 			i := i - 1
 		end
 		result := bag[i].value
 	end

 max: CHARACTER
 local i:INTEGER
 do
 	from
 			i := 0
 		until
 			bag[i].number_copies /= 0
 		loop
 			i := i + 1
 		end
 		result := bag[i].value
 end
 is_equal_bag (ba: MY_BAG): BOOLEAN
 local i:INTEGER
 b:BOOLEAN
 do
 	b := true
	from
		i := 0
	until
		i = 256
	loop
		if bag[i].value = ba.bag[i].value and bag[i].number_copies = ba.bag[i].number_copies then
			b := true
		else b := false
	end
	result := b
	end
end
	make

		local
		c: CELL_INFO
		i:INTEGER
		do
			create c.make ('0', 0)
			create bag.make_filled (c, 0, 255)
			i := 0
			across bag as cell loop

				create c.make (i.to_character_8, 0)
				bag[i] := c
				i := i + 1
			end
		end

end
