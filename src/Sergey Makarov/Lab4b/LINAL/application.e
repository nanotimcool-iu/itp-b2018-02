note
	description: "LINAL application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	EXECUTION_ENVIRONMENT

create
	make

feature {NONE} -- Initialization

	make
		local
			m1, m2: ARRAY2 [INTEGER]
			v1, v2:ARRAY[INTEGER]
			op: MATRIX_OPER
			op1:VECTOR_OPER
			t: ARRAY [INTEGER]
		do
			system ("clear")
			create op.make
			create op1.make
			create m1.make_filled (2, 3, 3)
			create m2.make_filled (5, 3, 10)
			m1 [1, 1] := 1
			m1 [1, 2] := 0
			m1 [1, 3] := 0
			m1 [2, 1] := 0
			m1 [2, 2] := 1
			m1 [2, 3] := 0
			m1 [3, 1] := 0
			m1 [3, 2] := 0
			m1 [3, 3] := 1

			v1 := <<1, 2, 3>>
			v2 := <<3, 2, 1>>
			print(op1.cross_product(v1, v2)[1].out+" ")
			print(op1.cross_product(v1, v2)[2].out+" ")
			print(op1.cross_product(v1, v2)[3].out+"%N")
			print(op1.dot_product (v1, v2).out+"%N")
			print(op1.triangle_area (v1, v2).out+"%N")
			print(op1.length (v1).out+"%N")
			print (op.determinant (m1).out+"%N")
			across
				op.prod (m1, m2) as m
			loop
				print (m.item.out + " ")
			end
		end

end
