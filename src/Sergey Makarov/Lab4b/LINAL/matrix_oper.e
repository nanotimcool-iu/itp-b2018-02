note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER

create
	make

feature

	make
		do
		end

	determinant (input: ARRAY2 [INTEGER]): INTEGER
		require
			condition: input.width = input.height
		local
			i: INTEGER
		do
			result := 0
			if (input.height = 1) then
				result := input [1, 1]
			else
				from
					i := 1
				until
					i = input.width + 1
				loop
					result := result + input [1, i] * ((-1).power (i + 1)).ceiling * determinant (min (input, 1, i))
					i := i + 1
				end
			end
		end

	min (input: ARRAY2 [INTEGER]; x, y: INTEGER): ARRAY2 [INTEGER]
		local
			ini, inj, i, j: INTEGER
		do
			create result.make_filled (0, input.height - 1, input.height - 1)
			from
				i := 1;
				ini := 1
			until
				i = input.height + 1
			loop
				if i /= x then
					from
						j := 1;
						inj := 1
					until
						j = input.height + 1
					loop
						if j /= y then
							result [ini, inj] := input [i, j]
							inj := inj + 1
						end
						j := j + 1
					end
					ini := ini + 1;
				end
				i := i + 1
			end
		end

	add (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			same_dimensions: m1.height = m2.height and m1.width = m2.width
		local
			i, j: INTEGER
		do
			create result.make_filled (0, m2.height, m2.width)
			from
				i := 1
			until
				i = m1.height + 1
			loop
				from
					j := 1
				until
					j = m1.width + 1
				loop
					result [i, j] := m1 [i, j] + m2 [i, j]
					j := j + 1
				end
				i := i + 1
			end
		end

	minus (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			same_dimensions: m1.height = m2.height and m1.width = m2.width
		local
			i, j: INTEGER
		do
			create result.make_filled (0, m2.height, m2.width)
			from
				i := 1
			until
				i = m1.height + 1
			loop
				from
					j := 1
				until
					j = m1.width + 1
				loop
					result [i, j] := m1 [i, j] - m2 [i, j]
					j := j + 1
				end
				i := i + 1
			end
		end

	prod (m1, m2: ARRAY2 [INTEGER]): ARRAY2 [INTEGER]
		require
			correct_dimensions_for_multiplication: m1.width = m2.height
		local
			a, b, c: INTEGER
		do
			create result.make_filled (0, m1.height, m2.width)
			from
				a := 1
			until
				a = m1.width + 1
			loop
				from
					b := 1
				until
					b = m2.width + 1
				loop
					from
						c := 1
					until
						c = m1.width + 1
					loop
						result [a, b] := result [a, b] + m1 [a, c] * m2 [c, b]
						c := c + 1
					end
					b := b + 1
				end
				a := a + 1
			end
		end

end
