note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER

create
	make

feature

	cross_product (v1, v2: ARRAY [INTEGER]): ARRAY [INTEGER]
		require
			correct_count_of_dimensions: v1.count = v2.count and v1.count = 3

		local op:MATRIX_OPER

		do
			create result.make_filled (0, 1, 3)
			result[1] := v1[2]*v2[3]-v1[3]*v2[2]
			result[2] := -v1[1]*v2[3]+v1[3]*v2[1]
			result[3] := v1[1]*v2[2]-v1[2]*v2[1]

		end

	length (v: ARRAY [INTEGER]): INTEGER
		local
			b:DOUBLE_MATH
			i: INTEGER
		do
			create b
			from
				i := 1
			until
				i = v.count+1
			loop
				result := result + v [i] * v [i]
				i := i + 1
			end
			result := b.sqrt (result).ceiling
		end

	dot_product (v1, v2: ARRAY [INTEGER]): INTEGER
		require
			correct_count_of_dimensions: v1.count = v2.count
		local
			i: INTEGER
		do
			from
				i := 1
			until
				i = v1.count + 1
			loop
				result := result + v1 [i] * v2 [i]
				i := i + 1
			end
		end

	triangle_area (v1, v2: ARRAY [INTEGER]): INTEGER
		do
			result := length (cross_product (v1, v2)) // 2
		end

	make
		do
		end

end
