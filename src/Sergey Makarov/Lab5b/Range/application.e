note
	description: "Range application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local r, r1:RANGE
			-- Run application.
		do
			create r.make (5, 4)
			create r1.make (6, 10)
		--	print(r.overlaps (r1))
			io.put_new_line
			print(r1.add (r).right)
			io.put_new_line
			print(r1.add (r).left)
		end

end
