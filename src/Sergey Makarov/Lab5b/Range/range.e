note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	make

feature

	make (l, r: INTEGER)
		do
			left := l
			right := r
		end

	left: INTEGER

	right: INTEGER

	is_equal_range (other: like Current): BOOLEAN
		do
			result := left = other.left and right = other.right
		end

	is_empty: BOOLEAN
		do
			result := left > right
		end

	is_sub_range_of (other: like Current): BOOLEAN
		do
			result := other.left <= left and other.right >= right or left > right
		end

	is_super_range_of (other: like Current): BOOLEAN
		do
			result := other.left >= left and other.right <= right or other.left > other.right
		end

	right_overlaps (other: like Current): BOOLEAN
		do
			result := other.left <= right and other.right >= left
		end

	left_overlaps (other: like Current): BOOLEAN
		do
			result := left <= other.right and right >= other.left
		end

	overlaps (other: like Current): BOOLEAN
		do
			result := right_overlaps (other) or left_overlaps (other)
		end

	add (other: like Current): RANGE
		require
			overlaps: other.is_empty or is_empty or overlaps (other) or else left - 1 = other.right or right + 1 = other.left
		do
			create result.make (left.min (other.left), right.max (other.right))
			if(other.is_empty) then
				create result.make (left, right)
				end
			if is_empty then
				create result.make(other.left, other.right)

			end
		end

	subtract (other: like current): RANGE
		require
			n: is_empty OR other.is_empty or left = other.left or else right = other.right or else (not is_super_range_of (other))
		do
			if is_sub_range_of (other) then
				create result.make (0, -1)
			else

				if right >= other.left and right < other.right then
					create result.make (left, other.left - 1)
					io.put_boolean (true)
				else
					if left <= other.right and left > other.left then
						create result.make (other.right+1, right)
					else
						create result.make(left, right)
					end
				end
			end
		end

end
