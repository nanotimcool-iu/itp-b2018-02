note
	description: "Lab6 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			a: ARRAY [INTEGER]
			i: INTEGER
			-- Run application.
		do

			print (reverse_recursive("123456789")+"%N")
			print (reverse_iterative("123456789")+"%N")
			print (lcs("1a23123123", "aaa1a2a3a")+"%N")
			a := <<4, 3, 2, 1, 0, -1>>
			a := merge_sort (a)
			from
				i := 1
			until
				i > a.count
			loop
				print (a [i].out + "  ")
				i := i + 1
			end
		end

	reverse_recursive (s: STRING): STRING
		do
			if (s.capacity = 1) then
				result := s
			else
				result := reverse_recursive (s.substring (2, s.capacity)) + s [1].out
			end
		end

	merge_sort (list: ARRAY [INTEGER]): ARRAY [INTEGER]
		local
			m: INTEGER
			left, right: ARRAY [INTEGER]
		do
			if list.count = 1 then
				result := list
			else
				m := (list.count // 2)
				left := list.subarray (list.lower, m + list.lower - 1)
				left := merge_sort (left)
				right := list.subarray (m + list.lower, list.count + list.lower - 1)
				right := merge_sort (right)
				result := merge (left, right)
			end
		end

	lcs (x, y: STRING): STRING
		local
			s1, s2: STRING
		do
			if (x.capacity = 0 or y.capacity = 0) then
				result := ""
			else
				if x [x.capacity] = y [y.capacity] then
					result := lcs (x.substring (1, x.capacity - 1), y.substring (1, y.capacity - 1)) + x [x.capacity].out
				else
					s1 := lcs (x.substring (1, x.capacity - 1), y)
					s2 := lcs (x, y.substring (1, y.capacity - 1))
					if (s1.capacity > s2.capacity) then
						result := s1
					else
						result := s2
					end
				end
			end
		end

	merge (a, b: ARRAY [INTEGER]): ARRAY [INTEGER]
		local
			inda, indb, ind: INTEGER
		do
			inda := a.lower
			indb := b.lower
			create result.make_filled (0, 1, a.count + b.count)
			from
				ind := result.lower
			until
				ind > a.count + b.count + result.lower - 1
			loop
				if inda < a.count + a.lower and (indb > b.count + b.lower - 1 or else a [inda] < b [indb]) then
					result [ind] := a [inda]
					ind := ind + 1
					inda := inda + 1
				else
					result [ind] := b [indb]
					ind := ind + 1
					indb := indb + 1
				end
			end
		end

	reverse_iterative (s: STRING): STRING
		local
			i: INTEGER
		do
				--	print(s.capacity)
			result := ""
			from
				i := 0
			until
				i = s.capacity
			loop
					--	print(result.capacity)
				result := result + s [s.capacity - i].out
				i := i + 1
			end
		end

end
