note
	description: "Lab6b application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	power_set (s: LINKED_SET [INTEGER]): LINKED_SET [LINKED_SET [INTEGER]]
		local
			s1: LINKED_SET [INTEGER]
			x: INTEGER
		do
			create Result.make
			if not s.is_empty then
				s1 := s.twin
				x := s [1]
				s1.prune (x)
				Result.append (power_set (s1))
				across
					power_set (s1) as s2
				loop
					s2.item.put (x)
					Result.put (s2.item)
				end
			else
				Result.put (create {LINKED_SET [INTEGER]}.make)
			end
		end

	hanoi (n: INTEGER; src, to, other: CHARACTER)
		do
			if n = 1 then
				print (src.out + " ->" + to.out + "%N")
			else
				hanoi (n - 1, src, other, to)
				hanoi (1, src, to, other)
				hanoi (n - 1, other, to, src)
			end
		end

	r, c: INTEGER

	grid: ARRAY [ARRAY [INTEGER]]

	find_path (r1, c1: INTEGER): ARRAY [STRING]
		local
			a1, a2: ARRAY [STRING]
		do
			if r1 > r or else c1 > c or else grid [r1].item (c1) = 1 then
				Result := <<>>
			elseif r1 = r and c1 = c then
				Result := <<"finish">>
			else
				a1 := find_path (r1 + 1, c1)
				a2 := find_path (r1, c1 + 1)
				if a2.count /= 0 and (a1.count = 0 or a2.count < a1.count) then
					a2.force ("right", a2.count + 1)
					Result := a2
				elseif a1.count /= 0 and (a2.count = 0 or a1.count < a2.count) then
					a1.force ("down", a1.count + 1)
					Result := a1
				else
					Result := <<>>
				end
			end
		end

	print_result (i: INTEGER; a: ARRAY [STRING])
		do
			if i /= a.count then
				print_result (i + 1, a)
			end
			print (a [i] + "%N")
		end

	make
		local
			set: LINKED_SET [INTEGER]
		do
			create set.make
			set.put (1)
			set.put (2)
			set.put (3)
			set.put (4)
			across
				power_set (set) as cur
			loop
				if cur.item.is_empty then
					print ("{}%N")
				else
					print ('{')
					across
						cur.item as elem
					loop
						print (elem.item.out + ", ")
					end
					print ("%B%B}%N")
				end
			end
			hanoi (3, 'A', 'B', 'C')
			r := 5
			c := 5
			grid := <<<<0, 0, 0, 0, 0>>, <<1, 0, 0, 0, 0>>, <<0, 1, 1, 0, 1>>, <<0, 0, 0, 0, 1>>, <<0, 0, 0, 0, 0>>>>
			print_result (1, find_path (1, 1))
		end

end
