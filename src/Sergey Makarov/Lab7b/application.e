note
	description: "Lab7b application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION


inherit

	EXECUTION_ENVIRONMENT

create
	make



feature {NONE}

	input_file: PLAIN_TEXT_FILE

	dictionary: ARRAY [STRING]

	count: INTEGER

	used: ARRAY [INTEGER]

	lifes: INTEGER

	current_s: STRING

	s: STRING
	turns: INTEGER

	random: RANDOM


	feature
	make
		local
			i: INTEGER
			s1: STRING
			l_time: TIME
			l_seed: INTEGER
		do
			turns := 0
			create used.make_filled (0, 1, 256)
			system ("clear")
			create dictionary.make_empty
			io.read_integer
			count := io.last_integer
			create input_file.make_open_read ("input.txt")
				-- create output_file.make_open_write ("output.txt")

			from
				input_file.readline
			until
				input_file.exhausted
			loop
					--output_file.put (input_file.last_character)
				s1 := input_file.laststring.twin
				if s1.count >= count then
					dictionary.force (s1, dictionary.count + 1)
				end
				input_file.readline
			end
			input_file.close
			io.read_integer
			lifes := io.last_integer
			current_s := "_"
			create l_time.make_now
			l_seed := l_time.hour
			l_seed := l_seed * 60 + l_time.minute
			l_seed := l_seed * 60 + l_time.second
			l_seed := l_seed * 1000 + l_time.milli_second
			create random.set_seed (l_seed)
			print (1 + random.item \\ dictionary.count)
			s := dictionary [1 + random.item \\ dictionary.count]
			s.to_lower
			current_s.multiply (s.capacity)
			system ("clear")
			from
				i := 1
			until
				i = s.capacity
			loop
				if s [i] = ' ' then
					current_s [i] := ' '
				end
				if s [i] = '-' then
					current_s [i] := '-'
				end
				i := i + 1
			end
			print (current_s + "%N")
			gamover := false
			game
		end

	gamover: BOOLEAN

	game
		local
			input: STRING
			i: INTEGER
		do
			print ("")
			if lifes = 0____0___0_0 then
				gamover := true
				print ("game over")
			elseif (current_s ~ s) then
				print ("winner: " + (((turns - 1) \\ count) + 1).out)
				gamover := true
			else
				print ("lifes: " + lifes.out + "%N")
				print ("player: " + ((turns \\ count) + 1).out + "%N")
				print_used
				print ("your input: ")
				io.read_line
				input := io.last_string
				if input.count /= 1 or else not (input[1] >='a' and input[1] <= 'z') then
					system("clear")
					print("wrong input%N")
					game
					else
				if (s.index_of (input [1], 1)) > 0 and used [input [1].code] = 0 then
					used [input [1].code] := 1
					from
						i := 1
					until
						i = s.capacity + 1
					loop
						if s [i] = input [1] then
							current_s [i] := input [1]
						end
						i := i + 1
					end
				else if used [input [1].code] = 0 then
					used [input [1].code] := 1
					end

					lifes := lifes - 1
				end
			end
			if (not gamover) then
				turns := turns + 1
				system ("clear")
				print (current_s + "%N")
				game
			end
		end

end
print_used
local i:INTEGER
do
	print("used: ")
	from
		i:= 1
	until
		i > used.capacity
	loop
		if(used[i] > 0) then
			print(i.to_character_8.out+" ")
			end
			i := i + 1
	end
	io.put_new_line

end
	invariant a: count > 0 and	s.capacity > 0 and lifes > 0 and lifes < 26
end
