note
	description: "Lab8a application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			t1, t2, t3, t4, t5: BINARY_TREE_NOT_LIKE_IN_LIBRARY [INTEGER]
		do
			create t1.make (0)
			create t2.make (1)
			create t3.make (2)
			create t4.make (3)
			create t5.make (4)
			t2.add (t3)
			t5.add (t4)
			t5.add (t2)
			t1.add (t5)
			print (t1.height)
			print (t2.height)
			print (t3.height)
			print (t4.height)
			print (t5.height)
		end

end
