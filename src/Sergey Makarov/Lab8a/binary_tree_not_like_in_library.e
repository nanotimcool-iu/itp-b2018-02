note
	description: "Summary description for {BINARY_TREE_NOT_LIKE_IN_LIBRARY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BINARY_TREE_NOT_LIKE_IN_LIBRARY [T]

create
	make

feature

	make (a_value: T)
		do
			value := a_value
			height := 1
		end

	value: T

	left, right: detachable BINARY_TREE_NOT_LIKE_IN_LIBRARY [T]

	height: INTEGER

	add (t: BINARY_TREE_NOT_LIKE_IN_LIBRARY [T])	
		do
			if left = Void then
				left := t.twin
				if attached right as r then
					height := t.height.max (r.height) + 1
				else
					height := t.height + 1
				end
			elseif right = Void then
				right := t.twin
				if attached left as l then
					height := t.height.max (l.height) + 1
				else
					height := t.height + 1
				end
			else
				if attached left as l and attached right as r then
					if l.height < r.height then
						l.add (t)
					elseif r.height < l.height then
						r.add (t)
					end
					height := l.height.max (r.height) + 1
				end
			end
		end

end
