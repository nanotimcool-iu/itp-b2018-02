note
	description: "lab8 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		local
			huffman: HUFFMAN
			str: STRING
		do
			create huffman.make ("test string for huffman tree")
			str := huffman.encode ("string for test")
			print(str + "%N")
			print(huffman.decode (str) + "%N")
		end

end
