note
	description: "Summary description for {HUFFMAN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN

create
	make

feature

	frequencies: ARRAY [INTEGER]

	tree: LINKED_SET [NODE]

	string: STRING

	emap: HASH_TABLE [STRING, CHARACTER]

	make (s: STRING)
		local
			m1, m2: detachable NODE
		do
			string := s
			create frequencies.make_filled (0, 0, 256)
			create tree.make
			create emap.make (0)
			across
				string as c
			loop
				frequencies [c.item.as_lower.code] := frequencies [c.item.as_lower.code] + 1
			end
			across
				frequencies as f
			loop
				if f.item /= 0 then
					tree.put (create {NODE}.make_leaf ((f.target_index).to_character_8, f.item))
				end
			end
			print (frequencies)
			from
			until
				tree.count = 1
			loop
				m1 := Void
				m2 := Void
				across
					tree as e
				loop
					if m1 = Void or else e.item.frequency < m1.frequency then
						m1 := e.item
					end
				end
				across
					tree as e
				loop
					if (m2 = Void or else e.item.frequency < m2.frequency) and e.item /= m1 then
						m2 := e.item
					end
				end
				if m1 /= Void and m2 /= Void then
					tree.prune (m1)
					tree.prune (m2)
					tree.put (create {NODE}.make (m1, m2))
				end
			end
			dfs (tree.first, "")
		end

	decode (s: STRING): STRING
		require
			format: across s as char all char.item = '0' or char.item = '1' end
		local
			cur: NODE
		do
			cur := tree.first
			Result := ""
			across
				s as char
			loop
				if attached cur then
					if char.item = '0' then
						cur := cur.left
					elseif char.item = '1' then
						cur := cur.right
					end
				end
				if attached cur then
					if cur.is_leaf then
						Result := Result + cur.value.out
						cur := tree.first
					end
				end
			end
		end

	encode (s: STRING): STRING
		require
			existing_symbols: across s as char all emap.has (char.item) end
		do
			Result := ""
			across
				s as char
			loop
				if attached emap.at (char.item) as encoded then
					Result := Result + encoded
				end
			end
		end

	dfs (vertex: NODE; path: STRING)
		do
			if vertex.is_leaf then
				emap.put (path, vertex.value)
				print ("character %""+vertex.value.out + "%": " + path + "%N")
			elseif attached vertex.left as l and attached vertex.right as r then
				dfs (l, path + "0")
				dfs (r, path + "1")
			end
		end

end
