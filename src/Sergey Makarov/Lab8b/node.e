note
	description: "Summary description for {NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NODE

create
	make, make_leaf

feature

	is_leaf: BOOLEAN

	left, right: detachable NODE

	value: CHARACTER

	frequency: INTEGER

	make (a_left, a_right: NODE)
		do
			left := a_left
			right := a_right
			is_leaf := false
			value := '_'
			frequency := a_left.frequency + a_right.frequency
		end

	make_leaf (a_value: CHARACTER; a_frequency: INTEGER)
		do
			value := a_value
			is_leaf := true
			frequency := a_frequency
		end

end
