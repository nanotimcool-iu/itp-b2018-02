note
	description: "Lab9a application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
make
local c:COMBINED_CIPHER
vc:VIGENERE_CIPHER
cc:SPIRAL_CIPHER
matr:ARRAY2[INTEGER]
n, i, j, k:INTEGER

			-- Run application.
		do

			create c.make
			create vc.make
			create cc.make
			io.new_line
			io.new_line
			n:=4
			j:=1
			k := 4
			i := 1

			print(c.decrypt( "LAGHYH QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF   FAZ2/TGBZKFIREE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIULZIE KFYGEL//:RLH DXE Z", "YQLRFYNTTNSDSYCN"))
			io.new_line
			print(c.decrypt (c.encrypt ("ABCDEFGHIJKLMNOPQRSTUVWXYZ", "AKSDJFVASKJ"), "AKSDJFVASKJ"))
		end

end
