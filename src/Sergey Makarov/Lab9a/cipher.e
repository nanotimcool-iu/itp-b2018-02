note
	description: "Summary description for {CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	CIPHER
	  feature
		encrypt(s, key:STRING):STRING
		deferred

		end
		decrypt(s, key:STRING):STRING
		deferred

		end

end
