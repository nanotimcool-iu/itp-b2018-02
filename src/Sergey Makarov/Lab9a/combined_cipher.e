note
	description: "Summary description for {COMBINED_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMBINED_CIPHER

inherit

	CIPHER

create
	make

feature  -- Initialization
vc:VIGENERE_CIPHER
cc:SPIRAL_CIPHER
	make
			-- Initialization for `Current'.
		do
			create cc.make
			create vc.make
		end

	encrypt (s, key: STRING): STRING
		do
			result:= cc.encrypt (vc.encrypt (s, key), key)
		end

	decrypt (s, key: STRING): STRING
		do
			result:= vc.decrypt (cc.decrypt (s, key), key)
		end

end
