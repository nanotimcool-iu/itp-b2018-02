note
	description: "Summary description for {SPIRAL_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SPIRAL_CIPHER

inherit

	CIPHER

	DOUBLE_MATH

create
	make

feature -- Initialization

	make
			-- Initialization for `Current'.
		do
		end

	decrypt (s, key: STRING): STRING
		local
			matr: ARRAY2 [CHARACTER]
			i, j, ind, n, k: INTEGER
			s1:STRING
		do

			create matr.make_filled (' ', sqrt (s.capacity).ceiling, sqrt (s.capacity).ceiling)
		    s1 := s
			n := matr.height
			j := 1
			k := n
			i := 1
			ind := 1
			from
			until
				k = 0
			loop
				from
					i := n - k + 1
				until
					i > k or ind > s1.capacity
				loop
					matr [i, k] := s1[ind]
					i := i + 1
					ind :=ind + 1
				end
				from
					i := k - 1
				until
					i < j or ind > s1.capacity
				loop
					 matr [k, i]:= s1[ind]
					 ind :=ind + 1
					i := i - 1
				end
				from
					i := k - 1
				until
					i < j or ind > s1.capacity
				loop
					 matr [i, j]:= s1[ind]
					 ind :=ind + 1
					i := i - 1
				end
				k := k - 1
				from
					i := j + 1
				until
					i > k or ind > s1.capacity
				loop
					 matr [j, i]:= s1[ind]
					 ind :=ind + 1
					i := i + 1
				end
				j := j + 1
			end
result:=""




			from
				j := 1
			until
				j > matr.height
			loop
				from
					i := 1
				until
					i > matr.height
				loop
					result := result + matr [j, i].out
					i := i + 1
				end
				j := j + 1
			end
			--result := reverse(result)
		end



	reverse(s:STRING):STRING
	local i:INTEGER
	do
		result:=""
		from
			i:= 1
		until
			i > s.capacity
		loop
			result:= s[i].out + result
			i := i + 1
		end
	end
	encrypt (s, key: STRING): STRING
		local
			matr: ARRAY2 [CHARACTER]
			f, n, k, ind, i, j: INTEGER
		do
			ind := 1
			result := ""
			create matr.make_filled (' ', sqrt (s.capacity).ceiling, sqrt (s.capacity).ceiling)
			from
				j := 1
			until
				j > matr.height or ind > s.capacity
			loop
				from
					i := 1
				until
					i > matr.height or ind > s.capacity
				loop
					matr [j, i] := s [ind]
					i := i + 1
					ind := ind + 1
				end
				j := j + 1
			end
			n := matr.height
			j := 1
			k := n
			i := 1
			from
			until
				k = 0
			loop
				from
					i := n - k + 1
				until
					i > k
				loop
					result := result + matr [i, k].out
					i := i + 1
				end
				from
					i := k - 1
				until
					i < j
				loop
					result := result + matr [k, i].out
					i := i - 1
				end
				from
					i := k - 1
				until
					i < j
				loop
					result := result + matr [i, j].out
					i := i - 1
				end
				k := k - 1
				from
					i := j + 1
				until
					i > k
				loop
					result := result + matr [j, i].out
					i := i + 1
				end
				j := j + 1
			end
		end

end
