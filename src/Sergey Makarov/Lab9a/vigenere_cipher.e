note
	description: "Summary description for {VIGENERE_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHER

inherit

	CIPHER

create
	make

feature -- Initialization

	make
			-- Initialization for `Current'.
		do
		end

	encrypt (s, key: STRING): STRING
		local
			i, ind: INTEGER
		do
			create result.make_from_string (s)
			from
				i := 0
				ind:=-1
			until
				i >= s.capacity
			loop
				if (s [i+1].lower.code >= ('a').code and s [i+1].lower.code <= ('z').code) then
					--print(key [i \\ key.capacity + 1])
					ind:= ind + 1
					result [i+1] := ((s [i+1].as_upper.code + key [ind \\ key.capacity +1].as_upper.code - 2*('A').code) \\ 26 + ('A').code).to_character_8
				end
				i := i + 1
			end
		end

	decrypt (s, key: STRING): STRING
		local
			i, ind: INTEGER
		do
			create result.make_from_string (s)
			from
				i := 0
				ind:=-1
			until
				i >= s.capacity
			loop
				if (s [i+1].lower.code >= ('a').code and s [i+1].lower.code <= ('z').code) then
					--print(key [i \\ key.capacity + 1])
					ind:= ind + 1
					result [i+1] := ((s [i+1].as_upper.code - key [ind \\ key.capacity +1].as_upper.code+26) \\ 26 + ('A').code).to_character_8
				end
				i := i + 1
			end
		end
end
