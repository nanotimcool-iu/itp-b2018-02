note
	description: "STACK application root class"
	author: "Sergey Makarov"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			stack:MY_STACK[STRING]
			bounded_stack:MY_BOUNDED_STACK[INTEGER]
		do
			create stack.make
			create bounded_stack.make_limited (2)
			io.put_boolean (stack.is_empty)
			print("%N")
			stack.push ("lskjdf;lawks.ghn;vlqek.wj,gmv;qlkew")
			stack.push ("some text")
			print(stack.item + "%N")
			stack.remove
			print(stack.item)
	
		end

end
