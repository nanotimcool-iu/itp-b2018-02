note
	description: "Summary description for {MY_BOUNDED_STACK}."
	author: "Sergey Makarov"
	date: "$Date$"
	revision: "$Revision$"

class
	MY_BOUNDED_STACK[G]

inherit
	MY_STACK [G]
		redefine
			push,
			make
		end

create
	make,
	make_limited

feature {NONE}

	upper_bound:INTEGER

	make
	do
		create stack.make(0)
		upper_bound := stack.capacity
	end

	make_limited(bound:INTEGER)
	require
		positive_bound: bound > 0
	do
		create stack.make(0)
		upper_bound := bound
	end

feature
	push(obj:G)
	do
		if stack.count < upper_bound then
			stack.extend (obj)
		else
			print("Can't add more elements%N")
		end
	end


end
