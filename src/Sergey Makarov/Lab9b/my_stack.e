note
	description: "Summary description for {MY_STACK}."
	author: "Sergey Makarov"
	date: "$Date$"
	revision: "$Revision$"

class
	MY_STACK[G]

create
	make

feature  -- Initialization

stack:ARRAYED_LIST[G]

	make
		do
			create stack.make (0)
		end
	push(obj:G)
	do
		stack.extend (obj)
	end

	remove
	do
		if not stack.is_empty then
			stack.remove_right
		else
			print("error%N")
		end

	end

	item:G
	do
		result := stack.i_th (stack.count)
	end

	is_empty:BOOLEAN
	do
		Result := stack.is_empty
	end

end
