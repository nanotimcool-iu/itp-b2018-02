note
	description: "lab11a application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS_32

create
	make

feature

	make
		local
			q: EVENT_QUEUE
			e: EVENT
			i: INTEGER
		do
			create q.make
			from
				i := 1
			until
				i = 100000
			loop
				q.add (create {EVENT}.make (i, "abc"))
				i := i + 1
			end
			print (q.item.out)
			io.put_new_line
		--	q.wipe
			print(q.is_empty)
			q.wipe
			io.put_new_line
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			q.add (create {EVENT}.make (123, "whether it overlaps"))
			e := q.extract
			io.print(q.is_empty)
		end

end
