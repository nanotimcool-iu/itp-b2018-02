note
	description: "Summary description for {EVENT}."
	author: "Sergey Makarov"
	date: "$Date$"
	revision: "$Revision$"

class
	EVENT

inherit
	COMPARABLE rename is_equal as equals

	redefine out
	select equals  end
	ANY redefine out end


create
	make


feature
	time:INTEGER
	name:STRING

	make(a_time: INTEGER; s: STRING)
	require
		not s.is_empty
	do
		time := a_time;
		name := s
	end

	get_time : INTEGER
	do
		Result := time
	end

	is_less alias "<" (other: like Current): BOOLEAN
		do
			Result := Current.get_time > other.get_time
		end

	out: STRING
	do
		Result := "name: " + name + ", time: " + time.out + "%N"
	end
end
