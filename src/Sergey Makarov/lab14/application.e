note
	description: "BNF application root class"
	date: "$Date$"
	author: "Sergey Makarov"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature

	expression_left: STRING

	make
		do
			io.readline
			expression_left := io.last_string.twin
			print (parse_I and parse_dont and parse_verb and parse_names and parse_quant and expression_left.count = 0)
		end

	parse_I: BOOLEAN
		do
			if expression_left.starts_with ("I") then
				result := true
				expression_left := expression_left.substring (2, expression_left.count)
			else
				result := false
			end
		end

	parse_dont: BOOLEAN
		do
			result := true
			if expression_left.starts_with (" do not") then
				expression_left := expression_left.substring (8, expression_left.count)
			end
		end

	parse_verb: BOOLEAN
		do
			if expression_left.starts_with (" like") or expression_left.starts_with (" hate") then
				result := true
				expression_left := expression_left.substring (6, expression_left.count)
			else
				result := false
			end
		end

	parse_names: BOOLEAN
		do
			result := parse_name
			from
			until
				not (expression_left.starts_with (" and") or expression_left.starts_with (","))
			loop
				if expression_left.starts_with (" and") then
					expression_left := expression_left.substring (5, expression_left.count)
				else
					expression_left := expression_left.substring (2, expression_left.count)
				end
				result := result and parse_name
			end
		end

	parse_name: BOOLEAN
		do
			if expression_left.starts_with (" books") or expression_left.starts_with (" shoes") then
				result := true
				expression_left := expression_left.substring (7, expression_left.count)
			elseif expression_left.starts_with (" programming") then
				result := true
				expression_left := expression_left.substring (13, expression_left.count)
			elseif expression_left.starts_with (" football") then
				result := true
				expression_left := expression_left.substring (10, expression_left.count)
			else
				result := false
			end
		end

	parse_quant: BOOLEAN
		do
			result := true
			if expression_left.starts_with (" a lot") then
				expression_left := expression_left.substring (7, expression_left.count)
			elseif expression_left.starts_with (" a little") then
				expression_left := expression_left.substring (10, expression_left.count)
			end
		end

end
