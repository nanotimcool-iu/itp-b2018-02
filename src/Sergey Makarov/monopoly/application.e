note
	description: "monopoly application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	EXECUTION_ENVIRONMENT

create
	make

feature {NONE} -- Initialization

	players_in_game: ARRAYED_LIST [PLAYER]

	n: INTEGER

	board: BOARD

	p: PLAYER

	new_random: INTEGER
		do
			random_sequence.forth
			Result := random_sequence.item
		end

	random_sequence: RANDOM

	make
		local
			go: BOOLEAN
			i, tmp, pos, s1, s2, turn: INTEGER
			l_time: TIME
			l_seed: INTEGER
		do
			create l_time.make_now
			l_seed := l_time.hour
			l_seed := l_seed * 60 + l_time.minute
			l_seed := l_seed * 60 + l_time.second
			l_seed := l_seed * 1000 + l_time.milli_second
			create random_sequence.set_seed (l_seed)
			io.putstring ("How many players will be? (2-6)%N")
			create players_in_game.make (0)
			io.read_integer
			n := io.last_integer
			create p.make ("")
			from
				i := 1
			until
				i > n
			loop
				create p.make ("player " + i.out)
				players_in_game.extend (p)
				i := i + 1
			end
			create board.make
			from
				i := 1
			until
				i > n
			loop
				board.items [1].players_staying.extend (players_in_game [i])
				i := i + 1
			end
			from
				i := 1
			until
				i > 100 or n = 1
			loop
				system ("clear")
				print (board.out)
				turn := turn \\ n
				turn := turn + 1
				i := i + 1
				s1 := new_random \\ 6 + 1
				s2 := new_random \\ 6 + 1
				go := true
				pos := players_in_game [turn].pos
				if s1 /= s2 and board.items [pos].name = "IN JAIL" and players_in_game [turn].days_in_jail > 0 then
					io.read_line
					if players_in_game [turn].days_in_jail >= 1 and players_in_game [turn].money >= 50 then
						system ("clear")
				print (board.out)
				io.readline
						print (players_in_game[turn].name + ",  want to pay to get out from Jail? y/n ")
						io.read_character
						if io.last_character = 'y' then
							go := true
							players_in_game [turn].buy (50)
							players_in_game [i].set_jail (-1)
							io.readline
						else
							players_in_game [i].set_jail (players_in_game [i].days_in_jail - 1)
							io.readline
						end
					else
						players_in_game [i].buy (50)
						if (players_in_game [turn].money >= 0) then
							go := true
							players_in_game [i].set_jail (-1)
							io.readline
						else
							go := false
							players_in_game.prune (players_in_game [turn])
							turn := turn - 1
							n := n - 1
							io.readline
						end
					end
				end
				if go then
					io.read_line
					board.items [pos].players_staying.prune (players_in_game [turn])
					pos := pos + s1 + s2
					if pos > 20 then
						pos := (pos - 1) \\ 20 + 1
						players_in_game [turn].buy (-200)
						io.readline
					end

						board.items [pos].players_staying.extend (players_in_game [turn])
					system ("clear")
				print (board.out)
				--print("lsdkfjasdljgmvoasidfx")
					if board.items [pos].name = "GO TO JAIL" then
						players_in_game [turn].set_jail (3)
						players_in_game [turn].set_pos (6)
						system ("clear")
						print (board.out)
						print ("GO TO JAIL%N")
						io.readline
					elseif board.items [pos].name = "INCOME TAX" then
						print ("income tax, pay 10%% (" + (players_in_game [turn].money // 10).out + "P)%N")
						players_in_game [turn].buy (players_in_game [turn].money // 10)
						io.readline
					elseif board.items [pos].name = "CHANCE" then
						tmp := new_random // 500 - 300
						print ("chance, you earn (" + tmp.out + "P)%N")
						players_in_game [turn].buy (- tmp)
						io.readline
						if players_in_game [turn].money < 0 then
							players_in_game.prune (players_in_game [turn])
							turn := turn - 1
							n := n - 1
						end
					elseif not board.items [pos].bought then
						if players_in_game [turn].money >= board.items [pos].price then
							print (players_in_game[turn].name + ", buy this property? y/n ")
							io.read_character
							if io.last_character.as_lower = 'y' then
								players_in_game [turn].buy (board.items [pos].price)
								board.items[pos].buy (players_in_game[turn])
								io.readline
							end
						end
						else
						players_in_game [turn].buy (board.items [pos].rent)
						if players_in_game [turn].money < 0 then
							players_in_game.prune (players_in_game [turn])
							turn := turn - 1
							n := n - 1
							io.readline
						end

					end
				end
			end
			system ("clear")
			io.putstring (board.out)
		end

end
