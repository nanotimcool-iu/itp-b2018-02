﻿note
	description: "Summary description for {BOARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOARD

inherit

	ANY
		redefine
			out end

create
	make

feature -- Initialization

	Items: ARRAYED_LIST [ITEM]

	make
		local
			item: ITEM
		do
			create Items.make (0)
			create item.make ("START")
			items.extend (item)
			create {PROPERTY} item.a_make ("Christ the Redeemer", 60, 2)
			items.extend (item)
			create {PROPERTY} item.a_make ("Luang Pho Tor", 60, 4)
			items.extend (item)
			create item.make ("income tax")
			items.extend (item)
			create {PROPERTY} item.a_make ("Alyosha monument", 80, 4)
			items.extend (item)
			create item.make ("IN JAIL")
			items.extend (item)
			create {PROPERTY} item.a_make ("Tokyo Wan Kannon", 100, 6)
			items.extend (item)
			create {PROPERTY} item.a_make ("Luangpho Yai", 120, 8)
			items.extend (item)
			create item.make ("CHANCE")
			items.extend (item)
			create {PROPERTY} item.a_make ("The Motherland", 160, 12)
			items.extend (item)
			create item.make ("FREE PARKING")
			items.extend (item)
			create {PROPERTY} item.a_make ("Awaji Kannon", 220, 18)
			items.extend (item)
			create item.make ("CHANCE")
			items.extend (item)
			create {PROPERTY} item.a_make ("Rodina-Mat Zovyot!", 260, 22)
			items.extend (item)
			create {PROPERTY} item.a_make ("Great Buddha of Thailand", 280, 22)

			items.extend (item)
			create item.make ("GO TO JAIL")
			items.extend (item) --------------------------
			create {PROPERTY} item.a_make ("Laykyun Setkyar", 320, 28)
			items.extend (item)
			create {PROPERTY} item.a_make ("Spring Temple Buddha", 360, 35)
			items.extend (item)
			create item.make ("CHANCE")
			items.extend (item)
			create {PROPERTY} item.a_make ("Statue of Unity", 400, 50)
			items.extend (item)
		end

	out: STRING
		local
			i: INTEGER
			s: STRING
		do
			s := " "
			s.multiply (137)
			result := ""
			from
				i := 1
			until
				i = 13
			loop
				result := result + items [11].prin [i] + " " + items [12].prin [i] + " " + items [13].prin [i] + " " + items [14].prin [i] + " " + items [15].prin [i] + " " + items [16].prin [i] + "%N"
				i := i + 1
			end
			from
				i := 1
			until
				i = 13
			loop
				result := result + items [10].prin [i] + s + items [17].prin [i] + "%N"
				i := i + 1
			end
			from
				i := 1
			until
				i = 13
			loop
				result := result + items [9].prin [i] + s + items [18].prin [i] + "%N"
				i := i + 1
			end
			from
				i := 1
			until
				i = 13
			loop
				result := result + items [8].prin [i] + s + items [19].prin [i] + "%N"
				i := i + 1
			end
			from
				i := 1
			until
				i = 13
			loop
				result := result + items [7].prin [i] + s + items [20].prin [i] + "%N"
				i := i + 1
			end
			from
				i := 1
			until
				i = 13
			loop
				result := result + items [6].prin [i] + " " + items [5].prin [i] + " " + items [4].prin [i] + " " + items [3].prin [i] + " " + items [2].prin [i] + " " + items [1].prin [i] + "%N"
				i := i + 1
			end
		end

end
