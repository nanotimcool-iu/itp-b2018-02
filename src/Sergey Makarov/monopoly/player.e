note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create
	make

feature -- Initialization

	name: STRING

	money: INTEGER

	days_in_jail: INTEGER

	properties: ARRAYED_LIST [PROPERTY]

	pos:INTEGER

	make (n: STRING)
			-- Initialization for `Current'.
		do
			create properties.make (0)
			name := n
			money := 1500
			pos:=1
			days_in_jail:= -1
		end

	set_jail (i: INTEGER)
		do
			days_in_jail := i
		end
	set_pos(i:INTEGER)
	do
		pos:=i
	end
	buy (value: INTEGER)
		do
			money := money - value
		end

end
