note
	description: "Summary description for {COURSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE
create
	create_class
feature
	name:STRING
	identifier:INTEGER
	number_of_student:INTEGER
	max_student :INTEGER = 250
feature
	create_class(a_name:STRING; an_identifier:INTEGER; a_number_of_student:INTEGER)
		do
			name:=a_name
			identifier:=an_identifier
			number_of_student:= a_number_of_student;
			if 	number_of_student > max_student or number_of_student < 3
				then
					io.put_string ("Invalid number of student")
				else
					io.put_string ("Coruse name " + name + "%N")
					io.put_string ("Identifier " )
					io.put_integer (identifier)
					io.put_new_line
					io.put_string ("Number of students ")
					io.put_integer (number_of_student)
					io.put_new_line
				end
		end
end
