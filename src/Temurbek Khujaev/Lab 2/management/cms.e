note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS
feature

	create_contact(name:STRING;phone_number:INTEGER;work_place:STRING;email:STRING):CONTACT
	do
		c.set_name(name)
		c.set_phone_number(phone_number)
		c.set_email(email)
		c.set_work_place(work_place)

	end
	c:CONTACT

end
