note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT
feature
	name : STRING = "default"
	phone_number : INTEGER = 123
	work_place : STRING ="def"
	email : STRING = "def@def"
	call_emergency : CONTACT
feature
	set_name(x:STRING)
	do
		name:=x
	end
	set_phone_number(x:INTEGER)
	do
		phone_number := x
	end
	set_email(y:STRING)
	do
		email := y
	end
	set_call_emergency(x:CONTACT)
	do
		call_emergency := x
	end
end
