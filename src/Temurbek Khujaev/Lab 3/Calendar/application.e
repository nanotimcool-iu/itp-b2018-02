note
	description: "Calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization


	make
			-- Run application.
		do

			create e.create_entry("AB","BC")
		end
feature
 	e:ENTRY

end
