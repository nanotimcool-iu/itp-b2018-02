note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR
create
	make_calendar
feature
	days:LIST[DAY]
	make_calendar(dys:LIST[DAY])
	do
		days:=dys
	end
	add_entry(e:ENTRY;day:INTEGER)
	do
		-- do something with lists
	end
	edit_subject(e:ENTRY;new_subject:STRING)
	do
		--e.subject := new_subject
	end
	edit_date(e:ENTRY;new_date:TIME)
	do
		--e.date := new_date
	end
	get_owner_name(e:ENTRY):STRING
	do
		Result:=e.owner.name
	end
	--in_conflict_day(day:INTEGER):LIST[ENTRY]
	--do
	--	-- again do something with lists
	--end
	printable_month:STRING
	do
		result:="default"
	end


end
