note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY
create
	make_day

feature
	date:INTEGER
	events:LIST[ENTRY]
	make_day(dt:INTEGER;evnts:LIST[ENTRY])
	do
		date:=dt
		events:=evnts
	end

end
