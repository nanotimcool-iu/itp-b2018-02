note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY
create
	create_entry
feature
	date:TIME
	owner:PERSON
	subject:STRING
	place:STRING

feature
	create_entry(sbjct:STRING;plc:STRING)
	do
		create date.make_now
		create owner.make_person
		subject := sbjct
		place := plc
	end

end
