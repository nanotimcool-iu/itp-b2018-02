class
	BUSINESS_CARD

create
	fill_in

feature {NONE} -- Initialization

	fill_in
			-- Fill in the card and print it.
		do
			io.put_string ("Please, enter your name%N")
			io.read_line
			set_name (io.last_string)

			io.put_string ("%NWhat are you?%N")
			io.read_line
			set_job (io.last_string)

			io.put_string ("%NWhat's your age?%N")
			io.read_integer
			set_age (io.last_integer)

			print_card
		end

feature -- Access

	name: STRING
			-- Owner's name.

	job: STRING
			-- Owner's job.

	age: INTEGER
			-- Owner's age.

feature -- Setting

	set_name (a_name: STRING)
			-- Set `name' to `a_name'.
		require
			name_exists: a_name /= Void
		do
			name := a_name.twin
		end

	set_job (a_job: STRING)
			-- Set `job' to `a_job'.
		require
			job_exists: a_job /= Void
		do
			job := a_job.twin
		end

	set_age (a_age: INTEGER)
			-- Set `age' to `a_age'.
		require
			age_non_negative: a_age >= 0
		do
			age := a_age
		end

feature -- Output

	print_card
			-- procedure used to print a business card using data stored in the object
		local
			result_string: STRING
		do
			result_string := line (Width)
                           + "%N|" + name + spaces(Width - name.count - 2)
                           + "|%N|" + job + spaces(Width - job.count - 2)
                           + "|%N|" + age_info + spaces(Width - age_info.count - 2)
                           + "|%N" + line(Width) + "%N"

			io.put_string (result_string)
		end

	age_info: STRING
			-- Text representation of age on the card.
		do
			Result := age.out + " years old"
		end

	Width: INTEGER = 50
			-- Width of the card (in characters), excluding borders.

	spaces (n: INTEGER): STRING
			-- Returns string of n spaces
		do
			Result := " "
			Result.multiply (n)
		end

	line (n: INTEGER): STRING
			-- Horizontal line on length `n'.
		do
			Result := "-"
			Result.multiply (n)
		end

end
