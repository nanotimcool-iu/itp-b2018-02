class
	APPLICATION

create
	execute

feature {NONE} -- Initialization

	execute
			-- Run application.
		do
			Io.put_string ("Name: Tymur Lysenko")
			Io.put_new_line
			Io.putstring ("Age: 19")
			Io.put_new_line
			Io.put_string ("Mother tongue: pink")
			Io.put_new_line
			Io.putstring ("Native language: Russian, Ukrainian")
			Io.put_new_line
			Io.putstring ("Has a cat: False")
			Io.put_new_line
		end

end
