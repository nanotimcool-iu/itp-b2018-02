note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		local
			zoo_view: STATION_VIEW
			i: INTEGER
		do
			zurich.add_station ("Zoo", 188, -500)
			zurich.connect_station (4, "Zoo")
			zurich_map.update
			zurich_map.fit_to_window

			zoo_view := zurich_map.station_view (zurich.station ("Zoo"))

			from i := 0
			until i = 3
			loop
				zoo_view.highlight
				wait (1)
				zoo_view.unhighlight
				wait (1)

				i := i + 1
			end
		end

end
