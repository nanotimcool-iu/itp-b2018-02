note
	description: "CoffeeShop application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			shop: COFFEE_SHOP
		do
			create shop.make
			shop.add_product(create {ESPRESSO}.make_espresso(5, 10))
			shop.add_product(create {CAPPUCCINO}.make_cappuccino(10, 15))
			shop.add_product(create {CAKE}.make_cake(7, 12))

			shop.print_menu
			print("Summary profit from each product: " + shop.profit.out + "%N%N")

			shop.set_price(8, 2)
			shop.set_price_public(15, 3)

			shop.print_menu
			print("Summary profit from each product: " + shop.profit.out + "%N")
		end

end
