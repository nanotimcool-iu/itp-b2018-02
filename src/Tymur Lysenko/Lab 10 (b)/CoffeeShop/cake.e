note
	description: "Summary description for {CAKE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAKE

inherit
	PRODUCT

create
	make_cake

feature {NONE} -- Initialization

	make_cake(nominal_cost, sell_cost: REAL)
			-- Initialization
		require
			nominal_cost_positive: nominal_cost > 0
			markup: sell_cost > nominal_cost
		do
			make(nominal_cost, sell_cost, "A sweet tasty treat")
		end

end
