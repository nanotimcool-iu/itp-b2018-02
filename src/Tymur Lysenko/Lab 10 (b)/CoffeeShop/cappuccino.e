note
	description: "Summary description for {CAPPUCCINO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAPPUCCINO

inherit
	COFFEE

create
	make_cappuccino

feature {NONE} -- Initialization

	make_cappuccino(nominal_cost, sell_cost: REAL)
			-- Initialization
		require
			nominal_cost_positive: nominal_cost > 0
			markup: sell_cost > nominal_cost
		do
			make(nominal_cost, sell_cost, "An espresso-based coffee drink, traditionally prepared with double espresso and steamed milk foam")
		end

end
