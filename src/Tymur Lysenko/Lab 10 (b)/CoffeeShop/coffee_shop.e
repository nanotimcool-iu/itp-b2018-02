note
	description: "Summary description for {COFFEE_SHOP}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COFFEE_SHOP

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for 'Current'.
		do
			create product_list.make(3)
		end

feature -- Opne commands

	print_menu
		do
			across product_list as prod
			loop
				print(prod.item.get_string_repr)
			end
		end

	set_price_public(new_price: REAL; prod_ind: INTEGER)
		require
			valid_ind: product_list.valid_index(prod_ind)
		do
			product_list[prod_ind].price_public := new_price
		end

	set_price(new_price: REAL; prod_ind: INTEGER)
		require
			valid_ind: product_list.valid_index(prod_ind)
		do
			product_list[prod_ind].price := new_price
		end

	add_product(prod: PRODUCT)
		require
			not_in_list: not product_list.has(prod)
		do
			product_list.extend(prod)
		ensure
			contains_prod: product_list.has(prod)
		end

feature -- Open queries

	profit: REAL
		do
			result := 0
			across product_list as prod
			loop
				result := result + prod.item.profit
			end
		end

feature -- Open attributes

	product_list: ARRAYED_LIST[PRODUCT]

end
