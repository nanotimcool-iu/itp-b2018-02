note
	description: "Summary description for {ESPRESSO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ESPRESSO

inherit
	COFFEE

create
	make_espresso

feature {NONE} -- Initialization

	make_espresso(nominal_cost, sell_cost: REAL)
			-- Initialization
		require
			nominal_cost_positive: nominal_cost > 0
			markup: sell_cost > nominal_cost
		do
			make(nominal_cost, sell_cost, "A small strong coffee")
		end

end
