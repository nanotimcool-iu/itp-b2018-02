note
	description: "Summary description for {PRODUCT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PRODUCT

feature {PRODUCT} -- Open features

	make(nominal_cost, sell_cost: REAL; desc: STRING)
			-- Initialization
		require
			nominal_cost_positive: nominal_cost > 0
			markup: sell_cost > nominal_cost
		do
			price := nominal_cost
			price_public := sell_cost
			description := desc
		end

feature -- Open queries

	get_string_repr: STRING
		do
			result := "Desc: " + description +
				"%NPrice: " + price_public.out +
				"%N"
		end

feature -- Open attributes

	price_public: REAL assign set_price_public
	description: STRING

feature {PRODUCT, COFFEE_SHOP} -- Restricted commands

	set_price_public(new_price: REAL)
		do
			price_public := new_price
		end

	set_price(new_price: REAL)
		do
			price := new_price
		end

feature {PRODUCT, COFFEE_SHOP} -- Restricted attributes

	price: REAL assign set_price

feature {PRODUCT, COFFEE_SHOP} -- Restricted queries

	profit: REAL
		do
			result := price_public - price
		ensure
			result > 0
		end

invariant
	markup: price < price_public
end
