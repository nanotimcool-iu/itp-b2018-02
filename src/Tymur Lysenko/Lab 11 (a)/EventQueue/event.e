note
	description: "Summary description for {EVENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EVENT

inherit
	COMPARABLE
	redefine
		is_less
	end

create
	make

feature {NONE} -- Initialization

	make(nm: STRING; time: DATE_TIME)
			-- Initialization for 'Current'.
		require
			non_empty_name: not nm.is_empty
		do
			name := nm
			time_tag := time
		end

feature -- Open queires

	is_less alias "<" (other: like Current): BOOLEAN
		do
			result := time_tag > other.time_tag
		end

feature -- Open attributes

	name: STRING
	time_tag: DATE_TIME

end
