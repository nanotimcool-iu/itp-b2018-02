note
	description: "Summary description for {EVENT_QUEUE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EVENT_QUEUE

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for 'Current'.
		do
			create events.make(100000)
		end

feature -- Open commands

	push (new_event: EVENT)
		do
			if not has_event(new_event) then
				if events.capacity < events.count + 1 then
					events.grow(events.count + 1)
				end

				events.put(new_event)
			end
		ensure
			event_added: has_event(new_event)
			not_empty_queue: not is_empty
		end

feature -- Open queries

	pop: EVENT
		require
			queue_not_empty: not is_empty
		do
			result := events.item
			events.remove
		ensure
			event_removed: not events.has(result)
		end

	has_event(ev: EVENT): BOOLEAN
		local
			event_list: ARRAYED_LIST[EVENT]
			i: INTEGER
		do
			event_list := events.linear_representation

			from
				i := 1
				result := false
			until
				i > event_list.count
				or else
				result
			loop
				result := event_list[i].time_tag ~ ev.time_tag

				i := i + 1
			end
		end

	is_empty: BOOLEAN
		do
			result := events.is_empty
		end

	size: INTEGER
		do
			result := events.count
		end

feature {EQA_TEST_SET} -- Closed attributes

	events: HEAP_PRIORITY_QUEUE[EVENT]

invariant
	is_empty implies (size = 0)
end
