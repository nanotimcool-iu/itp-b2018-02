note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	EVENT_QUEUE_TEST

inherit
	EQA_TEST_SET

feature -- Test routines

	push_100_000_events
		local
			i: INTEGER
			eq: EVENT_QUEUE
		do
			create eq.make

			from
				i := 1
			until
				i > 100000
			loop
				eq.events.put(
					create {EVENT}.make(
						"Event" + i.out,
						create {DATE_TIME}.make_now
					)
				)

				i := i + 1
			end

			assert("Can store 100000+ elements", eq.size >= 100000)
		end

	event_merge
			-- 100% functional path coverage
		local
			eq: EVENT_QUEUE
			event1, event2: EVENT
			event_1_name: STRING
		do
			create eq.make
			event_1_name := "Event 1"
			create event1.make(
				event_1_name,
				create {DATE_TIME}.make(
					2018, 11, 1, 10, 10, 0
				)
			)
			create event2.make(
				"Event 2",
				create {DATE_TIME}.make(
					2018, 11, 1, 10, 10, 0
				)
			)

			eq.push(event1)
			eq.push(event2)

			assert("Events with same time tag merged", eq.pop.name = event_1_name)
			assert("Size is zero", eq.size = 0)
			assert("Queue empty after popping merged events", eq.is_empty)
		end

	event_add_extract
			-- 100% functional instruction coverage
		local
			eq: EVENT_QUEUE
			early_event, late_event: EVENT
		do
			create eq.make
			create early_event.make(
				"Event 1",
				create {DATE_TIME}.make(
					2018, 11, 1, 10, 10, 0
				)
			)
			create late_event.make(
				"Event 2",
				create {DATE_TIME}.make(
					2018, 11, 2, 10, 10, 0
				)
			)

			eq.push(late_event)
			eq.push(early_event)

			assert("Early event extracted", eq.pop = early_event)
			assert("Size = 1", eq.size = 1)
			assert("Queue not empty", not eq.is_empty)
		end

	branch_and_conditional_coverage
		-- 100% functional branch and conditional coverage
		local
			eq: EVENT_QUEUE
			ev: EVENT
		do
			create eq.make

			create ev.make(
				"Event 1",
				create {DATE_TIME}.make(
					2018, 11, 1, 10, 10, 0
				)
			)
			assert("has_event = false", not eq.has_event(ev))

			eq.push(ev)
			assert("has_event = true", eq.has_event(ev))
		end

end
