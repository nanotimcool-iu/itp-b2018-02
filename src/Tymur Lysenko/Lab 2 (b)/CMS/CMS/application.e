note
	description: "CMS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS; CMS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			c1: CONTACT;
			c2: CONTACT;
			c3: CONTACT
		do
			c1 := create_contact("Innopolis University", 78432039253, "Innopolis University", "university@innopolis.ru", void)
      c2 := create_contact("Innopolis concierge service", 88002222287, "Innopolis", "helpme@innopolis.ru", void)
      c3 := create_contact("Innopolis city hall", 88432122724, "Innopolis", " city@innopolis.ru", void)

      add_emergency_contact(c1, c2)
      add_emergency_contact(c2, c3)

      edit_contact (c2, "Innopolis concierge service", 88003333387, "Innopolis", "concierge_service@innopolis.ru")

			remove_emergency_contact(c1)
			remove_emergency_contact(c2)
		end

end
