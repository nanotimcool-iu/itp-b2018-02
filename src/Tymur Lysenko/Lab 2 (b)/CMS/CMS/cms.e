note
	description: "Summary description for {CMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

feature -- Open commands

	create_contact(contact_name: STRING;
			 					 contact_phone_number: INTEGER_64;
								 contact_work_place: STRING;
			 					 contact_email: STRING;
			 					 contact_call_emergency: detachable CONTACT)
			 					 : CONTACT
			-- add new_contact to the CMS
  	do
			create result.make(contact_name,
												 contact_phone_number,
												 contact_work_place,
												 contact_email,
												 contact_call_emergency)
  	end

  edit_contact(c: CONTACT; contact_name: STRING;
			 					           contact_phone_number: INTEGER_64;
								           contact_work_place: STRING;
			 					           contact_email: STRING)
			-- update existing contact with the new data
		do
			c.name := contact_name
			c.phone_number := contact_phone_number
			c.work_place := contact_work_place
			c.email := contact_email
		end

	add_emergency_contact(c1: CONTACT; c2: CONTACT)
		do
			c1.call_emergency := c2
		end

	remove_emergency_contact(c: CONTACT)
		do
		  c.call_emergency := void
		end

end
