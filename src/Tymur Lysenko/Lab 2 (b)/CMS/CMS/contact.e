note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

create make

feature {NONE} -- Constructors

	make(contact_name: STRING;
			 contact_phone_number: INTEGER_64;
			 contact_work_place: STRING;
			 contact_email: STRING;
			 contact_call_emergency: detachable CONTACT)
  		-- constructor for all class attributes
  	do
  		name := contact_name
  		phone_number := contact_phone_number
  		work_place := contact_work_place
    	email := contact_email
    	call_emergency := contact_call_emergency
  	end

feature -- Attributes

	name: STRING assign set_name
		attribute
		  create result.make_empty
		end

	set_name(new_name: STRING)
		do
		  name := new_name
		end

	phone_number: INTEGER_64 assign set_phone_number

	set_phone_number(new_phone_number: INTEGER_64)
		do
		  phone_number := new_phone_number
		end

	work_place: STRING assign set_work_place
		attribute
		  create result.make_empty
		end

	set_work_place(new_work_place: STRING)
		do
		  work_place := new_work_place
		end

	email: STRING assign set_email
		attribute
		  create result.make_empty
		end

	set_email(new_email: STRING)
		do
		  email := new_email
		end

	call_emergency: detachable CONTACT assign set_call_emergency

	set_call_emergency(new_call_emergency: detachable CONTACT)
		do
		  call_emergency := new_call_emergency
		end

end
