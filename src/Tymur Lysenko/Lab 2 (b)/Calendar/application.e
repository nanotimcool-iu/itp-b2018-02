note
	description: "Calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS; CALENDAR

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
		  ITP_lab_monday,
		  ITP_lab_wednesday,
		  strange_club: ENTRY;
		  start_date, end_date: DATE_TIME;
		  tymur_lysenko_owner: PERSON
		do
			create tymur_lysenko_owner.make ("Tymur Lysenko", 47161516, "Student @ Innopolis University", "t.lyisenko@innopolis.university")
			create start_date.make(2018, 9, 3, 17, 20, 00)
			create end_date.make(2018, 9, 3, 18, 50, 00)

			ITP_lab_monday := create_entry(start_date,
                                     end_date,
                                     tymur_lysenko_owner,
                                     "Introduction to programming lab class",
                                     "Room 303, Innopolis University")


      create start_date.make(2018, 9, 5, 17, 20, 00)
			create end_date.make(2018, 9, 5, 18, 50, 00)

			ITP_lab_wednesday := create_entry (start_date,
                                         end_date,
                                         tymur_lysenko_owner,
                                         "Introduction to programming lab class",
                                         "Room 303, Innopolis University")

      io.put_boolean (in_conflict (ITP_lab_monday, ITP_lab_wednesday))
      io.new_line


      create start_date.make(2018, 9, 3, 18, 20, 00)
			create end_date.make(2018, 9, 3, 20, 00, 00)

      strange_club := create_entry(start_date,
                                   end_date,
                                   tymur_lysenko_owner,
                                   "Some strange club, that handles meetings during classes",
                                   "Room 108, Innopolis University")

      io.put_boolean (in_conflict (ITP_lab_monday, strange_club))
		end

end
