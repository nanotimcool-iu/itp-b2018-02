note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

feature -- Routines

	create_entry (entry_start_date,
                entry_end_date: DATE_TIME;
                entry_owner: PERSON;
                entry_subject,
                entry_place: STRING)
                : ENTRY
			-- factory function that returns newly created entry
		do
			create result.make(entry_start_date,
                         entry_end_date,
                         entry_owner,
                         entry_subject,
                         entry_place)
		end

	edit_subject (e: ENTRY; new_subject: STRING)
			-- edit entry's subject
		do
      e.subject := new_subject
		end

	edit_start_date (e: ENTRY; new_date: DATE_TIME)
			-- edit entry's date
		do
      e.start_date := new_date
		end

	edit_end_date (e: ENTRY; new_date: DATE_TIME)
			-- edit entry's date
		do
      e.end_date := new_date
		end

	get_owner_name (e: ENTRY): STRING
			-- returns entry's owner name
		do
		  result := e.owner.name
		end

	in_conflict (e1, e2: ENTRY): BOOLEAN
      -- returns true if events' dates overlap or
      -- they happen at the same place
		do
			result := e1.owner = e2.owner and then
                e1.start_date <= e2.end_date and then
                e1.end_date >= e2.start_date
		end

end
