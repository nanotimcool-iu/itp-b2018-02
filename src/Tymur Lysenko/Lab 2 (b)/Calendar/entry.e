note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	make

feature {NONE} -- Initialization

	make (entry_start_date,
				entry_end_date: DATE_TIME;
        entry_owner: PERSON;
        entry_subject,
        entry_place: STRING)
			-- Initialization for `Current'.
		do
			start_date := entry_start_date
			end_date := entry_end_date
			owner := entry_owner
			subject := entry_subject
			place := entry_place
		end

feature -- Attributes

	start_date: DATE_TIME assign set_start_date

	set_start_date (new_date: DATE_TIME)
		do
		  start_date := new_date
		end

	end_date: DATE_TIME assign set_end_date

	set_end_date (new_date: DATE_TIME)
		do
		  end_date := new_date
		end

	owner: PERSON

	subject: STRING assign set_subject

	set_subject (new_subject: STRING)
		do
		  subject := new_subject
		end

	place: STRING

invariant
  end_date_greater_or_equal_begin: end_date >= start_date

end
