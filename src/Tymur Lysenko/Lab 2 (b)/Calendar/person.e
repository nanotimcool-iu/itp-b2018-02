note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature {NONE} -- Initialization

	make (person_name: STRING;
        person_phone_number: INTEGER;
        person_work_place: STRING;
        person_email: STRING)
			-- Initialization for `Current'.
		do
			name := person_name
			phone_number := person_phone_number
			work_place := person_work_place
			email := person_email
		end

feature -- Attributes

	name: STRING

	phone_number: INTEGER

	work_place: STRING

	email: STRING

end
