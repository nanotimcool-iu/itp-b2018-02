note
	description: "Bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	BANK_ACCOUNT

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Entry point
		do
			create_bank_account("Tymur", 36000.0)

			-- create_bank_account("Acc1", 10.0)
			-- create_bank_account("Acc2", 1000001.0)

		end

feature -- Constructors

	create_bank_account(ba_owner_name: STRING; balance_amount: DOUBLE)
			-- Parametrized constructor
		require
			balance_restrictions: balance_amount >= 100.0 and balance_amount <= 1000000.0
		do
			owner_name := ba_owner_name
			balance := balance_amount
		end

feature -- Methods

	deposit(amount: DOUBLE)
			-- add money to account
		require
			positive_amount: amount > 0.0
		local
			new_balance: DOUBLE
		do
			new_balance := balance + amount

			if new_balance <= 1000000.0
			then
				balance := new_balance
			end

			ensure
				balance <= 1000000.0
		end

	widthdraw(amount: DOUBLE)
			-- take money from account
		require
			positive_amount: amount > 0.0
		local
			new_balance: DOUBLE
		do
			new_balance := balance - amount

			if new_balance >= 100.0
			then
				balance := new_balance
			end

			ensure
				hundred_balance: balance >= 100.0
		end

feature -- Fields

	owner_name: STRING

	balance: DOUBLE

end
