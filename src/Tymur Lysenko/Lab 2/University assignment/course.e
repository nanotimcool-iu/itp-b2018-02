note
	description: "University application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	COURSE

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Entry point
		do
			create_class(1, "ITP", "Monday;Wednesday", 30)
		end

feature -- Constructors

	create_class (course_id: INTEGER;
                  course_name: STRING;
                  course_schedule: STRING;
                  course_max_students: INTEGER)
			-- constructor
		require
			min_num_students: course_max_students >= 3
		do
			id := course_id
			name := course_name
			schedule := course_schedule
			max_number_of_students := course_max_students
		end

feature -- Course fields

	name: STRING

	id: INTEGER

	schedule: STRING

	max_number_of_students: INTEGER

end
