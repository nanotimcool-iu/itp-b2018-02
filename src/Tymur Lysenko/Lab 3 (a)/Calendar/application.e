note
	description: "Calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
		  start_time, end_time: TIME
		  tymur_lysenko_owner: PERSON
		  day_conflict_list: LIST[ENTRY]
		do
			create tymur_lysenko_owner.make ("Tymur Lysenko", 47161516, "Student @ Innopolis University", "t.lyisenko@innopolis.university")

			create clndr.make

			create start_time.make(17, 20, 00)
			create end_time.make(18, 50, 00)
			clndr.add_entry(create {ENTRY}.make(start_time,
                                          end_time,
																					tymur_lysenko_owner,
																					"Introduction to programming lab class",
																					"Room 303, Innopolis University"),
											2)

			create start_time.make(18, 20, 00)
			create end_time.make(19, 50, 00)
			clndr.add_entry(create {ENTRY}.make(start_time,
                                          end_time,
																					tymur_lysenko_owner,
																					"Club",
																					"Room 301, Innopolis University"),
											2)

			create start_time.make(18, 20, 00)
			create end_time.make(20, 00, 00)
			clndr.add_entry(create {ENTRY}.make(start_time,
                                          end_time,
																					tymur_lysenko_owner,
																					"Introduction to programming lab class",
																					"Room 304, Innopolis University"),
											5)

			day_conflict_list := clndr.in_conflict(2)
			day_conflict_list := clndr.in_conflict(5)

			print(clndr.printable_month)
		end

feature -- Attributes

	clndr: CALENDAR

end
