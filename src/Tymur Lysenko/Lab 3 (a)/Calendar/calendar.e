note
	description: "Summary description for {CALENDAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

create make

feature -- Initialization

	make
		local
		  i: INTEGER
  	do
  	  create days.make(31)

			from
			  i := 1
			until
			  i > 31
			loop
				days.extend(create {DAY}.make(i))

  	    i := i + 1
			end
  	end

feature -- Routines

	add_entry(e: ENTRY; day: INTEGER)
			-- adds new entry to the calendar
		require
			day > 0 and then day <= 31
		do
			days[day].add_entry(e)
		end

	edit_subject (e: ENTRY; new_subject: STRING)
			-- edit entry's subject
		do
      e.subject := new_subject
		end

	edit_start_time (e: ENTRY; new_time: TIME)
			-- edit entry's date
		do
      e.start_time := new_time
		end

	edit_end_time (e: ENTRY; new_time: TIME)
			-- edit entry's date
		do
      e.end_time := new_time
		end

	get_owner_name (e: ENTRY): STRING
			-- returns entry's owner name
		do
		  result := e.owner.name
		end

		in_conflict(day_number: INTEGER): ARRAYED_LIST[ENTRY]
				-- returns list of entries that overlap in time
			local
				i, j, num_events: INTEGER
				day_of_month: DAY
			do
			  create result.make(3)
				day_of_month := days[day_number]
				num_events := day_of_month.events.count

					from
					  i := 1
					until
						i = num_events
					loop
						from
						  j := i + 1
						until
						  j = num_events + 1
						loop
						  if in_conflict_entries(day_of_month.events[i], day_of_month.events[j]) then
						  	if not result.has(day_of_month.events[i]) then
						  	  result.extend(day_of_month.events[i])
						  	end

						  	if not result.has(day_of_month.events[j]) then
						  	  result.extend(day_of_month.events[j])
						  	end

						  	j := j + 1
						  end

						  i := i + 1
						end
					end
			end

	printable_month: STRING
			-- returns printable version of the month
			-- along with events of each day
		do
			result := ""

			across days as day
			loop
				result := result + "Day " + day.item.day_number.out + "%N%T"

			  if not day.item.events.is_empty then
					result := result + "Events:%N"

					across day.item.events as event
					loop
					  result := result + "%T%TOwner: " + event.item.owner.name + "%N"
					  				+ "%T%TSubject: " + event.item.subject + "%N"
					  				+ "%T%TLocation: " + event.item.place + "%N"
					  				+ "%T%TTime: " + event.item.start_time.out + " - " + event.item.end_time.out + "%N%N"
					end
			  else
			  	result := result + "No events%N"
			  end

			end
		end

feature {NONE} -- Internal queries

	in_conflict_entries (e1, e2: ENTRY): BOOLEAN
      -- returns true if events' dates overlap or
      -- they happen at the same place
		do
			result := e1.owner = e2.owner and then
                e1.start_time <= e2.end_time and then
                e1.end_time >= e2.start_time
		end

feature -- Attributes

	days: ARRAYED_LIST[DAY]

end
