class
	DAY

create
	make

feature {NONE} -- Initialization

	make(day: INTEGER)
	require
	  day > 0 and then day <= 31
	do
		day_number := day
		create events.make(3)
	end

feature -- Attributes

	day_number: INTEGER

	events: ARRAYED_LIST[ENTRY]

feature -- Routines

	add_entry(e: ENTRY)
			-- adds an entry to the day
		do
		  events.extend(e)
		end

end
