note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	make

feature {NONE} -- Initialization

	make (entry_start_time,
				entry_end_time: TIME;
        entry_owner: PERSON;
        entry_subject,
        entry_place: STRING)
			-- Initialization for `Current'.
		do
			start_time := entry_start_time
			end_time := entry_end_time
			owner := entry_owner
			subject := entry_subject
			place := entry_place
		end

feature -- Attributes

	start_time: TIME assign set_start_time

	set_start_time (new_time: TIME)
		do
		  start_time := new_time
		end

	end_time: TIME assign set_end_time

	set_end_time (new_time: TIME)
		do
		  end_time := new_time
		end

	owner: PERSON

	subject: STRING assign set_subject

	set_subject (new_subject: STRING)
		do
		  subject := new_subject
		end

	place: STRING

invariant
  end_date_greater_or_equal_begin: end_time >= start_time

end
