note
	description: "ConnectFour application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create game.start
			game.play
		end

feature

		game: CONNECTFOUR

end
