note
	description: "Summary description for {CONNECTFOUR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONNECTFOUR

inherit
	EXECUTION_ENVIRONMENT

create
	start

feature {NONE} -- Initialization

	start
			-- Initialization for `Current'.
		do
			create game_board.make(6, 7)
			create Player1.make ("Player1", '@')
			get_player_data(Player1, create {CHARACTER}.default_create)
			create Player2.make ("Player2", '#')
			get_player_data(Player2, Player1.short_name)
		end

feature -- Open commands

	play
			-- play the game
		local
		  current_player: PLAYER
			game_state: GAMESTATE
			winner: PLAYER
		do
			print_game
			current_player := Player1

			from game_state := game_board.calculate_game_state until
			  game_state.is_finished
			loop
				print_game
				print(current_player.player_name + " (" + current_player.short_name.out + "), enter # of a column to put disc in:%N")
				io.read_integer

				from until
				  game_board.add_disc(io.last_integer, current_player)
				loop
					print_game
					print("Can't insert a disc into column " + io.last_integer.out + "%N")

					print(current_player.player_name + " (" + current_player.short_name.out + "), enter # of a column to put disc in:%N")
					io.read_integer
				end

				if current_player ~ Player1
				then
				  current_player := Player2
				else
				  current_player := Player1
				end

				game_state := game_board.calculate_game_state
			end

			print_game

			winner := game_state.winner

			if winner /= void
			then
				print(winner.player_name + " (" + winner.short_name.out + ") is a winner! Congratulations!!!")
			else
				print("Noone is  a winner :(")
			end
		end

feature {NONE} -- Internal commands

	get_player_data(player_to_create: PLAYER; forbidden_short_name: CHARACTER)
			-- command asks player to enter full and short name
		local
		  player_full_name: STRING;
			player_short_name: CHARACTER
		do
			print(player_to_create.player_name + ", please, enter your name:%N")
			io.read_word

			player_full_name := create {STRING}.make_from_string(io.last_string)

			from
			  create player_short_name.default_create
			until
				(player_short_name = '@'
				or else
				player_short_name = '#'
				or else
				player_short_name = '+'
				or else
				player_short_name = '='
				or else
				player_short_name = '$'
				or else
				player_short_name = '&'
				or else
				player_short_name = '%%')
				and then
				player_short_name /= forbidden_short_name
			loop
			  print(player_full_name + ", pick a symbol for your disc (@, #, +, =, $, &, %%)%N")
			  io.read_word

				player_short_name := io.last_string[1]

			  if player_short_name = forbidden_short_name
			  then
			    print("You can't pick " + player_short_name.out + ", it has already been picked!%N")
			  end
			end

			player_to_create.player_name := player_full_name
			player_to_create.short_name := player_short_name
		end

	print_game
			-- refresh screen by clearing it and printing last game state
		do
			system("CLS")
			print(game_board.to_string + "%N")
		end

feature {NONE} -- Attributes

	game_board: CONNECTFOUR_BOARD
	player1, player2: PLAYER

end
