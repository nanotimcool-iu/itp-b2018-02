note
	description: "Summary description for {CONNECTFOUR_BOARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONNECTFOUR_BOARD

create
	make

feature {NONE} -- Initialization

	make(number_rows, number_columns: INTEGER)
			-- Initialization for CONNECTFOUR_BOARD.
		local
		  i, j: INTEGER
		do
			rows := number_rows
			columns := number_columns
			last_inserted_row := 1
			last_inserted_column := 1
			create current_disc.make(void)
			create current_state.make
			create board.make_filled(create {DISC}.make(void), rows, columns)

			from
			  i := 1
			until
				i > rows
			loop
				from
				  j := 1
				until
				  j > columns
				loop
					board[i, j] := create {DISC}.make (void)
					j := j + 1
				end

				i := i + 1
			end

		end

feature -- Commands

	add_disc(column_no: INTEGER; current_player: PLAYER): BOOLEAN
			-- adds player's disc to the specified column
			-- and returns true if disc has successfully been added, false otherwise
		local
			i : INTEGER
		do
			from
				i := rows
			until
				i < 1
				or else
				column_no > columns
				or else
				column_no < 1
				or else
				board[i, column_no].disc_owner = void
			loop
				i := i - 1
			end

			if i < 1
			or else
			column_no > columns
			or else
			column_no < 1
			then

			  result := false

			else

				board[i, column_no].disc_owner := current_player

				number_of_moves := number_of_moves + 1

				last_inserted_row := i
				last_inserted_column := column_no

				current_disc := board[last_inserted_row, last_inserted_column]

			  result := true

			end
		end

	calculate_game_state: GAMESTATE
			-- the query checks if last inserted disc brings the game to victory
			-- needs to be called after each changes in
			-- last_inserted_row, last_inserted_column, that is when new disc is added
			-- successfully
		local
			vertical_number_of_discs,
			horizontal_number_of_discs,
			top_left_bottom_right_number_of_discs,
			top_right_bottom_left_number_of_discs: INTEGER
  	do
			if not current_state.is_finished
			then

				if number_of_moves = rows * columns
    		then
  				current_state.is_finished := true
  				current_state.winner := void
    		end

				vertical_number_of_discs := 1
				horizontal_number_of_discs := 1
				top_left_bottom_right_number_of_discs := 1
				top_right_bottom_left_number_of_discs := 1

				vertical_number_of_discs := vertical_number_of_discs + count_same_discs_top

				-- is current player a winner?
				if vertical_number_of_discs < 4
				then
					vertical_number_of_discs := vertical_number_of_discs + count_same_discs_bottom

					if vertical_number_of_discs < 4
					then
						horizontal_number_of_discs := horizontal_number_of_discs + count_same_discs_left

						-- is current player a winner?
						if horizontal_number_of_discs < 4
						then
							horizontal_number_of_discs := horizontal_number_of_discs + count_same_discs_right

							-- is current player a winner?
							if horizontal_number_of_discs < 4
							then
								top_left_bottom_right_number_of_discs := top_left_bottom_right_number_of_discs + count_same_discs_top_left

								if top_left_bottom_right_number_of_discs < 4
								then
									top_left_bottom_right_number_of_discs := top_left_bottom_right_number_of_discs + count_same_discs_bottom_right

									if top_left_bottom_right_number_of_discs < 4
									then
									  top_right_bottom_left_number_of_discs := top_right_bottom_left_number_of_discs + count_same_discs_top_right

									  if top_right_bottom_left_number_of_discs < 4
									  then
									    top_right_bottom_left_number_of_discs := top_right_bottom_left_number_of_discs + count_same_discs_bottom_left

									    if top_right_bottom_left_number_of_discs >= 4
									    then
												set_current_winner
									    end
									  else
									    set_current_winner
									  end
									else
									  set_current_winner
									end
								else
								  set_current_winner
								end
							else
								set_current_winner
							end
						else
						  set_current_winner
						end
					else
						set_current_winner
					end

				else
					set_current_winner
				end
			end

			result := current_state
  	end

	to_string: STRING
			-- returns a string representation of the current game state
		local
		  row_no,
		  column_no: INTEGER;
		  current_disc_owner: PLAYER
  	do
			result := "  "

  		from
  			column_no := 1
  		until
  			column_no > columns
  		loop
  			result := result + column_no.out + " "

  			column_no := column_no + 1
  		end

  		result.append_character('%N')

			from
				row_no := 1
			until
				row_no > rows
			loop
				result := result + row_no.out + " "

				from
				  column_no := 1
				until
				  column_no > columns
				loop
					current_disc_owner := board[row_no, column_no].disc_owner

					if current_disc_owner = void
					then
						result := result + "."
  				else
						result.append_character(current_disc_owner.short_name)
					end

					result.append_character(' ')

					column_no := column_no + 1
				end

				result.append_character('%N')
				row_no := row_no + 1
			end
  	end

feature {NONE} -- Internal queries

	set_current_winner
			-- finishes the game and sets winner as an owner of the last inserted disc
		do
			current_state.is_finished := true
			current_state.winner :=  board[last_inserted_row, last_inserted_column].disc_owner
		end

	count_same_discs_top: INTEGER
			-- count number of the same discs on the top
			-- of the last inserted disc
		local
		  row_no: INTEGER
		do
			result := 0

      from
      	row_no := last_inserted_row - 1
      until
      	row_no < 1
      	or else
      	not current_disc.standard_is_equal(board[row_no, last_inserted_column])
      	or else
      	current_disc.disc_owner = void
      	or else
      	result = 3
      loop
      	result := result + 1

      	row_no := row_no - 1
      end
		end

	count_same_discs_bottom: INTEGER
			-- count number of the same discs on the bottom
			-- of the last inserted disc
		local
			row_no: INTEGER
		do
			result := 0

      from
      	row_no := last_inserted_row + 1
      until
      	row_no > rows
      	or else
      	not current_disc.standard_is_equal(board[row_no, last_inserted_column])
      	or else
      	current_disc.disc_owner = void
      	or else
      	result = 3
      loop
      	result := result + 1

      	row_no := row_no + 1
      end
		end

	count_same_discs_left: INTEGER
			-- count number of the same discs on the left
			-- of the last inserted disc
		local
		  column_no: INTEGER
		do
		  result := 0

		  from
		    column_no := last_inserted_column - 1
		  until
				column_no < 1
      	or else
      	not current_disc.standard_is_equal(board[last_inserted_row, column_no])
      	or else
      	current_disc.disc_owner = void
      	or else
      	result = 3
		  loop
      	result := result + 1

      	column_no := column_no - 1
		  end
		end

	count_same_discs_right: INTEGER
			-- count number of the same discs on the left
			-- of the last inserted disc
		local
		  column_no: INTEGER
		do
		  result := 0

		  from
		    column_no := last_inserted_column + 1
		  until
				column_no > columns
      	or else
      	not current_disc.standard_is_equal(board[last_inserted_row, column_no])
      	or else
      	current_disc.disc_owner = void
      	or else
      	result = 3
		  loop
      	result := result + 1

      	column_no := column_no + 1
		  end
		end

	count_same_discs_top_left: INTEGER
			-- count number of the same discs on the top left
			-- of the last inserted disc
  	local
  		row_no,
			column_no: INTEGER
  	do
			result := 0

			from
			  row_no := last_inserted_row - 1
			  column_no := last_inserted_column - 1
			until
				row_no < 1
				or else
      	column_no < 1
				or else
				not current_disc.standard_is_equal(board[row_no, column_no])
				or else
				current_disc.disc_owner = void
      	or else
      	result = 3
			loop
				result := result + 1

				row_no := row_no - 1
			  column_no := column_no - 1
			end
  	end

	count_same_discs_top_right: INTEGER
			-- count number of the same discs on the top right
			-- of the last inserted disc
  	local
  		row_no,
			column_no: INTEGER
  	do
			result := 0

			from
			  row_no := last_inserted_row - 1
			  column_no := last_inserted_column + 1
			until
				row_no < 1
      	or else
      	column_no > columns
				or else
				not current_disc.standard_is_equal(board[row_no, column_no])
				or else
				current_disc.disc_owner = void
      	or else
      	result = 3
			loop
				result := result + 1

				row_no := row_no - 1
			  column_no := column_no + 1
			end
  	end

	count_same_discs_bottom_left: INTEGER
			-- count number of the same discs on the bottom left
			-- of the last inserted disc
  	local
  		row_no,
			column_no: INTEGER
  	do
			result := 0

			from
			  row_no := last_inserted_row + 1
			  column_no := last_inserted_column - 1
			until
				row_no > rows
      	or else
      	column_no < 1
				or else
				not current_disc.standard_is_equal(board[row_no, column_no])
				or else
				current_disc.disc_owner = void
      	or else
      	result = 3
			loop
				result := result + 1

				row_no := row_no + 1
			  column_no := column_no - 1
			end
  	end

	count_same_discs_bottom_right: INTEGER
			-- count number of the same discs on the bottom right
			-- of the last inserted disc
  	local
  		row_no,
			column_no: INTEGER
  	do
			result := 0

			from
			  row_no := last_inserted_row + 1
			  column_no := last_inserted_column + 1
			until
				row_no > rows
      	or else
      	column_no > columns
				or else
				not current_disc.standard_is_equal(board[row_no, column_no])
				or else
				current_disc.disc_owner = void
      	or else
      	result = 3
			loop
				result := result + 1

				row_no := row_no + 1
			  column_no := column_no + 1
			end
  	end

feature -- Open attributes

	columns, rows : INTEGER

feature {NONE} -- Internal attributes

	board : ARRAY2[DISC]
	last_inserted_row, last_inserted_column : INTEGER
	current_state: GAMESTATE
	number_of_moves: INTEGER
	current_disc: DISC

end
