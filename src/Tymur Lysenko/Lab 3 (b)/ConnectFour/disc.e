note
	description: "Summary description for {DISC}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DISC

create
	make

feature {NONE} -- Initialization

	make(owner: detachable PLAYER)
			-- Initialization for `Current'.
		do
			disc_owner := owner
		end

feature -- Attributes

	disc_owner: detachable PLAYER assign set_owner

	set_owner(new_owner: detachable PLAYER)
  	do
			disc_owner := new_owner
  	end

end
