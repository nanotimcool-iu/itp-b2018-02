note
	description: "Summary description for {GAMESTATE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAMESTATE

create
	make

feature {NONE} -- Initialization

	make
			-- Make start state of the game
		do
			is_finished := false
			winner := void
		end

feature -- Attributes

	is_finished: BOOLEAN assign set_finished

	set_finished(is_game_finished: BOOLEAN)
		do
		  is_finished := is_game_finished
		end

	winner: detachable PLAYER assign set_winner

	set_winner(game_winner: detachable PLAYER)
		do
		  winner := game_winner
		end

end
