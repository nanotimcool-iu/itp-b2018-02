note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create
	make

feature {NONE} -- Initialization

	make(name: STRING; player_short_name: CHARACTER)
			-- Initialization for `Current'.
		do
			player_name := name
			short_name := player_short_name
		end

feature -- Attributes

	player_name: STRING assign set_player_name

		set_player_name(new_name: STRING)
			do
			  player_name := new_name
			end

	short_name: CHARACTER assign set_short_name

	set_short_name(new_short_name: CHARACTER)
		do
		  short_name := new_short_name
		end

end
