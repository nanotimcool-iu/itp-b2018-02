note
	description: "DrawTriangle application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			height,
			row_number,
			current_number,
			current_row_number,
			num_elements,
			tabulation_length: INTEGER
			numbers: ARRAYED_LIST[STRING]
			current_string: STRING
		do
			print("Enter a number:%N")
			io.read_integer

			height := io.last_integer

			create numbers.make(height)

			from
				row_number := 0
			  current_number := 1
			  num_elements := 1
			until
			  row_number = height
			loop
				current_string := ""

				from
					current_row_number := 0
				until
					current_row_number = num_elements
				loop
					current_string := current_string + current_number.out + " "

					current_number := current_number + 1
					current_row_number := current_row_number + 1
				end

				numbers.extend(current_string)

  			row_number := row_number + 1

  			if row_number \\ 2 = 0 then
  				num_elements := num_elements + 1
  			end
			end

			from
			  row_number := 1
			until
				row_number > numbers.count
			loop
				if numbers[height].count = numbers[row_number].count then
				  tabulation_length := 0
				else
				  tabulation_length := (numbers[height].count - numbers[row_number].count - 1)
				end

				numbers[row_number] := numbers[row_number] + create {STRING}.make_filled(' ', tabulation_length) -- Tabulate

				numbers[row_number] := numbers[row_number] + join_to_string(reverse_string_list(numbers[row_number].split(' ')), " ") -- add reversed triangle

				print(numbers[row_number] + "%N")
				row_number := row_number + 1
			end
		end

	trim_string(str_to_trim: STRING): STRING
		do
		  result := str_to_trim
		  result.trim
		end

	join_to_string(list_to_join: ARRAYED_LIST[STRING]; separator: STRING): STRING
		do
			result := ""

			across list_to_join as cur_word
			loop
			  result := result + cur_word.item + separator
			end

			result := result.substring(1, result.count - separator.count)
		end

	reverse_string_list(list_to_reverse: LIST[STRING]): ARRAYED_LIST[STRING]
		local
			num_elements: INTEGER
		do
			num_elements := list_to_reverse.count
			create result.make(num_elements)

		  across list_to_reverse as cur_string
		  loop
				result.put_front (cur_string.item)

				num_elements := num_elements - 1
		  end
		end
end
