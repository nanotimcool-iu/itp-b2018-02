note
	description: "Bags application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
		  ms: MULTISET
		do
			create ms.make

			ms.add ('a', 5)
			ms.add ('c', 3)
			ms.add ('b', 1)

			print(ms.to_string + "%N%N")

			print("Max = " + ms.max.out + "%N")
			print("Min = " + ms.min.out + "%N")

			ms.remove('a', 3)
			ms.remove('c', 4)
			ms.remove('d', 3)

			print("%N" + ms.to_string + "%N")
		end

end
