note
	description: "Summary description for {CELL_INFO}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CELL_INFO

create
	make

feature {NONE} -- Initialization

	make(char_val: CHARACTER; num_of_chars: INTEGER)
			-- Initialization for `Current'.
		require
		  positive_num_chars: num_of_chars > 0
		do
			val := char_val
			num := num_of_chars
		end

feature -- Commands

	add_chars(n: INTEGER)
			-- add n more chars
		require
			positive_number: n > 0
		do
			num := num + n
		end

	subtract_chars(n: INTEGER)
			-- subtract n chars
		require
		  positive_number: n > 0
		do
			num := num - n
		end

feature -- Attributes

	val: CHARACTER
	num: INTEGER

invariant positive_num: num > 0

end
