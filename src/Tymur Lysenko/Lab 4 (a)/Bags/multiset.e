note
	description: "Summary description for {MULTISET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MULTISET

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			create elements.make(3)
		end

feature -- Commands

	add(val: CHARACTER; n: INTEGER)
			-- add n copies of val
		require
			positive_num: n > 0
		local
		  cell_index: INTEGER
		do
			cell_index := get_index_by_char(val)

			if cell_index = 0 then
				elements.extend(create {CELL_INFO}.make (val, n))
			else
				elements[cell_index].add_chars(n)
			end

		ensure
			get_index_by_char(val) /= 0
		end

	remove(val: CHARACTER; n: INTEGER)
			-- remove up to n characters
		require
		  positive_n: n > 0
		local
		  target_element: CELL_INFO
		  element_index: INTEGER
		do
			element_index := get_index_by_char(val)

			if element_index /= 0 then
				target_element := elements[element_index]

  			if target_element.num - n > 0 then
  				target_element.subtract_chars(n)
  			else
					elements.prune(target_element)
  			end
			end
		end

feature -- Queries

	min: CHARACTER
			-- get minimal character in the collection that contains elements
		require
		  elements.count /= 0
		do
			result := elements[1].val

		  across elements as element
		  loop
		  	if element.item.val < result then
		  	  result := element.item.val
		  	end
		  end
		end

	max: CHARACTER
			-- get maximum character in the collection that contains elements
		require
		  elements.count /= 0
		do
			result := elements[1].val

		  across elements as element
		  loop
		  	if element.item.val > result then
		  	  result := element.item.val
		  	end
		  end
		end

	is_equal_multiset(ms: MULTISET): BOOLEAN
			-- checks if ms is the current object
		do
			result := (current.is_equal(ms))
		end

	to_string: STRING
			-- return a string representation
		local
		  elements_string: STRING
		  i: INTEGER
		do
			result := "{ "

		  across elements as element
		  loop
		  	from
		  		elements_string := ""
					i := 1
		  	until
		  		i > element.item.num
		  	loop
					elements_string := elements_string + element.item.val.out + "; "
					i := i + 1
		  	end

		    result := result + elements_string
		  end

		  result := result.substring(1, result.count - 2) + " }"
		end

feature -- Internal queries

	get_index_by_char(char: CHARACTER): INTEGER
			-- returns an index of char in elements
			-- if no char was found result := 0
		local
		  i, num_elements: INTEGER
		do
			num_elements := elements.count

			from
				result := 0
				i := 1
			until
				result /= 0
				or else
				i > num_elements
			loop
				if elements[i].val = char then
				  result := i
				end

				i := i + 1
			end
		end

feature -- Attributes

	elements: ARRAYED_LIST[CELL_INFO]

end
