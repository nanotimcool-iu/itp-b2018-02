# Binary search, las session 5, exercise 6 (https://moodle.innopolis.university/pluginfile.php/2197/course/section/812/04b_lab_session_exercises.pdf):

## (P1)

<pre>
from
  i := 1; j := n
until
  i = j
loop
  m := (i + j) // 2

  if t [m] <<span style="color:red">=</span> x then
    i := m
  else
    j := m
  end
end

Result := (x = t[i])
</pre>

### **Explanation**

Because of the highlighted comparation and the loop condition the loop won't finish in case `x = t [n + 1] // 2`.

### **Colclusion**

<span style="color:red">The algorithm is incorrect.</span>

#### **Failed test input**

t = [1 9 45 59 126 256 856 1095]

x = 59

#### **Expected result**

True

#### **Actual result**

Infinite loop

## (P2)

<pre>
from
  i := 1; j := n; Result := False
until i = j
      <span style="color:red">and
      not</span> Result
loop
  m := (i + j) // 2

  if t [m] < x then
    i := m + 1
  elseif t [m] = x then
    Result := True
  else
    j := m - 1
  end
end
</pre>

### **Explanation**

First, the second loop condition should be `Result` (without `not`). Secondly, the loop conditions should be linked via `or`, not `and`. That's because Eiffel finishes loop only in case its condition is true. However, when the `result` will be `True` and `i != j`, the loop won't finish, because it's condition will be `False`, although it must finish.

### **Colclusion**

<span style="color:red">The algorithm is incorrect.</span>

#### **Failed test input**

t = [1 9 45 59 126 256 856 1095]

x = 59

#### **Expected result**

True

#### **Actual result**

Infinite loop

## (P3)

<pre>
from
  i := 0; j := n;
until i = j
loop
  m := (i + j + 1) // 2

  if t [m] <<span style="color:red">=</span> x then
    i := m + 1
  else
    j := m
  end
end

if i >= 1 and i <= n then
  Result := (x = t [i])
else
  Result := False
end
</pre>

### **Explanation**

In case `t [m] = x` the loop won't be finished, when we initially look for the middle element.

### **Colclusion**

<span style="color:red">The algorithm is incorrect.</span>

#### **Failed test input**

t = [1 9 45 59 126 256 856 1095]

x = 59

#### **Expected result**

True

#### **Actual result**

Infinite loop

## (P4)

<pre>
from
  i := 0; j := n + 1;
until i = j
loop
  m := (i + j) // 2

  if t [m] <<span style="color:red">=</span> x then
    i := m + 1
  else
    j := m
  end
end

if i >= 1 and i <= n then
  Result := (x = t [i])
else
  Result := False
end
</pre>

### **Explanation**

In case `t [m] = x` the loop won't be finished, when we initially look for the middle element.

### **Colclusion**

<span style="color:red">The algorithm is incorrect.</span>

#### **Failed test input**

t = [1 9 45 59 126 256 856 1095]

x = 59

#### **Expected result**

True

#### **Actual result**

Infinite loop
