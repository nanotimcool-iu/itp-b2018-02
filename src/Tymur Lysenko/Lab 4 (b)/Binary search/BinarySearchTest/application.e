note
	description: "BinarySearchTest application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create t.make(8)
			t.extend(1)
			t.extend(9)
			t.extend(45)
			t.extend(59)
			t.extend(126)
			t.extend(256)
			t.extend(856)
			t.extend(1095)
			n := t.count
			across t as array_elem
			loop
			  print(array_elem.item.out + " ")
			end

			-- print("%NArray has element 59 (p1): " + p1(59).out + "%N") -- infinite loop
			-- print("%NArray has element 59 (p2): " + p2(59).out + "%N") -- infinite loop
			-- print("%NArray has element 59 (p3): " + p3(59).out + "%N") -- infinite loop
			-- print("%NArray has element 59 (p4): " + p4(59).out + "%N") -- infinite loop
		end

	p1(x: INTEGER): BOOLEAN
		local
		  i, j, m: INTEGER
		do
		  from
        i := 1; j := n
      until
        i = j
      loop
        m := (i + j) // 2

        if t [m] <= x then
          i := m
        else
          j := m
        end
      end

      Result := (x = t[i])
		end


	p2(x: INTEGER): BOOLEAN
		local
		  i, j, m: INTEGER
		do
		  from
        i := 1; j := n; Result := False
      until i = j
            and
            not Result
      loop
        m := (i + j) // 2

        if t [m] < x then
          i := m + 1
        elseif t [m] = x then
          Result := True
        else
          j := m - 1
        end
      end
		end

	p3(x: INTEGER): BOOLEAN
		local
		  i, j, m: INTEGER
		do
		  from
        i := 0; j := n;
      until i = j
      loop
        m := (i + j + 1) // 2

        if t [m] <= x then
          i := m + 1
        else
          j := m
        end
      end

      if i >= 1 and i <= n then
        Result := (x = t [i])
      else
        Result := False
      end
		end

	p4(x: INTEGER): BOOLEAN
		local
		  i, j, m: INTEGER
		do
		  from
        i := 0; j := n + 1;
      until i = j
      loop
        m := (i + j) // 2

        if t [m] <= x then
          i := m + 1
        else
          j := m
        end
      end

      if i >= 1 and i <= n then
        Result := (x = t [i])
      else
        Result := False
      end
		end

feature -- Attributes

	t: ARRAYED_LIST[INTEGER]
	n: INTEGER

end
