note
	description: "MatrixOpers application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS; MATRIX_OPER

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
		  m1, m2,
		  added_matrices,
		  subtracted_matrices,
		  multiplied_matrices: ARRAY2[INTEGER]
		do
			-- Create & print matrices
			create m1.make_filled (0, 3, 3)
			create m2.make_filled (0, 3, 3)

			m1[1, 1] := 5
			m1[1, 2] := 10
			m1[1, 3] := 8
			m1[2, 1] := 8
			m1[2, 2] := 1
			m1[2, 3] := 2
			m1[3, 1] := 9
			m1[3, 2] := 23
			m1[3, 3] := 7

			m2[1, 1] := 49
			m2[1, 2] := 12
			m2[1, 3] := 3
			m2[2, 1] := 2
			m2[2, 2] := 5
			m2[2, 3] := 1
			m2[3, 1] := 56
			m2[3, 2] := 2
			m2[3, 3] := 59

			print("m1 =%N" + matrix_to_string(m1) + "%N")
			print("m2 =%N" + matrix_to_string(m2) + "%N")

			-- Add matrices
			added_matrices := add(m1, m2)

			print("m1 + m2 =%N" + matrix_to_string(added_matrices) + "%N")

			-- Subtract matrices
			subtracted_matrices := minus(m1, m2)

			print("m1 - m2 =%N" + matrix_to_string(subtracted_matrices) + "%N")

			-- Multiply matrices
			multiplied_matrices := prod(m1, m2)

			print("m1 * m2 =%N" + matrix_to_string(multiplied_matrices) + "%N")

			-- Determinant
			print("det(m1) = " + det(m1).out + "%N")
			print("det(m2) = " + det(m2).out + "%N")
		end
end
