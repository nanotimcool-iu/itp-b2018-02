note
	description: "Summary description for {MATRIX_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPER

--create
--	make

--feature {NONE} -- Initialization

--	make
--			-- Initialization for `Current'.
--		do

--		end

feature -- Open routines

	matrix_to_string(m: ARRAY2[INTEGER]): STRING
			-- get string representation of a given matrix
  	local
  	  i, j, matrix_height, matrix_width: INTEGER
		do
			matrix_height := m.height
			matrix_width := m.width

			result := ""

			from
				i := 1
			until
				i > matrix_height
			loop
				result := result + "(%T"

				from
				  j := 1
				until
				  j > matrix_width
				loop
					result := result + m[i, j].out + "%T"

					j := j + 1
				end

				result := result + ")%N"
				i := i + 1
			end
		end

	add(m1, m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
			-- add 2 matrices that can be added
		require
			addable_matrices:
				m1.height = m2.height
  			and then
  			m1.width = m2.width
  	local
  	  i, j, matrix_height, matrix_width: INTEGER
		do
			matrix_height := m1.height
			matrix_width := m1.width

			create result.make_filled (0, matrix_height, matrix_width)

			from
				i := 1
			until
				i > matrix_height
			loop
				from
				  j := 1
				until
				  j > matrix_width
				loop
					result[i, j] := m1[i, j] + m2[i, j]

					j := j + 1
				end

				i := i + 1
			end

			ensure
				same_size:
					result.height = m1.height
					and then
					result.height = m2.height
					and then
					result.width = m1.width
					and then
					result.width = m2.width
		end

	multiply_by_scalar(scalar: INTEGER; m: ARRAY2[INTEGER]): ARRAY2[INTEGER]
			-- multiply a matrix by a scalar value
  	local
			i, j, matrix_height, matrix_width: INTEGER
		do
			matrix_height := m.height
			matrix_width := m.width
			create result.make_filled(0, matrix_height, matrix_width)

			from
				i := 1
			until
				i > matrix_height
			loop
				from
				  j := 1
				until
				  j > matrix_width
				loop
					result[i, j] := scalar * m[i, j]

					j := j + 1
				end

				i := i + 1
			end

			ensure
				same_size:
					result.height = m.height
					and then
					result.width = m.width
		end

	minus(m1, m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
				-- subtract 2 matrices that can be subtracted
		require
			addable_matrices:
				m1.height = m2.height
  			and then
  			m1.width = m2.width
  	do
			result := add(m1, multiply_by_scalar(-1, m2))

			ensure
				same_size:
					result.height = m1.height
					and then
					result.height = m2.height
					and then
					result.width = m1.width
					and then
					result.width = m2.width
  	end

	prod(m1, m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
			-- calculate the product of 2 matrices
		require
		  multipliable_matrices:
		  	m1.width = m2.height
  	local
  	  i, j, k,
  	  matrix_height,
  	  matrix_width: INTEGER
		do
			matrix_height := m1.height
			matrix_width := m2.width

			create result.make_filled(0, matrix_height, matrix_width)

			from
				i := 1
			until
				i > matrix_height
			loop
				from
				  j := 1
				until
				  j > matrix_width
				loop
					-- sum products of the corresponding elements of matrices
					from
						k := 1
					until
						k > matrix_width
					loop
						result[i, j] := result[i, j] + (m1[i, k] * m2[k, j])

						k := k + 1
					end

					j := j + 1
				end

				i := i + 1
			end

			ensure
				matrix_product_size:
					result.height = m1.height
					and then
					result.width = m2.width
		end

	det(m: ARRAY2[INTEGER]): INTEGER
			-- calculate the determinant of a square matrix
		require
		  square_matrix:
		  	m.height = m.width

		  non_zero_size:
		  	m.height /= 0
		  	and then
		  	m.width /= 0
		local
		  i, width: INTEGER
		  additional_factors: ARRAY[INTEGER]
		do
			result := 0

			inspect m.height
			when 1 then
				result := m[1, 1]
			when 2 then
				result := (m[1, 1] * m[2, 2]) - (m[1, 2] * m[2, 1])
			else
				additional_factors := additional_factors_for_first_row(m)
				width := additional_factors.count

				from
					i := 1
				until
					i > width
				loop
					result := result + (m[1, i] * additional_factors[i])

					i := i + 1
				end
			end
		end

	additional_factors_for_first_row(m: ARRAY2[INTEGER]): ARRAY[INTEGER]
			-- calculate additional factors for the first row of a matrix m
		require
		  square_matrix: m.height = m.width
		local
			i, width, sign: INTEGER
		do
			result := minors_for_first_row(m)
			width := result.count
			sign := 1

			from
			  i := 1
			until
				i > width
			loop
				result[i] := sign * result[i]
				sign := -sign

				i := i + 1
			end

		  ensure
		    same_width: result.count = m.width
		end

	minors_for_first_row(m: ARRAY2[INTEGER]): ARRAY[INTEGER]
			-- calculate minors for the first row of a matrix m
		require
		  square_matrix: m.height = m.width
		local
		  i, j, k,
		  width, height,
		  new_width, new_height: INTEGER
		  new_matrix: ARRAY2[INTEGER]
		do
			width := m.width
			height := m.height
			new_width := width - 1
			new_height := height - 1
			create result.make_filled(0, 1, width)

			from
				i := 1
			until
				i > width
			loop
				-- calculate new matrix, by crossing out elements of
				-- row_number-th row and i-th column

				create new_matrix.make_filled(0, new_height, new_width)

				from
					j := 1
				until
					j > new_height
				loop

					-- a part of the old matrix before current column
					from
					  k := 1
					until
					  k = i
					loop
					  new_matrix[j, k] := m[j + 1, k]

					  k := k + 1
					end

					-- a part of the old matrix after current column
					from
					until
						k > new_width
					loop
						new_matrix[j, k] := m[j + 1, k + 1]

						k := k + 1
					end

					j := j + 1
				end

				result[i] := det(new_matrix)
				i := i + 1
			end

		  ensure
		    same_width: result.count = m.width
		end
end
