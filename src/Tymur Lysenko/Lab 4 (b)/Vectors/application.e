note
	description: "vectors application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS; VECTOR_OPER

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
		  v1, v2: ARRAY[INTEGER]
		do
			create v1.make_filled(0, 1, 3)
			create v2.make_filled(0, 1, 3)

			v1[1] := 3
			v1[2] := 4
			v1[3] := 5

			v2[1] := 6
			v2[2] := 7
			v2[3] := 8

			print("v1 =%N" + vector_to_string(v1) + "%N")
			print("v2 =%N" + vector_to_string(v2) + "%N")

			-- Vector product
			print("v1 x v2 =%N" + vector_to_string(cross_product(v1, v2)) + "%N")

			-- Dot product
			print("v1 * v2 = " + dot_product(v1, v2).out + "%N")

			-- Triangle area
			print("S(v1, v2) = " + triangle_area(v1, v2).out)

		end

end
