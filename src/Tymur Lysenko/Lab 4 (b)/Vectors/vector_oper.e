note
	description: "Summary description for {VECTOR_OPER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER



feature -- Open routines

	single_math: SINGLE_MATH
		once
		  create result
		end

	cross_product(v1, v2: ARRAY[INTEGER]): ARRAY[INTEGER]
		-- calculate cross product of v1 and v2 (3d vectors)
	require
	  dimensions3:
	  	v1.count = 3
	  	v2.count = 3
	do
		create result.make_filled(0, 1, 3)

		result[1] := v1[2] * v2[3] - v1[3] * v2[2]
		result[2] := v1[3] * v2[1] - v1[1] * v2[3]
		result[3] := v1[1] * v2[2] - v1[2] * v2[1]

		ensure
			dimensions3:
				result.count = 3
	end

	dot_product(v1, v2: ARRAY[INTEGER]): INTEGER
			-- calculate dot product of 2 integers
		require
		  same_dimensions:
		  	v1.count = v2.count
		local
		  i, dimension: INTEGER
		do
			dimension := v1.count

			result := 0

		  from
		    i := 1
		  until
		    i > dimension
		  loop
		    result := result + v1[i] * v2[i]

		    i := i + 1
		  end
		end

	triangle_area(v1, v2: ARRAY[INTEGER]): INTEGER
		require
  	  dimensions3:
  	  	v1.count = 3
  	  	v2.count = 3
		do
			result := (absoulte_value(cross_product(v1, v2)) / 2).rounded
		end

	absoulte_value(v: ARRAY[INTEGER]): INTEGER
		do
			result := 0

		  across v as coordinate
		  loop
		    result := (result + (coordinate.item ^ 2)).rounded
		  end

		  result := (single_math.sqrt(result)).rounded
		end

	vector_to_string(v: ARRAY[INTEGER]): STRING
		do
		  result := ""

		  across v as coordinate
		  loop
		    result := result + "(%T" + coordinate.item.out + "%T)%N"
		  end
		end

end
