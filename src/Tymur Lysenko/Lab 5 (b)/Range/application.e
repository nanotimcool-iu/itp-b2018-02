note
	description: "Range application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization	

	make
			-- Run application.
		local
		  left, right: INTEGER
		  range1, range2: RANGE
		  union, difference: detachable RANGE
		do
			print("Input left number for range1:%N")
			io.read_integer
			left := io.last_integer
			print("Input right number for range1:%N")
			io.read_integer
			right := io.last_integer
			create range1.make(left, right)

			print("Input left number for range2:%N")
			io.read_integer
			left := io.last_integer
			print("Input right number for range2:%N")
			io.read_integer
			right := io.last_integer
			create range2.make(left, right)

			print("range1 = " + range1.to_string + "%N")
			print("range2 = " + range2.to_string + "%N")

			-- are ranges equal
			print("%Nrange1 == range2 = " + range1.is_equal_range(range2).out + "%N")

			-- is range1 a sub range of range2
			print("%Nrange1 is a sub range of range2 = " + range1.is_sub_range_of(range2).out + "%N")

			-- is range1 a super range of range2
			print("%Nrange1 is a super range of range2 = " + range1.is_super_range_of(range2).out + "%N")

			-- does range1 have left overlaps with range2
			print("%Nrange1 has left overlaps with range2 = " + range1.left_overlaps(range2).out + "%N")

			-- does range1 have right overlaps with range2
			print("%Nrange1 has right overlaps with range2 = " + range1.right_overlaps(range2).out + "%N")

			-- is range1 overlapses with range2
			print("%Nrange1 overlapses with range2 = " + range1.overlaps(range2).out + "%N")

			-- range1 U range2
			-- range1 - range2
			if range1.can_add(range2) then
			  union := range1.add(range2)
			  print("%Nrange1 U range2 = " + union.to_string + "%N")
			else
			  print("%NCan't union range1 and range2%N")
			end

			if range1.overlaps(range2) then
			  difference := range1.subtract(range2)
			  print("%Nrange1 - range2 = " + difference.to_string + "%N")
			else
				print("%NCan't subtract range2 from range1%N")
			end

		end

end
