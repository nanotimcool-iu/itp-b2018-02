note
	description: "Summary description for {RANGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE

create
	make, make_copy

feature {NONE} -- Initialization

	make(init_left, init_right: INTEGER)
			-- Initialization for `Current'.
		do
			left := init_left
			right := init_right
		end

	make_copy(other: RANGE)
			-- create a copy of other
		do
			left := other.left
			right := other.right
		end

feature -- Open queries

	is_empty: BOOLEAN
			-- returns true only if range is empty
		do
			result :=  left > right
		end

	is_equal_range(other: like current): BOOLEAN
			-- checks if the current range is equal to another range
  	do
  	  result := current ~ other
  	end

	is_sub_range_of(other: like current): BOOLEAN
			-- returns true if current is fully contained in other
			-- or current is_empty
		do
			result := (other.left <= left and then other.right >= right)
				or else is_empty
		end

	is_super_range_of(other: like current): BOOLEAN
			-- returns true if other is fully contained in current
			-- or other is_empty
		do
			result := other.is_sub_range_of(current)
		end

	left_overlaps(other: like current): BOOLEAN
			-- returns true if left point of current is within other
		do
		  result := (other.left <= left and then left <= other.right)
		  	or else is_empty
				or else other.is_empty
		end

	left_unique_overlaps(other: like current): BOOLEAN
			-- returns true if left point of current is within other and right one is >=
			-- other.right
		do
		  result := (other.left <= left and then left <= other.right
		  		and then right >= other.right)
		  	or else is_empty
				or else other.is_empty
		end

	right_overlaps(other: like current): BOOLEAN
			-- returns true if right point of current is within other
		do
		  result := (other.left <= right and then right <= other.right)
		  	or else is_empty
				or else other.is_empty
		end

	right_unique_overlaps(other: like current): BOOLEAN
			-- returns true if right point of current is within other and left one is <=
			-- other.left
		do
		  result := (other.left <= right and then right <= other.right
		  		and then left <= other.left)
		  	or else is_empty
				or else other.is_empty
		end

	overlaps(other: like current): BOOLEAN
			-- returns true if any point of current is within other
		do
			result := (left <= other.right and then other.left <= right)
				or else is_empty
				or else other.is_empty
		end

	can_add(other: like current): BOOLEAN
			-- returns true if ranges can be added together
		local
		  addable_range: RANGE
		do
			create addable_range.make_copy(current)
			addable_range.left := addable_range.left - 1
			addable_range.right := addable_range.right + 1

		  result := addable_range.overlaps(other)
		  	or else current.is_empty
		  	or else other.is_empty
		end

	add(other: like current): RANGE
			-- union 2 left/right overlapsing ranges
		require
			can_add: can_add(other)
		local
		  new_left, new_right: INTEGER
		do
			if current.is_empty and then
				other.is_empty then
			  new_left := 0
			  new_right :=-1
			elseif current.is_empty then
				new_left := other.left
			  new_right := other.right
			elseif other.is_empty then
			  new_left := current.left
			  new_right := current.right
			else
				new_left := left.min(other.left)
				new_right := right.max(other.right)
			end

			create result.make(new_left, new_right)

			ensure
			  result.is_super_range_of(current)
			  and then
			  result.is_super_range_of(other)
		end

	can_subtract(other: like current): BOOLEAN
			-- returns true if other can be subtracted from current
		do
		  result := not current.is_super_range_of(other)
		  	or else current.is_equal_range(other)
		  	or else other.is_empty
		  	or else current.is_empty
		end

	subtract(other: like current): RANGE
			-- subtract 2 left/right overlapsing ranges
		 require
			 can_subtract: can_subtract(other)
		local
		  new_left, new_right: INTEGER
		do
			if other.is_empty
				or else not current.overlaps(other) then
				new_left := left
			  new_right := right
			elseif current.left_unique_overlaps(other) then
				new_left := other.right + 1
				new_right := right
			elseif current.right_unique_overlaps(other) then
				new_left := left
				new_right := other.left - 1
			else -- if both are empty or current is a subset of other
			  new_left := 0
			  new_right := -1
			end

			create result.make(new_left, new_right)

		  ensure
			  (result.is_sub_range_of(current)
			  and then
			  not result.is_sub_range_of(other))
			  or else result.is_empty
		end

	to_string: STRING
			-- get string representation of the range
		do
			if is_empty then
				result := "empty"
			else
			  result := "[ " + left.out + "; " + right.out + " ]"
			end
		end

feature -- Getters/Setters

	set_left(new_left: INTEGER)
		do
		  left := new_left
		end

	set_right(new_right: INTEGER)
		do
		  right := new_right
		end

feature -- Attributes

	-- boundaries
	left: INTEGER assign set_left
	right: INTEGER assign set_right

end
