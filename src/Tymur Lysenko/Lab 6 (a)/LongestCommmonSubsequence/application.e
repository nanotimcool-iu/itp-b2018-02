note
	description: "LongestCommmonSubsequence application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	longest(string1, string2: STRING): STRING
			-- returns the longest of 2 strings
			-- in case 2 strings are equal returns the first
		do
			if string1.count >= string2.count then
				result := string1
			else
				result := string2
			end
		end

	lcs(string1, string2: STRING; i, j: INTEGER): STRING
			-- recursively calculate the longest common subsequence of 2 strings
			-- with lengths i and j respectively
		do
			if i = 0 or else j = 0 then
			-- in case 1 string is empty
			-- the longest common subsequence will be empty as well
				result := ""
			elseif string1[i] = string2[j] then
			-- in case 2 last chars of the strings are equal,
			-- the result is (the longest common subsequence of (substrings minus
			-- their last characters)) plus the common character
				result := lcs(string1, string2, i - 1, j - 1)
								+ string1[i].out
			else
			-- in case when the last characters aren't equal,
			-- we select the longest common subsequence of the two strings
			-- which we get by eliminating the last character in both strings
			-- and calculating those cases separately
				result := longest(lcs(string1, string2, i - 1, j),
										lcs(string1, string2, i, j - 1))
			end
		end

	longest_common_subsequence(string1, string2: STRING): STRING
		do
			result := lcs(string1, string2, string1.count, string2.count)
		end

	make
			-- Run application.
		local
			string1, string2: STRING
		do
			print ("Enter 1-st string:%N")
			io.read_line
			string1 := create {STRING}.make_from_string(io.last_string)

			print ("Enter 2-nd string:%N")
			io.read_line
			string2 := create {STRING}.make_from_string(io.last_string)


			print("The longest common subsequence of the string is:%N" +
				longest_common_subsequence(string1, string2))
		end

end
