note
	description: "MergeSort application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	merge_sort_rec(input: ARRAYED_LIST[INTEGER]; start_index, end_index: INTEGER): ARRAYED_LIST[INTEGER]
			-- calculated ascending sorted input using merge sort recursively
		require
			start_is_less_or_equal_end: start_index <= end_index
		local
			left, right: ARRAYED_LIST[INTEGER]
			middle: INTEGER
		do
			if start_index = end_index then
				create result.make(1)
				result.extend(input[start_index])
			else
				middle := ((start_index + end_index) // 2)

				left := merge_sort_rec(input, start_index, middle)
				right := merge_sort_rec(input, middle + 1, end_index)

				result := merge_arrayed_lists(left, right)
			end
		end

	merge_arrayed_lists(left, right: ARRAYED_LIST[INTEGER]): ARRAYED_LIST[INTEGER]
			-- merges left and right array, so that the result is sorted ascending
		local
			i, j,
			left_len, right_len,
			new_len: INTEGER
		do
			left_len := left.count
			right_len := right.count
			new_len := left_len + right_len
			create result.make(new_len)

			-- select the minimal element from
			-- either left or right, until we ran
			-- out of elements from 1 side
			from
				i := 1
				j := 1
			until
				i > left_len
				or else
				j > right_len
			loop
				if left[i] <= right[j] then
					result.extend(left[i])
					i := i + 1
				else
					result.extend(right[j])
					j := j + 1
				end
			end

			-- add elements that left either in left or right
			from
			until
				i > left_len
			loop
				result.extend(left[i])

				i := i + 1
			end

			from
			until
				j > right_len
			loop
				result.extend(right[j])

				j := j + 1
			end
		end

	merge_sort(input: ARRAYED_LIST[INTEGER]): ARRAYED_LIST[INTEGER]
			-- sort ascending input via merge sort and get the result
		do
			result := merge_sort_rec(input, 1, input.count)
		end

	make
			-- Run application.
		local
			temp_collection: LIST[STRING]
			input_collection, sorted_collection: ARRAYED_LIST[INTEGER]
		do
			print("Enter numbers separated by space%N")
			io.read_line
			temp_collection := io.last_string.split(' ')
			create input_collection.make(temp_collection.count)
			across temp_collection as el
			loop
				input_collection.extend(el.item.to_integer)
			end

			sorted_collection := merge_sort(input_collection)
			across sorted_collection as el
			loop
				print(el.item.out + " ")
			end
		end

end
