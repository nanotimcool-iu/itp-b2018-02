note
	description: "Rabbits application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	fibonacci(n: INTEGER): INTEGER
			-- calculate n-th fibonacci number
		require
			positive_n: n > 0
		do
			result := 1

			if n > 2 then
				result := fibonacci(n - 1) + fibonacci(n - 2)
			end
		end

	make
			-- Run application.
		local
			n: INTEGER
		do
			print("Input an integer:%N")
			io.read_integer
			n := io.last_integer
			print("fibonacci(" + n.out + ") = " + fibonacci(n).out)
		end

end
