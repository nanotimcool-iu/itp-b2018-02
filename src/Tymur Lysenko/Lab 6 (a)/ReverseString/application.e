note
	description: "ReverseString application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	reverse_string_rec(input: STRING): STRING
			-- recursively reverse a given string
		do
			if input ~ "" then
				result := ""
			else
				result := input[input.count].out + reverse_string_rec(input.substring(1, input.count - 1))
			end
		end

	reverse_string_iter(input: STRING): STRING
			-- iterativerly reverse a given string
		local
			i: INTEGER
		do
			result := ""

			from
				i := input.count
			until
				i < 1
			loop
				result := result + input[i].out

				i := i - 1
			end
		end

	make
			-- Run application.
		local
			input: STRING
		do
			print("Input a string:%N")
			io.read_line
			input := create {STRING}.make_from_string(io.last_string)
			print("Iteratively reversed string:%N" + reverse_string_iter(input) + "%N")
			print("Recursively reversed string:%N" + reverse_string_rec(input) + "%N")
		end

end
