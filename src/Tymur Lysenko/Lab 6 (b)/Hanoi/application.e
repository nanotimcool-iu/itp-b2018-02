note
	description: "Hanoi application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	solve_hanoi(n: INTEGER; source, target, buffer: CHARACTER)
			-- recursively solve hanoi tower
		require
			non_negative_n:
				n >= 0
			distinct_needles:
				source /= target and then
				source /= buffer and then
				target /= buffer
		do
			if n > 0 then
				solve_hanoi(n - 1, source, buffer, target)
				move_disk(source, target)
				solve_hanoi(n - 1, buffer, target, source)
			end
		end

	move_disk(source, target: CHARACTER)
			-- move top 1 disk from source to target
		do
			print(source.out + " to " + target.out + "%N")
		end

	make
			-- Run application.
		do
			print("Enter a positive number:%N")
			io.read_integer
			solve_hanoi(io.last_integer, 'S', 'T', 'B')
		end

end
