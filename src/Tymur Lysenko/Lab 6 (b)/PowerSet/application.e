note
	description: "PowerSet application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	set_of_sets_to_string(input_set: LINKED_SET[LINKED_SET[INTEGER]]): STRING
			-- get string representation of set of sets
		do
			result := "{ "

			across input_set as int_set
			loop
				result := result + "{ "

				across int_set.item as int
				loop
					result := result + int.item.out + "; "
				end

				if int_set.item.is_empty then
					result := result + " }; "
				else
					result := result.substring(1, result.count - 2) + " }; "
				end
			end

			if input_set.is_empty then
				result := result + " }"
			else
				result := result.substring(1, result.count - 2) + " }"
			end
		end

	power_set(input_set: LINKED_SET[INTEGER]): LINKED_SET[LINKED_SET[INTEGER]]
			-- find all possible subsets of the input_set
		local
			input_set_copy: LINKED_SET[INTEGER]
			current_element: INTEGER
		do
			create result.make

			if input_set.is_empty then
				result.put(create {LINKED_SET[INTEGER]}.make)
			else
				input_set_copy := input_set.twin
				current_element := input_set_copy[1]
				input_set_copy.prune(current_element)

				result.append(power_set(input_set_copy))

				across power_set(input_set_copy) as element
				loop
					element.item.put(current_element)
					result.put(element.item)
				end
			end
		end

	make
			-- Run application.
		local
			string_input_set: LIST[STRING]
			input_set: LINKED_SET[INTEGER]
			result_power_set: LINKED_SET[LINKED_SET[INTEGER]]
		do
			print("Enter input set separated by space:%N")
			io.read_line

			create input_set.make

			string_input_set := io.last_string.split(' ')

			across string_input_set as element
			loop
				if not input_set.has(element.item.to_integer) then
					input_set.extend(element.item.to_integer)
				end
			end

			result_power_set := power_set(input_set)

			print(set_of_sets_to_string(result_power_set))
		end

end
