note
	description: "RobotPath application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			rob: ROBOT
			labyrinth: MAP
			is_solved: BOOLEAN
		do
			create labyrinth.make_from_array(
				<<
					create {POINT}.make(1, 1, true), create {POINT}.make(2, 1, true), create {POINT}.make(3, 1, true), create {POINT}.make(4, 1, false),
					create {POINT}.make(1, 2, false), create {POINT}.make(2, 2, false), create {POINT}.make(3, 2, true), create {POINT}.make(4, 2, false),
					create {POINT}.make(1, 3, true), create {POINT}.make(2, 3, false), create {POINT}.make(3, 3, true), create {POINT}.make(4, 3, false),
					create {POINT}.make(1, 4, true), create {POINT}.make(2, 4, true), create {POINT}.make(3, 4, true), create {POINT}.make(4, 4, false),
					create {POINT}.make(1, 5, true), create {POINT}.make(2, 5, true), create {POINT}.make(3, 5, true), create {POINT}.make(4, 5, true)
				>>)
			create rob.make(labyrinth)

			print(rob.to_string)
			is_solved := rob.solve

			if is_solved then
				print("%N" + rob.to_string)
			else
				print("%NCan't solve!")
			end
		end

end
