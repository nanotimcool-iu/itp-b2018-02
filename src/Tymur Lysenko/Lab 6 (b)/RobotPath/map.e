note
	description: "Summary description for {MAP}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MAP

create
	make,
	make_from_array

feature {NONE} -- Initialization

	make(num_rows, num_columns: INTEGER)
		require
			positive_rows: num_rows > 0
			positive_columns: num_columns < 0
		local
			i, j: INTEGER
		do
			create rect_field.make_filled(create {POINT}.make(0, 0, TRUE), 1, num_rows * num_columns)
			rows := num_rows
			columns := num_columns
			total_points := rows + columns

			from
				i := 1
			until
				i > rows
			loop
				from
					j := 1
				until
					j > columns
				loop
					rect_field.put(create {POINT}.make(j, i, TRUE), i * j)

					j := j + 1
				end

				i := i + 1
			end

			ensure
				positive_rows: rows > 0
				positive_columns: columns > 0
				non_empty_field: not rect_field.is_empty
				field_is_rect: is_rect(rect_field)
				correct_total: total_points = rect_field.count
		end

	make_from_array(in_array: ARRAY[POINT])
		require
			not_empty: not in_array.is_empty
			is_rect: is_rect(in_array)
		local
			last_element: POINT
		do
			total_points := in_array.count

			last_element := in_array[total_points]

			rows := last_element.y
			columns := last_element.x

			rect_field := in_array
		end

feature -- Commands

	set_can_move_for_point(x, y: INTEGER; can_move: BOOLEAN)
		require
			has_point: has_point(x, y)
		do
			get_point_by_coordinates(x, y).can_move_here := can_move
		end

feature -- Queries

	get_point_by_coordinates(x, y: INTEGER): POINT
			-- returns point by coordinates
		require
			has_point: has_point(x, y)
		local
			i: INTEGER
			is_found: BOOLEAN
		do
			is_found := false

			from
				i := 0
			until
				is_found
			loop
				i := i + 1

				is_found := rect_field[i].is_same_point(x, y)
			end

			result := rect_field[i]
		end

feature -- contract functions

	is_rect(in_array: ARRAY[POINT]): BOOLEAN
			-- validate if input array of points is rectangle
		require
			not_empty: not in_array.is_empty
		local
--			i, len: INTEGER
--			previous_element: POINT
		do
			-- TODO: implement validation later
			result := true
		end

	has_point(x, y: INTEGER): BOOLEAN
		local
			i: INTEGER
		do
			result := false

			from
				i := 1
			until
				i > total_points
				or else
				result
			loop
				if rect_field[i].is_same_point(x, y) then
					result := true
				end

				i := i + 1
			end
		end

feature -- Attributes

	rect_field: ARRAY[POINT]
	rows, columns, total_points: INTEGER

end
