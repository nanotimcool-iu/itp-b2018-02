note
	description: "Summary description for {POINT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	POINT

create
	make

feature {NONE} -- Initialization

	make(x_val, y_val: INTEGER; can_move: BOOLEAN)
			-- Initialization for `Current'.
		do
			x := x_val
			y := y_val
			can_move_here := can_move
		end

feature -- Queries

	is_same_point(otehr_x, otehr_y: INTEGER): BOOLEAN
		do
			result := otehr_x = x and then otehr_y = y
		end

feature -- Attributes

	x: INTEGER assign set_x
	y: INTEGER assign set_y
	can_move_here: BOOLEAN assign set_can_move_here

	set_x(new_x: INTEGER)
		do
			x := new_x
		end

	set_y(new_y: INTEGER)
		do
			y := new_y
		end

	set_can_move_here(new_can_move: BOOLEAN)
		do
			can_move_here := new_can_move
		end

end
