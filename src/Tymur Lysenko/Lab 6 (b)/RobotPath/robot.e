note
	description: "Summary description for {ROBOT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ROBOT

create
	make

feature {NONE} -- Initialization

	make(start_labyrinth: MAP)
			-- Initialization for 'Current'.
		do
			labyrinth := start_labyrinth
			position := labyrinth.get_point_by_coordinates(1, 1).twin
			end_point := labyrinth.get_point_by_coordinates(labyrinth.columns, labyrinth.rows)
			create path.make(labyrinth.columns + labyrinth.rows)
			create visited.make
		end

feature -- Commands

	solve: BOOLEAN
			-- find a path from;
			-- returns in-ordered list of points, which represent the path
		do
			if position.is_same_point(end_point.x, end_point.y) then
				path.extend(end_point)
				result := true
			else
				visited.put(position)

				if can_move_to(position.x + 1, position.y)
					and then not visited.has(labyrinth.get_point_by_coordinates(position.x + 1, position.y)) then

					position.x := position.x + 1
					path.extend(position.twin)

					if solve then
						result := true
					else
						path.prune(get_path_element_by_coordinates(position.x, position.y))
						position.x := position.x - 1
						result := false
					end

				else
					result := false
				end

				if not result then
					if can_move_to(position.x, position.y + 1)
						and then not visited.has(labyrinth.get_point_by_coordinates(position.x, position.y + 1)) then

						position.y := position.y + 1
						path.extend(position.twin)

						if solve then
							result := true
						else
							path.prune(get_path_element_by_coordinates(position.x, position.y))
							position.y := position.y - 1
							result := false
						end

					else
						result := false
					end
				end
			end
		end

	can_move_to(x, y: INTEGER): BOOLEAN
		do
			result := labyrinth.has_point(x, y) and then labyrinth.get_point_by_coordinates(x, y).can_move_here
		end

	to_string: STRING
			-- get string representation of the current state
		local
			i, j: INTEGER
		do
			result := ""

			from
				i := 1
			until
				i > labyrinth.rows
			loop
				from
					j := 1
				until
					j > labyrinth.columns
				loop
					if path_contains(j, i) then
						result := result + "@ "
					elseif not labyrinth.get_point_by_coordinates(j, i).can_move_here then
						result := result + "X "
					else
						result := result + ". "
					end

					j := j + 1
				end

				result := result + "%N"
				i := i + 1
			end
		end

	path_contains(x, y: INTEGER): BOOLEAN
			-- returns true if path contains such point
		local
			i, path_size: INTEGER
		do
			result := false
			path_size := path.count

			from
				i := 1
			until
				i > path_size
				or else
				result
			loop
				if path[i].is_same_point(x, y) then
					result := true
				end

				i := i + 1
			end
		end

	get_path_element_by_coordinates(x, y: INTEGER): POINT
			-- returns path element by coordinates
		require
			has_element: path_contains(x, y)
		local
			i, path_size: INTEGER
			is_found: BOOLEAN
		do
			is_found := false
			path_size := path.count

			from
				i := 1
			until
				i > path_size
				or else
				is_found
			loop
				if path[i].is_same_point(x, y) then
					is_found := true
				end

				i := i + 1
			end

			result := path[i]
		end

feature -- Attributes

	position, end_point: POINT
	labyrinth: MAP

	path: ARRAYED_LIST[POINT]
	visited: LINKED_SET[POINT]

end
