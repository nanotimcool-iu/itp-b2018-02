note
	description: "HangmanGame application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			game: HANGMAN_GAME
		do
			create game.init
			game.play
		end

end
