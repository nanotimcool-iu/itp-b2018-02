note
	description: "Summary description for {HANGMAN_GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

frozen class
	HANGMAN_GAME

inherit
	EXECUTION_ENVIRONMENT

create
	init

feature {NONE} -- Initialization

	init
		do
			is_finished := false
			players := get_players
			system("CLS")
			max_guesses := get_max_guesses
			guesses_left := max_guesses
			system("CLS")
			game_word := get_word(players.count)
		end

feature -- Open commands

	play
			-- main game logic
		local
			i, num_players: INTEGER
		do
			num_players := players.count

			from
				i := 1
			until
				is_finished
			loop
				print("Attempts left: " + guesses_left.out + "%N")
				print(game_word.cur_state_repr + "%N")
				print(players[i].name + ", enter a character:%N")
				io.read_line

				if game_word.guess(io.last_string[1].as_lower) then
					if game_word.is_guessed then
						is_finished := true
						print(players[i].name + " IS A WINNER!")
					end
				else
					guesses_left := guesses_left - 1

					if guesses_left = 0 then
						is_finished := true
						print("THERE IS NO WINNER!")
					end

					if i >= num_players then
						i := 1
					else
						i := i + 1
					end
				end
			end
		end

feature {NONE} -- Closed queries

	prompt_for_int_greater_than_restriction(prompt_str: STRING; restriction: INTEGER): INTEGER
			-- prompts user to enter a number, that is greater than restriction, until
			-- (s)he enters a valid one
		local
			is_num_players_correct: BOOLEAN
		do
			is_num_players_correct := false

			from
			until
				is_num_players_correct
			loop
				print(prompt_str + "%N")
				io.read_integer
				result := io.last_integer

				is_num_players_correct := result > restriction
			end

			ensure
				result_g_restriction: result > restriction
		end

	get_players: ARRAYED_LIST[PLAYER]
			-- prompts user to enter players
		local
			num_players, i: INTEGER
		do
			create result.make(1)

			num_players := prompt_for_int_greater_than_restriction("Enter number of players (> 0):", 0)

			from
				i := 1
			until
				i > num_players
			loop
				print("Enter name for player " + i.out + ":%N")
				io.read_line
				result.extend(create {PLAYER}.make(io.last_string.twin))

				i := i + 1
			end

			ensure
				not result.is_empty
		end

	get_max_guesses: INTEGER
			-- prompts user to input maximum number of guesses
		do
			result := prompt_for_int_greater_than_restriction("Enter maximum number of guesses (>= 3):", 2)

			ensure
				result > 2
		end

	get_random_number_in_range(rng_start, rng_end: INTEGER): INTEGER
			-- returns a random number
		local
			r: RANDOM
			cur_date_time: DATE_TIME
			seed: INTEGER
		do
			cur_date_time := create {DATE_TIME}.make_now
			seed := cur_date_time.year + cur_date_time.month +
				cur_date_time.day + cur_date_time.hour +
				cur_date_time.minute + cur_date_time.second
			create r.set_seed(seed)
			r.start

			result := (r.item \\ rng_end) + rng_start
		end

	get_word(num_players: INTEGER): HANGMAN_WORD
		require
			num_players > 0
		local
			input_file: PLAIN_TEXT_FILE
			qualafied_words: ARRAYED_LIST[STRING]
		do
			create input_file.make_open_read("words_db.txt")
			create qualafied_words.make(200)

			from
				input_file.read_line
			until
				input_file.exhausted
			loop
				if input_file.last_string.count >= num_players then
					qualafied_words.extend(create {STRING}.make_from_string(input_file.last_string))
				end

				input_file.read_line
			end

			create result.make(qualafied_words[get_random_number_in_range(1, qualafied_words.count)])

			input_file.close

			ensure
				word_len_ge_num_part: result.current_state.count >= num_players
		end

feature {NONE} -- Closed attributes

	players: ARRAYED_LIST[PLAYER]
	max_guesses, guesses_left: INTEGER
	game_word: HANGMAN_WORD
	is_finished: BOOLEAN

end
