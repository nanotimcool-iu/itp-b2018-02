note
	description: "Summary description for {HANGMAN_WORD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HANGMAN_WORD

create
	make

feature {NONE} -- Initialization

	make(word_to_guess: STRING)
			-- Initialization for `Current'.
		do
			word_to_guess.to_lower
			word := word_to_guess
			create current_state.make_filled('_', word.count)
			is_guessed := false
		end

feature -- Open queries

	guess(char: CHARACTER): BOOLEAN
			-- if char is found within word, replace _ in current_state with the char
			-- and return true, otherwise - just return false
		local
			i, num_chars: INTEGER
		do
			result := false

			from
				i := 1
				num_chars := word.count
			until
				i > num_chars
			loop
				if word[i] = char then
					current_state[i] := char
					result := true
				end

				i := i + 1
			end

			is_guessed := not current_state.has('_')
		end

	cur_state_repr: STRING
			-- get ready-to-print representation of the current_state
		do
			result := ""

			across current_state as cur_char
			loop
				result := result + cur_char.item.out + " "
			end

			result := result + "%N"
		end

feature -- Open attributes

	current_state: STRING
	is_guessed: BOOLEAN

feature {NONE} -- Open attributes

	word: STRING

end
