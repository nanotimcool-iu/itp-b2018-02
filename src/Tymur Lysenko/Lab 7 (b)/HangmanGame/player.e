note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER

create make

feature

	make(player_name: STRING)
		do
			name := player_name
		end

feature

	name: STRING

end
