note
	description: "BinTree application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			tree: BIN_TREE[INTEGER]
		do
			create tree.make(10)
			tree.add_left(create {BIN_TREE[INTEGER]}.make(5))

			if attached tree.left as tl then
				tl.add_left(create {BIN_TREE[INTEGER]}.make(2))
				tl.add_right(create {BIN_TREE[INTEGER]}.make(3))
			end
			
			tree.add_right(create {BIN_TREE[INTEGER]}.make(15))

			print(tree.height)
		end

end
