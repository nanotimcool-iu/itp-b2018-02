note
	description: "Summary description for {BIN_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIN_TREE[G]

create
	make

feature {NONE} -- Initialization

	make(data: G)
			-- Initialization for `Current'.
		do
			info := data
		end

feature -- Open commands

	add_left(t: BIN_TREE[G])
			-- assign left to t
		do
			left := t
		end

	add_right(t: BIN_TREE[G])
			-- assign right to t
		do
			right := t
		end

feature -- Open queries

	height: INTEGER
			-- calculate binary tree height
		local
			left_count, right_count: INTEGER
		do
			left_count := 0
			right_count := 0

			if attached left as la then
				left_count := la.height
			end

			if attached right as ra then
				right_count := ra.height
			end

			result := 1 + max(left_count, right_count)
		end

feature -- Open attributes

	info: G
	left, right: detachable BIN_TREE[G]

feature {NONE} -- Closed queries

	max(left_val, right_val: INTEGER): INTEGER
		do
			if left_val > right_val then
				result := left_val
			else
				result := right_val
			end
		end

end
