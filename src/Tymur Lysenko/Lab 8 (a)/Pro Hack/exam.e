note
	description: "Summary description for {EXAM}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EXAM

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			task := ""
			answer := ""
		end


feature -- Open attributes

	grade: detachable STUDENT_GRADE --assign set_grade
	task: STRING assign set_task
	answer: STRING assign set_answer

feature {STUDENT}

	set_answer(new_answer: STRING)
		do
			answer := new_answer
		end

feature {PROFESSOR}

	set_grade(new_grade: STUDENT_GRADE)
		do
			grade := new_grade
		end

	set_task(new_task: STRING)
		do
			task := new_task
		end

end
