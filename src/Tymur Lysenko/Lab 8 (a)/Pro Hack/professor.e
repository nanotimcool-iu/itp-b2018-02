note
	description: "Summary description for {PROFESSOR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROFESSOR

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for 'Current'.
		do
			create student_grades.make(0)
			create exams.make(0)
		end

feature -- Open queries

	get_grades: ARRAYED_LIST[STUDENT_GRADE]
		do
			result := student_grades.twin
		end

	produce_exam
		local
			ex: EXAM
		do
			create ex.make
			ex.task := "Some task"

			exams.extend(ex)
		end

	get_last_exam: EXAM
		do
			result := exams.last
		end

	check_exam(ex: EXAM; stud: STUDENT)
		do
			if ex.answer = "Some answer" then
				ex.set_grade(create {STUDENT_GRADE}.make(stud, 100))
			else
				ex.set_grade(create {STUDENT_GRADE}.make(stud, 0))
			end

			if attached ex.grade as a_grade then
				student_grades.extend(a_grade)
			end
		end

feature {PROFESSOR}

	student_grades: ARRAYED_LIST[STUDENT_GRADE]
	exams: ARRAYED_LIST[EXAM]

end
