note
	description: "Summary description for {STUDENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	STUDENT

create
	make

feature {NONE} -- Initialization

	make
		local
			grade:STUDENT_GRADE
		do
			create grade.make (Current, 1000)
		end

	solve1(exam_to_solve: EXAM)
		do
			exam_to_solve.answer := "Blablabla"
		end

	solve2(exam_to_solve: EXAM)
		do
			exam_to_solve.answer := "Some answer"
		end

end
