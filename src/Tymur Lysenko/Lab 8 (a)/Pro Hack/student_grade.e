note
	description: "Summary description for {STUDENT_GRADE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	STUDENT_GRADE

create
	make

feature {NONE} -- Initialization

	make(stud: STUDENT; grade_value: INTEGER)
			-- Initialization for `Current'.
		do
			student := stud
			grade := grade_value
		end

feature -- Open attributes

	student: STUDENT
	grade: INTEGER assign set_grade

feature {PROFESSOR}

	set_grade(new_grade: INTEGER)
		do
			grade := new_grade
		end

end
