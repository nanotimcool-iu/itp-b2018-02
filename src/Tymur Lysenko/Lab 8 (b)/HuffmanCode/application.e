note
	description: "HuffmanCode application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	get_byte_repr(data: STRING): STRING
		local
			hex_str: STRING
			cur_char: CHARACTER
			i: INTEGER
		do
			result := ""

			across data as char
			loop
				hex_str := char.item.code.to_hex_string

				result := result + hex_str + " "
			end
		end

	make
			-- Run application.
		local
			huff_code: HUFFMAN_CODE
			data: STRING
		do
			print("Enter a string:%N")
			io.read_line
			data := io.last_string.twin
			create huff_code.make(data)

			print(data + "%N")
			print((data.count * 8).out  + " bits%N")

			print(get_byte_repr(data) + "%N")


			print(huff_code.encoded_data + "%N")
			print((huff_code.encoded_data.count).out + " bits%N")

			print(huff_code.get_decoded_data)
		end

end
