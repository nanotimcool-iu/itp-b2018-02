note
	description: "Summary description for {HUFFMAN_CODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN_CODE

create
	make

feature {NONE} -- Initialization

	make(data: STRING)
			-- Initialization for 'Current'.
		require
			not_empty: not data.is_empty
			no_null_terminator: not data.has('%U')
		do
			set_data(data)
		end

feature -- Open queries

	get_decoded_data: STRING
		local
			cur_parse_seq: STRING
		do
			result := ""
			cur_parse_seq := ""


			across encoded_data as char
			loop
				cur_parse_seq := cur_parse_seq + char.item.out

				if code_mapping.has_code(cur_parse_seq) then
					result := result + code_mapping.get_char_by_code(cur_parse_seq).out
					cur_parse_seq := ""
				end
			end
		end

feature -- Open commands

	set_data(data: STRING)
		require
				not_empty: not data.is_empty
				no_null_terminator: not data.has('%U')
		do
			huffman_encode(data)
		end

feature -- Open attributes

	encoded_data: STRING

feature {NONE} -- Closed commands

	huffman_encode(data: STRING)
		require
			not_empty: not data.is_empty
			no_null_terminator: not data.has('%U')
		do
			create code_mapping.make(data)
			encoded_data := ""

			across data as char
			loop
				encoded_data := encoded_data + code_mapping.get_code_by_char(char.item)
			end
		end

feature {NONE} -- Closed attributes

	code_mapping: HUFFMAN_MAPPING

end
