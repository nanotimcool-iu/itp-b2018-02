note
	description: "Summary description for {HUFFMAN_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN_MAPPING

create
	make

feature {NONE} -- Initialization

	make(data: STRING)
			-- Initialization for 'Current'.
		require
			not_empty: not data.is_empty
			no_null_terminator: not data.has('%U')
		local
			tree: HUFFMAN_TREE
		do
			create tree.make(get_chars_with_freq(data))
			char_code_mapping := tree.get_paths
--			char_code_mapping.object_comparison := true
--			char_code_mapping.compare_references := false
		end

feature -- Open queries

	get_code_by_char(c: CHARACTER): STRING
		require
			char_is_in_tree: has_char(c)
		do
			if attached char_code_mapping[c] as res then
				result := res
			else
				result := ""
			end
		end

	has_char(c: CHARACTER): BOOLEAN
		do
			result := char_code_mapping.has(c)
		end

	get_char_by_code(code: STRING): CHARACTER
		require
			code_is_in_tree: has_code(code)
		local
			is_found: BOOLEAN
			cur_code: STRING
		do
			is_found := false

			from
				char_code_mapping.start
			until
				is_found
			loop
				if char_code_mapping.at(char_code_mapping.key_for_iteration) ~ code then
					is_found := true
					result := char_code_mapping.key_for_iteration
				end

				char_code_mapping.forth
			end
		end

	has_code(code: STRING): BOOLEAN
		local
			code_list: ARRAYED_LIST[STRING]
			i: INTEGER
		do
			result := false
			code_list := char_code_mapping.linear_representation

			from
				i := 1
			until
				i > code_list.count or result
			loop
				if code.is_equal(code_list[i]) then
					result := true
				end

				i := i + 1
			end
		end

feature {HUFFMAN_TREE} -- Closed queries

	get_chars_with_freq(data: STRING): HEAP_PRIORITY_QUEUE[HUFFMAN_TREE_NODE]
		require
			not_empty: not data.is_empty
			no_null_terminator: not data.has('%U')
		do
			create result.make(23)

			across get_distinct_chars(data) as char
			loop
				result.put(
					create {HUFFMAN_TREE_NODE}.make(
						[char.item, data.occurrences(char.item)]
					)
				)
			end

		ensure
			not_empty_result: not result.is_empty
		end

	get_distinct_chars(data: STRING): LINKED_SET[CHARACTER]
		require
			not_empty: not data.is_empty
			no_null_terminator: not data.has('%U')
		do
			create result.make

			across data as char
			loop
				result.put(char.item)
			end

		ensure
			not_empty_result: not result.is_empty
		end

feature {NONE} -- Closed attributes

	char_code_mapping: HASH_TABLE[STRING, CHARACTER]

end
