note
	description: "Summary description for {HUFFMAN_TREE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN_TREE

create
	make

feature {NONE} -- Initialization

	make(leaf_nodes_queue: HEAP_PRIORITY_QUEUE[HUFFMAN_TREE_NODE])
		local
			leaf_nodes: ARRAYED_LIST[HUFFMAN_TREE_NODE]
			first_lowest, second_lowest, temp_node: HUFFMAN_TREE_NODE
			i: INTEGER
		do
			leaf_nodes := leaf_nodes_queue.linear_representation

			if leaf_nodes.count = 1 then
				create root.make(['%U', leaf_nodes[0].val.num_occur])
				root.left := leaf_nodes[0]
			else
				from
				until
					leaf_nodes.count = 1
				loop
					first_lowest := leaf_nodes[1]
					from
						i := 2
					until
						i > leaf_nodes.count
					loop
						if first_lowest.val.num_occur > leaf_nodes[i].val.num_occur then
							first_lowest := leaf_nodes[i]
						end

						i := i + 1
					end
					leaf_nodes.prune_all(first_lowest)

					second_lowest := leaf_nodes[1]
					from
						i := 2
					until
						i > leaf_nodes.count
					loop
						if second_lowest.val.num_occur >= leaf_nodes[i].val.num_occur
						and leaf_nodes[i] /= first_lowest then
							second_lowest := leaf_nodes[i]
						end

						i := i + 1
					end

					if second_lowest /= first_lowest then
						create temp_node.make(['%U', first_lowest.val.num_occur + second_lowest.val.num_occur])
						temp_node.left := first_lowest
						temp_node.right := second_lowest
						leaf_nodes.extend(temp_node)
						leaf_nodes.prune_all(second_lowest)
					end
				end
			end

			root := leaf_nodes[1]
		end

feature -- Open queries

	get_paths: HASH_TABLE[STRING, CHARACTER]
		local
			nodes_to_visit: LINKED_QUEUE[TUPLE[node: HUFFMAN_TREE_NODE; path: STRING]]
			current_node: TUPLE[node: HUFFMAN_TREE_NODE; path: STRING]
		do
			create result.make(23)
			create nodes_to_visit.make
			nodes_to_visit.put([root, ""])

			from
			until
				nodes_to_visit.is_empty
			loop
				current_node := nodes_to_visit.item
				nodes_to_visit.remove

				if attached current_node.node.left as l then
					nodes_to_visit.put([l, current_node.path + "0"])
				end

				if attached current_node.node.right as r then
					nodes_to_visit.put([r, current_node.path + "1"])
				end

				if current_node.node.val.char /= '%U' then
					result.extend(current_node.path, current_node.node.val.char)
				end
			end
		end

feature {NONE} -- Closed attributes

	root: HUFFMAN_TREE_NODE

end
