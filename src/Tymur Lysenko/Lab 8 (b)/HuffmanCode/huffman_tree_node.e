note
	description: "Summary description for {HUFFMAN_TREE_NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUFFMAN_TREE_NODE

inherit
	COMPARABLE

create
	make

feature {NONE} -- Initialization

	make(value: TUPLE[char: CHARACTER; num_occur: INTEGER])
			-- Initialization for `Current'.
		do
			val := value
		end

feature -- Open queries

	is_less alias "<" (other: like Current): BOOLEAN
				-- Is current object less than 'other'?
		do
			result := val.num_occur > other.val.num_occur
		ensure then
			asymmetric: Result implies not (other < Current)
		end

feature -- Open attributes

	val: TUPLE[char: CHARACTER; num_occur: INTEGER]
	left: detachable HUFFMAN_TREE_NODE assign set_left
	right: detachable HUFFMAN_TREE_NODE assign set_right

	set_left(new_left: detachable HUFFMAN_TREE_NODE)
		do
			left := new_left
		end

	set_right(new_right: detachable HUFFMAN_TREE_NODE)
		do
			right := new_right
		end


end
