note
	description: "Ciphers application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			data: STRING
			comb_cipher: COMBINED_CIPHER
		do
			-- YQLRFYNTTNSDSYCN
			create comb_cipher.make("YQLRFYNTTNSDSYCN")
			data := "LAGHYH QSCUJRKS.IRNTEFRPRXMOIY CGAEYXWYGNENLRF   FAZ2/TGBZKFIREE.RWD Y LU! LMK SLXG.E./PMAWUHWHCGYZGYFHEIWG QUG RSS.XUVIULZIE KFYGEL//:RLH DXE Z"
			print(comb_cipher.decrypt(data))
		end

end
