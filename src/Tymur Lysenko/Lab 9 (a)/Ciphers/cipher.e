note
	description: "Summary description for {CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	CIPHER

feature -- Open queries

	encrypt(data: STRING): STRING
		deferred
		end

	decrypt(data: STRING): STRING
		deferred
		end

end
