note
	description: "Summary description for {CIPHER_WITH_KEY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	CIPHER_WITH_KEY

inherit
	CIPHER

feature -- Open queries

	set_key(new_key: STRING)
		require
			not_empty: not new_key.is_empty
			only_alphabetsymbs: has_only_letters(new_key)
		do
			key := new_key.as_upper
		end

	has_only_letters(str: STRING): BOOLEAN
		local
			i: INTEGER
		do
			result := true

			from
				i := 1
			until
				i > str.count
				or else
				not result
			loop
				if not str[i].is_alpha then
					result := false
				end

				i := i + 1
			end
		end

feature -- Open attributes

	key: STRING assign set_key

end
