note
	description: "Summary description for {COMBINED_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMBINED_CIPHER

inherit
	CIPHER_WITH_KEY
		redefine
			set_key
		end

create
	make

feature {NONE} -- Initialization

	make(init_key: STRING)
			-- Initialization for 'Current'.
		do
			key := init_key
			create vig_ciph.make(key)
			create spi_ciph.make
		end

feature -- Open queries

	encrypt(data: STRING): STRING
		do
			result := spi_ciph.encrypt(vig_ciph.encrypt(data))
		end

	decrypt(data: STRING): STRING
		do
			result := vig_ciph.decrypt(spi_ciph.decrypt(data))
		end

feature -- Open commands

	set_key(new_key: STRING)
		do
			key := new_key
			vig_ciph.set_key(key)
		end

feature {NONE} -- Closed attributes

	vig_ciph: VIGENERE_CIPHER
	spi_ciph: SPIRAL_CIPHER

end
