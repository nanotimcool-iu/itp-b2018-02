note
	description: "Summary description for {SPIRAL_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SPIRAL_CIPHER

inherit
	CIPHER

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for 'Current'.
		do

		end

feature

	encrypt(data: STRING): STRING
		local
			square_len,
			i, j, char_iter,
			i_inc, j_inc,
			ver_left_bound, ver_right_bound,
			hor_upper_bound, hor_lower_bound: INTEGER
			matrix: ARRAY2[CHARACTER]
		do
			result := ""

			-- Create a square matrix from data & fill it
			square_len := sing_mat.sqrt(data.count.to_real).ceiling
			create matrix.make_filled('%U', square_len, square_len)

			from
				i := 1
				char_iter := 1
			until
				i > matrix.height
			loop
				from
					j := 1
				until
					j > matrix.width
				loop
					if char_iter > data.count then
						matrix[i, j] := ' '
					else
						matrix[i, j] := data[char_iter]
						char_iter := char_iter + 1
					end


					j := j + 1
				end

				i := i + 1
			end


			-- Traverse the matrix clockwise starting from top right corner
			from
				ver_left_bound  := 1
				ver_right_bound := matrix.width
				hor_upper_bound := 1
				hor_lower_bound := matrix.height
				i := 1
				j := matrix.width
				i_inc := 1
				j_inc := -1
			invariant
				(i_inc = 1 or else i_inc = -1)
				and then
				(j_inc = 1 or else j_inc = -1)
			until
				result.count = matrix.count
			loop
				from
				until
					i < hor_upper_bound
					or else
					i > hor_lower_bound
				loop
					result := result + matrix[i, j].out

					i := i + i_inc
				end
				if i_inc > 0 then
					ver_right_bound := ver_right_bound - 1
				else
					ver_left_bound := ver_left_bound + 1
				end
				i_inc := -i_inc
				i := i + i_inc
				j := j + j_inc

				from
				until
					j > ver_right_bound
					or else
					j < ver_left_bound
				loop
					result := result + matrix[i, j].out

					j := j + j_inc
				end
				if j_inc > 0 then
					hor_upper_bound := hor_upper_bound + 1
				else
					hor_lower_bound := hor_lower_bound - 1
				end

				j_inc := -j_inc
				i := i + i_inc
				j := j + j_inc
			end
		end

	decrypt(data: STRING): STRING
		require else
			size_is_square_root: sing_mat.sqrt(data.count.to_real).ceiling
			=
			sing_mat.sqrt(data.count.to_real).floor
		local
			square_len,
			i, j, char_iter,
			i_inc, j_inc,
			ver_left_bound, ver_right_bound,
			hor_upper_bound, hor_lower_bound: INTEGER
			matrix: ARRAY2[CHARACTER]
		do
			result := ""
			square_len := sing_mat.sqrt(data.count.to_real).ceiling
			create matrix.make_filled('%U', square_len, square_len)

			-- Build the matrix of the string
			from
				ver_left_bound  := 1
				ver_right_bound := matrix.width
				hor_upper_bound := 1
				hor_lower_bound := matrix.height
				i := 1
				j := matrix.width
				i_inc := 1
				j_inc := -1
				char_iter := 1
			invariant
				(i_inc = 1 or else i_inc = -1)
				and then
				(j_inc = 1 or else j_inc = -1)
			until
				char_iter > data.count
			loop
				from
				until
					i < hor_upper_bound
					or else
					i > hor_lower_bound
				loop
					matrix[i, j] := data[char_iter]

					i := i + i_inc
					char_iter := char_iter + 1
				end
				if i_inc > 0 then
					ver_right_bound := ver_right_bound - 1
				else
					ver_left_bound := ver_left_bound + 1
				end
				i_inc := -i_inc
				i := i + i_inc
				j := j + j_inc

				from
				until
					j > ver_right_bound
					or else
					j < ver_left_bound
				loop
					matrix[i, j] := data[char_iter]

					j := j + j_inc
					char_iter := char_iter + 1
				end
				if j_inc > 0 then
					hor_upper_bound := hor_upper_bound + 1
				else
					hor_lower_bound := hor_lower_bound - 1
				end

				j_inc := -j_inc
				i := i + i_inc
				j := j + j_inc
			end

			from
				i := 1
			until
				i > matrix.height
			loop
				from
					j := 1
				until
					j > matrix.width
				loop
					result := result + matrix[i, j].out

					j := j + 1
				end

				i := i + 1
			end
		end

feature -- Closed attributes

	sing_mat: SINGLE_MATH
		once
			create result
		end

end
