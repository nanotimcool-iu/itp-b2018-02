note
	description: "Summary description for {VIGENERE_CIPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIGENERE_CIPHER

inherit
	CIPHER_WITH_KEY

create
	make

feature {NONE} -- Initialization

	make(init_key: STRING)
			-- Initialization for 'Current'.
		do
			set_key(init_key)
		end

feature -- Open queries

	encrypt(data: STRING): STRING
	local
		normalized_key, upper_data: STRING
		i, char_inner_code: INTEGER
	do
		result := ""
		upper_data := data.as_upper
		normalized_key := get_normalized_key(data)

		from
			i := 1
		until
			i > upper_data.count
		loop
			if normalized_key[i].is_alpha then
				char_inner_code := (char_code_map[upper_data[i]] + char_code_map[normalized_key[i]]) \\ 26
				result := result + code_char_map[char_inner_code].out
			else
				result := result + upper_data[i].out
			end

			i := i + 1
		end
	end

	decrypt(data: STRING): STRING
	local
		normalized_key, upper_data: STRING
		i, char_inner_code, data_code, key_code, diff: INTEGER
	do
		result := ""
		upper_data := data.as_upper
		normalized_key := get_normalized_key(data)

		from
			i := 1
		until
			i > upper_data.count
		loop
			if normalized_key[i].is_alpha then
				data_code := char_code_map[upper_data[i]]
				key_code := char_code_map[normalized_key[i]]
				diff := data_code - key_code

				if diff < 0 then
					char_inner_code := 26 + diff
				else
					char_inner_code := diff
				end

				result := result + code_char_map[char_inner_code].out
			else
				result := result + upper_data[i].out
			end

			i := i + 1
		end
	end

feature {NONE} -- Closed queries

	get_normalized_key(data: STRING): STRING
		local
			i: INTEGER
		do
			result := ""
			i := 1

			across data as char
			loop
				if char.item.is_alpha then
					if i > key.count then
						i := 1
					end

					result := result + key[i].out

					i := i + 1
				else
					result := result + char.item.out
				end
			end

		ensure
			same_size_as_data: result.count = data.count
		end

feature {NONE} -- Closed attributes

	char_code_map: HASH_TABLE[INTEGER, CHARACTER]
		once
			create result.make(26)

			char_code_map.put(0, 'A')
			char_code_map.put(1, 'B')
			char_code_map.put(2, 'C')
			char_code_map.put(3, 'D')
			char_code_map.put(4, 'E')
			char_code_map.put(5, 'F')
			char_code_map.put(6, 'G')
			char_code_map.put(7, 'H')
			char_code_map.put(8, 'I')
			char_code_map.put(9, 'J')
			char_code_map.put(10, 'K')
			char_code_map.put(11, 'L')
			char_code_map.put(12, 'M')
			char_code_map.put(13, 'N')
			char_code_map.put(14, 'O')
			char_code_map.put(15, 'P')
			char_code_map.put(16, 'Q')
			char_code_map.put(17, 'R')
			char_code_map.put(18, 'S')
			char_code_map.put(19, 'T')
			char_code_map.put(20, 'U')
			char_code_map.put(21, 'V')
			char_code_map.put(22, 'W')
			char_code_map.put(23, 'X')
			char_code_map.put(24, 'Y')
			char_code_map.put(25, 'Z')
		end

	code_char_map: HASH_TABLE[CHARACTER, INTEGER]
		once
			create result.make(26)
			code_char_map.put('A', 0)
			code_char_map.put('B', 1)
			code_char_map.put('C', 2)
			code_char_map.put('D', 3)
			code_char_map.put('E', 4)
			code_char_map.put('F', 5)
			code_char_map.put('G', 6)
			code_char_map.put('H', 7)
			code_char_map.put('I', 8)
			code_char_map.put('J', 9)
			code_char_map.put('K', 10)
			code_char_map.put('L', 11)
			code_char_map.put('M', 12)
			code_char_map.put('N', 13)
			code_char_map.put('O', 14)
			code_char_map.put('P', 15)
			code_char_map.put('Q', 16)
			code_char_map.put('R', 17)
			code_char_map.put('S', 18)
			code_char_map.put('T', 19)
			code_char_map.put('U', 20)
			code_char_map.put('V', 21)
			code_char_map.put('W', 22)
			code_char_map.put('X', 23)
			code_char_map.put('Y', 24)
			code_char_map.put('Z', 25)
		end

feature {NONE} -- Initialization routines

--	setup
--		do
--			create char_code_map.make(26)
--			char_code_map.put(0, 'A')
--			char_code_map.put(1, 'B')
--			char_code_map.put(2, 'C')
--			char_code_map.put(3, 'D')
--			char_code_map.put(4, 'E')
--			char_code_map.put(5, 'F')
--			char_code_map.put(6, 'G')
--			char_code_map.put(7, 'H')
--			char_code_map.put(8, 'I')
--			char_code_map.put(9, 'J')
--			char_code_map.put(10, 'K')
--			char_code_map.put(11, 'L')
--			char_code_map.put(12, 'M')
--			char_code_map.put(13, 'N')
--			char_code_map.put(14, 'O')
--			char_code_map.put(15, 'P')
--			char_code_map.put(16, 'Q')
--			char_code_map.put(17, 'R')
--			char_code_map.put(18, 'S')
--			char_code_map.put(19, 'T')
--			char_code_map.put(20, 'U')
--			char_code_map.put(21, 'V')
--			char_code_map.put(22, 'W')
--			char_code_map.put(23, 'X')
--			char_code_map.put(24, 'Y')
--			char_code_map.put(25, 'Z')

--			create code_char_map.make(26)
--			code_char_map.put('A', 0)
--			code_char_map.put('B', 1)
--			code_char_map.put('C', 2)
--			code_char_map.put('D', 3)
--			code_char_map.put('E', 4)
--			code_char_map.put('F', 5)
--			code_char_map.put('G', 6)
--			code_char_map.put('H', 7)
--			code_char_map.put('I', 8)
--			code_char_map.put('J', 9)
--			code_char_map.put('K', 10)
--			code_char_map.put('L', 11)
--			code_char_map.put('M', 12)
--			code_char_map.put('N', 13)
--			code_char_map.put('O', 14)
--			code_char_map.put('P', 15)
--			code_char_map.put('Q', 16)
--			code_char_map.put('R', 17)
--			code_char_map.put('S', 18)
--			code_char_map.put('T', 19)
--			code_char_map.put('U', 20)
--			code_char_map.put('V', 21)
--			code_char_map.put('W', 22)
--			code_char_map.put('X', 23)
--			code_char_map.put('Y', 24)
--			code_char_map.put('Z', 25)
--		end

	invariant
		not_empty_key: not key.is_empty

end
