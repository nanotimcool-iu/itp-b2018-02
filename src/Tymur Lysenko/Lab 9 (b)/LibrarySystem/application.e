note
	description: "LibrarySystem application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			lib_sys: LIBRARY_SYSTEM
			people_ids: ARRAYED_LIST[INTEGER]
			--temp_person: detachable PERSON
		do
			create lib_sys.make
			create people_ids.make(3)
			print("Library created%N")

			-- Add books
			lib_sys.add_book(
				create {BOOK}.make(
					"Touch of class", false, true, 1),
				9999)
			lib_sys.add_book(
				create {BOOK}.make(
					"Code complete", false, true, 50),
				3)
			lib_sys.add_book(
				create {BOOK}.make(
					"1984", true, true, 20),
				30)
			lib_sys.add_book(
				create {BOOK}.make(
					"Low level programming", false, false, 45),
				7)

			lib_sys.print_books

			-- Register people
			people_ids.extend(
				lib_sys.register_person(
					create {PERSON}.make(
						"Tymur Lysenko",
						"Russia, Innopolis,Innopolis University dorm, campus #4, room 315",
						1244589863,
						19
					)
				)
			)
			people_ids.extend(
				lib_sys.register_person(
					create {PERSON}.make(
						"Oleg",
						"Russia, Kazan",
						895321564,
						10
					)
				)
			)
			people_ids.extend(
				lib_sys.register_person(
					create {PERSON}.make(
						"Alex",
						"Russia, Rostov-on-Don",
						156879213,
						19
					)
				)
			)

			-- lend books to people
			lib_sys.lend_book(
				lib_sys.find_person_by_id(people_ids[1]).registered_to,
				lib_sys.books[1].lib_book
			)
			lib_sys.lend_book(
				lib_sys.find_person_by_id(people_ids[1]).registered_to,
				lib_sys.books[2].lib_book
			)
			lib_sys.lend_book(
				lib_sys.find_person_by_id(people_ids[1]).registered_to,
				lib_sys.books[3].lib_book
			)
			lib_sys.lend_book(
				lib_sys.find_person_by_id(people_ids[1]).registered_to,
				lib_sys.books[4].lib_book
			)

			lib_sys.lend_book(
				lib_sys.find_person_by_id(people_ids[2]).registered_to,
				lib_sys.books[1].lib_book
			)


			lib_sys.lend_book(
				lib_sys.find_person_by_id(people_ids[3]).registered_to,
				lib_sys.books[2].lib_book
			)
			lib_sys.lend_book(
				lib_sys.find_person_by_id(people_ids[3]).registered_to,
				lib_sys.books[3].lib_book
			)

			lib_sys.find_person_by_id(people_ids[3]).books_borrowed[2].due_date.make_fine(2018, 10, 1, 1, 10, 30)
			lib_sys.print_people_stats
		end
end
