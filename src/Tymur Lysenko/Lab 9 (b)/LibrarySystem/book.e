note
	description: "Summary description for {BOOK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOOK

create
	make

feature {NONE} -- Initialization

	make(book_name: STRING; restricted, best_seller: BOOLEAN; book_cost: INTEGER)
			-- Initialization for 'Current'.
		do
			name := book_name
			is_restricted_to_children := restricted
			is_best_seller := best_seller
			cost := book_cost
		end

feature -- Open commands

	set_best_seller(new_best_seller: BOOLEAN)
		do
			is_best_seller := new_best_seller
		end

feature -- Open queries

	get_string_repr: STRING
		do
			result := "%NName: " + name +
					"%NIs restricted to children: " + is_restricted_to_children.out +
					"%NIs best seller: " + is_best_seller.out +
					"%NCost: $" + cost.out + "%N"
		end

feature -- Open attributes

	name: STRING
	is_restricted_to_children: BOOLEAN
	is_best_seller: BOOLEAN assign set_best_seller
	cost: INTEGER

end
