note
	description: "Summary description for {BORROWED_BOOK_RECORD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BORROWED_BOOK_RECORD

create
	make

feature {NONE} -- Initialization

	make(book_to_borrow: BOOK; expire_date: DATE_TIME)
			-- Initialization for 'Current'.
		do
			borrowed_book := book_to_borrow
			due_date := expire_date
		end

feature -- Open queries

	calculate_fine: INTEGER_64
		local
			cur_date: DATE_TIME
		do
			create cur_date.make_now

			result := (cur_date.days * cur_date.seconds) -
				(due_date.days * due_date.seconds)

			if result <= 0 then
				result := 0
			else
				if result > borrowed_book.cost then
					result := borrowed_book.cost.to_integer_64
				else
					result := result // 864
				end
			end
		end

	get_string_representation: STRING
		do
			result := borrowed_book.get_string_repr +
				"%NDue date: " + due_date.out +
				"%NCurrent fine: " + calculate_fine.out + "%N"
		end

feature -- Open attributes

	borrowed_book: BOOK
	due_date: DATE_TIME

end
