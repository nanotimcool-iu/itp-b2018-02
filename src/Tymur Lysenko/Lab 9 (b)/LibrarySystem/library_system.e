note
	description: "Summary description for {LIBRARY_SYSTEM}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LIBRARY_SYSTEM

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for 'Current'.
		do
			create people_records.make(0)
			create books.make(0)
			last_id := 0
		end

feature -- Open commands

	print_books
		do
			print("%NBook list:%N")
			across books as b
			loop
				print(b.item.lib_book.get_string_repr +
					"%NNum books: " + b.item.num.out + "%N")
			end
		end

	print_people_stats
		do
			print("%NRegistered people statistics:%N")
			across people_records as pr
			loop
				print(pr.item.get_string_representation)
			end
		end

	register_person(person_to_register: PERSON): INTEGER
		require
			person_not_registered: not is_registered(person_to_register)
		do
			result := generate_new_key
			people_records.put(
				create {PERSON_RECORD}.make_from_person(person_to_register),
				result)
		ensure
			people_records.count = old people_records.count + 1
			person_registered: is_registered(person_to_register)
		end

	add_book(book_to_add: BOOK; num_books: INTEGER)
		require
			not_negative_books: num_books >= 0
		do
			if attached find_book_by_reference(book_to_add) as b then
				b.num := num_books
			else
				books.extend([book_to_add, num_books])
			end
		ensure
			book_is_added: book_in_library(book_to_add)
		end

	lend_book(person_to_lend_to: PERSON; book_to_lend: BOOK)
		require
			person_registered: is_registered(person_to_lend_to)
			book_is_in_library: attached find_book_by_reference(book_to_lend) as b b.num > 0
			person_can_borrow_book: attached find_person_by_reference(person_to_lend_to) as per per.can_borrow_book(book_to_lend)
		local
			due_date: DATE_TIME
		do
			create due_date.make_now

			if book_to_lend.is_best_seller then
				due_date.day_add(14)
			else
				due_date.day_add(21)
			end

			if attached find_person_by_reference(person_to_lend_to) as p then
				p.add_borrowed_book(book_to_lend, due_date)

				if attached find_book_by_reference(book_to_lend) as b then
					b.num := b.num - 1
				end
			end
		end

feature -- Open queries

	book_in_library(book_to_find: BOOK): BOOLEAN
		do
			result := attached find_book_by_reference(book_to_find)
		end

	find_book_by_reference(book_to_find: BOOK): detachable TUPLE[lib_book: BOOK; num: INTEGER]
		local
			is_found: BOOLEAN
			i: INTEGER
		do
			is_found := false

			from
				i := 1
			until
				is_found
				or else
				i > books.count
			loop
				if books[i].lib_book = book_to_find then
					is_found := true
					result := books[i]
				end

				i := i + 1
			end
		end

	has_id(id: INTEGER): BOOLEAN
		do
			result := people_records.has(id)
		end

	is_registered(person_to_find: PERSON): BOOLEAN
		do
			result := attached find_person_by_reference(person_to_find)
		end

	find_person_by_reference(person_to_find: PERSON): detachable PERSON_RECORD
		local
			is_found: BOOLEAN
		do
			is_found := false

			from
				people_records.start
			until
				is_found
				or else
				people_records.off
			loop
				if people_records.item_for_iteration.registered_to = person_to_find then
					is_found := true
					result := people_records.item_for_iteration
				end

				people_records.forth
			end
		end

	find_person_by_id(id: INTEGER): PERSON_RECORD
		require
			id_exists: has_id(id)
		do
			create result.make_from_person(
				create {PERSON}.make(
					"Yet another crutch, because stupid Eiffel can't return attached data from dictionary, even if key exists", "", 0, 0
				)
			)

			if attached people_records[id] as p then
				result := p
			end
		end

feature -- Open attributes

	books: ARRAYED_LIST[TUPLE[lib_book: BOOK; num: INTEGER]]

feature {NONE} -- Closed queries

	generate_new_key: INTEGER
		do
			last_id := last_id + 1
			result := last_id
		ensure
			unique_key: not attached people_records.at(result)
		end

feature {NONE} -- Closed attributes

	people_records: HASH_TABLE[PERSON_RECORD, INTEGER]
	last_id: INTEGER

end
