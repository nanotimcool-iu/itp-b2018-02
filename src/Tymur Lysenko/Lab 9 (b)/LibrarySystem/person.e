note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature {NONE} -- Initialization

	make(person_name, person_address: STRING;
		phone_num: INTEGER_64;
		person_age: INTEGER)
			-- Initialization for 'Current'.
		do
			name := person_name
			address := person_address
			phone := phone_num
			age := person_age
		end

feature -- Open attributes

	name: STRING
	address: STRING
	phone: INTEGER_64
	age: INTEGER

end
