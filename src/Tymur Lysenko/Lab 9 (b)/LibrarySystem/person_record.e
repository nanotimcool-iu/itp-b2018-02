note
	description: "Summary description for {PERSON_RECORD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON_RECORD

create
	make_from_person

feature {NONE} -- Initialization

	make_from_person(person_to_make_from: PERSON)
			-- Initialization for 'Current'.
		do
			registered_to := person_to_make_from
			create books_borrowed.make(0)
		end

feature -- Open commands

	add_borrowed_book(borrowed_book: BOOK; due_to: DATE_TIME)
		do
			books_borrowed.extend(
				create {BORROWED_BOOK_RECORD}.make(
					borrowed_book, due_to))
		end

	return_book(book_to_return: BOOK)
		require
			has_book_borrowed: get_book_index(book_to_return) /= 0
		do
			books_borrowed.prune_all(books_borrowed[get_book_index(book_to_return)])
		end

feature -- Open queries

	can_borrow_book(book_to_borrow: BOOK): BOOLEAN
		do
			result := not (registered_to.age <= 12
				and then (books_borrowed.count > 5
					or else book_to_borrow.is_restricted_to_children))
		end

	get_book_index(book_to_search: BOOK): INTEGER
		local
			i: INTEGER
		do
			result := 0
			from
				i := 1
			until
				i > books_borrowed.count
				or else
				result > 0
			loop
				if books_borrowed[i].borrowed_book = book_to_search then
					result := i
				end

				i := i + 1
			end
		end

	calculate_fine: ARRAYED_LIST[TUPLE[book: BOOK; fine: INTEGER_64]]
		do
			create result.make(0)

			across books_borrowed as bb
			loop
				result.extend(
					[bb.item.borrowed_book,
					bb.item.calculate_fine]
				)
			end
		end

	get_string_representation: STRING
		do
			result := "%NName: " + registered_to.name +
					"%NAddress: " + registered_to.address +
					"%NPhone: " + registered_to.phone.out +
					"%NAge: " + registered_to.age.out +
					"%NBooks statistics:%N"

				across books_borrowed as bb
				loop
					result := result + bb.item.get_string_representation
				end
		end

feature -- Open attributes

	registered_to: PERSON
	books_borrowed: ARRAYED_LIST[BORROWED_BOOK_RECORD]

end
