note
	description: "Stack application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
				stck: MY_STACK[INTEGER]
				bndt_stck: MY_BOUNDED_STACK[INTEGER]
				cur_el: INTEGER
		do
			create stck.make
			stck.push(10)
			stck.push(15)
			stck.push(7)

			from
			until
				stck.is_empty
			loop
				cur_el := stck.pop
				print(cur_el.out + "%N")
			end
			print("%N")

			create bndt_stck.make_with_size(2)

			bndt_stck.push(11)
			bndt_stck.push(22)

			if bndt_stck.is_full then
				print("The bounded stack is full%N")
			else
				bndt_stck.push(33) -- dead code
			end

			from
			until
				bndt_stck.is_empty
			loop
				cur_el := bndt_stck.pop
				print(cur_el.out + "%N")
			end

			io.read_character
		end

end
