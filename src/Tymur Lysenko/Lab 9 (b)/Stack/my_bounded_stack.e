note
	description: "Summary description for {MY_BOUNDED_STACK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_BOUNDED_STACK[G]

inherit
	MY_STACK[G]
		rename
			push as stack_push,
			pop as stack_pop,
			remove as stack_remove
		export
			{NONE}
			stack_push,
			stack_pop,
			stack_remove
		end

create
	make_with_size

feature {NONE} -- Initialization

	make_with_size(initial_capacity: INTEGER)
			-- Initialization for 'Current'.
		require
			positive_capacity: initial_capacity > 0
		do
			capacity := initial_capacity
			num_elements := 0
		end

feature -- Open commands

	push(item_to_add: G)
		require
			not_full: not is_full
		do
			stack_push(item_to_add)
			num_elements := num_elements + 1
		ensure
			num_elements_increased: num_elements = old num_elements + 1
		end

	remove
		require
			not_empty: not is_empty
		do
			stack_remove
			num_elements := num_elements - 1
		ensure
			num_elements_redused: num_elements = old num_elements - 1
			not_full: not is_full
		end

feature -- Open queries

	pop: detachable G
		require
			not_empty: not is_empty
		do
			result := item
			remove
		ensure
			num_elements_redused: num_elements = old num_elements - 1
			not_full: not is_full
		end

	is_full: BOOLEAN
		do
			result := num_elements = capacity
		end

feature -- Open attributes

	capacity: INTEGER
	num_elements: INTEGER

invariant
	full_not_empty: is_full implies not is_empty
	empty_not_full: is_empty implies not is_full

end
