note
	description: "Summary description for {MY_STACK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MY_STACK[G]

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for 'Current'.
		do
		end

feature -- Open commands

	push(item_to_add: G)
		local
			prev: detachable STACK_ELEMENT[G]
		do
			prev := head
			create head.make_from_val(item_to_add)

			if attached head as h then
				h.prev := prev
			end

		ensure
			item_is_on_top: item = item_to_add
			not_empty: not is_empty
		end

	remove
		require
			not_empty: not is_empty
		do
			if attached head as h then -- redundant code, because
				-- head will never be void due to require contract
				head := h.prev
			end
		end

feature -- Open queries

	is_empty: BOOLEAN
		do
			result := not attached head
		end

	item: detachable G
		require
			not_empty: not is_empty
		do
			if attached head as h then -- redundant code, because
				-- head will never be void due to require contract
				result := h.value
			end
		end

	pop: detachable G
		require
			not_empty: not is_empty
		do
			result := item
			remove
		end

feature {NONE} -- Closed attributes

	head: detachable STACK_ELEMENT[G]

end
