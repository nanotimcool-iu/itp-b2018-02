note
	description: "Summary description for {STACK_ELEMENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	STACK_ELEMENT[G]

create
	make_from_val

feature {NONE} -- Initialization

	make_from_val(val: G)
			-- Initialization for 'Current'.
		do
			value := val
		end

feature {MY_STACK, STACK_ELEMENT}
	 -- Attributes available to MY_STACK and to the current

	prev: detachable STACK_ELEMENT[G] assign set_prev
	value: G

feature {MY_STACK, STACK_ELEMENT}
	-- Commands available to MY_STACK and to the current

	set_prev(prev_el: detachable STACK_ELEMENT[G])
		do
			if attached prev as p then
				p.prev := prev_el
			else
				prev := prev_el
			end
		end

end
