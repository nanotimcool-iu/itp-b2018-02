note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			--| Add your code here
			io.put_string("Name: Yerzhan Gapurinov")
			io.new_line
			io.put_string ("Age: 17")
			io.new_line
			io.put_string ("Mother tongue: Kazakh")
			io.new_line
			io.put_string ("Has a pet: ")
			io.put_boolean(True)
			io.new_line
		end

end
