note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"
	author: "Gapurinov Yerzhan"

class
	BANK_ACCOUNT

inherit
	ARGUMENTS

create
	make
feature
	name: STRING
	balance: INTEGER
feature

	make
		do
			balance := 36000
			name := "Yerzhan"
			deposit(5000)
			withdraw(1000)
			print(balance)
		end
	deposit(money: INTEGER)
		require
			positive_money: money >= 0
		do
			balance := balance + money
		end
	withdraw(money: INTEGER)
		require
			positibe_money: money >= 0
		do
			balance := balance - money
		end
invariant
		minimum_money: balance > 99
		max_money: balance <= 1000000
end
