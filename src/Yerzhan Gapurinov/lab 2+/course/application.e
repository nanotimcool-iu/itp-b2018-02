class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			the_course: COURSE
			-- Run application.
		do
			--| Add your code here
			create the_course.create_course ("Programming", 18, "Mondays", 180)
			print(the_course.name + "%N" + the_course.id.out + "%N" + the_course.schedule + "%N")
		end

end
