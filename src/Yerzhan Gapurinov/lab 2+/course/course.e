
class
	COURSE

create
	create_course

feature
	name: STRING
	id: INTEGER
	schedule: STRING
	max_students: INTEGER = 250

feature -- Initialization

	create_course(a_name: STRING; an_id: INTEGER; a_schedule: STRING; num_students: INTEGER)
		do
			if num_students < 3 or num_students > max_students then
				io.put_string ("Incorrect number of students")
			end
			name := a_name
			id := an_id
			schedule := a_schedule
		end

end
