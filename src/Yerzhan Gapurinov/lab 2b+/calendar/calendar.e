note
	description: "calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		do
		end
feature
		create_entry(date: TIME; owner: PERSON; place: STRING; subject: STRING) : ENTRY
				local
						x: ENTRY
				do
					create x.do_something(date, owner, place, subject)
					Result := x
				end
		edit_subject(x: ENTRY; a_subject: STRING)
				do
					x.set_subject(a_subject)
				end
		edit_date(x: ENTRY; a_date: TIME)
				do
					x.set_date (a_date)
				end
		get_owner_name(x: ENTRY): STRING
				do
					Result := x.get_name
				end
		in_conflict(x1: ENTRY; x2: ENTRY): BOOLEAN
				do
					if x1.place = x2.place or x1.date = x2.date then
						Result := TRUE
					else
						Result := FALSE

					end
				end
end
