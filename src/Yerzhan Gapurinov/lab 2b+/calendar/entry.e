note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	do_something

feature
		date: TIME
		owner: PERSON
		subject: STRING
		place: STRING

feature  -- Initialization

	do_something(a_date: TIME; an_owner: PERSON; a_subject: STRING; a_place: STRING)
		do
			date := a_date
			owner := an_owner
			subject := a_subject
			place := a_place
		end
	set_date(a_date: TIME)
		do
			date := a_date
		end
	set_subject(a_subject: STRING)
		do
			subject := a_subject
		end
	get_place: STRING
		do
			Result := place
		end
	get_name: STRING
		do
			Result := owner.get_owner_name
		end
	get_date: TIME
		do
			Result := date
		end
end
