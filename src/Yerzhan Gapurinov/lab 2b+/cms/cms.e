note
	description: "cms application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CMS

inherit
	ARGUMENTS

create
	make

feature -- Initialization

	make
		local
		cnt_1: CONTACT
		cnt_2: CONTACT
		cnt_3: CONTACT

		do
			cnt_1 := create_contact("Yerzhan", 111, "Innopolis", "raketa@gmail.com")
			cnt_2 := create_contact("Dona", 222, "SDU", "dona@gmail.com")
			cnt_3 := create_contact("Nurlykhan", 333, "NU", "nurlykh@gmail.com")
			add_emergency_contact(cnt_1, cnt_2)
			add_emergency_contact(cnt_2, cnt_3)
			edit_contact(cnt_2, "Dona", 666, "SDU", "dno@gmail.com")
			remove_emergency_contact(cnt_2)
			remove_emergency_contact(cnt_3)
		end
feature
		create_contact(a_name: STRING; a_number: INTEGER_64; a_work_place: STRING; an_email: STRING) : CONTACT
				local
						x : CONTACT
				do
						create x.initialization (a_name, a_number, a_work_place, an_email)
						Result := x
				end
		edit_contact(x: CONTACT; a_name: STRING; a_number: INTEGER_64; a_work_place: STRING; an_email: STRING)
				do
					x.set_name (a_name)
					x.set_email (an_email)
					x.set_number (a_number)
					x.set_work_place (a_work_place)
				end
		add_emergency_contact(x1: CONTACT; x2: CONTACT)
				do
					x1.set_emergency (x2)
				end
		remove_emergency_contact(x1: CONTACT;)
				do
					x1.set_emergency (void)
				end
end
