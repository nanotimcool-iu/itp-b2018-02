note
	description: "Summary description for {CONTACT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONTACT

create
	initialization

feature
		name: STRING assign set_name
		set_name(a_name: STRING)
			do
				name := a_name
			end
		number: INTEGER_64 assign set_number
		set_number(a_number: INTEGER_64)
			do
				number := a_number
			end
		work_place: STRING assign set_work_place
		set_work_place(a_work_place: STRING)
			do
				work_place := a_work_place
			end
		email: STRING assign set_email
		set_email(an_email: STRING)
			do
				email := an_email
			end
		emergency_cont: detachable CONTACT assign set_emergency
		set_emergency(an_emergency_cont: detachable CONTACT)
			do
				emergency_cont := an_emergency_cont
			end
feature

	initialization(a_name: STRING; a_number: INTEGER_64; a_work_place: STRING; an_email: STRING)
		do
			name := a_name
			number := a_number
			work_place := a_work_place
			email := an_email
			emergency_cont := void
		end

end
