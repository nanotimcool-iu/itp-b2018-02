note
	description: "calendar application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	CALENDAR

inherit
	ARGUMENTS

create
	make
feature
			main_list: ARRAYED_LIST[DAY]
			list_length: INTEGER

feature {NONE}

	make
		local
				event: ENTRY
				event2: ENTRY
				date: THE_TIME
				date2: THE_TIME
				owner: PERSON
				owner2: PERSON
				test_list: ARRAYED_LIST[ENTRY]
		do
				create main_list.make (0)
				create test_list.make (0)
				list_length := 0
				create owner.init_person("YERZHAN", 776699, "GOOGLE", "gapurinovezhan@gmail.com")
				create date.time_init(10, 13)
				create event.entry_init(owner, date, "CODING COMPETITION", "MAIN HALL")
				create owner2.init_person("ANDREY", 789878, "FACEBOOK", "coolbek@gmail.com")
				create date2.time_init(14, 19)
				create event2.entry_init(owner2, date2, "VIDEO GAMING COMPETITON", "READING HALL 3rd FLOOR")
				add_entry(event, 6)
				add_entry(event2, 6)
				test_list := in_conflict(6)
				print(test_list[1].get_owners_name + "%N")
				print(printable_month)
		end

feature
	days : ARRAYED_LIST[DAY]
		do
			Result := main_list
		end
	add_entry (e : ENTRY; a_day : INTEGER)
		local
			i : INTEGER
			ended : BOOLEAN
			day_added : DAY
		do
			ended := FALSE
			from
				i := 1
			until
				i > list_length or ended = TRUE
			loop
				if main_list[i].get_day = a_day then
					main_list[i].add_event(e)
					ended := TRUE
				end
				i := i + 1
			end
			if ended = FALSE then
				create day_added.day_init(a_day)
				day_added.add_event(e)
				main_list.extend(day_added)
				list_length := list_length + 1
			end
		end
	edit_subject (x : ENTRY; new_subject : STRING)
		do
			x.edit_subject (new_subject)
		end
	edit_date (x : ENTRY; new_date : THE_TIME)
		do
			x.edit_date (new_date)
		end
	get_owner_name (x : ENTRY) : STRING
		do
			Result := x.get_owners_name
		end
	in_conflict (a_day : INTEGER) : ARRAYED_LIST[ENTRY]
		local
			i : INTEGER
			empty_list : ARRAYED_LIST[ENTRY]
			checked : BOOLEAN
		do
			create empty_list.make(0)
			checked := FALSE
			Result := empty_list
			from
				i := 1
			until
				checked = TRUE or i > list_length
			loop
				if main_list[i].get_day = a_day then
					Result := main_list[i].get_overlaped
					checked := TRUE
				end
				i := i + 1
			end
		end
	printable_month : STRING
		local
			i : INTEGER
		do
			Result := ""
			from
				i := 1
			until
				i > list_length
			loop
				Result := Result + "DAY " + main_list[i].get_day.out + ":%N"
				Result := Result + main_list[i].print_events
				Result := Result + "%N"
				i := i + 1
			end
		end
end
