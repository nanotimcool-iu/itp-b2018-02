note
	description: "Summary description for {DAY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DAY

create
	day_init

feature
	date: INTEGER
	events: ARRAYED_LIST[ENTRY]
	events_amount: INTEGER
feature

	day_init(a_date: INTEGER)
		do
			set_date(a_date)
			create events.make(0)
			set_events_amount(0)
		end
	set_date(x: INTEGER)
		do
			date := x
		end
	set_events_amount(x: INTEGER)
		do
			events_amount := x
		end
	get_day: INTEGER
		do
			Result := date
		end
	add_event(x: ENTRY)
		do
			events.extend(x)
			events_amount := events_amount + 1
		end
	get_overlaped: ARRAYED_LIST[ENTRY]
		local
				i, j: INTEGER
				ended: BOOLEAN
				overlaped_events: ARRAYED_LIST[ENTRY]
		do
			create overlaped_events.make(0)
			from
				i := 1
			until
				i > events_amount
			loop
				ended := FALSE
				from
					j:=1
				until
					j > events_amount or ended = TRUE
				loop
					if events[i].get_start_time >= events[j].get_start_time and
						events[i].get_start_time <= events[j].get_end_time or
						events[i].get_end_time >= events[j].get_start_time and
						events[i].get_end_time <= events[j].get_end_time or
						events[i].get_owner = events[j].get_owner or
						events[i].get_place = events[j].get_place then
							ended := TRUE
							overlaped_events.extend(events[i])
					end
					j := j + 1
				end
				i := i + 1
			end
			Result := overlaped_events
		end
	print_events: STRING
		local
			i: INTEGER
		do
			Result := ""
			from
				i:= 1
			until
				i > events_amount
			loop
				Result := Result + "Event " + i.out + ":%N"
				Result := Result + "From " + events[i].get_start_time.out + ":00 till " + events[i].get_end_time.out + ":00%N"
				Result := Result + "HELD AT " + events[i].get_place + "%N"
				Result := Result + "BY: " + events[i].get_owners_name + "%N"
				Result := Result + "SUBJECT: " + events[i].get_subject + "%N%N"
				i := i + 1
			end
		end
end
