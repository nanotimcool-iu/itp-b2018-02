note
	description: "Summary description for {ENTRY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENTRY

create
	entry_init
feature
		owner: PERSON
		date: THE_TIME
		subject: STRING
		place: STRING

feature
	entry_init(an_owner: PERSON; a_date: THE_TIME; a_subject: STRING; a_place: STRING)
		do
			edit_owner(an_owner)
			edit_date(a_date)
			edit_subject(a_subject)
			edit_place(a_place)
		end
	edit_owner(x: PERSON)
		do
			owner := x
		end
	edit_date(x: THE_TIME)
		do
			date := x
		end
	edit_subject(x: STRING)
		do
			subject := x
		end
	edit_place(x: STRING)
		do
			place := x
		end
	get_owners_name: STRING
		do
			Result := owner.get_name
		end
	get_subject: STRING
		do
			Result := subject
		end
	get_start_time: INTEGER
		do
			Result := date.get_beginning_time
		end
	get_end_time: INTEGER
		do
			Result := date.get_ending_time
		end
	get_owner: PERSON
		do
			Result := owner
		end
	get_place: STRING
		do
			Result := place
		end
end
