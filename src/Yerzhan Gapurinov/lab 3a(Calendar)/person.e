note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	init_person

feature
		name: STRING
		phone_number: INTEGER
		work_place: STRING
		email: STRING
feature

	init_person(a_name: STRING; a_phone_number: INTEGER; a_work_place: STRING; an_email: STRING)
			do
				set_name(a_name)
				set_phone_number(a_phone_number)
				set_work_place(a_work_place)
				set_email(an_email)
			end
	set_name(x: STRING)
			do
				name := x
			end
	set_phone_number(x: INTEGER)
			do
				phone_number := x
			end
	set_work_place(x: STRING)
			do
				work_place := x
			end
	set_email(x: STRING)
			do
				email := x
			end
	get_name : STRING
			do
				Result := name
			end
end
