note
	description: "Summary description for {THE_TIME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	THE_TIME

create
	time_init
feature
		begin_time: INTEGER
		end_time: INTEGER

feature
	time_init(begins: INTEGER; ends: INTEGER)
			do
				set_the_beginning_time(begins)
				set_the_ending_time(ends)
			end
	get_beginning_time: INTEGER
			do
				Result := begin_time
			end
	set_the_beginning_time(x: INTEGER)
			do
				begin_time := x
			end
	get_ending_time: INTEGER
			do
				Result := end_time
			end
	set_the_ending_time(x: INTEGER)
			do
				end_time := x
			end
end
