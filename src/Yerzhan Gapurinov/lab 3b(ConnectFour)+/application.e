note
	description: "ConnectFour application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make
feature
		a: ARRAY2[INTEGER]
		n: INTEGER
		m: INTEGER
		player: BOOLEAN
		game_end: BOOLEAN
		name1: STRING
		name2: STRING
		x: INTEGER
		it: INTEGER
		value: INTEGER
feature
		control(row: INTEGER; col: INTEGER): BOOLEAN
				do
					if check_vertical(row, col) = TRUE or check_horizontal(row, col) = TRUE or check_diagonal_1(row, col) = TRUE or check_diagonal_2(row, col) = TRUE then
						Result := TRUE
					else
						Result := FALSE
					end
				end
		check_vertical(row: INTEGER; col: INTEGER): BOOLEAN
				local
						i: INTEGER;
						ans: INTEGER;
				do
					ans := 0
					from
						i := row
					until
						i > n or ans >= 4 or a[i, col] /= a[row, col]
					loop
						i := i + 1
						ans := ans + 1
					end

					from
						i := row-1
					until
						i <= 0 or ans >= 4 or a[i, col] /= a[row, col]
					loop
						i := i - 1
						ans := ans + 1
					end
					if ans >= 4 then
						Result := TRUE
					else
						Result := FALSE
					end
				end
		check_horizontal(row: INTEGER; col: INTEGER): BOOLEAN
				local
					j: INTEGER
					ans: INTEGER
				do
					ans := 0
					from
						j := col
					until
						j > m or ans >= 4 or a[row, j] /= a[row, col]
					loop
						j := j + 1
						ans := ans + 1
					end

					from
						j := col - 1
					until
						j <= 0 or ans >= 4 or a[row, j] /= a[row, col]
					loop
						j := j - 1
						ans := ans + 1
					end
					if ans >= 4 then
						Result := TRUE
					else
						Result := FALSE
					end
				end
		check_diagonal_1(row: INTEGER; col: INTEGER): BOOLEAN
				local
					i: INTEGER
					j: INTEGER
					ans: INTEGER
				do
					ans := 0
					from
						i := row
						j := col
					until
						i <= 0 or j > m or ans >= 4 or a[i, j] /= a[row, col]
					loop
						i := i - 1
						j := j + 1
						ans := ans + 1
					end

					from
						i := row + 1
						j := col - 1
					until
						i > n or j <= 0 or ans >= 4 or a[i, j] /= a[row, col]
					loop
						i := i + 1
						j := j - 1
						ans := ans + 1
					end
					if ans >= 4 then
						Result := TRUE
					else
						Result := FALSE
					end
				end
		check_diagonal_2(row: INTEGER; col: INTEGER): BOOLEAN
				local
					i: INTEGER
					j: INTEGER
					ans: INTEGER
				do
					ans := 0
					from
						i := row
						j := col
					until
						i <= 0 or j <= 0 or ans >= 4 or a[i, j] /= a[row, col]
					loop
						i := i - 1
						j := j - 1
						ans := ans + 1
					end

					from
						i := row + 1
						j := col + 1
					until
						i > n or j > m or ans >= 4 or a[i, j] /= a[row, col]
					loop
						i := i + 1
						j := j + 1
						ans := ans + 1
					end

					if ans >= 4 then
						Result := TRUE
					else
						Result := FALSE
					end
				end
feature {NONE} -- Initialization

	make
		local
				nn: INTEGER
				mm: INTEGER
				cnt: INTEGER
		do
				print("Enter your names please%N")
				print("First player: ")
				io.read_word
				name1 := create {STRING}.make_from_string (io.last_string)
				print("Second player: ")
				io.read_word
				name2 := create {STRING}.make_from_string (io.last_string)

				print(name1 + " " + name2 + "%N")

				player := FALSE
				game_end := FALSE
				print("Enter n: ")
				io.read_integer
				n := io.last_integer
				print("Enter m: ")
				io.read_integer
				m := io.last_integer
				create a.make_filled(0, n, m)
				from

				until
						game_end = TRUE
				loop
						if player = FALSE then
								print(name1 + "'s turn: ")
						else
								print(name2 + "'s turn: ")
						end
						io.read_integer
						x := io.last_integer
						if a[1, x] > 0 then
								print("ERROR, this column is full%N")
								player := player XOR TRUE
						else
								cnt := cnt + 1
								from
										it := 1
								until
										it > n or a[it, x] > 0
								loop
										value := it
										it := it + 1
								end
								if player = FALSE then
										a[value, x] := 1
								else
										a[value, x] := 2
								end

								from
										nn := 1
								until
										nn > n
								loop
										from
												mm := 1
										until
												mm > m
										loop
												print(a[nn, mm].out + " ")
												mm := mm + 1
										end
										nn := nn + 1
										print("%N")
								end

								if control(value, x) = TRUE then
										if player = FALSE then
												print(name1 + " wins, CONGRATULATIONS!!!")
										else
												print(name2 + " wins, CONGRATULATIONS!!!")
										end
										game_end := TRUE
								end
								if game_end = FALSE and cnt = n * m then
										print("DRAW")
										game_end := TRUE
								end
						end
						player := player XOR TRUE
					end
			end

end
