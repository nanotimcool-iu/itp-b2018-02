note
	description: "triangnle application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	PAINTING

inherit
	ARGUMENTS

create
	make

feature
		n: INTEGER
		x: INTEGER
		num: INTEGER
		i: INTEGER
		probel: INTEGER
		last: INTEGER
		crt: INTEGER
		height: INTEGER
		crt_probel: INTEGER

feature {NONE} -- Initialization

	make
		do
			io.read_integer
			num := io.last_integer
			probel := 0
			last := 0
			x := num
			x := (x + 1) // 2
			n := (x + 1) * x
			if num \\ 2 /= 0 then
				n := n - num
			end
			from
					i := n
			until
					i = n - x
			loop
					from
							crt := i
					until
							crt = 0
					loop
							probel := probel + 1
							crt := crt // 10
					end
					probel := probel + 1
					i := i - 1
			end

			if num \\ 2 = 0 then
				probel := probel + 1
			end
			probel := probel * 2
			from
					i := 1
					height := 2
			until
					i > n
			loop
					from
							crt := i
					until
							crt = 0
					loop
							crt_probel := crt_probel + 1
							crt := crt // 10
					end

					if height // 2 = i - last then
							height := height + 1
							print(i)
							from
									crt := 1
							until
									crt > probel - (crt_probel * 2)
							loop
									print(" ")
									crt := crt + 1
							end
							crt_probel := 0
							from
									crt := i
							until
									crt = last
							loop
									if last + 1 /= crt then
											print(crt.out + " ")
									else
											print(crt.out)
											if height \\ 2 = 0 then
													print(" ")
											end
									end
									crt := crt - 1
							end
							print("%N")
							if height \\ 2 = 1 then
									print(" ")
									crt_probel := crt_probel + 1
							end
							last := i
					else
							crt_probel := crt_probel + 1
							print(i.out + " ")
					end
					i := i + 1
				end
		end

end
