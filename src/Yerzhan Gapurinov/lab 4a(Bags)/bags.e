note
	description: "Bags application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	BAGS

inherit
	ARGUMENTS

create
	make
feature
	elements: ARRAYED_LIST[CELL_INFO]
	elements_len: INTEGER

feature {NONE}

	make
		local
			i: INTEGER
		do

			elements_len := 0
			print(elements_len)
			create elements.make(0)
			add('a', 2)
			add('b', 1)
			print(min)
			print("%N")
			print(max)
			print("%N")
			print("[")
			from
				i := 1
			until
				i < 0
			loop
				print("(")
				print(elements[i].get_value.out)
				print(",")
				print(elements[i].get_number_copies)
				print(")")
				print(",")
				i := i + 1
			end
			print("]")
		end
	add(val: CHARACTER; n: INTEGER)
		local
			i: INTEGER
			checker: BOOLEAN
			new: CELL_INFO
		do
			checker := FALSE
			from
				i := 1
			until
				i > elements_len or CHECkER = TRUE
			loop
				if val = elements[i].get_value then
					elements[i].set_number_copies (elements[i].get_number_copies + n)
					CHECKER := TRUE
				end
				i := i + 1
			end
			if checker = FALSE then
				create new.init(val, n)
				elements.extend (new)
				elements_len := elements_len + 1
			end
		end
	remove(val: CHARACTER; n: INTEGER)
		local
			i: INTEGER
			save: CELL_INFO
		do
			from
				i := 1
			until
				i > elements_len
			loop
				if elements[i].get_value = val then
					if n < elements[i].get_number_copies then
						elements[i].set_number_copies(elements[i].get_number_copies - n)
					else
						save := elements[1]
						elements[1] := elements[i]
						elements[i] := save
						elements.remove_right
						elements_len := elements_len - 1
					end
				end
				i := i + 1
			end
		end
	min: CHARACTER
		local
			i: INTEGER
			mn: CHARACTER
		do
			mn := '~'
			from
				i := 1
			until
				i > elements_len
			loop
				if elements[i].value < mn then
					 mn := elements[i].value
				end
				i := i + 1
			end
			Result := mn
		end
	max: CHARACTER
		local
			i: INTEGER
			mx: CHARACTER
		do
			mx := '?'
			from
				i := 1
			until
				i > elements_len
			loop
				if elements[i].value > mx then
					mx := elements[i].value
				end
				i := i + 1
			end
			Result := mx
		end
end
