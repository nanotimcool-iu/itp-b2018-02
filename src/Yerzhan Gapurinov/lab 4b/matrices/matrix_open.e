note
	description: "martix_oper application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_OPEN

inherit
	ARGUMENTS

create
	make
feature
		a: ARRAY2[INTEGER]
		b: ARRAY2[INTEGER]
		c: ARRAY2[INTEGER]
		d: ARRAY2[INTEGER]
feature {NONE}

	make
		local
			i: INTEGER
			j: INTEGER
		do

			create a.make_filled (0, 4, 4)
			create b.make_filled (0, 2, 2)
			create c.make_filled (0, 2, 2)

			a[1, 1] := 1
			a[1, 2] := 2
			a[1, 3] := 3
			a[1, 4] := 4
			a[2, 1] := 5
			a[2, 2] := 6
			a[2, 3] := 6
			a[2, 4] := 10
			a[3, 1] := 2
			a[3, 2] := 3
			a[3, 3] := 21
			a[3, 4] := 13
			a[4, 1] := 12
			a[4, 2] := 10
			a[4, 3] := 7
			a[4, 4] := 3

			b[1, 1] := 2
			b[1, 2] := 3
			b[2, 1] := 4
			b[2, 2] := 5

			c[1, 1] := 21
			c[1, 2] := 9
			c[2, 1] := 1
			c[2, 2] := -4

			print("Matrix A%N")
			from
				i := 1
			until
				i > 4
			loop
				from
					j := 1
				until
					j > 4
				loop
					print(a[i, j].out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
			print("%N")
			print("Matrix B%N")
			from
				i := 1
			until
				i > 2
			loop
				from
					j := 1
				until
					j > 2
				loop
					print(b[i, j].out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
			print("%N")

			print("Matrix C%N")
			from
				i := 1
			until
				i > 2
			loop
				from
					j := 1
				until
					j > 2
				loop
					print(c[i, j].out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
			print("%N")


			print("Determinant of 2*2 matrix B%N")
			print(det(b).out + "%N%N")

			print("Determinant of 4*4 matrix A%N")
			print(det(a).out + "%N%N")

			Print("Product of 2*2 matrix B and C%N")
			d := prod(b, c)

			from
				i := 1
			until
				i > 2
			loop
				from
					j := 1
				until
					j > 2
				loop
					print(d[i, j].out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end

			print("Addition of 2*2 matrices B and C%N")
			print("%N")

			d := add(b, c)
			from
				i := 1
			until
				i > 2
			loop
				from
					j := 1
				until
					j > 2
				loop
					print(d[i, j].out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end

			print("Substraction of 2*2 matrices B and C")
			print("%N")
			d := minus(b, c)
			from
				i := 1
			until
				i > 2
			loop
				from
					j := 1
				until
					j > 2
				loop
					print(d[i, j].out + " ")
					j := j + 1
				end
				print("%N")
				i := i + 1
			end
		end
feature
		add(m1: ARRAY2[INTEGER]; m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
			local
				i: INTEGER
				j: INTEGER
				ans: ARRAY2[INTEGER]
			do
				create ans.make_filled (0, m1.height , m1.width)
				from
					i := 1
				until
					i > m1.height
				loop
					from
						j := 1
					until
						j > m1.width
					loop
						ans[i, j] := m1[i, j] + m2[i, j]
						j := j + 1
					end
					i := i + 1
				end
				Result := ans
			end
		minus(m1: ARRAY2[INTEGER]; m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
			local
				i: INTEGER
				j: INTEGER
				ans: ARRAY2[INTEGER]
			do
				create ans.make_filled (0, m1.height, m1.width)
				from
					i := 1
				until
					i > m1.height
				loop
					from
						j := 1
					until
						j > m1.width
					loop
						ans[i, j] := m1[i, j] - m2[i, j]
						j := j + 1
					end
					i := i + 1
				end
				Result := ans
			end
		prod(m1: ARRAY2[INTEGER]; m2: ARRAY2[INTEGER]): ARRAY2[INTEGER]
			local
				i: INTEGER
				j: INTEGER
				k: INTEGER
				l: INTEGER
				ans: ARRAY2[INTEGER]
				cnt: INTEGER
				n: INTEGER
				m: INTEGER
				p: INTEGER
				q: INTEGER
			do
				n := m1.height
				m := m1.width
				p := m2.height
				q := m2.width
				create ans.make_filled (0, n, q)
				from
					i := 1
				until
					i > n
				loop
					from
						j := 1
					until
						j > q
					loop
						from
							k := 1
						until
							k > n
						loop
							ans[i, j] := ans[i, j] + (m1[i, k] * m2[k, j])
							k := k + 1
						end
						j := j + 1
					end
					i := i + 1
				end
				Result := ans
			end

		det(m: ARRAY2[INTEGER]): INTEGER
			local
				ans: ARRAY2[INTEGER]
				i: INTEGER
				j: INTEGER
				k: INTEGER
				n: INTEGER
				sum: INTEGER
				x: INTEGER
			do
				Result := 0
				n := m.height
				if m.width = 2 and m.height = 2 then
					Result := m[1,1] * m[2, 2] - m[1, 2] * m[2, 1]
				else
					Result := 0
					create ans.make_filled(0, n-1, n-1)
					from
						i := 1
					until
						i > n
					loop
						from
							j := 2
						until
							j > n
						loop
							x := 0
							from
								k := 1
							until
								k > n
							loop
								if k /= i then
									x := x + 1
									ans[j-1, x] := m[j, k]
								end
								k := k + 1
							end
							j := j + 1
						end
						if i \\ 2 = 0 then
							Result := Result - m[1, i] * det(ans)
						else
							Result := Result + m[1, i] * det(ans)
						end
						i := i + 1
					end
				end
			end
end

