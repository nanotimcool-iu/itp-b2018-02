note
	description: "vectors application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_OPER

inherit
	ARGUMENTS

create
	make
feature
	uu: ARRAY[INTEGER]
	vv: ARRAY[INTEGER]
	uu2: ARRAY[INTEGER]
	vv2: ARRAY[INTEGER]
	last_vector: ARRAY[INTEGER]
feature {NONE}
	make
		do
			create uu.make_filled(0, 1, 3)
			create vv.make_filled(0, 1, 3)
			uu[1] := 3
			uu[2] := 5
			uu[3] := 2

			vv[1] := 6
			vv[2] := -2
			vv[3] := 4
			print("Triangle_area of 3D vectors%N")
			print(triangle_area(uu, vv).out + "%N%N")

			create uu2.make_filled(0, 1, 2)
			create vv2.make_filled(0, 1, 2)

			uu2[1] := 0
			uu2[2] := 1

			vv2[1] := 1
			vv2[2] := 0
			print("Triangle_area of 2D vectors%N")
			print(triangle_area(uu2, vv2).out + "%N%N")

			last_vector := cross_product(uu, vv)
			print("Cross_product:%N")
			print(last_vector[1].out + " " + last_vector[2].out + " " + last_vector[3].out + "%N%N")

			print("Dot product:%N")
			print(dot_product(uu, vv))
		end


feature
	dot_product(v1: ARRAY[INTEGER]; v2: ARRAY[INTEGER]): INTEGER
		require
			true_length: v1.capacity = v2.capacity
		local
			i: INTEGER
			ans: INTEGER
		do
			ans := 0
			from
				i := 1
			until
				i > v1.capacity
			loop
				if i \\ 2 = 1 then
					ans := ans + v1[i] * v2[i]
				else
					ans := ans - v1[i] * v2[i]
				end
				i := i + 1
			end
			Result := ans
		end
	cross_product(u: ARRAY[INTEGER]; v: ARRAY[INTEGER]): ARRAY[INTEGER]
		require
			true_length: u.capacity = 3 and u.capacity = v.capacity
		local
			ans: ARRAY[INTEGER]
		do
			create ans.make_filled(0, 1, 3)
			ans[1] := (u[2]*v[3] - u[3]*v[2])
			ans[2] := (u[3]*v[1] - u[1]*v[3])
			ans[3] := (u[1]*v[2] - u[2]*v[1])
			Result := ans
		end
	single_math: SINGLE_MATH
		once
			create Result
		end
	magnitude_of_vector(v: ARRAY[INTEGER]): REAL_64
		local
			ans: INTEGER
			i: INTEGER
		do
			from
				i := 1
			until
				i > v.capacity
			loop
				ans := ans + (v[i] * v[i])
				i := i + 1
			end
			Result := single_math.sqrt(ans)

		end
	triangle_area(v1: ARRAY[INTEGER]; v2: ARRAY[INTEGER]): REAL_64
		local
			u: ARRAY[INTEGER]
			ans: REAL_64
		do
			if v1.capacity = 2 then
				ans := (v1[1] * v2[2] - v1[2] * v2[1]) / 2
				if ans < 0 then
					ans := ans * (-1)
				end
				Result := ans
			else
				u := cross_product(v1, v2)
				Result := magnitude_of_vector(u) / 2
			end
		end
end
