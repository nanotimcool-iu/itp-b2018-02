note
	description: "RANGE_OF_NUMBERS application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	RANGE_OF_NUMBERS

inherit
	ARGUMENTS

create
	make

create
	init

feature
	l: INTEGER
	r: INTEGER

feature
	init(x: INTEGER; y: INTEGER)
		do
			set_left(x)
			set_right(y)
		end
	set_left (x: INTEGER)
		do
			l := x
		end
	set_right (x: INTEGER)
		do
			r := x
		end
	left: INTEGER
		do
			Result := l
		end
	right: INTEGER
		do
			Result := r
		end
	is_equal_range(a: RANGE_OF_NUMBERS): BOOLEAN
		do
			if is_empty and a.left > a.right then
				Result := True
			elseif a.left = l and a.right = r then
				Result := True
			else
				Result := False
			end
		end
	is_empty: BOOLEAN
		do
			if l > r then
				Result := True
			else
				Result := False
			end
		end
	is_sub_range_of_other(a : RANGE_OF_NUMBERS): BOOLEAN
		do
			if is_empty then
				Result := True
			elseif a.left > a.right then
				Result := False
			elseif a.left <= l and r <= a.right then
				Result := True
			else
				Result := False
			end
		end
	is_super_range_of_other (a: RANGE_OF_NUMBERS): BOOLEAN
		do
			if a.left > a.right then
				Result := True
			elseif is_empty then
				Result := False
			elseif a.left >= l and a.right <= r then
				Result := True
			else
				Result := False
			end
		end
	left_overlaps(a: RANGE_OF_NUMBERS): BOOLEAN
		do
			if a.left > a.right or is_empty then
				Result := False
			elseif a.left <= l and a.right >= l then
				Result := True
			else
				Result := False
			end
		end
	right_overlaps(a : RANGE_OF_NUMBERS): BOOLEAN
		do
			if a.left > a.right or is_empty then
				Result := False
			elseif a.left <= r and a.right >= r then
				Result := True
			else
				Result := False
			end
		end
	overlaps(a : RANGE_OF_NUMBERS): BOOLEAN
		do
			if a.left > a.right or is_empty then
				Result := False
			elseif left_overlaps(a) or right_overlaps(a) or is_super_range_of_other(a) or is_sub_range_of_other(a) then
				Result := True
			else
				Result := False
			end
		end
	add(a: RANGE_OF_NUMBERS): RANGE_OF_NUMBERS
		require
			addable : overlaps(a) = True or a.left = right + 1 or l = a.right + 1
		local
			b : RANGE_OF_NUMBERS
		do
			if a.left > a.right and is_empty then
				Result := a
			elseif a.left > a.right then
				Result := current
			elseif is_empty then
				Result := a
			else
				create b.init(a.left.min(left), a.right.max(right))
				Result := b
			end
		end
	subtract(a : RANGE_OF_NUMBERS) : RANGE_OF_NUMBERS
		require
			subtractable : is_super_range_of_other(a) = True and (a.left = l or a.right = r)
		local
			b : RANGE_OF_NUMBERS
		do
			if is_empty and a.left > a.right then
				Result := current
			elseif is_equal_range(a) then
				create b.init (2, 1)
				Result := b
			elseif a.left = l then
				create b.init(a.right + 1, r)
				Result := b
			else
				create b.init(l, a.left - 1)
				Result := b
			end
		end

feature {NONE}
	make
		do
			print("EVERYTHING WORKS!")
		end
end
