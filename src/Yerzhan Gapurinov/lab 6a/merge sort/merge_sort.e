note
	description: "merge_sort application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	MERGE_SORT

inherit
	ARGUMENTS

create
	make
feature
	a: ARRAYED_LIST[INTEGER]
	n: INTEGER

feature {NONE}

	make
		local
			i: INTEGER
		do
			create a.make(0)
			io.read_integer
			n := io.last_integer
			from i := 1	until i > n	loop
				io.read_integer
				a.extend(io.last_integer)
				i := i + 1
			end

			a := merge_sort(a)

			from i := 1 until i > n loop
				print(a[i].out + " ")
				i := i + 1
			end
		end
feature
	merge_sort(v: ARRAYED_LIST[INTEGER]): ARRAYED_LIST[INTEGER]
		local
			left: ARRAYED_LIST[INTEGER]
			right: ARRAYED_LIST[INTEGER]
			i: INTEGER
			j: INTEGER
			ans: ARRAYED_LIST[INTEGER]
		do
			create left.make (0);
			create right.make (0);
			create ans.make (0);
			if v.count = 1 then
				Result := v
			else
				from i := 1 until i > v.count // 2 loop
					left.extend (v[i])
					i := i + 1
				end
				left := merge_sort(left)
				from i := v.count // 2 + 1 until i > v.count loop
					right.extend (v[i])
					i := i + 1
				end
				right := merge_sort(right)
				from i := 1; j := 1 until i > left.count and j > right.count loop
					if i > left.count then
						ans.extend (right[j])
						j := j + 1
					elseif j > right.count then
						ans.extend (left[i])
						i := i + 1
					else
						if left[i] < right[j] then
							ans.extend (left[i])
							i := i + 1
						else
							ans.extend (right[j])
							j := j + 1
						end
					end
				end
				Result := ans
			end
		end
end
