note
	description: "reverse application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	REVERSE_STRING

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			word: STRING
			word2: STRING
		do
			print("Enter the first word: ")
			io.read_line
			word := io.last_string.out
			print("Enter the second word: ")
			io.read_line
			word2 := io.last_string.out
			print(reverse_iterative(word))
			print("%N")
			print(reverse_recursive(word2))
		end
feature
	reverse_recursive(s: STRING;): STRING
		do
			if s.capacity < 1 then
				Result := ""
			else
				Result := reverse_recursive(s.substring (2, s.capacity)) + s.at(1).out
			end
		end
	reverse_iterative(s: STRING): STRING
		local
			s2: STRING
			i: INTEGER
			n: INTEGER
		do
			n := s.capacity
			s2 := ""
			from
				i := n
			until
				i < 1
			loop
				s2 := s2 + s[i].out
				i := i - 1
			end
			Result := s2
		end
end
