note
	description: "power_set application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	POWER_SET

inherit
	ARGUMENTS

create
	make
feature {NONE}

	make
		local
			a: ARRAYED_LIST[INTEGER]
			i: INTEGER
			n: INTEGER
		do
			create a.make (0)
			io.read_integer
			n := io.last_integer
			from i := 1 until i > n loop
				io.read_integer
				a.extend (io.last_integer)
				i := i + 1
			end
			power_set(a)
		end
feature
	power_set(v: ARRAYED_LIST[INTEGER])
		local
			i: INTEGER
			j: INTEGER
			x: INTEGER
			two: INTEGER
			use: BOOLEAN
		do
			two := 2
			print ("{")
			from i := 0 until i > two.power(v.count)-1 loop
				x := i
				print("{")
				from j := 1 until j > v.count loop
					use := FALSE
					if x \\ 2  = 1 then
						print (v[j].out)
						use := TRUE
					end
					x := x // 2
					if x > 0 and use = TRUE then
						print(",")
					end
					j := j + 1
				end
				print ("}")
				if i < two.power(v.count)-1 then
					print(",")
				end
				i := i + 1
			end
			print("}")
			print("%N")
		end
end
