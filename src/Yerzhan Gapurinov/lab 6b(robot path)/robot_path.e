note
	description: "robot_path application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	ROBOT_PATH

inherit
	ARGUMENTS

create
	make
feature
	a: ARRAY2[CHARACTER]
	r: INTEGER
	c: INTEGER

feature {NONE}

	make
		local
			i: INTEGER
			j: INTEGER
			s: STRING
		do
			io.read_integer
			r := io.last_integer
			io.read_integer
			c := io.last_integer
			create a.make_filled ('.', r, c)
			s := ""
			from i := 1 until i > r loop
				io.read_line
				s := io.last_string.out
				from j := 1 until j > c loop
					a[i, j] := s[j]
					j := j + 1
				end
				i := i + 1
			end

			solve(r, c)
		end

feature
	solve(n: INTEGER; m: INTEGER)
		local
			i: INTEGER
			j: INTEGER
			dp: ARRAY2[BOOLEAN]
			ans: ARRAYED_LIST[STRING]
		do
			create dp.make_filled(FALSE, n, m)

			from i := 1 until i > n loop
				from j := 1 until j > m loop
					if i = 1 and j = 1 then
						dp[i, j] := TRUE
					elseif i > 1 and j = 1 then
						if a[i,j] = '.' and dp[i-1, j] = TRUE then
							dp[i,j] := TRUE
						end
					elseif j > 1 and i = 1 then
						if a[i,j] = '.' and dp[i, j-1] = TRUE then
							dp[i,j] := TRUE
						end
					else
						if a[i,j] = '.' and (dp[i, j-1] = TRUE or dp[i-1, j]) = TRUE then
							dp[i,j] := TRUE
						end
					end
					j := j + 1
				end
				i := i + 1
			end
			create ans.make (0)
			from
				i := n
				j := m
			until
				i = 1 and j = 1
			loop
				if dp[i-1, j] then
					i := i - 1
					ans.extend("down")
				elseif dp[i, j-1] then
					j := j - 1
					ans.extend("right")
				end
			end

			from i := ans.count until i = 0 loop
				print(ans[i] + " ")
				i := i - 1
			end
		end
end
