note
	description: "tower_of_hanoi application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	HANOI

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			x: INTEGER
		do
			print("Enter number of disks: ")
			io.read_integer
			x := io.last_integer

			print("Solution:%N")
			hanoi(x, 'A', 'B', 'C')
		end
feature
	hanoi(n: INTEGER; a: CHARACTER; b: CHARACTER; c: CHARACTER)
		require
			non_negative: n >= 0
			dif1: a /= b
			dif2: b /= c
			dif3: a /= c
		do
			if n > 0 then
				hanoi(n-1, a, c, b)
				move(a, b)
				hanoi(n-1, c, b, a)
			end
		end
	move(a: CHARACTER; b: CHARACTER)
		require
			dif1: a /= b
		do
			print(a.out + " -> " + b.out)
			print("%N")
		end
end
