note
	description: "hangman application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	HANGMAN_GAME

inherit
	ARGUMENTS

create
	make
feature
	a: ARRAYED_LIST[STRING]
	players: ARRAYED_LIST[STRING]
	cur_bool: ARRAY[BOOLEAN]
feature {NONE} -- Initialization

	make
		local
			i: INTEGER
			j: INTEGER
			kosu: STRING
			name: STRING
			n: INTEGER
			cnt: INTEGER
			finish: BOOLEAN
			rnd: INTEGER
			char: CHARACTER
			cur: STRING
			stop: INTEGER
			flag: INTEGER
		do
			create a.make (0)
			create players.make (0)
			create cur_bool.make_filled (False, 1, 15)
			create input_file.make_open_read ("input.txt")
            from
                input_file.read_line
            until
                input_file.exhausted
            loop
            	kosu := input_file.last_string.out
            	a.extend(kosu)
                input_file.read_line
            end
            input_file.close


			rnd := random_int
			cur := a[rnd]
			print(cur.out + "%N")
			cur[1] := cur[1].as_lower
     		from
     		until
     			stop > cur.count or finish = TRUE
     		loop
     			print_word(cur)
     			print("Enter your character: ")
			    io.read_line
				char := io.last_string.item (1)
				flag := 0
				from j := 1
				until j > cur.count
				loop
					if char = cur[j] then
						cur_bool[j] := TRUE
						cnt := cnt+1
						flag := flag + 1
					end
					j := j + 1
				end
				if check_use(cur.count) = cur.count then
					print_word(cur)
					print("CONGRATS!")
				end
				if flag = 0 then
					stop := stop + 1
				end
     		end
     		if finish = FALSE then
     			print("GAME OVER!!!")
			end
		end
	check_use(n: INTEGER): INTEGER
			local
				i: INTEGER
				cnt: INTEGER
			do
				cnt := 0
				from i := 1 until i > n
				loop
					if cur_bool[i] = TRUE then
						cnt := cnt + 1
					end
					i := i + 1
				end
				Result := cnt
			end
	random_int: INTEGER
		local
			l_time: TIME
     	 	l_seed: INTEGER
    	do
      		create l_time.make_now
     		l_seed := l_time.hour
     		l_seed := l_seed * 60 + l_time.minute
     		l_seed := l_seed * 60 + l_time.second
      		l_seed := l_seed * 1000 + l_time.milli_second
      		Result := l_seed \\ a.count + 1
    	end
	print_word(s: STRING)
		local
			i: INTEGER
		do
			print("%N")
			from i := 1 until i > s.count loop
				if cur_bool[i] = TRUE then
					print(s[i])
				else
					print("_")
				end
				i := i + 1
			end
			print("%N")
		end
feature -- Access
    input_file: PLAIN_TEXT_FILE

end
