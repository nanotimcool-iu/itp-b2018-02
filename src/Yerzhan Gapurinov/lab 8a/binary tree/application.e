note
	description: "binary_tree application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make
feature
	tree: NODE[INTEGER]
	tr2: NODE[INTEGER]
	tr3: NODE[INTEGER]
	tr4: NODE[INTEGER]

feature {NONE}
	make
		do
			create tree.init(1)
			create tr2.init(2)
			create tr3.init(3)
			create tr4.init(4)
			tr3.add(0,tr4)
			tr2.add(1, tr3)
			tree.add(1, tr2)
			print(tree.height)
		end

end
