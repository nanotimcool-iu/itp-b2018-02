note
	description: "Summary description for {NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NODE[G]

create
	init
feature
	value: G
	pair: detachable NODE[G]
	left_c: detachable NODE[G]
	right_c: detachable NODE[G]
	tree_height: INTEGER

feature
	init (a_value: G)
		do
			value := a_value
		end
	get_value: G
		do
			Result := value
		end
	right: detachable NODE[G]
		do
			Result := right_c
		end
	left: detachable NODE[G]
		do
			Result := left_c
		end
	set_pair(a_pair: NODE[G])
		do
			pair := a_pair
		end
	height: INTEGER
		do
			Result := tree_height
		end
	update_height(a_height: INTEGER)
		do
			tree_height := a_height
		end
	add(place: INTEGER; t: NODE[G])
		require
			sides: place = 1 or place = 0
			exist: (place = 0 and right_c = void) or (place = 1 and left_c = void)
		local
			cur: NODE[G]
			passed_nodes: INTEGER
		do
			passed_nodes := 0
			cur := current
			from until cur = void loop
				passed_nodes := passed_nodes + 1
				cur.update_height(cur.height.max(passed_nodes + t.height))
				passed_nodes := cur.height
				cur := cur.pair
			end
			if place = 0 then
				right_c := t
			else
				left_c := t
			end
			t.set_pair(current)
		end
end
