note
	description: "Summary description for {BOOKS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOOKS
create
	init
feature
	name: STRING
	number_of_book: INTEGER
	availability: BOOLEAN
	age_restriction: INTEGER
feature
	init(a_name: STRING; a_number_of_book: INTEGER; an_age_restriction: INTEGER)
		do
			name := a_name
			number_of_book := a_number_of_book
			age_restriction := an_age_restriction
		end
	get_name: STRING
		do
			Result := name
		end
	get_age_restriction: INTEGER
		do
			Result := age_restriction
		end
	get_number_of_book: INTEGER
		do
			Result := number_of_book
		end
	check_availability: BOOLEAN
		do
			if number_of_book > 0 then
				Result := TRUE
			else
				Result := FALSE
			end
		end
end
