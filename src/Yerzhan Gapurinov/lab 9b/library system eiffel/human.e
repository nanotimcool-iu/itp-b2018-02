note
	description: "Summary description for {HUMAN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	HUMAN

create
	init_human
feature
	name: STRING
	surname: STRING
	age: INTEGER
	ID: STRING
	address: STRING
	phone_number: INTEGER
	number_of_books: INTEGER
	list_of_taken_books: ARRAYED_LIST[TAKEN_BOOKS]
feature

	init_human(a_name, a_surname, an_address, an_ID: STRING; a_number_of_books, an_age, a_phone_number: INTEGER;)
		do
			create list_of_taken_books.make(0)
			name := a_name
			surname := a_surname
			address := an_address
			age := an_age
			ID := an_ID
			phone_number := a_phone_number
		end
	get_name: STRING
		do
			Result := name
		end
	get_surname: STRING
		do
			Result := surname
		end
	get_address: STRING
		do
			Result := address
		end
	get_age: INTEGER
		do
			Result := age
		end
	get_ID: STRING
		do
			Result := ID
		end
	get_phone_number: INTEGER
		do
			Result := phone_number
		end
end
