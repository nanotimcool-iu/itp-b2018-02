note
	description: "library application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	LIBRARY_SYSTEM

inherit
	ARGUMENTS

create
	make
feature

	list_of_users: ARRAYED_LIST[HUMAN]
	list_of_books: ARRAYED_LIST[BOOKS]

feature
	make
		local
			i: INTEGER
			j: INTEGER
			ID: STRING
			day: INTEGER
			month: INTEGER
			book_name: STRING
			user1: HUMAN
			user2: HUMAN
			user3: HUMAN
			book1: BOOKS
			book2: BOOKS
			book3: BOOKS
			book4: BOOKS
			stop: BOOLEAN
			id_check: BOOLEAN
		do

			day := 21
			month := 6
			stop := FALSE
			id_check := FALSE
			create user1.init_human ("Yerzhan", "Gapurinov", "Universitetskay", "001", 0, 18, 22083)
			create user2.init_human ("Ben", "Ten", "Univer", "002", 0, 11, 21322)
			create user3.init_human ("Ada", "Kenny", "Campus 4", "003", 0, 16, 212121)
			create book1.init ("Math", 2, 5)
			create book2.init ("Love", 1, 18)
			create book3.init ("Eiffel", 10, 12)
			create book4.init ("C++", 0, 16)
			create list_of_users.make(0)
			create list_of_books.make (0)
			list_of_users.extend (user1)
			list_of_users.extend (user2)
			list_of_users.extend (user3)
			list_of_books.extend (book1)
			list_of_books.extend (book2)
			list_of_books.extend (book3)
			list_of_books.extend (book4)
			print("Enter your ID please: ")
			io.read_line
			ID := io.last_string
			from i := 1 until i > list_of_users.count or stop = TRUE loop
				if list_of_users[i].get_ID ~ ID then
					id_check := TRUE
					print("Enter a name of book: ")
					io.read_line
					book_name := io.last_string
					from j := 1 until j > list_of_books.count loop
						if list_of_books[j].get_name ~ book_name then
							if list_of_users[i].get_age >= list_of_books[j].get_age_restriction then
								if(list_of_books[j].get_number_of_book > 0) then
									print("Take " + book_name.out + " and bring it back " + day.out + "." + (month \\ 13).out + "%N")
								else
									print("Sorry this book is not available right now!%N")
									stop := TRUE
								end
							else
								print("You are too young to this book!%N")
								stop := TRUE
							end
						end
						j := j + 1
					end
				end
				i := i + 1
			end
			if id_check = FALSE then
				print("You are not registered in our Library!%N")
			else
				if stop = FAlSE then
					print ("Sorry we have not this book.%N")
				else
					print("Good bye!%N")
				end

			end
		end

end
