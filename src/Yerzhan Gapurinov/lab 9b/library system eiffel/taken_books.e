note
	description: "Summary description for {TAKEN_BOOKS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	TAKEN_BOOKS

create
	init_book
feature
	name: STRING
	day: INTEGER
	month: INTEGER

feature

	init_book(a_name: STRING; a_day: INTEGER; a_month: INTEGER)
		do
			name := a_name
			day := a_day
			month := a_month
		end
	get_name: STRING
		do
			Result := name
		end
	get_returning_day: INTEGER
		do
			Result := day
		end
	get_returning_month: INTEGER
		do
			Result := (month + 1) \\ 13
 		end
end
